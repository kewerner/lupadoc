<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zsalg4373640c2s.html#6">Opere del conte
      Algarotti, Tom. II</a>
      <ul>
        <li>
          <a href="zsalg4373640c2s.html#8">Dialoghi sopra
          l'ottica neutoniana</a>
          <ul>
            <li>
              <a href="zsalg4373640c2s.html#10">A Frédéric le
              Grand</a>
            </li>
            <li>
              <a href="zsalg4373640c2s.html#18">Dialogo
              Primo</a>
            </li>
            <li>
              <a href="zsalg4373640c2s.html#66">Dialogo
              Secondo</a>
            </li>
            <li>
              <a href="zsalg4373640c2s.html#108">Dialogo
              Terzo</a>
            </li>
            <li>
              <a href="zsalg4373640c2s.html#155">Dialogo
              Quarto</a>
            </li>
            <li>
              <a href="zsalg4373640c2s.html#210">Dialogo
              Quinto</a>
            </li>
            <li>
              <a href="zsalg4373640c2s.html#278">Dialogo
              Sesto</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zsalg4373640c2s.html#352">Opuscoli spettanti
          al neutonianismo</a>
          <ul>
            <li>
              <a href="zsalg4373640c2s.html#354">Caritea ovvero
              Dialogo</a>
            </li>
            <li>
              <a href="zsalg4373640c2s.html#373">De colorum
              immutabilitate eorumque diversa refrangibilitate
              dissertatio</a>
            </li>
            <li>
              <a href="zsalg4373640c2s.html#396">Memoire</a>
            </li>
            <li>
              <a href="zsalg4373640c2s.html#410">Second
              Memoire</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zsalg4373640c2s.html#430">Indice delle
          materie contenute nel Tomo Secondo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
