<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbot738735417s.html#6">Raccolta Di Lettere Sulla
      Pittura Scultura Ed Architettura</a>
      <ul>
        <li>
          <a href="ghbot738735417s.html#8">Lo Stampatore agli
          amatori delle belle arti</a>
        </li>
        <li>
          <a href="ghbot738735417s.html#10">Lettere su la
          pittura scultura ed architettura</a>
        </li>
        <li>
          <a href="ghbot738735417s.html#82">Dialoghi di un
          amatore della verità</a>
        </li>
        <li>
          <a href="ghbot738735417s.html#157">All'illustrissimo
          et reverendissimo nostro protettore colendissimo Mons.
          Alfonso Gesualdo</a>
        </li>
        <li>
          <a href="ghbot738735417s.html#213">Indice de' nomi
          degli autori</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
