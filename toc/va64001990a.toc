<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="va64001990as.html#6">Origine, Et Progresso
      Dell’Academia Del Dissegno, De Pittori, Scultori, &amp;
      Architetti di Roma</a>
      <ul>
        <li>
          <a href="va64001990as.html#8">All'illustrissimo et
          reverendissimo Sig. Conte Federico Cardinale Borromeo</a>
        </li>
        <li>
          <a href="va64001990as.html#18">Origine e progresso
          dell'Academia del dissegno de' Pittori, Scultori, et
          Architetti di Roma</a>
        </li>
        <li>
          <a href="va64001990as.html#98">Parte delle conclusioni
          del Rev. D. Ventura Venturi da Siena</a>
          <ul>
            <li>
              <a href="va64001990as.html#98">All'illustrissimo et
              eccellentissimo Signore et Patron mio osservandissimo
              il Sig. Marchese della Corgna</a>
            </li>
            <li>
              <a href="va64001990as.html#100">Del
              dissegno&#160;</a>
            </li>
            <li>
              <a href="va64001990as.html#101">Pitori academici
              che di mano in mano si sottoscrissero di loro propria
              mano, et altri</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
