<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbet75653740s.html#6">Delle lettere e delle arti
      mantovane</a>
      <ul>
        <li>
          <a href="ghbet75653740s.html#8">Al nobil Signore il
          Signor Barone De Sperges e Palenz&#160;</a>
        </li>
        <li>
          <a href="ghbet75653740s.html#10">A chi legge</a>
        </li>
        <li>
          <a href="ghbet75653740s.html#12">Discorso primo delle
          lettere e delle arti mantovane dal 1000 fino al 1500</a>
        </li>
        <li>
          <a href="ghbet75653740s.html#60">Discorso secondo delle
          lettere e delle arti mantovane dall'anno 1500 fino al
          1600</a>
        </li>
        <li>
          <a href="ghbet75653740s.html#164">Poemetto a
          Mantova</a>
        </li>
        <li>
          <a href="ghbet75653740s.html#182">Appendice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
