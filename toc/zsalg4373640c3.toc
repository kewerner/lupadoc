<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zsalg4373640c3s.html#8">Opere del conte
      Algarotti, Tom. III.</a>
      <ul>
        <li>
          <a href="zsalg4373640c3s.html#10">Saggi sopra le
          belle arti</a>
          <ul>
            <li>
              <a href="zsalg4373640c3s.html#12">Saggio sopra
              l'architettura</a>
              <ul>
                <li>
                  <a href="zsalg4373640c3s.html#14">Al Signor
                  Senatore Co: Cesare Malvasia</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c3s.html#62">Saggio sopra la
              pittura</a>
              <ul>
                <li>
                  <a href="zsalg4373640c3s.html#64">All'
                  Accademia inglese</a>
                </li>
                <li>
                  <a href=
                  "zsalg4373640c3s.html#68">Introduzione</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#72">Della
                  educazione prima del pittore</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#76">Della
                  notomia</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#93">Della
                  prospettiva</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#106">Della
                  simmetria</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#120">Del
                  colorito</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#130">Dell' uso
                  della camera ottica</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#135">Delle
                  pieghe</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#141">Dello
                  studio del paesaggio e dell' architettura</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#145">Del
                  costume</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#151">Della
                  invenzione</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#175">Della
                  disposizione</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#186">Della
                  espressione degli affetti</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#198">Dei libri
                  convenienti al pittore</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#208">Della
                  utilita' di un amico con cui consigliarsi</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#214">Della
                  importanza del giudizio del pubblico</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#224">Della
                  critica necessaria al pittore</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#227">Della
                  bilancia pittorica</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#245">Della
                  imitazione</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#248">Delle
                  recreazioni del pittore</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#251">Della
                  fortunata condizione del pittore</a>
                </li>
                <li>
                  <a href=
                  "zsalg4373640c3s.html#259">Conclusione</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c3s.html#262">Saggio sopra
              l'Accademia di Francia che e' in Roma</a>
              <ul>
                <li>
                  <a href="zsalg4373640c3s.html#264">Al Signor
                  Tommaso Hollis</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c3s.html#318">Saggio sopra
              l'opera in musica</a>
              <ul>
                <li>
                  <a href="zsalg4373640c3s.html#320">A
                  Guglielmo Pitt</a>
                </li>
                <li>
                  <a href=
                  "zsalg4373640c3s.html#322">Introduzione</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#328">Del
                  libretto</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#338">Della
                  musica</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#362">Della
                  maniera del cantare e del recitare</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#374">Dei
                  balli</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#379">Delle
                  scene</a>
                </li>
                <li>
                  <a href="zsalg4373640c3s.html#395">Del
                  teatro</a>
                </li>
                <li>
                  <a href=
                  "zsalg4373640c3s.html#409">Conclusione</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="zsalg4373640c3s.html#414">Enea in Troja</a>
        </li>
        <li>
          <a href="zsalg4373640c3s.html#422">Iphigénie en
          Aulide</a>
        </li>
        <li>
          <a href="zsalg4373640c3s.html#482">Indice delle
          materie contenute nel Tomo Terzo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
