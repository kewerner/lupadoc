<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katpdor32503940s.html#6">Descrizione ragionata
      della Galleria Doria</a>
      <ul>
        <li>
          <a href="katpdor32503940s.html#8">Altezza</a>
        </li>
        <li>
          <a href="katpdor32503940s.html#12">Agli amatori della
          pittura</a>
        </li>
        <li>
          <a href="katpdor32503940s.html#15">Divisione del saggio
          pittorico</a>
        </li>
        <li>
          <a href="katpdor32503940s.html#16">Indice delle cose
          notabili</a>
        </li>
        <li>
          <a href="katpdor32503940s.html#23">Indice delle stanze,
          e bracci della Galleria</a>
        </li>
        <li>
          <a href="katpdor32503940s.html#24">Saggio di
          Pittura</a>
        </li>
        <li>
          <a href="katpdor32503940s.html#74">Descrizione
          ragionata della Galleria Doria</a>
        </li>
        <li>
          <a href="katpdor32503940s.html#215">Ristratto de'quadri
          già descritti</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
