<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zx3702730s.html#8">Athanasii Kircheri Phonurgia
      nova sive conjugium mechanico-physicum artis &amp; naturae
      paranympha phonosophia concinnatum</a>
      <ul>
        <li>
          <a href="zx3702730s.html#18">[Epistola dedicatoria]
          Leopoldo I. Romanorum Imperatori [...]&#160;</a>
        </li>
        <li>
          <a href="zx3702730s.html#24">Exegis frontispicii
          praecedentis</a>
        </li>
        <li>
          <a href="zx3702730s.html#26">In Phonurgiam Kircherianum
          Syncharmicum</a>
        </li>
        <li>
          <a href="zx3702730s.html#29">Ad P. Anthanasium
          Kircherum</a>
        </li>
        <li>
          <a href="zx3702730s.html#32">[Epigramma] Admirabilem
          Phonurgiam P. Athanasii Kircheri</a>
        </li>
        <li>
          <a href="zx3702730s.html#33">Praefatio ad lectorem</a>
        </li>
        <li>
          <a href="zx3702730s.html#36">Authentica Testimonia de
          prima hujus Artis inventione</a>
        </li>
        <li>
          <a href="zx3702730s.html#42">Index argumentorum, quae
          hoc Opere continentur</a>
        </li>
        <li>
          <a href="zx3702730s.html#54">Athanasii Kirchere
          Phonurgiae</a>
          <ul>
            <li>
              <a href="zx3702730s.html#54">Liber I. Phonosophia
              anacamptica quae et Ars Echonica dictur&#160;</a>
              <ul>
                <li>
                  <a href="zx3702730s.html#54">Praefatio</a>
                </li>
                <li>
                  <a href="zx3702730s.html#59">Sectio I. De natura
                  et proprietate soni</a>
                  <ul>
                    <li>
                      <a href="zx3702730s.html#59">Caput I. Qua
                      reflexi soni Natura et proprietas
                      describitur</a>
                    </li>
                    <li>
                      <a href="zx3702730s.html#71">II.
                      Demonstrationes soni Reflexi&#160;</a>
                    </li>
                    <li>
                      <a href="zx3702730s.html#75">III. Canones
                      phonocamptici sive Echometrici</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="zx3702730s.html#94">Sectio II.
                  Architectura echonica</a>
                </li>
                <li>
                  <a href="zx3702730s.html#111">Sectio III.
                  Tuborum tubarumque acusticarum fabrica</a>
                </li>
                <li>
                  <a href="zx3702730s.html#126">Sectio IV. De
                  Fabricis in usum recreationemque Principum, queis
                  secreto Consilia sua sibi invicem communicare
                  possint, constiduendis</a>
                </li>
                <li>
                  <a href="zx3702730s.html#156">Sectio V. De
                  Mirificis organorum Acusticorum Fabricis, queis
                  [...]</a>
                </li>
                <li>
                  <a href="zx3702730s.html#164">Sectio Vi.
                  Acrchitectonica Instrumentorum acusticorum,
                  quorum [...]</a>
                </li>
                <li>
                  <a href="zx3702730s.html#179">Sectio VII. De
                  Fabricis diversorum organorum, ad producendum
                  longissime sonum apte construendis</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zx3702730s.html#228">Liber II. Phonosophia
              nova, qua recondita, et abstrusae sonorum rationes
              per numeros exponuntur</a>
              <ul>
                <li>
                  <a href="zx3702730s.html#229">Sectio I. De
                  Prodigiosa Sonorum quorundam vi et affiacia</a>
                </li>
                <li>
                  <a href="zx3702730s.html#251">Sectio II.
                  Phonurgia latrica sive De Perturbationibus animi,
                  morbisque vi Musicae curandis&#160;</a>
                </li>
                <li>
                  <a href="zx3702730s.html#274">Sectio III. De
                  variis prodigiosis sonis</a>
                </li>
                <li>
                  <a href="zx3702730s.html#288">Epistola</a>
                </li>
                <li>
                  <a href="zx3702730s.html#295">Index rerum et
                  verborum</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
