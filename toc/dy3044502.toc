<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy3044502s.html#6">La patriarcale Basilica
      Vaticana; Tomo II.</a>
      <ul>
        <li>
          <a href="dy3044502s.html#8">[Prima parte del Volume
          secondo]</a>
          <ul>
            <li>
              <a href="dy3044502s.html#10">Nave minore a
              sinistra</a>
            </li>
            <li>
              <a href="dy3044502s.html#49">Tribuna meridionale
              detta de' Ss. Simone e Giuda</a>
            </li>
            <li>
              <a href="dy3044502s.html#57">Altari della Madonna e
              di S. Leone Magno, ed arcate contigue</a>
            </li>
            <li>
              <a href="dy3044502s.html#65">Della sacrestia
              vaticana</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy3044502s.html#94">Seconda parte del Volume
          secondo</a>
          <ul>
            <li>
              <a href="dy3044502s.html#96">Introduzione</a>
            </li>
            <li>
              <a href="dy3044502s.html#100">Arcata prima</a>
            </li>
            <li>
              <a href="dy3044502s.html#102">Arcata seconda</a>
            </li>
            <li>
              <a href="dy3044502s.html#105">Arcata terza</a>
            </li>
            <li>
              <a href="dy3044502s.html#108">Arcata quarta</a>
            </li>
            <li>
              <a href="dy3044502s.html#111">Arcata quinta</a>
            </li>
            <li>
              <a href="dy3044502s.html#114">Arcata sesta</a>
            </li>
            <li>
              <a href="dy3044502s.html#117">Arcata settima</a>
            </li>
            <li>
              <a href="dy3044502s.html#119">Arcata ottava</a>
            </li>
            <li>
              <a href="dy3044502s.html#123">Arcata nona</a>
            </li>
            <li>
              <a href="dy3044502s.html#125">Arcata decima</a>
            </li>
            <li>
              <a href="dy3044502s.html#128">Arcata undecima</a>
            </li>
            <li>
              <a href="dy3044502s.html#131">Arcata duodecima</a>
            </li>
            <li>
              <a href="dy3044502s.html#134">Arcata
              tredicesima</a>
            </li>
            <li>
              <a href="dy3044502s.html#140">[Tavole]</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
