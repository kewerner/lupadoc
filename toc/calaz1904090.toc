<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="calaz1904090s.html#7">Gregorio Lazzarini Pittore
      ▣</a>
    </li>
    <li>
      <a href="calaz1904090s.html#8">Vita di Gregorio Lazzarini
      scritta da Vincenzo da Canal P.V.</a>
      <ul>
        <li>
          <a href="calaz1904090s.html#10">[Dedica]</a>
        </li>
        <li>
          <a href="calaz1904090s.html#12">Praefazione
          dell'Editore</a>
        </li>
        <li>
          <a href="calaz1904090s.html#20">Introduzione
          dell'Autore</a>
        </li>
        <li>
          <a href="calaz1904090s.html#23">Vita di Gregorio
          Lazzarini</a>
        </li>
        <li>
          <a href="calaz1904090s.html#85">Annotazioni
          dell'Editore</a>
        </li>
        <li>
          <a href="calaz1904090s.html#92">Nomi de' Pittori di cui
          si parla nella Vita</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
