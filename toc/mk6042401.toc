<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="mk6042401s.html#8">Storia dell'arte col mezzo dei
      monumenti dalla sua decadenza nel IV. secolo fino al suo
      risorgimento nel XVI. Volume I.</a>
      <ul>
        <li>
          <a href="mk6042401s.html#10">L'Editore
          [Introduzione]</a>
        </li>
        <li>
          <a href="mk6042401s.html#14">Notizie sulla vita e sui
          lavori di G.B.L.G. Seroux D'Agincourt</a>
        </li>
        <li>
          <a href="mk6042401s.html#28">Prefazione</a>
        </li>
        <li>
          <a href="mk6042401s.html#34">Discorso preliminare Scopo
          e Disegno dell'Opera Specie e Sorgente dei Monumenti sui
          quali è fondata</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="mk6042401s.html#42">Architettura</a>
      <ul>
        <li>
          <a href="mk6042401s.html#42">Introduzione</a>
          <ul>
            <li>
              <a href="mk6042401s.html#44">Dell'Architettura
              presso gli Egizj</a>
            </li>
            <li>
              <a href="mk6042401s.html#46">Dell'Architettura
              presso gli Etruschi</a>
            </li>
            <li>
              <a href="mk6042401s.html#48">Dell'Architettura
              presso i Greci</a>
            </li>
            <li>
              <a href="mk6042401s.html#49">Dell'Architettura
              presso i Romani</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042401s.html#54">I. Decadenza
          dell'Architettura dal IV secolo fino allo stabilimento
          del sistema gotico</a>
          <ul>
            <li>
              <a href="mk6042401s.html#54">Tavola I.
              L'Architettura antica nel suo stato di perfezione
              presso i Greci ed i Romani</a>
            </li>
            <li>
              <a href="mk6042401s.html#56">Tavola II. Principio
              della decadenza sotto i regni di Settimio Severo, di
              Diocleziano, e di Costantino. II, III e IV secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#58">Tavola III. Veduta
              dell'interno di una corte del palazzo di Diocleziano
              a Spalatro [sic]. III Secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#59">Tavola IV. Basilica di
              ›San Paolo fuori le Mura‹ di Roma, ne' suoi diversi
              stati, dalla sua fondazione nel IV secolo, fino al
              presente</a>
            </li>
            <li>
              <a href="mk6042401s.html#63">Tavola V. Arco della
              gran navata di ›San Paolo fuori le Mura‹, sostenuto
              da due colonne differenti di epoca e di stile</a>
            </li>
            <li>
              <a href="mk6042401s.html#64">Tavola VI. Base e
              capitello corintio della nave di ›San Paolo fuori le
              Mura‹, del miglior tempo antico</a>
            </li>
            <li>
              <a href="mk6042401s.html#64">Tavola VII. Base e
              capitello composito della nave di ›San Paolo fuori le
              Mura‹, del tempo della sua costruzione. IV secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#65">Tavola VIII. Basilica
              di ›Sant'Agnese fuori le Mura‹ di Roma : chiesa di
              santa Costanza ›Mausoleo di Santa Costanza‹ : tempio
              di Nocera. IV. Secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#70">Tavola IX. Veduta
              delle più celebri catacombe sì pagane che
              cristiane</a>
            </li>
            <li>
              <a href="mk6042401s.html#82">Tavola X. Parte delle
              catacombe od ipogei etruschi dell'antica Tarquinia
              presso Corneto</a>
            </li>
            <li>
              <a href="mk6042401s.html#82">Tavola XI. Altra parte
              delle catacombe etrusche di Tarquinia</a>
            </li>
            <li>
              <a href="mk6042401s.html#83">Tavola XII. ›Sepolcro
              degli Scipioni‹, ›Catacombe di sant'Ermete‹, tomba di
              questo santo convertita in altare</a>
            </li>
            <li>
              <a href="mk6042401s.html#87">Tavola XIII. Cappelle
              ed oratorj delle catacombe, le cui forme trasportate
              nelle chiese cristiane, vi hanno alterate quelle
              dell'Architettura antica</a>
            </li>
            <li>
              <a href="mk6042401s.html#93">Tavola XIV. Pianta di
              ›San Martino ai Monti‹ in Roma, esempio di una chiesa
              innalzata al di sopra di un oratorio sotterraneo. IV
              secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#93">Tavola XV. San Nazaro
              [sic] e san Celso a Ravenna: imitazione d'una
              cappella sepolcrale sotterranea. V secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#94">Tavola XVI. ›San
              Clemente‹ a Roma, modello meglio conservato della
              disposizione delle primitive chiese. V secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#97">Tavola XVII. Palazzi,
              chiese ed altre costruzioni del tempo di Teodorico, a
              Terracina ed a Ravenna. V e VI secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#99">Tavola XVIII. Mausoleo
              di Teodorico a Ravenna, oggi Santa Maria della
              Rotonda. VI secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#101">Tavola XIX. Pianta,
              elevazioni e dettagli del ›Ponte Salario‹, sul
              Teverone ›Aniene‹, vicino a Roma; riedificato da
              Narsete. VI secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#102">Tavola XX. Tempio
              antico della Caffarella ›Sant'Urbano‹ vicino a Roma,
              uno dei primi esempj di un tempio pagano consacrato
              al culto cristiano. IV secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#103">Tavola XXI. ›San
              Pietro in Vincoli‹ a Roma, esempio d'una chiesa
              fabbricata con colonne antiche. V secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#104">Tavola XXII. ›Santo
              Stefano Rotondo‹ a Roma, esempio d'un edifizio antico
              convertito in chiesa. V o VI secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#107">Tavola XXIII. Chiesa
              di san Vitale a Ravenna fabbricata sotto il regno di
              Giustiniano e con disegni provenuti dall'Oriente. VI
              secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#110">Tavola XXIV. Figura e
              dettagli di varie chiese, stile dell'Architettura in
              Italia sotto il regno de' Longobardi. VI, VII ed VIII
              secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#113">Tavola XXV.
              Architettura migliorata in Italia sotto il regno di
              Carlo Magno nel IX secolo, e dai Pisani nel X ed XI
              secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#123">Tavola XXVI. Santa
              Sofia di Costantinopoli, san Marco ed altre chiese di
              Venezia, fabbricate secondo lo stile greco
              moderno</a>
            </li>
            <li>
              <a href="mk6042401s.html#126">Tavola XXVII. Quadro
              generale della decadenza della Architettura, nelle
              contrade orientali</a>
            </li>
            <li>
              <a href="mk6042401s.html#131">Tavola XXVIII. Ultimo
              grado della decadenza dell'Architettura nelle
              province occidentali d'Italia. XIII secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#135">Tavola XXIX. Edifizj
              monastici: pianta, elevazione e dettagli del
              monastero di santa Scolastica, a Subiaco, presso
              Roma. XIII secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#135">Tavola XXX. Piante, e
              spaccati del chiostro di ›San Giovanni in Laterano‹ e
              di ›San Paolo fuori le Mura‹ di Roma. XII, XIII
              secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#135">Tavola XXXI. Chiostro
              di ›San Paolo fuori le Mura‹: spaccati generali,
              sopra una scala maggiore, e dettagli delle basi e dei
              capitelli delle sue colonne</a>
            </li>
            <li>
              <a href="mk6042401s.html#136">Tavola XXXII. Il
              medesimo chiostro, pianta ed elevazioni, disegnate
              più in grande, d'alcune parti delle sue facciate ›San
              Paolo fuori le Mura‹</a>
            </li>
            <li>
              <a href="mk6042401s.html#136">Tavola XXXIII. Il
              medesimo chiostro ›San Paolo fuori le Mura‹: dettagli
              dell'Architrave ricco di musaici : ornamenti scolpiti
              fra gli archi</a>
            </li>
            <li>
              <a href="mk6042401s.html#137">Tavola XXXIV. Piante,
              elevazioni e dettagli della ›Casa dei Crescenzi‹ a
              Roma. XI secolo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042401s.html#140">II. Sistema
          d'Architettura detto gotico dal IX, X, ed XI secolo sino
          verso la metà del secolo XV</a>
          <ul>
            <li>
              <a href="mk6042401s.html#140">Tavola XXXV. Primi
              indizj dell'Architettura detta gotica in Italia,
              nella badia di Subiaco presso Roma</a>
            </li>
            <li>
              <a href="mk6042401s.html#150">Tavola XXXVI.
              Riunione di diversi edifizj che mostrano lo stile
              della Architettura gotica, dalla sua origine nel IX
              secolo sino al secolo XIII</a>
            </li>
            <li>
              <a href="mk6042401s.html#152">Tavola XXXVII.
              Piante, spaccati e dettagli delle chiese inferiori e
              superiori di san Francesco, ad Assisi</a>
            </li>
            <li>
              <a href="mk6042401s.html#154">Tavola XXXVIII.
              Pianta, spaccato e facciata della chiesa di san
              Flaviano, presso Montefiascone. XII e XIII secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#158">Tavola XXXIX. Pianta,
              spaccato sulla lunghezza e parti in grande della
              chiesa di Notre-Dame, cattedrale di Parigi. XII e
              XIII secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#159">Tavola XL. Facciata,
              elevazione laterale, veduta interna e dettagli della
              decorazione della chiesa di Notre-Dame in Parigi. XII
              e XIII secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#160">Tavola XLI. Monumenti
              principali dell'Architettura gotica innalzati in
              varie parti d'Europa nella più brillante epoca di
              questo sistema. XIV e XV secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#166">Tavola XLII. Serie
              cronologica degli archi sostituiti alle trabeazioni,
              nella Architettura detta gotica, e delle altre parti
              che ne constituiscono [sic] il sistema</a>
            </li>
            <li>
              <a href="mk6042401s.html#170">Tavola XLIII.
              Architettura di Svezia prima e dopo la introduzione
              in que' paesi, nel XIII secolo, del sistema
              gotico</a>
            </li>
            <li>
              <a href="mk6042401s.html#175">Tavola XLIV. Stato
              dell'Architettura araba in Europa dall'VIII secolo
              fino al XV</a>
            </li>
            <li>
              <a href="mk6042401s.html#181">Tavola XLV. Serie di
              edifizj di diversi paesi, i quali sembrano
              partecipare dello stile detto gotico, ed aver
              contribuito alla sua invenzione in Europa</a>
            </li>
            <li>
              <a href="mk6042401s.html#190">Tavola XLVI.
              Congettura sull'origine, sulle forme diverse e
              sull'uso dell'arco detto gotico ne' paesi i più
              conosciuti</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042401s.html#196">III. Risorgimento
          dell'Architettura verso la metà del XV secolo</a>
          <ul>
            <li>
              <a href="mk6042401s.html#205">Tavola XLVII. Pianta
              e spaccato della chiesa di san Lorenzo a Firenze,
              opera di Filippo Brunelleschi principal autore del
              risorgimento dell'Architettura</a>
            </li>
            <li>
              <a href="mk6042401s.html#205">Tavola XLVIII.
              Intercolonio e dettagli dell'ordine interno della
              chiesa di san Lorenzo a Firenze, opera del
              Brunelleschi</a>
            </li>
            <li>
              <a href="mk6042401s.html#205">Tavola XLIX. Pianta,
              spaccato, elevazione e dettagli della chiesa di Santo
              Spirito a Firenze, opera del Brunelleschi</a>
            </li>
            <li>
              <a href="mk6042401s.html#205">Tavola L. Principali
              opere di Architettura del Brunelleschi</a>
            </li>
            <li>
              <a href="mk6042401s.html#208">Tavola LI. Pianta,
              elevazione e dettagli della chiesa di san Francesco a
              Rimini, terminata coi disegni di Leon Battista
              Alberti. XV secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#211">Tavola LII. Chiesa di
              Sant'Andrea e di San Sebastiano, a Mantova, innalzate
              sui disegni di Leon Battista Alberti. XV secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#214">Tavola LIII. Arco di
              trionfo innalzato a Napoli in onore di Alfonso I
              d'Aragona; fortificazioni militari. XV secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#219">Tavola LIV. Diversi
              edifizj innalzati a Roma ed a Napoli. XIII, XIV, XV
              secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#221">Tavola LV. Antico
              teatro dei confratelli della passione a Velletri
              presso Roma. XV secolo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042401s.html#226">IV. Risorgimento
          dell'Architettura alla fine del XV secolo ed in principio
          del XVI</a>
          <ul>
            <li>
              <a href="mk6042401s.html#226">Tavola LVI. Studj di
              Architettura, disegnati dall'antico da Bramante e da
              Antonio da Sangallo</a>
            </li>
            <li>
              <a href="mk6042401s.html#228">Tavola LVII.
              Principali opere d'Architettura di Bramante Lazzari;
              edifizj civili. Principio del XVI secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#228">Tavola &#160;LVIII.
              Continuazione delle opere di Bramante Lazzari;
              edifizj sacri. Principio del XVI secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#235">Tavole LIX e LX.
              Piante, elevazioni, spaccati e profili de' principali
              edifizj eseguiti sui disegni di Michelangelo
              Buonarroti. XVI secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#245">Tavola LXI. Piante,
              spaccati, e dettagli dell'antica e della nuova
              basilica di ›San Pietro in Vaticano‹, a Roma. IV, XV,
              XVI e XVII secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#246">Tavola LXII. Veduta
              generale della basilica di ›San Pietro in Vaticano‹,
              del palazzo del Vaticano ›Palazzo Apostolico
              Vaticano‹ e della annessa piazza ›Piazza San
              Pietro‹</a>
              <ul>
                <li>
                  <a href="mk6042401s.html#247">Primo disegno</a>
                </li>
                <li>
                  <a href="mk6042401s.html#248">Secondo
                  disegno</a>
                </li>
                <li>
                  <a href="mk6042401s.html#249">Terzo disegno</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042401s.html#254">Tavola LXIII. Forme
              dei principali battisteri, specie particolare di
              edifizj dovuta allo stabilimento della religione
              cristiana</a>
            </li>
            <li>
              <a href="mk6042401s.html#261">Tavola LXIV. Quadro
              storico e cronologico delle facciate dei tempi [sic],
              prima e durante la decadenza</a>
            </li>
            <li>
              <a href="mk6042401s.html#266">Tavola LXV. Serie
              degli architravi adoperati nell'interno degli edifizj
              in tutto il tempo della decadenza dell'Arte; non che
              degli archi di diversa forma che vi furono
              sostituiti</a>
            </li>
            <li>
              <a href="mk6042401s.html#267">Tavola LXVI.
              Principali forme delle vôlte o delle soffitta usate
              negli edifizj sacri, in tempo della decadenza
              dell'Arte</a>
            </li>
            <li>
              <a href="mk6042401s.html#269">Tavola LXVII. Quadro
              cronologico dell'invenzione e dell'uso delle
              cupole</a>
            </li>
            <li>
              <a href="mk6042401s.html#276">Tavola LXVIII. Quadro
              delle forme e delle proporzioni di colonne, usate
              prima e durante la decadenza dell'Arte, fino al suo
              risorgimento</a>
            </li>
            <li>
              <a href="mk6042401s.html#277">Tavola LXIX. Serie
              cronologica delle diverse specie di basi e di
              capitelli, adoperate dal principio della decadenza
              fino all'XI secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#279">Tavola LXX.
              Continuazione della serie cronologica delle basi e
              de' capitelli, dall'XI secolo fino al XVI</a>
            </li>
            <li>
              <a href="mk6042401s.html#280">Tavola LXXI. Maniere
              diverse di costruzione usate prima e durante la
              decadenza dell'Arte</a>
              <ul>
                <li>
                  <a href="mk6042401s.html#281">Mura di
                  pietra</a>
                </li>
                <li>
                  <a href="mk6042401s.html#282">Archi e vôlte in
                  pietre di taglio</a>
                </li>
                <li>
                  <a href="mk6042401s.html#283">Opus incertum di
                  Vitruvio. Pietre o rottami di tufo. Opus
                  reticulatum</a>
                </li>
                <li>
                  <a href="mk6042401s.html#284">Mura, vôlte ed
                  archi di mattoni</a>
                </li>
                <li>
                  <a href="mk6042401s.html#285">Miscuglio di
                  costruzione in pietre ed in mattoni</a>
                </li>
                <li>
                  <a href="mk6042401s.html#287">Uso dei vasi e
                  tubi di terra nelle costruzioni</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042401s.html#288">Tavola LXXII. Quadro
              comparativo dello stile dell'Architettura civile,
              dalla sua decadenza fino all'epoca del completo suo
              rinnovellamento</a>
            </li>
            <li>
              <a href="mk6042401s.html#292">Tavola LXXIII ed
              ultima</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042401s.html#292">RIASSUNTO E QUADRO
          GENERALE dei monumenti che servono a tessere la storia
          della decadenza dell'Architettura</a>
          <ul>
            <li>
              <a href="mk6042401s.html#293">PRIMO STATO
              DELL'ARTE. Monumenti della decadenza, nel IV e V
              secolo, nei quali si riscontra la imitazione dei
              tempj [sic] e delle basiliche antiche</a>
            </li>
            <li>
              <a href="mk6042401s.html#294">SECONDO STATO
              DELL'ARTE. Monumenti del V, VI e VII secolo</a>
            </li>
            <li>
              <a href="mk6042401s.html#295">TERZO STATO
              DELL'ARTE. Epoca di Carlomagno e del papa Adriano I.
              Monumenti dei secoli VIII e IX</a>
            </li>
            <li>
              <a href="mk6042401s.html#295">QUARTO STATO
              DELL'ARTE. Monumenti dei secoli X, XI e XII</a>
            </li>
            <li>
              <a href="mk6042401s.html#296">QUINTO STATO
              DELL'ARTE. Monumenti del sistema gotico, dall'XI
              secolo sino alla metà del XV</a>
            </li>
            <li>
              <a href="mk6042401s.html#296">SESTO STATO
              DELL'ARTE. Monumenti del risorgimento, nel XV secolo,
              e del completo rinnovamento dell'Arte, nel XVI</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042401s.html#298">Tavola delle principali
          divisioni della storia dell'Architettura</a>
        </li>
        <li>
          <a href="mk6042401s.html#300">Tavola delle materie
          della Storia dell'arte per mezzo dei monumenti - Sezione
          d'Architettura</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
