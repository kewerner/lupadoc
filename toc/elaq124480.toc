<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="elaq124480s.html#7">Monumenti storici artistici
      della città di Aquila e suoi contorni</a>
      <ul>
        <li>
          <a href="elaq124480s.html#10">Avvertenza</a>
        </li>
        <li>
          <a href="elaq124480s.html#18">Monumenti storici
          artistici della città di Aquila e suoi contorni</a>
          <ul>
            <li>
              <a href="elaq124480s.html#29">Le mura della città e
              distribuzione di essa</a>
            </li>
            <li>
              <a href="elaq124480s.html#32">S. Spirito</a>
            </li>
            <li>
              <a href="elaq124480s.html#35">La Madonna
              Lauretana</a>
            </li>
            <li>
              <a href="elaq124480s.html#37">S. Benedetto</a>
            </li>
            <li>
              <a href="elaq124480s.html#38">S. Teresa</a>
            </li>
            <li>
              <a href="elaq124480s.html#39">S. Petro di
              Coppito</a>
            </li>
            <li>
              <a href="elaq124480s.html#45">S. Silvestro</a>
            </li>
            <li>
              <a href="elaq124480s.html#56">Le case de'
              Branconi</a>
            </li>
            <li>
              <a href="elaq124480s.html#59">S. Domenico</a>
            </li>
            <li>
              <a href="elaq124480s.html#70">S. Pietro di
              Sassa</a>
            </li>
            <li>
              <a href="elaq124480s.html#71">La Madonna
              Addolorata</a>
            </li>
            <li>
              <a href="elaq124480s.html#72">La Madonna del
              Rifugio (oggi S. Bernardo)</a>
            </li>
            <li>
              <a href="elaq124480s.html#73">Lo Spirito Santo</a>
            </li>
            <li>
              <a href="elaq124480s.html#74">La Rivera</a>
            </li>
            <li>
              <a href="elaq124480s.html#76">La Madonna del Ponte
              della Rivera</a>
            </li>
            <li>
              <a href="elaq124480s.html#76">S. Giovanni di
              Lucoli</a>
            </li>
            <li>
              <a href="elaq124480s.html#76">S. Chiara
              d'Aquila</a>
            </li>
            <li>
              <a href="elaq124480s.html#78">S. Apollonia</a>
            </li>
            <li>
              <a href="elaq124480s.html#80">S. Marciano</a>
            </li>
            <li>
              <a href="elaq124480s.html#82">S. Maria di Rojo</a>
            </li>
            <li>
              <a href="elaq124480s.html#85">S. Chiara</a>
            </li>
            <li>
              <a href="elaq124480s.html#90">L'Annunziata</a>
            </li>
            <li>
              <a href="elaq124480s.html#92">S. Maria di
              Cascina</a>
            </li>
            <li>
              <a href="elaq124480s.html#95">La Madonna della
              Misericordia</a>
            </li>
            <li>
              <a href="elaq124480s.html#97">S. Amico</a>
            </li>
            <li>
              <a href="elaq124480s.html#98">S. Basilio e S. Maria
              del Guasto</a>
            </li>
            <li>
              <a href="elaq124480s.html#98">S. Martino</a>
            </li>
            <li>
              <a href="elaq124480s.html#99">S. Maria di
              Paganica</a>
            </li>
            <li>
              <a href="elaq124480s.html#106">La Madonna
              de'Raccomandati</a>
            </li>
            <li>
              <a href="elaq124480s.html#106">Il Palazzo del
              Comune</a>
            </li>
            <li>
              <a href="elaq124480s.html#109">Il Palazzo de'
              Tribunali</a>
            </li>
            <li>
              <a href="elaq124480s.html#115">S. Francesco a
              Palazzo</a>
            </li>
            <li>
              <a href="elaq124480s.html#120">S. Maria ad
              Civitatem</a>
            </li>
            <li>
              <a href="elaq124480s.html#122">La Concezione</a>
            </li>
            <li>
              <a href="elaq124480s.html#123">S. Margherita</a>
            </li>
            <li>
              <a href="elaq124480s.html#125">S. Caterina
              Martire</a>
            </li>
            <li>
              <a href="elaq124480s.html#126">S. Filippo</a>
            </li>
            <li>
              <a href="elaq124480s.html#129">S. Giuseppe</a>
            </li>
            <li>
              <a href="elaq124480s.html#140">S. Caterina da
              Siena</a>
            </li>
            <li>
              <a href="elaq124480s.html#141">S. Massimo</a>
            </li>
            <li>
              <a href="elaq124480s.html#150">La Fraternità di S.
              Massimo</a>
            </li>
            <li>
              <a href="elaq124480s.html#153">Galleria del
              Marchese de Torres</a>
            </li>
            <li>
              <a href="elaq124480s.html#155">S. Antonio di
              Padova</a>
            </li>
            <li>
              <a href="elaq124480s.html#157">S. Francesco di
              Paola</a>
            </li>
            <li>
              <a href="elaq124480s.html#158">S. Marco</a>
            </li>
            <li>
              <a href="elaq124480s.html#159">S. Agostino</a>
            </li>
            <li>
              <a href="elaq124480s.html#162">S. Michele</a>
            </li>
            <li>
              <a href="elaq124480s.html#165">S. Giusta</a>
            </li>
            <li>
              <a href="elaq124480s.html#175">S. Flaviano</a>
            </li>
            <li>
              <a href="elaq124480s.html#177">L'Ospedale di S.
              Salvatore</a>
            </li>
            <li>
              <a href="elaq124480s.html#179">La Madonna del
              Carmine e S. Tommaso</a>
            </li>
            <li>
              <a href="elaq124480s.html#180">S. Tommaso</a>
            </li>
            <li>
              <a href="elaq124480s.html#181">Il Castello</a>
            </li>
            <li>
              <a href="elaq124480s.html#186">I condotti
              dell'Aquila</a>
            </li>
            <li>
              <a href="elaq124480s.html#187">S. Sisto</a>
            </li>
            <li>
              <a href="elaq124480s.html#188">S. Giuliano</a>
            </li>
            <li>
              <a href="elaq124480s.html#191">La Madonna del
              Soccorso</a>
            </li>
            <li>
              <a href="elaq124480s.html#199">S. Bernardino</a>
            </li>
            <li>
              <a href="elaq124480s.html#229">S. Maria di
              Forfona</a>
            </li>
            <li>
              <a href="elaq124480s.html#229">S. Maria di
              Collemaggio</a>
            </li>
            <li>
              <a href="elaq124480s.html#245">S. Maria del Ponte
              fuori Porta Nuova</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="elaq124480s.html#247">I contorni
          dell'Aquila</a>
          <ul>
            <li>
              <a href="elaq124480s.html#248">Pettino, e la Murata
              del Diavolo</a>
            </li>
            <li>
              <a href="elaq124480s.html#252">S. Vittorino</a>
            </li>
            <li>
              <a href="elaq124480s.html#262">Pizzoli</a>
            </li>
            <li>
              <a href="elaq124480s.html#264">Preturo</a>
            </li>
            <li>
              <a href="elaq124480s.html#268">Cese</a>
            </li>
            <li>
              <a href="elaq124480s.html#269">Civita Tomassa e il
              piano di San Silvestro</a>
            </li>
            <li>
              <a href="elaq124480s.html#271">Tornimparte</a>
            </li>
            <li>
              <a href="elaq124480s.html#272">Lucoli</a>
            </li>
            <li>
              <a href="elaq124480s.html#275">S. Pietro di
              Sassa</a>
            </li>
            <li>
              <a href="elaq124480s.html#275">Civita di Bagno</a>
            </li>
            <li>
              <a href="elaq124480s.html#277">Ocre</a>
            </li>
            <li>
              <a href="elaq124480s.html#281">Fossa</a>
            </li>
            <li>
              <a href="elaq124480s.html#286">S. Eusanio</a>
            </li>
            <li>
              <a href="elaq124480s.html#287">Sinizzo</a>
            </li>
            <li>
              <a href="elaq124480s.html#289">S. Paolo di Peltuino
              in Prata</a>
            </li>
            <li>
              <a href="elaq124480s.html#291">Poggio di
              Picenza</a>
            </li>
            <li>
              <a href="elaq124480s.html#292">Filetto</a>
            </li>
            <li>
              <a href="elaq124480s.html#293">Paganica</a>
            </li>
            <li>
              <a href="elaq124480s.html#295">Bazzano</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="elaq124480s.html#298">Appendice prima</a>
        </li>
        <li>
          <a href="elaq124480s.html#302">Appendice II</a>
        </li>
        <li>
          <a href="elaq124480s.html#312">Appendice III</a>
        </li>
        <li>
          <a href="elaq124480s.html#328">Appendice IV</a>
        </li>
        <li>
          <a href="elaq124480s.html#330">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
