<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katerom10618701s.html#6">Catalogo degli oggetti
      ammessi alla Esposizione Romana del 1870 relativa all'arte
      cristiana e al culto cattolico nel chiostro di Santa Maria
      degli Angeli alle Terme Diocleziane [...]</a>
      <ul>
        <li>
          <a href="katerom10618701s.html#8">Avvertimento</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="katerom10618701s.html#10">[Catalogo]</a>
      <ul>
        <li>
          <a href="katerom10618701s.html#10">Portico dalla parte
          destra dell'ingresso verso il muro</a>
          <ul>
            <li>
              <a href="katerom10618701s.html#15">Giuseppe Nelli
              - Roma- Fac-simile della ›Colonna Traiana‹ [...]</a>
            </li>
            <li>
              <a href="katerom10618701s.html#16">Girolamo
              Rainaldi - Roma - Una colonna [...], copia di quella
              esistente nella Piazza di Santa Maria Maggiore
              ›Colonna della Basilica di Massenzio‹ [...]</a>
            </li>
            <li>
              <a href="katerom10618701s.html#17">Eugenio
              Rubicondi, Musaicista - Roma - Quadro [...]
              rappresentante il ›Colosseo‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#33">Augusto
              Ciccognani, Pittore - Roma - Quadro rappresentante la
              facciata della Chiesa di ›Santa Francesca Romana‹ al
              ›Foro Romano‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#33">Alessandro
              Faure, Pittore - Roma - Quadro rappresentante
              l'interno della Chiesa di ›San Lorenzo fuori le Mura‹
              prima del restauro</a>
            </li>
            <li>
              <a href="katerom10618701s.html#35">Benilde
              Rossignani - Roma - Piccolo quadro rappresentante una
              parte del Chiostro della Certosa di Roma ›Terme di
              Diocleziano‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#43">Adriano Trois
              Musaicista - Roma - Quadro ovale [...] rappresentante
              la Basilica ›San Pietro in Vaticano‹ e la ›Piazza San
              Pietro‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#44">Vincenzo
              Giovannini Pittore - Roma - Quadro moderno
              rappresentante la solenne Processione del Corpus
              Domini nella ›Piazza San Pietro‹ in Vaticano</a>
            </li>
            <li>
              <a href="katerom10618701s.html#44">Isabella
              Barberi - Roma - Quadro moderno rappresentante la
              piazza ›Piazza San Pietro‹ e la Basilica di ›San
              Pietro in Vaticano‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#44">Cesare Detti
              Pittore - Roma - Quadro esprimente la Santità di
              Nostro Signore Papa Pio IX che passeggia al monte
              ›Pincio‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#44">Carolina vedova
              De Paris - Roma - Quadro rappresentante la
              Proclamazione del Dogma dell'Immacolata Concezione
              nella Basilica Vaticana ›San Pietro in Vaticano‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#45">Enrico
              Verzaschi - Roma - Fotografia rappresentante
              l'interno della ›Cappella Sistina‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#52">Le Esposte
              della città di Faenza - Faenza - Ricamo in seta
              rappresentante la facciata della Basilica Vaticana
              ›San Pietro in Vaticano‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#61">Virginia
              Valentina Nobili - Roma - Tavola rotonda [...] colla
              ›Piazza San Pietro‹ e Chiesa di ›San Pietro in
              Vaticano‹ in mezzo [...]</a>
            </li>
            <li>
              <a href="katerom10618701s.html#63">Modello in
              marmo rappresentante la facciata e la cupola della
              Basilica Vaticana ›San Pietro in Vaticano‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katerom10618701s.html#64">Salone N. 1.</a>
          <ul>
            <li>
              <a href="katerom10618701s.html#64">Augusto Lazzè -
              Roma - Dipinto antico [...] rappresentante l'interno
              della Basilica Vaticana ›San Pietro in Vaticano‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#64">Conte Virginio
              Vespignani - Roma - Bozzetto delle pitture eseguite
              nell'arco della Basilica di ›Santa Maria in
              Trastevere‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#66">Carlo
              Quaedvlieg Pittore - Roma - Piccolo quadro moderno
              rappresentante la Benedizione dei Cavalli nella
              ›Piazza di Santa Maria Maggiore‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#66">Ciro Marini -
              Roma - Fac-simile [...] del Monumento della
              Immacolata Concezione ›Colonna dell'Immacolata
              Concezione‹ esistente in ›Piazza di Spagna‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#66">Silverio
              Capparoni - Roma - Bozzetto [...] rappresentante la
              facciata della Chiesa di ›San Lorenzo fuori le
              Mura‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#66">Id [i.e.
              Silverio Capparoni]. Bozzetto [...] della facciata
              della Chiesa di ›Santa Maria in Trastevere‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#66">Luigi Bazzani -
              Roma - Bozzetto all'acquarello rappresentante la
              decorazione della parete di fronte della Chiesa di
              ›San Lorenzo fuori le Mura‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#67">Alessandro
              Palombi Pittore - Roma - Quadro rappresentante la
              Sala maggiore della ›Biblioteca Apostolica
              Vaticana‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#67">Augusto Lazzè -
              Roma - Dipinto antico [...] rappresentante l'interno
              della ›Cappella Paolina (Città del Vaticano)‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katerom10618701s.html#68">Sala N. 2.</a>
          <ul>
            <li>
              <a href="katerom10618701s.html#73">Varii volumi
              legati contenenti incisioni rappresentanti [...]
              Cripta della Basilica Liberiana ›Santa Maria
              Maggiore‹; Ornati della ›Cappella Sistina‹ [...]</a>
            </li>
            <li>
              <a href="katerom10618701s.html#74">Paolo
              Cacchiatelli - Roma - L'interno della Chiesa di ›San
              Pietro in Vaticano‹ [...]</a>
            </li>
            <li>
              <a href="katerom10618701s.html#75">Sepolcro del
              Cardinale Guglielmo Fieschi nella Basilica di ›San
              Lorenzo fuori le Mura‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#75">Pavimento della
              Basilica Liberiana ›Santa Maria Maggiore‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#75">Abside a
              musaico esistente nella Chiesa di Santa Maria Nuova
              ›Santa Francesca Romana‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#75">Musaico
              dell'Arco esistente nella Basilica Costantiniana di
              ›San Lorenzo fuori le Mura‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#76">Pianta del
              Cimitero di Callisto ›Catacombe di San Callisto‹
              [...]</a>
            </li>
            <li>
              <a href="katerom10618701s.html#76">Cattedra
              Pontificale e Sedile del Presbiterio esistente nella
              Basilica di ›San Lorenzo fuori le Mura‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#76">Agostino
              Valentini - Roma - Due volumi in foglio contenenti
              l'illustrazione della Basilica Vaticana ›San Pietro
              in Vaticano‹ [...]</a>
            </li>
            <li>
              <a href="katerom10618701s.html#77">Giovanni
              Inglesi - Roma - Quadro [...] rappresentante la
              ›Piazza San Pietro‹ ed altri Monumenti antichi di
              Roma</a>
            </li>
            <li>
              <a href="katerom10618701s.html#77">Giacomo Fontana
              architetto, incisore - Roma - Cornice contenente due
              disegni [...] rappresentanti le Basiliche di ›San
              Pietro in Vaticano‹ e di ›San Paolo fuori le
              Mura‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#78">Idem [i.e.
              Giacomo Fontana]. Cornice contenente due disegni
              [...] che rappresentano uno l'interno della Chiesa di
              ›San Lorenzo fuori le Mura‹, l'altro un lato della
              medesima Chiesa</a>
            </li>
            <li>
              <a href="katerom10618701s.html#78">Idem [i.e.
              Giacomo Fontana]. Altra cornice contenente tre
              disegni [...] rappresentanti uno il Battisterio [sic]
              della Chiesa di ›Santa Maria Maggiore‹, il secondo
              ›San Giovanni in Laterano‹ [...]</a>
            </li>
            <li>
              <a href="katerom10618701s.html#78">Idem [i.e.
              Giacomo Fontana]. Cornice che contiene quattro
              disegni [...] rappresentanti la nuova Cripta della
              Basilica Liberiana ›Santa Maria Maggiore‹; l'interno
              di detta Basilica; il sotterraneo della Chiesa di
              ›San Martino ai Monti‹, e l'interno della Nave
              principale della Basilica Vaticana ›San Pietro in
              Vaticano‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#80">Rinaldo Werner,
              pittore - Roma - Pittura [...] rappresentante
              l'Arciconfraternita dei Sacconi nel ›Colosseo‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#81">Vincenzo
              Marchi, pittore - Roma - Quadro [...] rappresentante
              l'interno della Cappella del Santissimo Sacramento
              ›Cappella Sistina (Santa Maria Maggiore)‹ nella
              Chiesa di ›Santa Maria Maggiore‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#81">Michele Wittmer
              pittore bavarese - Roma - Disegno [...]
              rappresentante la Benedizione [...] nella ›Piazza del
              Popolo‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#81">Vincenzo Marchi
              pittore - Roma - Piccolo quadro [...] rappresentante
              la Cappella di San Bernardino esistente nella Chiesa
              di ›Santa Maria in Aracoeli‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#81">Lorenzo Bandoni
              - Roma - Dipinto [...] rappresentante l'interno della
              Chiesa di ›San Paolo fuori le Mura‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#82">Rinaldo Werner
              pittore - Roma - Pittura [...] rappresentante la
              facciata laterale della Chiesa di ›Santa Maria in
              Aracoeli‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#82">Bianchi Filippo
              - Roma - Incisione [...] rappresentante l'interno
              della Basilica Ostiense ›San Paolo fuori le Mura‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katerom10618701s.html#82">Salone N. 3.
          Accademia di San Luca</a>
          <ul>
            <li>
              <a href="katerom10618701s.html#82">Giovanni
              Montiroli Architetto - Roma - Progetto della facciata
              della Chiesa di ›Santa Maria degli Angeli‹ alle
              ›Terme di Diocleziano‹ [...]</a>
            </li>
            <li>
              <a href="katerom10618701s.html#82">Commendator
              Antonio Sarti Architetto - Roma - Scenografia [...]
              rappresentante l'interno della Basilica di ›Santa
              Maria Maggiore‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#82">Idem [i.e.
              Commendator Antonio Sarti Architetto - Roma -
              Scenografia]. della Basilica di ›San Giovanni in
              Laterano‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#83">Calcografia
              Camerale - Roma - Due incisioni rappresentanti le
              vedute interne ed esterne della Basilica di ›San
              Lorenzo fuori le Mura‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#84">Idem [i.e.
              Conte Virginio Vespignani]. Bozzetto [...]
              rappresentante la fronte dell'arco maggiore della
              Basilica di ›San Lorenzo fuori le Mura‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#85">Conte Virginio
              Vespignani _ Roma - Modello [...] del Monumento
              innalzato sulla ›Via Nomentana‹ a Sant'Alessandro e
              Sant'Agnese ›Porta Pia‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#86">Cavalier Andrea
              Busiri Architetto - Roma - Due piante una della
              antica Basilica Lateranense ›San Giovanni in
              Laterano‹ [...]; l'altra l'ampliazione della stessa
              Basilica [...]</a>
            </li>
            <li>
              <a href="katerom10618701s.html#88">L'interno della
              Chiesa di ›Sant'Andrea della Valle‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#88">Idem [i.e.
              Annibale Angelini]. L'esterno della Chiesa di ›San
              Giovanni dei Fiorentini‹ veduto dalla Longara ›Via
              della Lungara‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katerom10618701s.html#88">Salone N. 4.
          Accademia di San Luca</a>
        </li>
        <li>
          <a href="katerom10618701s.html#92">Salone N. 5.
          Monsignor Teodoli Presidente dello Studio di Musaico al
          Vaticano. Roma.</a>
          <ul>
            <li>
              <a href="katerom10618701s.html#92">Quadri in
              Musaico rappresentanti</a>
              <ul>
                <li>
                  <a href="katerom10618701s.html#92">Saturnino
                  Quintiliani Musaicista - Roma - Piccolo quadro
                  [...] rappresentante il prospetto della Basilica
                  di ›San Pietro in Vaticano‹ e dell'annessa piazza
                  ›Piazza San Pietro‹</a>
                </li>
                <li>
                  <a href="katerom10618701s.html#93">Augusto
                  Moglia - Roma - Piccola tavola rotonda [...] con
                  la ›Piazza San Pietro‹ nel mezzo ed otto vedute
                  di Roma all'intorno</a>
                </li>
                <li>
                  <a href="katerom10618701s.html#93">Ferdinando
                  Civilotti - Roma - Quadro [...] rappresentante
                  l'interno della Basilica Ostiense ›San Paolo
                  fuori le Mura‹</a>
                </li>
                <li>
                  <a href="katerom10618701s.html#94">Angusto
                  [sic] Jolibois - Roma - Piccolo quadro [...]
                  rappresentante il ›Pantheon‹</a>
                </li>
                <li>
                  <a href="katerom10618701s.html#94">Cavalier
                  Luigi Moglia, musaicista - Roma - sei quadri
                  [...] rappresentanti La ›Piazza San Pietro‹
                  [...]</a>
                </li>
                <li>
                  <a href="katerom10618701s.html#95">Costantino
                  Rinaldi, musaicista - Tre piccoli quadri [...]
                  rappresentanti La piazza del Vaticano ›Piazza San
                  Pietro‹ [...]</a>
                </li>
                <li>
                  <a href="katerom10618701s.html#101">Cavalier
                  Giuseppe Bianchi, Incisore - Roma - [...] una
                  [medaglia] dorata rappresentante [...] nel
                  rovescio l'interno della Basilica Vaticana ›San
                  Pietro in Vaticano‹ [...]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katerom10618701s.html#101">Reverendo
              Capitolo Liberiano</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katerom10618701s.html#102">Salone N. 6.</a>
          <ul>
            <li>
              <a href="katerom10618701s.html#102">Santissima
              Basilica Lateranense</a>
            </li>
            <li>
              <a href="katerom10618701s.html#103">Reverendo
              Capitolo Vaticano</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katerom10618701s.html#104">Sala ove sono
          esposti gli oggetti mandati da Sua Santità Papa Pio
          IX.</a>
        </li>
        <li>
          <a href="katerom10618701s.html#107">Salone N. 7.</a>
        </li>
        <li>
          <a href="katerom10618701s.html#126">Salone N. 8.</a>
        </li>
        <li>
          <a href="katerom10618701s.html#136">Salone N. 9.</a>
          <ul>
            <li>
              <a href="katerom10618701s.html#140">Luigi De
              Belardini - Roma - Fotografia [...] rappresentante
              l'interno della Basilica Ostiense ›San Paolo fuori le
              Mura‹</a>
            </li>
            <li>
              <a href="katerom10618701s.html#140">Principe Don
              Alessandro Torlonia - Roma - Modello [...]
              rappresentante la Basilica di ›San Pietro in
              Vaticano‹ [...], piazza e colonnato ›Piazza San
              Pietro‹ [...]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katerom10618701s.html#142">Sezione
          Francese</a>
          <ul>
            <li>
              <a href="katerom10618701s.html#142">Salone Numero
              10</a>
            </li>
            <li>
              <a href="katerom10618701s.html#143">Salone N.
              11.</a>
            </li>
            <li>
              <a href="katerom10618701s.html#145">Salone N.
              12.</a>
            </li>
            <li>
              <a href="katerom10618701s.html#146">Salone N.
              13.</a>
            </li>
            <li>
              <a href="katerom10618701s.html#147">Salone N.
              14.</a>
            </li>
            <li>
              <a href="katerom10618701s.html#148">Salone N.
              15.</a>
            </li>
            <li>
              <a href="katerom10618701s.html#149">Salone N.
              16.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katerom10618701s.html#150">Lato del
          quadriportico riservato agli Esponenti Francesi</a>
        </li>
        <li>
          <a href="katerom10618701s.html#153">Statue esistenti
          fuori la porta d'ingresso</a>
        </li>
        <li>
          <a href="katerom10618701s.html#153">Statua esistente
          fuori la porta dell'egresso</a>
        </li>
        <li>
          <a href="katerom10618701s.html#153">Altare esistente
          nel Giardino</a>
        </li>
        <li>
          <a href="katerom10618701s.html#154">Commissione
          direttiva eletta [...] per la Esposizione Romana di ogni
          opera d'arte da servire al culto cattolico</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
