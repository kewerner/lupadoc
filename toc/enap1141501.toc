<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="enap1141501s.html#6">Napoli antica e moderna;
      parte prima</a>
      <ul>
        <li>
          <a href="enap1141501s.html#8">Alla Maestà di
          Ferdinando IV.</a>
        </li>
        <li>
          <a href="enap1141501s.html#12">Napoli antica</a>
        </li>
        <li>
          <a href="enap1141501s.html#14">I. Origine, e
          situazione di Palepoli, e di Napoli colle loro mura, e
          sepolcro della Sirena</a>
        </li>
        <li>
          <a href="enap1141501s.html#39">II. Divisione di Napoli
          in Fratrie, o adunanze sacre, e loro siti</a>
        </li>
        <li>
          <a href="enap1141501s.html#58">III. Antico Porto</a>
        </li>
        <li>
          <a href="enap1141501s.html#64">IV. Regioni, Porte, ed
          antiche Vie di Napoli</a>
        </li>
        <li>
          <a href="enap1141501s.html#79">V. Temj, e
          Basiliche</a>
        </li>
        <li>
          <a href="enap1141501s.html#108">VI. Teatro</a>
        </li>
        <li>
          <a href="enap1141501s.html#111">VII. Ginnasio, Giuochi
          pubblici, e Terme</a>
        </li>
        <li>
          <a href="enap1141501s.html#121">VIII. Collegj
          d'Arti</a>
        </li>
        <li>
          <a href="enap1141501s.html#136">IX. Catacombe, e
          Sepolcreto pubblico</a>
        </li>
        <li>
          <a href="enap1141501s.html#151">X. Grotta Puteolana, e
          Sepolcro di Virgilio</a>
        </li>
        <li>
          <a href="enap1141501s.html#159">XI. Corso del fiume
          Sebeto</a>
        </li>
        <li>
          <a href="enap1141501s.html#165">XII. Antico
          acquidotto, e descrizione delle altre acque, che furono
          intromesse in Napoli</a>
        </li>
        <li>
          <a href="enap1141501s.html#178">XIII. Monte
          Vesuvio</a>
        </li>
        <li>
          <a href="enap1141501s.html#196">Indice de'
          Capitoli</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
