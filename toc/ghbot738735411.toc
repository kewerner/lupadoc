<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbot738735411s.html#6">Raccolta Di Lettere Sulla
      Pittura Scultura Ed Architettura; Tomo I.</a>
      <ul>
        <li>
          <a href="ghbot738735411s.html#8">All'Eminentissimo e
          Reverendissimo Principe il Signor Cardinale Silvio
          Valenti</a>
        </li>
        <li>
          <a href="ghbot738735411s.html#12">Al cortese
          lettore</a>
        </li>
        <li>
          <a href="ghbot738735411s.html#14">Lettere su la
          pittura, scultura ed architettura</a>
        </li>
        <li>
          <a href="ghbot738735411s.html#340">Indice de' nomi
          degli Autori di queste Lettere</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
