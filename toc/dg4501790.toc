<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501790s.html#6">Le Cose Maravigliose Dell’Alma
      Città Di Roma</a>
      <ul>
        <li>
          <a href="dg4501790s.html#8">Le sette chiese
          principali</a>
        </li>
        <li>
          <a href="dg4501790s.html#17">In Trastevere</a>
        </li>
        <li>
          <a href="dg4501790s.html#19">Nel Borgo</a>
        </li>
        <li>
          <a href="dg4501790s.html#21">Della Porta Flaminia fuori
          del Popolo fino alle radici del Campidoglio</a>
        </li>
        <li>
          <a href="dg4501790s.html#30">Del Campidoglio a man
          sinistra verso li monti</a>
        </li>
        <li>
          <a href="dg4501790s.html#35">Dal Campidoglio a man
          dritta verso li Monti</a>
        </li>
        <li>
          <a href="dg4501790s.html#40">Tavola delle chiese</a>
        </li>
        <li>
          <a href="dg4501790s.html#42">Le stationi, che sono nelle
          chiese di Roma, si per la Quadragesima, come per tutto
          l'anno</a>
        </li>
        <li>
          <a href="dg4501790s.html#49">Trattato over modo
          d'acquistar l'indulgentie alle Stationi</a>
        </li>
        <li>
          <a href="dg4501790s.html#53">La guida romana per li
          forastieri che vengono per vedere le Antichità di Roma, a
          una per una in bellissima forma, e brevità</a>
        </li>
        <li>
          <a href="dg4501790s.html#63">Summi Pontifices</a>
        </li>
        <li>
          <a href="dg4501790s.html#80">Reges et Imperatores
          romani&#160;</a>
        </li>
        <li>
          <a href="dg4501790s.html#84">Li Re di Francia</a>
        </li>
        <li>
          <a href="dg4501790s.html#85">Li Re del regno di Napoli e
          di Sicilia</a>
        </li>
        <li>
          <a href="dg4501790s.html#86">Li Dugi di Venegia</a>
        </li>
        <li>
          <a href="dg4501790s.html#88">Li Duchi di Milano</a>
        </li>
        <li>
          <a href="dg4501790s.html#90">L'Antichità di Roma di M.
          Andrea Palladio</a>
          <ul>
            <li>
              <a href="dg4501790s.html#91">Alli lettori</a>
            </li>
            <li>
              <a href="dg4501790s.html#92">Delle antichità della
              città di Roma</a>
            </li>
            <li>
              <a href="dg4501790s.html#126">De i fuochi de gli
              antichi</a>
            </li>
            <li>
              <a href="dg4501790s.html#129">Tavola delle Antighita
              della città di Roma</a>
            </li>
            <li>
              <a href="dg4501790s.html#131">Poste de Italia</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
