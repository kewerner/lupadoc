<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="fa1703810c3s.html#8">Voyage pittoresque ou
      Description des Royaumes de Naples et de Sicile troisième
      Volume</a>
      <ul>
        <li>
          <a href="fa1703810c3s.html#10">Avant-Propos</a>
        </li>
        <li>
          <a href="fa1703810c3s.html#14">Discours préliminaire
          ou Introduction au Voyage et à la Description de la
          Grande-Grèce</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#21">De l'Apulie, nommée
              aujourd'hui la Puglia</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#25">De l'Yapigie ou
              Messapie, maintenant Terre d'Otrante</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#30">De la Lucanie ou
              Province de la Basilicate</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#39">De l'ancien
              Brutium, aujourd'hui la Calabre citérieure et
              ultérieure</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#55">Carte de l'Italie
              Méridionale et de la Sicile Ancienne ◉</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#58">Unbenannt</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#58">I. Route de Naples à
          Siponto, par Bénévent, Lucera, Manfredonia, Monte
          Sant-Angelo, etc.</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#62">Vue de l'Arc de
              Trajan, à Bénévent. Planche première</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#64">1. Vue de l'Arc de
              Trajan à Bénévent 2. Restes de l'ancien Amphithéâtre
              de Bénévent ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#66">Ruines de
              l'Amphithéâtre de Bénévent. Planche deuxième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#69">Vues de la Fontaine
              de Sainte-Sophie d'une Porte antique de Bénévent.
              Planches trois et quatre</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#70">3. Vue de la
              Fontaine de Sainte-Sophie à Bénévent 4. Vue d'un
              ancienne Porte de Bénévent ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#74">Vue du Château de
              Lucera dans la Pouille. Planche cinquième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#76">5. Vue d'un vieux
              Château, bâti près de Lucera dans la Pouille par
              l'Empereur Frederic II. vers l'Année 1240 6. Vue de
              l'Entrée des Carriers et des Rochers qui terminent le
              Mont Gargano près de Manfredonia, dans la Pouille
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#80">Vue du Cap ou
              Promontoire appellée Monte Gargano. Planche
              sixième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#80">Vue de l'Église de
              Siponto et d'une Chapelle souterreine construite dans
              le même Lieu, de Débris antiques. Planches sept et
              huit</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#82">7. Vue extérieure
              d'une Église de Capucina à Siponto construite de
              Débris antiques et dans le même Lieu ou était
              l'ancienne Sipuntum 8. Vue intérieure d'une Chapelle
              souterreine à Siponto ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#84">Vue du Monte
              Sant-Angelo, prise de l'Entrée de l'Église et le Jour
              de la Fête du Saint. Planche neuvième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#86">9. Vue de l'Église
              de la Madona di Santa Croce di Barletta 10. Vue de
              Monte Sant-Angelo prise de l'Entrée de l'Église et le
              Jour de la Fête du Saint ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#88">Vue de l'Église de
              la Madone di Santa-Croce di Barletta. Planche
              dixième</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#90">II. Route depuis Cannes
          jusqu'à Polignano, en passant par Canosa, Trani,
          Bisceglia, Bari, Mola, et l'Abbaye de San Vito</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#97">Vue de la Ville de
              Canosa, et de quelques Tombeaux ou Monumens antiques,
              Arc de Terentius Varro. Planches douze et treize</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#98">12. Vue des Restes
              de l'antique Ville et du Château de Cannes 13. Vue de
              Canosa Ville de la Pouille ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#102">Ruines antiques
              dans les Environs de Canose. Planche quatorzième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#103">Vue de l'Entrée de
              la Chapelle ou est renfermé le Tombeau de Boemond.
              Planche quinzième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#104">14. Débis de
              Constructions antiques, situées près de Canosa dans
              la Pouille 15. Vue de l'Entrée d'une Église appellée
              la Chiesa Madre, près de Canosa ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#107">Vue de l'Église
              principale, et de la Place publique de Trani. Planche
              seizième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#108">16. Vue de
              l'Église principale et de la Place publique de Trani
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#111">Vue de l'Arrivée
              de Bisceglia. Planche dix-septième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#112">17. Vue extérieure
              de Bisceglia, dans la Pouille 18. Vue de Giovenazzo
              petite Ville de la Pouille ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#115">Vue du Village de
              Giovenazzo. Planche dix-huitième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#116">Vue de la Ville et
              du Port de Bari. Planches dix-neuf et vingt</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#117">Vue du Village de
              Mola dans la Terre de Bari. Planche vingt-unième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#118">19. Vue de la
              Ville et du Port de Bari 20. Vue de l'Entrée et d'une
              des Portes de la Ville de Bari ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#121">20. Vue intérieure
              de l'Abbaye de San Vito di Polignano 21. Vue du
              Village de Mola situé sur le bord de la Mer ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#122">Vue de l'Abbaye de
              San-Vito di Polignano. Planche vingt-deuxième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#123">Vues de
              l'Intérieur et de l'Extérieur des Grottes de
              Polignano. Planches vingt-trois et vingt-quatre</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#124">22. Vue extérieure
              d'une Grotte rustique, et formée par la Nature 23.
              Vue intérieure de la même Grotte, appellée dans le
              Pays Grotta di Palazzo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#128">III. Terre d'Otrante.
          Route de Polignano jusqu'à Gallipoli, en passant par
          Brindes, Squinzano, Lecce, Soletta et Otrante</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#131">Carte de la
              Seconde Partie du Royaume de Naples, contenant la
              Capitale, la Pouille, la Terre de Bari et la Terre
              d'Otrante ◉</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#135">Vues de la Ville
              et du Château de Brindes. Planches vingt-six et
              vingt-sept</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#136">26. Vue de la
              Ville de Brindes ou Brendisi anciennement Brundusium
              27. Vue du Château de Brindes et d'une Partie de son
              Port ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#141">28. Vue du Cloître
              des Dominicains de Lecce dans la Terre d'Otrante 29.
              Vue du Village de Squinzano situé entre Brindisi et
              Lecce ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#142">Vues du Village de
              Squinzano et du Cloître des Dominicains de Lecce.
              Planches vingt-huit et vingt-neuf</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#145">30. Vue du
              Campanille de Soletta, Village situé dans la Terre
              d'Otrantes 31. Vue du Bourg ou Village de Moglié dans
              la Terre d'Otrantes ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#146">Vue du Village et
              du Campanille de Soletta dans la Terre d'Otrante.
              Planche trentième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#148">Vue du Village de
              Moglié, dans la Terre dÄOtrante, l'ancienne Yapigia.
              Planche trente-unième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#149">Vue de la Ville et
              du Port d'Otrante. Planche trente-deuxième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#150">32. Vue du Port et
              de l'Entrée de la Ville d'Otrantes, anciennement
              appellée Hydruntum ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#155">33. Vue du Port et
              de la Ville de Gallipoli, située sur le Golfe de
              Tarente 34. Grotte anciennement taillée dans les
              Rochers, près de l'antique Ville de Mandurium dans la
              Grande-Grèce ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#156">Vue de la Ville et
              du Port de Gallipoli. Planche trente-troisième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#160">Vue de la Fontaine
              de Pline. Planche trente-quatrième</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#162">IV. Province de la
          Basilicate, ou l'ancienne Lucanie. Route de Tarente
          jusqu'à Héraclée, en passant par les Ruines de Métaponte,
          Bernaldo, Anglone et Policoro</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#163">Vue de la Ville de
              Tarente, prise du côté de la Grande Mer, et de la
              Partie appellée Mare Piccolo. Planches trente-cinq et
              trente-six</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#164">35. Première Vue
              de la Ville et du Golfe de Tarente 36. Seconde Vue du
              Port de Tarente, prise du côté du Marché aux Poissons
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#173">37. Vue des Ruines
              du Temple de Junon, à Metapontum ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#174">Vue des Ruines du
              Temple de Métaponte. Planche trente-septième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#175">Vue latérale du
              même Temple de Métaponte. Planche trente-huitième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#176">38. Vue de la
              petite Ville de Bernaldo, située dans la Province de
              la Basilicata, près de Metaponte 39. Vue latérale du
              Temple de Metaponte ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#178">Vue de la petite
              Ville de Bernaldo, près des Ruines de Métaponte.
              Planche trente-neuvième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#181">40. Vue de Torre
              di Policoro, sur le Golfe de Tarente 41. Vue des
              Marais formés par les Eaux de la Mer dan le Lieu où
              l'on pense que devoit être située l'ancien Port de
              Metaponte ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#182">Vue de l'ancien
              Port de Métaponte. Planche quarantième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#182">Vue du Château de
              Policoro. Planche quarante-unième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#187">42. Vue prise dans
              les Environs et près du Lieu ou l'on pense qu'était
              autrefois située l'antique Ville d'Heraclea, dans la
              Grande-Grèce ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#189">43. Vue des
              Appenins et d'une Vallée de la Basilicate, l'ancienne
              Lucanie ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#190">Vue du Site et de
              l'Emplacement que l'on croit avoir été occupé
              autrefois par l'antique Héraclée. Planches
              quarante-deuxième et quarante-troisième</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#192">V. Calabre citérieure.
          Route de Policoro jusqu'à Corigliano, près du Lieu où
          était située l'antique Sybaris, en passant par Rocca
          Imperiale, Castel Rosetto et Casal Nuovo</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#193">Vue de la Rocca
              Imperiale. Planche quarante-quatrième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#194">44. Vue de la
              Rocca Imperiale petite Ville bâtie sur un Rocher aux
              confins de la Basilicate et de la Calabre citérieure
              45. Vue de Castel-Rosetto situé dans la Calabre
              citérieure ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#196">Vue de
              Castel-Rozetto. Planche quarante-cinquième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#197">Vue d'un Pont
              rustique sur le Fleuve Sybaris. Planche
              quarante-sixième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#198">46. Passage du
              Crati, principal Fleuve de la Calabre citérieure 47.
              Pont-Rustique construit sur la petite Rivière de
              Sybaris ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#200">Passage du Fleuve
              Crati. Planche quarante-septième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#201">Vue générale du
              Bourg de Corigliano. Planche quarante-huitième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#201">Vues de
              Corigliano, prises sous divers Aspects. Planches
              quarante-neuvième et cinquantième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#202">48. Vue générale
              de la petite Ville de Corigliano ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#205">49. Vue de
              Corigliano prise du milieu de la Montagne 50. Vue
              prise sur les hauteurs de Corigliano au sortir de la
              Ville, du côté de la Plaine de Sybaris ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#207">Vues de l'Aqueduc
              de Corigliano, et d'une Fabrique de Réglisse.
              Planches cinquante-unième et cinquante-deuxième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#208">51. Vue de
              l'Acqueduc de Corigliano en Calabre 52. Vue d'une
              Fabrique de Réglisse à Corigliano ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#211">53. Vue du Cours
              du Crati et de la Vallée délicieuse où était située
              l'antique Ville de Sybaris ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#212">Vue de la Plaine
              où était située l'antique Sybaris. Planche
              cinquante-troisième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#217">54. Carte
              géographique et numismatique de la troisième et
              quatrième Partie du Royaume de Naples: Contenant la
              Basilicate, ou l'ancienne Lucanie, avec la Calabre
              citérieure et ultérieure, ou le Brutium des Anciens
              ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#218">VI. Calabre
          citérieure. Route de Corigliano jusqu'à Squillace,
          l'antique Scyllatium, en passant par Melissa, Strongoli,
          Cotorne, Capo delle Colonne, et Catanzaro</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#221">55. Vue de la Tour
              ou Château de Melissa en Calabre appartenant au
              Prince de Strongoli 56. Vue de la Ville de Strongoli
              bâtie sur les Ruines de Petilia ancienne Ville de
              Brutium ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#222">Vue de la Tour de
              Mélissa. Planche cinquante-cinquième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#222">Vue de Strongoli,
              l'ancienne Pétilie. Planche cinquante-sixième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#225">Vue de la Ville de
              Cotrone. Planche cinquante-septième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#226">57. Vue de la
              Ville modèrne de Cotrone élevée près des Ruines de
              l'antique et célébre Crotone 58. Vue prise à
              l'extérieure du Cap ou Promontoire appellé
              aujourd'hui Capo delle Colonne au lieu où était
              autrefois le fameux Temple de Junon Lacinienne ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#228">Vue du Cap
              Colonna. Planche cinquante-huitième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#231">59. Vue de la
              petite Ville d'Isola située dans la Calabre
              ultérieure 60. Vue de Catanzaro Ville capitale de la
              Calabre ultérieure ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#232">Vue du Village
              d'Isola. Planche cinquante-neuvième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#233">Vue de la Ville de
              Catanzaro, Capitale de la Calabre. Planche
              soixantième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#235">Vue de la
              Rochetta. Planche soixante-unième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#236">61. Vue du Golphe
              de Squilace et des Ruines de l'antique Scylatium au
              Lieu nommé aujourd'hui la Rochetta dans la Calabre
              ultérieure 62. Vue de la petite Ville de Squilace
              dans la Calabre ultérieure ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#238">Vue de Squillace,
              située près des Ruines de l'antique Scyllatium.
              Planche soixante-deuxième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#241">Descrizione del
              Tremuoto in Messina e nella Calabria di 5 Febraio
              1783</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#244">VII. Calabre
          ultérieure. Route de Squillace, l'antique Scyllatium,
          jusqu'à Reggio, en passant par la Rocella, Gerace, les
          Ruines de Locres, Condoyane, etc.</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#247">63. Vue du Bourg
              de La Roccella situé dans la Calabre ultérieure ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#248">Vue du Bourg de la
              Rocella. Planche soixante-troisième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#249">Vue de Gerace,
              près de l'ancienne Ville de Locres. Planche
              soixante-quatrième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#250">64. Vue de la
              Ville de Gerace dans la Calabre ultérieure près de
              l'ancienne Ville de Locres 65. Vue de la Tour de
              Pagliapoli, et du Golphe ou était située l'ancienne
              Ville des Locriens Epizephiriens ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#252">Vue de la Tour de
              Pagliapoli et des Ruines de Locres. Planche
              soixante-cinquième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#255">Vue de la Ville de
              Condoyane, en Calabre. Planche soixante-sixième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#256">66. Vue prise dans
              les Apennins et aux pieds des Rochers escarpés
              lesquels est située la petite Ville de Condoyane à
              l'extérieure de la Calabre ultérieure ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#259">Vue des Rochers et
              de la Marine de Bova. Planche soixante-septième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#260">67. Vue des
              Rochers et de la Marine de Bova près le Cap
              Spartivento ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#263">Vue du Passge du
              Torrent ou Fleuve Alice, à l'éxtremité de la Chaîne
              des Apennins. Planche soixante-huitième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#264">68. Passage du
              Fleuve Alice dans les Montagnes qui teminent
              l'Appenin entre Punta della Saetta et le Cap de
              Spartivento 69. Vue du Pahre ou Détroit de Messine
              prise du côté de la Calabre et en arrivant à Reggio
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#267">Vue de l'Arrivée
              de Reggio et du Détroit qui sépare l'Italie d'avec la
              Sicile; avec quelques Costumes Calabrois. Planches
              soixante-neuvième et soixante-dixième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#268">70. Vue prise dans
              les Environs de Reggio 71. Vue de la Ville et du Port
              de Reggio avec une Partie des Côtes de la Sicile et
              de l'Etna que l'ou apperçoit de l'autre côté du
              Détroit ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#270">Vue du Port de
              Reggio. Planche soixante-onzième</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#296">VIII. Suite du Voyage
          de la Calabre. Route depuis le Détroit de Messine
          jusqu'au Nerino, en passant par Tropea, Nicastro,
          Cosenza</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#297">Vues du Rocher et
              Écueil de Scylla, prises en traversant le Détroit de
              Messine. Planches soixante-douzième et
              soixante-treizième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#298">72. Vue du Rocher
              de Scylla et d'une Parte de la Coste de la Calabre
              ultérieure prise du Phare de Messine et en traversant
              le Détroit 73. Rochers et Écueils renommés de Scylla
              avec la Vue de la Ville, et du Château qui était
              élevé au dessus avant le tremblement de Terre ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#301">Vue de la Ville et
              du Port de Tropea. Planche soixante-quatorzième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#302">74. Vue de la
              Ville et du Château de Tropoea situé sur la Coste de
              la Calabre ultérieure ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#305">Vue de l'Hermitage
              de Tropea. Planche soixante-quinzième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#306">75. Vue du Château
              ou Hermitage de Tropoea, situé dans la Calabre
              ultérieure 76. Vue des Montagnes des Apennins, prise
              près d'un Torrent appellé Fiume di San Polito et en
              arrivant à la petite Ville de Nicastro située dans la
              Calabre ultérieure ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#311">77. Vue de la
              Ville de Nicastro située au milieu des Montagnes de
              l'Apennin dans la Calabre ultérieure ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#312">Vues de la Ville
              de Nicastro et des Montagnes de la Calabre qui
              l'environnent. Planches soixante-seizième et
              soixante-dix-septième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#315">Vue de Cosenza,
              Ville de la Calabre citérieure. Planche
              soixante-dix-huitième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#316">78. Vue des
              Environs de la Ville de Cosenza prise sur les bords
              du Crati, dans la Calabre citérieure ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#321">79. Vue du riche
              Vallon de Sybaris, prise de dessus les hauteurs de
              l'Apennin ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#322">Vallon de Sybaris,
              vu dans l'Éloignement de dessus les Montagnes de la
              Calabre, près de Castro Villari. Planche
              soixante-dix-neuvième</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#324">IX. Province de la
          Basilicate. Route depuis les Confins de la Calabre
          jusqu'à la Principauté de Salerne, en passant par
          Lago-Negro, la Polla et Paestum</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#325">Vue de Lago Negro,
              Ville située dans la Basilicate. Planche
              quatre-vingtième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#326">80. Vue de la
              petite Ville de Lago Negro, dans les Apennins sur les
              Confins de la Basilicate et de la Principauté de
              Salerne ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#331">Vue des Cascades
              de Fiume Negro, et du Moulin de la Pertosa, situé
              dans la Vallée de Diana dans la Basilicate. Planche
              quatre-vingt-unième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#332">81. Vue des
              Cascades de Fiume Negro et du Moulin de la Pertosa
              situé à l'Entrée de la Vallée de Diana dans la
              Principauté citérieure de Salernes 82. Petite Vue des
              Temples de Pestum prise en y arrivant du côté du
              Couchant ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#334">Vue générale des
              Temples de Pestum, prise en arrivant du côté du
              Couchant. Planche quatre-vingt-deuxième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#337">Vue générale et
              plus détaillée des trois Temples de Pestum, prise du
              côté du Levant. Planche quatre-vingt-troisième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#338">83. Vue générale
              des Temples de Pestum, situés sur le bord de la Mer
              et près du Golphe de Salernes ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#341">Plan et Elevation
              géometrale, avec les Details en grand, du Temple de
              Pestum ◉ ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#343">84. Vue du Temple
              Exastile Periptere de Pestum près de Salernes à 20
              Lieues de Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#344">Vue du petit
              Temple Exastile Périptere de Pestum. Planche
              quatre-vingt-quatrième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#345">Vues intérieures
              et extérieures du grand Temple Périptere de Pestum.
              Planches quatre-vingt-cinquième et
              quatre-vingt-sixième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#346">85. Coupe ou Vue
              intérieure du Temple Périptere Hypetre de Pestum
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#348">86. Vue du Temple
              Hipetre de Pestum à 18 ou 20 Lieues de Naples, situé
              sur le bord de la mer dans le Golphe de Salernes
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#351">Coupes, Plans et
              Détails des Temples de Pestum. Planches
              quatre-vingt-septième et quatre-vingt-huitième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#352">87./88. Plan,
              Elevation, et Détails du Temple Périptere et
              Pseudopériptere de Pestum ◉ ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#355">89. Vue de la
              Ville de Salerne, située au fond du Golphe de ce nom
              et au pied d'une Partie des Montagnes de l'Apennin,
              appellée Monte di Gragnano ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#356">Vue de la Ville de
              Salerne. Planche quatre-vingt-neuvième</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#358">X. Retour à Naples, en
          passant par Salerne, l'Abbaye de la Cava, Nocera dei
          Pagani, l'Isle de Caprée, Sorrente, Massa, et
          Castella-Mare</a>
          <ul>
            <li>
              <a href="fa1703810c3s.html#360">Vue de l'Intérieur
              de l'Église Cathédrale de Salerne. Planche
              quatre-vingt-dixième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#361">Vue de la Cour ou
              Portique de l'Église Cathédrale de Salerne. Planche
              quatre-vingt-onzième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#362">90. Vue intérieure
              de l'Église Cathédrale de Salerne 91. Vue d'un
              Peristile entouré de Colonnes et de Tombeaux antiques
              servant de Portique et d'Entrée à la Cathédrale de
              Salerne ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#365">Vue de l'Arrivée
              et de dehors du Bourg de la Cava, près Salerne.
              Planches quatre-vingt-treizième et
              quatre-vingt-quatorzième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#366">93. Vue du Bourg
              de la Cava près de Salerne 94. Site pittoresque et
              Sauvage de l'ancienne Abbaye de la Cava ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#369">Vue intérieure
              d'un Temple antique à Nocera, avec la Coupe et
              l'Élévation de ce Monument. Planches
              quatre-vingt-quinzième er quatre-vingt-seizième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#370">95. Vue intérieure
              d'un Temple antique formant aujourd'hui l'Église
              principale de Nocera di Pagani l'ancienne Ville de
              Nocera 92. Vue d'un ancienne Église de Salerne
              construite en Partie de Fragments et de Colonnes
              antiques, et abandonnée à cause de sa grande vétuste
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#372">96. Coupe et Plan
              d'un Temple Antique, converti en Église sous le Nom
              de Sainte-Marie Majeure à Nocera de Pagani entre
              Naples et Salernes ◉</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#375">Vues de l'Isle de
              Caprée, prises de dessus la Mer. Planches
              quatre-vingt-dix-septième et
              quatre-vingt-dix-huitième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#376">97. Première Vue
              de l'Isle de Caprée prise dans la Partie
              septentrionale de l'Isle ou est située la petite
              Ville ou Port de Capri, enface du Golphe et de la
              Ville de Naples 98. Seconde Vue de l'Isle de Caprée
              prise à l'Extrémité et dans la Partie la plus
              escarpée de l'Isle du côté du Détroit et près de la
              Pointe de la Coste de Sorrente appellée Punta della
              Campanella ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#385">99. Vue de la
              Coste de Sorrente prise sur le Golphe de Naples près
              du Lieu appellée Massa 100. Vue des Clochers
              volcaniques d'ou est formée en grande Partie la Coste
              de Sorrente, sur le bord de la Mer de Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#386">Vue prise le long
              de la Côte de Sorrente, près de Massa. Planche
              quatre-vingt-dix-neuvième</a>
            </li>
            <li>
              <a href="fa1703810c3s.html#387">Vue de Sorrente et
              d'une Partie de la Côte, prise de dessus Mer. Planche
              centième</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c3s.html#393">Fragment de la Carte
          Theodosienne, publiée à Venise en 1591 par Marc Velser,
          et connue sous le Nom de Carte de Peutinger ◉</a>
        </li>
        <li>
          <a href="fa1703810c3s.html#394">Carte Théodosienne,
          connue sous le Nom de Carte de Peutinger</a>
        </li>
        <li>
          <a href="fa1703810c3s.html#402">Table des Chapitres,
          avec les Noms des Planches et des Vues contenues dans ce
          troisième Volume</a>
        </li>
        <li>
          <a href="fa1703810c3s.html#406">Explication des
          Fleurons et Vignettes</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
