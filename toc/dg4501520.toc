<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501520s.html#4">[Titelblatt] COMPENDIO DI ROMA
      ANTICA. RACCOLTO E SCRITTO DA M. LU- cio Fauno con somma
      brevita,&amp;ordine con quanto gli Antichi o Mo- derni
      scritto ne hanno.</a>
    </li>
    <li>
      <a href="dg4501520s.html#6">[Imprimatur] IULIUS PAPA
      III.</a>
    </li>
    <li>
      <a href="dg4501520s.html#8">[Widmung I] A tergo.</a>
    </li>
    <li>
      <a href="dg4501520s.html#9">[Widmung II] 1551.die
      12.Decemb.In Rogatis.</a>
    </li>
    <li>
      <a href="dg4501520s.html#10">[Inhalt] TAVOLA DEL COMPENDIO
      di Roma Antica.</a>
    </li>
    <li>
      <a href="dg4501520s.html#12">NEL PRIMO LIBRO. TUTTI I LUOGHI
      ANTICHI di Roma, che in questi libri si contengo- no,
      brevißimamente raccolti</a>
      <ul>
        <li>
          <a href="dg4501520s.html#12">[Cap. I] DE LA CITTA DI
          ROMOLO E DE LE SUE PORTE.</a>
        </li>
        <li>
          <a href="dg4501520s.html#12">[Cap. II.] De le porte che
          sono in Roma, e de le strade, che ne escono.</a>
        </li>
        <li>
          <a href="dg4501520s.html#16">[Cap. III.] De le tre porte
          di Trasteuere,e de le sei di Vaticano.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501520s.html#17">NEL SECONDO LIBRO.</a>
      <ul>
        <li>
          <a href="dg4501520s.html#17">[Cap. I.] Del Campidoglio,e
          de'luoghi suoi.</a>
        </li>
        <li>
          <a href="dg4501520s.html#20">[Cap. II.] De´luoghi,che
          sono ne la valle,che è fra il Cam- pidoglio,e´l
          Palatino.</a>
        </li>
        <li>
          <a href="dg4501520s.html#21">[Cap. III.] De´luoghi del
          Foro Romano,e del Comitio.</a>
        </li>
        <li>
          <a href="dg4501520s.html#25">[Cap. IV.] Del colle
          Palatino,e de´luoghi suoi.</a>
        </li>
        <li>
          <a href="dg4501520s.html#27">[Cap. V.] Del Foro di
          Augusto,di Cesare,di Nerva, e di Traiano.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501520s.html#29">NEL TERZO LIBRO.</a>
      <ul>
        <li>
          <a href="dg4501520s.html#29">[Cap. I.] De l´Aventino,e
          de´luoghi suoi.</a>
        </li>
        <li>
          <a href="dg4501520s.html#30">[Cap. II.] Del Campo,dove è
          Testaccio.</a>
        </li>
        <li>
          <a href="dg4501520s.html#31">[Cap. III.] De´luoghi di
          quel piano,che è fra l´Aventino, il Tevere,il
          Campidoglio,e´l Palatino.</a>
        </li>
        <li>
          <a href="dg4501520s.html#35">[Cap. IV.] Del monte
          Celio,e de´luoghi suoi.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501520s.html#38">NEL QUARTO LIBRO.</a>
      <ul>
        <li>
          <a href="dg4501520s.html#38">[Cap. I.] De le Esquilie,e
          de´luoghi suoi.</a>
        </li>
        <li>
          <a href="dg4501520s.html#41">[Cap. II.] Del colle
          Viminale,e de´luoghi suoi.</a>
        </li>
        <li>
          <a href="dg4501520s.html#43">[Cap. III.] Del Quirinale
          chiamato hoggi Montecavallo, e de´luoghi suoi.</a>
        </li>
        <li>
          <a href="dg4501520s.html#46">[Cap. IV.] Del colle de gli
          Hortoli chiamato ancho Pinciano.</a>
        </li>
        <li>
          <a href="dg4501520s.html#46">[Cap. V.] Del Campo
          Martio,e de´luoghi de la città pia- na,che hora piu
          s´habita.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501520s.html#51">NEL QUINTO LIBRO.</a>
      <ul>
        <li>
          <a href="dg4501520s.html#51">[Cap. I.] Del Tevere,e
          de´ponti,che vi sono sopra, e de l´Isola.</a>
        </li>
        <li>
          <a href="dg4501520s.html#53">[Cap. II.] Di Trastevere
          chiamato da gli antichi Ianicolo.</a>
        </li>
        <li>
          <a href="dg4501520s.html#54">[Cap. III.] Di Vaticano,e
          de´luoghi suoi.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
