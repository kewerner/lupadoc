<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghgal40653670s.html#6">Trattato sopra gli errori
      degli architetti</a>
      <ul>
        <li>
          <a href="ghgal40653670s.html#8">Vita letteraria del
          celebre filosofo, medico, matemantico, e storico Teofilo
          Gallaccini Sanese</a>
        </li>
        <li>
          <a href="ghgal40653670s.html#16">Tavola dei
          Capitoli</a>
        </li>
        <li>
          <a href="ghgal40653670s.html#22">Parte prima</a>
        </li>
        <li>
          <a href="ghgal40653670s.html#40">Seconda parte</a>
        </li>
        <li>
          <a href="ghgal40653670s.html#76">Parte terza</a>
        </li>
        <li>
          <a href="ghgal40653670s.html#90">Indice delle amterie
          contenute in quest' opera</a>
        </li>
        <li>
          <a href="ghgal40653670s.html#100">Indice delle materie
          per ordine alfabetico</a>
        </li>
        <li>
          <a href="ghgal40653670s.html#104">Osservazioni sopra
          gli errori degli architetti</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
