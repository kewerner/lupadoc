<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502651s.html#6">Les merveilles de la ville de
      Rome</a>
    </li>
    <li>
      <a href="dg4502651s.html#8">[Text]</a>
      <ul>
        <li>
          <a href="dg4502651s.html#8">[Traicté des églises,
          stations, reliques des corps saincts qui y sont]</a>
          <ul>
            <li>
              <a href="dg4502651s.html#8">Les sept Eglises
              principales</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#8">La premier Eglise
                  est Saint Iean de Latran ›San Giovanni in
                  Laterano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#15">La seconde Eglise
                  de Saint Pierre au Mont Vatican ›San Pietro in
                  Vaticano‹ ▣</a>
                  <ul>
                    <li>
                      <a href="dg4502651s.html#13">[Saint Pierre
                      au Mont Vatican] ▣</a>
                      <ul>
                        <li>
                          <a href="dg4502651s.html#88">De
                          Saint'Agnes ›Sant'Agnese ‹, et des
                          Thermes des Diocletian ›Terme di
                          Diocleziano‹, et autres choses
                          &#160;▣</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502651s.html#22">La troisieme Eglise
                  est Saint Paul ›San Paolo fuori le Mura‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#25">La quatrieme Eglise
                  est Sainte Marie Majeure ›Santa Maria Maggiore‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#28">La cinquieme Eglise
                  est Saint Laurens hors des murs ›San Lorenzo
                  fuori le Mura‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#29">La sixieme Eglise
                  est Saint Sebastien ›San Sebastiano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#31">La septieme Eglise
                  est celle de Saint Croix in Hierusalem ›Santa
                  Croce in Gerusalemme‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502651s.html#32">Dans l'Isle ›Isola
              Tiberina‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#32">De la le Tybre, dict
              Transtevere ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#37">Aubourg ›Borgo‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#39">Depuis la Porte
              Flaminia autrement du Populo ›Porta del Popolo‹,
              iusques au pied du Capitole</a>
            </li>
            <li>
              <a href="dg4502651s.html#56">Depuis le Capitole à
              maingauche vers les monts</a>
            </li>
            <li>
              <a href="dg4502651s.html#63">Du Capitole à main in
              droicte vers les Monts</a>
            </li>
            <li>
              <a href="dg4502651s.html#70">Les stations qui sontes
              Eglises de Rome, tant le Quaresme, comme toute
              l'année, avec leurs indulgences accoustumées</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502651s.html#78">La guide romaine pour les
          estrangers qui viennent voir les Antiquitez de Rome,
          l'une apres l'autre distinctement reduite en un tresbel
          ordre, et forme</a>
          <ul>
            <li>
              <a href="dg4502651s.html#78">La premiere iournée, du
              Bourg ▣</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#79">De là le Tybre</a>
                </li>
                <li>
                  <a href="dg4502651s.html#80">De l'Isle du Tybre
                  ›Isola Tiberina‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#81">Du pont Sainte
                  Marie ›Ponte Rotto‹, du palais de Pilate ›Casa
                  dei Crescenzi‹, et d'autres choses ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#82">Du Mont Testaceus
                  ›Testaccio (Monte)‹, et d'autres choses ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#83">Des Thermes
                  d'Antoninus Caracalla ›Terme di Caracalla‹, et
                  autres choses</a>
                </li>
                <li>
                  <a href="dg4502651s.html#84">De Saint Iean
                  Lateran ›San Giovanni in Laterano‹, et autres
                  choses ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502651s.html#85">La seconde iournée
              ▣</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#85">De la Porte du
                  Populo ›Porta del Popolo‹</a>
                </li>
                <li>
                  <a href="dg4502651s.html#86">Des Cheaux de
                  marbre, qui sont à Monte Cavallo ›Fontana di
                  Monte Cavallo‹, et des Brains, ou Thermes de
                  Diocletian ›Terme di Diocleziano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#88">De la rue Pia</a>
                </li>
                <li>
                  <a href="dg4502651s.html#88">De la Vigne qui
                  estoit au Cardinal de Ferrare</a>
                </li>
                <li>
                  <a href="dg4502651s.html#88">De la Vigne du
                  Cardinal de Carpi, et autres</a>
                </li>
                <li>
                  <a href="dg4502651s.html#88">De la porta Pia
                  ›Porta Pia‹</a>
                </li>
                <li>
                  <a href="dg4502651s.html#90">Du temple d'Isis,
                  et d'autres choses</a>
                </li>
                <li>
                  <a href="dg4502651s.html#90">Des sept Sales
                  ›Sette Sale‹, du Collisee ›Colosseo‹, et autre
                  choses ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#91">Du Temple de la
                  Paix, du Monte Palatinus maintenant dict Palais
                  maieur ›Palatino‹, et d'autres choses notables
                  &#160;▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#92">Du For de Nerva
                  ›Foro di Nerva‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#93">Du Capitole
                  ›Campidoglio‹, et d'autres remarques ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#94">Des Porches
                  d'Octavia, de Septimius, et du Theatre de Pompèe
                  ›Teatro di Pompeo‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502651s.html#95">La troisieme iournée
              ▣</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#95">Des deux Colomnes
                  une d'Antoninus Pius ›Colonna di Antonino Pio‹
                  l'autre de Traian ›Colonna Traiana‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#96">De la Rotonde, ou
                  du Pantheon ›Pantheon‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#97">Des Baings
                  d'Agrippa, et de Neron ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#98">De la Place Navone
                  ›Piazza Navona‹, et du Pasquin ›Statua di
                  Pasquino‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502651s.html#99">[Ensemble les noms des
          papes, empereurs, &amp; autres princes chrestiens]</a>
          <ul>
            <li>
              <a href="dg4502651s.html#99">Table des Papes
              romains</a>
            </li>
            <li>
              <a href="dg4502651s.html#115">Les Roys, et Empereurs
              romains</a>
            </li>
            <li>
              <a href="dg4502651s.html#117">Les Roys de France</a>
            </li>
            <li>
              <a href="dg4502651s.html#118">Le Roys de Naples et
              de Sicile, qui commencerent à regner l'an de nostre
              salut MCLXXV.</a>
            </li>
            <li>
              <a href="dg4502651s.html#119">Les Ducs de Venize</a>
            </li>
            <li>
              <a href="dg4502651s.html#120">Les Ducs de Milan</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502651s.html#121">Table des eglises de
          Rome</a>
        </li>
        <li>
          <a href="dg4502651s.html#128">Les sept Merveilles du
          Monde</a>
          <ul>
            <li>
              <a href="dg4502651s.html#128">I. Des Murailles de
              Babylone. ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#129">II. De la Tour de
              Pharos ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#130">III. De la statue de
              Iuppiter ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#131">IV. Du Colosse de
              Rhodes ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#132">V. Du temple de Diane
              ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#133">VI. Du Mausolée
              d'Artemisia ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#134">VII. Des Pyramides
              d'Egypte ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502651s.html#136">Les Antiquites de la Villa
          de Rome</a>
          <ul>
            <li>
              <a href="dg4502651s.html#137">[Dedica ai lettori]
              Aux lecteurs</a>
            </li>
            <li>
              <a href="dg4502651s.html#138">De l'edification de
              Rome</a>
            </li>
            <li>
              <a href="dg4502651s.html#140">Du circuit, et
              enceinte de Rome</a>
            </li>
            <li>
              <a href="dg4502651s.html#141">Des Portes</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#142">Porte Maieur ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502651s.html#143">Des rues</a>
            </li>
            <li>
              <a href="dg4502651s.html#143">Des Ponts qui sont sur
              le Tybre, et ceux qui les ont edifiez</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#145">De l'Isle du Tybre
                  ›Isola Tiberina‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502651s.html#145">Des Monts</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#146">Palazzo Maggiore
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#147">De Mont Testaceus
                  ›Testaccio (Monte)‹ &#160;▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502651s.html#148">Des Eaux, et de ceux
              qui les conduirent à Rome</a>
            </li>
            <li>
              <a href="dg4502651s.html#148">De la Cloque ›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#149">Des Aquedues ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#150">Des sept Sales ›Sette
              Sale‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#151">Des Thermes, c'est à
              sçavoir des Bains, et de ceux qui les ont faict
              bastir ▣</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#152">Thermes
                  Diocletianes ›Terme di Diocleziano‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502651s.html#153">Des Naumachies, où se
              faisoient les batailles Navales</a>
            </li>
            <li>
              <a href="dg4502651s.html#153">Des Cirques</a>
            </li>
            <li>
              <a href="dg4502651s.html#154">Des Theatres, et de
              ceux quiles edifierent</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#154">Du Theatre de
                  Marcellus ›Teatro di Marcello‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502651s.html#155">Des Amphitheatres, et
              de leurs edificateurs</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#155">Le Colisee
                  ›Colosseo‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502651s.html#156">Amphitheatre de
                  Statilius ›Anfiteatro di Statilio Tauro‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502651s.html#156">Des Places</a>
              <ul>
                <li>
                  <a href="dg4502651s.html#157">Foro Boario
                  appellé aujourd'huy Campovaccino ›Foro Boario‹
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502651s.html#158">Des Arcs Triomphans,
              et à qui on les donnoit et eslevoit</a>
            </li>
            <li>
              <a href="dg4502651s.html#159">Des Porches ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#161">Des Trophèes, et
              Colomnes remarquables ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#162">Des Colosses</a>
            </li>
            <li>
              <a href="dg4502651s.html#162">Des Pyramides</a>
            </li>
            <li>
              <a href="dg4502651s.html#162">Des Metes</a>
            </li>
            <li>
              <a href="dg4502651s.html#163">Des Obelisques</a>
            </li>
            <li>
              <a href="dg4502651s.html#163">Des Statues</a>
            </li>
            <li>
              <a href="dg4502651s.html#163">De Marfore ›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#163">Des Cheuvaux</a>
            </li>
            <li>
              <a href="dg4502651s.html#164">Des Librairies</a>
            </li>
            <li>
              <a href="dg4502651s.html#164">Des Horologes</a>
            </li>
            <li>
              <a href="dg4502651s.html#164">Des Palais</a>
            </li>
            <li>
              <a href="dg4502651s.html#165">De la maison dorèe de
              Neron ›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#166">Des autres maisons des
              Citoyens</a>
            </li>
            <li>
              <a href="dg4502651s.html#166">Des Cours</a>
            </li>
            <li>
              <a href="dg4502651s.html#167">Des petits Senats</a>
            </li>
            <li>
              <a href="dg4502651s.html#167">Des Magistrats</a>
            </li>
            <li>
              <a href="dg4502651s.html#168">Des cornices</a>
            </li>
            <li>
              <a href="dg4502651s.html#168">Des Tribus</a>
            </li>
            <li>
              <a href="dg4502651s.html#168">Des contrües, ou
              quartiers, et de leurs enseignes</a>
            </li>
            <li>
              <a href="dg4502651s.html#168">Des Basiliques</a>
            </li>
            <li>
              <a href="dg4502651s.html#169">Du Capitole
              ›Campidoglio‹ ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#170">De l'Aerarium
              ›Aerarium‹, ou chambre, et maison de Ville, et quelle
              monnoye courit à Rome en ce temps</a>
            </li>
            <li>
              <a href="dg4502651s.html#171">De Craecostasis
              ›Grecostasi‹ ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#171">De Greffe du peuple
              Romain</a>
            </li>
            <li>
              <a href="dg4502651s.html#172">De ce qu'ils
              appelloient Rostra ›Rostri‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#172">De la Colonme dicte
              Milliaria ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#172">De Temple de Carmenta
              ›Tempio di Carmenta‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#172">De la Colomne de
              guerre ›Columna Bellica‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#173">De la Colomne Lattaria
              ›Columna Lactaria‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#173">De l'Equimelium
              ›Equimelio‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#173">Du champ de Mars
              ›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#173">Du Sigillum Sororium
              ›Tigillum Sororium‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#173">Des champs nommez
              Forastieri ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#174">De la Ville publique
              ›Villa Publica‹ ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#174">De la ›Taberna
              Meritoria‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#175">Du Parc</a>
            </li>
            <li>
              <a href="dg4502651s.html#175">Des Iardins</a>
            </li>
            <li>
              <a href="dg4502651s.html#175">Du Velabrum
              ›Velabro‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#176">Des Carines
              ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#176">Des Tertres</a>
            </li>
            <li>
              <a href="dg4502651s.html#176">Des Prez</a>
            </li>
            <li>
              <a href="dg4502651s.html#177">Des Greniers
              publics</a>
            </li>
            <li>
              <a href="dg4502651s.html#177">Des Prisons
              publiques</a>
            </li>
            <li>
              <a href="dg4502651s.html#177">De quelque jeux, et
              festes, qu'on souloit celebrer dans Rome</a>
            </li>
            <li>
              <a href="dg4502651s.html#178">Du Sepulchre
              d'Auguste, d'Adrian, et de Septimius ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#179">Des Temples</a>
            </li>
            <li>
              <a href="dg4502651s.html#180">Des Prestres, des
              Vierges, Vestements, Vases, et autres instrumens
              faits à l'usage des sacrifices, et de leurs
              instituteurs</a>
            </li>
            <li>
              <a href="dg4502651s.html#181">De l'Arcenal</a>
            </li>
            <li>
              <a href="dg4502651s.html#181">De l'armee terrestre,
              et maritime des Romains et de leurs enseignes</a>
            </li>
            <li>
              <a href="dg4502651s.html#182">Des Triopmhes, et à
              qui on les octroyoit, et qui fut le premier qui
              triomphe, et de combien de sortes il y en avoit</a>
            </li>
            <li>
              <a href="dg4502651s.html#182">Des Couronnes, et à
              qui elles se donnoient</a>
            </li>
            <li>
              <a href="dg4502651s.html#183">Du Nombres du peuple
              Romain</a>
            </li>
            <li>
              <a href="dg4502651s.html#183">Des Richesses du
              peuple Romain</a>
            </li>
            <li>
              <a href="dg4502651s.html#183">De la liberalitè des
              anciens Romains</a>
            </li>
            <li>
              <a href="dg4502651s.html#184">Des anciens Mariages,
              et de leurs ceaemonies</a>
            </li>
            <li>
              <a href="dg4502651s.html#184">De la civilte, qu'on
              enseignoit aux enfans</a>
            </li>
            <li>
              <a href="dg4502651s.html#185">De devorce des
              mariages</a>
            </li>
            <li>
              <a href="dg4502651s.html#185">Des obseques antiques,
              et de leurs ceremonies</a>
            </li>
            <li>
              <a href="dg4502651s.html#186">Des Tours</a>
            </li>
            <li>
              <a href="dg4502651s.html#186">Du Tybre</a>
            </li>
            <li>
              <a href="dg4502651s.html#187">Du Palais Papal
              ›Palazzo Apostolico Vaticano‹, et Belvedere ›Cortile
              del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4502651s.html#187">De dela le Tybre</a>
            </li>
            <li>
              <a href="dg4502651s.html#187">Recapitulation des
              Antiquites</a>
            </li>
            <li>
              <a href="dg4502651s.html#188">Des Temples des
              anciens hors de Rome ▣</a>
            </li>
            <li>
              <a href="dg4502651s.html#190">Combien de fois Rome a
              estè prise</a>
            </li>
            <li>
              <a href="dg4502651s.html#190">Des feux des anciens,
              dont peu d'Autheurs ont escrit, recueilliz de
              quelques fragmens, et historiens</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502651s.html#194">Table de ce qui est plus
          remarquable en ce livre</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
