<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dm6504600a2s.html#6">Le Scienze e le Arti sotto
      il Pontificato di Pio IX.</a>
      <ul>
        <li>
          <a href="dm6504600a2s.html#8">Nuova Scuola delle
          Fanciulle in Borgo Vittorio</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#10">Nuova Scuola per le
              Fanciulle in Borgo Vittorio ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#12">Nuova Scuola per le
          Fanciulle nella regione Celimontana</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#14">Nuova Scuola per le
              Fanciulle nella regione Celimontana ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#16">Nuova Scuola per le
          Fanciulle a Monte Mario</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#18">Nuova Scuola per le
              Fanciulle a Monte Mario ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#20">Nuova Fontana a Monte
          Mario</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#22">Nuova Fontana a
              Monte Mario ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#24">Piazza Pia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#26">Piazza Pia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#28">Palazzo della
          Dataria</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#30">Palazzo della
              Dataria ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#32">Nuovo Palazzo di San
          Felice</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#34">Nuovo Palazzo detto
              di San Felice ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#36">Nuova Strada e Piazza a
          Monte Cavallo</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#38">Nuova Strada e
              Piazza di Monte Cavallo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#40">Nuova Casa pei Poveri
          in Trastevere</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#42">Casa pei Poveri in
              Trastevere ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#44">Casa per le famiglie
          più indigenti sulla Piazza di San Clemente</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#46">Casa pei Poveri
              sulla Piazza di San Clemente ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#48">Nuova Fontana e
          Lavatojo sulla Piazza di San Clemente</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#50">Nuovo Lavatojo e
              Fontana nella Piazza di San Clemente ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#52">Villa Pia ›Casina di
          Pio IV‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#54">Villa Pia ›Casina
              di Pio IV‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#56">Istituto agrario di
          Carità di Vigna Pia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#60">Vigna Pia ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#62">Interno del nuovo
              edificio di Vigna Pia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#64">Nuova Chiesa
          parrocchiale fuori di Porte Portese</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#66">Nuova Chiesa
              parocchiale fuori di Porta Portese ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#68">Nuovo braccio al Buon
          Pastore per le Carceri delle donne</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#70">Nuovo braccio al
              Monastero del Buon Pastore ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#72">Chiesa di Santa Croce e
          San Bonaventura ›Santa Croce e San Bonaventura dei
          Lucchesi‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#74">Restauri alla
              Chiesa di ›Santa Croce e San Bonaventura dei
              Lucchesi‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#76">Nuova Chiesa
          sull'Esquilino dedicata al Santissimo Redentore in onore
          di San'Alfonso Maria de Liguori</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#80">Nuova Chiesa di
              Sant'Alfonso Maria de Liguori sull'Esquilino ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#82">Interno della Nuova
              Chiesa di Sant'Alfonso Maria de Liguori ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#84">Restauro alla Chiesa di
          ›Santa Maria in Monticelli‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#86">Restauro alla
              Chiesa di Santa Maria in Monticelli ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#88">Restauri nella Basilica
          de' Santi Bonifacio ed Alessio al Monte Aventino
          ›Sant'Alessio‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#90">Interno della
              Basilica de' Santi Bonifacio, ed Alessio
              ›Sant'Alessio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#92">›San Vitale‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#94">Interno della
              Chiesa di ›San Vitale‹e ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#96">Restauri in ›San
          Bernardo alle Terme‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#98">Interno della
              Chiesa di San Bernardo ›San Bernardo alle Terme‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#100">La ›Scala Santa‹ ed il
          Cenobio dei Passionisti</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#102">La ›Scala Santa‹
              ed il Cenobio dei Passionisti ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#104">Restauro alla Chiesa
          di ›Santa Maria in Via Lata‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#106">Restauro alla
              Chiesa di ›Santa Maria in Via Lata‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#108">Monumento a Torquato
          Tasso e Cappella in Sant'Onofrio ›Sant'Onofrio al
          Gianicolo: Monumento al Tasso‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#112">Monumento a
              Torquato Tasso nella Chiesa di Sant'Onofrio
              ›Sant'Onofrio al Gianicolo: Monumento al Tasso‹ ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#114">Cappalla eretta
              per il Monumento di Torquato Tasso ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#116">Restauri in ›San
          Lorenzo in Lucina‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#118">Interno della
              Chiesa di ›San Lorenzo in Lucina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#120">Chiesa di Santa Maria
          della Pace in Sinigallia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#122">Chiesa di Santa
              Maria della Pace in Sinigaglia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#124">Nuova Fontana in
          Anagni</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#126">Nuova Fontana in
              Anagni ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#128">Casa di Ricovero in
          Sinigallia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#130">Casa di Ricovero
              in Sinigaglia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#132">Prospetto della Chisa
          del Nuovo Ginnasio in Sinigaglia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#134">Chiesa del
              Collegio in Sinigaglia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#136">Basilica di Sant'Elia
          presso Nepi</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#140">Esterno della
              Basilica di Sant'Elia presso Nepi ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#142">Interno della
              Basilica di Sant'Elia presso Nepi ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#144">Statua d'Ercole
          rinvenuta negli Scavi al Palazzo Righetti</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#146">La Statua d'Ercole
              rinvenuta nel Palazzo Pio ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#148">Scavi a ›San Clemente
          ‹ Nartece, e parte inferiore della Nave di mezzo
          dell'antichissima Basilica</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#152">Scavi a ›San
              Clemente‹.Nartece dell'antichissima Basilica ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#154">Scavi a ›San
              Clemente‹. Parte inferiore della Nave di mezzo
              del'anitchissima Basilica ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#156">Scavi a ›San
          Clemente‹. Parte superiore della Nave di mezzo e Navata
          settentrionale</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#160">Scavi a ›San
              Clemente‹. Parte superiore della Navata di mezzo
              dell'antichissima Masilica ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#162">Scavi a ›San
              Clemente‹. Navata destra dell'antichissima Basilica
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#164">Scavi al Lato sud-est
          del Palatino</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#168">Scavi al Lato
              sud-est del Palatino, 1. ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#170">Scavi al Lato
              sud-est del Palatino, 2. ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#172">Colombario della Vigna
          Codini</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#176">Colombario presso
              la Porta San Sebastiano nella vigna Codini, 1. ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#178">Colombario presso
              la Porta San Sebastiano nella vigna Codini, 2. σ</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#180">Scavi a prima
          Porta</a>
        </li>
        <li>
          <a href="dm6504600a2s.html#184">Restauri nella Chiesa
          di San Nicola in Carcere Tulliano ›San Nicola in
          Carcere‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#186">Restauri alla
              Chiesa di San Nicola in Carcere Tulliano ›San Nicola
              in Carcere‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#188">Sotterranei di San
          Nicola in Carcere Tulliano ›San Nicola in Carcere‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#192">Sotterranei di San
              Nicola in Carcere Tulliano ›San Nicola in Carcere‹,
              1. ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#194">Sotterranei di San
              Nicola in Carcere Tulliano ›San Nicola in Carcere‹,
              2. ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#196">Il Monte Pincio
          ›Pincio‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#198">Passeggiata
              pubblica al Monte Pincio ›Pincio‹ ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#200">Ritrovamento della
              Statua di Cesare Augusto a Prima Porta ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#202">Camera con pitture
              discoperta negli Scavi di Prima Porta ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#204">›Portico degli Dei
          Consenti‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#206">›Portico degli Dei
              Consenti‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#208">Scavi nel Trastevere.
          Stazione della Coorte Settima dei Vigili</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#210">Scavi in
              Trastevere. Stazione della VIII. Coorte dei Vigili
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#212">Il Ponte Senatorio
          ›Ponte Rotto‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#214">Ponte Senatorio
              ›Ponte Rotto‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#216">Pantheon d'Agrippa
          detto la Rotonda ›Pantheon‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#218">Pantheon d'Agrippa
              detta la Rotonda ›Pantheon‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#220">Arsenale d'Artiglieria
          a Belvedere. Laboratorio e Macchine</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#222">Arsenale
              d'Artiglieria a Belvedere. Maboratorio, e Macchine
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#224">Restauri fatti al
          ›Colosseo‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#228">Ambulacro del
              terzo piano dell'Anfiteatro Flavio ›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#230">Ambulacro del
              quarto piano dell'Anfiteatro Flavio ›Colosseo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#232">La Basilica Giulia
          ›Basilica Iulia‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#234">Basilica Giulia
              nel Foro Romano ›Basilica Iulia‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#236">Scavi d'Ostia - Gran
          cella con vettine</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#238">Scavi d'Ostia.
              Gran cella con vettine ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#240">Mitreo</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#242">Mitreo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#244">Terme Ostiensi</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#246">Terme Ostiense
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#248">Palestra nelle Terme
          d'Ostia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#250">Palestra nelle
              Terme Ostiense ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#252">Scavi d'Ostia Via
          Ostiense e Porta Romana</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#254">Scavi d'Ostia. Via
              Ostiense e Porta Romana ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#256">Restauri della Rocca
          d'Ostia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#258">Restauri della
              Rocca d'Ostia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#260">Scavi a
          Sant'Anastasia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#264">Scavi a
              Sant'Anastasia, 1. ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#266">Scavi a
              Sant'Anastasia, 2. ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#268">Cemeterio di San
          Calisto</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#272">Catacombe di San
              Calisto. Camera di Santa Cecilia ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#274">Catacombe di San
              Calisto. Camera de Pontefici ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#276">Cemeterio di
          Priscilla</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#280">Cemeterio di
              Priscilla. Cripte discoperte attorno la Camera Greca
              ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#282">Cemeterio di
              Priscilla. Cripta detta Camera Greca ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#284">Gli Scavi della Via
          Latina</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#286">Scavi della Via
              Latina ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#288">Basilica di Santo
          Stefano Protomartire al terzo Miglio della Via Latina
          ›Santo Stefano sulla Via Latina‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#290">Basilica di Santo
              Stefano nella Via Latina ›Santo Stefano sulla Via
              Latina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#292">Ipogeo con Stucchi e
          Pitture. Sulla sinistra della Via Latina</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#294">Ipogeo con Stucchi
              e Pitture. Sulla sinistra della Via Latina ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#296">Sepolcro con Stucchi
          Bianchi. A destra della Via Latina</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#298">Sepolcro con
              Stucchi Bianchi. A destra della Via Latina ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#300">Ristauro all'Arco di
          Trajano in Benevento</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#302">Arco di Trajanio
              in Benevento ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#304">Scavi della Via
          Appia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#306">Scavi della Via
              Appia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#308">Nuovo Ponte. Sulla
          Nuova Via di Valvisciolo</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#310">Nuovo Ponte sulla
              Nuova Via di Valvisciolo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#312">Ponte Sospeso sul
          Tevere presso San Giovanni de' Fiorentini</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#314">Ponte Sospeso sul
              Tevere presso San Giovanni de' Fiorentini ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#316">Ponte di Ferro della
          Pio-Latina presso Velletri</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#318">Ponte di Ferro
              della Pio-Latina presso Velletri ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#320">Ponte d'Aricia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#322">Ponte d'Aricia
              detta volgarmente la Riccia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#324">Nuovo Ponte a
          Civitacastellana</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#326">Nuovo Ponte presso
              Civita Castellana ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#328">Nuovo Ponte sul
          Torrente Rio-Fiume nella Via Aurelia</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#330">Nuovo Ponte sul
              Torrente di Riofiume nella Via Nazionale Aurelia
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#332">Nuovo Ponte
          sull'Aniene</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#334">Nuovo Ponte
              sull'Aniene presso il Ponte Mamolo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#336">Nuova Caserma al
          Castro Pretorio</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#340">Nuova Caserma al
              Castro Pretorio ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#342">Interno della
              Nuova Caserma al Castro Pretorio ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#344">Arsenale d'Artiglieria
          a Belvedere. Cavallerizza</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#348">Esterno della
              Cavallerizza a Belvedere ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#350">Interno della
              Cavallerizza a Belvedere ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#352">Arsenale d'Aeriglierie
          a Belvedere. Magazzino di Carriaggi</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#354">Arsenale
              d'Artiglieria d Belvedere. Magazzino di Carriaggi
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#356">Nuova Strada al
          Gianicolo</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#360">Nuova Strada al
              Gianicolo. Sezione principale - fino a San Pietro
              Montorio ▣</a>
            </li>
            <li>
              <a href="dm6504600a2s.html#362">Nuova Strada al
              Gianicolo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#364">La Porta Gianicolense
          detta di San Pancrazio ›Porta San Pancrazio‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#366">Porta Gianicolense
              detta di San Pancrazio ›Porta San Pancrazio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#368">Le Mura
          Gianicolensi</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#370">Le Mura
              Gianicolensi ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#372">Edicola fuori di Porta
          San Pancrazio. Erette ove fu ritrovato il Capo di
          Sant'Andrea Apostolo</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#374">Edicola fuori di
              Porta di San Pancrazio, erette ove fu ritrovato il
              capo di Sant'Andrea Apostolo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#376">Badia di
          Valvisciòlo</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#378">Badia di
              Valvisciòlo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#380">›Porta Pia‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#382">Porta Pia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#384">Esterno di ›Porta
          Pia‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#386">Esterno di ›Porta
              Pia‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#388">Monastero de' Canonici
          Regolari Lateranensi presso la Basilica nomentana di
          Sant'Agnese</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#390">Monastero de'
              Canonici Regolari Lateranensi presso la Basilica
              nomentana di Sant'Agnese ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#392">Interno della Basilica
          di Sant'Agnese nella Via Nomentana ›Sant'Agnese fuori le
          Mura‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#394">Interno della
              Basilica di Sant'Agnese nella Via Nomentana
              ›Sant'Agnese fuori le Mura‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#396">Basilica e Cemeterio
          di Sant'Alessandro nella Via Nomentana ›Sant'Alessandro
          (Via Nomentana)‹</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#398">Basilica e
              Cemterio di Sant'Alessandro nella Via Nomentana
              ›Sant'Alessandro (Via Nomentana)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dm6504600a2s.html#400">Prospetto Laterale del
          Monastero di Santa Marta</a>
          <ul>
            <li>
              <a href="dm6504600a2s.html#402">Prospetto Laterale
              del Monastero di Santa Marta ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
