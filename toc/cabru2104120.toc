<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cabru2104120s.html#8">Vita di Filippo di Ser
      Brunellesco, architetto fiorentino</a>
      <ul>
        <li>
          <a href="cabru2104120s.html#10">Al chiarissimo Signore
          Senatore Cesare Lucchesini</a>
        </li>
        <li>
          <a href="cabru2104120s.html#14">Memoria intorno al
          Risorgimento delle belle arti in Toscana e di rastauri
          delle medesime</a>
        </li>
        <li>
          <a href="cabru2104120s.html#166">Vita di Filippo di Ser
          Brunellesco scultore e architetto fiorentino scritta da
          Filippo Baldinucci</a>
        </li>
        <li>
          <a href="cabru2104120s.html#302">Vita di Filippo di Ser
          Brunellesco scultore e architetto fiorentino scritta da
          Anonimo contemporaneo autore&#160;</a>
        </li>
        <li>
          <a href="cabru2104120s.html#374">Indice generale</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
