<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="emon5404380s.html#8">Del duomo di Monreale e di
      altre chiese siculo normanne</a>
      <ul>
        <li>
          <a href="emon5404380s.html#10">Introduzione</a>
        </li>
        <li>
          <a href="emon5404380s.html#14">I. Del Duomo di
          Monreale</a>
        </li>
        <li>
          <a href="emon5404380s.html#32">II. Delle più cospicue e
          meglio conservate chiese siculo-normanne</a>
        </li>
        <li>
          <a href="emon5404380s.html#52">III. Della forma delle
          chiese siculo-normanne</a>
        </li>
        <li>
          <a href="emon5404380s.html#64">Note</a>
        </li>
        <li>
          <a href="emon5404380s.html#98">Indice delle materie</a>
        </li>
        <li>
          <a href="emon5404380s.html#98">Indice delle tavole</a>
        </li>
        <li>
          <a href="emon5404380s.html#100">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
