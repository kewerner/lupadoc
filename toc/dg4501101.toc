<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501101s.html#6">Opusculum De Mirabilibus Novae
      &amp; Veteris Urbis Romae</a>
      <ul>
        <li>
          <a href="dg4501101s.html#7">Epistola</a>
        </li>
        <li>
          <a href="dg4501101s.html#17">Liber I.</a>
        </li>
        <li>
          <a href="dg4501101s.html#88">Liber II.</a>
        </li>
        <li>
          <a href="dg4501101s.html#164">Liber III.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
