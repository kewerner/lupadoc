<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4504051a12s.html#8">Roma</a>
      <ul>
        <li>
          <a href="dg4504051a12s.html#6">[Antiporta] ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4504051a12s.html#8">Tomo I.</a>
      <ul>
        <li>
          <a href="dg4504051a12s.html#10">Indice de' capi</a>
        </li>
        <li>
          <a href="dg4504051a12s.html#12">Al cortese lettore.
          Idea e vantaggi dell'opera.</a>
        </li>
        <li>
          <a href="dg4504051a12s.html#15">Pianta dimostrativa di
          Roma Antica ◉</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Capena‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta San Paolo‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Testaccio
              (monte)‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Portuensis‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Settimiana‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Porta Ianiculensis
              ›Porta San Pancrazio‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Isola Tiberina‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Porta Aurelia
              ›Porta San Pietro‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Porta Flaminia
              ›Porta del Popolo‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Pinciana‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Salaria‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Nomentana‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Viminalis‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Porta Gabiusa
              ›Porta Metronia‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Porta Inter
              Aggeres ›Porta Chiusa‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Porta Praenestina
              ›Porta Maggiore‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Porta Celimontana
              ›Arco di Dolabella‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Naevia‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Latina‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Romanula‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Trigonia‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Mugonia‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Porta Ianuale
              ›Ianus Geminus, aedes‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta
              Carmentalis‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Trionfale‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Collina‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Porta Esquilina
              ›Arco di Gallieno‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Porta Asinaria‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Ponte Sublicio‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Ponte Palatino‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Ponte Fabricio‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">›Ponte Cestio‹
              ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Ponte Gianicolense
              ›Ponte Sisto‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Ponte Trionfale
              ›Pons Neronianus‹ ◉</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#15">Ponte Elio ›Ponte
              Sant'Angelo‹ ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#16">Capo I. Notizie
          preliminari.</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#19">Piano Antico.</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#20">Settimonzio.</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#20">›Palatino‹</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#21">›Capitolino
                  (Monte)‹</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#22">›Celio‹</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#23">›Aventino‹</a>
                </li>
                <li>
                  <a href=
                  "dg4504051a12s.html#23">›Quirinale‹</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#24">›Viminale‹</a>
                </li>
                <li>
                  <a href=
                  "dg4504051a12s.html#24">›Esquilino‹</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#25">›Pincio‹</a>
                </li>
                <li>
                  <a href=
                  "dg4504051a12s.html#25">›Gianicolo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#26">›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#27">›Tevere‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#27">Mura.</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#31">Porte.</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#35">Regioni.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#37">Capo II. Adjacenze del
          Monte Palatino</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#37">Colonne dette del
              ›Tempio di Giove Statore ‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#40">›Velabro‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#40">›Tempio del Divo
              Romolo‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#41">›Arco di
              Giano‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#43">Arco piccolo di
              Settimio Severo ›Arco degli Argentari‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#44">Tav. I. Arco
                  di Settimio Severo al Velabro ›Arco degli
                  Argentari‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#46">Tav. II. Arco
                  di Settimio Severo al Velabro ›Arco degli
                  Argentari‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#49">›Foro Boario‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#49">Acqua di Giuturna
              ›Fonte di Giuturna‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#51">›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#52">›Circo
              Massimo‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#55">›Acqua Crabra‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#57">›Arco di
              Costantino‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#60">Tav. III.
                  ›Arco di Costantino‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#62">Tav. IV. ›Arco
                  di Costantino‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#64">Tav. V. ›Arco
                  di Costantino‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#66">›Arco di Tito‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#68">Tav. VI ›Arco
                  di Tito‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#70">Capo III. Monte
          ›Palatino‹</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#70">Riflessioni</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#72">Rovine nell'orto
              degl'Inglesi</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#73">Rovine negli Orti
              Spada ›Villa Spada (Palatino)‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#74">Tav. VII
                  ›Villa Spada (Palatino)‹? ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#84">Tav. VIII.
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#86">Tav. IX.
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#88">Tav. X. F. 1
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#90">Tav. XI. F. 1
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#92">Tav. XII.
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#94">Tav. XIII.
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#96">Tav. XIV.
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#81">Rovine negl'›Orti
              Farnesiani‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#98">Capo IV. Antichità del
          moderno campo vaccino ›Foro Boario‹</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#98">›Tempio di Venere
              e Roma‹. Oggi Chiesa di ›Santa Francesca Romana‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#100">›Via Sacra‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#101">Tempio della Pace
              ›Foro della Pace‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#103">Tempio di Venere
              Cloacina ›Cloacina, Sacrum‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#104">Tempio di Romolo
              e Remo ›Tempio del Divo Romolo‹ . Oggi Chiesa dei
              ›Santi Cosma e Damiano‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#105">›Tempio di
              Antonino e Faustina‹. Oggi ›San Lorenzo in
              Miranda‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#108">Tav. XV.
                  ›Tempio di Antonino e Faustina‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#110">Tav. XVI.
                  ›Tempio di Antonino e Faustina‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#112">Tav. XVII.
                  ›Tempio di Antonino e Faustina‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#107">›Foro Romano‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#114">Basilica di Paolo
              Emilio ›Basilica Aemilia‹. Oggi Chiesa di
              ›Sant'Adriano al Foro Romano‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#116">Tav. XVIII.
                  ›Basilica Aemilia‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#118">›Arco di Settimio
              Severo‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#121">Colonna detta del
              ›Tempio di Giove Custode‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#122">Capo V. Monte
          Capitolino ›Campidoglio‹, e sue adiacenze.</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#122">›Carcere
              Mamertino‹, e Tulliano. Oggi ›San Pietro in
              Carcere‹.</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#124">›Sepolcro di Gaio
              Poplicio Bibulo‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#125">Sepolcro de'
              Claudi ›Sepulcrum: Claudii‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#130">›Teatro di
              Marcello‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#127">Tav. XX.
                  ›Teatro di Marcello‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#129">Tav XIX.
                  Pianta del ›Teatro di Marcello‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#139">Tav. XXI.
                  ›Teatro di Marcello‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#140">Casa detta di
              Cola di Rienzo ›Casa dei Crescenzi‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#141">›Ponte
              Palatino‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#142">›Tevere‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#144">›Tempio della
              Fortuna Virile‹. Oggi ›Santa Maria Egiziaca‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#146">Tav. XXII.
                  ›Tempio della Fortuna Virile‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#149">›Tempio di Vesta
              (Foro Romano)‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#151">Tav. XXIII.
                  ›Tempio di Vesta‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#153">›Tempio della
              Pietà‹. Oggi ›San Nicola in Carcere‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#154">Carcere de'
              Decmviri ›San Nicola in Carcere‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#154">Salite del
              Campidoglio ›Centum Gradus‹ ›Clivus Capitolinus‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#155">›Tempio della
              Concordia‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#156">Tav. XXIV.
                  ›Tempio della Concordia‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#158">TAV. XXV.
                  ›Tempio della Concordia‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#163">Tempio di Giove
              Tonante ›Iuppiter Tonans, Aedes‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#164">›Tabularium‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#165">Sostruzioni
              (sic!), o siano Mura Capitoline</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#165">Fabriche di sito
              incerto</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#167">Intermonzio</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#167">›Rupe Tarpea‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#168">Campidoglio
              moderno</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#175">Capo VI. Monte
          ›Celio‹</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#175">›Clivo di
              Scauro‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#176">›Curia
              Hostilia‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#177">Arco di Silano e
              Dolabella ›Arco di Dolabella‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#178">Castro de'
              Pellegrini ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#179">›Acquedotto
              Claudio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#180">›Tempio del Divo
              Claudio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#181">Battistero di
              Costantino ›Battistero Lateranense‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#182">Tav. XXVI.
              ›Battistero Lateranense‹ ▣</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#186">›Anfiteatro
              Castrense‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#187">›Tempio di Venere
              e Cupido‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#188">›Obelisco
              Lateranense‹</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4504051a12s.html#192">Tomo II</a>
      <ul>
        <li>
          <a href="dg4504051a12s.html#194">Indice de' capi</a>
        </li>
        <li>
          <a href="dg4504051a12s.html#196">Capo VII. Adiacenze
          del Monte ›Celio‹</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#196">›Suburra‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#197">Anfiteatro Flavio
              ›Colosseo‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#200">Tav. I.
                  ›Colosseo‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#206">Tav. II.
                  ›stampa‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#208">Tav. II.
                  ›Colosseo‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#210">Tav. III.
                  ›Colosseo‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#219">Parte
                  esteriore dell'Anfiteatro</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#220">Parte interna
                  dell'Anfiteatro</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#221">Podio, e
                  Arena</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#224">Tav.
                  ›Colosseo‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#226">›Meta Sudans‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#227">Capo VIII. Monumenti
          lungo la Via Appia</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#227">›Via Appia‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#228">›Via Appia‹
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#230">›Sepolcro degli
              Scipioni‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#232">›Arco di
              Druso‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#233">Campo degli
              Orazi, e tomba di Orazia</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#234">Fiumicello Almone
              ›Almone‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#235">Sepolcri
              d'incerta denominazione</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#235">›Catacombe di San
              Sebastiano‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#236">Mutatorio
              ›Mutatorium Caesaris‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#237">›Tomba di Cecilia
              Metella‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#239">Sepolcro de'
              Servili ›Sepulcrum: Servilii‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#240">Circo di
              Caracalla ›Circo di Massenzio‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#243">Tav. XXXIII.
                  ›Circo di Massenzio‹? ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#245">Tav. XXXII.
                  Pianta del Circo detto di Caracalla ›Circo di
                  Massenzio‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#248">Tav. XXXIV.
                  ›Circo di Massenzio‹? ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#250">Tav. XXXV.
                  ›Circo di Massenzio‹? ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#252">Tav. XXXVI.
                  ›Circo di Massenzio‹? ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#254">Tav. XXXVII.
                  ›Circo di Massenzio‹? ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#256">Tav. XXXVIII.
                  ›Circo di Massenzio‹? ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#258">Tav. XXXIX.
                  ›Circo di Massenzio‹? ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#260">Tav. XL.
                  ›Circo di Massenzio‹? ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#269">Tempio detto
              dell'Onore e della Virtù ›Honos et Virtus, aedes‹ .
              In oggi Chiesa di ›Sant'Urbano‹ alla Caffarella.</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#270">Tav. XLI.
                  Tempio detto dell'Onore e della Virtù ›Honos et
                  Virtus, aedes‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#272">Tav. XLII.
                  Tempio detto dell'Onore e della Virtù ›Honos et
                  Virtus, aedes‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#274">Fontana Egeria
              ›Camenarum, Fons et Lucus‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#275">Tempio del Dio
              Ridicolo ›Tempio di Rediculus‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#278">Capo IX. Monte
          ›Aventino‹ e sue adjacenze</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#278">›Terme di
              Caracalla‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#277">Tav. XLIII.
                  Pianta delle ›Terme di Caracalla‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#281">Tav. XLIV.
                  ›Terme di Caracalla‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#285">Tempio della
              Pudicizia Patrizia ›Tempio della Pudicitia Patricia‹
              &#160;Oggi ›Santa Maria in Cosmedin‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#286">Clivo Publicio
              ›Clivo dei Publicii‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#287">›Acqua Appia‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#287">Navali ›Navalia‹.
              Oggi ›Marmorata‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#288">›Ponte
              Sublicio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#290">Arco detto di
              Orazio Coclite ›Arco di San Lazzaro‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#290">›Testaccio
              (monte)‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#292">›Piramide di Caio
              Cestio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#295">Basilica di San
              Paolo ›San Paolo fuori le Mura‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#297">Capo X. ›Campo
          Marzio‹ e sue adjacenze</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#297">›Portico di
              Ottavia‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#298">›Ponte
              Fabricio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#299">›Isola
              Tiberina‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#301">›Ponte
              Cestio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#301">›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#303">Ponte e Mole
              Adriana. Oggi ›Ponte Sant'Angelo‹ e ›Castel
              Sant'Angelo‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#305">Ponte Trionfale
              ›Pons Neronianus‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#307">›Vaticano‹
              moderno</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#324">Sepoltura di
              Nerone ›Sepulcrum Domitii‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#321">›Sepulcrum
                  Domitii‹? ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#325">Ponte Molle
              ›Ponte Milvio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#326">›Muro Torto‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#328">Obelisco del
              Popolo ›Obelisco Flaminio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#328">›Mausoleo di
              Augusto‹, e Ustrino de' Cesari ›Ustrinum Augsti‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#330">›Colonna di Marco
              Aurelio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#330">Obelisco Orario
              ›Horologium Augusti‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#331">Tempio di
              Antonino Pio ›Tempio di Antonino e Faustina‹. In oggi
              Dogana di Terra ›Dogana nuova di Terra‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#333">›Pantheon‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#334">Tav. XLV.
                  ›Pantheon‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#336">Tav. XLVI.
                  ›Pantheon‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#338">Tav. XLVII.
                  ›Pantheon‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#347">Circo Agonale o
              Alessandrino. Oggi ›Piazza Navona‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#347">Tempio d'Apollo
              ›San Nicola ai Cesarini‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#347">Condotto
              dell'Acqua Vergine ›Acqua Vergine/Acquedotto
              Vergine‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#348">Campo Marzio
              moderno, e sue adjacenze</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#368">Capo XI e XII. Monte
          ›Quirinale‹ e ›Viminale‹</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#368">Bagni di Paolo
              Emilio ›Bagni di Paolo Emilio‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#365">Tav. LI.
                  ›Bagni di Paolo Emilio‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#367">Tav. LII.
                  ›Bagni di Paolo Emilio‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#368">Avanzi del Tempio
              del Sole nel Giardino Colonna ›Tempio di
              Serapide‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#369">Obelisco e
              Cavalli antichi ›Fontana di Monte Cavallo‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#369">Obelisco della
              Trinità dei Monti ›Obelisco Sallustiano (Trinità de'
              Monti)‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#370">Circo di
              Sallustio ›Horti Sallustiani‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#371">Ponte Salaro
              ›Ponte Salario‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#372">Muasoleo di
              Costanza ›Mausoleo di Santa Costanza‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#372">Basilica di
              ›Sant'Agnese fuori le Mura‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#373">›Ponte
              Nomentano‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#373">›Monte Sacro‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#373">›Castra
              Praetoria‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#374">Porta inter
              aggeres ›Porta Chiusa‹ . Oggi San Lorenzo ›Porta
              Tiburtina‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#377">Aggere di Servio
              Tullio ›Argine di Tarquinio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#377">›Terme di
              Diocleziano‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#378">Terme di
              Olimpiade ›Thermae Olympiadis‹ Oggi San Lorenzo Pane,
              e Perna ›San Lorenzo in Panisperna‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#379">›Casa di
              Pompeo‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#379">Casa di Pudente
              &#160;Oggi ›Chiesa di Santa Pudenziana‹.</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#379">Vico Patrizio
              ›Vicus Patricius‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4504051a12s.html#380">Capo XIII. Monte
          ›Esquilino‹ e sue adjacenze</a>
          <ul>
            <li>
              <a href="dg4504051a12s.html#380">Obelisco di Santa
              Maria Maggiore ›Obelisco Esquilino‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#380">Colonna antica
              ›Colonna della Basilica di Massenzio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#381">Tempio di Diana
              ›Tempio di Diana sull'Esquilino‹ . Oggi Chiesa di
              Sant'Antonio Abbate ›Sant'Antonio Abate‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#381">›Trofei di
              Mario‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#381">›Tempio di
              Minerva Medica‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#382">Tav. XLIX.
                  Tempio detto Le Gallucce, e Minerva Medica
                  ›Tempio di Minerva Medica‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#384">Colombario della
              Famiglia Arrunzia ›Sepulcrum: Arruntii‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#385">Monumento
              dell'Acqua Claudia ›Castello dell'Acqua Claudia‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#386">Vivario
              ›Vivarium‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#386">Aggere di
              Tarquinio ›Argine di Tarquinio‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#387">›Arco di
              Gallieno‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#388">›Terme di
              Traiano‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#388">›Sette Sale‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#389">Palazzo e ›Terme
              di Tito‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#390">Tav. L.
                  Pianta delle ›Terme di Tito‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4504051a12s.html#392">Tempio di Pallade
              ›Tempio di Minerva (Foro di Nerva)‹</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#394">Tav. LIII.
              ›Tempio di Minerva (Foro di Nerva)‹ ▣</a>
            </li>
            <li>
              <a href="dg4504051a12s.html#393">Foro e Tempio di
              Nerva ›Foro di Nerva‹</a>
              <ul>
                <li>
                  <a href="dg4504051a12s.html#396">Tav. LIV.
                  ›Foro di Nerva‹ ▣</a>
                </li>
                <li>
                  <a href="dg4504051a12s.html#399">Tav. LV.
                  Tempio detto di Nerva ›Foro di Nerva‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4504051a12s.html#402">Indice delle cose
      notabili</a>
    </li>
  </ul>
  <hr />
</body>
</html>
