<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="eper3113840s.html#10">Guida al forestiere per
      l’augusta città di Perugia al quale si pongono in vista le
      più eccellenti pitture, sculture ed architetture con alcune
      osservazioni</a>
      <ul>
        <li>
          <a href="eper3113840s.html#12">Al nobil uomo il Signor
          Conte Giulio Cesarei</a>
        </li>
        <li>
          <a href="eper3113840s.html#14">L'autore a chi legge</a>
        </li>
        <li>
          <a href="eper3113840s.html#20">Descrizione di Porta San
          Pietro</a>
        </li>
        <li>
          <a href="eper3113840s.html#126">Descrizione di Porta
          Sant'Angelo</a>
        </li>
        <li>
          <a href="eper3113840s.html#210">Descrizione di Porta
          Sole</a>
        </li>
        <li>
          <a href="eper3113840s.html#288">Descrizione di Porta
          Santa Susanna</a>
        </li>
        <li>
          <a href="eper3113840s.html#350">Descrizone di Porta
          Eburnea</a>
        </li>
        <li>
          <a href="eper3113840s.html#374">Indice delle Chiese, ed
          Oratorj, e degli Edifizj Pubblici, e Privati</a>
        </li>
        <li>
          <a href="eper3113840s.html#379">Indice de' Pittori,
          Scultori, Architetti [...]</a>
        </li>
        <li>
          <a href="eper3113840s.html#392">Aggiunta</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
