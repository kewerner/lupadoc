<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katpcol55213830s.html#6">Catalogo dei quadri, e
      pitture esistenti nel palazzo dell’eccellentissima casa
      Colonna in Roma</a>
      <ul>
        <li>
          <a href="katpcol55213830s.html#8">I. Quadri, e pitture
          degli appartamenti terreni</a>
        </li>
        <li>
          <a href="katpcol55213830s.html#22">II. Appartamento
          nobile, e Galleria&#160;</a>
        </li>
        <li>
          <a href="katpcol55213830s.html#39">III. Secondo
          appartamento nobile e terzo piano superiore</a>
        </li>
        <li>
          <a href="katpcol55213830s.html#85">IV. Appartamento
          nobile verso la chiesa de SS. XII. Apostoli</a>
        </li>
        <li>
          <a href="katpcol55213830s.html#107">V. Appartamento
          nobile verso la strada detta la Pilotta&#160;</a>
        </li>
        <li>
          <a href="katpcol55213830s.html#155">VI. Palazetto alla
          Pilotta di tre piani</a>
        </li>
        <li>
          <a href="katpcol55213830s.html#182">Indice
          alfabetico</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
