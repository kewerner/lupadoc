<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zf47302640s.html#6">Delle Lodi del Commendatore
      Cassiano dal Pozzo</a>
      <ul>
        <li>
          <a href="zf47302640s.html#8">[Dedica] Agli Amatori della
          Virtu</a>
        </li>
        <li>
          <a href="zf47302640s.html#9">[Dedica di Ezechiel
          Spanhemius all'autore] Clarissimo viro, Carolo Dati
          [...]</a>
        </li>
        <li>
          <a href="zf47302640s.html#10">[Ritratto] Eques Cassianus
          a Puteo [...] ▣</a>
        </li>
        <li>
          <a href="zf47302640s.html#12">Synopsis, atque Ordo
          Antiquitatum Romanarum [...] Cassiani a puteo studio, ac
          impensis xxiii voluminibus digestarum</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="zf47302640s.html#14">[Text]</a>
    </li>
    <li>
      <a href="zf47302640s.html#74">[Dedica di Valerius
      Chimentellius Florentinus &#160;a Cassiano dal Pozzo]
      Illustrissimus eques [...]</a>
    </li>
    <li>
      <a href="zf47302640s.html#75">[Dedica dell'autore a Cassiano
      dal Pozzo] lllustrissimi equitis [...]</a>
    </li>
  </ul>
  <hr />
</body>
</html>
