<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghsan15952750112s.html#12">L’ academia todesca
      della architectura, scultura &amp; pittura oder Teutsche
      Academie der edlen Bau-, Bild- und Mahlerey-Künste; Darinn
      enthalten ein gründlicher Unterricht von dieser dreyer Künste
      Eigenschaft, Lehr-Sätzen und Geheimnissen</a>
      <ul>
        <li>
          <a href="ghsan15952750112s.html#18">Der Teutschen
          Academie ersten Theils erstes Buch von der Architectur
          oder Bau-Kunst&#160;</a>
        </li>
        <li>
          <a href="ghsan15952750112s.html#122">Der Teutschen
          Academie ersten Theils zweytes Buch von der Scultura oder
          Bildereykunst&#160;</a>
        </li>
        <li>
          <a href="ghsan15952750112s.html#234">Der Teutschen
          Academie ersten Theils drittes Buch von der Pittura oder
          Mahleren-Kunst&#160;</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghsan15952750112s.html#290">Der Teutschen
      Academie zweyter Theil von der alt- und neu-
      berühmtenEgyptischen/ Griechischen/
      Römischen/Italienischen/Hoch- und Niederteutschen Bau- Bild-
      und Mahlern Künstlere&#160;</a>
      <ul>
        <li>
          <a href="ghsan15952750112s.html#294">Der Teutschen
          Academie zweyten Theils erstes Buch von der
          ur-alt-berühmten Egyptischen/Griechischen und Römischen
          Ersten Kunst-Mahlere&#160;</a>
        </li>
        <li>
          <a href="ghsan15952750112s.html#362">Der Teutschen
          Academie andern Theils zweytes Buch: von der mordernen
          berühmten Italienischen Mahlere/ Bildhauere/ und
          Maumeistere&#160;</a>
        </li>
        <li>
          <a href="ghsan15952750112s.html#540">Der Teutschen
          Academie andern Theils drittes Buch: von der Hoch-und
          Nieder-Teutschen berühmten Mahler/ Bildhauer und
          Baumeister&#160;</a>
        </li>
        <li>
          <a href="ghsan15952750112s.html#736">Register aller
          berühmten Künstler und anderer merkwürdigen Dinge die in
          diesen zweyen Theilen der Deutschen Acadmie enthalten</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghsan15952750112s.html#748">Lebenslauf und
      Kunst-Werke des Woledlen und Gestrengen Herrn Joachims von
      Sandrart</a>
    </li>
  </ul>
  <hr />
</body>
</html>
