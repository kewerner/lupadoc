<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlom5391840s.html#6">Trattato Dell’Arte De La
      Pittvra</a>
      <ul>
        <li>
          <a href="ghlom5391840s.html#8">Al serenissimo principe
          Don Carlo Emanuello</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#10">Dilecto silio Io. Paulo
          Lomatio Mediolanensi&#160;</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#20">Tavola de i
          capitoli&#160;</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#28">Tavola de le più
          eccellenti opere, di pitture, e di scoltura</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#46">Proemio de l'opera</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#58">Divisione di tutta
          l'opera</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#62">I. De la proportione
          naturale, et artificiale de le cose</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#150">II. Del sito,
          positione, decoro, moto, furia, e gratia delle figure</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#232">III. Del colore</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#256">IV. De i lumi</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#290">V. Della
          prospettiva</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#322">VI. De la prattica,
          della pittura</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#574">VII. Dell'historia di
          pittura</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#728">Tavola de i nomi de
          gl'artefici più illustri cosi antichi come moderni</a>
        </li>
        <li>
          <a href="ghlom5391840s.html#743">Tavola de i nomi de
          gl'Autori citati nell'opera</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
