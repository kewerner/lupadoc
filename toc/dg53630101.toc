<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg53630101s.html#8">Stvdio D’Architettvra Civile
      .... &#160;</a>
      <ul>
        <li>
          <a href="dg53630101s.html#8">Sopra gli Ornamenti di
          Porte e Finestre tratti da alcune Fabbriche insigni di
          Roma con le Misure Piante Modini, e Profili</a>
          <ul>
            <li>
              <a href="dg53630101s.html#10">Beatissimo Padre</a>
            </li>
            <li>
              <a href="dg53630101s.html#12">Agli Amatori delle
              belle arti</a>
            </li>
            <li>
              <a href="dg53630101s.html#14">[Tavole]</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
