<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy1403480s.html#8">Memorie istoriche della gran
      cupola del Tempio Vaticano ›San Pietro in Vaticano: Cupola‹,
      e de' danni di essa, e de' ristoramenti loro, divise in libri
      cinque [...]</a>
      <ul>
        <li>
          <a href="dy1403480s.html#10">Alla Santità di Nostro
          Signore Papa Benedetto XIV. [Dedica dell'autore]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dy1403480s.html#14">Libro primo</a>
      <ul>
        <li>
          <a href="dy1403480s.html#14">Prefazione</a>
        </li>
        <li>
          <a href="dy1403480s.html#15">I. Fondatore primo della
          vecchia Basilica di ›San Pietro in Vaticano‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#17">Tavola A. ›San Pietro in
          Vaticano‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#19">II. Anno della fondazione
          della vecchia Basilica di ›San Pietro in Vaticano‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#21">Tavola B. ›San Pietro in
          Vaticano‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#22">III. Sito della vecchia
          Basilica di ›San Pietro in Vaticano‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#24">IV. Della figura, e misure
          della vecchia Basilica di ›San Pietro in Vaticano‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#24">V. Della demolizione della
          vecchia Basilica ›San Pietro in Vaticano‹ per costruire
          la nuova</a>
        </li>
        <li>
          <a href="dy1403480s.html#25">VI. Della costruzione della
          nuova Basilica ›San Pietro in Vaticano‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#26">VII. Di alcune
          particolarità da notarsi nella fabbrica della cupola ›San
          Pietro in Vaticano: Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#31">Tavola C. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#34">VIII. Della figura degli
          archi, ed in universale della vera regolar figura delle
          cupole</a>
        </li>
        <li>
          <a href="dy1403480s.html#37">Tavola D. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#43">Tavola E. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#44">IX. Della figura della
          cupola di San Pietro ›San Pietro in Vaticano: Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#49">X. Della natura, e degli
          accidenti d'alcuni materiali</a>
        </li>
        <li>
          <a href="dy1403480s.html#57">XI. Dell'union delle pietre
          causata dalle frizioni</a>
        </li>
        <li>
          <a href="dy1403480s.html#58">XII. Dell'opera degli
          artefici nella fabbrica della cupola ›San Pietro in
          Vaticano: Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#58">XIII. Degli accidenti da
          considerarsi nelle fabbriche</a>
        </li>
        <li>
          <a href="dy1403480s.html#61">XIV. Delle cause esterne
          de' danni nelle fabbriche</a>
        </li>
        <li>
          <a href="dy1403480s.html#62">XV. Delle resistenze de'
          cerchioni di ferro da cignere le cupole</a>
        </li>
        <li>
          <a href="dy1403480s.html#65">Tavola F. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#70">XVI. Degli adattamenti de'
          cerchioni in opera</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dy1403480s.html#74">Libro secondo</a>
      <ul>
        <li>
          <a href="dy1403480s.html#74">Prefazione</a>
        </li>
        <li>
          <a href="dy1403480s.html#75">XVII. De' difetti delle
          cupole di Padova</a>
        </li>
        <li>
          <a href="dy1403480s.html#76">XVIII. De' difetti delle
          cupole del Duomo di Firenze</a>
        </li>
        <li>
          <a href="dy1403480s.html#80">XIX. De' difetti della
          cupola del Duomo di Montefiascone</a>
        </li>
        <li>
          <a href="dy1403480s.html#81">XX. De' difetti della
          cupola di San Marco di Venezia</a>
        </li>
        <li>
          <a href="dy1403480s.html#82">XXI. De' primi difetti
          nella cupola vaticana ›San Pietro in Vaticano:
          Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#82">XXII. De' secondi difetti
          nel Tempio Vaticano ›San Pietro in Vaticano‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#83">XXIII. De' terzi difetti
          nella cupola vaticana ›San Pietro in Vaticano:
          Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#85">Tavola G. Disegno di uno
          delli quattro Pilastroni dove impostano li quattro
          Arconi, che sostengono la Cupola della Chiesa di ›San
          Pietro in Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#87">XXIV. Del principio de'
          romori de' quarti difetti nella cupola vaticana ›San
          Pietro in Vaticano: Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#88">XXV. Dell'incremento de'
          romori de' quarti difetti nella cupola vaticana ›San
          Pietro in Vaticano: Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#88">XXVI. Della cura
          zelantissima di Sua Santità per la cupola ›San Pietro in
          Vaticano: Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#89">XXVII. Delle prime
          scritture escite intorno i danni, e rimedii della
          cupola</a>
        </li>
        <li>
          <a href="dy1403480s.html#90">XXVIII. Delle congregazioni
          per la cupola ›San Pietro in Vaticano: Cupola‹ tenute in
          Roma</a>
        </li>
        <li>
          <a href="dy1403480s.html#91">XXIX. D'altre scritture
          escite intorno i danni, ed i rimedii della cupola ›San
          Pietro in Vaticano: Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#93">XXX. Viaggio dell'autore a
          Roma</a>
        </li>
        <li>
          <a href="dy1403480s.html#94">XXXI. Altre nuove scritture
          intorno le cose della cupola ›San Pietro in Vaticano:
          Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#95">XXXII. Delle diligenti
          visitazioni della cupola ›San Pietro in Vaticano:
          Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#96">XXXIII. D'una seconda
          scrittura dell'autore</a>
        </li>
        <li>
          <a href="dy1403480s.html#96">XXXIV. Delle beneficenze di
          Sua Santità verso l'autore</a>
        </li>
        <li>
          <a href="dy1403480s.html#96">XXXV. Delle commissioni,
          che l'autor ricevette, e della partenza sua da Roma</a>
        </li>
        <li>
          <a href="dy1403480s.html#97">XXXVI. Di queste memorie
          istoriche, e dello stato de' difetti inseritovi</a>
        </li>
        <li>
          <a href="dy1403480s.html#97">XXXVII. Siegue l'opera
          dello stato de' difetti</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dy1403480s.html#98">Stato de' difetti da
      considerarsi nella cupola di San Pietro in Vaticano ›San
      Pietro in Vaticano: Cupola‹, rilevato da Giovanni Poleni nel
      maggio dell'anno MDCCXLIII, e presentato alla Santità di
      Nostro Signore Papa Benedetto XIV.</a>
      <ul>
        <li>
          <a href="dy1403480s.html#98">Proemio</a>
        </li>
        <li>
          <a href="dy1403480s.html#100">Tavola I. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
          <ul>
            <li>
              <a href="dy1403480s.html#99">Indice dell'Elevazione
              della Cupola</a>
            </li>
            <li>
              <a href="dy1403480s.html#99">Indice delle Fessure
              nel Piedestallo E de' Contrafforti, e nel Zoccolone
              C</a>
            </li>
            <li>
              <a href="dy1403480s.html#99">Indice delle Spaccature
              nelle Parti delle Fenestre</a>
            </li>
            <li>
              <a href="dy1403480s.html#99">Indice delle Fessure
              nell'Attico Esterno</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy1403480s.html#102">Tavola II. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
          <ul>
            <li>
              <a href="dy1403480s.html#101">Indice delle Fessure
              nel Piedestallo E de' Contrafforti, e nel Zoccolone
              C</a>
            </li>
            <li>
              <a href="dy1403480s.html#101">Indice delle
              Spaccature nelle Parti delle Fenestre</a>
            </li>
            <li>
              <a href="dy1403480s.html#101">Indice delle Fessure
              nell'Attico esterno</a>
            </li>
            <li>
              <a href="dy1403480s.html#101">Nota delle
              Inclinazioni, o Deviazioni dal Perpendicolo, delli
              Contrafforti, e della Muraglia esterna del
              Tamburo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy1403480s.html#104">Tavola III. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
          <ul>
            <li>
              <a href="dy1403480s.html#103">Indice delli
              Contrafforti, e Parti aggiacenti</a>
            </li>
            <li>
              <a href="dy1403480s.html#103">Indicazione delle cose
              appartenenti alle Fessure de' Contrafforti</a>
            </li>
            <li>
              <a href="dy1403480s.html#103">Spostamenti de'
              Travertini, che producono alcuni (come dicono) Denti
              negli Archetti delle Porticelle A nel sodo tra l'uno,
              e l'altro Pilastro</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy1403480s.html#106">Tavola IV. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#108">Tavola V. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#110">Tavola VI. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#112">Tavola VII. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#114">Tavola VIII. ›San Pietro
          in Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#116">Tavola IX. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#118">Tavola X. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#120">Tavola XI. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
          <ul>
            <li>
              <a href="dy1403480s.html#119">Indice della Pianta
              della Cupola</a>
            </li>
            <li>
              <a href="dy1403480s.html#119">Indice delle Fessure,
              che sono nel Corridore S circolare interno sotto li
              Contrafforti. Medesimamente nelle Tavole de'
              Contrafforti questo Corridore è segnato con la
              lettera S</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy1403480s.html#122">Tavola XII. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
          <ul>
            <li>
              <a href="dy1403480s.html#121">Indice delli Difetti
              delle quattro Scalette a Lumaca, segnate nella Pianta
              (Tavola XI.) con le lettere T. V. X. Y.</a>
            </li>
            <li>
              <a href="dy1403480s.html#121">Scaletta sopra il
              Pilone della Veronica. Corrispondente al Contrafforte
              III. segnata con la lettera T.</a>
            </li>
            <li>
              <a href="dy1403480s.html#121">Difetti delli
              Scalini</a>
            </li>
            <li>
              <a href="dy1403480s.html#121">Scaletta sopra il
              Pilone di Sant'Andrea. Corrispondente al Contrafforte
              VII. segnata con la lettera V.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy1403480s.html#124">Tavola XIII. ›San Pietro
          in Vaticano: Cupola‹ ▣</a>
          <ul>
            <li>
              <a href="dy1403480s.html#123">Scaletta sopra il
              Pilone di San Longino. Corrispondente al Contrafforte
              XI. segnata con la lettera X.</a>
            </li>
            <li>
              <a href="dy1403480s.html#123">Difetti delli
              Scalini</a>
            </li>
            <li>
              <a href="dy1403480s.html#123">Scaletta sopra il
              Pilone di Sant'Elena. Corrispondente al Contrafforte
              XV. segnata con la lettera Y.</a>
            </li>
            <li>
              <a href="dy1403480s.html#123">Difetti delli
              Scalini</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy1403480s.html#126">Tavola XIV. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
          <ul>
            <li>
              <a href="dy1403480s.html#125">Indice delle Fessure
              negli Arconi, o sovraposte Parti</a>
            </li>
            <li>
              <a href="dy1403480s.html#125">[I.] Arcone della
              Cattedra</a>
            </li>
            <li>
              <a href="dy1403480s.html#125">[II.] Arcone de' Santi
              Simone, e Giuda</a>
            </li>
            <li>
              <a href="dy1403480s.html#125">[III.] Arcone della
              Navata Grande</a>
            </li>
            <li>
              <a href="dy1403480s.html#125">[IV.] Arcone de' Santi
              Processo, e Martiniano</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy1403480s.html#128">Tavola XV. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
          <ul>
            <li>
              <a href="dy1403480s.html#127">Indice dello Spaccato
              della Cupola</a>
            </li>
            <li>
              <a href="dy1403480s.html#127">Indice delle Fessure
              nella parte interiore della Cupola</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy1403480s.html#130">Tavola XVI. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
          <ul>
            <li>
              <a href="dy1403480s.html#129">Indice delle Fessure
              nella parte interiore della Cupola</a>
            </li>
            <li>
              <a href="dy1403480s.html#129">Nota delle
              Inclinazioni, o Deviazioni dal Perpendicolo della
              Muraglia interna del Tamburo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy1403480s.html#132">Tavola XVII. ›San Pietro
          in Vaticano: Cupola‹ ▣</a>
          <ul>
            <li>
              <a href="dy1403480s.html#131">Indice delle Parti
              delle estremità superiori de' Costoloni, su le quali
              s'innalza la Lanterna, o sia Cupolino</a>
            </li>
            <li>
              <a href="dy1403480s.html#131">Indice de' Difetti
              delle estremità superiori de' Costoloni</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy1403480s.html#134">Tavola XVIII. ›San Pietro
          in Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#136">Tavola XIX. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dy1403480s.html#138">Libro terzo</a>
      <ul>
        <li>
          <a href="dy1403480s.html#138">Prefazione</a>
        </li>
        <li>
          <a href="dy1403480s.html#139">XXXVIII. Discorso di Don
          Saverio Brunetti. Manoscritto</a>
        </li>
        <li>
          <a href="dy1403480s.html#141">XXXIX. Copia d'una lettera
          di Diofanio Pastor Arcade. Manoscritta</a>
        </li>
        <li>
          <a href="dy1403480s.html#144">XL. Parere di tre
          matematici. Stampato</a>
        </li>
        <li>
          <a href="dy1403480s.html#147">Tavola H. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
        <li>
          <a href="dy1403480s.html#152">XLI. Riflessioni di Lelio
          Cosatti. Stampate</a>
        </li>
        <li>
          <a href="dy1403480s.html#154">XLII. Padre Santini.
          Risoluzione del dubbio. Stampata</a>
        </li>
        <li>
          <a href="dy1403480s.html#157">XLIII. Lettera del Signor
          N.N. Stampata</a>
        </li>
        <li>
          <a href="dy1403480s.html#159">XLIV. Osservazioni del
          Padre Abate Revillas. Manoscritte</a>
        </li>
        <li>
          <a href="dy1403480s.html#160">XLV. Riflessioni di tre
          matematici. Stampate</a>
        </li>
        <li>
          <a href="dy1403480s.html#168">XLVI. Sentimenti d'un
          filosofo. Manoscritti</a>
        </li>
        <li>
          <a href="dy1403480s.html#169">XLVII. Scrittura del
          Marchese Theodoli etcaetera. Manoscritta</a>
        </li>
        <li>
          <a href="dy1403480s.html#170">XLVIII. Riflessioni di
          Giovanni Poleni. Manoscritte</a>
        </li>
        <li>
          <a href="dy1403480s.html#175">XLIX. Aggiunta alle
          riflessioni di Lelio Cosatti. Stampata</a>
        </li>
        <li>
          <a href="dy1403480s.html#177">L. Scrittura di Gabbriello
          [sic] Manfredi. Manoscritta</a>
        </li>
        <li>
          <a href="dy1403480s.html#179">LI. Scrittura de'
          matematici di Napoli. Manoscritta</a>
        </li>
        <li>
          <a href="dy1403480s.html#181">LII. Lettera di Niccolò
          Ricciolini. Manoscritta</a>
        </li>
        <li>
          <a href="dy1403480s.html#182">LIII. Scrittura di un
          cavaliere. Manoscritta</a>
        </li>
        <li>
          <a href="dy1403480s.html#186">LIV. Discorso di N.N. capo
          mastro muratore. Manoscritto</a>
        </li>
        <li>
          <a href="dy1403480s.html#190">LV. Riflessioni del Padre
          Abate Revillas. Manoscritte</a>
        </li>
        <li>
          <a href="dy1403480s.html#191">LVI. Aggiunta alle
          riflessioni di Giovanni Poleni. Manoscritta</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dy1403480s.html#196">Libro quarto</a>
      <ul>
        <li>
          <a href="dy1403480s.html#196">Prefazione</a>
        </li>
        <li>
          <a href="dy1403480s.html#197">LVII. Breve parere di N.N.
          Manoscritto</a>
        </li>
        <li>
          <a href="dy1403480s.html#199">LVIII. Scrittura del Conte
          Giovanni Rizzetti. Stampata</a>
        </li>
        <li>
          <a href="dy1403480s.html#201">LIX. Sentimenti d'un
          filosofo Stampati</a>
        </li>
        <li>
          <a href="dy1403480s.html#213">LX. Sentimento di Gaetano
          Chiavery. Stampato</a>
        </li>
        <li>
          <a href="dy1403480s.html#215">LXI. Osservazioni sulle
          precedenti scritture</a>
        </li>
        <li>
          <a href="dy1403480s.html#216">LXII. De' sistemi da non
          ammettersi</a>
        </li>
        <li>
          <a href="dy1403480s.html#218">LXIII. Degli strappiombi
          [sic]</a>
        </li>
        <li>
          <a href="dy1403480s.html#222">LXIV. Delle vere cause
          interne de' danni della cupola vaticana ›San Pietro in
          Vaticano: Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#226">LXV. Delle vere cause
          esterne de' danni della cupola vaticana ›San Pietro in
          Vaticano: Cupola‹</a>
        </li>
        <li>
          <a href="dy1403480s.html#229">LXVI. De' rimedj, che sono
          stati riputati da non adoperarsi</a>
        </li>
        <li>
          <a href="dy1403480s.html#230">LXVII. De' rimedj, che
          sono stati riputati da adoperarsi</a>
        </li>
        <li>
          <a href="dy1403480s.html#234">LXVIII. Dell'esecuzione
          de' rimedj, e come fu compiuta</a>
        </li>
        <li>
          <a href="dy1403480s.html#239">Tavola K. ›San Pietro in
          Vaticano: Cupola‹ ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dy1403480s.html#244">Libro quinto</a>
      <ul>
        <li>
          <a href="dy1403480s.html#244">Prefazione</a>
        </li>
        <li>
          <a href="dy1403480s.html#244">LXIX. Della prima rottura
          scoperta in uno de' due vecchj cerchioni</a>
        </li>
        <li>
          <a href="dy1403480s.html#246">LXX. Della seconda rottura
          scoperta nel mentovato cerchione</a>
        </li>
        <li>
          <a href="dy1403480s.html#248">LXXI. D'alcune lettere
          spettanti alle rotture del predetto cerchione, ed a'
          rimedj di que' scoperti danni</a>
        </li>
        <li>
          <a href="dy1403480s.html#251">LXXII. D'alcune
          considerazioni sopra le due scoperte rotture del
          cerchione</a>
        </li>
        <li>
          <a href="dy1403480s.html#252">LXXIII. Delle rotture del
          gran cerchio, e delle più probabili cagioni di esse</a>
        </li>
        <li>
          <a href="dy1403480s.html#255">LXXIV. Della rottura o
          interezza dell'inferiore vecchio cerchione, e de'
          convenienti rimedj</a>
        </li>
        <li>
          <a href="dy1403480s.html#257">LXXV. Della buona
          opinione, che, non ostante le scoperte rotture, si dee
          avere dell'uso de' cerchj, e della sussistenza della
          cupola</a>
        </li>
        <li>
          <a href="dy1403480s.html#258">LXXVI. Del compimento de'
          rimedj agli ultimi scoperti danni</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dy1403480s.html#261">Indice degli autori, e d'altri
      nominati</a>
    </li>
    <li>
      <a href="dy1403480s.html#263">Indice di cose</a>
    </li>
  </ul>
  <hr />
</body>
</html>
