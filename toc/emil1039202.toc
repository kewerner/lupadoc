<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="emil1039202s.html#6">Delle Antichità
      Longobardico-Milanesi Illustrate Con Dissertazioni Dai Monaci
      Della Congregazione Cisterciese Di Lombardia; Tomo II.</a>
      <ul>
        <li>
          <a href="emil1039202s.html#8">Prefazione</a>
        </li>
        <li>
          <a href="emil1039202s.html#13">Indice delle
          Dissertazioni</a>
        </li>
        <li>
          <a href="emil1039202s.html#14">XI. Sopra la spedizione
          di Federigo I. Imperadore contro i Milanesi</a>
        </li>
        <li>
          <a href="emil1039202s.html#112">XII. Sopra i due
          Navili dal Tessino e dall'Adda condotti a questa
          città</a>
        </li>
        <li>
          <a href="emil1039202s.html#146">XIII. Sulla coltura
          delle campagne, e sull'irrigazione de' Prati promossa ed
          estesa dai Monaci di Chiaravalle&#160;</a>
        </li>
        <li>
          <a href="emil1039202s.html#160">XIV. Sull'antico Brolo
          e Broletto di Milano</a>
        </li>
        <li>
          <a href="emil1039202s.html#174">XV. Sui templari
          sull'autore cioè della loro regola, e sulla cagione del
          loro distruggimento&#160;</a>
        </li>
        <li>
          <a href="emil1039202s.html#252">XVI. Sui campi e le
          diete di Roncaglia</a>
        </li>
        <li>
          <a href="emil1039202s.html#266">XVII. Sulla zecca del
          Borgo di Noceto, sulle monete, denominate imperiali,
          sulle terzole, ed altre antiche Milanesi&#160;</a>
        </li>
        <li>
          <a href="emil1039202s.html#293">XVIII. Sul
          carroccio</a>
        </li>
        <li>
          <a href="emil1039202s.html#301">XIX. Sopra alcune
          indecenti e ridicole maniere, usate una volta dai
          pincitori con vinti nemici</a>
        </li>
        <li>
          <a href="emil1039202s.html#316">XX. Sopra le antiche
          case del lavoro e gli antichi spedali di Milano</a>
        </li>
        <li>
          <a href="emil1039202s.html#331">XXI. Sul governo
          politico delle italiane repubbliche, della milanese in
          specie, e sul successivo cambiamento di esse</a>
        </li>
        <li>
          <a href="emil1039202s.html#364">XXII. Intorno le
          formole e i riti nei tempi di mezzo praticati nelle
          investiture</a>
        </li>
        <li>
          <a href="emil1039202s.html#386">XXIII. Sulla
          manumissione dei servi ne' secoli di mezzo</a>
        </li>
        <li>
          <a href="emil1039202s.html#401">XXIV. Sul singolar
          privilegio dell'esenzione dal giuramento di calunnia di
          cui godevano i cisterciesi lombardi</a>
        </li>
        <li>
          <a href="emil1039202s.html#408">Indice delle
          materie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
