<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghpal5491395023s.html#8">El museo pictorico, y
      escala óptica. Práctica de la pintura</a>
      <ul>
        <li>
          <a href="ghpal5491395023s.html#10">Prólogo al
          lector</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#14">Tabla de los
          capitulos</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#18">IV. El
          principante, primer grado de los pintores</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#53">V. El copiante,
          segundo grado de los pintores</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#103">VI. El
          aprovechado, tercero grado de los pintores</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#138">VII. El inventor,
          quarto grado de los pintores</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#174">VIII. El
          practico, quinto grado de los pintores</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#212">IX. El perfecto,
          sexto, y ultimo grado de los pintores</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghpal5491395023s.html#362">El museo pictorico, y
      escala óptica. El parnaso español pintoresco
      laureado&#160;</a>
      <ul>
        <li>
          <a href="ghpal5491395023s.html#364">Preludio de esta
          obra</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#368">Noticias,
          elogios, y vidas de los pintores, y escultores eminentes
          espanoles</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#752">Indice de las
          cosas mas notables</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#762">Tabla de las
          figuras morales ó ideales</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#765">Tabla de los
          pintores y escultores</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#769">Table de los
          apellidos de los pintores</a>
        </li>
        <li>
          <a href="ghpal5491395023s.html#774">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
