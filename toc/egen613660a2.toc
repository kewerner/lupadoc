<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.4.0" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="egen613660a2s.html#4">Descrizione Delle Pitture,
      Scolture E Architetture Ecc. Che Trovansi In Alcune Città,
      Borghi, E Castelli Delle due Riviere dello Stato Ligure Qui
      disposti per ordine Alfabetico</a>
      <ul>
        <li>
          <a href="egen613660a2s.html#8">A Sua Eccellenza il
          Signor Girolamo Durazzo Patrizio Genovese</a>
        </li>
        <li>
          <a href="egen613660a2s.html#10">Ivone Gravier al
          Lettore</a>
        </li>
        <li>
          <a href="egen613660a2s.html#12">[Descrizione delle
          Pitture, Scolture e Archtietture ecc. che travansi in
          alcune città...]</a>
        </li>
        <li>
          <a href="egen613660a2s.html#66">Avviso al lettore</a>
        </li>
        <li>
          <a href="egen613660a2s.html#68">Dell'indole e costume
          de' Genovesi</a>
        </li>
        <li>
          <a href="egen613660a2s.html#83">Parrocchie della
          Diocesi dell'Arcivescivati di Genova</a>
        </li>
        <li>
          <a href="egen613660a2s.html#106">Imperatori romani
          genovesi</a>
        </li>
        <li>
          <a href="egen613660a2s.html#106">Dogi della
          serenissima Repubblica di Genova</a>
        </li>
        <li>
          <a href="egen613660a2s.html#149">Catologo de'
          Magistrati</a>
        </li>
        <li>
          <a href="egen613660a2s.html#160">Cronologia de' Sommi
          Pontefici</a>
        </li>
        <li>
          <a href="egen613660a2s.html#164">Catalogo de'
          Cardinali liguri</a>
        </li>
        <li>
          <a href="egen613660a2s.html#176">Catalogo de' Vescovi,
          e Arcivescovi di Genova</a>
        </li>
        <li>
          <a href="egen613660a2s.html#188">Catalogo de'
          Patriarchi, Arcivescovi, e Vescovi liguri</a>
        </li>
        <li>
          <a href="egen613660a2s.html#196">Catalogo de' Vescovi
          luguri</a>
        </li>
        <li>
          <a href="egen613660a2s.html#227">Catalogo de' Generali
          d'ordini religiosi</a>
        </li>
        <li>
          <a href="egen613660a2s.html#234">Catalogo de' Santi
          liguri&#160;</a>
        </li>
        <li>
          <a href="egen613660a2s.html#238">Catalogo de' Beati
          liguri</a>
        </li>
        <li>
          <a href="egen613660a2s.html#244">Catalogo de' Beati, e
          venerabili Genovesi</a>
        </li>
        <li>
          <a href="egen613660a2s.html#246">Notizie d'alcuni
          martiri genovesi</a>
        </li>
        <li>
          <a href="egen613660a2s.html#248">Serie de Scrittori
          d'annali, ed istorie di Genova</a>
        </li>
        <li>
          <a href="egen613660a2s.html#254">Dell'Uffizio ossia
          Casa di San Giorgio</a>
        </li>
        <li>
          <a href="egen613660a2s.html#267">Tavola</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
