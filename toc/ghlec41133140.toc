<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlec41133140s.html#8">[Planches]&#160;</a>
      <ul>
        <li>
          <a href="ghlec41133140s.html#8">Les ordres des
          colonnes&#160;</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#168">Les ordres des
          pilastres</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#248">Colonne torse</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#260">L'assemblage des
          ordres&#160;</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#302">Lucarnes</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#306">Fronton
          triangulaire</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#316">Niches</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#324">Balustrades</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#356">Ordre francois</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghlec41133140s.html#374">Traité d'architecture
      avec des remarques et des observations très utiles</a>
      <ul>
        <li>
          <a href="ghlec41133140s.html#376">Au lecteur</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#379">Privilege du Roy</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#382">I Pour servir
          d'introduction a L'etude des batiments</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#404">II Des ordres de
          colomnes&#160;</a>
          <ul>
            <li>
              <a href="ghlec41133140s.html#404">De l'ordre
              toscan</a>
            </li>
            <li>
              <a href="ghlec41133140s.html#416">D'un II. ordre
              toscan</a>
            </li>
            <li>
              <a href="ghlec41133140s.html#419">De l'ordre
              dorique</a>
            </li>
            <li>
              <a href="ghlec41133140s.html#434">De l'ordre
              ionique</a>
            </li>
            <li>
              <a href="ghlec41133140s.html#444">De l'ordre
              romain</a>
            </li>
            <li>
              <a href="ghlec41133140s.html#454">De l'ordre
              espagnol</a>
            </li>
            <li>
              <a href="ghlec41133140s.html#460">De l'ordre
              corinthien</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghlec41133140s.html#470">III Des
          pilastres&#160;</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#482">IV Des colonnes
          particulieres&#160;</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#500">V De l'assemblage des
          ordres&#160;</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#516">VI Des portes, des
          fenestres ou croisées, des frontons et des
          niches&#160;</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#548">VII Des balustrades,
          des balcons et des perons&#160;</a>
        </li>
        <li>
          <a href="ghlec41133140s.html#576">Tables des
          principales parties de ce traité</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
