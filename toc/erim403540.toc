<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="erim403540s.html#10">Pitture delle chiese di
      Rimino</a>
      <ul>
        <li>
          <a href="erim403540s.html#12">Lettera del Sig. Dottoro
          Giovanni Bianchi</a>
        </li>
        <li>
          <a href="erim403540s.html#18">Indice delle chiese, e
          d'altri luoghi</a>
        </li>
        <li>
          <a href="erim403540s.html#24">Descrizione delle pitture
          delle chiese di Rimino</a>
        </li>
        <li>
          <a href="erim403540s.html#82">Indice alfabetico, e
          cronologico</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
