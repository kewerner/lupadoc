<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="perb10038403s.html#6">Giornale delle belle arti e
      dell' antiquaria incisione, musica, e poesia</a>
      <ul>
        <li>
          <a href="perb10038403s.html#8">Eminentissomo, e
          Reverendissimo Principe</a>
        </li>
        <li>
          <a href="perb10038403s.html#14">Num. 1. li 7. Gennaro
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#22">Num. 2. li 14. Gennaro
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#30">Num. 3. li 21. Gennaro
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#38">Num. 4. li 28. Gennaro
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#46">Num. 5. li 4. Febbraro
          1786&#160;</a>
        </li>
        <li>
          <a href="perb10038403s.html#54">Num. 6. li 11. Febbraro
          1786&#160;</a>
        </li>
        <li>
          <a href="perb10038403s.html#62">Num. 7. li 18. Febbraro
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#70">Num. 8. li 25. Febbraro
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#78">Num. 9. li 4. Marzo
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#86">Num. 10. li 11. Marzo
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#94">Nu. 11. li 18. Marzo
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#102">Num. 12. li 25. Marzo
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#110">Num. 13. il 1. Aprile
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#118">Num. 14. li 8. Aprile
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#126">Num. 15. li 15. Aprile
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#134">Num. 16. li 22. Aprile
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#142">Num. 17. li 29. Aprile
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#150">Num. 18. li 6. Maggio
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#158">Num. 19. li 13. Maggio
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#166">Num. 20. li 20. Maggio
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#174">Num. 21. li 27. Maggio
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#182">Num. 22. li 3. Giugno
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#190">Num. 23. li 10. Giugno
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#198">Num. 24. li 17. Giugno
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#206">Num. 25. li 24. Giugno
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#214">Num. 26. il 1. Luglio
          1786&#160;</a>
        </li>
        <li>
          <a href="perb10038403s.html#222">Num. 27. il 8. Luglio
          1786&#160;</a>
        </li>
        <li>
          <a href="perb10038403s.html#230">Num. 28. il 15. Luglio
          1786&#160;</a>
        </li>
        <li>
          <a href="perb10038403s.html#238">Num. 29. il 11. Luglio
          1786&#160;</a>
        </li>
        <li>
          <a href="perb10038403s.html#246">Num. 30. il 29. Luglio
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#254">Num. 31. il 5. Agosto
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#262">Num. 32. il 12. Agosto
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#270">Num. 33. il 19. Agosto
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#278">Num. 34. il 26. Agosto
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#286">Num. 35. il 2.
          Settembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#294">Num. 36. il 9.
          Settembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#302">Num. 37. il 16.
          Settembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#310">Num. 38. li 23.
          Settembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#318">Num. 39. li 30.
          Settembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#326">Num. 40. li 7. Ottobre
          1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#334">Num. 41. li 14.
          Ottobre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#342">Num. 42. li. 21.
          Ottobre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#350">Num. 43. li 28.
          Ottobre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#358">Num. 44. li 4.
          Novembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#370">Num. 45. li 11.
          Novembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#378">Num. 46. li 18.
          Novembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#388">Num. 47. li 25.
          Novembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#396">Num. 48. li 2.
          Dicembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#404">Num. 49. li 9.
          Decembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#412">Num. 50. li 16.
          Decembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#420">Num. 51. li 23.
          Decembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#428">Num. 52. li 30.
          Decembre 1786</a>
        </li>
        <li>
          <a href="perb10038403s.html#436">Indice delle cose più
          notabili in questo Terzo Tomo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
