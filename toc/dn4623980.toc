<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dn4623980s.html#6">Palais, maisons et autres
      édifices modernes, dessinés à Rome</a>
      <ul>
        <li>
          <a href="dn4623980s.html#8">Discours préliminaire</a>
        </li>
        <li>
          <a href="dn4623980s.html#14">Avis des èditeurs</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dn4623980s.html#18">[Text]</a>
      <ul>
        <li>
          <a href="dn4623980s.html#18">Explication des planches
          contenues dans cet ouvrage</a>
          <ul>
            <li>
              <a href="dn4623980s.html#18">Vignette</a>
            </li>
            <li>
              <a href="dn4623980s.html#18">Premier cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#18">1. [›Palazzo Mattei
                  di Giove‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#19">2. Palais au coin
                  du Vicolo de' Balestrari et de la place de palais
                  Spada; petit palais dans rue Borgo Vecchio</a>
                </li>
                <li>
                  <a href="dn4623980s.html#19">3. [›Palazzo Muti
                  Bussi‹]; maison au bas du Capitole; [›Palazzo
                  Mattei di Paganica‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#19">4. [›Palazzo
                  Massimo alle Colonne‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#20">5. [›Palazzo
                  Pamphilj (Piazza Navona)‹]; [›Palazzo Muti
                  Bussi‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#20">6. [›Palazzo Del
                  Bufalo‹]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#21">Deuxième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#21">7. [Ipotesi di
                  ricostruzione] Fontaine composée de sarcophages,
                  chapiteaux, mascarons, et autre fragmens antiques
                  tirés de different palais</a>
                </li>
                <li>
                  <a href="dn4623980s.html#21">8. [›Palazzo
                  Sacchetti‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#21">9. [›Palazzo
                  Sacchetti‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#21">10. [›Palazzo
                  Cesi‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#22">11. [[›Palazzo
                  Gaddi‹], palais Colonna di Sonnino, aujourd'hui
                  Colonna-Stigliano dans ›Via de' Cesarini‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#22">12. Cour prés du
                  ›Palazzo Mattei di Giove‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#22">13. Cour voisine
                  prés du ›Pantheon‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#23">Troisième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#23">14. Voûte décorée
                  d'ornamens arabesques, tirés des appartements du
                  palais du Vatican</a>
                </li>
                <li>
                  <a href="dn4623980s.html#23">15. Palais Giraud
                  ›Palazzo Castellesi‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#23">16. [›Palazzo
                  Sciarra‹]; petite maison dans le Vicolo del
                  Governo; [›Palazzo Muti Papazzurri‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#24">17. Maison dans rue
                  du théâtre della Valle</a>
                </li>
                <li>
                  <a href="dn4623980s.html#24">18. Palais dans rue
                  dell'Orso; palais prés de la place du palais
                  Farnèse; palais Rita au Campo Marzo; palais
                  Gamberucci; [›Palazzo Maccarani‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#25">19. Cour voisine de
                  l'église de ›San Luigi dei Francesi‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#25">Quatrième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#25">20.Fragmens de
                  corniches, modillons et bases antiques [...]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#25">21. [›Palazzo
                  Giustiniani‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#25">22. Petit Palais
                  dans Vicolo dell'Aquila</a>
                </li>
                <li>
                  <a href="dn4623980s.html#25">23. Maison dans
                  Borgo di San Pietro; [›Palazzo Altemps‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#26">24. Palais dans
                  ›Via Giulia‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#26">25. Cour du palais
                  dans la planche précédente</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#26">Cinquième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#26">26. [Ipotesi di
                  ricostruzione] Fontaine composée de divers
                  fragmens [...]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#26">27. [›Palazzo Doria
                  Pamphilj‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#27">28. Palais Negroni,
                  autrefois Gonzaga, faisant l'angle de la rue de
                  Ripetta et de la place du collége Clémentin;
                  maison dans rue de' Chiavari</a>
                </li>
                <li>
                  <a href="dn4623980s.html#27">29. Maison in ›Via
                  Giulia‹; maison prés la place Fiammeta; palais en
                  face de la poste de Venise, rue delle Copelle</a>
                </li>
                <li>
                  <a href="dn4623980s.html#28">30. Petit Palais
                  dans ›Vicolo della Pedacchia‹; palais degli Atti
                  dans ›Via Giulia‹; maison voisine de l'église
                  ›Santa Maria in Campitelli‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#28">31. Maison dans
                  ›Via Felice‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#28">Sixième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#28">32. Panneau
                  d'ornemens dans le genre arabesque, tiré de la
                  décoration d'une maison particulière</a>
                </li>
                <li>
                  <a href="dn4623980s.html#28">33. Palais Negroni,
                  autrefois Mattei, ›Via delle Botteghe Oscure‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#29">34. Palais Lanti,
                  place de' Caprettari; maison entre la rue de
                  Parione et l'église de Santa Maria in
                  Monterone</a>
                </li>
                <li>
                  <a href="dn4623980s.html#29">35. [›Palazzo
                  Ruspoli‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#29">36. Palais Ginnasi,
                  rue delle Botteghe oscure; palais Caoizucchi, en
                  face de l'église de Santa Maria in Campitelli</a>
                </li>
                <li>
                  <a href="dn4623980s.html#30">37. Clôitre de ›San
                  Pietro in Vincoli‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#30">Septième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#30">38. Vue d'une
                  partie de la gallerie du ›Palazzo Farnese‹
                  [...]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#30">39. [›Palazzo
                  Farnese‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#31">40. [›Palazzo
                  Farnese‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#31">41. [›Palazzo
                  Farnese‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#32">42. [›Palazzo
                  Farnese‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#32">43. [›Palazzo
                  Farnese‹]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#32">Huitième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#32">44. Chapiteaux,
                  vases, autels, bustes et fragmens antiques, tirés
                  du palais Lanti et autre lieux</a>
                </li>
                <li>
                  <a href="dn4623980s.html#32">45. Palais
                  Buon-Compagni, sur la ›Piazza Sora‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#32">46. Palais
                  Buon-Compagni ou Sora</a>
                </li>
                <li>
                  <a href="dn4623980s.html#33">47. Maison dans le
                  jardin Fini, prés le Colisée; hospice della
                  Mercede</a>
                </li>
                <li>
                  <a href="dn4623980s.html#33">48. Palais della
                  Curia Innocenziana sur la place de Monte
                  Citorio</a>
                </li>
                <li>
                  <a href="dn4623980s.html#33">49. Fontaine
                  derrière le ›Palazzo Apostolico Vaticano‹, dans
                  la cour della Panneteria</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#33">Neuviéme cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#33">50. Fragmens tirés
                  des palais et autres édifices de Rome [...]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#33">51. [›Palazzo della
                  Sapienza‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#34">52. [›Palazzo della
                  Sapienza‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#34">53. [›Palazzo della
                  Sapienza‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#34">54. [›Palazzo di
                  Venezia‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#35">55. Couvent des
                  Pères Pénitenciers de l'église de Saint
                  Pierre</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#35">Dixième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#35">56. Ornemens des
                  voûtes et plafonds du ›Palazzo Massimo alle
                  Colonne‹ [...]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#35">57. [›Palazzo
                  Lancellotti‹]; maison dans ›Strada di Borgo
                  nuovo‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#35">58. Maison dans
                  ›Via Giulia‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#35">59. [›Palazzo
                  Massimo alle Colonne‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#36">60. [›Palazzo
                  Massimo alle Colonne‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#36">61. [›Palazzo
                  Massimo alle Colonne‹]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#36">Onzième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#36">62. Frises, autels,
                  candelabres, cinéraires et autres antiquités
                  tirée de divers endroits [...]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#36">63. Palais
                  pontifical de Monte Cavallo</a>
                </li>
                <li>
                  <a href="dn4623980s.html#37">64. Palais
                  pontifical de Monte Cavallo</a>
                </li>
                <li>
                  <a href="dn4623980s.html#37">65. Palais
                  pontifical de Monte Cavallo</a>
                </li>
                <li>
                  <a href="dn4623980s.html#37">66. Palais
                  pontifical de Monte Cavallo</a>
                </li>
                <li>
                  <a href="dn4623980s.html#37">67. [›Palazzo
                  Lancellotti‹]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#38">Douzième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#38">68. Vases,
                  chapiteaux, frises, autels, bustes et autres
                  fragmens tirés de différens édifices [...]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#38">69. [›Palazzo
                  Spada‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#38">70. [›Palazzo
                  Spada‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#38">71. Palais prés
                  l'eglise du Gesù</a>
                </li>
                <li>
                  <a href="dn4623980s.html#39">72. Collége romain
                  et l'église de ›Sant'Ignazio‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#39">73. [›Palazzo
                  Mattei di Giove‹]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#39">Trezième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#39">74. [›Palazzo della
                  Cancelleria‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#40">75. [›Palazzo della
                  Cancelleria‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#40">76. [›Palazzo della
                  Cancelleria‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#40">77. [›Palazzo della
                  Cancelleria‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#41">78. Porte de
                  l'église de ›San Lorenzo in Damaso‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#41">79. [›Palazzo della
                  Cancelleria‹]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#41">Quatorzième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#41">80. [Ipotesi di
                  ricostruzione] Vue d'un portique orné de
                  statutes, [...]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#41">81. [›Palazzo
                  Barberini‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#42">82. [›Palazzo
                  Barberini‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#42">83. Maison hors de
                  la ›Porta del Popolo‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#42">84. [›Palazzo
                  Corsini‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#42">85. [›Palazzo
                  Corsini‹]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#42">Qiunzième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#42">86. [Ipotesi di
                  ricostruzione] Galerie decorée [...]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#43">87. [›Palazzo
                  Borghese‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#43">88. [›Palazzo
                  Borghese‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#43">89. Maison ou
                  casin, dit la Vigna di Papa Giulio; maison
                  particulière sur la place du palais Borghese</a>
                </li>
                <li>
                  <a href="dn4623980s.html#43">90. Porte de
                  l'église de San Giacomo de' Spagnoli ›Nostra
                  Signora del Sacro Cuore‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#44">91. Nouvelle entrée
                  du ›Musei Vaticani‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#44">Seizième cahier</a>
              <ul>
                <li>
                  <a href="dn4623980s.html#44">92. [Ipotesi di
                  ricostruzione] Intérieur d'une église [...]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#44">93. Couvent et
                  église ›San Clemente‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#45">94. [›Santi Nereo e
                  Achilleo‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#45">95. [›San
                  Martinello‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#46">96. Convent et
                  &#160;église de ›Santa Prassede‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#46">97. [›San
                  Pancrazio‹]</a>
                </li>
                <li>
                  <a href="dn4623980s.html#46">98. Porte de
                  l'église ›San Pietro in Montorio‹</a>
                </li>
                <li>
                  <a href="dn4623980s.html#46">99. [Facades du
                  plusieur basiliques et églises]</a>
                  <ul>
                    <li>
                      <a href="dn4623980s.html#46">›Santo Stefano
                      Rotondo‹</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">›San
                      Sebastiano‹</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">›San Lorenzo
                      fuori le Mura‹</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">›San Pietro in
                      Montorio‹</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">›Santa
                      Pudenziana‹</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">›Sant'Andrea
                      del Vignola‹</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">›San Saba‹</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">Oratorio di
                      Santa Catarina</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">›Santi Giovanni
                      e Paolo‹</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">›Santa Maria
                      dell'Anima‹</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">›San Pietro in
                      Vincoli‹</a>
                    </li>
                    <li>
                      <a href="dn4623980s.html#46">›Santa Maria in
                      Aracoeli‹</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dn4623980s.html#49">100. Interiéur de ›San
              Lorenzo fuori le Mura‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4623980s.html#50">Noms des principaux
          auteurs</a>
        </li>
        <li>
          <a href="dn4623980s.html#52">Table chronologique des
          architects</a>
        </li>
        <li>
          <a href="dn4623980s.html#54">Liste des souscripteurs</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dn4623980s.html#58">[Tavole]</a>
      <ul>
        <li>
          <a href="dn4623980s.html#58">1. [›Palazzo Mattei di
          Giove‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#60">2. Palais au coin du Vicolo
          de' Balestrari et de la place de palais Spada; petit
          palais dans rue Borgo Vecchio ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#62">3. [›Palazzo Muti Bussi‹];
          maison au bas du Capitole; [›Palazzo Mattei di Paganica‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#64">4. [›Palazzo Massimo alle
          Colonne‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#66">5. [›Palazzo Pamphilj
          (Piazza Navona)‹]; [›Palazzo Muti Bussi‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#68">6. [›Palazzo Del Bufalo‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#70">7. [Ipotesi di
          ricostruzione] Fontaine composée de sarcophages,
          chapiteaux, mascarons, et autre fragmens antiques tirés
          de different palais ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#72">8. [›Palazzo Sacchetti‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#74">9. [›Palazzo Sacchetti‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#76">10. [›Palazzo Cesi‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#78">11. [[›Palazzo Gaddi‹],
          palais Colonna di Sonnino, aujourd'hui Colonna-Stigliano
          dans ›Via de' Cesarini‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#80">12. Cour prés du ›Palazzo
          Mattei di Giove‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#80">13. Cour voisine prés du
          ›Pantheon‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#82">14. Voûte décorée
          d'ornamens arabesques, tirés des appartements du palais
          du Vatican ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#84">15. Palais Giraud ›Palazzo
          Castellesi‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#86">16. [›Palazzo Sciarra‹];
          petite maison dans le Vicolo del Governo; [›Palazzo Muti
          Papazzurri‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#88">17. Maison dans rue du
          théâtre della Valle ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#90">18. Palais dans rue
          dell'Orso; palais prés de la place du palais Farnèse;
          palais Rita au Campo Marzo; palais Gamberucci; [›Palazzo
          Maccarani‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#92">19. Cour voisine de
          l'église de ›San Luigi dei Francesi‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#94">20.Fragmens de corniches,
          modillons et bases antiques [...] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#96">21. [›Palazzo Giustiniani‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#98">22. Petit Palais dans
          Vicolo dell'Aquila ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#100">23. Maison dans Borgo di
          San Pietro; [›Palazzo Altemps‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#102">24. Palais dans ›Via
          Giulia‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#104">25. Cour du palais dans la
          planche précédente ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#106">26. [Ipotesi di
          ricostruzione] Fontaine composée de divers fragmens [...]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#108">27. [›Palazzo Doria
          Pamphilj‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#110">28. Palais Negroni,
          autrefois Gonzaga, faisant l'angle de la rue de Ripetta
          et de la place du collége Clémentin; maison dans rue de'
          Chiavari ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#112">29. Maison in ›Via
          Giulia‹; maison prés la place Fiammeta; palais en face de
          la poste de Venise, rue delle Copelle ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#114">30. Petit Palais dans
          ›Vicolo della Pedacchia‹; palais degli Atti dans ›Via
          Giulia‹; maison voisine de l'église ›Santa Maria in
          Campitelli‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#116">31. Maison dans ›Via
          Felice‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#118">32. Panneau d'ornemens
          dans le genre arabesque, tiré de la décoration d'une
          maison particulière ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#120">33. Palais Negroni,
          autrefois Mattei, ›Via delle Botteghe Oscure‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#122">34. Palais Lanti, place
          de' Caprettari; maison entre la rue de Parione et
          l'église de Santa Maria in Monterone ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#124">35. [›Palazzo Ruspoli‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#126">36. Palais Ginnasi, rue
          delle Botteghe oscure; palais Caoizucchi, en face de
          l'église de Santa Maria in Campitelli ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#128">37. Clôitre de ›San Pietro
          in Vincoli‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#130">38. Vue d'une partie de la
          gallerie du ›Palazzo Farnese‹ [...] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#132">39. [›Palazzo Farnese‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#134">40. [›Palazzo Farnese‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#136">41. [›Palazzo Farnese‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#138">42. [›Palazzo Farnese‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#140">43. [›Palazzo Farnese‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#142">44. Chapiteaux, vases,
          autels, bustes et fragmens antiques, tirés du palais
          Lanti et autre lieux ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#144">45. Palais Buon-Compagni,
          sur la ›Piazza Sora‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#146">46. Palais Buon-Compagni
          ou Sora ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#148">47. Maison dans le jardin
          Fini, prés le Colisée; hospice della Mercede ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#150">48. Palais della Curia
          Innocenziana sur la place de Monte Citorio ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#152">49. Fontaine derrière le
          ›Palazzo Apostolico Vaticano‹, dans la cour della
          Panneteria ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#154">50. Fragmens tirés des
          palais et autres édifices de Rome [...] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#156">51. [›Palazzo della
          Sapienza‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#158">52. [›Palazzo della
          Sapienza‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#160">53. [›Palazzo della
          Sapienza‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#162">54. [›Palazzo di Venezia‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#164">55. Couvent des Pères
          Pénitenciers de l'église de Saint Pierre ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#166">56. Ornemens des voûtes et
          plafonds du ›Palazzo Massimo alle Colonne‹ [...] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#168">57. [›Palazzo
          Lancellotti‹]; maison dans ›Strada di Borgo nuovo‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#170">58. Maison dans ›Via
          Giulia‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#172">59. [›Palazzo Massimo alle
          Colonne‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#174">60. [›Palazzo Massimo alle
          Colonne‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#176">61. [›Palazzo Massimo alle
          Colonne‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#178">62. Frises, autels,
          candelabres, cinéraires et autres antiquités tirée de
          divers endroits [...] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#180">63. Palais pontifical de
          Monte Cavallo ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#182">64. Palais pontifical de
          Monte Cavallo ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#184">65. Palais pontifical de
          Monte Cavallo ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#186">66. Palais pontifical de
          Monte Cavallo ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#188">67. [›Palazzo
          Lancellotti‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#190">68. Vases, chapiteaux,
          frises, autels, bustes et autres fragmens tirés de
          différens édifices [...] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#192">69. [›Palazzo Spada‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#194">70. [›Palazzo Spada‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#196">71. Palais prés l'eglise
          du Gesù ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#198">72. Collége romain et
          l'église de ›Sant'Ignazio‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#200">73. [›Palazzo Mattei di
          Giove‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#202">74. [›Palazzo della
          Cancelleria‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#204">75. [›Palazzo della
          Cancelleria‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#206">76. [›Palazzo della
          Cancelleria‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#208">77. [›Palazzo della
          Cancelleria‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#210">78. Porte de l'église de
          ›San Lorenzo in Damaso‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#212">79. [›Palazzo della
          Cancelleria‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#214">80. [Ipotesi di
          ricostruzione] Vue d'un portique orné de statutes, [...]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#216">81. [›Palazzo Barberini‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#218">82. [›Palazzo Barberini‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#220">83. Maison hors de la
          ›Porta del Popolo‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#222">84. [›Palazzo Corsini‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#224">85. [›Palazzo Corsini‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#226">86. [Ipotesi di
          ricostruzione] Galerie decorée [...] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#228">87. [›Palazzo Borghese‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#230">88. [›Palazzo Borghese‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#232">89. Maison ou casin, dit
          la Vigna di Papa Giulio; maison particulière sur la place
          du palais Borghese ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#234">90. Porte de l'église de
          San Giacomo de' Spagnoli ›Nostra Signora del Sacro Cuore‹
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#236">91. Nouvelle entrée du
          ›Musei Vaticani‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#238">92. [Ipotesi di
          ricostruzione] Intérieur d'une église [...] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#240">93. Couvent et église ›San
          Clemente‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#242">94. [›Santi Nereo e
          Achilleo‹] ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#244">95. [›San Martinello‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#246">96. Convent et
          &#160;église de ›Santa Prassede‹ ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#248">97. [›San Pancrazio‹]
          ▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#250">98. Porte de l'église ›San
          Pietro in Montorio‹▣</a>
        </li>
        <li>
          <a href="dn4623980s.html#252">99. [Facades du plusieur
          basiliques et églises] ▣</a>
          <ul>
            <li>
              <a href="dn4623980s.html#252">›Santo Stefano
              Rotondo‹ ▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">›San Sebastiano‹ ▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">›San Lorenzo fuori le
              Mura‹ ▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">›San Pietro in
              Montorio‹ ▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">›Santa Pudenziana‹
              ▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">›Sant'Andrea del
              Vignola‹ ▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">›San Saba‹ ▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">Oratorio di Santa
              Catarina &#160;▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">›Santi Giovanni e
              Paolo‹ ▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">›Santa Maria
              dell'Anima‹ ▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">›San Pietro in
              Vincoli‹ ▣</a>
            </li>
            <li>
              <a href="dn4623980s.html#252">›Santa Maria in
              Aracoeli‹ &#160;▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4623980s.html#254">100. Interiéur de ›San
          Lorenzo fuori le Mura‹ ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
