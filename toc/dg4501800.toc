<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501800s.html#6">Le antichità della città di
      Roma</a>
      <ul>
        <li>
          <a href="dg4501800s.html#8">All'illustrissimo et
          eccellentissimo Signor il Signor Don Francesco De' Medici
          Principe di Fiorenza, et di Siena Bernardo Gamucci</a>
        </li>
        <li>
          <a href="dg4501800s.html#14">Giovanni Varisco a'
          lettori</a>
        </li>
        <li>
          <a href="dg4501800s.html#16">[Tre sonetti offerti
          all'autore da Benedetto Varchi, Laura Battiferri
          Ammannati e Gherardo Spini]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501800s.html#18">Il primo libro della antichità
      della città di Roma</a>
      <ul>
        <li>
          <a href="dg4501800s.html#18">Del luogo dove fu edificata
          Roma, et del vario accrescimento d'essa, incominciando da
          Romulo</a>
          <ul>
            <li>
              <a href="dg4501800s.html#19">Sasso di Carmenta
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#21">Porta Carmentale, e
              Scelerata ›Porta Carmentalis‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#21">›Porta Pandana‹, Libera
              et Saturnia</a>
            </li>
            <li>
              <a href="dg4501800s.html#21">Porta Romana, o ›Porta
              Mugonia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#22">Porta Ianuale ›Ianus
              Geminus, aedes‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#24">›Aventino‹ per che non
              é congiunto con gli altri colli</a>
            </li>
            <li>
              <a href="dg4501800s.html#25">›Ponte Sublicio‹ da chi
              fatto</a>
            </li>
            <li>
              <a href="dg4501800s.html#25">›Tevere‹ fiume</a>
            </li>
            <li>
              <a href="dg4501800s.html#26">Tevere perchè così
              chiamato</a>
            </li>
            <li>
              <a href="dg4501800s.html#26">Tevere per qual cagione
              inonda la città di Roma</a>
            </li>
            <li>
              <a href="dg4501800s.html#27">Fossa dè Quiriti
              ›Fossae Quiritium‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#27">Vaticano da chi cinto
              di mura ›Mura Leonine‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#28">Mura di Roma da chi
              rinomate in diversi tempi</a>
            </li>
            <li>
              <a href="dg4501800s.html#29">Porte di Roma</a>
            </li>
            <li>
              <a href="dg4501800s.html#31">Nerone hebbe animo
              d'accrescer Roma fino al porto d'Hostia</a>
            </li>
            <li>
              <a href="dg4501800s.html#31">Vie pubbliche fuor
              delle porte di Roma, quali</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501800s.html#34">Del Colle del
          ›Campidoglio‹, prima detto Capitolino</a>
          <ul>
            <li>
              <a href="dg4501800s.html#34">Campidoglio perchè
              detto prima Colle Capitolino, Saturnio, o Tarpeo
              ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#34">›Rupe Tarpea‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#35">Campidoglio quanto
              superbamente fosse ornato da Domitiano Imperatore</a>
            </li>
            <li>
              <a href="dg4501800s.html#36">Via trionfale ›Via
              Triumphalis‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#37">›Tempio di Giove
              Feretrio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#39">›Chiodo annale‹ dove si
              ficcava</a>
            </li>
            <li>
              <a href="dg4501800s.html#39">Libri Sibillini dove si
              conservavano</a>
            </li>
            <li>
              <a href="dg4501800s.html#40">›Tempio di Giove
              Custode‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#41">›Curia Calabra‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#42">›Atrium Publicum in
              Capitolio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#44">Palazzo del Senatore in
              Campidoglio da chi edificato ›Palazzo Senatorio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#45">›Statua Equestre di
              Marco Aurelio‹ posta in Campidoglio</a>
            </li>
            <li>
              <a href="dg4501800s.html#45">›Statua di Ercole in
              bronzo dorato‹ in Campidoglio</a>
            </li>
            <li>
              <a href="dg4501800s.html#48">[Tavola] Campidoglio
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501800s.html#49">Del Foro Romano e de gli
          altri Fori et edificij che vi sono appresso</a>
          <ul>
            <li>
              <a href="dg4501800s.html#50">›Foro Romano‹
              dov'havesse il suo principio</a>
            </li>
            <li>
              <a href="dg4501800s.html#51">Comitio che cosa era
              ›Comitium‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#51">›Fico Ruminale‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#53">[Tavola] Foro Romano
              ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#54">Carcere Tulliano
              ›Carcere Mamertino‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#55">›Arco di Settimio
              Severo‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#56">[Tavola] Arco di
              Settimio ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#59">›Miliario Aureo‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#59">›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#60">›Nera (fiume)‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#61">Tempio di Marte nel
              Foro Romano ›Tempio di Marte Ultore‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#62">›Tempio di Saturno‹ nel
              Foro Romano</a>
            </li>
            <li>
              <a href="dg4501800s.html#64">Erario antico, dove
              ›Erario militare o del Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#64">›Tempio della
              Concordia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#65">Curia et Senatulo
              ›Senaculum‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#65">Basilica di Paolo
              Emilio ›Basilica Aemilia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#66">Tempio di Faustina
              ›Tempio di Antonino e Faustina‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#67">[Tavola. Tempio di
              Antonino e Faustina] ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#68">Tempio di Giano Ianus
              Geminus, aedes‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#69">Dolioli che cosa
              fossero ›Doliola‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#69">›Cloaca Maxima‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#70">Tempio di Giove Statore
              ›Tempio del Divo Romolo‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#71">[Tavola. Tempio di
              Giove Statore] ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#72">Rostri di bronzo
              ›Rostra‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#73">Tempio di Castore e
              Polluce ›Tempio dei Càstori‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#74">›Tempio di Augusto‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#75">Colonna di Cesare</a>
            </li>
            <li>
              <a href="dg4501800s.html#75">Comitio dove era
              ›Comitium‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#76">Tempio di Romolo e di
              Remo ›Tempio del Divo Romolo‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#78">Basilica di Porcio
              Catone ›Basilica Porcia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#82">[Tavola] Pianta del
              Tempio della Pace ›Foro della Pace‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#84">Tempio della Pace per
              che così detto ›Foro della Pace‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#85">[Tavola] Tempio della
              Pace ›Foro della Pace‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#86">Serapide et Iside che
              significassero</a>
            </li>
            <li>
              <a href="dg4501800s.html#87">Templi di Giove del
              Sole, et daltro perchè si facessero aperti di
              sopra</a>
            </li>
            <li>
              <a href="dg4501800s.html#88">›Via Sacra‹ dov'era, et
              perchè così chiamata</a>
            </li>
            <li>
              <a href="dg4501800s.html#89">›Arco di Tito‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#92">[Tavola] ›Arco di Tito‹
              &#160;▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#98">[Tavola] ›Arco di
              Costantino‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#100">Disegno dell'›Arco di
              Costantino‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#100">›Colosseo‹, o
              Anfiteatro di Vespasiano</a>
            </li>
            <li>
              <a href="dg4501800s.html#105">[Tavola] Anfiteatro,
              ›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#106">›Meta sudans‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#106">›Foro di Cesare‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#108">Pitture di Timomaco
              nel ›Tempio di Venere Genitrice‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#108">›Foro di Augusto‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#109">Pitture di Castore e
              di Polluce di mano d'Apelle nel foro d'Augusto</a>
            </li>
            <li>
              <a href="dg4501800s.html#110">›Foro di Nerva‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#113">[Tavola] ›Foro di
              Nerva‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#114">Foro di Traiano‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#116">[Tavola] ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#120">[Tavola] Colonna di
              Traiano ›Colonna Traiana‹ &#160;▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501800s.html#122">Del Colle Palatino</a>
          <ul>
            <li>
              <a href="dg4501800s.html#122">›Palatino‹ colle
              perchè fosse così chiamato</a>
            </li>
            <li>
              <a href="dg4501800s.html#124">Tempio della Vittoria
              edificato da Postumio Console ›Victoria, aedes‹</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501800s.html#127">Il secondo libro
      dell'antichità della città di Roma</a>
      <ul>
        <li>
          <a href="dg4501800s.html#127">Del Foro Olitorio et
          Boario, et di tutto quello che è restato nella valle, che
          è tra il Campidoglio et il Palatino</a>
          <ul>
            <li>
              <a href="dg4501800s.html#128">Vico Giogario perchè
              così detto ›Vico Jugario‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#135">›Foro Olitorio‹ cioè
              ›Piazza Montanara‹ et ›Argiletum‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#137">Tempio di Carmenta
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#137">Equimelio
              ›Aequimaelium‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#138">Colonna Lattaria
              ›Columna Lactaria‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#142">[Tavola] ›Teatro di
              Marcello‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#144">›Tempio della
              Pietà‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#145">Foro Piscario ›Forum
              Piscarium‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#147">[Tavola] ›Santa Maria
              Egiziaca‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#149">›Foro Boario‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#151">[Tavola] Arco de gli
              Orefici ›Arco degli Argentari‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#154">[Tavola] ›Arco di
              Giano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#155">Tempio d'Hercole, ove
              non entravano mosche, nè cani ›Tempio di Hercules
              Olivarius‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#156">›Ara Massima di
              Ercole‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#157">Tempio della Pudicitia
              nel Foro Boario ›Tempio della Pudicitia Patritia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#158">›Fonte di
              Giuturna‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#159">›Circo Massimo‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#163">›Acqua Crabra‹, et
              ›Acqua Appia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#166">›Casa di Pompeo‹
              Magno</a>
            </li>
            <li>
              <a href="dg4501800s.html#167">Settizonio di Severo
              Imperatore ›Septizodium (2)‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#170">[Tavola] Settizonio
              ›Septizodium (2)‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#173">›Porta Capena‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#174">›Chiesa del "Domine
              quo vadis"‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#175">Pietra manale›Lapis
              Manalis‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#176">›Testaccio
              (monte)‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#178">Sepoltura di caio
              Cestio ›Piramide di Caio Cestio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#179">[Tavola] Sepultura di
              Cestio ›Piramide di Caio Cestio‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#181">›Porta Trigemina‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501800s.html#182">Del Colle Aventino</a>
          <ul>
            <li>
              <a href="dg4501800s.html#183">›Armilustrium‹ dove
              fosse</a>
            </li>
            <li>
              <a href="dg4501800s.html#184">Terme di Decio
              Imperatore ›Terme Deciane‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#185">Terme d'Antonino
              ›Terme di Caracalla‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501800s.html#187">Del Monte Celio et
          Celiolo</a>
          <ul>
            <li>
              <a href="dg4501800s.html#189">›San Giovanni in
              Laterano‹ chiesa da chi edificata</a>
            </li>
            <li>
              <a href="dg4501800s.html#191">›Santa Croce in
              Gerusalemme‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#192">›Acqua Claudia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#196">[Tavola] ›Porta
              Maggiore‹ ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501800s.html#199">Il libro terzo dell'antichità
      della città di Roma</a>
      <ul>
        <li>
          <a href="dg4501800s.html#199">Del Colle dell'Esquilie
          ›Esquilino‹</a>
          <ul>
            <li>
              <a href="dg4501800s.html#200">›Carinae‹ dove
              fossero</a>
            </li>
            <li>
              <a href="dg4501800s.html#201">Chiesa di ›San Pietro
              in Vincoli‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#202">›Sette Sale‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#204">[Tavola] ›Trofei di
              Mario‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#206">[Tavola] ›Pianta del
              Trofeo di Mario‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#207">Acqua Martia ›Acqua
              Marcia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#209">›Acqua Tepula‹ et
              ›Acqua Iulia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#210">Vico Scelerato ›Clivus
              Orbius‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#210">Busti Gallici ›Busta
              Gallica‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#211">Tigillo Sororio
              ›Tigillum Sororium‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#213">Chiesa di ›Santa Maria
              Maggiore‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#214">Macello cosa fosse
              ›Macellum Liviae‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#215">Terme di Gordiano
              ›Balnea Gordiani‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#220">Basilica di Caio e
              Lucio ›Basilica Iulia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#223">Palazzo di Nerone, o
              Casa Aurea ›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#225">Tempio della Fortuna
              Seia, fatto di pietre trasparenti ›Fortuna Seiani,
              Aedes‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#226">Chiesa di ›San Lorenzo
              fuori le Mura‹ edificata da Costantino</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501800s.html#227">Del Colle Viminale</a>
          <ul>
            <li>
              <a href="dg4501800s.html#227">›Terme di
              Diocleziano‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#230">[Tavola] Parte di
              dentro delle ›Terme di Diocleziano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#232">[Tavola] Parte di
              fuora delle ›Terme di Diocleziano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#233">Terme di Diocleziano
              ridotte in una chiesa detta ›Santa Maria degli
              Angeli‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#234">Libreria Ulpia
              ›Biblioteca Ulpia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#237">›Porta
              Querquetulana‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#239">Terme di Novatio
              Imperatore ›Thermae Novati‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501800s.html#241">Del ›Quirinale‹ et del
          colle de gli hortoli</a>
          <ul>
            <li>
              <a href="dg4501800s.html#242">›Terme di
              Costantino‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#244">Vico de' Cornelii
              ›Vicus Corneli‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#246">[Tavola] Frontespicio
              della casa di Nerone ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#250">[Tavola] ›Bagni di
              Paolo Emilio‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#252">›Alta Semita‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#253">Tempio di Romulo
              ›Tempio di Quirino‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#255">Vico patricio ›Vicus
              Patricius‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#256">›Suburra‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#257">Torre Mamilia ›Turris
              Mamilia‹ et Suburra ›Torre dei Conti‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#259">Vico Mamurro ›Vicus
              Mamuri‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#263">Senatulo delle Donne
              ›Senaculum mulierum‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#264">Porta di Santa Agnese,
              o ›Nomentana‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#266">Porta Quirinale ›Porta
              Quirinalis‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#268">Colle degli orti
              ›Collis Hortulorum‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#269">Mura inchinate ›Muro
              Torto‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#270">Chiesa della Trinità
              ›Trinità dei Monti‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#270">Porta Flaminia, o del
              popolo Porta del Popolo‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#272">›Ponte Milvio‹, o
              Molle</a>
            </li>
            <li>
              <a href="dg4501800s.html#272">›Porta Collatina‹, o
              ›Porta Pinciana‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#274">›Tempio di Bellona‹,
              perchè edificato da Appio cieco</a>
            </li>
            <li>
              <a href="dg4501800s.html#276">Colonna bellica
              ›Columna Bellica‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#279">[Tavola] Santo Angelo
              in Pescaria ›Sant'Angelo in Pescheria‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#286">›Teatro di Pompeo‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#290">Chiesa di ›San Lorenzo
              in Damaso‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501800s.html#291">Del Campo Martio</a>
          <ul>
            <li>
              <a href="dg4501800s.html#293">Naumachia edificata da
              Domitiano ›Naumachia Domitiani‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#297">[Tavola] Arco di
              Domitiano ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#298">Chiesa di ›San Lorenzo
              in Lucina‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#298">›Mausoleo di
              Augusto‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#302">Colonna Antoniniana
              ›Colonna di Antonino Pio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#305">[Tavola] ›Colonna
              Antoniniana‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#307">[Tavola] Basilica di
              Antonino ›Tempio di Antonino e Faustina‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#308">Monte Citorio, o
              Citatorio ›Piazza di Montecitorio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#309">Septi che cosa era
              ›Septa Iulia‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#309">›Villa Publica‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#310">›Acqua
              Vergine/Acquedotto Vergine‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#312">Iuturna dove
              affogasse</a>
            </li>
            <li>
              <a href="dg4501800s.html#312">›Via Lata‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#313">›Foro Suario‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#314">Pantheone tempio
              ›Pantheon‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#316">[Tavola] Parte di
              fuori della Ritonda ›Pantheon‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#321">[Tavola] Parte di
              dentro della Ritonda ›Pantheon‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#322">›Terme di Agrippa‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#323">Buono Evento, come
              figurato da gli antichi ›Bonus Eventus, Templum‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#324">Piazza d'Agona ›Piazza
              Navona‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#325">Altari di Plutone
              ›Tempio di Saturno‹ et di Conso ›Ara di Conso‹,
              perchè posti insieme et sotto terra</a>
            </li>
            <li>
              <a href="dg4501800s.html#326">Giochi Equirri
              ›Equirria‹</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501800s.html#327">Il quarto libro dell'antichità
      della città di Roma</a>
      <ul>
        <li>
          <a href="dg4501800s.html#327">Del ›Trastevere‹</a>
          <ul>
            <li>
              <a href="dg4501800s.html#329">›Trastevere‹ già Colle
              Ianicolo ›Gianicolo‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#330">Ianicolo per che così
              detto ›Gianicolo‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#332">›Ponte Sublicio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#334">Sepoltura di Numa
              Pompilio ›Sepulcrum Numae Pompilii‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#334">Statio poeta dove
              sepolto ›Sepulcrum: Caecilius Statius‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#337">Porta Navale, o
              Portuense ›Porta Portuensis‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#338">Naumachia di Cesare
              ›Naumachia Cesaris‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#341">Chiesa di ›San Pietro
              in Montorio‹ da chi rinnovata</a>
            </li>
            <li>
              <a href="dg4501800s.html#343">[Tavola] Parte di
              fuora di ›San Pietro in Montorio‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#346">[Tavola] Parte di
              dentro di ›San Pietro in Montorio‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#347">›Porta Settimiana‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#348">Porta Aurelia ›Porta
              San Pancrazio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#349">Giardino di Galba
              ›Horti: Galba‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#350">Isola nel Tevere, come
              havesse principio ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#351">[Tavola] Isola di San
              Bartolomeo ›Isola Tiberina‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#352">Tempio di Giove
              Licaonio ›Iuppiter Iurarius‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#354">[Tavola] Isola di San
              Bartolomeo ›Isola Tiberina‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#355">Ponte a quattro capi
              ›Ponte Fabricio‹, et ›Ponte Cestio‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#355">Ponte di Santa Maria,
              o Senatorio ›Ponte Rotto‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#357">›Ponte Sisto‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501800s.html#358">Del Vaticano</a>
          <ul>
            <li>
              <a href="dg4501800s.html#359">›Porta Santo Spirito‹,
              del Torrione ›Porta Cavalleggeri‹, ›Porta Pertusa‹,
              di Belvedere ›Porta San Pellegrino‹, Posterula ›Porta
              Sant'Angelo‹, et Aenea ›Porta di San Pietro‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#360">›Castel Sant'Angelo‹,
              o mole d'Adriano</a>
            </li>
            <li>
              <a href="dg4501800s.html#364">[Tavola] ›Castel
              Sant'Angelo‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#370">Porta e ponte
              trionfale ›Porta Trionfale‹ ›Pons Neronianus‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#378">[Tavola] ›Obelisco
              Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501800s.html#383">Palazzo del Papa
              ›Palazzo Apostolico Vaticano‹</a>
            </li>
            <li>
              <a href="dg4501800s.html#385">›Cortile del
              Belvedere‹</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501800s.html#389">Registro</a>
    </li>
  </ul>
  <hr />
</body>
</html>
