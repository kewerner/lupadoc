<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbar5871690as.html#6">La pratica della
      perspettiva</a>
      <ul>
        <li>
          <a href="ghbar5871690as.html#7">Al molto magnifico et
          eccellente M. Matheo Macigni</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#8">Proemio</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#10">Divisione del
          trattamento della perspettiva</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#10">Prima parte</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#30">Parte seconda nella
          quale si tratta della Ichnographia, cioè descrittione
          delle piante</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#48">La terza parte che
          tratta del modo di levare i corpi dalle piante</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#134">Parrte quarta, nella
          quale si tratta della Scenographia, cioè descrittione
          delle Scene</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#164">Parte quinta nella
          quale si espone una bella, e secreta parte di
          Perspettiva</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#168">Parte sesta che si
          chiama Planispherio</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#180">Parte settima, la
          quale tratta de i Lumi, delle Ombre, e de i Colori</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#184">Parte ottava, nella
          quale si tratta delle misure del copro Humano</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#192">Parte nona, nella
          quale si descriveno molti instrumenti, e modi di ponere,
          e trapportare le cose in Perspettiva</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#199">Come con un nuovo
          instrumento si possino sapere le quantità delle scarpe
          delle muraglie, secondo la inventione di Iacomo
          Castriotto</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#202">Tavola di quello che
          si contoene in tutta l'opera secondo l'ordine de i
          Capi</a>
        </li>
        <li>
          <a href="ghbar5871690as.html#206">Tavola Generale
          delle nove partri della Perspettiva</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
