<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghser42252190s.html#6">Tutte l’opere
      d’architettura, et prospetiva</a>
      <ul>
        <li>
          <a href="ghser42252190s.html#8">Al molto honorato M.
          Francesco Senese</a>
        </li>
        <li>
          <a href="ghser42252190s.html#12">Sonetto
          dell'eccellente Signor Lodovico Roncone</a>
        </li>
        <li>
          <a href="ghser42252190s.html#13">Di M. Marco Stecchini
          per l'istessa occasione</a>
        </li>
        <li>
          <a href="ghser42252190s.html#14">Indice copiosissimo
          delle cose più degne</a>
        </li>
        <li>
          <a href="ghser42252190s.html#54">I. Nel quale con
          facile, e breve modo si tratta de' primi principij della
          Geometria</a>
        </li>
        <li>
          <a href="ghser42252190s.html#86">II. Di Prospettiva</a>
        </li>
        <li>
          <a href="ghser42252190s.html#150">III. Nel quale sono
          descritti, e disegnati la maggior parte degl'edificij
          antichi di Roma, e molti d'Italia, a' altre parti più
          lontane, con le loro misure&#160;</a>
        </li>
        <li>
          <a href="ghser42252190s.html#302">IV. Nel quale si
          tratta in disegno delle maniere de' cinque ordini</a>
        </li>
        <li>
          <a href="ghser42252190s.html#454">V. Nel quale si
          tratta di diverse forme di Templij sacri, secondo il
          costume Christiano, e al modo antico</a>
        </li>
        <li>
          <a href="ghser42252190s.html#492">VI. Nel quale si
          descrivono, e mettono in desegni cinquanta porte; cioè
          trenta di opera Rustica mista con diversi ordini, e venti
          di opera più delicata, le quali possono servire à molti
          generi di edificij</a>
        </li>
        <li>
          <a href="ghser42252190s.html#546">VII. Nel quale si
          tratta, e mettono in disegno molti nobili edificij, tanto
          publici, come privati; e varij accidenti,, che possono
          occorrere nello edificare, come si narra nella seguente
          pagina</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
