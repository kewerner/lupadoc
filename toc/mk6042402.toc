<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="mk6042402s.html#8">Storia dell'arte col mezzo dei
      monumenti dalla sua decadenza nel IV secolo fino al suo
      risorgimento nel XVI. Volume II. Contenente le tavole
      d'Architettura</a>
    </li>
    <li>
      <a href="mk6042402s.html#12">Indice delle tavole Contenente
      un sommario dei diversi monumenti che esse rappresentano, ed
      alcune notizie che non potevano essere inserite nel corpo
      dell'Opera</a>
      <ul>
        <li>
          <a href="mk6042402s.html#12">I. Decadenza
          dell'Architettura dal IV secolo fino allo stabilimento
          del sistema gotico &#160; &#160;&#160;</a>
          <ul>
            <li>
              <a href="mk6042402s.html#11">Tavola I.
              L'Architettura Antica nel suo stato di perfezione
              presso i Greci e presso i Romani</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#12">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#14">Tavola II. Principio
              della decadenza sotto Settimio Severo, Diocleziano, e
              Costantino nel secondo, terzo e quarto Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#13">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#16">Tavola III. Veduta
              dell'Interno di una corte del Palazzo di Diocleziano
              a Spalatro [sic]. III. Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#20">[Text]&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#18">Tavola IV. Chiesa di
              ›San Paolo fuori le Mura‹ di Roma eretta da
              Costantino nel IV. Secolo colle innovazioni dal suo
              innalzamento ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#20">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#22">Tavola V. Arco della
              navata di ›San Paolo fuori le Mura‹, sostenuto da due
              colonne fra loro diverse nell'epoca, e nello stile.
              IV. Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#21">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#24">Tavola VI. Base e
              Capitello Corinto [sic] in ›San Paolo fuori le Mura‹,
              dei migliori tempi antichi ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#21">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#26">Tavola VII. Base e
              Capitello Composito della nave di ›San Paolo fuori le
              mura‹, del tempo della sua costruzione nel IV Secolo
              ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#30">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#28">Tavola VIII. Basilica
              di ›Sant'Agnese fuori le Mura‹, Chiesa di Santa
              Costanza ›Mausoleo di Santa Costanza‹, Tempio di
              Nocera IV. Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#30">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#32">Tavola IX. Quadro
              delle più celebri Catacombe Pagane e
              Cristiane&#160;</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#31">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#38">Tavola X. Parte delle
              Catacombe, o Sotterranei Etruschi dell'antica
              Tarquinia presso Corneto</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#40">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#42">Tavola XI. Altra parte
              di Catacombe Etrusche dell'antica Tarquinia presso
              Corneto</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#41">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#46">Tavola XII. ›Sepolcro
              degli Scipioni‹, Catacomba di Sant'Ermes ›Catacombe
              di Sant'Ermete‹, e Tomba di questo Santo convertita
              in Altare ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#48">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#50">Tavola XIII. Cappelle,
              ed Oratorj delle Catacombe, le di cui forme
              trasportate nelle Chiese vi hanno alterate quelle
              della Architettura Antica I. II. III. Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#49">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#52">Tavola XIV. Piante di
              San Martino ai Colli ›San Martino ai Monti‹ in Roma.
              Esempio di una Chiesa innalzata sopra un'oratorio
              [sic] sotterraneo. Opera del IV. Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#54">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#56">Tavola XV. San Nazaro
              e San Celso a Ravenna, imitazione di una cappella
              sepolcrale sotterranea. V. Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#55">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#58">Tavola XVI. ›San
              Clemente‹ a Roma, modello il più conservato della
              disposizione delle prime chiese. V. secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#55">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#60">Tavola XVII. Palazzi,
              Chiese ed altre costruzioni del tempo di Teodorico a
              terracina ed a Ravenna V, e VI Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#62">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#64">Tavola XVIII. Mausoleo
              di Teoderico a Ravenna oggi Santa Maria della
              rotonda. IV Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#66">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#68">Tavola XIX. Pianta,
              elevazioni e dettagli del ›Ponte Salario‹ sul
              Teverone ›Aniene‹ vicino a Roma, riedificato da
              Narsete. VI Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#67">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#70">Tavola XX. Tempio
              antico della Caffarella ›Sant'Urbano‹ vicino a Roma,
              uno dei primi esempj di un tempio pagano consacrato
              al culto cristiano. IV Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#72">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#74">Tavola XXI. ›San
              Pietro in Vincoli‹ a Roma, esempio d'una chiesa
              fabbricata con colonne antiche. V Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#76">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#78">Tavola XXII. ›Santo
              Stefano Rotondo‹ a Roma, esempio di un edifizio
              antico convertito in chiesa. V e VI Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#77">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#82">XXIII. Chiesa di san
              Vitale a Ravenna, fabbricata sotto il regno di
              Giustiniano e con disegni provenuti dall'Oriente. VI
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#81">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#86">Tavola XXIV. Figura e
              dettagli di varie chiese, stile dell'Architettura in
              Italia, sotto il regno dei Longobardi VI, VII ed VIII
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#85">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#90">Tavola XXV.
              L'Architettura migliorata in Italia, sotto il regno
              di Carlo Magno, nel IX Secolo, e dai Pisani nel X ed
              XI Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#89">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#96">Tavola XXVI. Santa
              Sofia di Costantinopoli, san Marco ed altre chiese di
              Venezia, fabbricate secondo lo stile greco moderno. X
              ed XI Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#95">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#98">Tavola XXVII. Quadro
              generale della decadenza dell'Architettura, nelle
              contrade orientali</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#100">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#104">Tavola XXVIII. Ultimo
              grado della decadenza dell'Architettura nelle
              province occidentali d'Italia. XIII Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#103">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#108">Tavola XXIX. Edifizj
              monastici; pianta, elevazione e dettagli del
              monastero di santa Scolastica, a Subiaco, presso
              Roma. XIII Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#110">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#112">Tavola XXX. Piante e
              spaccati del chiostro di ›San Giovanni in Laterano‹ e
              di ›San Paolo fuori le Mura‹ di Roma. XII e XII [sic]
              Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#111">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#114">Tavola XXXI. Chiostro
              di ›San Paolo fuori le Mura‹: sezioni generali e
              dettagli delle basi e dei capitelli delle sue
              colonne. XII e XIII Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#111">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#116">Tavola XXXII.
              Chiostro di ›San Paolo fuori le Mura‹: piante ed
              elevazioni di alcune parti delle sue facciate. XII e
              XIII Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#120">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#118">Tavola XXXIII.
              Chiostro di ›San Paolo fuori le Mura‹: dettagli della
              trabeazione ricca di musaici, ornamenti scolpiti fra
              gli archi. XII e XIII Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#120">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#122">Tavola XXXIV. Piante,
              elevazioni e dettagli della casa di Crescenzio o di
              Cola di Rienzo detta la casa di Pilato ›Casa dei
              Crescenzi‹ a Roma. XI Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#121">[Text]</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042402s.html#128">II. Sistema di
          Architettura, detta gotica, in cominciando dai secoli IX,
          X e XI fino al XV</a>
          <ul>
            <li>
              <a href="mk6042402s.html#126">Tavola XXXV. Primi
              indizj dell'Architettura detta Gotica in Italia nella
              Badia di Subiaco vicino a Roma. IX, X, XI e XII
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#128">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#132">Tavola XXXVI.
              Riunione di diversi edifizj che mostrano lo stile
              dell'Architettura detta Gotica dalla sua origine nel
              IX secolo, fino al XIII</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#134">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#138">Tavola XXXVII.
              Pianta, spaccato e dettagli delle chiese, inferiore e
              superiore, di San Francesco ad Assisi. XIII
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#140">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#142">XXXVIIII. Pianta,
              spaccato e facciata della chiesa di San Flaviano,
              presso Montefiascone. XIII e XIV Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#141">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#144">Tavola XXXIX. Pianta,
              spaccato per il lungo e parti in grande della chiesa
              di Nostra Signora, Cattedrale di Parigi. XII e XIII
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#148">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#146">Tavola XL. Porta,
              elevazione laterale, veduta interna e dettagli della
              decorazione della Cattedrale di Parigi. XII e XIII
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#148">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#150">Tavola XLI. Monumenti
              principali dell'Architettura detta gotica, innalzati
              in varie contrade di Europa, nei secoli XIV e XV,
              epoca la più brillante di questo Sistema</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#149">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#152">Tavola XLII. Serie
              cronologica degli Archi sostituiti alle Trabeazioni,
              nell'Architettura detta gotica, e di altre parti che
              ne costituiscono il Sistema</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#154">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#158">Tavola XLIII.
              Architettura di Svezia, prima e dopo l'introduzione
              in quel paese, del sistema detto gotico nel XIII
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#157">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#162">Tavola XLIV. Stato
              dell'Architettura Araba in Europa, dall'VIII fino al
              XV Secolo &#160;</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#161">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#168">Tavola XLV. Serie di
              Edifizj di diversi paesi i quali sembrano partecipare
              dello stile, detto Gotico, ed aver dato occasione
              alla sua invenzione in Europa</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#167">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#172">Tavola XLVI.
              Congetture sull'origine, le forme diverse, e l'uso
              dell'arco di sesto acuto detto Gotico, nei paesi più
              conosciuti</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#174">[Text]</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042402s.html#179">III. Rinascimento
          dell'Architettura, verso la metà del XV secolo&#160;</a>
          <ul>
            <li>
              <a href="mk6042402s.html#180">Tavola XLVII. Pianta
              e spaccato della chiesa di San Lorenzo in Firenze di
              Filippo Brunelleschi, primario autore del
              rinnovamento dell'Architettura &#160;</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#179">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#182">Tavola XLVIII.
              Intercolunnio e dettagli dell'ordine interno della
              chiesa di San Lorenzo a Firenze, del Brunelleschi. XV
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#179">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#184">Tavola XLIX. Pianta,
              spaccato, elevazione e dettaglio della chiesa dello
              Spirito Santo a Firenze del Brunelleschi. XV
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#186">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#188">Tavola L. Riunione
              delle principali opere di Architettura di Filippo
              Brunelleschi. XV Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#187">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#190">Tavola LI. Piante ed
              elevazioni della chiesa di San Francesco a Rimini
              terminata sul disegno di Leon Battista Alberti. XV
              secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#192">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#194">Tavola LII. Chiese di
              Sant'Andrea, e di San Sebastiano in Mantova,
              fabbricate coi disegni di Leon Battista Alberti</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#196">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#197">Tavola LIII. Arco
              trionfale innalzato a Napoli in onore di Alfonso I
              d'Aragona. XV secolo [Text]&#160;</a>
            </li>
            <li>
              <a href="mk6042402s.html#200">Tavola LIV. Edifizi
              diversi innalzati a Roma ed a Napoli. XIII, XIV e XV
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#199">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#204">Tavola LV. Antico
              Teatro dei confratelli della Passione a Velletri
              presso Roma. Secolo XV</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#206">[Text]</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042402s.html#207">IV. Rinnovellamento
          dell'Architettura, alla fine del XV secolo ed in
          principio del XVI</a>
          <ul>
            <li>
              <a href="mk6042402s.html#208">Tavola LVI. Studj
              d'Architettura, disegnati sull'Antico da Bramante ed
              Antonio Sangallo. Secolo XV</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#207">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#210">Tavola LVII. Opere
              principali di Architettura di Bramante Lazzari,
              edifizj civili. Principio del Secolo XVI</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#207">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#214">Tavola LVIII.
              Continuazione delle opere di Bramante Lazzari;
              edifizi sacri. Principio del Secolo XVI</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#213">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#218">Tavola LIX. Piante
              elevazioni e spaccati dei principali edifizj
              innalzati sui disegni di Michelangelo Bonarroti.
              Secolo XVI</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#220">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#222">Tavola LX. Dettagli e
              profili dei principali edifizj costruiti sui disegni
              di Michel Angelo Bonarroti. Secolo XVI</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#221">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#224">Tavola LXI. Piante,
              spaccati e dettagli dell'antica e della nuova
              basilica di ›San Pietro in Vaticano‹ a Roma. IV, XV,
              XVI e XVII Secolo ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#226">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#228">Tavola LXII. Veduta
              generale della Basilica di ›San Pietro in Vaticano‹,
              e del ›Palazzo Apostolico Vaticano‹ ▣</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#227">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#230">Tavola LXIII. Forme
              dei principali Battisteri, spezie particolare
              d'edifizj dovuta allo stabilimento della religione
              cristiana</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#227">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#234">Tavola LXIV. Quadro
              storico e cronologico delle facciate dei tempj [sic]
              prima e durante la decadenza dell'Arte</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#236">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#240">Tavola LXV. Quadro
              degli architravi usati nell'interno degli edifizj
              durante la decadenza dell'Arte e degli archi che
              furono ad essi sostituiti</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#239">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#244">Tavola LXVI. Forme
              principali delle volte e delle soffitta usate negli
              edifizj sacri durante la decadenza</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#243">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#246">Tavola LXVII. Quadro
              cronologico ed istorico dell'invenzione e dell'uso
              delle cupole</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#248">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#252">Tavola LXVIII. Quadro
              delle forme e proporzioni delle colonne avanti e
              durante la decadenza dell'Arte fino al suo
              rinnovellamento</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#254">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#260">Tavola LXIX. Quadro
              cronologico delle diverse spezie di basi e di
              capitelli dal principio della decadenza fino all'XI
              Secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#262">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#266">Tavola LXX.
              Continuazione del quadro cronologico delle basi e dei
              capitelli, dall'XI fino al XVI secolo</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#268">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#274">Tavola LXXI.
              Apparecchi e maniere di costruzioni in uso prima e
              durante la decadenza dell'Arte</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#273">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#283">Tavola LXXII. Quadro
              comparativo dello stile dell'Architettura civile
              durante la sua decadenza, fino al totale suo
              rinnovellamento</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#281">[Text]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042402s.html#290">Tavola LXXIII.
              Riassunto e quadro generale dei monumenti, che hanno
              servito a formare la storia della decadenza
              dell'Architettura</a>
              <ul>
                <li>
                  <a href="mk6042402s.html#292">[Text]</a>
                </li>
                <li>
                  <a href="mk6042402s.html#308">Tavola
                  cronologica dei monumenti che compongono la
                  Tavola LXXIII classificati secondo l'ordine
                  storico del testo di questa opera</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="mk6042402s.html#310">Titoli e soggetti delle
      tavole relative all'Architettura</a>
    </li>
  </ul>
  <hr />
</body>
</html>
