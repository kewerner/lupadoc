<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghsca46412150s.html#6">L’ idea della architettura
      universale</a>
      <ul>
        <li>
          <a href="ghsca46412150s.html#6">Parte prima</a>
          <ul>
            <li>
              <a href="ghsca46412150s.html#8">Al serenissimo
              Prencipe Massimiliano Arciduca d'Austria</a>
            </li>
            <li>
              <a href="ghsca46412150s.html#10">A prudenti, e
              benigni lettori</a>
            </li>
            <li>
              <a href="ghsca46412150s.html#12">Indice de'
              capi&#160;</a>
            </li>
            <li>
              <a href="ghsca46412150s.html#16">Libro primo</a>
            </li>
            <li>
              <a href="ghsca46412150s.html#108">Libro secondo</a>
            </li>
            <li>
              <a href="ghsca46412150s.html#242">Libro terzo</a>
            </li>
            <li>
              <a href="ghsca46412150s.html#380">Indice
              copiosissimo delle materie, che si contengono nella
              prima parte&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghsca46412150s.html#412">Parte seconda</a>
          <ul>
            <li>
              <a href="ghsca46412150s.html#414">Al serenissimo
              Cosmo de Medici</a>
            </li>
            <li>
              <a href="ghsca46412150s.html#416">Indice de'
              capi&#160;</a>
            </li>
            <li>Libro Sesto</li>
            <li>
              <a href="ghsca46412150s.html#596">Libro settimo</a>
            </li>
            <li>
              <a href="ghsca46412150s.html#696">Libro ottavo</a>
            </li>
            <li>
              <a href="ghsca46412150s.html#800">Indice
              copiosissimo delle materie, che si contengono nella
              seconda parte</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
