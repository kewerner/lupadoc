<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlom5391900s.html#8">Idea del Tempio della
      Pittura</a>
      <ul>
        <li>
          <a href="ghlom5391900s.html#10">All'invittissimo, et
          potentissimo signore il re don Filippo d'Austria</a>
        </li>
        <li>
          <a href="ghlom5391900s.html#16">Tavola dei capitoli
          dell'opera</a>
        </li>
        <li>
          <a href="ghlom5391900s.html#18">Tavola delle cose
          notabili contenute nella presente opera</a>
        </li>
        <li>
          <a href="ghlom5391900s.html#31">Tavola de' nomi degli
          artefici più illustri, così anctichi come moderni citati
          in quest'opera</a>
        </li>
        <li>
          <a href="ghlom5391900s.html#40">Idea del Tempio della
          Pittura di Gio. Paolo Lomazzo pittore</a>
          <ul>
            <li>
              <a href="ghlom5391900s.html#40">Proemio al lettore.
              Cap. 1</a>
            </li>
            <li>
              <a href="ghlom5391900s.html#45">De la forza de
              l'institutione dell'arte, &amp; della diversità de i
              genij. Cap. 2</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghlom5391900s.html#208">Della Forma delle Muse,
      cavata da gli antichi autori greci, et latini</a>
      <ul>
        <li>
          <a href="ghlom5391900s.html#210">Al serenissimo
          Ferdinando de Medici Gran Duca di Toscana</a>
        </li>
        <li>
          <a href="ghlom5391900s.html#214">Della Forma delle
          Muse, cavata da gi antichi autori greci, et latini</a>
        </li>
        <li>
          <a href="ghlom5391900s.html#254">Tavola de' nomi de gli
          autori citati in uest'opera, tanto greci, quanto
          latini</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
