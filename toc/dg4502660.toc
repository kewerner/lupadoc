<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502660s.html#8">Le cose meravigliose dell’alma
      città di Roma</a>
    </li>
    <li>
      <a href="dg4502660s.html#10">Le sette chiese principali</a>
      <ul>
        <li>
          <a href="dg4502660s.html#10">Prima Chiesa è San Giovanni
          Laterano ›San Giovanni in Laterano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#21">La seconda Chiesa è ›San
          Pietro in Vaticano‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502660s.html#16">[›San Pietro in
              Vaticano‹] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502660s.html#27">La terza Chiesa è di San
          Paolo ›San Paolo fuori le Mura‹ ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#29">La quarta Chiesa è ›Santa
          Maria Maggiore‹ ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#32">La quarta [sic] Chiesa è
          San Lorenzo fuor delle mura ›San Lorenzo fuori le Mura‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#33">La sesta Chiesa è ›San
          Sebastiano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#35">La settima Chiesa è Santa
          Croce in Gierusalemme ›Santa Croce in Gerusalemme‹ ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502660s.html#36">Dell'Isola ›Isola Tiberina‹</a>
    </li>
    <li>
      <a href="dg4502660s.html#37">In ›Trastevere‹</a>
    </li>
    <li>
      <a href="dg4502660s.html#41">›Borgo‹</a>
    </li>
    <li>
      <a href="dg4502660s.html#43">Della porta Flaminia, overo del
      Popolo ›Porta del Popolo‹ fino alle radici di Campidoglio</a>
      <ul>
        <li>
          <a href="dg4502660s.html#56">›San Carlo ai Catinari‹
          ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502660s.html#59">Del Campidoglio à man sinistra
      verso i Monti ▣</a>
    </li>
    <li>
      <a href="dg4502660s.html#66">Del Campidoglio à man dritta
      verso il Trastevere ▣</a>
    </li>
    <li>
      <a href="dg4502660s.html#71">Stationi delle Chiese di
      Roma</a>
    </li>
    <li>
      <a href="dg4502660s.html#78">Guida Romana per li Forastieri
      che vogliono vedere l'Antichità di Roma una per una</a>
      <ul>
        <li>
          <a href="dg4502660s.html#78">Del Borgo prima
          Giornata</a>
          <ul>
            <li>
              <a href="dg4502660s.html#79">Del ›Trastevere‹ ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#80">Dell' ›Isola Tiberina‹,
              e Licaonia ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#81">Del Ponte Santa Maria
              ›Ponte Rotto‹, del Palazzo di Pilato ›Casa dei
              Crescenzi‹, e altre cose ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#82">Del Monte Testaccio
              ›Testaccio (Monte)‹, e altre cose ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#83">Delle Terme Antoniane
              ›Terme di Caracalla‹, e altre cose</a>
            </li>
            <li>
              <a href="dg4502660s.html#83">Di San Giovanni
              Laterano ›San Giovanni in Laterano‹, Santa Croce
              ›Santa Croce in Gerusalemme‹, e altre cose</a>
            </li>
            <li>
              <a href="dg4502660s.html#84">La chiesa ve la fece
              fare Costantino Magno</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502660s.html#84">Giornata seconda</a>
          <ul>
            <li>
              <a href="dg4502660s.html#84">Della ›Porta del
              Popolo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#86">Del Monte Cavallo,
              detto Quirinale ›Quirinale‹, e de i Cavalli ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#87">Della Strada Pia, e
              della Vigna, ch'era già del Cardinale di Ferrara
              ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#88">Della ›Porta Pia‹ di
              ›Sant'Agnese ‹ , e altare anticaglie ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#89">Delle Terne Diocletiane
              ›Terme di Diocleziano‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4502660s.html#91">[›Colosseo‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502660s.html#94">Del Monte Palatino
              ›Palatino‹, hoggi detto Palzzo Maggiore, Del Tempio
              della Pace ›Foro della Pace‹, e altre cose ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#95">Del Foro di Nerva ›Foro
              di Nerva‹</a>
            </li>
            <li>
              <a href="dg4502660s.html#95">Dell'Arca Trionfale di
              Settimio Severo ›Arco di Settimio Severo‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4502660s.html#96">[›Campidoglio‹
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502660s.html#97">De' Portici d'Ottavia,
              di Settimo, e Teatro di Pompeo ›Teatro di Pompeo‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502660s.html#97">Giornata terza</a>
          <ul>
            <li>
              <a href="dg4502660s.html#98">Della Rotonda, overo
              Panteon ›Pantheon‹ ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#99">De' Bagni d'Agrippa, e
              di Nerone ▣</a>
            </li>
            <li>
              <a href="dg4502660s.html#100">Della ›Piazza Navona‹,
              e Pasquino ›Statua di Pasquino‹ ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502660s.html#100">[Nomi de' Sommi Pontefici,
      Imperatori e altri Principi Christiani ]</a>
      <ul>
        <li>
          <a href="dg4502660s.html#100">Indice de' sommi Pontefici
          romani</a>
        </li>
        <li>
          <a href="dg4502660s.html#112">Reges, et Imperatores
          romani</a>
        </li>
        <li>
          <a href="dg4502660s.html#116">[Re di Francia]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502660s.html#117">Le sette maraviglie del
      mondo</a>
      <ul>
        <li>
          <a href="dg4502660s.html#117">I. Delle mure di Babilonia
          ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#118">II. Della Torre di Faros
          ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#119">III. Della Statua di Giove
          ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#120">IV. Del Colosseo di Rodi
          ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#121">V. Del Tempio di Diana
          ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#122">VI. Del Mausoleo
          d'Artemisia ▣</a>
        </li>
        <li>
          <a href="dg4502660s.html#123">VII.Delle Piramide
          d'Egitto ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502660s.html#124">Le principali poste
      d'Italia</a>
      <ul>
        <li>
          <a href="dg4502660s.html#124">Poste da Roma Loreto, e
          Ancona</a>
        </li>
        <li>
          <a href="dg4502660s.html#124">Poste da Roma à Siena, da
          Siena à Fiorenza, da Fiorenza à Bologna, da Bologna à
          Milano, da Milano à Genova per il camino dritto</a>
        </li>
        <li>
          <a href="dg4502660s.html#125">Poste da Roma à
          Venetia</a>
        </li>
        <li>
          <a href="dg4502660s.html#126">Poste da Roma à Napoli, e
          da Napoli à Messina per il camino dritto</a>
        </li>
        <li>
          <a href="dg4502660s.html#126">Poste da Milano à Venetia
          per il camino di Brescia</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
