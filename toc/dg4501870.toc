<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501870s.html#10">le cose maravigliose dell’alma
      citta di Roma</a>
      <ul>
        <li>
          <a href="dg4501870s.html#12">Le sette chiese
          principali</a>
        </li>
        <li>
          <a href="dg4501870s.html#24">In Trastevere</a>
        </li>
        <li>
          <a href="dg4501870s.html#27">In Borgo</a>
        </li>
        <li>
          <a href="dg4501870s.html#29">Da la porta del Popolo
          della Porta Flaminia, fuori del Popolo fino alle radici
          del Campidoglio</a>
        </li>
        <li>
          <a href="dg4501870s.html#40">Del Campidoglio a man
          sinistra verso li monti</a>
        </li>
        <li>
          <a href="dg4501870s.html#47">Dal Campidoglio a man
          dritta verso li Monti</a>
        </li>
        <li>
          <a href="dg4501870s.html#52">Tavola delle chiese di
          Roma</a>
        </li>
        <li>
          <a href="dg4501870s.html#55">Le Stationi che sono ne le
          Chiese di Roma, si per la Quadregesima, come per tutto
          l'anno</a>
        </li>
        <li>
          <a href="dg4501870s.html#64">Trattato over modo
          d'acquistar l'indulgenze e le Stationi&#160;</a>
        </li>
        <li>
          <a href="dg4501870s.html#69">La guida romana</a>
        </li>
        <li>
          <a href="dg4501870s.html#80">Summi Pontifices</a>
        </li>
        <li>
          <a href="dg4501870s.html#104">Reges et Imperatores</a>
        </li>
        <li>
          <a href="dg4501870s.html#109">Li Re di Francia</a>
        </li>
        <li>
          <a href="dg4501870s.html#110">Li Re del regno di Napoli,
          et di Sicilia</a>
        </li>
        <li>
          <a href="dg4501870s.html#111">Li Dogi di Venetia</a>
        </li>
        <li>
          <a href="dg4501870s.html#116">De l'antichità di Roma di
          M. Andrea Palladio</a>
          <ul>
            <li>
              <a href="dg4501870s.html#117">A li lettori</a>
            </li>
            <li>
              <a href="dg4501870s.html#119">Delle antichità della
              città di Roma</a>
            </li>
            <li>
              <a href="dg4501870s.html#167">Tavola de le antichità
              de la città di Roma</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
