<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghcom551138803s.html#6">Bibliografia
      storico-critica dell'architettura civile ed arti subalterni,
      Vol. III.</a>
      <ul>
        <li>
          <a href="ghcom551138803s.html#8">Agli amatori delle
          belle arti</a>
        </li>
        <li>
          <a href="ghcom551138803s.html#10">Bibliografia
          storico-critica dell'architettura; classe seconda</a>
        </li>
        <li>
          <a href="ghcom551138803s.html#324">Indice degli
          autori, e delle opere</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
