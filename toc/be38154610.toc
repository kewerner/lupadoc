<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="be38154610s.html#6">La città di Rifugio
      dell’Abruzzo-Aquilano, o sia Descrizione storica delle più
      venerabili chiese ed immagini di Maria Santissima ...</a>
      <ul>
        <li>
          <a href="be38154610s.html#9">Prefazione</a>
        </li>
        <li>
          <a href="be38154610s.html#14">[La città di Rifugio
          dell’Abruzzo-Aquilano, o sia Descrizione storica delle
          più venerabili chiese ed immagini di Maria Santissima
          ...]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
