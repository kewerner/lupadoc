<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="even11642630s.html#8">Venetia città nobilissima et
      singolare descritta in XIIII libri da Francesco Sansovino</a>
      <ul>
        <li>
          <a href="even11642630s.html#10">Illustrissimo et
          eccellentissimo Signore</a>
        </li>
        <li>
          <a href="even11642630s.html#16">Au auctorem</a>
        </li>
        <li>
          <a href="even11642630s.html#17">A chi legge</a>
        </li>
        <li>
          <a href="even11642630s.html#19">Materie, che si
          trattano nell'opera presente</a>
        </li>
        <li>
          <a href="even11642630s.html#22">Della Venezia, città
          nobilissima</a>
          <ul>
            <li>
              <a href="even11642630s.html#22">I. [Del sestiero di
              Castello]</a>
            </li>
            <li>
              <a href="even11642630s.html#113">II. [Del sestiero
              di San Marco]&#160;</a>
            </li>
            <li>
              <a href="even11642630s.html#161">III. [Del sestiero
              di Canareio]&#160;</a>
            </li>
            <li>
              <a href="even11642630s.html#202">IV. [Del sestiero
              di S. Polo]</a>
            </li>
            <li>
              <a href="even11642630s.html#223">V. [Del sestiero
              di S. Croce]</a>
            </li>
            <li>
              <a href="even11642630s.html#265">VI. [Del sestiero
              di Dorsoduro]</a>
            </li>
            <li>
              <a href="even11642630s.html#304">VII. [Delle
              Fraterne o Schole Grandi]</a>
            </li>
            <li>
              <a href="even11642630s.html#315">VIII. [Delle
              Fraterne pubbliche]</a>
            </li>
            <li>
              <a href="even11642630s.html#404">IX. [De i Palazzi
              privati et de loro ornamenti]&#160;</a>
            </li>
            <li>
              <a href="even11642630s.html#421">X. [De gli habiti,
              costumi et usi della città]</a>
            </li>
            <li>
              <a href="even11642630s.html#490">XI. [Della
              grandezza, et dignità del Principe]</a>
            </li>
            <li>
              <a href="even11642630s.html#515">XII. [Dell'andate
              publiche del Principe]&#160;</a>
            </li>
            <li>
              <a href="even11642630s.html#550">XIII. [Delle vite
              de Principi]</a>
            </li>
            <li>
              <a href="even11642630s.html#782">Cronico
              particolare delle cose fatte da i veneti</a>
            </li>
            <li>
              <a href="even11642630s.html#868">Prima tavola delle
              Chiese, e Monasteri di Venetia&#160;</a>
            </li>
            <li>
              <a href="even11642630s.html#871">Seconda tavola dei
              Dogi di Venetia</a>
            </li>
            <li>
              <a href="even11642630s.html#873">Terza tavola de
              gli Huomini Letterati veneti</a>
            </li>
            <li>
              <a href="even11642630s.html#878">Prima catalogo de
              gl'huomini letterati veneti</a>
            </li>
            <li>
              <a href="even11642630s.html#888">Secondo catalogo
              de i Dottori</a>
            </li>
            <li>
              <a href="even11642630s.html#892">Terzo catalogo de
              i Medici</a>
            </li>
            <li>
              <a href="even11642630s.html#895">Quarto catalogo de
              gli Avvocati</a>
            </li>
            <li>
              <a href="even11642630s.html#898">Quinto catalogo de
              gli pittori di nome</a>
            </li>
            <li>
              <a href="even11642630s.html#901">Sesto catalogo
              delli Scultori</a>
            </li>
            <li>
              <a href="even11642630s.html#902">Tavola di tutte le
              materie che si contengono nell'Opera presente</a>
            </li>
            <li>
              <a href="even11642630s.html#968">Catalogo de i
              Senatori et Huomini illustri</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
