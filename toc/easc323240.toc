<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="easc323240s.html#10">Ascoli in prospettiva colle
      sue piú singolari pitture, sculture, e architetture</a>
      <ul>
        <li>
          <a href="easc323240s.html#12">Illustrissimo, e
          Reverendissimo Signore, Signor Padrone Colendissimo</a>
        </li>
        <li>
          <a href="easc323240s.html#17">Cortese lettore&#160;</a>
        </li>
        <li>
          <a href="easc323240s.html#27">In lode del Signore
          Avvocato Tullio Lazzari</a>
        </li>
        <li>
          <a href="easc323240s.html#28">Sovra lo stesso
          soggetto</a>
        </li>
        <li>
          <a href="easc323240s.html#29">Sonetto del Signor
          Dottore Francesco Borgianelli</a>
        </li>
        <li>
          <a href="easc323240s.html#30">In lode dello stesso</a>
        </li>
        <li>
          <a href="easc323240s.html#31">In lode dell'autore</a>
        </li>
        <li>
          <a href="easc323240s.html#32">In lode dell'istesso
          Signore</a>
        </li>
        <li>
          <a href="easc323240s.html#34">Indice degli Autori
          citati nell'opera</a>
        </li>
        <li>
          <a href="easc323240s.html#38">Ascoli in prospettiva</a>
          <ul>
            <li>
              <a href="easc323240s.html#38">Introduzione</a>
            </li>
            <li>
              <a href="easc323240s.html#44">[Text]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="easc323240s.html#208">Tavola</a>
        </li>
        <li>
          <a href="easc323240s.html#212">Lo stampatore a chi
          legge</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
