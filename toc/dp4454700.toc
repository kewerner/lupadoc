<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dp4454700s.html#6">Musaici della pimitica epoca
      delle chiese di Roma espressi in tavole disegnate ed incise
      dall’architetto prospettico Giacomo Fontana e corredati di
      brevi notizie storiche e descrittive</a>
      <ul>
        <li>
          <a href="dp4454700s.html#8">Idea dell'opera</a>
        </li>
        <li>
          <a href="dp4454700s.html#10">[Descripzioni]</a>
          <ul>
            <li>
              <a href="dp4454700s.html#10">I. Musaico dell'abside
              della Basilica di ›San Giovanni in Laterano‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#11">II. Prospetto ed
              interno dei musaici che decorano l'abside di ›San
              Clemente‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#12">III. Musaici che
              decorano l'abside e parte superiore de' ›Santi Cosma
              e Damiano‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#12">IV. Abside della
              Basilica Liberiana o di ›Santa Maria
              Maggiore‹&#160;</a>
            </li>
            <li>
              <a href="dp4454700s.html#13">V. Musaici dell'arcone
              fatti eseguire da Sisto III. nella Basilica Liberiana
              [›Santa Maria Maggiore‹]</a>
            </li>
            <li>
              <a href="dp4454700s.html#13">VI. Musaico dell'abside
              di ›Santa Maria in Trastevere‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#14">VII. Musaico che decora
              la facciata superiore dell'abside ›Santa Maria in
              Trastevere‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#14">VIII. Musaico
              dell'abside della Basilica di ›Sant'Agnese fuori le
              Mura‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#14">IX. Musaico dell'abside
              di ›Santa Francesca Romana‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#15">X. Musaici che decorano
              la facciata ed interno della cappella della Santa
              Colonna [›Santa Prassede‹]</a>
            </li>
            <li>
              <a href="dp4454700s.html#16">XI. Altri particolari
              sui musaici disposti nella stessa cappella della
              Santa Colonna [›Santa Prassede‹]</a>
            </li>
            <li>
              <a href="dp4454700s.html#16">XII. Musaico della
              facciata dell'arcone sovraposto all'abside di ›Santa
              Prassede‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#17">XIII. Musaico
              dell'abside di Santa Maria in Domnica detta della
              Navicella ›Santa Maria in Domnica alla Navicella‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#18">XIV. Musaico
              dell'abside della chiesa di ›Santa Pudenziana‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#18">XV. Musaico che
              decorava l'antica facciata della Basilica Liberiana
              ora rimasto pero decorazione interna al sopra portico
              della loggia [›Santa Maria Maggiore‹]</a>
            </li>
            <li>
              <a href="dp4454700s.html#19">XVI. Musaico che si
              ergeva sopra il portico dell'antica facciata di San
              Paolo ora disposto per decorazione interna della
              ricostruita Basilica ›San Paolo fuori le Mura‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#19">XVII. Musaico che
              decora la facciata dell'arco di placidia nella
              Baslicia di San Paolo ›San Paolo fuori le Mura‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#20">XVIII. Musaico
              dell'abside della Basilica di San Paolo ›San Paolo
              fuori le Mura‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#21">XIX. Musaico
              dell'arcone che decora la Basilica di San Marco di
              Roma ›San Marco‹&#160;</a>
            </li>
            <li>
              <a href="dp4454700s.html#22">XX. Musaico dell'antico
              ›Triclinio leoniano‹ ora esistente all'esterno del
              lato meridionale della cappella del Santissimo
              Salvatore alla scala santa</a>
            </li>
            <li>
              <a href="dp4454700s.html#22">XXI. Musaico
              dell'abside di ›Santa Cecilia in Trastevere‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#23">XXII. Musaico
              sull'arcone che guarda il presbiterio dell'antica
              Basilica di ›San Lorenzo fuori le Mura‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#24">XXIII. Musaico della
              Navicella di Giotto ora &#160;situato dicontro alla
              porta maggiore della Basilica Vaticana ›San Pietro in
              Vaticano: Mosaico della Navicella‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#24">XXIV. Musaico al
              disopra dell'abside de' ›Santi Nereo e Achilleo‹,
              sedia pontificale, ed ambone</a>
            </li>
            <li>
              <a href="dp4454700s.html#25">XXV. Pulpito o ambone e
              sedia episcopale ambedue esistenti nella Basilica
              ›San Lorenzo fuori le Mura‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#25">XXVI. Monumento del
              cardinal Consalvo Rodrigo esistente nella nave minore
              a destra della Basilica Liberiana [›Santa Maria
              Maggiore‹]</a>
            </li>
            <li>
              <a href="dp4454700s.html#26">XXVII. Dettaglio
              esterno di una perta del chiostro di ›San Giovanni in
              Laterano‹, ossia l'antica canonica</a>
            </li>
            <li>
              <a href="dp4454700s.html#27">XXVIII. Tabernacolo che
              s'innalza sopra l'altare della confessione di San
              Paolo nella sua Basilica ›San Paolo fuori le
              Mura‹</a>
            </li>
            <li>
              <a href="dp4454700s.html#28">XXIX. Scomparti di
              pavimenti esistenti nella Basilica Liberiana [›Santa
              Maria Maggiore‹]</a>
            </li>
            <li>
              <a href="dp4454700s.html#28">XXX. Antico paliotto
              che conservasi sotto l'altare di Sant'Elena ed
              effigie della vergine nella chiesa di Aracoeli ›Santa
              Maria in Aracoeli‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dp4454700s.html#30">Indice e titoli delle
          tavole in musaico esposte nel presente volume</a>
        </li>
        <li>
          <a href="dp4454700s.html#32">[Tavole]</a>
          <ul>
            <li>
              <a href="dp4454700s.html#32">I. Mosaico dell'abside
              della Basilica di ›San Giovanni in Laterano‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#34">II. Prospetto ed
              interno dei musaici che decorano l'abside di ›San
              Clemente‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#36">III. Musaici che
              decorano l'abside e parte superiore de' ›Santi Cosma
              e Damiano‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#38">IV. Abside della
              Basilica Liberiana o di ›Santa Maria Maggiore‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#40">V. Musaici dell'arcone
              fatti eseguire da Sisto III. nella Basilica Liberiana
              [›Santa Maria Maggiore‹] ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#42">VI. Musaico dell'abside
              di ›Santa Maria in Trastevere‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#44">VII. Musaico che decora
              la facciata superiore dell'abside ›Santa Maria in
              Trastevere‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#46">VIII. Musaico
              dell'abside della Basilica di ›Sant'Agnese fuori le
              Mura‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#48">IX. Musaico dell'abside
              di ›Santa Francesca Romana‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#50">X. Musaici che decorano
              la facciata ed interno della cappella della Santa
              Colonna [›Santa Prassede‹] ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#52">XI. Altri particolari
              sui musaici disposti nella stessa cappella della
              Santa Colonna [›Santa Prassede‹] ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#54">XII. Musaico della
              facciata dell'arcone sovraposto all'abside di ›Santa
              Prassede‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#56">XIII. Musaico
              dell'abside di Santa Maria in Domnica detta della
              Navicella ›Santa Maria in Domnica alla Navicella‹
              ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#58">XIV. Musaico
              dell'abside della chiesa di ›Santa Pudenziana‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#60">XV. Musaico che
              decorava l'antica facciata della Basilica Liberiana
              ora rimasto pero decorazione interna al sopra portico
              della loggia [›Santa Maria Maggiore‹] ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#62">XVI. Musaico che si
              ergeva sopra il portico dell'antica facciata di San
              Paolo ora disposto per decorazione interna della
              ricostruita Basilica ›San Paolo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#64">XVII. Musaico che
              decora la facciata dell'arco di placidia nella
              Baslicia di San Paolo ›San Paolo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#66">XVIII. Musaico
              dell'abside della Basilica di San Paolo ›San Paolo
              fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#68">XIX. Musaico
              dell'arcone che decora la Basilica di San Marco di
              Roma ›San Marco‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#70">XX. Musaico dell'antico
              ›Triclinio leoniano‹ ora esistente all'esterno del
              lato meridionale della cappella del Santissimo
              Salvatore alla scala santa ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#72">XXI. Musaico
              dell'abside di ›Santa Cecilia in Trastevere‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#74">XXII. Musaico
              sull'arcone che guarda il presbiterio dell'antica
              Basilica di ›San Lorenzo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#76">XXIII. Musaico della
              Navicella di Giotto ora &#160;situato dicontro alla
              porta maggiore della Basilica Vaticana ›San Pietro in
              Vaticano: Mosaico della Navicella‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#78">XXIV. Musaico al
              disopra dell'abside de' ›Santi Nereo e Achilleo‹,
              sedia pontificale, ed ambone ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#80">XXV. Pulpito o ambone e
              sedia episcopale ambedue esistenti nella Basilica
              ›San Lorenzo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#82">XXVI. Monumento del
              cardinale Consalvo Rodrigo esistente nella nave
              minore a destra della Basilica Liberiana [›Santa
              Maria Maggiore‹] ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#84">XXVII. Dettaglio
              esterno di una perta del chiostro di ›San Giovanni in
              Laterano‹, ossia l'antica canonica ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#86">XXVIII. Tabernacolo che
              s'innalza sopra l'altare della confessione di San
              Paolo nella sua Basilica ›San Paolo fuori le Mura‹
              ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#88">XXIX. Scomparti di
              pavimenti esistenti nella Basilica Liberiana [›Santa
              Maria Maggiore‹] ▣</a>
            </li>
            <li>
              <a href="dp4454700s.html#90">XXX. Antico paliotto
              che conservasi sotto l'altare di Sant'Elena ed
              effigie della vergine nella chiesa di Aracoeli ›Santa
              Maria in Aracoeli‹ ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
