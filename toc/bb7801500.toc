<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.4.0" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="bb7801500s.html#6">Descrittione di tutta Italia
      [...]</a>
      <ul>
        <li>
          <a href="bb7801500s.html#8">A i dui christianissimi
          Henrico Secondo re di Francia et Catherina sua
          consorte</a>
        </li>
        <li>
          <a href="bb7801500s.html#9">Ioannes Antonius Flaminius
          Forocorneliensis Leandro Alberto Bononiensi
          salutem&#160;</a>
        </li>
        <li>
          <a href="bb7801500s.html#10">Herculi Atestio II.
          Illustrissimo Ferrariensi Duci IIII. Anselmus
          Giaccarellus &#160;</a>
        </li>
        <li>
          <a href="bb7801500s.html#14">Tavola della descrittione
          d'Italia</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="bb7801500s.html#70">Descrittione de la Italia di
      Frate Leandro Alberti bolognese dell'ordine de
      Predicatori&#160;</a>
      <ul>
        <li>
          <a href="bb7801500s.html#86">I. Riviera di Genova</a>
          <ul>
            <li>
              <a href="bb7801500s.html#105">Riviera di
              Levante&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#110">II. Thoscana&#160;</a>
          <ul>
            <li>
              <a href="bb7801500s.html#116">Luoghi di Thoscana
              appresso la marina</a>
            </li>
            <li>
              <a href="bb7801500s.html#136">Luoghi di Thoscana fra
              terra</a>
            </li>
            <li>
              <a href="bb7801500s.html#192">Falisci</a>
            </li>
            <li>
              <a href="bb7801500s.html#215">Veienti</a>
            </li>
            <li>
              <a href="bb7801500s.html#219">Tevero fiume</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#223">III. Ducato di Spoleto
          &#160;</a>
          <ul>
            <li>
              <a href="bb7801500s.html#243">Savina</a>
            </li>
            <li>
              <a href="bb7801500s.html#258">Crustumini</a>
            </li>
            <li>
              <a href="bb7801500s.html#259">Fidenati</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#261">IV. Campagna di Roma</a>
          <ul>
            <li>
              <a href="bb7801500s.html#265">Roma del mondo
              imperatrice</a>
            </li>
            <li>
              <a href="bb7801500s.html#275">Porte nuove di
              Roma</a>
            </li>
            <li>
              <a href="bb7801500s.html#286">Li governadori di
              Roma</a>
            </li>
            <li>
              <a href="bb7801500s.html#290">Aureo secolo</a>
            </li>
            <li>
              <a href="bb7801500s.html#298">Luoghi di Campagna di
              Roma appresso la marina&#160;</a>
            </li>
            <li>
              <a href="bb7801500s.html#317">Luoghi fra terra</a>
            </li>
            <li>
              <a href="bb7801500s.html#327">Hernici</a>
            </li>
            <li>
              <a href="bb7801500s.html#333">Ecquicoli</a>
            </li>
            <li>
              <a href="bb7801500s.html#335">Marsi</a>
            </li>
            <li>
              <a href="bb7801500s.html#339">Ecquicoli</a>
            </li>
            <li>
              <a href="bb7801500s.html#347">Gabii</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#351">V. Terra di Lavoro</a>
          <ul>
            <li>
              <a href="bb7801500s.html#360">Cumani</a>
            </li>
            <li>
              <a href="bb7801500s.html#411">Picentini</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#418">VI. Basilicata</a>
        </li>
        <li>
          <a href="bb7801500s.html#426">VII. Calabria</a>
          <ul>
            <li>
              <a href="bb7801500s.html#437">I luoghi fra terra</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#449">VIII. Magna Grecia</a>
          <ul>
            <li>
              <a href="bb7801500s.html#467">I luoghi moderni fra
              terra et il lito del mare</a>
            </li>
            <li>
              <a href="bb7801500s.html#469">Luoghi della
              Basilicata</a>
            </li>
            <li>
              <a href="bb7801500s.html#472">Luoghi di Puglia</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#483">IX. Terra di Otranto</a>
          <ul>
            <li>
              <a href="bb7801500s.html#491">Luoghi posti fra
              terra</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#497">X. Terra di Barri</a>
          <ul>
            <li>
              <a href="bb7801500s.html#504">Luoghi posti fra
              terra</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#509">XI. Puglia piana</a>
          <ul>
            <li>
              <a href="bb7801500s.html#512">Discrittione del monte
              di S. Angelo</a>
            </li>
            <li>
              <a href="bb7801500s.html#516">Capitinata</a>
            </li>
            <li>
              <a href="bb7801500s.html#521">Giapigia</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#525">XII. Abbruzzo</a>
          <ul>
            <li>
              <a href="bb7801500s.html#530">Peligni</a>
            </li>
            <li>
              <a href="bb7801500s.html#536">Vestini</a>
            </li>
            <li>
              <a href="bb7801500s.html#539">Marrhucini</a>
            </li>
            <li>
              <a href="bb7801500s.html#541">Pregutini</a>
            </li>
            <li>
              <a href="bb7801500s.html#545">Hirpini, Sannio</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#561">XIII. Marca Anconitana</a>
          <ul>
            <li>
              <a href="bb7801500s.html#578">Umbri. Senones</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#594">XIV. Romagna</a>
          <ul>
            <li>
              <a href="bb7801500s.html#607">Galli Boii</a>
            </li>
            <li>
              <a href="bb7801500s.html#677">Romagna di là dal
              Po</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#700">XV. Lombardia</a>
          <ul>
            <li>
              <a href="bb7801500s.html#726">Termine de i Boij</a>
            </li>
            <li>
              <a href="bb7801500s.html#732">Doria</a>
            </li>
            <li>
              <a href="bb7801500s.html#740">Doria. Ligures
              Cisapennini. Aemilia</a>
            </li>
            <li>
              <a href="bb7801500s.html#745">Monferrato</a>
            </li>
            <li>
              <a href="bb7801500s.html#757">Nascimento del
              Po&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#762">XVI. Lombardia di la dal
          Po</a>
          <ul>
            <li>
              <a href="bb7801500s.html#806">Rheti. Venonetes</a>
            </li>
            <li>
              <a href="bb7801500s.html#869">Lepontii</a>
            </li>
            <li>
              <a href="bb7801500s.html#870">Riva sinestra del Lago
              Maggiore</a>
            </li>
            <li>
              <a href="bb7801500s.html#876">Libici</a>
            </li>
            <li>
              <a href="bb7801500s.html#879">Salassi. Canaveso</a>
            </li>
            <li>
              <a href="bb7801500s.html#884">Taurini, Pie de monti.
              ducato di Turino</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#887">XVII. Marca Trevigiana</a>
          <ul>
            <li>
              <a href="bb7801500s.html#928">Carni</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#934">XVIII. Ducato di
          Frioli</a>
        </li>
        <li>
          <a href="bb7801500s.html#961">XIX. Histria</a>
          <ul>
            <li>
              <a href="bb7801500s.html#967">Luoghi mediterrani</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801500s.html#969">Discrittione della molto
          magnifica città di Vinegia</a>
          <ul>
            <li>
              <a href="bb7801500s.html#1002">Vescovi et Patriarchi
              de la inclita città di Vinegia</a>
            </li>
            <li>
              <a href="bb7801500s.html#1003">Isole intorno
              Vinegia</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="bb7801500s.html#1007">F. Leandro alli candidi
      lettori</a>
    </li>
  </ul>
  <hr />
</body>
</html>
