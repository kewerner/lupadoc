<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501740s.html#10">Le Cose Maravigliose Dell’Alma
      Città Di Roma</a>
      <ul>
        <li>
          <a href="dg4501740s.html#12">Le sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg4501740s.html#12">I. S. Giovanni
              Laterano</a>
            </li>
            <li>
              <a href="dg4501740s.html#15">II. S. Pietro in
              Vaticano</a>
            </li>
            <li>
              <a href="dg4501740s.html#17">III. San Paolo</a>
            </li>
            <li>
              <a href="dg4501740s.html#18">IV. S. Maria
              Maggiore</a>
            </li>
            <li>
              <a href="dg4501740s.html#19">V.S. Lorenzo for delle
              mura&#160;</a>
            </li>
            <li>
              <a href="dg4501740s.html#20">VI. S. Sebastiano</a>
            </li>
            <li>
              <a href="dg4501740s.html#20">VII. S. Croce in
              Gierusalem</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501740s.html#21">Nell'isola</a>
        </li>
        <li>
          <a href="dg4501740s.html#21">In Trastevere</a>
        </li>
        <li>
          <a href="dg4501740s.html#23">Nel Borgo</a>
        </li>
        <li>
          <a href="dg4501740s.html#25">Della Porta Flaminia fuori
          del Popolo fino alle radici del Campidoglio</a>
        </li>
        <li>
          <a href="dg4501740s.html#34">Del Campidoglio a man
          sinistra verso li monti</a>
        </li>
        <li>
          <a href="dg4501740s.html#39">Dal Campidoglio a man
          dritta verso li Monti</a>
        </li>
        <li>
          <a href="dg4501740s.html#44">Tavola delle chiese</a>
        </li>
        <li>
          <a href="dg4501740s.html#46">Le stationi che sono nelle
          chiese di Roma, si per la Quadragesima, come per tutto
          l'anno, con le solite indulgentie</a>
        </li>
        <li>
          <a href="dg4501740s.html#57">La guida romana per li
          forastieri che vengono per vedere le antichità di Roma, a
          una per una, in bellissima forma e brevità</a>
          <ul>
            <li>
              <a href="dg4501740s.html#57">Prima giornata</a>
            </li>
            <li>
              <a href="dg4501740s.html#61">Giornata seconda</a>
            </li>
            <li>
              <a href="dg4501740s.html#65">Giornata terza</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501740s.html#67">Summi Pontifices</a>
        </li>
        <li>
          <a href="dg4501740s.html#84">Reges et Imperatores
          romani</a>
        </li>
        <li>
          <a href="dg4501740s.html#88">Li Re di Francia</a>
        </li>
        <li>
          <a href="dg4501740s.html#89">Li Re del Regno di Napoli e
          di Sicilia, li quali cominciono a regnare l'anno di
          nostra salute&#160;</a>
        </li>
        <li>
          <a href="dg4501740s.html#90">Li Dugi di Venegia</a>
        </li>
        <li>
          <a href="dg4501740s.html#92">Li Duchi di Milano</a>
        </li>
        <li>
          <a href="dg4501740s.html#94">L'Antichità di Roma</a>
          <ul>
            <li>
              <a href="dg4501740s.html#95">Alli lettori</a>
            </li>
            <li>
              <a href="dg4501740s.html#96">[L'Antichità di
              Roma]</a>
            </li>
            <li>
              <a href="dg4501740s.html#133">Tavola delle Antichità
              della città di Roma</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
