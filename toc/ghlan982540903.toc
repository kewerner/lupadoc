<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlan982540903s.html#8">Storia pittorica della
      Italia; tomo III. Ove si descrive la scuola veneziana</a>
      <ul>
        <li>
          <a href="ghlan982540903s.html#10">Compartimento di
          questo tomo terzo</a>
        </li>
        <li>
          <a href="ghlan982540903s.html#10">Della Storia
          pittorica della Italia superiore</a>
        </li>
        <li>
          <a href="ghlan982540903s.html#12">I. Scuola
          veneziana</a>
          <ul>
            <li>
              <a href="ghlan982540903s.html#16">I. Gli
              antichi&#160;</a>
            </li>
            <li>
              <a href="ghlan982540903s.html#79">II. Giorgione,
              Tiziano, il Tintoretto, Jacopo da Bassano, Paolo
              Veronese&#160;</a>
            </li>
            <li>
              <a href="ghlan982540903s.html#203">III. I
              Manieristi nel secolo XVII guastano la pittura
              veneta&#160;</a>
            </li>
            <li>
              <a href="ghlan982540903s.html#271">IV. Stili
              esteri e nuovi in Venezia&#160;</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
