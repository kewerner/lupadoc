<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="emil113870s.html#6">Nuova guida di Milano per gli
      amanti delle belle arti e delle sacre, e profane antichità
      milanesi</a>
      <ul>
        <li>
          <a href="emil113870s.html#8">Al lettore</a>
        </li>
        <li>
          <a href="emil113870s.html#12">Ristretto storico della
          città</a>
        </li>
        <li>
          <a href="emil113870s.html#22">Duomo</a>
        </li>
        <li>
          <a href="emil113870s.html#71">Porta Orientale</a>
        </li>
        <li>
          <a href="emil113870s.html#103">Porta Romana</a>
        </li>
        <li>
          <a href="emil113870s.html#195">Porta Ticinese</a>
        </li>
        <li>
          <a href="emil113870s.html#261">Porta Vercellina</a>
        </li>
        <li>
          <a href="emil113870s.html#350">Porta Comasina</a>
        </li>
        <li>
          <a href="emil113870s.html#374">Porta Nuova</a>
        </li>
        <li>
          <a href="emil113870s.html#458">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
