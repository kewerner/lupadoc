<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cagam5604070s.html#8">Della vita e delle pitture
      di Lattanzio Gambara</a>
      <ul>
        <li>
          <a href="cagam5604070s.html#10">Alli Signori Tommaso
          Balucanti vice podestà</a>
        </li>
        <li>
          <a href="cagam5604070s.html#16">Della vita e delle
          pitture di Lattanzio Gambara</a>
        </li>
        <li>
          <a href="cagam5604070s.html#140">Brevi notizie intorno
          a' più celebri pittori bresciani&#160;</a>
        </li>
        <li>
          <a href="cagam5604070s.html#214">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
