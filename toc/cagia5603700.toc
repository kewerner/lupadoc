<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cagia5603700s.html#4">Vita di Silvestro Giannotti
      Lucchese Intagliatore, e Statuario in Legno als nobilissimo
      Signore Tommaso Francesco Bernardi Patrizio Lucchese</a>
      <ul>
        <li>
          <a href="cagia5603700s.html#6">[Dedica]</a>
        </li>
        <li>
          <a href="cagia5603700s.html#12">Vita di Silvestro
          Giannotti Lucchese Intagliatore, e Statuario in Legno</a>
        </li>
        <li>
          <a href="cagia5603700s.html#26">Supplica di Silvestro
          Giannotti etc. letta nell'eccellentissimo Consiglio di
          Lucca a di 29. Decembre 1740</a>
        </li>
        <li>
          <a href="cagia5603700s.html#28">Memoriale dello
          spettabile, e magnifico Offizio sopra le nuove Arti, che
          accompagna detta Supplica</a>
        </li>
        <li>
          <a href="cagia5603700s.html#30">Decreto</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
