<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503507s.html#8">Roma ricercata nel suo sito</a>
      <ul>
        <li>
          <a href="dg4503507s.html#10">[Text]</a>
          <ul>
            <li>
              <a href="dg4503507s.html#10">Giornata Prima</a>
              <ul>
                <li>
                  <a href="dg4503507s.html#10">Da ›Ponte
                  Sant'Angelo‹ à ›San Pietro in Vaticano‹</a>
                  <ul>
                    <li>
                      <a href="dg4503507s.html#10">[›Castel
                      Sant'Angelo‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#16">[San Pietro in
                      Vaticano‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#20">[›San Pietro in
                      Vaticano: Baldacchino di San Pietro‹] ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503507s.html#34">Giornata Seconda</a>
              <ul>
                <li>
                  <a href="dg4503507s.html#34">Da ›Santo Spirito
                  in Sassia‹ per il ›Trastevere‹</a>
                  <ul>
                    <li>
                      <a href="dg4503507s.html#40">[›Fontana
                      dell'Acqua Paola (via Garibaldi)‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#50">[›Porto di Ripa
                      Grande‹] ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503507s.html#55">Giornata Terza</a>
              <ul>
                <li>
                  <a href="dg4503507s.html#55">Da Strada Giulia
                  ›Via Giulia‹ all'Isola di San Bartolomeo ›Isola
                  Tiberina‹</a>
                  <ul>
                    <li>
                      <a href="dg4503507s.html#58">[›Palazzo
                      Falconieri‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#60">[›Fontana
                      dell'Acqua Paola (Piazza Trilussa)‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#68">[›Isola
                      Tiberina‹] ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503507s.html#70">Giornata Quarta</a>
              <ul>
                <li>
                  <a href="dg4503507s.html#70">Da ›San Lorenzo in
                  Damaso‹ &#160;al Monte Aventino</a>
                  <ul>
                    <li>
                      <a href="dg4503507s.html#70">›Palazzo della
                      Cancelleria‹ ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#78">›Porta San
                      Paolo‹ ▣ [›Piramide di Caio Cestio‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#80">›San Paolo
                      fuori le Mura‹ ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503507s.html#86">Giornata Quinta</a>
              <ul>
                <li>
                  <a href="dg4503507s.html#86">Dalla Piazza di
                  ›Monte Giordano‹ per li Monti ›Celio‹, e
                  ›Palatino‹</a>
                  <ul>
                    <li>
                      <a href="dg4503507s.html#86">[›Piazza di
                      Pasquino‹] &#160;▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#86">[›Statua di
                      Pasquino‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#90">[›Teatro di
                      Marcello‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#95">[›San
                      Sebastiano‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#99">›San Giovanni
                      in Laterano‹ ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503507s.html#111">Giornata Sesta</a>
              <ul>
                <li>
                  <a href="dg4503507s.html#111">Da ›San Salvatore
                  in Lauro‹ per ›Campidoglio‹, e per le
                  ›Carinae‹</a>
                  <ul>
                    <li>
                      <a href="dg4503507s.html#113">[›Fontana dei
                      Fiumi‹] ▣</a>
                    </li>
                    <li>
                      <a href=
                      "dg4503507s.html#118">[›Campidoglio‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#123">[›Foro
                      Romano‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#128">[›Colosseo‹]
                      ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#134">[›Colonna
                      Traiana‹] ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503507s.html#137">Giornata Settima</a>
              <ul>
                <li>
                  <a href="dg4503507s.html#137">Dalla ›Piazza di
                  Sant'Agostino‹ per il Monte ›Viminale‹, e
                  ›Quirinale‹</a>
                  <ul>
                    <li>
                      <a href="dg4503507s.html#140">[›San Luigi
                      dei Francesi‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#141">[›Palazzo
                      Madama‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#145">[›Collegio
                      Romano‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#151">›Santa Croce
                      in Gerusalemme‹ ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#153">[›San Lorenzo
                      fuori le Mura‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#158">›Santa Maria
                      Maggiore‹ ▣</a>
                    </li>
                    <li>
                      <a href=
                      "dg4503507s.html#168">[›Sant'Ignazio‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#169">[›Pantheon‹]
                      ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503507s.html#172">Giornata Ottava</a>
              <ul>
                <li>
                  <a href="dg4503507s.html#172">Dalla Strada
                  dell'Orso ›Via dell'Orso‹ a Monte Cavallo
                  ›Quirinale‹, e alle Terme Diocleziane ›Terme di
                  Diocleziano‹</a>
                </li>
                <li>
                  <a href="dg4503507s.html#174">[›Dogana nuova di
                  Terra‹]</a>
                </li>
                <li>
                  <a href="dg4503507s.html#175">[›Fontana di
                  Trevi‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503507s.html#177">[›Palazzo del
                  Quirinale‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503507s.html#180">[›Palazzo della
                  Consulta‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503507s.html#187">Giornata Nona</a>
              <ul>
                <li>
                  <a href="dg4503507s.html#187">Dal ›Palazzo
                  Borghese‹, a ›Porta del Popolo‹, e a ›Piazza di
                  Spagna‹</a>
                  <ul>
                    <li>
                      <a href="dg4503507s.html#188">[›Palazzo
                      Borghese‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#190">[›Porto di
                      Ripetta‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#190">[›San Girolamo
                      degli Illirici‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#192">[›Santa Maria
                      dei Miracoli‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#192">[›Santa Maria
                      di Montesanto‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#192">[›Fontana di
                      Piazza del Popolo / Obelisco Flaminio‹] ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503507s.html#205">Giornata Decima</a>
              <ul>
                <li>
                  <a href="dg4503507s.html#205">Dal Monte Citorio
                  ›Piazza di Montecitorio‹ alla ›Porta Pia‹, e al
                  Monte ›Pincio‹</a>
                  <ul>
                    <li>
                      <a href="dg4503507s.html#205">[›Palazzo di
                      Montecitorio‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#207">[›Colonna di
                      Antonino Pio‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#210">[›Palazzo
                      Barberini‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503507s.html#220">›Trinità dei
                      Monti‹ ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503507s.html#222">Indice</a>
            </li>
            <li>
              <a href="dg4503507s.html#231">Cronologia di tutti li
              sommi Pontefici</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
