<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="camar1252850s.html#7">[Titelblatt] FIORI D'INGEGNO
      Composizioni In lode d'una belissima Effigie Di PRIMAVERA
      Opera del Signor CARLO MARATI Famoso Pittore Romano, Apresso
      S.E. il Signor NICCOLO' MICHIELI SENATORE VENETO. Raccolti da
      GIO: BATTISTA MAGNAVINI Cittad. Ven. Accad. Dodoneo, E
      consagrati all' Altezza Serenissima D' ALESSANDRO PICO Duca
      della Mirandola, Concordia e c. IN VENEZIA, M.DC.LXXXV.
      Presso Paolo Baglioni. CON LICENZA DE' SUPERIORI</a>
    </li>
    <li>
      <a href="camar1252850s.html#11">[Widmung] Serenissima
      Altezza</a>
    </li>
    <li>
      <a href="camar1252850s.html#16">[Hinweis des Autors]
      Avvertimento a' lettori</a>
    </li>
    <li>
      <a href="camar1252850s.html#17">[Vorwort] LA FILOSOFIA
      DELL' PENNELLO OVERO discorso intorno al significato Fisico e
      Mitologico DELLA PRIMAVERA DEL SIGNOR CARLO MARATI DI GIO:
      BATTISTA MAGNAVINI</a>
    </li>
    <li>
      <a href="camar1252850s.html#43">[Sonett 1] Per una
      vaghissima Primavera Opera del Signor CARLO MARATI. SONETTO.
      Del Signor ADRIANO MORSELLI Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#44">[Sonett 2] Per la stessa
      Dipinta in tempo d' Inverno. SONETTO. Del medesimo</a>
    </li>
    <li>
      <a href="camar1252850s.html#45">[Sonett 3] Nello stesso
      Soggetto. SONETTO. Del medesimo</a>
    </li>
    <li>
      <a href="camar1252850s.html#46">[Sonett 4] Per la stessa
      Arrivata à Venetia ne' più rigorosi freddi del Verno.
      SONETTO. Del Signor Alvise Basadonna NOB. VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#47">[Sonett 5] Nello stesso
      Soggetto. SONETTO. Dello stesso</a>
    </li>
    <li>
      <a href="camar1252850s.html#48">[Sonett 6] Nel medesimo
      Soggetto. SONETTO. Del Signor ANTONIO OTTOBON NOB. VEN.
      Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#49">[Sonett 7] Nello stesso
      soggetto. SONETTO. Del Signor ANDREA CATANEO.</a>
    </li>
    <li>
      <a href="camar1252850s.html#50">[Sonett 8] Nello stesso
      Soggetto. SONETTO. Del medesimo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#51">[Sonett 9] Nello stesso
      Soggetto. SONETTO. Del medesimo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#52">[Sonett 10] Al Signor CARLO
      MARATI Per la stessa sua Primavera. SONETTO. Del
      medesimo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#53">[Sonett 11] Per la medesima
      SONETTO. Del Signor ALESSANDRO MARIA VIANOLI NOB. VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#54">[Sonett 12] La Primavera
      Pittura del SignorCarlo Marati giunta à Venzia in tempo d'
      Inverno. SONETTO. Del Signor ANTONIO GIUSTI. Accademico
      Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#55">[Sonett 13] Nello stesso
      Soggetto. SONETTO. Del medesimo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#56">[Sonett 14] Per la stessa
      Arrivata à Venetia in tempo d' Inverno. SONETTO. Del Signor
      ALLESSANDRO CARIOLATO.</a>
    </li>
    <li>
      <a href="camar1252850s.html#57">[Sonett 15] Nello stesso
      Soggetto. SONETTO. Del Signor Dottor ANTONIO ARCOLEO.</a>
    </li>
    <li>
      <a href="camar1252850s.html#58">[Sonett 16] Nel medesimo
      Soggetto. SONETTO. Del Signor ALUISE GARZONI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#59">[Sonett 17] Nel medesimo
      Soggetto. A S.E. Il Signor NICOLO MICHIELI. SONETTO. Del
      Signor Cavalier AURELIO AMALTEO. Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#60">[Sonett 18] Nel medesimo
      Soggetto. SONETTO. Del Signor BERNARDO TRIUISAN. NOB.
      VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#61">[Sonett 19] Nel medesimo
      Soggetto. Al Signor CARLO MARATI. SONETTO. Del Signor CO:
      CARLO DE' DOTTORI Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#62">[Sonett 20] Nel medesimo
      Soggetto. SONETTO. Del Signor CONSTANTINO MICHIELI NOB.
      VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#63">[Sonett 21] Nel medesimo
      Soggetto. SONETTO. Del Signor C. M. NOB. VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#64">[Sonett 22] Nello stesso
      Soggetto. SONETTO. Del Signor DOMENICO DAVID Accademico
      Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#65">[Sonett 23] Nel medesimo
      Soggetto. A S.E. Il Signor NICOLÒ MICHELI. SONETTO. Del
      Signor ENRICO ALTANI CO: DI SALVAROLO. Accademico
      Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#66">[Sonett 24] Per la stessa.
      SONETTO. Di Sua Altezza Il Sig. Prencipe FRANCESCO PICO DELLA
      MIRANDOLA.</a>
    </li>
    <li>
      <a href="camar1252850s.html#67">[Sonett 25] Nel medesimo
      Soggetto. SONETTO. Del Signor FRANCESCO CROTA NOB. VEN.
      Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#68">[Sonett 26] Per la stessa
      PRIMAVERA Giunta à Venetia d' Inverno. SONETTO. Del Signor
      FRANCESCO TEBALDI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#69">[Sonett 27] Al Signor CARLO
      MARATI Per la sua Primavera. SONETTO. Del Signor Baron
      FERDINANDO TURRIANO DE TASSIS. Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#70">[Sonett 28] Nel medesimo
      Soggetto. SONETTO. Del Signor Ab. FELICE VIALE Accademico
      Ricovrato.</a>
    </li>
    <li>
      <a href="camar1252850s.html#71">[Sonett 29] Nello stesso
      Soggetto. SONETTO. Del Signor FRANCESCO MILIATI ROMANO.</a>
    </li>
    <li>
      <a href="camar1252850s.html#72">[Sonett 30] Nel medesimo
      Soggetto. SONETTO. Dello stesso.</a>
    </li>
    <li>
      <a href="camar1252850s.html#73">[Sonett 31] Nello stesso
      Soggetto. SONETTO. Del Signor FRANCESCO CUCHI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#74">[Sonett 32] Per la stessa
      PRIMAVERA SONETTO. Dello stesso</a>
    </li>
    <li>
      <a href="camar1252850s.html#75">[Sonett 33] Nello stesso
      Soggetto. A. S.E. IL Signor NICOLO MICHIELI SONETTO. Di sua
      Altezza Il Sig. Prencipe GIOVANNI PICO DELLA MIRANDOLA.</a>
    </li>
    <li>
      <a href="camar1252850s.html#76">[Sonett 34] Nel medesimo
      Soggetto. SONETTO. Del Signor N. N. NOB. VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#77">[Sonett 35] Nel medesimo
      Soggetto. A S.E. Il Signor NICOLO MICHIELI. SONETTO. Dello
      stesso.</a>
    </li>
    <li>
      <a href="camar1252850s.html#78">[Sonett 36] Per la stessa
      PRIMAVERA SONETTO. Del Signor GIOVANNI QUERINI Accademico
      Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#79">[Sonett 37] PRIMAVERA Del
      Signor CARLO MARATI Capitata in Venezia in tempo d' Inverno,
      essendo Neve, e Ghiaccio in Terra. SONETTO. Del Signor GIO:
      MATTEO GIANNINI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#80">[Sonett 38] Nello stesso
      Soggetto. SONETTO. Del medesimo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#81">[Sonett 39] Nello stesso
      Soggetto SONETTO. Del Signor CO: GIROLAMO FRIGIMELICA
      ROBERTI. Accademico Ricovrato.</a>
    </li>
    <li>
      <a href="camar1252850s.html#82">[Sonett 40] Per la
      bellissima PRIMAVERA Del Signor CARLO MARATI Arrivata d'
      Inverno à Venezia. SONETTO. Del Signor GIACOPO GRANDI.
      Professore publico di Notomia.</a>
    </li>
    <li>
      <a href="camar1252850s.html#83">[Sonett 41] Motivo morale,
      per cui il Signor CARLO MARATI Pittore celebratissimo di-
      pinse nell' Inverno presente una belissima Primavera.
      SONETTO. Del Signor Ab. GG. Nob. VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#84">[Sonett 42] Nello stesso
      Soggetto. SONETTO. Del Signor GIROLAMO ZOLIO NOB. VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#85">[Sonett 43] Nel medesimo
      Soggetto. SONETTO. Del Signor GIO: BATTISTA ROTA NOB. VEN.
      Accademico Dodoneo, e Ricovrato</a>
    </li>
    <li>
      <a href="camar1252850s.html#86">[Sonett 44] Nel medesimo
      Soggetto. A S. E. Il Signor NICOLO' MICHEIL [MICHIELI]
      SONETTO. Del Signor Dottor GIACOPO MAZZI Academico
      Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#87">[Sonett 45] Per la stessa
      PRIMAVERA Capitata à Venetia in tempo d' Inverno. SONETTO.
      Del Signor Ab. GIUSEPPE CAPITANIO Accademico Ricovrato.</a>
    </li>
    <li>
      <a href="camar1252850s.html#88">[Sonett 46] A S. E. Il
      Signor NICOLO MICHIEL Per occasione di una Primavera
      perventuale dà Roma di mano del Signor CARLO MARATI in tempo
      d' Inverno. SONETTO. Del Signor GIUSEPPE CUCHI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#89">[Sonett 47] Per la stessa.
      SONETTO. Del Signor GIULIO AVELLINO PITTORE.</a>
    </li>
    <li>
      <a href="camar1252850s.html#90">[Sonett 48] Nel medesimo
      Soggetto. SONETTO. Dello stesso.</a>
    </li>
    <li>
      <a href="camar1252850s.html#91">[Sonett 49] Nello stesso
      Soggetto. SONETTO. Del Signor Dottor GIO: BATTISTA
      CIASSI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#92">[Sonett 50] Nello stesso
      Soggetto. SONETTO. Del Signor GIROLAMO CASTELLI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#93">[Sonett 51] Nello stesso
      Soggetto. SONETTO. Di S.E. Il Signor Cavalier GIROLAMO
      ZEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#94">[Sonett 52] Nello stesso
      Soggetto. SONETTO. Del Del Signor Ab. GENESIO SODERINI NOB.
      VEN. Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#95">[Sonett 53] Nel medesimo
      Soggetto. SONETTO. Del Signor GIOSEPPE TERZI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#96">[Sonett 54] Nello stesso
      Soggetto. SONETTO. DI GIO: BATTISTA MAGNAVINI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#97">[Sonett 55] Coll' occasione
      della stessa PRIMAVERA A S.E. Il Signor NICOLO' MICHIEL.
      SONETTO. Dello stesso.</a>
    </li>
    <li>
      <a href="camar1252850s.html#98">[Sonett 56] Per la stessa
      SONETTO. Tradotto dall' Epigramma, che comincia: Florida nisa
      diù est natura emittere faetum et c. Di Monsignor GIO:
      FRANCESCO ROTA Referendario dell' una, e l' altra
      Segnatura.</a>
    </li>
    <li>
      <a href="camar1252850s.html#99">[Sonett 57] Per la stessa
      SONETTO. Del Signor LAZARO FERRO NOB. VEN. Accademico
      Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#100">[Sonett 58] Per la
      medesima SONETTO. Del Signor CO: LELIO PIOVENE NOB. VEN.
      Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#101">[Sonett 59] Nello stesso
      Soggetto. SONETTO. Del medesimo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#102">[Sonett 60] Nel medesimo
      Soggetto. SONETTO. Del Signor MATTEO NORIS.</a>
    </li>
    <li>
      <a href="camar1252850s.html#103">[Sonett 61] Nello stesso
      Soggetto. SONETTO. Del Signor MEDICO MONDINI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#104">[Sonett 62] Nello stesso
      Soggetto. SONETTO. Del medesimo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#105">[Sonett 63] Nello stesso
      Soggetto SONETTO. Del Signor NICOLA BEREGAN. NOB. VEN.
      Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#106">[Sonett 64] Nel medesimo
      Soggetto SONETTO. Del Signor NICCOLO' BERLENDIS NOB. VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#107">[Sonett 65] Per la stessa
      PRIMAVERA. Giunta à Venezia in tempo d' Inverno. SONETTO. Del
      Signor N. M.</a>
    </li>
    <li>
      <a href="camar1252850s.html#108">[Sonett 66] Nello stesso
      Soggetto SONETTO. Dello stesso.</a>
    </li>
    <li>
      <a href="camar1252850s.html#109">[Sonett 67] Nello stesso
      Soggetto SONETTO. Del medesimo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#110">[Sonett 68] Nel medesimo
      Soggetto. SONETTO. Del Signor CO: OGNIBEN SECCO.</a>
    </li>
    <li>
      <a href="camar1252850s.html#111">[Sonett 69] Nello stesso
      Soggetto. SONETTO. Del Signor PELEGRIN ZAGURI. NOB. VEN.
      Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#112">[Sonett 70] Per la stessa
      PRIMAVERA Del Signor CARLO MARATI SONETTO. Del Sig. Canonico
      PIETRO BELTRAME Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#113">[Sonett 71] Per la stessa
      PRIMAVERA SONETTO. Del Signor PIEVAN DI S. MARCILIANO.
      Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#114">[Sonett 72] Per la stessa
      PRIMAVERA SONETTO. Del Signor Dottor PIETRO MARCELLO.
      Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#115">[Sonett 73] Nello stesso
      Soggetto SONETTO. Dello stesso.</a>
    </li>
    <li>
      <a href="camar1252850s.html#116">[Sonett 74] Nel medesimo
      Soggetto SONETTO. Del Signor PIETRO MAFETTI NOB. VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#117">[Sonett 75] Nello stesso
      Soggetto. SONETTO. Del Signor POLO LOREDANO NOB. VEN.
      Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#118">[Sonett 76] Per la stessa
      PRIMAVERA SONETTO. Del Signor PICCOLI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#119">[Sonett 77] Per la stessa
      PRIMAVERA Del Signor CARLO MARATI SONETTO. Del Signor
      MARCHESE SAGRAMOSO.</a>
    </li>
    <li>
      <a href="camar1252850s.html#120">[Sonett 78] Per la stessa
      PRIMAVERA SONETTO. Del Signor Ab. SANTI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#121">[Sonett 79] Nello stesso
      Soggetto Al Signor GIO: BATTISTI MAGNAVINI SONETTO. Del
      Signor SANDRINELLI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#122">[Sonett 80] Nello stesso
      Soggetto SONETTO. Del Signor Dottor TEBALDO FATTORINI
      Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#123">[Sonett 81] Per la stessa
      SONETTO. Del Signor FRANCESCO TEBALDI. Omesso nella lettera
      F.</a>
    </li>
    <li>
      <a href="camar1252850s.html#124">[Sonett 82] Per la
      medesima PRIMAVERA SONETTO. Del Sitgnor V. G. NOB. VEN.</a>
    </li>
    <li>
      <a href="camar1252850s.html#125">[Sonett 83] Nello stesso
      Soggetto SONETTO. Del medesimo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#126">[Sonett 84] Nello stesso
      Soggetto. SONETTO. Del Signor VETTO SANDI NOB. VEN.
      Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#127">[Sonett 85] Nello stesso
      Soggetto. SONETTO. Del Signor VICENZO PASINI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#128">[Sonett 86] Per la stessa
      PRIMAVERA SONETTO. Del Sig. Canonico VINCENZO TODESCHINI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#129">[Sonett 87] Per la stessa
      PRIMAVERA SONETTO. In lingua Rustica. DI TAPAROTTO DE
      RUSSIGNATI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#130">[Madrigal 1] Nel medesimo
      Soggetto. MADRIGALE. D'Incerto.</a>
    </li>
    <li>
      <a href="camar1252850s.html#131">[Madrigal 2] Nello stesso
      Soggetto. MADRIGALE. D'Incerto.</a>
    </li>
    <li>
      <a href="camar1252850s.html#132">[Ode 1] Il Verno Fiorito.
      Per un' effigie di Primavera del Signor CARLO MARATI,
      tramessa d'Inverno A S.E. Il Signor NICOLO MICHELI ODA. Del
      Signor ADRIANO CHESINI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#136">[Ode 2] Nello stesso
      Sogetto ODA. Del Signor CHESINI Medico di Castelfranco.</a>
    </li>
    <li>
      <a href="camar1252850s.html#140">[Ode 3] Nello stesso
      Sogetto ODA. Del Signor Dottor DOMENICO VETTORAZZI</a>
    </li>
    <li>
      <a href="camar1252850s.html#145">[Ode 4] Per la stessa
      PRIMAVERA Del Signor CARLO MARATI ODA. Del Signor Ab. GENESIO
      SODERINI NOB. VEN. Accademico Dodoneo.</a>
    </li>
    <li>
      <a href="camar1252850s.html#154">[Ode 5] VENERE in
      fembianza di PRIMAVERA Di mano del Signor CARLO MARATI ODA.
      DI GIO: BATTISTA MAGNAVINI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#161">[Epigramm 1] De eximia
      VERIS Tabula, quam CAROLUS MARATI NICOLAO MICHAELI SENATORI
      VENETO ELOQUENTISSIMO Pinxit, [et] hyemali tempore Venetias
      misit. EPIGRAMMA. FABIILIO.</a>
    </li>
    <li>
      <a href="camar1252850s.html#162">[Epigramm 2] In eandem
      EPIGRAMMA. Eiusdem.</a>
    </li>
    <li>
      <a href="camar1252850s.html#163">[Epigramm 3] In Veris
      Tabulam A CAROLO MARATO Pictore celeberrimo AD NICOLAUM
      MICHAELIUM Veneti Senatus Demosthenem, missam. EPIGRAMMA.
      IACOBI GRANDII Pub. Anotomes Professoris.</a>
    </li>
    <li>
      <a href="camar1252850s.html#164">[Epigramm 4] De eadem
      EPIGRAMMA IACOBI IANOLE [und] De eadem EPIGRAMMA Eiusdem</a>
    </li>
    <li>
      <a href="camar1252850s.html#165">[Epigramm 5] In eandem,
      cùm Venetias perlata, Hyems recruduisset. EPIGRAMMA IOANNIS
      BAPT. MAGNAVINII</a>
    </li>
    <li>
      <a href="camar1252850s.html#166">[Epigramm 6] In eandem
      EPIGRAMMA IOANNIS FRANCISCI ROTAE</a>
    </li>
    <li>
      <a href="camar1252850s.html#167">[Epigramm 7] In eandem
      EPIGRAMMA Eiusdem</a>
    </li>
    <li>
      <a href="camar1252850s.html#168">[Epigramm 8 und 9] In
      eandem EPIGRAMMA Eiusdem In eandem EPIGRAMMA Eiusdem</a>
    </li>
    <li>
      <a href="camar1252850s.html#169">[Epigramm 10 in Griechisch
      mit lateinischer Übersetzung] In eandem EPIGRAMMA NICOLAI
      BUBULII Phil. et Med. D. Idem ex Graeco</a>
    </li>
    <li>
      <a href="camar1252850s.html#170">[Epigramm 11] In eandem
      EPIGRAMMA SEBASTIANI BERNARDI</a>
    </li>
    <li>
      <a href="camar1252850s.html#171">[Epigramm 12] In eandem
      EPIGRAMMA SIMONIS LINAROLII</a>
    </li>
    <li>
      <a href="camar1252850s.html#172">[Ode 6] Venus sub Imagine
      Veris, cùm Geminis Amoribus, in Tabula Apud NICOLAUM
      MICHAELIUM SENATOREM VENETUM, OPUS CAROLI MARATI. ODE MARCI
      ANT: FRANCHINI.</a>
    </li>
    <li>
      <a href="camar1252850s.html#174">[Elogium] In eandem Veris
      Tabulam ELOGIUM NICOLAI BON Acad. Dodonei, I.V.D.</a>
    </li>
  </ul>
  <hr />
</body>
</html>
