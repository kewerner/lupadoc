<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="epis1138701s.html#6">Pisa Illustrata Nelle Arti
      Del Disegno; Tomo I.</a>
      <ul>
        <li>
          <a href="epis1138701s.html#8">Prefazione</a>
        </li>
        <li>
          <a href="epis1138701s.html#40">Indice</a>
        </li>
        <li>
          <a href="epis1138701s.html#44">I. Duomo</a>
        </li>
        <li>
          <a href="epis1138701s.html#256">II. San Giovanni</a>
        </li>
        <li>
          <a href="epis1138701s.html#292">III. Campanile</a>
        </li>
        <li>
          <a href="epis1138701s.html#312">IV. Campo Santo</a>
        </li>
        <li>
          <a href="epis1138701s.html#427">V. Fabbriche disposte
          nel giro della Piazza del Duomo</a>
        </li>
        <li>
          <a href="epis1138701s.html#435">VI. Saggio sull'arte
          pisana de' tempi di mezzo</a>
        </li>
        <li>
          <a href="epis1138701s.html#476">Ordine alfabetico de'
          prefessori del Disegno</a>
        </li>
        <li>
          <a href="epis1138701s.html#484">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
