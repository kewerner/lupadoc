<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="emil163390s.html#6">Distinto ragguaglio
      dell’ottava maraviglia del mondo o sia della gran
      metropolitana dell’insubria, volgarmente detta il Duomo di
      Milano, cominciando dalla sua origine sino allo stato
      presente ...</a>
      <ul>
        <li>
          <a href="emil163390s.html#8">Emminentissimo
          Principe&#160;</a>
        </li>
        <li>
          <a href="emil163390s.html#12">Lo stampatore a chi
          legge</a>
        </li>
        <li>
          <a href="emil163390s.html#16">Indice de' capitoli</a>
        </li>
        <li>
          <a href="emil163390s.html#18">Distinto ragguaglio
          dell’ottava maraviglia del mondo o sia della gran
          metropolitana dell’insubria, volgarmente detta il Duomo
          di Milano</a>
        </li>
        <li>
          <a href="emil163390s.html#236">Indice delle cose
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
