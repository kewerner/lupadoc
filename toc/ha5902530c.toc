<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ha5902530cs.html#8">Mondo simbolico formato
      d’imprese scelte, spiegate, ed illustrate con sentenze, ed
      eruditioni, sacre, e profane, che somministrano à gli
      oratori, predicatori, accademici, poeti, et c. infinito
      numero di concetti</a>
      <ul>
        <li>
          <a href="ha5902530cs.html#10">All'eminentissimo
          Principe il Signor Cardinale Carlo Barberini</a>
        </li>
        <li>
          <a href="ha5902530cs.html#12">Lettore</a>
        </li>
        <li>
          <a href="ha5902530cs.html#14">Compendioso Trattato
          della Natura dell'Imprese&#160;</a>
        </li>
        <li>
          <a href="ha5902530cs.html#20">Ordine i sia dispositione
          del Mondo Simbolico Ampliato&#160;</a>
        </li>
        <li>
          <a href="ha5902530cs.html#24">Indice alfabetico de i
          Corpi usati nell'Imprese, del Mondo Simbolico
          Ampliato</a>
        </li>
        <li>
          <a href="ha5902530cs.html#28">Autori d'Imprese.
          Concorsi alla formatione del Mondo Simbolico Ampliato</a>
        </li>
        <li>
          <a href="ha5902530cs.html#32">Del Mondo Simbolico
          Ampliato</a>
          <ul>
            <li>
              <a href="ha5902530cs.html#32">I. Corpi
              celesti&#160;</a>
            </li>
            <li>
              <a href="ha5902530cs.html#72">II. Elementi</a>
            </li>
            <li>
              <a href="ha5902530cs.html#123">III. Dei de gli
              Antichi, Eroi, et Huomini, con loro attenenti</a>
            </li>
            <li>
              <a href="ha5902530cs.html#137">IV. Ucelli, e loro
              attenenti</a>
            </li>
            <li>
              <a href="ha5902530cs.html#197">V. Quadrupedi, e
              loro attenenti&#160;</a>
            </li>
            <li>
              <a href="ha5902530cs.html#266">VI. Pesci</a>
            </li>
            <li>
              <a href="ha5902530cs.html#299">VII. Serpenti, et
              animali velenosi</a>
            </li>
            <li>
              <a href="ha5902530cs.html#314">VIII. Animali
              imperfetti</a>
            </li>
            <li>
              <a href="ha5902530cs.html#342">IX. Piante, frutti,
              e loro attenenti</a>
            </li>
            <li>
              <a href="ha5902530cs.html#398">X. Erbe</a>
            </li>
            <li>
              <a href="ha5902530cs.html#419">XI. Fiori</a>
            </li>
            <li>
              <a href="ha5902530cs.html#443">XII. Gemme, e
              pietre</a>
            </li>
            <li>
              <a href="ha5902530cs.html#477">XIII. Metalli</a>
            </li>
            <li>
              <a href="ha5902530cs.html#490">XIV. Strumenti di
              chiesa</a>
            </li>
            <li>
              <a href="ha5902530cs.html#498">XV. Strumenti
              domestici</a>
            </li>
            <li>
              <a href="ha5902530cs.html#526">XVI. Edificii, e
              loro attenenti</a>
            </li>
            <li>
              <a href="ha5902530cs.html#548">XVII. Strumenti
              fabrili</a>
            </li>
            <li>
              <a href="ha5902530cs.html#569">XVIII. Strumenti da
              giuoco</a>
            </li>
            <li>
              <a href="ha5902530cs.html#579">XIX. Lettere
              alfabetali, e loro attenenti</a>
            </li>
            <li>
              <a href="ha5902530cs.html#586">XX. Strumenti
              marinareschi</a>
            </li>
            <li>
              <a href="ha5902530cs.html#604">XXI. Strumenti
              matematici</a>
            </li>
            <li>
              <a href="ha5902530cs.html#628">XXII. Strumenti
              militari</a>
            </li>
            <li>
              <a href="ha5902530cs.html#647">XXIII. Strumenti
              musicali</a>
            </li>
            <li>
              <a href="ha5902530cs.html#654">XXIV. Strumenti
              rurali</a>
            </li>
            <li>
              <a href="ha5902530cs.html#665">XXV. Corpi misti</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ha5902530cs.html#680">Indice dei corpi e
          motti&#160;</a>
        </li>
        <li>
          <a href="ha5902530cs.html#715">Applicationi varie
          dell'imprese</a>
        </li>
        <li>
          <a href="ha5902530cs.html#794">Indice delle cose
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
