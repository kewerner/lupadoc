<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ecor8793000s.html#5">Storia della Città di
      Cortona</a>
    </li>
    <li>
      <a href="ecor8793000s.html#7">Nuova Descrizzione
      dell'antichissima città di Cortona</a>
      <ul>
        <li>
          <a href="ecor8793000s.html#11">All'altezza reale di
          Cosmo III. Gran Duca di Toscana&#160;</a>
        </li>
        <li>
          <a href="ecor8793000s.html#39">[Testo]</a>
        </li>
        <li>
          <a href="ecor8793000s.html#208">Indice degli Autori, e
          Scrittori classici, che hanno scritto sopra l'Antichità,
          e Nobilità di Cortona&#160;</a>
        </li>
        <li>
          <a href="ecor8793000s.html#211">Indice delle cose
          notabili</a>
        </li>
        <li>
          <a href="ecor8793000s.html#217">Indice dei Capitoli,
          che nella presente Opera si contengono</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
