<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbot738735413s.html#6">Raccolta Di Lettere Sulla
      Pittura Scultura Ed Architettura</a>
      <ul>
        <li>
          <a href="ghbot738735413s.html#8">A Monsignor D. Andrea
          Corsini</a>
        </li>
        <li>
          <a href="ghbot738735413s.html#12">Prefazione</a>
        </li>
        <li>
          <a href="ghbot738735413s.html#14">Lettere su la
          Pittura, Scultura ed Architettura</a>
        </li>
        <li>
          <a href="ghbot738735413s.html#401">Nomi degli autori
          delle Lettere di questo terzo Tomo</a>
        </li>
        <li>
          <a href="ghbot738735413s.html#403">Nomi di quelli a
          quali sono dirette le lettere contenute in questo terzo
          Tomo</a>
        </li>
        <li>
          <a href="ghbot738735413s.html#406">Indice delle cose
          notabili</a>
        </li>
        <li>
          <a href="ghbot738735413s.html#422">Indice degli autori
          delle lettere</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
