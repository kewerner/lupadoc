<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghvig50552020s.html#8">Regola delli cinque ordini
      d’Architettura</a>
      <ul>
        <li>
          <a href="ghvig50552020s.html#10">[Dedica al Cardinale
          Farnese] All'Illustrissimo et rarissimo Signore mio et
          Padrone Singularissimo, il Cardinale Farnese]</a>
        </li>
        <li>
          <a href="ghvig50552020s.html#10">[Dedica ai lettori] Ai
          lettori</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvig50552020s.html#12">[Tavole]</a>
    </li>
  </ul>
  <hr />
</body>
</html>
