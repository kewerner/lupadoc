<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502731s.html#6">Grandezas, Y Maravillas De La
      Inclyta, Y Sancta Civdad De Roma</a>
      <ul>
        <li>
          <a href="dg4502731s.html#8">Division de esta obra en
          tres partes</a>
        </li>
        <li>
          <a href="dg4502731s.html#10">A la soberana reyna de Los
          Angeles</a>
        </li>
        <li>
          <a href="dg4502731s.html#12">Reverendissimo P.
          Maestro</a>
        </li>
        <li>
          <a href="dg4502731s.html#13">Illustrissimo Senor</a>
        </li>
        <li>
          <a href="dg4502731s.html#18">Aprobacion de el
          reverendissimo Padre Maestro Fray Clemente Alvarez</a>
        </li>
        <li>
          <a href="dg4502731s.html#19">Licencia de el
          Ordinario</a>
        </li>
        <li>
          <a href="dg4502731s.html#20">Aprobacion de el Doctor Don
          Ioseph Carpancano</a>
        </li>
        <li>
          <a href="dg4502731s.html#21">La reyna Gobernadora</a>
        </li>
        <li>
          <a href="dg4502731s.html#23">Tassa</a>
        </li>
        <li>
          <a href="dg4502731s.html#25">A el lector</a>
        </li>
        <li>
          <a href="dg4502731s.html#26">Grandezas y Maravillas de
          la inclyta y Sancta Ciudad de Roma</a>
          <ul>
            <li>
              <a href="dg4502731s.html#26">Primera parte</a>
            </li>
            <li>
              <a href="dg4502731s.html#136">Segunda
              parte&#160;</a>
              <ul>
                <li>
                  <a href="dg4502731s.html#137">I. Del Summo
                  Pontifice</a>
                </li>
                <li>
                  <a href="dg4502731s.html#168">II. Del Sacro
                  Consistorio de los Eminentissimos Cardenales</a>
                </li>
                <li>
                  <a href="dg4502731s.html#182">III. De los
                  Patriarchas, Primados, Arcobipos, Obipos, Abades,
                  Priores, y Prepositos, que se preconizan, y
                  proponen en el Sacro Consistorio de su Santidad,
                  y Cardenales</a>
                </li>
                <li>
                  <a href="dg4502731s.html#228">IV. De las diez y
                  ocho Congregaciones, o Consejos, que tiene su
                  Santidad para el Gobierno Espiritual de la
                  Christiandad, y Temporal del Estado de la
                  Iglesia, y su Corte&#160;</a>
                </li>
                <li>
                  <a href="dg4502731s.html#236">V. De la Dataria,
                  Cancellaria, Sacra Rota, y Camara Apostolica</a>
                </li>
                <li>
                  <a href="dg4502731s.html#250">VI. De los demas
                  Osicios, y Tribunales, para el Gobierno de la
                  Corte</a>
                </li>
                <li>
                  <a href="dg4502731s.html#257">VII. De los
                  Prefectos</a>
                </li>
                <li>
                  <a href="dg4502731s.html#259">VIII. De los
                  Ministros que tiene su Santidad para el Gobierno
                  Militar</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502731s.html#262">Tercera
              parte&#160;</a>
              <ul>
                <li>
                  <a href="dg4502731s.html#265">I. De los Sacros
                  Cementerios, Estaciones, y Ano Santo</a>
                </li>
                <li>
                  <a href="dg4502731s.html#283">II. De las Siete
                  Iglesias Principales de Roma, y las Dos Unidas,
                  en todas Nueve</a>
                </li>
                <li>
                  <a href="dg4502731s.html#358">III. De las diez y
                  siete Iglesias, que ay en el Rion, o Quartel de
                  Campitello, oy Campidolio</a>
                </li>
                <li>
                  <a href="dg4502731s.html#379">IV. De las Setenta
                  y tres Iglesias, que ay en el Rion, o Quartel de
                  Monti</a>
                </li>
                <li>
                  <a href="dg4502731s.html#432">V. De las Veinte y
                  dos Iglesias que ay en el Rion de Trivio</a>
                </li>
                <li>
                  <a href="dg4502731s.html#448">VI. De las Veinte
                  Iglesias, que en el Rion de Colona</a>
                </li>
                <li>
                  <a href="dg4502731s.html#465">VII. De las Veinte
                  y quatro Iglesias, que ay en al Rion de Campo
                  Martio</a>
                </li>
                <li>
                  <a href="dg4502731s.html#483">VIII. De las
                  Veinte y dos Iglesias, que ay en el Rion de el
                  Puente</a>
                </li>
                <li>
                  <a href="dg4502731s.html#493">IX. De las Veinte
                  y siete Iglesias, que ay en el Rion de el
                  Burgo</a>
                </li>
                <li>
                  <a href="dg4502731s.html#504">X. De las Treinta
                  y quatro Iglesias, que ay en el Rion de
                  Transtyber</a>
                </li>
                <li>
                  <a href="dg4502731s.html#523">XI. De las
                  Quarenta y tres Iglesias, que ay en el Rion de
                  Ripa</a>
                </li>
                <li>
                  <a href="dg4502731s.html#554">XII. De las Seis
                  Iglesias, que ay en el Rion de el Santo Angel</a>
                </li>
                <li>
                  <a href="dg4502731s.html#558">XIII. De las
                  Treinta Iglesias, que ay en el Rion de la
                  Regula</a>
                </li>
                <li>
                  <a href="dg4502731s.html#571">XIV. De las Diez
                  Iglesias, que ay en el Rion de Parion</a>
                </li>
                <li>
                  <a href="dg4502731s.html#576">XV. De las Quince
                  Iglesias, que ay en el Rion de San Eustachio</a>
                </li>
                <li>
                  <a href="dg4502731s.html#584">XVI. De las Diez
                  Iglesias, que ay en el Rion de la Pina</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502731s.html#592">Indice, o Memoria de
              las Reliquias de Nuestro Senor Iesu Christo</a>
            </li>
            <li>
              <a href="dg4502731s.html#599">Las Reliquias de los
              Demas Santos</a>
            </li>
            <li>
              <a href="dg4502731s.html#629">Indice de las
              Indulgencias Concedidas por los Summos Pontifices</a>
            </li>
            <li>
              <a href="dg4502731s.html#643">Breve Noticia de los
              mas Principales excessos de Amor</a>
            </li>
            <li>
              <a href="dg4502731s.html#650">Chronologia de Iesu
              Christo, y de los Summos Pontifices [...]&#160;</a>
            </li>
            <li>
              <a href="dg4502731s.html#754">Tabla o Repertorio de
              todo lo que se contiene este libro</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
