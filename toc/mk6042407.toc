<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="mk6042407s.html#6">Storia dell'arte col mezzo dei
      monumenti dalla sua decadenza nel IV secolo fino al suo
      risorgimento nel XVI. Volume VII. Quadro storico</a>
      <ul>
        <li>
          <a href="mk6042407s.html#10">Quadro storico dello stato
          civile, politico e letterario della Grecia e dell'Italia
          relativamente alle belle arti poco tempo prima della loro
          decadenza, durante questa decadenza, fino al totale
          risorgimento. Notizie succinte intorno le loro produzioni
          durante questo periodo di tempo</a>
          <ul>
            <li>
              <a href="mk6042407s.html#10">I. Grecia-Italia.
              Dell'Arte nella sua perfezione, trasportata in Roma
              dopo la conquista della Grecia</a>
            </li>
            <li>
              <a href="mk6042407s.html#13">II. Italia.
              Dell'impero romano e dello stato dell'Arte fino alla
              sua decadenza nel IV secolo</a>
            </li>
            <li>
              <a href="mk6042407s.html#17">III. Italia.
              Circostanze generali che produssero la prima epoca
              della decadenza dell'Arte, nel IV secolo</a>
            </li>
            <li>
              <a href="mk6042407s.html#23">IV. Grecia.
              Traslazione della sede dell'impero romano a
              Costantinopoli, verso l'anno 330. Stato dell'Arte, in
              Grecia, da quest'epoca fino alla divisione in impero
              d'oriente ed impero d'occidente, nell'anno 364</a>
            </li>
            <li>
              <a href="mk6042407s.html#29">V. Italia. Dell'impero
              romano in occidente fino alla sua distruzione fatta
              dai Goti verso la fine del V secolo: seconda epoca
              della decadenza dell'Arte</a>
            </li>
            <li>
              <a href="mk6042407s.html#37">VI. Italia.
              Considerazioni per le quali la seconda epoca della
              decadenza dell'Arte in Italia non devesi attribuire
              all'influenza dei popoli barbari che ne erano
              diventati padroni. - Prospetto dell'istruzione che
              questi popoli avevano successivamente acquistata</a>
            </li>
            <li>
              <a href="mk6042407s.html#43">VII. Italia.
              Continuazione del medesimo argomento</a>
            </li>
            <li>
              <a href="mk6042407s.html#49">VIII. Italia. Regno
              dei re goti in Italia. Stato delle Arti durante il
              loro governo, nei secoli V e VI</a>
            </li>
            <li>
              <a href="mk6042407s.html#56">IX. Continuazione del
              regno dei re goti in Italia</a>
            </li>
            <li>
              <a href="mk6042407s.html#63">X. Italia. Regno dei
              re longobardi in Italia. - Quadro della situazione di
              Roma, di Napoli, di Venezia e dell'esarcato di
              Ravenna. - Stato delle Arti, sotto il governo dei
              Longobardi, nei secoli Vi e VII, fino alla sua
              distruzione sul finire dell'VIII</a>
            </li>
            <li>
              <a href="mk6042407s.html#73">XI. Italia. Della
              Chiesa nei primi tre secoli. - Dei pontefici dopo il
              IV secolo; dei loro possedimenti e della loro
              influenza sulle belle arti</a>
            </li>
            <li>
              <a href="mk6042407s.html#79">XII. Italia.
              Continuazione del medesimo argomento fino alla
              donazione di Carlo Magno</a>
            </li>
            <li>
              <a href="mk6042407s.html#83">XIII. Italia. Notizie
              dei lavori d'Arte ordinati dai pontefici, sino alla
              fine del IX secolo</a>
            </li>
            <li>
              <a href="mk6042407s.html#87">XIV. Grecia.
              Dell'impero d'Oriente, della sua separazione
              dall'impero d'Occidente nel IV secolo, sino alla fine
              dell'VIII. - Stato delle arti in Grecia e nelle
              contrade orientali, durante questo periodo di
              tempo</a>
            </li>
            <li>
              <a href="mk6042407s.html#97">XV. Grecia.
              Continuazione del medesimo argomento fino al IX
              secolo</a>
            </li>
            <li>
              <a href="mk6042407s.html#102">XVI. Italia.
              Conquista dell'Italia fatta da Carlo Magno e
              ristabilimento dell'impero d'Occidente in principio
              del IX secolo. - Protezione che egli accordò alle
              lettere ed alle arti. Discendenti di questo principe,
              suoi successori in Italia, sino verso il fine del IX
              secolo</a>
            </li>
            <li>
              <a href="mk6042407s.html#109">XVII. Italia. Stato
              di questa contrada, sotto i diversi principi che la
              dominarono, dagli ultimi anni del IX secolo fino alla
              fine del X</a>
            </li>
            <li>
              <a href="mk6042407s.html#112">XVIII. Italia.
              Turbolenze nella Chiesa per l'elezione dei papi e nel
              governo pontificio, durante il corso dei secoli IX e
              X. - Stato delle Arti in tutto questo periodo</a>
            </li>
            <li>
              <a href="mk6042407s.html#118">XIX. Grecia.
              Dell'impero d'Oriente e dello stato delle Arti in
              questa contrada, dal ristabilimento dell'impero
              d'Occidente fino al IX secolo</a>
            </li>
            <li>
              <a href="mk6042407s.html#127">XX. Italia. L'Italia
              sotto gli imperadori d'Occidente, nei secoli XI XII.
              Controversie tra il sacerdozio e l'impero. Le Arti
              all'ultimo grado della loro decadenza</a>
            </li>
            <li>
              <a href="mk6042407s.html#132">XXI. Italia.
              Tentativi di varie città e provincie d'Italia,
              nell'XI secolo, per costituirsi in governo
              particolare. Conquiste dei Normanni e loro
              stabilimento nelle due Sicilie, sino alla fine del
              XII secolo. - Influenza di questi avvenimenti sulle
              Arti</a>
            </li>
            <li>
              <a href="mk6042407s.html#137">XXII. Grecia. Delle
              crociate. Dell'impero d'Oriente nei secoli XI e XII,
              fino alla presa di Costantinopoli fatta dai Latini,
              nel 1204. Stato delle Arti in questo periodo di
              tempo</a>
            </li>
            <li>
              <a href="mk6042407s.html#145">XXIII. Grecia. Regno
              dei Latini nell'impero greco, a Costantinopoli, fino
              alla metà del XIII secolo. Divisione del resto di
              quell'impero fra i principi greci, i quali ne
              trasportarono la sede in differenti città</a>
            </li>
            <li>
              <a href="mk6042407s.html#151">XXIV. Grecia.
              Riconquista di Costantinopoli fatta dai principi
              greci, nel XIII secolo. Loro governo dal XIV secolo
              fino all'anno 1433, epoca della distruzione
              dell'impero d'Oriente</a>
            </li>
            <li>
              <a href="mk6042407s.html#158">XXV. Italia. Stato
              civile e politico dell'Italia nel XIII secolo. Aurora
              o primo grado del risorgimento delle Lettere e delle
              Belle Arti</a>
            </li>
            <li>
              <a href="mk6042407s.html#165">XXVI. Italia.
              Continuazione dello stesso argomento, durante il XIV
              secolo</a>
            </li>
            <li>
              <a href="mk6042407s.html#171">XXVII. Italia.
              Progressi del risorgimento delle lettere e delle Arti
              e principio del loro rinnovellamento nel XV
              secolo</a>
            </li>
            <li>
              <a href="mk6042407s.html#179">XXVIII. Italia.
              Rinnovellamento delle Arti, terminato nei primi anni
              del XVI secolo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042407s.html#184">Quadro dei principi e dei
          più rinomati artisti contemporanei alle cure ed alle
          opere dei quali devesi il rinnovellamento dell'arte
          [tabella]</a>
        </li>
        <li>
          <a href="mk6042407s.html#185">[Tavole cronologiche di
          concordanza tra alcune delle principali personalità
          politiche e/o religiose dell'alto medioevo e le più
          importanti opere d'arte coeve]</a>
        </li>
        <li>
          <a href="mk6042407s.html#189">Vasi, utensili, mobili ed
          ornamenti ad uso della primitiva Chiesa [elenco bilingue
          latino-italiano]</a>
        </li>
        <li>
          <a href="mk6042407s.html#192">Estratti del liber
          Pontificalis di Anastasio</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="mk6042407s.html#198">Tavola dei capitoli del
      quadro storico</a>
    </li>
  </ul>
  <hr />
</body>
</html>
