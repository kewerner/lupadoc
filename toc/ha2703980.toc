<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ha2703980s.html#8">Soggetti per quadri ad uso de'
      Giovani Pittori</a>
      <ul>
        <li>
          <a href="ha2703980s.html#10">Introduzione</a>
        </li>
        <li>
          <a href="ha2703980s.html#24">Parte prima soggetti tratti
          dall' Iliade</a>
        </li>
        <li>
          <a href="ha2703980s.html#76">Parte seconde soggetti
          tratti dall' Eneide</a>
        </li>
        <li>
          <a href="ha2703980s.html#136">Parte terza soggetti
          tratti dalla Gerusalemme Liberata</a>
        </li>
        <li>
          <a href="ha2703980s.html#208">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
