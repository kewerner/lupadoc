<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebre2983780s.html#6">Il Palazzo Pubblico alle
      Ragioni della città di Brescia antico, e moderno, misurato,
      disegnato, e descritto in 16 tavole</a>
    </li>
    <li>
      <a href="ebre2983780s.html#8">[Tavole]</a>
      <ul>
        <li>
          <a href="ebre2983780s.html#8">I. Prima Pianta al
          Pianoterreno del Palazzo Pubblico delle Ragioni
          ▣&#160;</a>
        </li>
        <li>
          <a href="ebre2983780s.html#10">II. Nuova Pianta della
          Gran Sala antica, (come era costrutta avanti l'Incendio
          del 1575) [...] ▣&#160;</a>
        </li>
        <li>
          <a href="ebre2983780s.html#12">III. Spaccato per il
          Largo della Sotto Loggia, e del Salone antico superiore,
          e Taglio dell'Atrio, e della Scala esterna ▣</a>
        </li>
        <li>
          <a href="ebre2983780s.html#14">IV. Profilo interiore
          per la lunghezza di tutta la Fabbrica, e del Salone
          decorato all'intorno con Colonne isolate, e dipinto nelle
          Pareti, e nella Volta da Tiziano Vecellio, e da altri
          Professori ▣</a>
        </li>
        <li>
          <a href="ebre2983780s.html#16">V. Pianta della gran
          Sala Moderna, secondo l'ultima riduzione del K. Luigi
          Vanvitelli ▣&#160;</a>
        </li>
        <li>
          <a href="ebre2983780s.html#18">VI. [Metà della Pianta
          ottangolare e Profilo dell'Attico Superiore] ▣&#160;</a>
        </li>
        <li>
          <a href="ebre2983780s.html#20">VII. Spaccato per il
          lungo di tutta la Fabbrica, e Prospetto interiore della
          gran Sala Moderna ▣</a>
        </li>
        <li>
          <a href="ebre2983780s.html#22">VIII. Facciata
          ersteriore antica del Palazzo verso la Piazza, e
          Prospetto del Coperto Moderno del K. Vanvitelli ▣</a>
        </li>
        <li>
          <a href="ebre2983780s.html#24">IX. Aspetto ersteriore
          del Palazzo dal Lato di Mezzodi, e Coperto secondo il
          Sistema antico ▣</a>
        </li>
        <li>
          <a href="ebre2983780s.html#26">X. [Metà della Pianta
          del Salone] ▣&#160;</a>
        </li>
        <li>
          <a href="ebre2983780s.html#28">XI. Prospetto interiore
          di un lato dell'ottogano verso la porta degli Archivj ▣;
          Altro lato verso la Piazza ▣</a>
        </li>
        <li>
          <a href="ebre2983780s.html#30">XII. Prospetto
          interiore, e Profilo di uno dei quattro Nicchioni
          cantonali del Salone ▣</a>
        </li>
        <li>
          <a href="ebre2983780s.html#32">XIII. Pianta della gran
          Sala Ottogona, colla metà del Pavimento, e della Volta:
          conforme al primo Progetto dell'Abate Turbini, che fù
          anche eseguito in Modello di Legno ▣; Taglie per la
          larghezza della gran Sala; ed Aspetto interno verso gli
          Archivj ▣&#160;</a>
        </li>
        <li>
          <a href="ebre2983780s.html#34">XIV. Pianta, ed
          Elevazione geometrica di un quarto della gran Sala posta
          in angolo; e suo Prospetto interiore dalla parte degli
          Archivj, e dell'Ingresso ▣; Facciata esterna verso la
          Piazza, con il Coperto, che dovrai essere simile anche
          dalla parte posteriore del Palazzo[...] ▣&#160;</a>
        </li>
        <li>
          <a href="ebre2983780s.html#36">XV. Altra Pianta ideata
          dall'Abate Turbini per la gran Sala suddivisa in tre Navi
          da quattro Colonne isolate nel mezzo, che sostentano il
          Tetto; colla metà del Pavimento, e del Soffiato Piano ▣;
          Spaccato interno della gran Sala suddivisa in tre Navi; e
          col Coperto a Linea pendente ▣</a>
        </li>
        <li>
          <a href="ebre2983780s.html#38">XVI. Piante, e Profili
          del Castello dei Ponti eseguiti nell'1774; e da eseguirsi
          per la Riffabbrica della Sala del Palazzo Pubblico;
          d'Invenzione dell'Abate Gasparo Turbini Architetto ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
