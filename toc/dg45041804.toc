<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg45041804s.html#6">Roma antica</a>
      <ul>
        <li>
          <a href="dg45041804s.html#10">[Tavola]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45041804s.html#10">›Piramide di Caio Cestio‹
      ◉</a>
    </li>
    <li>
      <a href="dg45041804s.html#12">Discorso di Ottavio
      Falconieri intorno alla ›Piramide di Caio Cestio‹</a>
    </li>
    <li>
      <a href="dg45041804s.html#55">Lettera del medesimo al
      Signor Carlo Dati sopra l'iscrizione di un Mattone cavato
      dalle ruine d'un muro antico gittato a terra</a>
    </li>
    <li>
      <a href="dg45041804s.html#72">Memorie di varie antichità
      trovate in diversi luoghi della città di Roma</a>
      <ul>
        <li>
          <a href="dg45041804s.html#74">Al molto magnifico
          Signore Simonetto Anastasii Padrone Onorando</a>
        </li>
        <li>
          <a href="dg45041804s.html#76">[Testo]</a>
        </li>
        <li>
          <a href="dg45041804s.html#119">Tavola sopra le
          memorie</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45041804s.html#124">Delle vie degli antichi</a>
      <ul>
        <li>
          <a href="dg45041804s.html#126">Introduzione</a>
        </li>
        <li>
          <a href="dg45041804s.html#130">I. Dell'Amministrazione
          delle vie</a>
          <ul>
            <li>
              <a href="dg45041804s.html#130">I. Definizione de'
              Nomi ed origine delle Vie</a>
            </li>
            <li>
              <a href="dg45041804s.html#136">II. De' Magistrati,
              che avevano cura delle vie sì interne, che
              esterne</a>
            </li>
            <li>
              <a href="dg45041804s.html#145">III. Amministrazione
              delle vie</a>
            </li>
            <li>
              <a href="dg45041804s.html#149">IV. Imperadori, che
              più cura si presero delle vie</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45041804s.html#155">II. Della Costruzione
          delle vie</a>
          <ul>
            <li>
              <a href="dg45041804s.html#155">I. Materie, delle
              quali facevasi uso nel costruire le vie</a>
            </li>
            <li>
              <a href="dg45041804s.html#157">II. Costruzione
              delle vie</a>
            </li>
            <li>
              <a href="dg45041804s.html#173">III. Degli Itinerari
              antichi, e del numero delle vie che uscivano da
              Roma</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45041804s.html#177">III. Descrizione delle
          vie</a>
          <ul>
            <li>
              <a href="dg45041804s.html#177">I. Vie, che
              partivano a Settentrione di Roma</a>
              <ul>
                <li>
                  <a href="dg45041804s.html#177">Della ›Via
                  Flaminia‹</a>
                </li>
                <li>
                  <a href="dg45041804s.html#198">Delle Vie Cassia
                  ›Via Cassia‹, e Claudia, o Clodia ›Via Clodia‹ ,
                  e della Trionfale ›Via Trionfale‹</a>
                </li>
                <li>
                  <a href="dg45041804s.html#205">Della ›Via
                  Salaria‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45041804s.html#216">II. Vie che
              partivano all'oriente di Roma</a>
              <ul>
                <li>
                  <a href="dg45041804s.html#216">Della ›Via
                  Nomentana‹</a>
                </li>
                <li>
                  <a href="dg45041804s.html#218">Delle vie
                  Tiburtina ›Via Tiburtina‹ e Valeria ›Via
                  Tiburtina Valeria‹</a>
                </li>
                <li>
                  <a href="dg45041804s.html#227">Della ›Via
                  Prenestina‹</a>
                </li>
                <li>
                  <a href="dg45041804s.html#230">Della ›Via
                  Labicana‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45041804s.html#233">III. Vie che
              partivano a Mezzogiorno di Roma</a>
              <ul>
                <li>
                  <a href="dg45041804s.html#233">Delle vie Latina
                  ›Via Latina‹ ed ›Via Asinaria‹</a>
                </li>
                <li>
                  <a href="dg45041804s.html#242">Della ›Via
                  Appia‹</a>
                </li>
                <li>
                  <a href="dg45041804s.html#253">Delle vie
                  Ostiense ›Via Ostiense‹ , ›Via Laurentina‹ e
                  Severiana ›Via Severiana‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45041804s.html#257">IV. Vie, che
              uscivano ad Occidente di Roma</a>
              <ul>
                <li>
                  <a href="dg45041804s.html#257">Della ›Via
                  Portuense‹</a>
                </li>
                <li>
                  <a href="dg45041804s.html#259">Della ›Via
                  Aurelia‹</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
