<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cacar27202030s.html#6">&#160; Il funerale
      d’Agostin Carraccio fatto in Bologna sua patria da
      gl’Incaminati Academici del Disegno</a>
      <ul>
        <li>
          <a href="cacar27202030s.html#8">Illustrissimo e
          Reverendissimo Signore</a>
        </li>
        <li>
          <a href="cacar27202030s.html#34">Oratione di Lutio
          Faberio Academico Gelato in morte d'Agostin Carraccio</a>
        </li>
        <li>
          <a href="cacar27202030s.html#49">In morte d'Agostino
          Carracci di Cesare Rinaldi</a>
        </li>
        <li>
          <a href="cacar27202030s.html#49">Di Gabriele
          Bambasi</a>
        </li>
        <li>
          <a href="cacar27202030s.html#50">Di Lorenzo Arrighi</a>
        </li>
        <li>
          <a href="cacar27202030s.html#50">Di Lucio Faberio</a>
        </li>
        <li>
          <a href="cacar27202030s.html#51">Del velato Academico
          Insensato</a>
        </li>
        <li>
          <a href="cacar27202030s.html#54">D'Incerto</a>
        </li>
        <li>
          <a href="cacar27202030s.html#55">In obitum Augustini
          Carracii</a>
        </li>
        <li>
          <a href="cacar27202030s.html#56">Eiusdem distichon</a>
        </li>
        <li>
          <a href="cacar27202030s.html#56">Joannis Baptistae
          Lauri</a>
        </li>
        <li>
          <a href="cacar27202030s.html#57">Eiusdem</a>
        </li>
        <li>
          <a href="cacar27202030s.html#57">Iulii Signii&#160;</a>
        </li>
        <li>
          <a href="cacar27202030s.html#57">Eiusdem</a>
        </li>
        <li>
          <a href="cacar27202030s.html#57">Incerti</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
