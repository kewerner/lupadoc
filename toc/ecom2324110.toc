<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ecom2324110s.html#8">Selva di notizie autentiche
      risguardanti la fabbrica della Cattedrale di Como, con altre
      memorie patrie, ed analoghe all’argomento</a>
      <ul>
        <li>
          <a href="ecom2324110s.html#10">Al cortese lettore</a>
        </li>
        <li>
          <a href="ecom2324110s.html#12">[Selva di notizie
          autentiche risguardanti la fabbrica della Cattedrale di
          Como, con altre memorie patrie, ed analoghe
          all’argomento]</a>
        </li>
        <li>
          <a href="ecom2324110s.html#266">Avvertenza</a>
        </li>
        <li>
          <a href="ecom2324110s.html#286">Episcopi Comenses</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
