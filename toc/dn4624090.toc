<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dn4624090s.html#7">Choix des plus célèbres maisons
      de plaisance de Rome et de ses environs</a>
      <ul>
        <li>
          <a href="dn4624090s.html#10">Discours préliminaire</a>
        </li>
        <li>
          <a href="dn4624090s.html#16">›Villa Albani‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#20">Fragments et
              bas-reliefs antiques tirés de la ›Villa Albani‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#22">Plan général des
              jardins et de la maison de plaisance du prince Albani
              ›Villa Albani‹ ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#24">Vue générale de la
              maison de plaisance du prince Albani ›Villa Albani‹
              ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#26">Vue du grand casin de
              la ›Villa Albani‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#28">Vue de l'entrée de la
              salle de billard de la ›Villa Albani‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#30">Vue de la fontaine et
              de la grotte des potagers de la ›Villa Albani‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#32">Vue du portique des
              cariatides de la ›Villa Albani‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#34">›Villa Medici‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#36">Plan général de la
              ›Villa Medici‹ ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#38">Vue de la façade du
              coté de l'entrée du palais ›Villa Medici‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#40">Vue du palais et de la
              galerie ›Villa Medici‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#42">Vue de l'intérieur du
              vestibule ›Villa Medici‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#44">Vue de la face latérale
              du palais ›Villa Medici‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#46">Vue du pavillon de la
              cléoptàtre ›Villa Medici‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#48">Villa Panfili ›Villa Doria
          Pamphilj‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#50">Plan général du casin
              principal de la Villa Panfili ›Villa Doria Pamphilj‹
              ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#52">Vue du casin de la
              Villa Panfili ›Villa Doria Pamphilj‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#54">Vue principal de la
              Villa Panfili ›Villa Doria Pamphilj‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#56">Vue de la grotte des
              jardins de la Villa Panfili ›Villa Doria Pamphilj‹
              ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#58">Vue de l'entrée de
              l'une des allés du jardin de Villa Panfili ›Villa
              Doria Pamphilj‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#60">›Villa Barberini‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#62">Plan de la ›Villa
              Barberini‹ ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#64">›Villa Borghese‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#68">›Villa Borghese‹ a Rome
              ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#70">Plan général de la
              ›Villa Borghese‹ et de ses jardins ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#72">Plan général du grand
              casin de la ›Villa Borghese‹ ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#74">Vue du grand casin de
              la ›Villa Borghese‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#76">Vue de la façade
              principale du grand casin ›Villa Borghese‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#78">Vue de l'entrée des
              bosquets du jardin particulier ›Villa Borghese‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#80">Vue d'une fontaine
              jaillissante ›Villa Borghese‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#82">›Villa Mattei‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#84">Plan général de la
              ›Villa Mattei‹ ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#86">Vue de la face latérale
              du casin de la ›Villa Mattei‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#88">Vue du cirque et de
              l'obélisque sur la grande terrasse ›Villa Mattei‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#90">Villa Farnesiana ›Orti
          Farnesiani‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#92">Plan de la Villa
              Farnesiana ›Orti Farnesiani‹ ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#94">Vue générale de la
              Villa Farnesiana ›Orti Farnesiani‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#96">Vue du grand escalier
              de la Villa Farnesiana ›Orti Farnesiani‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#98">›Villa Negroni‹, ou
          Montalto</a>
          <ul>
            <li>
              <a href="dn4624090s.html#100">Plan de la ›Villa
              Negroni‹ ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#102">Vue du casin de la
              ›Villa Negroni‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#104">Vue de l'entrée de la
              ›Villa Negroni‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#106">Casino del Papa, ou Villa
          Pia ›Casina di Pio IV‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#108">Plan de la Villa Pia
              ›Casina di Pio IV‹ ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#110">Vue générale de la
              Villa Pia ›Casina di Pio IV‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#112">Vue de la cour et de
              la façade intérieure du casin de la Villa Pia ›Casina
              di Pio IV‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#114">›Villa Madama‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#116">›Villa Madama‹ a Rome
              ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#118">Plan des restes de la
              ›Villa Madama‹/Plan des restes de la Villa Sacchetti
              ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#120">Vue de la ›Villa
              Madama‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#122">Vue de la grande loge
              du casin de la ›Villa Madama‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#124">Villa Sacchetti</a>
          <ul>
            <li>
              <a href="dn4624090s.html#126">Plan, coupe et
              elevation de la Villa Sacchetti ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#128">Vue des ruines de la
              Villa Sacchetti ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#130">›Villa Altieri‹&#160;</a>
          <ul>
            <li>
              <a href="dn4624090s.html#132">Plan de la ›Villa
              Altieri‹ &#160;◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#134">Vue de la ›Villa
              Altieri‹ &#160;▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#136">Villa di Papa Giulio
          ›Villa Giulia‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#138">Plan de la Villa di
              Papa Giulio ›Villa Giulia‹ ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#140">Vue générale du casin
              de la Villa du Papa Jules ›Villa Giulia‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#142">Vue de l'intérieur de
              la cour et de la grotte souterraine de la Villa du
              Papa Jules ›Villa Giulia‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#144">Vue de l'intérieur de
              la grande cour ›Villa Giulia‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#146">›Villa Bolognetti‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#148">Plan de la ›Villa
              Bolognetti‹ ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#150">Vue de la cour et du
              casin de la ›Villa Bolognetti‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#152">Villa Monte Dragone</a>
          <ul>
            <li>
              <a href="dn4624090s.html#154">Plan de la Villa Monte
              Dragone ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#156">Vue de la Villa Monte
              Dragone ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#158">Vue de la fontaine du
              Dragon ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#160">Villa Taverna</a>
          <ul>
            <li>
              <a href="dn4624090s.html#162">Plan général de la
              Villa Taverna ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#164">Vue du casin de la
              Villa Taverna ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#166">Villa Muti</a>
          <ul>
            <li>
              <a href="dn4624090s.html#168">Plan de la Villa Muti
              avec une partie de ses jardins ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#170">Villa d'este, ou
          estense</a>
          <ul>
            <li>
              <a href="dn4624090s.html#174">Villa d'este a Tivoli
              ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#176">Plan général de la
              Villa d'est de ses jardins ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#178">Vue du palais de la
              Villa d'este ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#180">Vue de la terrasse des
              jets d'eau et du palais ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#182">Vue du grand bassin
              fontaine d'aréthuse et de la galerie qui l'entoure
              ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#184">Vue de la fontaine
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#186">Casino Colonna, a
          Marino</a>
          <ul>
            <li>
              <a href="dn4624090s.html#188">Plan du Casino
              Colonna, a Marino ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#190">›Villa Aldobrandini‹</a>
          <ul>
            <li>
              <a href="dn4624090s.html#194">Plan général de la
              ›Villa Aldobrandini‹ ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#196">Vue du palais et des
              jardins de la ›Villa Aldobrandini‹ ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#198">Vue de la terrasse de
              la grande cascade et du théatre des eaux ›Villa
              Aldobrandini‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#200">Villa Lanti, a Bagnaia</a>
          <ul>
            <li>
              <a href="dn4624090s.html#202">Plan de la Villa
              Lanti, a Bagnaia ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#204">Vue générale de la
              Villa Lanti, a Bagnaia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#206">Villa Giustiniani,
          a&#160;</a>
          <ul>
            <li>
              <a href="dn4624090s.html#208">Plan général de la
              Villa Giustiniani, a Bassano ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#210">Villa et palazzo di
          Caprarola</a>
          <ul>
            <li>
              <a href="dn4624090s.html#212">Plan du
              rez-de-chaussée du palais de Caprarola ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#214">Plan du premier etage
              du palais de Caprarola ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#216">Vue générale du palais
              de Caprarola ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#218">Palazzuolo di
          Caprarola</a>
          <ul>
            <li>
              <a href="dn4624090s.html#222">Plan général du petit
              casin de Caprarola ◉</a>
            </li>
            <li>
              <a href="dn4624090s.html#224">Vue générale du petit
              casin de Caprarola ▣</a>
            </li>
            <li>
              <a href="dn4624090s.html#226">Vue du petit casin de
              Caprarola ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn4624090s.html#228">Table des architectures et
          des artistes</a>
        </li>
        <li>
          <a href="dn4624090s.html#230">Tables des planches</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
