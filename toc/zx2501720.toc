<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zx2501720s.html#6">Evclidis Elementorvm Libri
      XV</a>
      <ul>
        <li>
          <a href="zx2501720s.html#8">Gregorii XIII. Pont. Maximus
          Privilegium</a>
        </li>
        <li>
          <a href="zx2501720s.html#10">Illustrissimo atque
          excellentissimo Francisco Mariae II. Urbinatum
          Principe</a>
        </li>
        <li>
          <a href="zx2501720s.html#13">Federici Commandini in
          elementa Euclidis prolegomena &#160;</a>
        </li>
        <li>
          <a href="zx2501720s.html#21">Index eorum, quae in his
          libris demonstrantur prater ea, que Euclidis sunt</a>
        </li>
        <li>
          <a href="zx2501720s.html#30">Liber primus&#160;</a>
        </li>
        <li>
          <a href="zx2501720s.html#85">Liber secundus</a>
        </li>
        <li>
          <a href="zx2501720s.html#101">Liber tertius</a>
        </li>
        <li>
          <a href="zx2501720s.html#129">Liber quartus</a>
        </li>
        <li>
          <a href="zx2501720s.html#142">Liber quintus</a>
        </li>
        <li>
          <a href="zx2501720s.html#170">Liber sextus</a>
        </li>
        <li>
          <a href="zx2501720s.html#203">Liber septimus</a>
        </li>
        <li>
          <a href="zx2501720s.html#231">Liber octavus</a>
        </li>
        <li>
          <a href="zx2501720s.html#248">Liber nonus</a>
        </li>
        <li>
          <a href="zx2501720s.html#273">Liber decimus</a>
        </li>
        <li>
          <a href="zx2501720s.html#406">Liber undecimus</a>
        </li>
        <li>
          <a href="zx2501720s.html#450">Liber duodecimus</a>
        </li>
        <li>
          <a href="zx2501720s.html#485">Liber
          tertiusdecimus&#160;</a>
        </li>
        <li>
          <a href="zx2501720s.html#515">Liber quartusdecimus</a>
        </li>
        <li>
          <a href="zx2501720s.html#526">Liber quintusdecimus</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
