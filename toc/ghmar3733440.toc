<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghmar3733440s.html#4">Delle cose gentilesche e
      profane trasportate ad uso, e adornamento delle chiese</a>
      <ul>
        <li>
          <a href="ghmar3733440s.html#6">Eminentissimo
          Principe</a>
        </li>
        <li>
          <a href="ghmar3733440s.html#12">Motivo, ed occasione
          dell'opera, e protesta dell'autore</a>
        </li>
        <li>
          <a href="ghmar3733440s.html#18">Indice de' capi</a>
        </li>
        <li>
          <a href="ghmar3733440s.html#24">[Text]</a>
        </li>
        <li>
          <a href="ghmar3733440s.html#515">Lo stampatore a chi
          legge</a>
        </li>
        <li>
          <a href="ghmar3733440s.html#520">Indice delle cose più
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
