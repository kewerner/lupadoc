<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="emil1039203s.html#6">Delle Antichità
      Longobardico-Milanesi Illustrate Con Dissertazioni Dai Monaci
      Della Congregazione Cisterciese Di Lombardia; Tomo III.</a>
      <ul>
        <li>
          <a href="emil1039203s.html#8">Prefazione</a>
        </li>
        <li>
          <a href="emil1039203s.html#19">Indice delle
          dissertazioni</a>
        </li>
        <li>
          <a href="emil1039203s.html#20">XXV. Saggio
          storico-critico sopra il rito ambrosiano</a>
        </li>
        <li>
          <a href="emil1039203s.html#248">XXVI. Sopra le
          triduane litanie della chiesa milanese</a>
        </li>
        <li>
          <a href="emil1039203s.html#274">XXVII. Sull'ampiezza
          delle facoltà altre volte esercitate dal minor clero
          della chiesa milanese riguardo le cose sacre ed
          ecclesiastiche</a>
        </li>
        <li>
          <a href="emil1039203s.html#288">XXVIII. Sulla
          denominazione ad rotam, che in altri tempi portava la
          Basilica di S. Stefano di Milano</a>
        </li>
        <li>
          <a href="emil1039203s.html#315">XXIX. Sulle varie
          denominazioni e specie di scuole che vi ebbero ne'
          passati tempi, e singolarmente in Milano</a>
        </li>
        <li>
          <a href="emil1039203s.html#351">XXX. Intorno gli
          antichi decumani della chiesa milanese</a>
        </li>
        <li>
          <a href="emil1039203s.html#433">Indice delle
          materie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
