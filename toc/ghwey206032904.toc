<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghwey206032904s.html#6">De levens-beschryvingen
      der nederlandsche konst-schilders en konst-schilderessen;
      tomo IV.</a>
      <ul>
        <li>
          <a href="ghwey206032904s.html#8">Voorbericht</a>
        </li>
        <li>
          <a href="ghwey206032904s.html#10">Aanmerkungen over
          het Betaamelyke en het Wanvoeglyke van de
          Schilderkonst</a>
        </li>
        <li>
          <a href="ghwey206032904s.html#46">Korte Levensschets
          der Konstschilder van de Schilder-Akademie in Hage</a>
        </li>
        <li>
          <a href="ghwey206032904s.html#102">De Afbeeldsels en
          de Leevensbedryven der nederlandsche Konstschilders en
          Konstschilderessen&#160;</a>
        </li>
        <li>
          <a href="ghwey206032904s.html#577">Bladwyzer op het
          vierde Deel van de Schilderkonst der Nederlanders</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
