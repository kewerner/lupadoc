<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zsalg4373640c6s.html#6">Opere</a>
      <ul>
        <li>
          <a href="zsalg4373640c6s.html#8">Viaggi di Russia</a>
        </li>
        <li>
          <a href="zsalg4373640c6s.html#10">A sua eccellenza il
          Signore Conte di Woronzow</a>
        </li>
        <li>
          <a href="zsalg4373640c6s.html#16">Al medesimo a
          Firenze</a>
        </li>
        <li>
          <a href="zsalg4373640c6s.html#18">Saggio di storia
          metallica della Russia</a>
        </li>
        <li>
          <a href="zsalg4373640c6s.html#26">Viaggi di
          Russia</a>
        </li>
        <li>
          <a href="zsalg4373640c6s.html#226">Il Congresso di
          Citera col giudicio di amore sopra l'istesso
          Congresso</a>
          <ul>
            <li>
              <a href="zsalg4373640c6s.html#228">A caritea</a>
            </li>
            <li>
              <a href="zsalg4373640c6s.html#230">Il Congresso
              di Citera</a>
            </li>
            <li>
              <a href="zsalg4373640c6s.html#301">Giudicio di
              amore sopra il Congresso di Citera&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zsalg4373640c6s.html#324">Vita di Stefano
          Benedetto Pallavicini</a>
        </li>
        <li>
          <a href="zsalg4373640c6s.html#346">Sinopsi di una
          introduzione alla nereidologia</a>
        </li>
        <li>
          <a href="zsalg4373640c6s.html#394">Indice delle
          materie contenute nel Tomo Sesto</a>
        </li>
        <li>
          <a href="zsalg4373640c6s.html#410">Indice delle
          materie contenute nel Tomo Sesto</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
