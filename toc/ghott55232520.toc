<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghott55232520s.html#6">Trattato della pittura, e
      scultura, uso, et abuso loro</a>
      <ul>
        <li>
          <a href="ghott55232520s.html#8">Prego, invece della
          dedicatione a San Luca Evangelista</a>
        </li>
        <li>
          <a href="ghott55232520s.html#10">A chi legge</a>
        </li>
        <li>
          <a href="ghott55232520s.html#13">Indice de' capi, e
          quesiti</a>
        </li>
        <li>
          <a href="ghott55232520s.html#22">Lo stampatore dopo
          alcune copie publicate del Libro</a>
        </li>
        <li>
          <a href="ghott55232520s.html#26">Trattato della
          pittura, e della scultura, uso, et abuso loro</a>
        </li>
        <li>
          <a href="ghott55232520s.html#428">Indice delle
          materie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
