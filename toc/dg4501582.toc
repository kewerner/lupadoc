<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501582s.html#6">Le cose maravigliose dell'alma
      citta di Roma</a>
      <ul>
        <li>
          <a href="dg4501582s.html#8">Della edificatione di Roma
          [...]</a>
        </li>
        <li>
          <a href="dg4501582s.html#14">De le sette chiesie (sic!)
          principali</a>
          <ul>
            <li>
              <a href="dg4501582s.html#14">›San Giovanni in
              Laterano‹</a>
            </li>
            <li>
              <a href="dg4501582s.html#21">›San Pietro in
              Vaticano‹</a>
            </li>
            <li>
              <a href="dg4501582s.html#25">›San Paolo fuori le
              mura‹</a>
            </li>
            <li>
              <a href="dg4501582s.html#26">›Santa Maria
              Maggiore‹</a>
            </li>
            <li>
              <a href="dg4501582s.html#28">›San Lorenzo fuori le
              mura‹</a>
            </li>
            <li>
              <a href="dg4501582s.html#30">›San Sebastiano‹</a>
            </li>
            <li>
              <a href="dg4501582s.html#30">›Santa Croce in
              Gerusalemme ‹</a>
            </li>
            <li>
              <a href="dg4501582s.html#32">Ne l'Isola</a>
              <ul>
                <li>
                  <a href="dg4501582s.html#32">›San Giovanni
                  Calibita‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#32">›San Bartolomeo
                  all'Isola‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501582s.html#33">In Trastevere</a>
              <ul>
                <li>
                  <a href="dg4501582s.html#33">›Santa Maria
                  dell'Orto‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#33">›Santa Cecilia‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#33">›Santa Maria in
                  Trastevere‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#34">›San Crisogono‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#35">›San Francesco a
                  Ripa‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#35">›San Cosimato‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#35">›San Pietro in
                  Montorio‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#36">›San Pancrazio‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#36">›Sant'Onofrio al
                  Gianicolo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501582s.html#36">Nel Borgo</a>
              <ul>
                <li>
                  <a href="dg4501582s.html#36">›Santo Spirito in
                  Sassia‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#37">›Sant'Angelo al
                  Corridoio‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#37">Santa Maria di
                  Campo ›Santa Maria della Pietà‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#38">Santo Stefano delli
                  Indiani ›Santo Stefano degli Abissini‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#38">›Sant'Egidio‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#38">›San Lazzaro‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#38">San Giacobo a
                  Scossa cavallo ›San Giacomo Scossacavalli‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#39">Santa Maria
                  Traspontina ›Santa Maria in Transpontina‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501582s.html#39">Dalla porta Flamminia
              fora del Popolo fino alla radici del Campidoglio</a>
              <ul>
                <li>
                  <a href="dg4501582s.html#39">Sant'Andrea fuori
                  dalla Porta del Popolo nella via flaminia
                  ›Sant'Andrea del Vignola‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#39">›Santa Maria del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#40">›Santa Maria dei
                  Miracoli‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#40">›Trinitá dei
                  Monti‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#40">San Giacobo in
                  Augusta ›San Giacomo in Augusta‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#41">Santo Ambroso sotto
                  al monte de la Trinità ›Santi Ambrogio e Carlo al
                  Corso‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#41">San Rocco a Ripetta
                  ›San Rocco‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#41">Santo Ieronimo de
                  li Schiavoni ›San Girolamo degli Illirici‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#41">›San Lorenzo in
                  Lucina‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#42">San Silvestro ›San
                  Silvestro in Capite‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#42">Monastero delle
                  Convertite ›Monastero di Santa Maria
                  Maddalena‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#42">Santi Apostoli
                  ›Santissimi XII Apostoli‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#43">›San Marcello al
                  Corso‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#43">›Santa Maria in Via
                  Lata‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#44">›San Marco‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#44">›Santa Maria di
                  Loreto‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#44">›Santa Marta
                  (Collegio Romano)‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#45">›Santa Maria della
                  Strada‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#45">›Santa Maria sopra
                  Minerva‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#45">Santa Maria Rotonda
                  ›Pantheon‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#46">›Santa Maria
                  Maddalena‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#46">Santa Maria de
                  campo marzo ›Santa Maria della Concezione in
                  Campo Marzio‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#46">Santa Elisabetta
                  ›Santa Maria in Aquiro‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#46">›San Macuto‹</a>
                </li>
                <li>
                  <a href=
                  "dg4501582s.html#46">›Sant'Eustachio‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#47">›San Luigi dei
                  Francesi‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#47">›Sant'Agostino‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#47">›San Trifone in
                  Posterula‹ [oggi scomparsa]</a>
                </li>
                <li>
                  <a href="dg4501582s.html#47">›Sant'Antonio dei
                  Portoghesi‹</a>
                </li>
                <li>
                  <a href=
                  "dg4501582s.html#48">›Sant'Apollinare‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#48">San Giacobo degli
                  Spagnoli ›Nostra Signora del Sacro Cuore‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#48">›Santa Maria
                  dell'Anima‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#48">›Santa Maria della
                  Pace‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#49">›San Tommaso in
                  Parione‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#49">›San Salvatore in
                  Lauro‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#49">›San Giovanni dei
                  Fiorentini‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#50">San Biagio de la
                  Panetta ›San Biagio della Pagnotta‹]</a>
                </li>
                <li>
                  <a href="dg4501582s.html#50">Santa Lucia detta
                  de la Chiavica ›Santa Lucia del Gonfalone‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#50">S. Joane in Nania
                  ›Santa Maria dell'Orazione e Morte‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#50">›San Girolamo della
                  Carità‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#51">›San Lorenzo in
                  Damaso‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#51">›Santa Barbara dei
                  Librari‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#51">›San Martino de
                  Panerella‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#52">›San Salvatore in
                  Campo‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#52">›Santa Maria in
                  Monticelli‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#52">›San Biagio a'
                  Catinari‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#52">›Santa Maria del
                  Pianto‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#52">›Santa Caterina dei
                  Funari‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#53">›Sant'Angelo in
                  Pescheria‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#53">›San Nicola in
                  Carcere‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#53">›Santa Maria in
                  Aracoeli‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501582s.html#54">Dal Campidoglio aman
              (sic!) manca ne li monti</a>
              <ul>
                <li>
                  <a href="dg4501582s.html#54">›San Pietro in
                  Carcere‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#55">›Sant'Adriano al
                  Foro Romano‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#55">›Santi Cosma e
                  Damiano‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#55">Santa Maria nova
                  ›Santa Francesca Romana‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#56">›San Clemente‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#57">›Santi Quattro
                  Coronati‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#57">›Santi Marcellino e
                  Pietro‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#58">›San Matteo in
                  Merulana‹ [oggi scomparsa]</a>
                </li>
                <li>
                  <a href="dg4501582s.html#58">›San Pietro in
                  Vincoli‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#58">›San Lorenzo in
                  Panisperna‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#59">›Sant'Agata dei
                  Goti‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#59">›San Lorenzo in
                  Fonte‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#59">›Santa
                  Pudenziana‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#60">San Vito in Macello
                  ›Santi Vito e Modesto‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#60">›San Giuliano‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#60">›Sant'Eusebio‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#61">›Santa Bibiana‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#61">›San Martino ai
                  Monti‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#61">›Santa
                  Prassede‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#62">›Santi Quirico e
                  Giulitta‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#62">›Santa Susanna‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#63">›San Vitale‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#63">›Mausoleo di Santa
                  Costanza‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501582s.html#64">Dal Campidoglio a man
              diritta</a>
              <ul>
                <li>
                  <a href="dg4501582s.html#64">Santa Maria
                  Liberatrice ›Santa Maria Antiqua‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#64">›Santa Maria della
                  Consolazione‹</a>
                </li>
                <li>
                  <a href=
                  "dg4501582s.html#64">›Sant'Anastasia‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#65">Santa Maria in
                  portico ›Santa Maria in Campitelli‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#65">›San Gregorio Magno
                  al Celio‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#66">›Santi Giovanni e
                  Paolo‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#66">Santa Maria de la
                  Navicella ›Santa Maria in Domnica‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#66">›Santo Stefano
                  Rotondo‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#67">›San Sisto
                  Vecchio‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#67">›Santa Sabina‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#68">›Sant'Alessio‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#69">›Santa Prisca‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#69">›San Saba‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#70">›Santa Balbina‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#70">›San Giovanni a
                  Porta Latina‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#70">Sant'Anastasio
                  ›Santi Vincenzo ed Anastasio alle Tre
                  Fontane‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#71">Santa Maria
                  annonciata ›Annunziatella‹</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501582s.html#71">Le stationi. Indulgentie,
          et gratie spirituali che sono in le Chiese di Roma
          [...]</a>
          <ul>
            <li>
              <a href="dg4501582s.html#71">El mesa (sic!) di
              Zenaro</a>
            </li>
            <li>
              <a href="dg4501582s.html#73">El mese di Febraro</a>
            </li>
            <li>
              <a href="dg4501582s.html#74">Il mese di Marzo</a>
            </li>
            <li>
              <a href="dg4501582s.html#81">El mese d'Aprile</a>
            </li>
            <li>
              <a href="dg4501582s.html#81">El mese di Maggio</a>
            </li>
            <li>
              <a href="dg4501582s.html#84">El mese di Zugno</a>
            </li>
            <li>
              <a href="dg4501582s.html#85">El mese di Luglio</a>
            </li>
            <li>
              <a href="dg4501582s.html#86">El mese d'Agosto</a>
            </li>
            <li>
              <a href="dg4501582s.html#90">El mese d'Ottobrio</a>
            </li>
            <li>
              <a href="dg4501582s.html#90">El mese di Novembre</a>
            </li>
            <li>
              <a href="dg4501582s.html#92">Il mese di Decembre</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501582s.html#99">La guida romana per tutti i
          forastieri che vengono per vedere le antichita di Roma
          [...]</a>
          <ul>
            <li>
              <a href="dg4501582s.html#99">L'auttore alli Lettori
              carissimi</a>
            </li>
            <li>
              <a href="dg4501582s.html#100">Del Borgho</a>
              <ul>
                <li>
                  <a href="dg4501582s.html#101">Del Trastevere</a>
                </li>
                <li>
                  <a href="dg4501582s.html#102">Dell'Isola
                  Tyberina ›Isola Tiberina‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#102">Del Ponte Santa
                  Maria ›Ponte Rotto‹, Palazzo di pilato ›Casa dei
                  Crescenzi‹, et altre cose ›Ponte Rotto‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#103">Del Monte
                  Testaccio ›Testaccio (Monte)‹ e di molte altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4501582s.html#104">Delle Therme
                  Antoniane ›Terme di Caracalla‹ , ed altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4501582s.html#105">Di ›San Giovanni
                  in Laterano‹, et altre cose</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501582s.html#106">Gioranta seconda</a>
              <ul>
                <li>
                  <a href="dg4501582s.html#106">Del Sepolcro
                  d'Augusto, et altre cose ›Mausoleo di
                  Augusto‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#107">De i Cavalli di
                  marmo che stanno a monte Cavallo ›Quirinale‹ , et
                  delle Therme Diocletiane ›Terme di
                  Diocleziano‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#109">Del Tempio di
                  Iside, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501582s.html#110">Delle ›Sette
                  Sale‹, et del ›Colosseo‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501582s.html#111">Del tempio della
                  Pace ›Foro della Pace‹, et del Palazzo maggiore
                  ›Palatino‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501582s.html#112">Del ›Campidoglio‹,
                  et altre cose</a>
                </li>
                <li>
                  <a href="dg4501582s.html#113">De i portichi di
                  Ottavia ›Portico d'Ottavia‹, et di Settimio
                  ›Porticus Severi‹, et del ›Teatro di Pompeo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501582s.html#113">Giornata terza</a>
              <ul>
                <li>
                  <a href="dg4501582s.html#113">Delle doi (sic!)
                  Colonne una di Antonio Pio, et l'altra di
                  Traiano, et altre cose ›Colonna di Antonino Pio‹
                  ›Colonna Traiana‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#114">Della Rotonda,
                  overo ›Pantheon‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#114">Dei Bagni di
                  Agrippa, et di Nerone ›Terme di Agrippa‹ ›Terme
                  Neroniano-Alessandrine‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#114">Della piazza
                  Nagona (sic!), et di M. Pasquino ›Piazza Navona‹
                  ›Statua di Pasquino‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#115">Da poi Pranzo.
                  Dell'Anticaglie di Monsignor d'Aquino, et del
                  palazzo di San Giorgio, et quello di Monte
                  Pulciano</a>
                </li>
                <li>
                  <a href="dg4501582s.html#115">Di Belvedere
                  ›Cortile del Belvedere‹</a>
                </li>
                <li>
                  <a href="dg4501582s.html#116">Dello (sic!) donne
                  Romane</a>
                </li>
                <li>
                  <a href="dg4501582s.html#116">Di Monsignor di
                  Piasenza, del Sepolcro di Bacco et vigna di Papa
                  Iulio ›Mausoleo di Santa Costanza‹ ›Villa
                  Giulia‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501582s.html#118">Li nomi de tutti li
              sommi Pontefici, delli Re di Francia, delli Re di
              Napoli, di Sicilia, delli Dogi di Vinetia, et delli
              Duchi di Milano</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501582s.html#142">Regola utile e necessaria a
      ciascuna persona che cerchi di vivere come fedele e buon
      Christiano</a>
      <ul>
        <li>
          <a href="dg4501582s.html#144">La vita de un Gentil'huomo
          veramente christiano</a>
        </li>
        <li>
          <a href="dg4501582s.html#171">[Componimento in versi]
          Capitolo del nostro Signor Iesu Christo</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501582s.html#174">Colletto della scrittura
      vecchia, per il quale si prova la chiarezza della santa fede
      cristiana</a>
      <ul>
        <li>
          <a href="dg4501582s.html#176">Allo illustrissimo et
          eccellentissimo signore il Signor Guidobaldo de la Rovere
          Feltrio Duca d'urbino et c.</a>
        </li>
        <li>
          <a href="dg4501582s.html#178">In Dei nomine. Hieremia
          Profeta.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501582s.html#230">Opera nova da insegnar'
      parlar' Hebraico et una disputa contra hebrei, approvando
      esser' venuto il vero Christo</a>
      <ul>
        <li>
          <a href="dg4501582s.html#231">Una interrogatione sopra
          il dodeci articoli de la fede</a>
        </li>
        <li>
          <a href="dg4501582s.html#232">El Giudeo disputa con una
          sua sorella et dice cosí</a>
        </li>
        <li>
          <a href="dg4501582s.html#242">Questi sono li vocaboli
          delli Hebrei, che si dichiararano in lingua
          Christiana</a>
        </li>
        <li>
          <a href="dg4501582s.html#245">Questa si é una Oratione
          che dicano li Hebrei quando si lavano le mani</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
