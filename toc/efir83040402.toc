<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efir83040402s.html#6">Memorie istoriche
      dell’Ambrosiana R. Basilica di S. Lorenzo di Firenze; Tomo
      I.</a>
      <ul>
        <li>
          <a href="efir83040402s.html#8">Illustrissimo, e
          reverendissimo Monsignore Filippo Sardi</a>
        </li>
        <li>
          <a href="efir83040402s.html#10">Prefazione</a>
        </li>
        <li>
          <a href="efir83040402s.html#22">Memorie istoriche
          dell'Ambrosiana R. Basilica di S. Lorenzo di Firenze</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
