<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caraf1474281s.html#7">Cose contenute in questo volume</a>
    </li>
    <li>
      <a href="caraf1474281s.html#12">Lettera sopra un’ anconetta di Raffaello posseduta dalla nobile famiglia Fumagalli in Milano scritta alla gentildonna Felicia Giovio Marchesa Porro da Abbondio Perpenti</a>
    </li>
    <li>
      <a href="caraf1474281s.html#20">Lettera sopra un quadretto di Raffaello posseduto dal nobile sig. Conte Paolo Tosi di Brescia scritta alla nobile signora Giuseppina Paravicini Bertani da Francesco Longhena</a>
    </li>
    <li>
      <a href="caraf1474281s.html#30">Notizie intorno a’ ritratti D’Agnolo e Maddalena Doni dipinti da Raffaello scritte da varj a Francesco Longhena</a>
    </li>
    <li>
      <a href="caraf1474281s.html#54">Lettera sopra il quadro di Raffaello trovato dal professore Angelo Boucheron di Torino e sopra l’ideale pittorico al signor Conte Antonio Galbiani di Sebenico Nicolò Tommaséo</a>
    </li>
    <li>
      <a href="caraf1474281s.html#78">Lettera sopra il quadro creduto originale di Raffaello posseduto da’ signori Brocca negozianti in Milano al suo amico C.A.P. Francesco Ambrosoli</a>
    </li>
    <li>
      <a href="caraf1474281s.html#88">Lettera sopra un quadro attribuito a Raffaello posseduto dal sig. Carlo Sanquirico e sopra un altro di Fra Sebastiani del Piombo posseduto dal sig. Antonio Bozzotti ambedue di Milano al sig. Angelo Longhena ingegnere-architetto Francesco, fratello</a>
    </li>
    <li>
      <a href="caraf1474281s.html#102">Lettera sopra il ritratto di Ant. Teobaldeo dipinto da Raffaello posseduto dal cav. e professore Antonio Scarpa in Pavia a Francesco Longhena il conte Cav. Luigi Bossi</a>
    </li>
    <li>
      <a href="caraf1474281s.html#112">Notizie intorno alla Fornarina sul vero ritratto della stessa dipinto da Raffaello e congettura intorno alla verità di quelli di casa Barberini in Roma e della Galleria di Firenze al nobile sig. Renato Arrigoni ec. ec. ec. Melchior Missirini</a>
    </li>
    <li>
      <a href="caraf1474281s.html#124">Lettera sopra un preziosissimo quadretto di Raffaello posseduto dal Sig. Michele Bisi di Milano incisore all’Egregio Sig. Enrico Carozzi Francesco Longhena</a>
    </li>
    <li>
      <a href="caraf1474281s.html#140">Lettera sopra un quadro rappresentante la Madonna Annunzuiata posseduto dal sig. Fortunato Gozzi di Milano a Francesco Longhena il Conte Cav. Luigi Bossi</a>
    </li>
    <li>
      <a href="caraf1474281s.html#150">Lettera sopra quattro cartoni e lucidi eseguiti mirabilmente sulle quattro più stupende opere di Raffaello che si trovano in Espagna al chiar. sig. Marchese Giuseppe Pallavicini Melchior Missirini</a>
    </li>
  </ul>
  <hr />
</body>
</html>
