<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caraf1464840s.html#8">Raffaello Sanzio studiato
      come architetto con l'aiuto di nuovi documenti per cura del
      barone Enrico di Geymüller</a>
      <ul>
        <li>
          <a href="caraf1464840s.html#12">Indici</a>
        </li>
        <li>
          <a href="caraf1464840s.html#18">Annunciazione. Planche
          I. ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="caraf1464840s.html#20">I. Raffaello Architetto</a>
    </li>
    <li>
      <a href="caraf1464840s.html#22">II. Primo Periodo:
      Raffaello a Urbino</a>
    </li>
    <li>
      <a href="caraf1464840s.html#26">III. Secondo Periodo:
      Raffaello a Perugia e a Firenze (1500-1508)</a>
      <ul>
        <li>
          <a href="caraf1464840s.html#26">Raffaello allo studio
          del Perugino</a>
        </li>
        <li>
          <a href="caraf1464840s.html#28">Raffaello a Firenze</a>
          <ul>
            <li>
              <a href="caraf1464840s.html#28">[Sposalizio della
              Vergine] ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#32">›Pantheon‹ Planche
              II. ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="caraf1464840s.html#34">IV. Terzo Periodo:
      Raffaello a Roma (dal 1508 fino alla morte di Bramante 11
      marzo 1514)</a>
      <ul>
        <li>
          <a href="caraf1464840s.html#34">Incontro di Raffaello e
          di Bramante. Bramante prepara Raffaello a diventare il
          suo successore</a>
        </li>
        <li>
          <a href="caraf1464840s.html#36">Prime fabbriche di
          Raffaello a Roma</a>
          <ul>
            <li>
              <a href="caraf1464840s.html#36">Chiesa di
              ›Sant'Eligio degli Orefici‹ a Roma ▣</a>
              <ul>
                <li>
                  <a href="caraf1464840s.html#37">Figura 4.
                  Lanterna di ›Sant'Eligio degli Orefici‹. Schizzo
                  di Aristotile da Sangallo ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#38">Figura 5.
                  Pianta di ›Sant'Eligio degli Orefici‹ ▣ o modani
                  del tempietto di ›San Pietro in Montorio‹ ▣.
                  Schizzo di Salustio Peruzzi</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#39">Figura 6.
                  Modello per ›Sant'Eligio degli Orefici‹. Schizzo
                  di Salustio Peruzzi. ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#40">Figura 7.
                  ›Sant'Eligio degli Orefici‹. Schizzo di
                  architetto francese ignoto del secolo XVI ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#41">Figura 7bis.
                  ›Sant'Eligio degli Orefici‹ Parte della lanterna
                  annesse alla fig. 7 ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#41">Figura 8.
                  ›Sant'Eligio degli Orefici‹. Spaccato del modello
                  e dell'edifizio attuale. ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="caraf1464840s.html#41">›Palazzo della
              Farnesina‹</a>
              <ul>
                <li>
                  <a href="caraf1464840s.html#42">Figura 9.
                  ›Palazzo della Farnesina‹. Prospetto nord e
                  frammento delle stalle. ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#43">Figura 10.
                  ›Palazzo della Farnesina‹. Facciata verso il
                  fiume. Prospetto restaurato ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#44">Figura 11.
                  ›Palazzo della Farnesina‹. Loggia sul Tevere
                  detta Coffee House. Prospetto restaurato ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#45">Figura 11bis.
                  ›Palazzo della Farnesina‹ ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#46">Figura 12.
                  ›Palazzo della Farnesina‹. Loggia sul Tevere.
                  Prospetto verso la Villa ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#47">Figura 13.
                  ›Palazzo della Farnesina‹. Loggia sul Tevere,
                  Spaccato trasversale ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#48">Figura 14.
                  ›Palazzo della Farnesina‹. Salone del primo
                  piano. Pianta rilevata da Baldassarre Peruzzi.
                  ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#49">Figura 15.
                  ›Palazzo della Farnesina‹. Zoccolo e balaustri.
                  ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#50">Figura 15bis.
                  ›Palazzo della Farnesina‹ Modani della loggetta
                  sul Tevere ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#52">Figura 16.
                  ›Palazzo della Farnesina‹ Modali dello zoccolo
                  interno della loggia nord. ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#54">Figura 17.
                  ›Palazzo della Farnesina‹. Piedistallo dipinto
                  nella Scuola d'Atene. ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#56">Figura 18.
                  ›Palazzo della Farnesina‹. Sulle chigiane. Alzato
                  sulla Lungara, restaurato ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#58">Figura 19.
                  ›Palazzo della Farnesina‹. Stalle Chigiane.
                  Modani degli avanzi sulla Lungara. ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="caraf1464840s.html#59">Cappella Chigi a
              ›Santa Maria del Popolo‹</a>
              <ul>
                <li>
                  <a href="caraf1464840s.html#59">Figura 20.
                  Santa Maria del Popolo. ›Cappella Chigi‹. Tamburo
                  e Lanterna. ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#59">Figura 21.
                  Pianta della ›Cappella Chigi‹ a Santa Maria del
                  Popolo ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#60">Planche III.
                  ›Cappella Chigi‹ ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#62">Figura 22.
                  ›Cappella Chigi‹ Fianco della Cappella
                  dell'Eccellentissima Casa Chigi nella Chiesa di
                  Santa Maria del Popolo ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="caraf1464840s.html#63">Architetture
              dipinte nelle stanze vaticane</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="caraf1464840s.html#66">V. Quarto Periodo:
      Raffaello a Roma (dalla morte di Bramante alla morte di
      Raffaello 14 marzo 1514 - 6 aprile 1520)</a>
      <ul>
        <li>
          <a href="caraf1464840s.html#66">Raffaello architetto in
          Capo della fabbrica di S. Pietro ›San Pietro in
          Vaticano‹</a>
        </li>
        <li>
          <a href="caraf1464840s.html#67">Palazzo Vaticano
          ›Palazzo Apostolico Vaticano‹</a>
        </li>
        <li>
          <a href="caraf1464840s.html#70">Palazzi privati</a>
          <ul>
            <li>
              <a href="caraf1464840s.html#70">›Palazzo
              Caffarelli‹ oggi Vidoni in Roma ▣</a>
              <ul>
                <li>
                  <a href="caraf1464840s.html#71">Figura 24.
                  ›Palazzo Caffarelli‹ Palazzo di Bramante, poi di
                  Raffaello. Restaurato da una antica incisione. Le
                  targhe e le mezzanine nelle metope sono prese
                  dallo schizzo del Palladio. ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#72">Figura 25.
                  ›Palazzo Caffarelli‹. Figura aggrandita da quella
                  del Le Taronilly ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#73">Figura 26.
                  ›Palazzo Caffarelli‹. Stato originale restaurato
                  e contorno dello stato attuale ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="caraf1464840s.html#73">Palazzo Pandolfini
              a Firenze</a>
              <ul>
                <li>
                  <a href="caraf1464840s.html#74">Figura 27.
                  Palazzo Pandolfini a Firenze. Schizzato da una
                  fotografia ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#75">Figura 28.
                  Palazzo Pandolfini. Schizzo preso dal Pontani
                  ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#75">Figura 29.
                  Disegno di Bramante ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="caraf1464840s.html#76">Palazzo dell'Aquila
              in Roma ›Palazzo Branconio dell'Aquila‹ ▣</a>
              <ul>
                <li>
                  <a href="caraf1464840s.html#77">Figura 31.
                  ›Palazzo Branconio dell'Aquila‹. Schizzo
                  restaurato ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#79">›Palazzo
                  Branconio dell'Aquila‹ Planche IV ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="caraf1464840s.html#82">La ›Villa
              Madama‹</a>
              <ul>
                <li>
                  <a href="caraf1464840s.html#82">Figura 32.
                  ›Villa Madama‹ dipinta nella battaglia di
                  Costantino ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#82">Figura 32bis.
                  Cortile della ›Villa Madama‹. Stato attuale.
                  ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#83">Figura 33.
                  ›Villa Madama‹. Stato attuale ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#84">Figura 34.
                  Disegno per la ›Villa Madama‹ riprodotto dal
                  Serlio ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#85">Figura 35.
                  ›Villa Madama‹. Ornamento di stucco. Da una
                  incisione di G. Marchetti ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#85">Figura 36.
                  Frammento restaurato da un progetto per la Loggia
                  della ›Villa Madama‹ ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#86">Figura 37.
                  Studio per la ›Villa Madama‹ ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#87">Figura 38.
                  ›Villa Madama‹. Studio di Antonio da Sangallo per
                  il Ninfeo della Valletta ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#88">Figura 39.
                  Altro studio per il Ninfeo ›Villa Madama‹ ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#89">Figura 40.
                  ›Villa Madama‹. &#160;Studio di Francesco da
                  Sangallo per i giardini ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#90">Figura 41.
                  ›Villa Madama‹. Misura del terreno rilevato da
                  Antonio da Sangallo ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="caraf1464840s.html#94">VI. Riassunto e
      Conclusione</a>
      <ul>
        <li>
          <a href="caraf1464840s.html#94">I. Del modo tecnico di
          Raffaello di schizzare e disegnare l'architettura</a>
          <ul>
            <li>
              <a href="caraf1464840s.html#95">Figura 42. Disegno
              attribuito al Perugino (Museo del Louvre) ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#96">Figura 43. Studio
              di Raffaello per la Madonna degli Ansidici. (Istituto
              Staedel a Francoforte) ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#97">Figura 44. Tempio
              nello Sposalizio del Perugino (Museo di Caen) ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#97">Figura 45. Tempio
              dipinto a fresco dal Perugino (Cappella Sistina a
              Roma) ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#98">Figura 46. Tempio
              in una tavola di Luca Signorelli (Galleria degli
              Uffizi) ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#98">Figura 47. Edifizio
              dipinto a fresco da Andrea Mantegna. (Palazzo ducale
              di Mantova) ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#99">Figura 48. ›Palazzo
              Branconio dell'Aquila‹. Frammento di schizzo del
              Dosio ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#99">Figura 49. ›Palazzo
              della Farnesina‹. Prospetto nord. [...] ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#100">Figura 50. Schizzi
              di Raffaello. (Museo Wicar a Lille) ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="caraf1464840s.html#100">II. Riassunto dei
          fatti principali della carriera architettonica di
          Raffaello</a>
          <ul>
            <li>
              <a href="caraf1464840s.html#102">Figura 51. Sistema
              della Farnesina, lucidato dall'opera del Le
              Tarouilly, coll'aggiunta dello zoccolo ora sotterrato
              ›Palazzo della Farnesina‹ ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#102">Figura 52. Sistema
              del Palazzo della Cancelleria, lucidato dall'opera
              del Le Tarouilly, senza le bugne di bassorilievo
              ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#103">Figura 52 (sic!).
              Parte d'un disegno di Pierino del Vaga (Già nella
              raccolta His de la Salle a Parigi) ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#104">Figura 54.
              Frammento di disegno attribuito a Domenico Fontana.
              La figura A è situata in A1. (Già nella raccolta His
              de la Salle) ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="caraf1464840s.html#105">III. Studio fatto da
          Raffaello delle fabbriche di Bramante</a>
          <ul>
            <li>
              <a href="caraf1464840s.html#105">Figura 55. Disegno
              per ›San Pietro in Vaticano‹ di Roma. Ingressi
              laterali ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#105">Figura 56.
              Paragone di vari studii per ›San Pietro in Vaticano‹
              ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#106">Figura 57. Parte
              di un disegno attribuito a Pierino del Vaga (Museo
              del Louvre) ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#106">Figura 58.
              Paragone tra il disegno S d'Antonio per la Loggia e
              lo stato attuale R ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="caraf1464840s.html#107">IV. Raffaello
          continuatore dell'ultima maniera di Bramante</a>
          <ul>
            <li>
              <a href="caraf1464840s.html#108">Figura 59. Schizzo
              di Bramante per l'architettura della Scuola d'Atene.
              Da un disegno che si conserva all'Università di
              Oxford pubblicato dal prof. Springer ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="caraf1464840s.html#109">Conclusione</a>
          <ul>
            <li>
              <a href="caraf1464840s.html#110">Figura 60.
              Ingresso della ›Villa Madama‹ verso Roma. Ristaurato
              dealla pianta di Antonio da Sangallo ▣</a>
            </li>
            <li>
              <a href="caraf1464840s.html#111">I. Nuove
              disposizioni introdotte da Raffaello</a>
            </li>
            <li>
              <a href="caraf1464840s.html#111">II. Alcuni motivi
              prediletti da Raffaello</a>
              <ul>
                <li>
                  <a href="caraf1464840s.html#113">Planche V.
                  ›Villa Madama‹ &#160;▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#116">Figura 61.
                  Giardini della ›Villa Madama‹ restaurati dal
                  disegno di Francesco da Sangallo ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#117">Figura 62.
                  ›Villa Madama‹. Spaccato restaurato della pianta
                  di Giovan Battista da Sangallo detto il Gobbo
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="caraf1464840s.html#117">III. &#160;Lavori
              a ›San Pietro in Vaticano‹</a>
            </li>
            <li>
              <a href="caraf1464840s.html#118">Studi per la
              ›Villa Madama‹</a>
              <ul>
                <li>
                  <a href="caraf1464840s.html#118">Figura 63.
                  ›Villa Madama‹. Piazza delle Fontane e Piscina
                  restaurate dalla pianta di Giovanni Battista da
                  Sangallo ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#120">Figura 64.
                  ›Villa Madama‹. Prospetto verso il tevere.
                  Restaurato dalla pianta disegnata da Giovan
                  Battista da Sangallo per Raffaello ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#120">Fugura 65.
                  ›Villa Madama‹. Prospetto realizzato dalla pianta
                  disegnata da Antonio Cordiani, detto Antonio da
                  Sangallo il Giovane, per Raffaello ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#123">Planche VI.
                  ›Villa Madama‹ ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#127">Planche VII.
                  ›Villa Madama‹ ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#130">Figura 66.
                  ›Villa Madama‹. Prospettiva restaurata dalla
                  pianta di Raffaello (Tav. VII) ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#131">Figura 67.
                  Monumento creduto per il Marchese di Mantova.
                  Ridotto da un disegno fatto per Raffaello da un
                  suo disegnatore (Museo del Louvre) ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#132">Figura 68.
                  Pianta del terreno comprato da Raffaello nella
                  ›Via Giulia‹ ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#134">Fugura 70.
                  Parte della casa di Bramante e di Raffaello nella
                  quale morirono i due grandi. Schizzo del Palladio
                  ▣</a>
                </li>
                <li>
                  <a href="caraf1464840s.html#137">Planche VIII.
                  ›Villa Madama‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="caraf1464840s.html#140">Appendici</a>
    </li>
    <li>
      <a href="caraf1464840s.html#152">Indice Generale</a>
    </li>
  </ul>
  <hr />
</body>
</html>
