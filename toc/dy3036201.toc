<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy3036201s.html#6">Nuova descrizione del Vaticano,
      o sia Della Sacrosanta Basilica di S. Pietro; Tomo I.</a>
      <ul>
        <li>
          <a href="dy3036201s.html#8">Altezza Reale
          Eminentissima</a>
        </li>
        <li>
          <a href="dy3036201s.html#14">Prefazione</a>
        </li>
        <li>
          <a href="dy3036201s.html#26">Indice</a>
        </li>
        <li>
          <a href="dy3036201s.html#32">Approvazione</a>
        </li>
        <li>
          <a href="dy3036201s.html#36">Introduzione</a>
        </li>
        <li>
          <a href="dy3036201s.html#52">Nuova Descrizione del
          Vaticano</a>
        </li>
        <li>
          <a href="dy3036201s.html#52">I. Della Piazza, e suo
          Anfiteatro</a>
        </li>
        <li>
          <a href="dy3036201s.html#65">II. Della Facciata</a>
        </li>
        <li>
          <a href="dy3036201s.html#69">III. Del Portico della
          Chiesa</a>
        </li>
        <li>
          <a href="dy3036201s.html#81">IV. Della interna
          Basilica, e sue parti</a>
        </li>
        <li>
          <a href="dy3036201s.html#83">V. Della Cappella della
          Pietà, e sua annessa Cappelletta del Crocifisso</a>
        </li>
        <li>
          <a href="dy3036201s.html#91">VI. Arco Primo con i due
          Deposti d'Innocenzio XIII., e della Regina di Svezia</a>
        </li>
        <li>
          <a href="dy3036201s.html#94">VII. Della Cappella di San
          Sebastiano Martire</a>
        </li>
        <li>
          <a href="dy3036201s.html#97">VIII. Secondo Arco con i
          Due Depositi del Pontefice Innocenzio XII. e della
          Contessa Matilde</a>
        </li>
        <li>
          <a href="dy3036201s.html#100">IX. Della Cappella
          dell'Augustissimo Sagramento; Dell'Altare di San
          Maurizio; e Deposito di Papa Sisto IV.&#160;</a>
        </li>
        <li>
          <a href="dy3036201s.html#108">X. Arco terzo con i
          Depositi de' Pontefici Gregorio XIII., e Gregorio
          XIV.</a>
        </li>
        <li>
          <a href="dy3036201s.html#110">XI. Dell'Altare di San
          Girolamo</a>
        </li>
        <li>
          <a href="dy3036201s.html#111">XII. Della Cappella
          Gregoriana</a>
        </li>
        <li>
          <a href="dy3036201s.html#115">XIII. Dell'Altare di San
          Basilio</a>
        </li>
        <li>
          <a href="dy3036201s.html#117">XIV. Della Tribuna
          Aquilonare, ove esistono gli Altari de' Ss. Processo, e
          Martiniano, di S. Erasmo, e di S. Vuincislao</a>
        </li>
        <li>
          <a href="dy3036201s.html#121">XV. Dell'Altare della
          Navicella</a>
        </li>
        <li>
          <a href="dy3036201s.html#122">CVI. Della Cappella, ed
          Altare di S. Michele Arcangelo, e sua sovraposta
          Cupola</a>
        </li>
        <li>
          <a href="dy3036201s.html#125">XVII. Dell'Altare di S.
          Petronilla</a>
        </li>
        <li>
          <a href="dy3036201s.html#127">XVIII. Dell'Altare della
          Tabìta</a>
        </li>
        <li>
          <a href="dy3036201s.html#128">XIX. Deposito di Clemente
          X.</a>
        </li>
        <li>
          <a href="dy3036201s.html#130">XX. Della Tribuna, ove
          risiede la maestosa Cattedra di San Pietro, ed ai lati
          due Depositi di Paolo III., e di Urbano VIII.</a>
        </li>
        <li>
          <a href="dy3036201s.html#139">XXI. Altare di San
          Pietro, che libera lo stroppio, con il Deposito di
          Alessandri VIII. incontro&#160;</a>
        </li>
        <li>
          <a href="dy3036201s.html#141">XXII. Altare di San Leone
          Magno&#160;</a>
        </li>
        <li>
          <a href="dy3036201s.html#143">XXIII. Altare della
          Madonna, detta della Colonna, e sua Cupola</a>
        </li>
        <li>
          <a href="dy3036201s.html#146">XXIV. Altare della Caduta
          di Simon Mago, e Deposito di Alessandri VII.
          dirimpetto</a>
        </li>
        <li>
          <a href="dy3036201s.html#148">XXV. Della Tribuna
          Meridionale, ove risiedono i tre Altari de' Santi Simone,
          e Giuda Apostili, de' Santi Marziale, e Valeria, e
          dell'Apostolo San Tomasso</a>
        </li>
        <li>
          <a href="dy3036201s.html#152">XXVI. Altare della
          Crocifissione di San Pietro, e Porta della Sagrestìa</a>
        </li>
        <li>
          <a href="dy3036201s.html#153">XXVII. Della Cappella
          Clementina</a>
        </li>
        <li>
          <a href="dy3036201s.html#155">XXVIII. Dell'Altare dei
          Santi Apostoli Pietro, e Andrea</a>
        </li>
        <li>
          <a href="dy3036201s.html#156">XXIX. Arco terzo, ove
          sono collocati i Depositi di Leone XI., e d'Innocenzio
          XI.</a>
        </li>
        <li>
          <a href="dy3036201s.html#159">XXX. Della Cappella del
          Coro</a>
        </li>
        <li>
          <a href="dy3036201s.html#168">XXXI. Arco secondo con il
          Deposito d'Innocenzio VIII.</a>
        </li>
        <li>
          <a href="dy3036201s.html#172">XXXIII. Arco Primo, ove
          esiste il Deposito della Regina della Gran Bretagna</a>
        </li>
        <li>
          <a href="dy3036201s.html#173">XXXIV. Della Cappella
          Ultima, ove esiste il Sacro Fonte Battesimale&#160;</a>
        </li>
        <li>
          <a href="dy3036201s.html#177">XXXV. Della Navata di
          mezzo</a>
        </li>
        <li>
          <a href="dy3036201s.html#187">XXXVI. Della interna
          Cupola Maggiore di mezzo, e seguito della Navata
          superiore</a>
        </li>
        <li>
          <a href="dy3036201s.html#198">XXXVII. Dell'Altare
          Maggiore, e Sacra Confessione</a>
        </li>
        <li>
          <a href="dy3036201s.html#206">XXXVIII. Delle Sacre
          Grotte Vaticane</a>
        </li>
        <li>
          <a href="dy3036201s.html#269">XXXIX. Delle Chiaviche
          sotteranee, che servono per lo scolo dell'acque superiori
          della Basilica</a>
        </li>
        <li>
          <a href="dy3036201s.html#276">XL. Della Sagrestìa di
          San Pietro</a>
        </li>
        <li>
          <a href="dy3036201s.html#299">XLI. Dell'Archivio del
          Reverendissimo Capitolo</a>
        </li>
        <li>
          <a href="dy3036201s.html#317">XLIII. [XLII.] Delle
          Volte Superiori della Basilica Vaticana&#160;</a>
        </li>
        <li>
          <a href="dy3036201s.html#320">XLIII. Ripiano che
          conduce all'interna Cupola del Battesimo, alla scala a
          Lumaca del Campanile, ed al Portico Superiore</a>
        </li>
        <li>
          <a href="dy3036201s.html#327">XLIV. Stanza bislunga
          sopra l'Arco Primo della Minor Navata</a>
        </li>
        <li>
          <a href="dy3036201s.html#328">XLV. Primo Corridore, che
          gira attorno tutta la Basilica, e corrisponde alle prime
          Finestre, che illuminano la Chiesa</a>
        </li>
        <li>
          <a href="dy3036201s.html#340">XLVI. Diverse Stanze che
          s'incontrano nelle grossezze de Muri dal piano del primo
          Corridore fino al secondo&#160;</a>
        </li>
        <li>
          <a href="dy3036201s.html#342">XLVII. Secondo Corridore,
          che corrisponde al Cornicione grande dell'interna
          Basilica, e superiori Finestre</a>
        </li>
        <li>
          <a href="dy3036201s.html#353">XLVIII. Lastrico
          Superiore nel Piano della Cupola sopra le Volte</a>
        </li>
        <li>
          <a href="dy3036201s.html#363">XLIX. Delle due Cupole
          Gregoriana, e Clementina, e della Cupola Maggiore</a>
        </li>
        <li>
          <a href="dy3036201s.html#386">L. Continuazione del
          Lastrico</a>
        </li>
        <li>
          <a href="dy3036201s.html#391">LI. Delle Volticelle, che
          sostengono il Lastrico superiore, e delle Stanze Ottagone
          situate nel Terzo Piano delle Volte</a>
        </li>
        <li>
          <a href="dy3036201s.html#398">LII. Continuazione del
          Lastrico Superiore&#160;</a>
        </li>
        <li>
          <a href="dy3036201s.html#401">LIII. Del Prospetto
          esterno della Basilica</a>
        </li>
        <li>
          <a href="dy3036201s.html#412">LIV. Confronto della
          Cupola, e Basilica Vaticana colle altre Cupole, e Tempi
          piu rinnomati del Mondo</a>
        </li>
        <li>
          <a href="dy3036201s.html#430">Indice delle cose
          notabili</a>
        </li>
        <li>
          <a href="dy3036201s.html#480">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
