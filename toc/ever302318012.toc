<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ever302318012s.html#6">Le vite de’ pittori,
      degli scultori, et architetti veronesi</a>
      <ul>
        <li>
          <a href="ever302318012s.html#8">Noi Reformatori dello
          studio di Padoua</a>
        </li>
        <li>
          <a href="ever302318012s.html#10">Indice</a>
        </li>
        <li>
          <a href="ever302318012s.html#14">Prefatione</a>
        </li>
        <li>
          <a href="ever302318012s.html#19">[Vite]</a>
        </li>
        <li>
          <a href="ever302318012s.html#217">Seguono gli
          Scultori e gli Architetti moderni&#160;</a>
        </li>
        <li>
          <a href="ever302318012s.html#229">Pitture insigni,
          che s'attrovano nelle Chiese, e ne' luoghi publici di
          Verona</a>
        </li>
        <li>
          <a href="ever302318012s.html#284">Pitture a fresco
          sopra le facciate, e dentro le Case di questa Città di
          Pittori Veronesi, e d'altri Forestieri</a>
        </li>
        <li>
          <a href="ever302318012s.html#304">Galeria di
          Quadri</a>
        </li>
        <li>
          <a href="ever302318012s.html#362">Pitture, che
          s'attrovano per lo Territorio Veronese descritte per
          ordine d'alfabeto de' loro luoghi</a>
        </li>
        <li>
          <a href="ever302318012s.html#374">Aggiunta alle vite
          de pittori, degli schultori et architetti veronesi</a>
          <ul>
            <li>
              <a href="ever302318012s.html#376">Prefazione</a>
            </li>
            <li>
              <a href="ever302318012s.html#380">[Aggiunta alle
              vite]</a>
            </li>
            <li>
              <a href="ever302318012s.html#456">Tavola degli
              artefici</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
