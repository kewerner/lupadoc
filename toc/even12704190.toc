<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="even12704190s.html#6">Ragguaglio delle cose
      notabili nella chiesa e nel seminario patriarcale di Santa
      Maria della Salute in Venezia</a>
    </li>
    <li>
      <a href="even12704190s.html#54">Notizie storiche della
      veneta chiesa priorale abbaziale intotolata S. Maria di
      Misericordia della Valverde</a>
    </li>
    <li>
      <a href="even12704190s.html#74">Illustrazione
      storico-critica della chiesa di S. Sofia che si riapre al
      culto divino dalla sua primissima fondazione fino a’nostri
      giorni</a>
      <ul>
        <li>
          <a href="even12704190s.html#76">Al prestantissimo
          Signore Giovanni Battisti Rebelleri Veneziano</a>
        </li>
        <li>
          <a href="even12704190s.html#80">Prefazione</a>
        </li>
        <li>
          <a href="even12704190s.html#84">[Text]</a>
        </li>
        <li>
          <a href="even12704190s.html#176">Indice</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="even12704190s.html#180">Lettere a Callofilo che
      illustrano la chiesa di San Marziale Vescovo</a>
      <ul>
        <li>
          <a href="even12704190s.html#184">A Callofilo</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="even12704190s.html#206">Leggende sopra Santa Fosca
      Vergine e Martire di Ravenna e sopra la chiesa di Santa Fosca
      in Venezia</a>
      <ul>
        <li>
          <a href="even12704190s.html#208">Al reverendissimo
          parroco de' SS. Ermagora e Fortunato</a>
        </li>
        <li>
          <a href="even12704190s.html#212">Leggenda sopra Santa
          Fosca V. M.</a>
        </li>
        <li>
          <a href="even12704190s.html#222">Leggenda sopra la
          chiesa di S. Fosca in Venezia</a>
        </li>
        <li>
          <a href="even12704190s.html#252">Indice</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="even12704190s.html#254">Cenni intorno alla chiesa
      di Santo Zaccaria di Venezia</a>
    </li>
    <li>
      <a href="even12704190s.html#277">La chiesa di S. Maria del
      Pianto in Venezia, ridonata al culto pubblico nel 21
      settembre 1851 &#160;: cenni storici intitolati ed offerti a
      monsignore illustriss. e reverendissimo Federico marchese dei
      Manfredini vescovo di Famagosta</a>
    </li>
    <li>
      <a href="even12704190s.html#291">Storia della chiesa
      parrocchiale de’ Santi Apostoli di Venezia unitamente alla
      serie cronologica de’ suoi piovani fino al giorno
      presente</a>
      <ul>
        <li>
          <a href="even12704190s.html#293">Al reverendissimo
          Signore D. Giuseppe D. Villotti</a>
        </li>
        <li>
          <a href="even12704190s.html#295">Storia della chiesa
          parrocchiale de' Santi Apostoli di Venezia</a>
        </li>
        <li>
          <a href="even12704190s.html#304">Serie cronologica
          illustrata dei piovani de' Santi Apostoli di Venezia</a>
        </li>
        <li>
          <a href="even12704190s.html#335">Annotazioni</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="even12704190s.html#347">Memorie storico-artistiche
      sull’Arciconfraternita di S. Rocco &#160;: operetta basata su
      documenti autentici e divisa in quattro parti ; notizie
      storiche della scuola, guida della scuola, notizie storiche
      della chiesa, guida della chiesa</a>
      <ul>
        <li>
          <a href="even12704190s.html#349">I. Notizie storiche
          della scuola di San Rocco&#160;</a>
        </li>
        <li>
          <a href="even12704190s.html#357">II. Guida della Scuola
          di San Rocco&#160;</a>
        </li>
        <li>
          <a href="even12704190s.html#363">III. Notizie storiche
          della chiesa di San Rocco</a>
        </li>
        <li>
          <a href="even12704190s.html#366">IV. Guida della chiesa
          di San Rocco</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="even12704190s.html#371">Breve notizia intorno alla
      origine della confraternita di S. Giovanni Evangelista in
      Venezia</a>
    </li>
    <li>
      <a href="even12704190s.html#387">Notizie
      storico-pittoresche dell’oratorio de’ SS. Filippo Neri e
      Luigi Gonzaga annesso all’ospizio di S. Maria de’ Crociferi
      in Campo de’ Gesuiti</a>
      <ul>
        <li>
          <a href="even12704190s.html#389">A sua eccellenza Conte
          Carlo Michiel</a>
        </li>
        <li>
          <a href="even12704190s.html#391">I. Storiche
          Notizie&#160;</a>
          <ul>
            <li>
              <a href="even12704190s.html#403">Annotazioni</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even12704190s.html#409">II. Notizie
          pittoresche&#160;</a>
          <ul>
            <li>
              <a href="even12704190s.html#419">Annotazioni</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="even12704190s.html#429">Brevi notizie della chiesa
      e dell’ex convento di S. Maria dei Miracoli unica chiesa in
      Venezia col titolo della Immacolata Concezione</a>
    </li>
    <li>
      <a href="even12704190s.html#449">La chiesa e il seminario
      di Santa Maria della Salute in Venezia &#160;: opera postuma
      con aggiunte</a>
      <ul>
        <li>
          <a href="even12704190s.html#451">Al nobile Signor
          Giovanni Co. Correr</a>
        </li>
        <li>
          <a href="even12704190s.html#453">Della vita e degli
          scritti di Giannantonio Moschini</a>
        </li>
        <li>
          <a href="even12704190s.html#469">La chiesa e il
          Seminario di Santa Maria della Salute</a>
          <ul>
            <li>
              <a href="even12704190s.html#474">Della chiesa</a>
            </li>
            <li>
              <a href="even12704190s.html#521">Del seminario</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even12704190s.html#616">Annotazioni</a>
        </li>
        <li>
          <a href="even12704190s.html#621">Indice dei luoghi</a>
        </li>
        <li>
          <a href="even12704190s.html#623">Indice generale</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="even12704190s.html#655">La chiesa di San Marziale
      vesc. nell’occasione faustissima che il novello pievano Don
      Giambattista Pisani ne prende solennemente possesso</a>
      <ul>
        <li>
          <a href="even12704190s.html#657">Veneratissimo
          Pievano</a>
        </li>
        <li>
          <a href="even12704190s.html#659">La chiesa di San
          Marziale Vescovo</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="even12704190s.html#683">La Chiesa di San Felice in
      Venezia ove dopo cinquant’anni di sacerdozio Monsignor
      Giuseppe Wiel, notario apostolico e pievano, celebra
      solennemente il divin sacrifizio il dì 11 luglio 1847</a>
      <ul>
        <li>
          <a href="even12704190s.html#685">Monsignore
          Reverendissimo</a>
        </li>
        <li>
          <a href="even12704190s.html#689">La chiesa di San
          Felice Nolano</a>
        </li>
        <li>
          <a href="even12704190s.html#705">Annotazioni</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="even12704190s.html#713">Il martirio di
      Sant’Apollinare pala da altare, dipinta da Lattanzio
      Querena</a>
    </li>
    <li>
      <a href="even12704190s.html#721">Guida per la chiesa di S.
      Maria Gloriosa dei Frari di Venezia</a>
    </li>
    <li>
      <a href="even12704190s.html#729">Indice</a>
    </li>
  </ul>
  <hr />
</body>
</html>
