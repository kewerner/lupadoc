<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="bn1054900s.html#8">Architectural studies in
      Italy</a>
      <ul>
        <li>
          <a href="bn1054900s.html#11">[plates]</a>
        </li>
        <li>
          <a href="bn1054900s.html#17">Subscribers names</a>
        </li>
        <li>
          <a href="bn1054900s.html#18">Preface</a>
        </li>
        <li>
          <a href="bn1054900s.html#20">Contents</a>
        </li>
        <li>
          <a href="bn1054900s.html#24">Plates&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
