<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501430s.html#6">Opera di Andrea Fvlvio delle
      antichità della Città di Roma, [et] delli edificij memorabili
      di quella</a>
      <ul>
        <li>
          <a href="dg4501430s.html#8">Paulus Papa Tertio</a>
        </li>
        <li>
          <a href="dg4501430s.html#11">Al reverendissimo Messer
          Francesco Soderini</a>
        </li>
        <li>
          <a href="dg4501430s.html#14">Libro primo</a>
          <ul>
            <li>
              <a href="dg4501430s.html#17">Di che età Romulo
              edificò la città di Roma</a>
            </li>
            <li>
              <a href="dg4501430s.html#20">In qual regione del
              mondo è posta Roma</a>
            </li>
            <li>
              <a href="dg4501430s.html#24">Delle mura, et circuito
              di Roma antica</a>
            </li>
            <li>
              <a href="dg4501430s.html#28">Delle porti, et vie
              della Città&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#73">Delle regioni antiche
              di Roma che erono XIIII [sic!]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501430s.html#91">Libro secondo</a>
          <ul>
            <li>
              <a href="dg4501430s.html#92">Del monte Capitolino et
              de gli ornamenti di quello&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#112">Del monte Palatino et
              de gli ornamenti di quello&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#125">Del monte Aventino, et
              delle cose che in quello si contengono&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#132">Del monte Celio e del
              monte Celiolo, et delle cose appartenente di
              queeli&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#142">Del monte Esquilino et
              da gli ornamenti di quello&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#154">Del monte Viminale</a>
            </li>
            <li>
              <a href="dg4501430s.html#157">Del monte Quirinale et
              della aggiunta di quello&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#167">Del monte Pincio overo
              colle de gli hortuli</a>
            </li>
            <li>
              <a href="dg4501430s.html#171">Del monte Vaticano et
              de gli ornamenti di quello&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#176">Del monte Ianiculo et
              de luoghi che gli sono all'intorno&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501430s.html#184">Libro terzo</a>
          <ul>
            <li>
              <a href="dg4501430s.html#184">Del Tevere</a>
            </li>
            <li>
              <a href="dg4501430s.html#189">De ponti che sono
              sopra il Tevere et de loro edificatori&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#199">De gli Acquidotti</a>
            </li>
            <li>
              <a href="dg4501430s.html#218">Delle Terme et de gli
              edificatori di quelle&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#237">De Fori cioè piazze,
              overo mercati, della antica città di Roma, et chi
              furono i loro edificatori&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501430s.html#276">Libro quarto</a>
          <ul>
            <li>
              <a href="dg4501430s.html#276">De gli archi
              trionfali</a>
            </li>
            <li>
              <a href="dg4501430s.html#287">De Teatri,et degli
              anfiteatri, et della forma di quelli&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#294">De gli Anfiteatru, et
              della forma di quelli&#160;</a>
            </li>
            <li>
              <a href="dg4501430s.html#299">De Cerchi, et della
              forma di quelli, et come e' fussero lavorati, et
              addorni</a>
            </li>
            <li>
              <a href="dg4501430s.html#319">De Portichi, et come
              egli erono fatti</a>
            </li>
            <li>
              <a href="dg4501430s.html#327">Delle colonne, et come
              le sono lavorate</a>
            </li>
            <li>
              <a href="dg4501430s.html#333">De gli Obelisci, cioè
              Agulie, et della forma di quelli</a>
            </li>
            <li>
              <a href="dg4501430s.html#343">De Cimiteri</a>
            </li>
            <li>
              <a href="dg4501430s.html#344">De Settizonij</a>
            </li>
            <li>
              <a href="dg4501430s.html#345">De Colossi, et della
              forma loro</a>
            </li>
            <li>
              <a href="dg4501430s.html#348">Delle statue et de
              Simulacri</a>
            </li>
            <li>
              <a href="dg4501430s.html#356">Delle curie, et della
              differenza di quelle</a>
            </li>
            <li>
              <a href="dg4501430s.html#359">De Senatuli</a>
            </li>
            <li>
              <a href="dg4501430s.html#360">De Grecostasi</a>
            </li>
            <li>
              <a href="dg4501430s.html#361">Del Comitio et de'
              Comitij</a>
            </li>
            <li>
              <a href="dg4501430s.html#362">Del Ginnasio, et della
              Academia</a>
            </li>
            <li>
              <a href="dg4501430s.html#363">Delle antiche
              Bibliothece, cioè librerie</a>
            </li>
            <li>
              <a href="dg4501430s.html#365">Quando si ritrovò
              l'arte dello stampare</a>
            </li>
            <li>
              <a href="dg4501430s.html#365">De Vestibuli et Atrij
              di Roma antica</a>
            </li>
            <li>
              <a href="dg4501430s.html#367">[De Carceri]</a>
            </li>
            <li>
              <a href="dg4501430s.html#372">Del monte Testaccio,
              et della cagione del suo crescimento</a>
            </li>
            <li>
              <a href="dg4501430s.html#374">Dello Hippodromom cioè
              corso nelquale' correvano i cavalli</a>
            </li>
            <li>
              <a href="dg4501430s.html#375">De gli Arzanali, et
              luoghi ove si tenevano la navi</a>
            </li>
            <li>
              <a href="dg4501430s.html#376">De Granai, et
              Ripostigli delle biade</a>
            </li>
            <li>
              <a href="dg4501430s.html#376">De luoghi, et
              Magazzini del Sale&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501430s.html#378">Libro quinto</a>
          <ul>
            <li>
              <a href="dg4501430s.html#378">[Luoghi sagrati
              ]&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501430s.html#454">Tavola di tutte le cose
          che si contengono nell'opera</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
