<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghfel32812760s.html#6">Des principes de
      l’architecture, de la sculpture, de la peinture et des autres
      arts qui en dependent</a>
      <ul>
        <li>
          <a href="ghfel32812760s.html#8">A Messire Jule Armad
          Colbert</a>
        </li>
        <li>
          <a href="ghfel32812760s.html#14">Preface</a>
        </li>
        <li>
          <a href="ghfel32812760s.html#26">Table des
          chapitres</a>
        </li>
        <li>
          <a href="ghfel32812760s.html#30">I. De
          l'architecture</a>
        </li>
        <li>
          <a href="ghfel32812760s.html#327">II. De la
          sculpture</a>
        </li>
        <li>
          <a href="ghfel32812760s.html#419">III. De la
          peinture</a>
        </li>
        <li>
          <a href="ghfel32812760s.html#486">Dictionnaire des
          termes propres a l'architecture, a la sculpture, a la
          peinture, et aux autres arts qui en dépendent</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
