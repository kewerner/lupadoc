<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="erim403940s.html#6">Tempio Malatestiano de’
      Francescani di Rimini, parte prima</a>
      <ul>
        <li>
          <a href="erim403940s.html#14">Avviso al lettore</a>
        </li>
        <li>
          <a href="erim403940s.html#18">Al Signor Don Carlo
          Fossati</a>
        </li>
        <li>
          <a href="erim403940s.html#24">Notizie della vita di
          Leon Battista Alberti</a>
        </li>
        <li>
          <a href="erim403940s.html#36">Descrizione delle cose
          più notabili contenute nel Tempio di San Francesco di
          Rimini</a>
        </li>
        <li>
          <a href="erim403940s.html#50">[Tavole]</a>
        </li>
        <li>
          <a href="erim403940s.html#68">Appendice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
