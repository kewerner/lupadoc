<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zsalg4373640c1s.html#8">Opere del conte
      Algarotti, Tom. I.</a>
      <ul>
        <li>
          <a href="zsalg4373640c1s.html#10">Alla sacra reale
          maesta' di Federico Guglielmo II. Re di Prussia</a>
        </li>
        <li>
          <a href="zsalg4373640c1s.html#16">A' lettori</a>
        </li>
        <li>
          <a href="zsalg4373640c1s.html#36">Memorie intorno
          alla vita ed agli scritti del conte Francesco Algarotti
          scritte dall'abate Domenico Michelessi</a>
          <ul>
            <li>
              <a href="zsalg4373640c1s.html#38">A Federico il
              grande</a>
            </li>
            <li>
              <a href="zsalg4373640c1s.html#42">Memorie</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zsalg4373640c1s.html#178">Poesie del conte
          Francesco Algarotti</a>
          <ul>
            <li>
              <a href="zsalg4373640c1s.html#180">A Madama du
              Boccage&#160;</a>
            </li>
            <li>
              <a href="zsalg4373640c1s.html#184">Epistole in
              versi</a>
              <ul>
                <li>
                  <a href="zsalg4373640c1s.html#186">Alla
                  maesta' di Federico re di Prussia</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#188">Alla
                  maesta' di Federico II. Re di Prussia</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#190">Alla
                  maesta' di Anna Giovannona</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#192">Alla
                  maesta' di Augusto III.</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#195">Al
                  serenissimo principe Pietro Grimani</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#199">Al Signor
                  abate Pietro Metastasio</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#203">A
                  Fillide</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#206">Ad
                  Aristo</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#209">Al Signor
                  Eustachio Zanotti</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#213">Al Signor
                  Eustachio Manfredi</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#216">A
                  Eudosso</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#218">Al Signor
                  Conte Cesare Gorani</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#225">Al Signor
                  Francesco di Voltaire</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#230">A S. E. il
                  Signor Marco Foscarini</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#234">A
                  Lesbia</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#236">Al Signor
                  Tommaso Villiers</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#241">A S.E. il
                  Signor Alessandro Zeno</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#253">Alla
                  maesta' di Elisabetta</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#255">Al Signor
                  Eustachio Zanotti</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#261">Al Signor
                  Eustachio Manfredi</a>
                </li>
                <li>
                  <a href=
                  "zsalg4373640c1s.html#272">Annotazioni</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c1s.html#288">Rime</a>
              <ul>
                <li>
                  <a href="zsalg4373640c1s.html#290">Al nobil
                  uomo Signor marchese Ubertino Landi&#160;</a>
                </li>
                <li>
                  <a href="zsalg4373640c1s.html#378">Ad
                  Francisci Mariae Zanotti Carmina Elegia</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c1s.html#382">Indice delle
              Poesie</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
