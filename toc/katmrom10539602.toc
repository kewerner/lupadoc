<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katmrom10539602s.html#6">Sculture del palazzo
      della ›Villa Borghese‹ detta Pinciana. Parte II [Tavole]</a>
      <ul>
        <li>
          <a href="katmrom10539602s.html#8">Stanza IV</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#8">1. Venere pudica
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#10">2. Giove
              sedente ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#12">3. Enea ed
              Anchise col fanciullo Ascanio Gruppo del Cavalier
              Bernino ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#14">4. Statua
              incognita di qualche Greco illustre ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#16">5. Polluce o
              Cestiario ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#18">6. Cerere
              Velata ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#20">7. Minerva
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#22">8. Bacco, e
              Sileno gruppo minore del naturale ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#24">9. Gruppo d'un
              capro con Genj Bacchici scultura del secolo
              decimosesto ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#26">10. Statua
              togata d'Augusto ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#28">11. Giuocatrice
              agli aliossi Ora Ninfa con conchiglia ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#30">12. Satiro che
              trae dal pie' d'un Fauno una spina ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#32">13. Venere
              volgare ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#34">14. Gruppo
              delle tre Grazie ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#36">15. Gruppo di
              putti dormenti [sic] su tavola di paragone scultura
              del secolo decimosesto ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#38">16. Apollo e
              Dafne Gruppo di Lorenzo Bernino ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#40">17. Ara con
              bucrani ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#42">18. Satiro con
              Siringa ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#44">19. Fauno con
              tirso e pelle di pantera bassorilievo ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#46">20. Baccante
              con vaso ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#48">21. Vergine
              Spartana che danza ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#50">22. Baccante
              Spartana con corona di foglie di canna e timpano
              bassorilievo ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#52">23. Vergine
              Spartana che danza ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#54">24. Scherzi di
              Putti Bassorilievo in paragone col campo di
              lapislazuli [sic] ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#56">25. Cacciatore
              Moro Statuetta di paragone ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#58">26. Altro
              Cacciatore Moro in paragone ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#60">Stanza V</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#60">1. Giove con
              aquila e fulmine ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#62">2. Venere per
              entrar nel bagno ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#64">3. Brittanico
              [sic] con bolla al petto ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#66">4. Statua
              Romana con bulla ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#68">5. Venere per
              entrar nel bagno ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#70">6. Marte
              imberbe ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#72">7. Venere
              vincitrice ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#74">8. Fauno
              sonante la Tibia ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#76">9. Venere
              Marina ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#78">10. Figure
              allusive alla stagione dell'autunno ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#80">11. Figure
              allusive alla stagione dell'Estate ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#82">12. Figure
              allusive alla stagione della Primavera ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#84">13. Figure
              allusive alla stagione dell'autunno ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#86">14. Testa
              muliebre maggiore del naturale di bizzarra
              acconciatura creduta di Berenice Regina d'Egitto
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#88">15. Lucio Vero
              Augusto [illustrazione ripetuta; v. Parte I] ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#90">16. Busto di
              Marco Aurelio ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#92">17. Venere
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#94">18. Lucio Vero
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#96">19. Busto con
              ritratto incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#98">20. Testa quasi
              colossale di Lucio Vero trovata ad acqua-traversa
              dov'era la Villa di questo Imperatore ›Villa di Lucio
              Vero‹ ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#100">21. Testa
              quasi colossale di Marco Aurelio trovata insieme con
              quella di Lucio Vero ad Acqua-traversa ›Villa di
              Lucio Vero‹ ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#102">22. Busto
              d'Eroe forse Diomede ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#104">23. Busto di
              Settimio Severo ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#106">24. Lucio
              Settimio Severo ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#108">25. Busto di
              Lucio Vero ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#110">26. Busto con
              testa di Venere ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#112">27. Roma busto
              quasi colossale ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#114">28. Busto di
              Diana maggiore del naturale ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#116">29. Busto di
              Nerone ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#118">Stanza VI</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#118">1. Musa o
              Sonatrice di Tibie ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#120">2. Telesforo
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#122">3. Gruppo di
              ritratti Romani in sembianza di Venere, e Marte ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#124">4. Cupido in
              atto di tender l'arco ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#126">5. Flora o
              Cloride ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#128">6. Gruppo di
              Mercurio, e Vulcano ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#130">7. Ermafrodito
              dormente [sic] Trovato presso le ›Terme di
              Diocleziano‹ ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#132">8. Venere in
              atto di uscir dal bagno ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#134">9. Gruppo di
              Venere marina con Cupido e pistrice ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#136">10. Venere
              Gnidia [sic] ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#138">11. Apollo
              Pizio ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#140">12. L'Autunno
              bassorilievo ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#142">13.
              Bassorilievo scolpito intorno ad un'ara tonda e
              rappresentante un Baccanale ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#144">14. Testa
              colossale creduta rappresentare la Spagna ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#146">Stanza VII</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#146">1. Atleta
              vincitore ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#148">2. Lupa
              lattante di rosso antico ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#150">3. Atleta che
              si unge ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#152">4. Gruppo
              rappresentante una Affricana col suo infante: è di
              marmo statuario colle carnagioni di nero antico ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#154">5. Cerere
              [illustrazione ripetuta; v. Parte I] ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#156">6. Giovinetto
              che si trae dal piede una spina copia in marmo dal
              bronzo antico Capitolino ›Statua dello Spinario‹
              &#160;▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#158">7. Pugile con
              cesti ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#160">8. Cigniale
              [sic] di marmo bigio ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#162">9. Discobolo o
              sia giuocator di ruzzola ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#164">10. Eroe
              combatente [sic] detto volgarmente il Gladiatore
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#166">11.
              Combattente ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#168">12. La Musa
              Polinnia ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#170">13. Il Sonno
              Scultura in paragone d'Alessandro Algardi ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#172">14. Venere
              marina, e Cupido Scultura a bassorilievo del secolo
              sestodecimo ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#174">15. Fauni in
              baccanale ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#176">16. Fiancate
              del sarcofago rappresentante la favola d'Atteone
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#178">17. Sarcofago
              rappresentante la favola d'Atteone ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#180">18. Meleagro
              moribondo con Atalanta e le sorelle ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#182">19. Busto di
              Venere ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#184">20. Perseo
              ultimo Re de' Macedoni ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#186">21. Alessandro
              Magno ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#188">Stanza VIII</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#188">1. Iside di
              granitello ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#190">2. Osiride
              Statua moderna scolpita in paragone ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#192">3. Idolo
              Egizio ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#194">4. Sacerdote
              Egiziano ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#196">5. Simulacro
              di Diana detto volgarmente la Zingarella ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#198">6. Adorante
              Statua di porfido coll'ignudo di marmo statuario
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#200">7. Moro Statua
              d'alabastro fiorito coll'ignudo di paragone ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#202">8. Iside
              sedente con testa di gatto in granito Egizio ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#204">9. Zingara
              Statua di marmo bianco con sopravesta di bigio e
              carni di metallo ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#206">10. Iside
              Statua moderna di paragone colle carnagioni
              d'alabastro bianco ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#208">11. Diana
              Cacciatrice il cui torso antico è d'alabastro agatino
              il rimanente è ristaurato in bronzo ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#210">12. Busto
              d'Iside di marmo bigio ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#212">13. Busto
              antico d'alabastro con testa di bronzo imitante la
              Niobe ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#214">Stanza IX</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#214">1. Centauro
              domato dal Genio di Bacco ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#216">2. Ninfa ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#218">3. Cupido
              coronato d'edera ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#220">4. Talia ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#222">5. Statua di
              Soemiade in sembianza di Cerere ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#224">6. Apollo
              Licio ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#226">7. Amore che
              dorme ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#228">8. Musa con
              tibie ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#230">9. Gruppo
              d'Amore e di Psiche ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#232">10. Cerere
              coronata di spiche [sic] ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#234">11. Amore
              Volgarmente detto il Genio ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#236">12. Cupido
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#238">13. Gruppo di
              Sileno e Bacco bambino ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#240">Secondo Piano</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#240">Elena ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#242">Paride ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#244">Venere ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#246">Marte ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#248">Giove ▣</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#250">Apollo ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="katmrom10539602s.html#254">Sculture del palazzo
      della ›Villa Borghese‹ detta Pinciana brevemente descritte.
      Parte seconda [Testo]</a>
      <ul>
        <li>
          <a href="katmrom10539602s.html#256">Stanza IV</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#257">1.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#258">2.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#259">3.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#259">4.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#260">5.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#261">6.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#262">7.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#262">8.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#263">9.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#263">10.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#264">11.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#265">12.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#265">13.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#266">14.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#267">15.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#268">16.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#269">17.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#270">18. 19.
              20.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#271">21. 22.
              23.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#272">24. 25.
              26.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#274">Stanza V</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#278">1.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#278">2.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#279">3.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#279">4.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#280">5.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#280">6.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#281">7.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#282">8.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#283">9.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#284">10. 11. 12.
              13.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#285">14.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#285">15. 16.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#286">17.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#286">18.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#286">19.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#287">20.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#287">21.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#288">22.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#289">23. 24.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#289">25.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#289">26.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#290">27.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#290">28.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#291">29.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#292">Stanza VI</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#294">1.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#294">2.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#295">3.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#296">4.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#297">5.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#297">6.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#299">7.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#300">8.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#301">9.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#302">10.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#302">11.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#303">12.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#303">13.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#304">14.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#306">Stanza VII</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#309">1.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#309">2.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#309">3.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#310">4.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#310">5.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#311">6.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#312">7.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#312">8.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#312">9.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#313">10.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#315">11.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#316">12.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#316">13.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#317">14.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#318">15.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#318">16.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#320">17.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#322">18.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#322">19.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#323">20.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#323">21.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#324">Stanza VIII</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#329">1.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#330">2.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#331">3.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#331">4.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#332">5.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#333">6.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#335">7.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#335">8.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#336">9.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#337">10.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#337">11.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#338">12.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#339">13.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#340">Stanza IX</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#342">1.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#344">2.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#344">3.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#345">4.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#346">5.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#346">6.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#347">7.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#347">8.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#347">9.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#348">10.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#349">11.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#350">12.</a>
            </li>
            <li>
              <a href="katmrom10539602s.html#351">13.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539602s.html#353">Piano
          Superiore</a>
          <ul>
            <li>
              <a href="katmrom10539602s.html#353">Stanza di
              Paride</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
