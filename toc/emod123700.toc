<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="emod123700s.html#6">Le pitture e sculture di
      Modena</a>
      <ul>
        <li>
          <a href="emod123700s.html#8">Lettor cortese</a>
        </li>
        <li>
          <a href="emod123700s.html#11">[Le pitture e sculture di
          Modena]</a>
        </li>
        <li>
          <a href="emod123700s.html#104">Descrizione delle
          pitture, e dei disegni che esistono nel Grande Ducale
          Appartamento di Francesco III.&#160;</a>
        </li>
        <li>
          <a href="emod123700s.html#206">Descrizione delle
          pitture che cono nel Palazzo di questo Illustrissimo, e
          Rispettabilissimo Pubblico</a>
        </li>
        <li>
          <a href="emod123700s.html#217">Indice nel quale
          soltanto sono accennati alcuni Palazzi, e Case che
          tengono, e posseggono celebri Dipinture</a>
        </li>
        <li>
          <a href="emod123700s.html#220">Indice delle chiese</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
