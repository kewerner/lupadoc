<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cabar21903810s.html#6">Notizie intorno al pittore
      Gasparantonio Baroni Cavalcabò di Sacco</a>
      <ul>
        <li>
          <a href="cabar21903810s.html#8">Al Signor Clemente
          Baroni Cavalcabò Revisor perpetuo dell'Accademia di
          Roveredo&#160;</a>
        </li>
        <li>
          <a href="cabar21903810s.html#12">Notizie</a>
        </li>
        <li>
          <a href="cabar21903810s.html#80">Non ostante ciò che si
          legge nell'introduzione a questa Vita, l'Autore stima
          opportuno d'aggiungere qui una breve Latina Lettera da
          lui scritta</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
