<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghric29273220s.html#6">An account of some of the
      statues, bas-reliefs, drawings and pictures in Italy,
      &amp;c., with remarks</a>
      <ul>
        <li>
          <a href="ghric29273220s.html#8">To thei Royal Highness
          the Prince and Princess</a>
        </li>
        <li>
          <a href="ghric29273220s.html#10">The preface</a>
        </li>
        <li>
          <a href="ghric29273220s.html#35">[Index of cities]</a>
        </li>
        <li>
          <a href="ghric29273220s.html#40">[Index of
          artists]&#160;</a>
        </li>
        <li>
          <a href="ghric29273220s.html#53">Antiques</a>
        </li>
        <li>
          <a href="ghric29273220s.html#56">Rotterdam</a>
        </li>
        <li>
          <a href="ghric29273220s.html#58">Leiden</a>
        </li>
        <li>
          <a href="ghric29273220s.html#58">Hague</a>
        </li>
        <li>
          <a href="ghric29273220s.html#58">Amsterdam</a>
        </li>
        <li>
          <a href="ghric29273220s.html#59">Antwerp</a>
        </li>
        <li>
          <a href="ghric29273220s.html#60">Brusselles</a>
        </li>
        <li>
          <a href="ghric29273220s.html#61">[Paris]</a>
        </li>
        <li>
          <a href="ghric29273220s.html#77">Fountainbleau</a>
        </li>
        <li>
          <a href="ghric29273220s.html#78">Milan</a>
        </li>
        <li>
          <a href="ghric29273220s.html#83">Modena</a>
        </li>
        <li>
          <a href="ghric29273220s.html#84">Piacenza</a>
        </li>
        <li>
          <a href="ghric29273220s.html#84">Parma</a>
        </li>
        <li>
          <a href="ghric29273220s.html#85">Bologna</a>
        </li>
        <li>
          <a href="ghric29273220s.html#97">Florence</a>
          <ul>
            <li>
              <a href="ghric29273220s.html#141">Of painting and
              sculpture</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghric29273220s.html#153">Rome</a>
          <ul>
            <li>
              <a href="ghric29273220s.html#248">The Vatican</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghric29273220s.html#382">Pisa</a>
        </li>
        <li>
          <a href="ghric29273220s.html#383">Sienna</a>
        </li>
        <li>
          <a href="ghric29273220s.html#384">Lucca</a>
        </li>
        <li>
          <a href="ghric29273220s.html#385">Parma</a>
        </li>
        <li>
          <a href="ghric29273220s.html#393">Modena</a>
        </li>
        <li>
          <a href="ghric29273220s.html#401">Mantua</a>
        </li>
        <li>
          <a href="ghric29273220s.html#404">Verona</a>
        </li>
        <li>
          <a href="ghric29273220s.html#406">Dusseldorf</a>
        </li>
        <li>
          <a href="ghric29273220s.html#408">Addenda</a>
        </li>
        <li>
          <a href="ghric29273220s.html#416">Postscript</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
