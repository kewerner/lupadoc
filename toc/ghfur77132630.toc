<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghfur77132630s.html#8">Mannhaffter Kunst-Spiegel
      Oder Continuatio, und fortsetzung allerhand Mathematisch und
      Mechanisch hochnutzlich Sowol auch sehr erfrölichen
      delectationen, und respective im Werck selbsten
      experimentirten freyen Künsten [...]</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#10">Mannhaffter
          Kunst-Spiegel Oder Continuatio und fortsetzung deren von
          Gott auß Genaden den Menschen mitgetheilten Ingenirischen
          Delectationen, mit ihren 16. anverwandten
          Recreationen</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#16">Dem Durchleuchtigsten
          Fürsten und Herrn Herrn Carl Ludwigen Pfaltz [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#20">Durchleuchtigister
          Churfürst Durchleuchtigiste Fürsten Hochgeborne Graven
          Hochwürdige etc. [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#23">An den Günstig: und
          wolgeneigten Leser</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#24">Register</a>
    </li>
    <li>
      <a href="ghfur77132630s.html#36">[Tre testi poetici
      dedicati all'autore]</a>
    </li>
    <li>
      <a href="ghfur77132630s.html#39">Innhalt deß Tittulblats
      No: 1.</a>
    </li>
    <li>
      <a href="ghfur77132630s.html#46">[Von der Arithmetica]</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#81">Das Kupfferblatt No:
          2. Grundriß deß Paradeiß Gartlins</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#83">[Kupferblatt No:
          2.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#88">Der Auffzug deß
          Grunds</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#90">Von der Geometria</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#91">Das Kupfferblat [sic]
          No: 3. Mit einem gemeinen Tischler Winckelhacken die
          weite von einem Orth zu dem andern nach Geometrischer
          Arth zuerkundigen</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#91">Beschreibung der
          ersten Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#93">Beschreibung der
          andern Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#93">Die dritte
          Geometrische Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#95">[Kupferblatt No:
          3.]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#101">Von der Planimetria</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#103">Das Planimetrische
          Kupfferblatt No: 4.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#103">Die Erste Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#105">[Kupferblatt No:
          4.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#110">Die ander Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#112">Beschreibung der
          dritten Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#116">Die vierdte Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#118">Beschreibung der
          fünfften Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#119">Beschreibung der
          sechsten Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#120">Die sibende Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#122">Die neundte Figur
          [sic]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#126">Von der Geographia</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#129">Das Kupfferblatt No:
          5. Ein Landschafft von zehen Flecken und Dörffer von dem
          Feld herein auff eine Mappen zubringen</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#131">Die Mappen N° 5
          [Kupferblatt]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#136">Von der Astronomia</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#139">Die Sonnenuhr beym
          Kupfferblatt No: 6.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#139">Die erste Figur
          [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#141">Die Sonnenuhr N° 6
          [Kupferblatt]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#144">Die ander Figur
          [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#145">Die Sonnenuhr oder
          das Kupfferblatt No: 7.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#145">Die dritte Figur
          [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#147">Die Sonnenuhr N° 7
          [Kupferblatt]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#150">Die vierdte Figur
          [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#151">Die Sonnenuhr oder
          das Kupfferblatt No: 8.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#151">Die fünfte Figur
          [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#153">Die Sonnenuhr N° 8
          [Kupferblatt]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#156">Die sechste Figur
          [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#161">Die Sonnenuhr oder
          das Kupfferblatt No: 9.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#161">Die vierdte Abtragung
          bey der sibenden Figur [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#163">Die Sonnenuhr N° 9
          [Kupferblatt]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#167">Die fünffte Abtragung
          der achten Figur</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#166">Von der Navigation</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#171">Das Kupfferblatt No:
          10. Die Bussola zu der Navigation recht zuverfertigen</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#173">Die Bussola N° 10
          [Kupferblatt]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#180">Von der Prospectiva</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#183">Das Kupfferblatt No:
          11. Der erste Grundriß die gantze grösse deß Comoedi
          Gebäws</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#185">[Kupferblatt No:
          11.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#195">[Kupferblatt No: 11
          1/2.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#198">Der ander Grundriß
          das Kupfferblatt No: 11 1/2. Die Secunda Scena, oder die
          ander verwandlung</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#199">Der dritte
          Durchschnitt</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#201">Das Kupfferblatt No:
          12.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#201">Auffzug der Prima
          Scena</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#203">[Kupferblatt No:
          12.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#206">Auffzug der Secunda
          Scena</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#207">Das Kupfferblatt No:
          13. Hernachstehende Machinae wurden zu underschidlichen
          Comoedien, mit sonderbarem Belieben der Zuseher gebraucht
          wie folgt</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#209">[Kupferblatt No:
          13.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#212">Von auff- und
          absteigenden Wolcken</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#214">Wie der innere Kasten
          erstlich zuerbawen seye</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#216">Hernachfolgende
          Machinae, seynd bey denen auß dem Erdboden herfür
          kommenden sowohlen auch auß dem Himmel herunder fallenden
          Dingen gebraucht worden</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#219">Von vier
          unterschidlichen Beleuchtungen erstlich die Dellampen</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#222">Von vier
          underschidlichen Meerwellen</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#222">Von der
      &#160;Mechanica</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#227">[Kupferblatt No:
          14.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#230">Das Kupfferbl. No:
          14.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#230">Die erste Mechanische
          Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#231">Die ander Mechanische
          Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#231">Das Kupfferbl. No:
          15.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#231">Die dritte
          Mechanische Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#233">[Kupferblatt No:
          15.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#237">[Kupferblatt No:
          16.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#240">Die vierdte
          Mechanische Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#240">Das Kupfferblatt No:
          16.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#240">Die fünfte
          Mechanische Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#241">Die sechste
          Mechanische Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#243">[Kupferblatt No:
          17.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#246">Das Kupfferb. No:
          17.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#246">Die sibende
          Mechanische Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#247">Die achtende
          Mechanische Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#247">Die neundte
          Mechanische Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#249">[Kupferblatt No:
          18.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#252">Das Kupfferbl. No:
          18.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#252">Die 10. Mechanische
          Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#253">Die ailffte
          Mechanische Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#253">Die zwölffte
          Mechanische Figur [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#253">Die dreyzehende
          Mechanische Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#254">Die vierzehende
          Mechanische Figur</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#254">Von dem Grottenwerck</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#259">[Kupferblatt No:
          19.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#262">Das Kupfferblatt No:
          19. die Manier der Drietter</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#267">Das Kupfferblatt No:
          20. Ein grosses Truckwerck sampt dem Stempffel und seiner
          Kurben</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#269">[Kupferblatt No:
          20.]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#279">Von den
      Wasserlaitungen</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#283">[Kupferblatt No:
          21.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#286">Das Kupfferblatt No:
          21.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#286">Von der
          Wasserlaitungen die erste Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#286">Die ander Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#287">Von Wasserlaitungen
          noch ein mahl die erste Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#291">Die dritte Figur
          [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#291">Die vierdte Figur
          [...]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#293">Die fünffte Figur
          [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#297">Von dem Feurwerck
      erstlichen vom Büchsenpulver</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#303">[Kupferblatt No:
          22.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#306">Das Kupfferbl. No:
          22. Ein Handpulverstampff</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#315">[Kupferblatt No:
          23.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#318">Das Kupfferblatt No:
          23.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#318">Die erste Figur
          [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#334">Von der
      Büchsenmeisterey</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#337">Das Kupfferblatt No:
          24.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#339">[Kupferblatt No:
          24.]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#347">Von der Architectura
      Militari</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#359">Aüffzüg deß Berghaüß
          N° 25. [Kupferblatt]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#362">Das Kupfferblatt No:
          25. Der Auffzug deß Berghauses</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#365">Das Kupfferblatt No:
          26. Grundriß deß Berghauß</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#367">Bründriß [sic] Züm
          Berghaüs N°: 26. [Kupferblatt]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#387">Das Kupfferblatt No:
          27. Grundriß der Schantzkorb oder der Batterey Kästen</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#389">[Kupferblatt No:
          27.]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#395">Von der Architectura
      Civili</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#399">Das Kupfferblatt No:
          28. Der Grundriß zum Schawspil-Saal</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#401">[Kupferblatt No:
          28.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#409">[Kupferblatt No:
          29.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#412">Das Kupfferblatt No:
          29.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#412">Die steinerne Brucken
          und erste Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#413">Die ander Figur</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#417">Aquadote, und
          Continouatio [sic], der Wasserlaitungen. Discurs über die
          Wasser Conduten, die dritte Figur</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#418">Von der Architectura
      Navali</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#425">[Kupferblatt No:
          30.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#428">Das Kupfferblatt No:
          30.</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#428">Grundriß deß ersten
          Schiffbodens</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#430">Grundriß deß andern
          Schiffbodens</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#435">[Kupferblatt No:
          31.]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#438">Das Kupfferblatt No:
          31. Auffzug deß Schiffs und wie dasselbige mit Menschen
          sambt dem Gethier in der Insul Fortunata ist beladen
          worden</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfur77132630s.html#440">[Von dem Insul Gebäw]</a>
      <ul>
        <li>
          <a href="ghfur77132630s.html#445">Die Insel N° 32.
          [Kupferblatt]</a>
        </li>
        <li>
          <a href="ghfur77132630s.html#448">Das Insul Gebäw. Das
          Kupfferblatt No: 32. Grundriß zu dem Insul Gebäw</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
