<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlan982540906s.html#6">Storia pittorica della
      Italia; tomo VI. Che contiene gl’indici generali
      dell’opera&#160;</a>
      <ul>
        <li>
          <a href="ghlan982540906s.html#10">Al lettore</a>
        </li>
        <li>
          <a href="ghlan982540906s.html#12">Indice primo:
          Professori nominati in quest'opera&#160;</a>
        </li>
        <li>
          <a href="ghlan982540906s.html#180">Indice secondo:
          Libri d'istoria e di critica citati per l'opera&#160;</a>
        </li>
        <li>
          <a href="ghlan982540906s.html#214">Indice terzo: Di
          alcune cose notabili</a>
        </li>
        <li>
          <a href="ghlan982540906s.html#226">Correzioni ed
          aggiunte di tutta l'opera&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
