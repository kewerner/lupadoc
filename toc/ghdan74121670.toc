<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghdan74121670s.html#8">Il primo libro del Trattato
      delle perfette proporzioni di tutte le cose che imitare, e
      ritrarre si possano con l’arte del disegno</a>
      <ul>
        <li>
          <a href="ghdan74121670s.html#10">All'illustrissimo et
          eccellentissimo Signor Cosimo de Medici</a>
        </li>
        <li>
          <a href="ghdan74121670s.html#16">Prefazione di tutta
          l'opera</a>
        </li>
        <li>
          <a href="ghdan74121670s.html#21">Del trattato delle
          perfette proporzioni di tutte le cose, libro primo</a>
          <ul>
            <li>
              <a href="ghdan74121670s.html#21">I. Che l'ordine è
              un'ottimo mezzo, à conseguire la perfetta proporzione
              dei composti nelle tre manifatture, Divine, Naturali,
              e Humane</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#27">II. Che il
              composto ordinato puo essere facile, dificile, e
              impossibile a mettersi in atto [...]</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#27">III. Che come
              nelle cose Naturali la piu perfetta, è piu difficile
              composozione è il composto de l'huomo [...]</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#28">IIII. [sic] Che
              mediante la cognizione dell'uso, e cagione delle
              cose, si conoscono quali in loro debbano essere le
              perfette proporzioni&#160;</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#30">V. Che la bellezza
              propriamente si vede, e riplende nelle membra, ed
              altre cose atte à conseguire il lorofine</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#33">VI. Che la
              bellezza puo haver luogo in tutte l'età dell'huomo
              [...]</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#35">VII. Che la grazia
              è parte di bellezzacorporale interiore</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#37">VIII. Che le cose
              proporzionate, essendo belle, piaciono, per che son
              parimente buone</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#38">IX. Che il vero
              mezzo di pervenire alla cognizione delle perfette
              proporzioni delle membra degl'animali, è propriamente
              l'uso della Notomia</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#40">X. Che la
              proporzione nasce, e depende dall'ordine, e la
              diferenza, che è tra loro</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#41">XI. Che il modo
              d'operare nell'arti del disegno no cade sotto alcuna
              misura di quantità perfettamente come vogliono alcuni
              [...]</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#48">XII. Che la
              proporzione puo trovarsi in tutti corpi inanimati</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#53">XIII. Che, e in
              che modo ne i corpi vegetativi, si trovi la perfetta
              proporzione&#160;</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#61">XIIII. [sic] Che
              cosi ne gl'animali sensitivi, come i corpi vegetativi
              ed in quelli, che sono senz'anima [...]</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#69">XV. Come nelle
              cose artefitiate: consiste la perfetta
              proporzione</a>
            </li>
            <li>
              <a href="ghdan74121670s.html#72">XVI. Della
              diferenza ch'io intendo che sia tra l'immitare, e
              ritrarre&#160;</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
