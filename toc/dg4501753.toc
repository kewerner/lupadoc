<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501753s.html#6">Mirabilia Romae adonde se trata
      de las yglesias, reliquias, stationes desta santa ciudad a sy
      dentro como fuera de sos muros</a>
      <ul>
        <li>
          <a href="dg4501753s.html#8">Las siete yglesias
          principales</a>
          <ul>
            <li>
              <a href="dg4501753s.html#8">La primera yglesia es
              San Ioan Laterano ›San Giovanni in Laterano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501753s.html#11">[›San Pietro in
              Vaticano‹ ] ▣</a>
            </li>
            <li>
              <a href="dg4501753s.html#14">[San Paolo fuori le
              Mura‹] ▣</a>
            </li>
            <li>
              <a href="dg4501753s.html#15">[›Santa Maria
              Maggiore‹] ▣</a>
            </li>
            <li>
              <a href="dg4501753s.html#16">[›San Lorenzo fuori le
              Mura‹] ▣</a>
            </li>
            <li>
              <a href="dg4501753s.html#17">[›San Sebastiano‹]
              ▣</a>
            </li>
            <li>
              <a href="dg4501753s.html#18">[›Santa Croce in
              Gerusalemme‹] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501753s.html#19">Las otras yglesias de Roma
          y las indulgentias y reliquias que enellas ay son estas
          que se siguen</a>
        </li>
        <li>
          <a href="dg4501753s.html#58">Las staciones</a>
          <ul>
            <li>
              <a href="dg4501753s.html#59">Las staciones dela
              Quaresma</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501753s.html#67">Tratado overamente manera
          de ganar la indulgencias enlas estaciones</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501753s.html#73">La Guia Romana, para todos los
      Forasteros, y Estrangeros, que viene a Roma [...]</a>
      <ul>
        <li>
          <a href="dg4501753s.html#73">El Author al lector</a>
        </li>
        <li>
          <a href="dg4501753s.html#74">[Giornata prima]</a>
          <ul>
            <li>
              <a href="dg4501753s.html#74">Del Transtiber
              ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#75">De la Isla Tiberina
              ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#76">Dela puente de santa
              Maria ›Ponte Rotto‹, y palacio de Pilatos ›Casa dei
              Crescenzi‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#77">Delas Thermas
              Antonianas et cetera ›Terme di Caracalla‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#77">De san Iaon Laterano
              ›San Giovanni in Laterano‹, y otras cosas</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501753s.html#78">Iornada secunda</a>
          <ul>
            <li>
              <a href="dg4501753s.html#78">Del sepulcro de Augusto
              ›Mausoleo di Augusto‹, y de otras cosas</a>
            </li>
            <li>
              <a href="dg4501753s.html#78">Delos cavallos de
              marmol, que stan en monte Cavallo ›Fontana di Monte
              Cavallo‹, y dellas thermas</a>
            </li>
            <li>
              <a href="dg4501753s.html#80">Delas siete salas
              ›Sette Sale‹, y del Coliseo ›Colosseo‹, et cetera</a>
            </li>
            <li>
              <a href="dg4501753s.html#81">Del templo dela Paz
              ›Foro della Pace‹, y del Palacio mayor ›Palatino‹, y
              otras cosas</a>
            </li>
            <li>
              <a href="dg4501753s.html#81">Del Capitolio
              ›Campidoglio‹, y de otras cosas</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501753s.html#82">Iornada tercera</a>
          <ul>
            <li>
              <a href="dg4501753s.html#82">Delas dos columnas, de
              Antonino Pio ›Colonna di Antonino Pio‹, y de Trajano
              ›Colonna Traiana‹, et cetera</a>
            </li>
            <li>
              <a href="dg4501753s.html#83">De la Redonda, o
              ›Pantheon‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#83">Dela plaza Nagon
              ›Piazza Navona‹, y de M. Pasquino ›Statua di
              Pasquino‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#84">Despues de comer</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501753s.html#85">Los nomnbres de todos Los
      Pontifices Romanos, Emperadores, Reyes de Francia, de Napoles
      y Sicilia, de Duques de Ventia y de Milan</a>
      <ul>
        <li>
          <a href="dg4501753s.html#85">Los Summos Pontifices</a>
        </li>
        <li>
          <a href="dg4501753s.html#114">Reges, et Imperatores
          Romanos</a>
          <ul>
            <li>
              <a href="dg4501753s.html#114">Consules</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501753s.html#118">Los Reyes de Francia</a>
        </li>
        <li>
          <a href="dg4501753s.html#120">Los Reyes de reyno des
          Napoles, y des Sicilia, los quales comenzaron a reynare
          el anno di nostra salude 1525</a>
          <ul>
            <li>
              <a href="dg4501753s.html#120">Normandos</a>
            </li>
            <li>
              <a href="dg4501753s.html#120">Tudeschos</a>
            </li>
            <li>
              <a href="dg4501753s.html#120">Franceses</a>
            </li>
            <li>
              <a href="dg4501753s.html#121">Aragoneses</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501753s.html#121">Los Duques de Venecia</a>
        </li>
        <li>
          <a href="dg4501753s.html#124">Los Duques de Milan</a>
        </li>
        <li>
          <a href="dg4501753s.html#125">Tabla delas Ygelsias</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501753s.html#130">Las antiguedades de Roma</a>
      <ul>
        <li>
          <a href="dg4501753s.html#131">Libro I</a>
          <ul>
            <li>
              <a href="dg4501753s.html#131">Dela edificacion di
              Roma</a>
            </li>
            <li>
              <a href="dg4501753s.html#134">Del Circuitu de
              Roma</a>
            </li>
            <li>
              <a href="dg4501753s.html#135">Delas puertas</a>
            </li>
            <li>
              <a href="dg4501753s.html#136">Delas Calles de
              Roma</a>
            </li>
            <li>
              <a href="dg4501753s.html#137">Delas Puentes que
              estan sopre el Rio Tiber y sus edificadores</a>
            </li>
            <li>
              <a href="dg4501753s.html#138">Dela ysla del Tiber
              ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#139">Delos montes</a>
            </li>
            <li>
              <a href="dg4501753s.html#140">Del Monte de Testachio
              ›Testaccio (Monte)‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#140">Delas aguas quien las
              traya a Roma</a>
            </li>
            <li>
              <a href="dg4501753s.html#142">Del Cloaca ›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#142">Delos Conductos de
              Agua</a>
            </li>
            <li>
              <a href="dg4501753s.html#143">Delas siete salas
              ›Sette Sale‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#143">Delas Termas, o
              veramente banos y de sus fundadores</a>
            </li>
            <li>
              <a href="dg4501753s.html#145">Delas Naumaquias donde
              se hazian las batallas navales, y que cosa eran</a>
            </li>
            <li>
              <a href="dg4501753s.html#145">Delos Cercos y que
              cosa eran</a>
            </li>
            <li>
              <a href="dg4501753s.html#146">Delos Theatros y que
              cosa eran y de sus edificadores</a>
            </li>
            <li>
              <a href="dg4501753s.html#146">Delos Amphitheatros y
              edificadores dellos y que cosa eran</a>
            </li>
            <li>
              <a href="dg4501753s.html#147">Delas Plazas</a>
            </li>
            <li>
              <a href="dg4501753s.html#148">Delos Arcos
              triumphales y aquien se davan</a>
            </li>
            <li>
              <a href="dg4501753s.html#149">Delos Porticos</a>
            </li>
            <li>
              <a href="dg4501753s.html#149">Delos Tropheos y
              Columnas memorables</a>
            </li>
            <li>
              <a href="dg4501753s.html#150">Delos Colosos</a>
            </li>
            <li>
              <a href="dg4501753s.html#151">Dela Piramyde
              ›Piramide di Caio Cestio‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#151">Delas metas</a>
            </li>
            <li>
              <a href="dg4501753s.html#151">Delos obeliscos
              overamete agujas</a>
            </li>
            <li>
              <a href="dg4501753s.html#152">Delas estatuas</a>
            </li>
            <li>
              <a href="dg4501753s.html#152">De Marfodio ›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#152">Delos Cavallos
              ›Fontana di Monte Cavallo‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#152">Delas librarias</a>
            </li>
            <li>
              <a href="dg4501753s.html#153">Delos reloxes</a>
            </li>
            <li>
              <a href="dg4501753s.html#153">Delos Palacios</a>
            </li>
            <li>
              <a href="dg4501753s.html#154">Dela casa aurea de
              Neron ›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#155">Delas Curias y que
              cosa eran</a>
            </li>
            <li>
              <a href="dg4501753s.html#156">Delos senados y que
              cosa eran</a>
            </li>
            <li>
              <a href="dg4501753s.html#156">Delos magistrados</a>
            </li>
            <li>
              <a href="dg4501753s.html#157">De los Comicios y que
              cosa eran</a>
            </li>
            <li>
              <a href="dg4501753s.html#157">Delos Tribos</a>
            </li>
            <li>
              <a href="dg4501753s.html#158">Delas Regiones dichas
              Riones y de sus Insignias</a>
            </li>
            <li>
              <a href="dg4501753s.html#158">Delas Basilicas y que
              cosa eran</a>
            </li>
            <li>
              <a href="dg4501753s.html#158">Del Campidolio
              ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#160">Del Erario ›Aerarium‹
              overamente camera del comun, y que moneda coria en
              Roma en a quel tiempo</a>
            </li>
            <li>
              <a href="dg4501753s.html#161">Del Gregostasio y que
              cosa era ›Grecostasi‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#161">Delo asilio ›Asilo di
              Romolo‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#161">Delos Rostros y que
              cosa eran ›Rostri‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#161">Dela columna dichia
              miliaria ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#162">Del tempo de Carmenta
              ›Tempio di Carmenta‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#162">Dela ›Columna
              Bellica‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#162">Dela ›Columna
              Lactaria‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#163">Del ›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#163">Del sigillo sororio
              ›Tigillum Sororium‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#163">De los campos
              Forasteros ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#163">Della ›Villa
              Publica‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#163">Dela taverna meritoria
              ›Taberna Meritoria‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#164">Del Viario
              ›Vivarium‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#164">Delos Huertos</a>
            </li>
            <li>
              <a href="dg4501753s.html#165">Del ›Velabro‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#165">Delas Carinas
              ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#165">Delos Clivios</a>
            </li>
            <li>
              <a href="dg4501753s.html#166">Delos Prados</a>
            </li>
            <li>
              <a href="dg4501753s.html#166">Delos graneros
              publicos y maganzenes del sal</a>
            </li>
            <li>
              <a href="dg4501753s.html#166">Delas Carceres
              publicas</a>
            </li>
            <li>
              <a href="dg4501753s.html#167">De algunas fiestas y
              luegos que se acostumbravan celebrar en Roma</a>
            </li>
            <li>
              <a href="dg4501753s.html#168">Delos Sepulchros de
              Augusto, Adriano y de Settimio</a>
            </li>
            <li>
              <a href="dg4501753s.html#168">Delos Templos</a>
            </li>
            <li>
              <a href="dg4501753s.html#170">Delos Sacerdotes delas
              Virgines vestales y de los vestimentos vasos, y otros
              instrumentos hechos para el uso delos sacrificios y
              des sus institutores</a>
            </li>
            <li>
              <a href="dg4501753s.html#172">Del Armamentario y que
              cosa era</a>
            </li>
            <li>
              <a href="dg4501753s.html#172">Del exercito Romano de
              tierra y de mar y de sus insignies</a>
            </li>
            <li>
              <a href="dg4501753s.html#173">Delos triumphos y
              aquien se concedian y quie fue el primero
              triumphador, y de quantas maneras eran</a>
            </li>
            <li>
              <a href="dg4501753s.html#173">Delas Coronas y aquien
              se davan</a>
            </li>
            <li>
              <a href="dg4501753s.html#174">Del numero del pueblo
              Romano</a>
            </li>
            <li>
              <a href="dg4501753s.html#174">Delas riquezas del
              populo Romano</a>
            </li>
            <li>
              <a href="dg4501753s.html#175">Dela liberalidad de
              los Romanos antiguos</a>
            </li>
            <li>
              <a href="dg4501753s.html#175">Delos matrimonios
              antiguos y de como se usavan</a>
            </li>
            <li>
              <a href="dg4501753s.html#176">De la buena crianza,
              que davan alos ninos</a>
            </li>
            <li>
              <a href="dg4501753s.html#177">Dela separacion delos
              matrimonios</a>
            </li>
            <li>
              <a href="dg4501753s.html#177">Delas exequias
              antiguas y sus Cerimonias</a>
            </li>
            <li>
              <a href="dg4501753s.html#178">Delas torres</a>
            </li>
            <li>
              <a href="dg4501753s.html#179">Del Tiber</a>
            </li>
            <li>
              <a href="dg4501753s.html#179">Del Palatio Papal
              ›Palazzo Apostolico Vaticano‹ y bel Veder ›Cortile
              del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#180">De Transtiber
              ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4501753s.html#181">Delos Templos antiguos
              fuera de Roma</a>
            </li>
            <li>
              <a href="dg4501753s.html#183">Quantas vezes a sido
              tomado Roma</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501753s.html#184">La significacion
          benediciones y virtud delos Agnusdei</a>
          <ul>
            <li>
              <a href="dg4501753s.html#189">Las virtudes destos
              agnus</a>
            </li>
            <li>
              <a href="dg4501753s.html#190">Viage de Roma a
              Venetia</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501753s.html#195">Tabla delas antiguedades
          de Roma</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
