<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="eman2303630s.html#6">Descrizione delle pitture,
      sculture, ed architetture</a>
      <ul>
        <li>
          <a href="eman2303630s.html#8">Eccellenza</a>
        </li>
        <li>
          <a href="eman2303630s.html#12">A chi legge</a>
        </li>
        <li>
          <a href="eman2303630s.html#14">San Pietro&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#23">Vescovado</a>
        </li>
        <li>
          <a href="eman2303630s.html#24">Corte regio-ducale</a>
        </li>
        <li>
          <a href="eman2303630s.html#24">Teatro nuovo in
          Corte&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#26">Santa Barbera in
          Corte</a>
        </li>
        <li>
          <a href="eman2303630s.html#30">Cavallerizza in
          Corte</a>
        </li>
        <li>
          <a href="eman2303630s.html#31">Appartamenti e giardini
          in Corte</a>
        </li>
        <li>
          <a href="eman2303630s.html#40">Ponte San Giorgio</a>
        </li>
        <li>
          <a href="eman2303630s.html#41">San Giorgio</a>
        </li>
        <li>
          <a href="eman2303630s.html#41">Madonna Annunciata</a>
        </li>
        <li>
          <a href="eman2303630s.html#42">Santa Maria del
          Melone</a>
        </li>
        <li>
          <a href="eman2303630s.html#43">Sant' Agnese&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#46">Dogana</a>
        </li>
        <li>
          <a href="eman2303630s.html#47">Santissima
          Trinità&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#49">Santo Stefano</a>
        </li>
        <li>
          <a href="eman2303630s.html#50">Palazzo della
          Ragione</a>
        </li>
        <li>
          <a href="eman2303630s.html#50">Orologio pubblico</a>
        </li>
        <li>
          <a href="eman2303630s.html#51">Piazza dell'Erbe</a>
        </li>
        <li>
          <a href="eman2303630s.html#52">Sant' Andrea&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#60">Santa Maria Gentile</a>
        </li>
        <li>
          <a href="eman2303630s.html#60">Palazzo Canossa</a>
        </li>
        <li>
          <a href="eman2303630s.html#61">Madonna della
          Vittoria</a>
        </li>
        <li>
          <a href="eman2303630s.html#62">Santi Simone, e
          Giuda</a>
        </li>
        <li>
          <a href="eman2303630s.html#62">San Giovanni
          Evangelista</a>
        </li>
        <li>
          <a href="eman2303630s.html#62">Sant' Ambrogio&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#63">San Francesco&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#67">Porta Mulina</a>
        </li>
        <li>
          <a href="eman2303630s.html#68">Palazzo regio-ducale
          della Favorita</a>
        </li>
        <li>
          <a href="eman2303630s.html#69">San Gervaso</a>
        </li>
        <li>
          <a href="eman2303630s.html#70">Immacolata Concezione,
          chiesa de' Padri Cappuccini &#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#70">Palazzo Cauriani</a>
        </li>
        <li>
          <a href="eman2303630s.html#71">Immacolata Concezione,
          chiesa della Madri Cappuccine&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#71">San Lionardo</a>
        </li>
        <li>
          <a href="eman2303630s.html#72">San Tommaso</a>
        </li>
        <li>
          <a href="eman2303630s.html#73">Immacolata
          Concezione</a>
        </li>
        <li>
          <a href="eman2303630s.html#74">Santa Croce Vecchia</a>
        </li>
        <li>
          <a href="eman2303630s.html#75">Palazzo del Diavolo</a>
        </li>
        <li>
          <a href="eman2303630s.html#75">Casa Zucchi</a>
        </li>
        <li>
          <a href="eman2303630s.html#76">Sant' Orsola</a>
        </li>
        <li>
          <a href="eman2303630s.html#82">Casa Bevilacqua</a>
        </li>
        <li>
          <a href="eman2303630s.html#82">Casa Pelliccelli</a>
        </li>
        <li>
          <a href="eman2303630s.html#82">Casa Porta</a>
        </li>
        <li>
          <a href="eman2303630s.html#82">Ognissanti</a>
        </li>
        <li>
          <a href="eman2303630s.html#83">Certosa</a>
        </li>
        <li>
          <a href="eman2303630s.html#84">Madonna delle Grazie</a>
        </li>
        <li>
          <a href="eman2303630s.html#86">San Marco</a>
        </li>
        <li>
          <a href="eman2303630s.html#87">Scuola Secreta</a>
        </li>
        <li>
          <a href="eman2303630s.html#88">Santa Maria della
          Presentazione&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#88">San Barnaba</a>
        </li>
        <li>
          <a href="eman2303630s.html#90">San Maurizio&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#92">Palazzo Gonzaga</a>
        </li>
        <li>
          <a href="eman2303630s.html#93">Casa Preti</a>
        </li>
        <li>
          <a href="eman2303630s.html#93">Santa Maria dell'
          Umiltà</a>
        </li>
        <li>
          <a href="eman2303630s.html#94">San Cristoforo</a>
        </li>
        <li>
          <a href="eman2303630s.html#95">Palazzo Colloredo</a>
        </li>
        <li>
          <a href="eman2303630s.html#95">Casa di Giulio
          Romano&#160;</a>
        </li>
        <li>
          <a href="eman2303630s.html#96">San Sebastiano</a>
        </li>
        <li>
          <a href="eman2303630s.html#97">Palazzo del Te</a>
        </li>
        <li>
          <a href="eman2303630s.html#111">Sant' Antonio</a>
        </li>
        <li>
          <a href="eman2303630s.html#111">Santa Maria
          Annunziata</a>
        </li>
        <li>
          <a href="eman2303630s.html#112">Santa Paola</a>
        </li>
        <li>
          <a href="eman2303630s.html#114">Santa Apollonia</a>
        </li>
        <li>
          <a href="eman2303630s.html#114">San Vincenzo</a>
        </li>
        <li>
          <a href="eman2303630s.html#115">Sant' Egidio</a>
        </li>
        <li>
          <a href="eman2303630s.html#116">Palazzo Valenti</a>
        </li>
        <li>
          <a href="eman2303630s.html#116">San Giovambatista</a>
        </li>
        <li>
          <a href="eman2303630s.html#117">San Domenico</a>
        </li>
        <li>
          <a href="eman2303630s.html#118">Pescheria, e
          Beccheria</a>
        </li>
        <li>
          <a href="eman2303630s.html#119">San Silvestro</a>
        </li>
        <li>
          <a href="eman2303630s.html#119">Santa Maria della
          Carità</a>
        </li>
        <li>
          <a href="eman2303630s.html#120">San Martino</a>
        </li>
        <li>
          <a href="eman2303630s.html#122">Magazzino delle
          Forniture</a>
        </li>
        <li>
          <a href="eman2303630s.html#126">Palazzo Sordi</a>
        </li>
        <li>
          <a href="eman2303630s.html#127">San Francesco di
          Paola</a>
        </li>
        <li>
          <a href="eman2303630s.html#127">Santissima
          Annunziata</a>
        </li>
        <li>
          <a href="eman2303630s.html#130">Santa Teresa</a>
        </li>
        <li>
          <a href="eman2303630s.html#131">Santo Spirito</a>
        </li>
        <li>
          <a href="eman2303630s.html#132">San Benedetto</a>
        </li>
        <li>
          <a href="eman2303630s.html#136">San Floriano</a>
        </li>
        <li>
          <a href="eman2303630s.html#138">Indice alfabetico</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
