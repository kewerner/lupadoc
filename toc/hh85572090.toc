<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="hh85572090s.html#6">Vita Beati P. Ignatii Loiolae
      Societatis Iesu fundatoris</a>
    </li>
    <li>
      <a href="hh85572090s.html#10">[Tavole]</a>
      <ul>
        <li>
          <a href="hh85572090s.html#10">1. Mater Ignatium paritura
          pro sua in natalem Domini pietate, deferri se iubet in
          Stabulum; eumque post septem filios postremum in stabulo
          parit, anno salutis 1491 ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#12">2. Militiam sequutus
          Ignatius, ictu mumlis globi crure perfructo à defensione
          arcis Pampelonae semianimis excutitur ut seculari militia
          relicta, ad divinam se transserat ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#14">3. E cruris vulnere
          laboranti, mortiquem iam proximo. Sanctus Petrus in sui
          peruigilij nocte per quietem apparet, ac sanitatem
          restituit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#16">4. In lecto decumbens, dum
          ad recreandum animum Christi domini vitam et exempla
          Sanctorem evoluit, divinarum virtutum imitatione
          exardescens, ad Deum convertitur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#18">5. Dum se invocata divinae
          Matris ope Deo dicat noctu vigilantem Beatiß. Virgo
          eiusam in gremio puer Iesus illustri in specie aliquandiu
          visi suavissime recreant ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#20">6. Dum se Deo iterum fusus
          in preces ferventissime offert, magno repente terrae
          &#160;motu concutitur domus ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#22">7. E domo et cognatione
          sua exit, retaque ad Virginis templum famulis redire
          iußis, in Montem Serratum contendit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#24">8. In itinere Maurum de
          virginitate Dei matris impure detrahentem dubitat an
          ferro ulciscatur, permißisque, equo habensis, ex eo quod
          iumentum ab antecedentis Mauri vestigijs divertit,
          divinitus interpretatur huiusmodi ultionem Deo cordi non
          esse ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#26">9. In eodem itinere,
          Beatae Virginis amore, atque imitatione succensus voto se
          illi castitatis obstringit; eiusdemque casitatis, omni
          extincto impuritatis sensu, perpetuum donum accipit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#28">10. Vestibus pretiosis
          exutus, ac pauperi donatis, sacco ac fune praecinctus
          Christi domini paupertatem amplectitur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#30">11. In Aede Montis Serrati
          tamquam novus Christi eques noctem unain ante aram
          Virginis excubat, humanaeque arma militiae e tholo
          suspendit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#32">12. In solitudinem
          profectus, juvente supra orantis caput varijs serpentum
          spectris daemone intrepidus atque inconnivens in precibus
          perseverat ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#34">13. Minoressae inter
          pauperum turbam vivit, xenodochio inservit, multos e
          vitiorum coeno extrahit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#36">14. Horas quotidie
          septenas genibus nixus in oratione persistit. Quotidie
          etiam ter sese quam acerrime flagellis caedit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#38">15. Quotidie acqua et pane
          contentus severe ieiunat, imo ad evincendos scrupulos,
          quorum angustijs a daemone ad praecipitium usque
          instigabatur, septem dies sine ullo cibo aut potu, nullo
          virium defectu transfigit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#40">16. Dum apud templi
          dominicani limina Beatae Virgini laudes recitat, miram de
          sanctissimo Trinitatis mysterio visionem, et lumina
          accipit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#42">17. Codem in templo dum
          missae sacrificio interest in sacrosancta hostia Christum
          Dominum oculis intuetur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#44">18. Saepe Christus
          dominus, eiusque Mater ei ad longum temporis spatium,
          contemplandos fruendosque se exhibent; magnamque eius
          animo inspirant in christiana fide, atque in suscepta
          pietate constantiam ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#46">19. In mentis raptu septem
          ipsos dies persistentem humaturi iam erant, nisi e tenuis
          sima cordis palpitatione vitae indicium deprehendissent;
          a quo tandem raptu veluti a dulci somno, nomen IESU
          suaviter ingeminans solvitur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#48">20. Magnam divinarum
          humanarumque rerum cognitionem divinitus infusam accipit
          ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#50">21. Libellum exercitiorum
          spiritualium singulari afflatu Dei, haustaque e caelo
          luce conscribit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#52">22. Navigaturus in Italiam
          sola DEI fiducia pro viatico minitus emendicatam pecuniam
          in littus abijcit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#54">23. Prope Patavium viae
          noctis et temporum periculis anxium, apparens in aere
          Christus Dominus consolatur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#56">24. Venetijs dum noctu sub
          Santi Marci porticibus iacet, Marcus Antonius Trevisanus
          nobilis Senator hisce verbis excitatus Tu delicate quidem
          cubas, sed famulus interim meus humi sub dio est, hominem
          sedulo quaerit ac suscipit perhumaniter ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#58">25. Nautae suis infensum
          vitijs in desertam Insulam exposituri; subito vento
          repelluntur, ac inuiti licet ad Cyprum uehunt ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#60">26. Hierosolymam naviganti
          saepe Christus dominus videndum se praebet, ac laborum
          difficultates lenit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#62">27. Sacra Palestinae loca
          religiosissime perlustrat, et ad montem Olivetum Christi
          Domini ascendentis vestigia diligentius contemplaturus
          cum periculo recurrit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#64">28. Ex Oliveto nevertens
          ab Armenio custode voce, ac fuste terretur; dumque ne
          solitarius ea loca peragraret, ferociter in hospitium
          trabitur, inter ea convicia, et contumelias Christum
          aspiat praeeuntem ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#66">29. In Hispaniam rediburum
          a navi Veneta optime instructa Navarchus exdudit,
          respondetque sanctitatem viri extollentibus. Si sanctus
          est quid navim petit, ac mare sicco vestigio non calcat.
          Quare in aliam relictam ac laceram admittutur: Sed haec
          oncolumis in Hispaniam appelit Veneta quamuis valida
          naufragiu facit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#68">31. [sic] Ab Hispanorum
          praesidio pro exploratore habitus nudus per media castra
          raptatur. quam ignominiam alacriter ferenti, species
          oblata est Christi domini ad Herodem a Pilato transmissi,
          atque illusi ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#70">32. Barcinone ut se ad
          animorum salutem instruat prima Grammaticae elementa
          annos tres, et triginta natus addiscit; furente ac
          rumpente se Daemone, qui importunis rerum caelestium
          gaudijs avocare eius animum frustra conatur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#72">33. Ob res titutam in
          virginum coenobio disciplinam ab impuris hominibus id
          indigne ferentibus soevissime plagis afficitur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#74">34. Hominem ad suspendium
          desperatione coactum precibus ad sensus eatenus revocat,
          quoad animum a scelere, confessionis sacramento purget
          ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#76">35. Noctu in preces,
          quatuor ferme cubitis elatus a terra, collucente mirum in
          modum facie, identinem, crebra inter suspiria inclamat. O
          DOMINE SI TE HOMINES NOSSENT! ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#78">36. Compluti primum;
          postea salmanticae, calumnias pro Christo, et carcerem
          passus, ex ipso etiam carcere animas lucratur, magnoque
          spiritus fervore succensus. Non tot, inquit, in hac urbe
          sunt compedes, quin plures ego Christi caula percupiam
          ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#80">37. Quidam ei infensus
          imprecans sibi aliquando flammas, quibus combustus
          expiraret, nisi Ignatius ignem se iudice mereretur, eodem
          die incendio domus suae deflagrantis absumptus est
          &#160;▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#82">38. Dum Lutetiae tamquam
          scholasticorum seductor virgis un publica animadversione
          caedendus inducitur, cognita hominis innocentia Rector ad
          eius pedes accidit, sanctum palam appellat, infamiaeque
          apparatum in gloriae scenam vertit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#84">39. Iuvenes ex Academia
          Parisiensi novem eligit, ac socios consilij sui destinat
          ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#86">40. Sicarius stricto illum
          petens, audita repente voce QUO TENDIS INFELIX? territus
          a facinore desistit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#88">41. In aede suburbana
          Beata Virginis ipse, ac socij certo se voto obstringunt
          divinam ubique gloriam, animarumque salutem in
          Hiesosolymitana praesertim expeditione procurandi, ac
          palmam inde martyrij sedulo conquirendi, quod votum
          ibidem quotannis renovant ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#90">42. Ab impuris amoribus
          quempiam revocaturus, in summa hyeme, gelido se in stagno
          collo tenus immergit, ibique praecereuntem
          conspicatus,voce, aspectuque terret, et convertit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#92">43. In Hispaniam
          valetudinis causa redeuntem excitata sanctitatis viri
          fama armati primum homines mox clerus omnis agmine
          composito demum populus fere universus ingenti
          gratulatione suscipiunt ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#94">44. Aeger Hispaniam
          repetens, animis ad virtutem excolendis strenue in patria
          laborat; eiusque in campo concionantis vox (quod populi
          frequentiam templa non caperent) ad trecentos passus
          auditur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#96">45. Comitali morbo
          laborantem sublatis in coelum oculis, ac precibus
          extemplo sanat ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#98">46. Multos saepe
          Energumenos liberat crucis signo ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#100">47. Foeminam phtysi ad
          interitum properantem sanitati restituit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#102">48. A rido, emortuoque
          foeminae bracchio Ignatij lintea dum lavat, vita statim,
          ac motus redit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#104">49. In Italiam reversus
          Venetijs, Socios e Gallia excipit, unaque cum illlis
          sacerdotio initiatur, tam coelesti voluptate perfuso
          Episcopo, ut non nisi divinum quid in novis sacerdotibus
          praesagiret ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#106">50. Ad Simonem Rodericum
          socium morti proximum octodecim milliarium itinere, febri
          ipse laborans propere contendit, eumque amplexu sanat
          ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#108">51. E socijs unus, cum
          tentatione iam victus ad solitudinem pergeret, obiecto
          sibi armati, ferumque intentantis equitis spedro, ad
          Ignatium remittitur, qui re tota per prophetiae spiritum
          cognita, redeuntem, illis Domini verbis blande excipit.
          Modicae fidei quare dubitasti! ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#110">52. Solitario homini eius
          vitam tacite despicienti apparet Dominus, ac viri
          sanctitatem aperit, docetque illum ad salutem plurimorum
          natum esse ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#112">53. Non longe ab Urbe
          templum desertum ingresso inter orandum se Deus Pater
          ostendens illum filio suo crucem gestanti socium
          attribuit; filius item placidissima illa verba
          pronuntians EGO VOBIS ROMAE PROPITIUS ERO illum recipit
          in socium Unde Ignatio lux oborta societatis IESU
          nominandae ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#114">54. Primum sacrum Romae
          ad Domini praesepe facit, cum se ad id post susceptum
          sacerdotium duodeviginti mensium studio praeparasset
          ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#116">55. In Casinati monte ut
          Sanctus Benedictus Germani, sic ille animan Hozij ferri
          in coelum videt, ac postea in ipso missae, cui astabat
          ingressu ad es verba et omnibus sanctis in illustru
          sanctorum choro agnoscit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#118">56. Paulus III Pontifex
          Maximus Societatis Iesu institutum ab Ignatio oblatum
          postquam legisset, DIGITUS inquit, DEI EST HIC.
          Societatemque confirmat anno salutis 1540 ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#120">57. Franciscum Xaverium,
          qui Indiarum Apostolus dictus est, divino instinctus
          afflatu in Indias mittit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#122">58. Generalis quamquam
          invitus, diuque repugnans, eligitur; atque in aede, quae
          extra urbem visitur, Sancti Pauli quarto solemni voto se,
          ac Societatem suam Romano Pontifici obstringit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#124">59. Ex Iris Beatis
          Xaverij, ad Ignatium ex India scriptis GRATIA &#160;ET
          CARITAS Christi Domini et Mi pater in Christi visceribo
          unice Te ego pater animae meae, summeque mihi venerande
          prositis humi genibus (sic nonime hanc tibi eptam scribo)
          suppliciter oro, ut mihi a Deo impetres, ut dum vivam
          sanctissimae voluntatis suae mihi det et plane
          agnoscendae, et omnio exequendae facultatem. Vale Tuus
          minimus filius, longissimeque exulans ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#126">60. Sacramentorum,
          piarumque, concionum usum Romae renovat, ac rationem
          pueris tradendi doctrinae chrisitanae rudimenta Romanis
          in templis, ac plateis inducit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#128">61. Dum Romae,
          insimulatur, quod multorum scelerum, diversis in Urbibus
          damnatus fuisset, divina providentia Romam simul
          confluunt omnes, qui illum alibi absolverant, ijsdem eius
          innocentiae antea iudices; nunc testes ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#130">62. Ad aedem Sancti Petri
          in monte aureo contendens, rem sacram pro Salute Codurij
          facturus, in medio Sixti Ponte resisit continuo,
          coelumque tantisper intuitus, ac divinitus de eius morte
          admonitus. Redeamus, inquit, socius mortuus est ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#132">63. Publica Romae
          pietatis opera instituit: coenobia mulierum male
          nuptarum: virginum Sanctae Catherinae ad funarios:
          puellarum Sancti quatuor coronatorum: puerorum item qui
          orbi parentibus per Urbem vagi mendicant. Cathecumenorum:
          aliorumque collegia magna oium admiratione, fructuque
          ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#134">64. Ignatij in
          Septemtrionis res apprime intenti studio, ac precibus
          Iulius III. Pontfex Maximus Collegium Germanicae
          inventutis non minori Ecclesiae Romanae ornamento, quam
          Germaniae praesidio Romae condit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#136">65. Societatis Iesu
          constitutiones frequentibus sanctissimae Trinitatis
          apparitionibus, atque illus trationibus. Beatissima item
          virgine saepe visa, atque illas approbante conscribit
          ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#138">66. Obstinatum Iudaeum
          tribus hisce verbis convertit. MANE NOBISCUM ISAAC ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#140">67. Saepe noctu inter
          orandum, aut quiescendum à Daemonibus verberatur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#142">68. Caeli aspectu
          mirifice captus vim lacrymarum profundere, atque
          exclamare solebat, HEV QUAM SORDET TELLUS, CUM COELUM
          ASPICIO? cumque prae lacrymis oculos perderet, imperium
          in illas a Deo impetrat; novoque dono donum lacrymarum
          moderatur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#144">69. Sacram hostiam Deo
          dum offert, supra missam celebrantis caput, ingens
          emicare flamma conspicitur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#146">70. Allatum e patria
          fasciculum litterarum redditumque inter orandum, in
          proximum ignem abijcit: curasque saeculi importune
          interpellantes una cum litteris concremandas dedit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#148">71. Prophetiae spiritu
          videt arcana animorum, ac saepe futura praesentit; inter
          alia aegrotanti praedicit, aspectu illum Virginis
          fruiturum, dictique consentit eventus ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#150">72. Daemonem serpentis
          facie collucentis exhibentem se detegit, ac per
          contemptum baculo abigit ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#152">73. Saepe Beatus
          Philippus Nerius illius faciem insigni luce radiantem
          videt, illustri, ut ipse dicebat, indicio sanctitatis
          ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#154">74. Alexandri Petronij
          morbo laborantis cubiculum adventu suo, magno repente
          fulgore collustrat: aegrum colloquio sanat; ac saepe
          alios invisens itidem sanat ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#156">75. Patres Collegij
          Lauretani, cum Lemurum spectris infestarentur, ad eius
          preces per litteras confugiunt; eiusque accepto responso,
          ac publice perlecto, illico Daemonum terroribus
          liberantur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#158">76. Cuidam e Societate
          Romam venire Colonia meditanti, ut illius aspectu
          frueretur, ultro ipse apparet, seque Coloniae videndum
          socio praebet ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#160">77. Romae sanctissime
          moritur, eodemque puncto temporis beata eius anima,
          ingenti splendore conspicua, Bononiae a nobili, sanctaque
          foemina ferri in coelum aspicitur ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#162">78. Puella strumis iam
          diu laborans, cum ad manus iacentis in pheretro
          deosculandas accedere prae turba non posset, frustulo
          vestis arrepto, et ad collum alligato, illico sanatur:
          folia passum, ad flores e pheretro subducti, aegrorum
          multis saluti sunt ▣</a>
        </li>
        <li>
          <a href="hh85572090s.html#164">79. Dum eius
          transferuntur sacra ossa, lucentes stellae in loculo
          visae, ac caeles inibi concentus auditus ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
