<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghdav350129101s.html#10">Cours d'Architecture qui
      comprend les Ordres de Vignole [...] Premiere partie</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#12">A Monseigneur le
          Marquis de Louvois [...] [dedica dell'autore] ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#16">Preface pour servir
          d'introduction à l'architecture</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#29">La vie de Jacques
      Barozzio de Vignole architecte et peintre</a>
    </li>
    <li>
      <a href="ghdav350129101s.html#40">Table des traitez et
      figures ou planches de ce livre</a>
    </li>
    <li>
      <a href="ghdav350129101s.html#48">Table des matieres
      contenües en ce Livre</a>
    </li>
    <li>
      <a href="ghdav350129101s.html#86">Preface de Vignole</a>
    </li>
    <li>
      <a href="ghdav350129101s.html#91">Planche. Figures des
      principes de la geometrie expliquées dans les definitions
      ▣</a>
    </li>
    <li>
      <a href="ghdav350129101s.html#94">Des Moulures, et de la
      maniere de les bien profiler</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#96">Planche A. Moulures
          au trait. Moulures umbrees ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#99">Des ornemens des
          Moulures</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#100">Planche B. Moulures
          couronnées et ornées ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#103">Du choix des
          profils</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#104">Planche C. Profils
          ioniques antiques et modernes ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#106">Des cinq Ordres en
      general</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#108">Planche I. Les cinq
          ordres de l'architecture ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#111">De l'Ordre
          Toscan</a>
          <ul>
            <li>
              <a href="ghdav350129101s.html#112">Planche II.
              Entre-colonne toscan ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#114">Les cinq
              Manieres d'espacer les Colonnes selon Vitruve ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#115">Portique Toscan
              sans Piedestal</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#116">Planche III.
              Portique Toscan sans Piedestal ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#117">Portique Toscan
              avec Piedestal</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#118">Planche IV.
              Portique Toscan avec Piedestal ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#119">Piedestal et
              Base Toscans</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#120">Planche V.
              Piedestal et Base Toscans ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#121">Chapiteau et
              Entablement Toscans</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#122">Planche VI.
              Chapiteau et Entablement Toscans ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghdav350129101s.html#123">De l'Ordre
          Dorique</a>
          <ul>
            <li>
              <a href="ghdav350129101s.html#124">Planche VII.
              Entre-colonement dorique ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#129">Portique
              Dorique sans Piedestal</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#130">Planche VIII.
              Portique Dorique sans Piedestal ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#131">Portique
              Dorique avec Piedestal</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#132">Planche IX.
              Portique Dorique avec Piedestal ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#133">Piedestal et
              Base Dorique</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#134">Planche X.
              Piedestal et Base Dorique ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#135">Entablement
              Dorique</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#136">Planche XI.
              Chapiteau et entablement doriques ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#137">Entablement
              Dorique</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#138">Planche XII.
              Chapiteau et entablement doriques ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#139">Plafonds des
              Corniches Doriques</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#140">Planche XIII et
              XIV. Plafonds des Corniches Doriques ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghdav350129101s.html#141">De l'Ordre
          Ionique</a>
          <ul>
            <li>
              <a href="ghdav350129101s.html#142">Planche XV.
              Entre colonnement ionique ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#145">Portique
              Ionique sans Piedestal</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#146">Planche XVI.
              Portique Ionique sans Piedestal ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#147">Portique
              Ionique avec Piedestal</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#148">Planche XVII.
              Portique Ionique avec Piedestal ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#149">Piedestal, Base
              et Imposte Ioniques</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#150">Planche XVIII.
              Piedestal et Base Ionique ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#151">Entablement
              Ionique</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#152">Planche XIX.
              Chapiteau et entablement ionques ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#153">Chapiteau
              Ionique</a>
              <ul>
                <li>
                  <a href="ghdav350129101s.html#154">Planche XX.
                  Chapiteau Ionique ▣</a>
                </li>
                <li>
                  <a href="ghdav350129101s.html#155">Maniere de
                  tracer la Volute Ionique</a>
                </li>
                <li>
                  <a href="ghdav350129101s.html#156">Planche
                  XXI. Maniere de tracer la Volute Ionique ▣</a>
                </li>
                <li>
                  <a href="ghdav350129101s.html#157">Autre
                  maniere de tracer la Volute Ionique</a>
                </li>
                <li>
                  <a href="ghdav350129101s.html#158">Planche
                  XXII. Autre maniere de tracer la Volute Ionique
                  ▣</a>
                </li>
                <li>
                  <a href="ghdav350129101s.html#159">Description
                  de la Volute de Goldman</a>
                </li>
                <li>
                  <a href="ghdav350129101s.html#160">Planche
                  XXIII. Description de la Volute de Goldman ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghdav350129101s.html#161">De l'Ordre
          Corinthien</a>
          <ul>
            <li>
              <a href="ghdav350129101s.html#162">Planche XXIV.
              Entre-colonne corinthien ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#165">Portique
              Corinthien sans Piedestal</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#166">Planche XXV.
              Portique Corinthien sans Piedestal ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#167">Portique
              Corinthien avec Piedestal</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#168">Planche XXVI.
              Portique Corinthien avec Piedestal ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#169">Piedestal et
              Base Corinthienne</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#170">Planche XXVII.
              Piedestal et Base Corinthienne ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#171">Plan et Profil
              du Chapiteau Corinthien</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#172">Planche XXVIII.
              Plan et Profil du Chapiteau Corinthien ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#175">Chapiteau et
              Entablement Corinthiens</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#176">Planche XXIX.
              Chapiteau et Entablement Corinthiens ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghdav350129101s.html#177">De l'Ordre
          Composite</a>
          <ul>
            <li>
              <a href="ghdav350129101s.html#178">Planche XXX.
              Entre-colonne composite ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#181">Portique
              Composite sans Piedestal</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#182">Planche XXXI.
              Portique Composite sans Piedestal ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#183">Portique
              Composite avec Piedestal</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#184">PlancheXXXII.
              Portique Composite avec Piedestal ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#185">Piedestal et
              Base Composite</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#186">Planche XXIII.
              Piedestal et Base Composites ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#187">Plan et Profil
              du Chapiteau Composite</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#188">Planche XXXIV.
              Plan et Profil du Chapiteau Composite ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#189">Chapiteau et
              Entablement Composites</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#190">Planche XXXV.
              Chapiteau et Entablement Composites ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#193">Plafonds des
              Corniches Corinthienne et Composite</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#194">Planche XXXVI.
              Plafonds des Corniches Corinthienne et Composite
              ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#197">Impostes
              Corinthienne et Composite</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#198">Planche XXXVII.
              Impostes Corinthienne et Composite ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghdav350129101s.html#201">Chapiteaux Antiques
          et Base Attique</a>
          <ul>
            <li>
              <a href="ghdav350129101s.html#202">Planche
              XXXVIII. Chapiteaux Antiques et Base Attique ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#204">Base
              Attique</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#205">Maniere de
              diminuer les Colonnes</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#205">Autre maniere
              de diminuer les Colonnes</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#206">Planche XXXIX.
              Maniere de diminuer les Colonnes ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#209">Description de
              la premiere Conchoide des Anciens</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#210">Planche XL.
              Instrument pour tracer la premiére conchoide ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#211">Maniere de
              torser les Colonnes</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#212">Planche XLI.
              Maniere de torser les Colonnes ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#215">Des Colones
              [sic] torses ornées</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#216">Planche XLII.
              De l'autel de Saint Pierre ›San Pietro in Vaticano:
              Baldacchino di San Pietro‹ ▣. De l'autel du Val de
              Grace</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#217">Entablement de
              Couronnement</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#218">Planche XLIII.
              Entablement de Couronnement ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#219">Des Portes en
      general</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#221">Planche XLIII B.
          Diverses especes de portes ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#227">Planche XLIV A.
          Portes, montans, et frises de serrurerie ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#235">Porte Rustique
          d'Ordre Toscan</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#236">Planche XLIV. Porte
          Rustique d'Ordre Toscan ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#237">Porte dessinée pour
          l'Illustrissime et Reverendissime Cardinal Farnése [sic]
          pour servir d'Entrée principale au Palais de la
          Chancellerie ›Palazzo della Cancelleria‹</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#238">Planche XLV. Porte
          por le Palais de la Chancellerie ›Palazzo della
          Cancelleria‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#239">Porte du Bâtiment
          de l'Illustrissime et Reverendissime Cardinal Farnése
          [sic] à Caprarole</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#240">Planche XLVI. Porte
          du chateau du Caprarole ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#241">Porte de l'Eglise
          de Saint Laurent in Damaso ›San Lorenzo in Damaso‹
          ouvrage de Vignole, quoique le Palais soit bâti par
          d'autres Architectes</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#242">Planche XLVII.
          Porte de l'Eglise de Saint Laurent in Damaso ›San Lorenzo
          in Damaso‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#243">Cette Porte est au
          Salon du Palais Farnése [sic] ›Palazzo Farnese‹. Elle est
          de l'invention de Vignole qui n'a pas peu contribué à
          rendre ce Palais magnifique par plusieurs ornemens de
          Portes, de Fenestres et de Manteaux de Cheminées qu'il y
          a fait</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#244">Planche XLVIII.
          Porte du Salon du Palais Farnese ›Palazzo Farnese‹ ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#245">Des Fenestres en
      general</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#246">Planche XLIX. Yeux
          de boeuf ou petites lucarnes ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#246">Planche XLIX.
          Fenestre cintrée (fenestre en platte-bande) ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#255">Fenestre du Palais
          Sachetti [sic] ›Palazzo Sacchetti‹</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#256">Planche L. Fenestre
          du Palais Sachetti [sic] ›Palazzo Sacchetti‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#257">Fenestre de
          Vignole</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#258">Planche LI.
          Fenestre de Vignole ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#259">Des Niches en
      general</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#260">Planche LII.
          Diverses especes de niches ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#262">Niches rustiques
          avec bossages a refends ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#267">Niches en Retables
          d'Autels</a>
          <ul>
            <li>
              <a href="ghdav350129101s.html#268">Planche LIII.
              Niches en retables d'autels ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghdav350129101s.html#269">Niche du Salon de
          Clagni [sic]</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#270">Planche LIV. Niche
          du Salon du Chasteau de Clagny ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#271">Des Cheminées en
      general</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#272">Planche LV. Tuyaux
          de cheminées devoyez ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#277">Cheminée du Palais
          Farnése [sic] ›Palazzo Farnese‹</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#278">Planche LVI.
          Cheminée du Palais Farnése [sic] ›Palazzo Farnese‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#279">Grandes Cheminées
          pour Salles et Galleries</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#280">Planche LVII.
          Grandes Cheminées pour Salles et Galleries ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#281">Moyennes Cheminées
          pour les Chambres</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#282">Planche LVIII.
          Moyennes cheminées pour les chambres ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#283">Petites Cheminées
          pour les Cabinets</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#284">Planche LIX.
          Petites Cheminées pour les Cabinets ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#285">De la distribution des
      Plans et de la decoration des Façades</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#287">Plan des
          Offices</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#289">Planche LX. Plan de
          l'étage souterrain ou des Offices ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#293">Plan du Rez
          de-Chaussée</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#295">Planche LXI. Plan
          du Res de Chaussée ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#301">Plan du premier
          Etage</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#303">Planche LXII. Plan
          du premier ou bel Etage ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#307">Elevation du grand
          Corps de Logis</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#309">Planche LXIII A.
          Elevation de l'entrée du grand corps de logis, et coupe
          d'une des ailes ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#313">Elevation d'une des
          Aisles et coupe du grand Corps de Logis</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#315">Planche LXIII B.
          Elevation d'une des Aisles et coupe du grand Corps de
          Logis ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#319">Explication de la
          Charpenterie</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#321">Planche LXIV A.
          Diverses especes de combles avec leurs assemblages et
          couvertures ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#327">Planche LXIV B.
          Diverses sortes d'assemblages de charpenterie ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#331">De la Decoration des
      Jardins</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#333">Planche LXV A.
          Parterre de Broderie avec massif torunant de Gazon ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#339">Planche LXV B.
          Parterre a l'angloise, et parterre de broderie ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#347">Planche LXV BB.
          Parterre de pieces coupées, et parterre de gazon comparti
          ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#354">De la Matiere et de la
      Construction des Edifices</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#355">Des Pierres propres
          à bastir</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#362">Des Marbres et de
          leurs differentes couleurs</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#366">De la Liaison des
          Pierres</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#369">De l'usage du Fer
          dans les bastimens</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#371">Planche LXV C.
          Diverses pieces de menus ouvrages de serruerie ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#377">Planche LXV D.
          Rampes, apuis, et balcons de serruerie ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#381">Des Bois qu'on
          employe dans les bastimens</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#384">De la Couverture des
      combles</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#385">Du Plomb</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#386">Du Cuivre</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#386">De l'Ardoise</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#387">De la Tuile</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#388">Des Vitres</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#389">De la Peinture ou
          Impression dans les bastimens</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#392">De la Construction des
      edifices</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#392">De la maniere de
          planter les bastimens</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#394">Des Fondemens des
          edifices</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#397">De la coupe des
          Pierres</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#399">Planche LXVI A.
          Outils, panneaux, et premieres pieces de la coupe des
          pierres ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#407">Planche LXVI B.
          Voutes, trompes, escaliers et autres pieces de trait
          ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#414">Remarques sur quelques
      bastimens de Vignole</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#415">De l'Eglise de
          Saint André à Ponte-Mole ›Oratorio di Sant'Andrea (via
          Flaminia)‹</a>
          <ul>
            <li>
              <a href="ghdav350129101s.html#417">Dedans de
              l'Eglise de Saint André ›Oratorio di Sant'Andrea (via
              Flaminia)‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghdav350129101s.html#416">Planche LXVII.
          Elevation de l'Eglise de Saint André ›Oratorio di
          Sant'Andrea (via Flaminia)‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#418">Planche LXVIII.
          Profil de l'Eglise de Saint André ›Oratorio di
          Sant'Andrea (via Flaminia)‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#419">De l'Eglise du
          Grand Jesus à Rome ›Santissimo Nome di Gesù‹</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#421">Planche LXIX. Plan
          de l'eglise du Saint Nom de Iesus a Rome ›Santissimo nome
          di Gesù‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#427">Planche LXX. Coupe
          et profil sur la logueur de l'Eglise du grand Iesus a
          Roma ›Santissimo Nome di Gesù‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#431">De la Vigne du Pape
          Jules ›Villa Giulia‹, à Rome</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#433">Planche LXXI.
          Entrée de la Vigna du Pape Iules III au fauxbourg du
          peuple, a Rome ›Villa Giulia‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#437">Du Chasteau de
          Caprarole, dans le Patrimoine de Saint Pierre</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#439">Planche LXXII. Plan
          au rez de cahussée du chasteau de Caprarole ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#445">Planche LXXIII.
          Elevation et coupe scenografiques du chasteau de
          Caprarole ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#448">Elevation du
          Chasteau de Caprarole</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#450">Preface sur la vie et
      sur les ouvrages de Michel-Ange</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#457">Porte au bout de la
          voye Flamine, à present le faux-bourg du Peuple, à la
          teste du Cours à Rome ›Porta del Popolo‹</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#458">Planche LXXIV.
          Porte du Peuple ›Porta del Popolo‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#459">Porte Pie ›Porta
          Pia‹ appellée autrefois Viminale, au bout de la voye
          Nomentane ›Via Nomentana‹, à la teste de Strada Julia
          ›Via XX Settembre‹, qui conduit à Monte-cavallo
          ›Quirinale‹, sur le Quirinal à Rome</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#460">Planche LXXV. Porte
          Pie ›Porta Pia‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#461">Porte de la Vigne
          du Patriarche Grimani ›Vigna Grimani‹ dans la Strada Pia
          ›Via XX Settembre‹ à Rome</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#462">Planche LXXVI.
          Porte de la Vigne du Patriarche Grimani ›Vigna Grimani‹
          ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#463">Profils de la Porte
          du Peuple ›Porta del Popolo‹, de la Porte Pie ›Porta Pia‹
          et de celle de la Vigne Grimani ›Vigna Grimani‹ à
          Rome</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#464">Planche LXXVII.
          Profils des portes precedentes ›Porta del Popolo‹ ›Porta
          Pia‹ ›Vigna Grimani‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#465">Porte de la Vigne
          du Cardinal Sermonette ›Vigna Sermoneta‹ qui commence
          depuis le bas du Mont Quirinal ›Quirinale‹ et s'étend
          jusques au sommet de la voye Pie ›Via XX Settembre‹
          nommée anciennement ›Alta Semita‹ à Rome</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#466">Planche LXXVIII.
          Porte de la Vigne de Cardinal Sermonete ›Vigna Sermoneta‹
          ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#467">Porte de la Vigne
          du Duc Sforce ›Palazzo Barberini‹ au faubourg du
          Peuple</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#468">Planche LXXIX.
          Porte di Iardin de l'Illustre Seigneur Duc Sforce
          ›Palazzo Barberini‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#469">Profils de la porte
          de la Vigne Sermonette ›Vigna Sermoneta‹, et de celle du
          Jardin du Duc Sforce ›Palazzo Barberini‹</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#470">Planche LXXX.
          Profil de la Porta Sermonette ›Vigna Sermoneta‹ ▣. Profil
          de la Porta Sforce ›Palazzo Barberini‹ ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#471">Le Capitole
          ›Campidoglio‹ moderne de Rome basti sur les ruines de
          l'ancien, est aujourd'huy nommé le Palais des
          Conservateurs ›Palazzo dei Conservatori‹ du Peuple
          Romain</a>
          <ul>
            <li>
              <a href="ghdav350129101s.html#473">Planche LXXXI.
              Plan d'une partie du Capitole moderne de Rome
              ›Campidoglio‹ ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#479">Planche LXXXII.
              Profil et elevation d'une des aisles du Capitole
              ›Campidoglio‹ ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#483">Porte
              principale di Palais des Conservateurs ›Palazzo dei
              Conservatori‹ du Peuple Romain au Capitole
              ›Campidoglio‹</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#484">Planche
              LXXXIII. Principale porte di Capitole ›Palazzo dei
              Conservatori‹ ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#485">Porte d'une des
              Chambres de Communauté d'Artisans sous le Portique du
              Capitole ›Campidoglio‹</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#486">Planche LXXXIV.
              Porte sous le portique du Capitole ›Campidoglio‹
              ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#487">Fenestre à
              balcon au premier étage du Palais des Conservateurs
              ›Palazzo dei Conservatori‹ au Capitole
              ›Campidoglio‹</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#488">Planche LXXXV.
              Fenestre du Capitole ›Palazzo dei Conservatori‹ ▣</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#489">Elevation de
              front et de costé d'un des chapiteaux ioniques du
              Portique du Capitole ›Campidoglio‹</a>
            </li>
            <li>
              <a href="ghdav350129101s.html#490">Planche LXXXVI.
              Chapiteau ionique de Michel-Ange ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#491">Bases et Chapiteaux
      corinthiens de feüilles d'acanthe et d'olivier</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#492">Planche LXXXVII.
          Bases et Chapiteaux corinthiens ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#493">Bases et Chapiteaux
      composites de feüilles de persil et de laurier</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#494">Planche LXXXVIII.
          Bases et Chapiteaux composites ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#495">Bases composées et
      Chapiteaux symboliques</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#496">Planche LXXXIX.
          Bases composées et Chapiteaux symboliques ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#497">Cannelures rudentées et
      ornées</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#498">Planche XC.
          Cannelures rudentées et ornées ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#499">Colonne avec diverses
      bandes</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#500">Planche XCI.
          Colonne avec diverses bandes ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#501">Dispositions de
      Colonnes et de Pilastres</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#502">Planche XCII.
          Dispositions de Colonnes et de Pilastres ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#503">Diverses especes de
      Colonnes extraordinaires et symboliques</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#505">Planche XCIII.
          Diverses especes de colonnes extraordinaries et
          symboliques ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#513">Diverses especes de
      Piedestaux extraordinaires</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#515">Planche XCIV.
          Diverses especes de Piedestaux extraordinaires ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#523">Divers Balustres
      d'apui</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#524">Planche XCIV.
          Divers Balustres d'apui ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#529">Divers Entrelas
      d'apui</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#530">Planche XCVI
          (sic!). Divers entrelas d'apui ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#531">Diverses especes de
      Bossages</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#532">Planche XCVII.
          Diverses especes de Bossages &#160;▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#533">Entablemens pour les
      Façades et Corniches pour les Apartemens</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#535">Planche XCVIII.
          Entablemens pour les Façades et Corniches pour les
          Apartemens ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghdav350129101s.html#544">Des Compartimens en
      general</a>
      <ul>
        <li>
          <a href="ghdav350129101s.html#545">Des Compartimens
          des Murs de face</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#547">Des Compartimens
          des Lambris</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#549">Planche XCIX.
          Compartimens de lambris de revestement de marbre et de
          menuiserie ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#553">Des Compartimens,
          Assemblages, et Profils de menuiserie</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#555">Planche C.
          Assemblages et profils pour les compartimens de
          menuiserie ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#559">Des Compartimens
          des Voutes et Plafonds</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#561">Planche CI.
          Compartimens pour les arcs doubleau des voutes, et
          pendentifs des coupes ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#569">Des Compartimens du
          Pavé</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#570">Planche CII. Pavez
          de grais brique et carreau ▣</a>
        </li>
        <li>
          <a href="ghdav350129101s.html#575">Planche CIII.
          Diverses especes de compartimens de pavez de marbres
          ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
