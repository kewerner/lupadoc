<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="gf2001210s.html#6">De architectura libri dece
      traducti de latino in vulgare affigurati</a>
      <ul>
        <li>
          <a href="gf2001210s.html#8">Tabula de vacobuli</a>
        </li>
        <li>
          <a href="gf2001210s.html#18">Tabula de li capituli</a>
        </li>
        <li>
          <a href="gf2001210s.html#20">Oratio</a>
        </li>
        <li>
          <a href="gf2001210s.html#21">La prefatione</a>
        </li>
        <li>
          <a href="gf2001210s.html#22">Primo libro</a>
          <ul>
            <li>
              <a href="gf2001210s.html#25">De la institutione de
              li architecti. Capo primo</a>
            </li>
            <li>
              <a href="gf2001210s.html#46">De quale cose consta la
              architectura. Capo secundo</a>
            </li>
            <li>
              <a href="gf2001210s.html#56">De la parte del
              architectura. Capo tertio</a>
            </li>
            <li>
              <a href="gf2001210s.html#57">De la electione de li
              loci apti ala salute: et de li lumi de le fenestre.
              Capo IIII. [sic]</a>
            </li>
            <li>
              <a href="gf2001210s.html#61">De li fundamenti de le
              mure et consttitutione de le torre. Capo quinto</a>
            </li>
            <li>
              <a href="gf2001210s.html#65">De le divisione de le
              opere quale sono intra le mure et de la loro
              dispositione acio li nocivi flati de li venti siano
              vitati: Capo VI.</a>
            </li>
            <li>
              <a href="gf2001210s.html#77">De la electione deli
              loci per situare le sacre aede intro et di fora de la
              civitate. Capo septimo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gf2001210s.html#79">Libro secundo&#160;</a>
          <ul>
            <li>
              <a href="gf2001210s.html#81">De la vita de li primi
              homini et principii de la humanita et initii
              architectura et soi augumenti. Capo I.&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#87">De li principii de le
              cose secundo le opinione de li philosophi. Capo
              secundo</a>
            </li>
            <li>
              <a href="gf2001210s.html#88">De la generatione de li
              quadri lateri. Capo tertio&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#91">De la Arena in qual
              modo epsa sia eligenda per la operatione de alligarla
              con la calce. Capo quarto</a>
            </li>
            <li>
              <a href="gf2001210s.html#92">De quale pietre si de
              fare la calce. Capo quinto&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#93">De la polvere
              puteolana. Capo sexto</a>
            </li>
            <li>
              <a href="gf2001210s.html#95">De la lapidicine seu
              dove si cavano li saxi. Capo septimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#97">De le generatione de le
              structure et de le loro qualitate modi et loci. Capo
              octavo</a>
            </li>
            <li>
              <a href="gf2001210s.html#105">De le grosseze de li
              muri et de li aedificii et de quadrelli. Capo
              nono</a>
            </li>
            <li>
              <a href="gf2001210s.html#106">Del modo de tagliare
              la materia seu arbori de lagnami. Capo nono</a>
            </li>
            <li>
              <a href="gf2001210s.html#112">De la abiete supernate
              et infernate con la descriptione del Apennino. Capo
              decimo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gf2001210s.html#114">Tertio libro</a>
          <ul>
            <li>
              <a href="gf2001210s.html#116">De la compositione de
              le sacre aede et de le symmetrie et mensura del corpo
              humano. Capo primo</a>
            </li>
            <li>
              <a href="gf2001210s.html#128">De le cinque specie de
              la aede. Capo secundo</a>
            </li>
            <li>
              <a href="gf2001210s.html#132">De le fundazione de le
              columne et de le loro membri et ornato et epistylii
              et zophori et corone. Capo tertio</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gf2001210s.html#141">Libro quarto</a>
          <ul>
            <li>
              <a href="gf2001210s.html#141">De le tre generatione
              de columne, et loro origini et inventione. Capo
              primo</a>
            </li>
            <li>
              <a href="gf2001210s.html#146">De li ornamenti de le
              columne et loro origine. Capo secundo</a>
            </li>
            <li>
              <a href="gf2001210s.html#149">De la ratione dorica.
              Capo terzo&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#153">De la interiore
              distributione de le celle et dil pronao. Capo
              quarto</a>
            </li>
            <li>
              <a href="gf2001210s.html#155">De le constitutione de
              le aede secundo le regione. Capo quinto</a>
            </li>
            <li>
              <a href="gf2001210s.html#156">De la ratione de li
              hostii et porte e antipagmenti de le sacre aede. Capo
              sexto&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#160">De le thuscanice
              ratione de le sacre aede. Capo septimo&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#162">De la ordine de li
              altari de li dei. Capo octavo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gf2001210s.html#163">Libro quinto</a>
          <ul>
            <li>
              <a href="gf2001210s.html#165">De la constitutione
              del foro. Capo primo</a>
            </li>
            <li>
              <a href="gf2001210s.html#169">De le ordinatione del
              aerario, et la carcere et la curia. Capo secundo</a>
            </li>
            <li>
              <a href="gf2001210s.html#170">De la constitutione
              del theatro. Capo tertio</a>
            </li>
            <li>
              <a href="gf2001210s.html#172">De la harmonia. Capo
              quarto</a>
            </li>
            <li>
              <a href="gf2001210s.html#178">De la collocatione de
              li vasi in lo theatro. Capo quinto</a>
            </li>
            <li>
              <a href="gf2001210s.html#182">De la conformatione
              del theatro: in qual modo ella sia da fare. Capo
              sexto&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#184">Del tecto del portico
              del theatro. Capo septimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#187">De le tre generatione
              de scaene. Capo octavo</a>
            </li>
            <li>
              <a href="gf2001210s.html#189">De le portice et
              ambulatione post a la scaena. Capo nono&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#193">De le dispositione de
              li balnei et de le loro parte. Capo decimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#196">De la aedificatione de
              le palestre et de li xysti. Capo undecimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#198">De li porti et
              structure da fare in laqua. Capo XII.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gf2001210s.html#201">Libro sexto</a>
          <ul>
            <li>
              <a href="gf2001210s.html#206">De la natura de le
              regione del coelo, a li quali aspecti li aedificic
              sono da essere dispositi: et cio che per variatione
              de epse regione fano le qualita te in li corpi de li
              homini. Capo primo&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#211">De le proportione et
              mensure de li privati aedificii. Capo secundo</a>
            </li>
            <li>
              <a href="gf2001210s.html#212">De li cavi de le aede.
              Capo tertio</a>
            </li>
            <li>
              <a href="gf2001210s.html#216">De la longitudine et
              latitudine et symmetrie de li altii, et de le loro
              ale, et tablini. Capo quarto</a>
            </li>
            <li>
              <a href="gf2001210s.html#218">De le simmetrie de li
              triclinii et exhedrae, anchora de li oeci et
              pinacothece et loro dimensione. Capo quinto</a>
            </li>
            <li>
              <a href="gf2001210s.html#219">De li oeci ciziceni.
              Capo sexto</a>
            </li>
            <li>
              <a href="gf2001210s.html#220">Ad quale regione del
              coelo ciascune generatione de aedificii debeno
              aspectare ad cio che al uso et a la utilitate siano
              idonei. Capo septimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#221">De li privati et
              communi aedificii che sono da collocare in li proprii
              loci secundo le generatione de ciascune persone et
              loro conveniente qualita. Capo octavo</a>
            </li>
            <li>
              <a href="gf2001210s.html#222">De le ratione de li
              rustici aedificii, et de le descriptione et usi de
              molte loro oarte. Capo nono</a>
            </li>
            <li>
              <a href="gf2001210s.html#223">De la dispositione de
              li graeci aedificii, et de le loro parte anchora de
              le differenti nomi asai da li italici consuetudine et
              usi dicrepanti. Capo decimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#228">De la firmitate de li
              aedificii et loro fundamenti. Capo XI.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gf2001210s.html#231">Libro septimo</a>
          <ul>
            <li>
              <a href="gf2001210s.html#242">De la ruderatione.
              Capo primo</a>
            </li>
            <li>
              <a href="gf2001210s.html#245">De la maceratione de
              la calce da perficere le opere albarie et tectorie.
              Capo secundo</a>
            </li>
            <li>
              <a href="gf2001210s.html#246">De la dispositione de
              le camere et trullisatione et tectoria opera. Capo
              tertio</a>
            </li>
            <li>
              <a href="gf2001210s.html#251">De le pollitone in li
              humidi loci. Capo quarto</a>
            </li>
            <li>
              <a href="gf2001210s.html#253">De le ratione de le
              picture fiende in li aedificii. Capo quinto</a>
            </li>
            <li>
              <a href="gf2001210s.html#259">De il marmore a che
              modo el se dispone a le opere de le tectorie
              politione de li parieti. Capo sexto</a>
            </li>
            <li>
              <a href="gf2001210s.html#260">De li colori et
              primamente del ochra. Capo septimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#262">De le ratione del
              minio. Capo octavo</a>
            </li>
            <li>
              <a href="gf2001210s.html#262">De la temepra del
              minio. Capo nono</a>
            </li>
            <li>
              <a href="gf2001210s.html#264">De li colori quali si
              fano con arte. Capo X.</a>
            </li>
            <li>
              <a href="gf2001210s.html#266">De le temperatione del
              ceruleo colore. Capo XI.</a>
            </li>
            <li>
              <a href="gf2001210s.html#266">In qual modo si facia
              la cerussa, et la aerugine, et sandaraca. Capo
              XII.</a>
            </li>
            <li>
              <a href="gf2001210s.html#267">In qual modo si facia
              lo ostro de tuti li factitii colori excellentissimo.
              Capo XIII.</a>
            </li>
            <li>
              <a href="gf2001210s.html#267">De li purpurei colori.
              Capo XIIII.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gf2001210s.html#269">Libro octavo</a>
          <ul>
            <li>
              <a href="gf2001210s.html#270">De le inventione de le
              aque. Capo primo&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#275">De laqua de le pluvie.
              Capo secundo</a>
            </li>
            <li>
              <a href="gf2001210s.html#279">De le aque calide et
              le loro virtute quale si atraheno da diversi metalli
              et de varii fonti et flumi et laci la loro natura.
              Capo tertio&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#291">De la proprietate de
              alcuni loci et fontii, quali si trovano di miranda
              operatione. Capo IIII. [sic]</a>
            </li>
            <li>
              <a href="gf2001210s.html#294">De li experimenti de
              le aque. Capo V.</a>
            </li>
            <li>
              <a href="gf2001210s.html#294">De le perductione et
              libramenti de le aque et instrumenti ad tale uso.
              Capo VI.</a>
            </li>
            <li>
              <a href="gf2001210s.html#296">In quanto modi si
              conduceno le aque. Capo VII.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gf2001210s.html#305">Libro nono</a>
          <ul>
            <li>
              <a href="gf2001210s.html#307">Inventione de platone
              del mensulare li campi. Capo primo</a>
            </li>
            <li>
              <a href="gf2001210s.html#310">De la norma emendata
              inventione de Pythagora da la deformatione del
              trigono hortogonico. Capo II.</a>
            </li>
            <li>
              <a href="gf2001210s.html#312">In qual modo la
              proportione del argento mixta con lo auro in la
              integra opera se possa deprehemdere et discernere.
              Capo III.</a>
            </li>
            <li>
              <a href="gf2001210s.html#316">De le gnomonice
              ratione da li radii del sole trovate per lumbra, et
              al mundo et anche ali planeti. Capo IIII. [sic]</a>
            </li>
            <li>
              <a href="gf2001210s.html#325">Del corso del sole per
              li duodecimi signi. Capo quinto</a>
            </li>
            <li>
              <a href="gf2001210s.html#327">De li syderi quali
              sono dal zodiaco al septentrione. Capo VI.</a>
            </li>
            <li>
              <a href="gf2001210s.html#330">De li syderi quali
              sono dal zodiaco al mezo. Capo septimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#334">De le ratione de li
              horologii et umbre de li gnomoni al tempo
              aequinoctiale in Roma et in altri loci. Capo VII.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gf2001210s.html#343">Libro decimo</a>
          <ul>
            <li>
              <a href="gf2001210s.html#345">De la machina que cosa
              sia, et de la lei differentia dal organo, de la
              origine et necessitate. Capo primo</a>
            </li>
            <li>
              <a href="gf2001210s.html#347">De le tractorie
              machinatione de le aede sacre et publice opere. Capo
              secundo</a>
            </li>
            <li>
              <a href="gf2001210s.html#348">De diverse
              appellatione de machine et cum qual ratione se
              errigeno. Capo tertio</a>
            </li>
            <li>
              <a href="gf2001210s.html#349">De una machina simile
              a la superiore cum la quale li colossicoteri piu
              securamante se pono trahere immutata solamente la
              sucula in tympano. Capo IIII. [sic]</a>
            </li>
            <li>
              <a href="gf2001210s.html#350">De una altra
              generatione de tractoria machina. Capo V.</a>
            </li>
            <li>
              <a href="gf2001210s.html#352">Una ingeniosa ratione
              de Ctesiphonte a conducere li gravi oneri. Capo
              VI.&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#354">De la inventione de la
              lapidicina da la qualle il templo de la Diana Ephesia
              fu constructo. Capo VII.&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#355">Del porrecto et
              rotundatione de le machine a la elevatione de le cose
              ponderose. Capo VIII.</a>
            </li>
            <li>
              <a href="gf2001210s.html#358">De le generatione de
              li organi ad cavare aqua et primamente del tympamo.
              Capo nono&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#360">De le rote et tympani
              per masinare la farina. Capo X.</a>
            </li>
            <li>
              <a href="gf2001210s.html#361">De la coclea quale
              eleva magna copia de aqua, ma non cosi altamente.
              Capo undecimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#363">De la ctesibica
              machina quale altissimamente extolle laqua. Capo
              XII.</a>
            </li>
            <li>
              <a href="gf2001210s.html#364">De le hydraulice
              machine, con le quale se perficeno li organi. Capo
              XIII.&#160;</a>
            </li>
            <li>
              <a href="gf2001210s.html#366">Con qual ratione
              quelli che sono menati in careta aut in nave possano
              il facto viagio mensurare. Capo XIIII. [sic]</a>
            </li>
            <li>
              <a href="gf2001210s.html#370">De le ratione de le
              catapulte et scorpioni. Capo XV.</a>
            </li>
            <li>
              <a href="gf2001210s.html#372">De la ratione de le
              baliste. Capo sextodecimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#373">De la proportione de
              li saxi da essere iactati proportionatamente al
              foramine de la balista. Capo XVII.</a>
            </li>
            <li>
              <a href="gf2001210s.html#374">Del modo de accordare
              et temperare le catapulte e baliste. Capo XVIII.</a>
            </li>
            <li>
              <a href="gf2001210s.html#375">De le cose
              oppugnatorie et defensorie, et primamente de la
              inventione del Ariete, et lui machina. Capo XIX.</a>
            </li>
            <li>
              <a href="gf2001210s.html#378">De la testudine a la
              congestione de le fosse da essere preparata. Capo
              vigessimo</a>
            </li>
            <li>
              <a href="gf2001210s.html#380">De altre generatione
              de testudine. Capo XXI.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gf2001210s.html#387">Registrum</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
