<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katefir181017291s.html#6">Nota de’ quadri e opere
      di scultura esposti per la festa di S. Luca dagli Accademici
      del Disegno nella loro cappella e nel chiostro secondo del
      Convento de’ PP. della SS. Nonziata di Firenze l’anno
      1729</a>
      <ul>
        <li>
          <a href="katefir181017291s.html#10">Cappella</a>
          <ul>
            <li>
              <a href="katefir181017291s.html#10">I. Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#11">II.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#11">III.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#12">IV.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#13">V. Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#14">VI.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#14">VII.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#15">VIII.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#16">IX.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#17">X. Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#22">XI.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#23">XII.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#24">XIII.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#25">XIV.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#26">XV.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#27">XVI.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#28">XVII.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#29">XVIII.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#30">XIX.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#30">XX.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#32">XXI.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#33">XXII.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#34">XXIII.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#34">XXIV.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#34">XXV.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#35">XXVI.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#36">XXVII.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#37">XXVIII.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#38">XXIX.
              Lunetta</a>
            </li>
            <li>
              <a href="katefir181017291s.html#38">XXX.
              Lunetta</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
