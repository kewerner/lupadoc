<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katebas695018071s.html#8">Catalogo degli Artisti
      Bassanesi viventi</a>
      <ul>
        <li>
          <a href="katebas695018071s.html#14">Intaglio in
          rame</a>
        </li>
        <li>
          <a href="katebas695018071s.html#34">Pittura scultura e
          disegno</a>
        </li>
        <li>
          <a href="katebas695018071s.html#44">Ode dell'abate
          Giuseppe Barbieri</a>
        </li>
        <li>
          <a href="katebas695018071s.html#52">Al Signor Giuseppe
          Casati, Sonetto</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
