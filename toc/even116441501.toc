<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="even116441501s.html#6">Guida per la città di
      Venezia all’amico delle belle arti; Tomo I.</a>
      <ul>
        <li>
          <a href="even116441501s.html#8">Alla Nobil Donna
          Marina Loredan-Gradenigo</a>
        </li>
        <li>
          <a href="even116441501s.html#10">Prefazione</a>
        </li>
        <li>
          <a href="even116441501s.html#46">[Guida per la città
          di Venezia all’amico delle belle arti]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
