<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghpal549139501s.html#10">El museo pictorico, y
      escala óptica</a>
      <ul>
        <li>
          <a href="ghpal549139501s.html#12">Advertencia del
          editor</a>
        </li>
        <li>
          <a href="ghpal549139501s.html#14">Prólogo al
          Lector&#160;</a>
        </li>
        <li>
          <a href="ghpal549139501s.html#18">Tabla de los libros,
          y capitulos</a>
        </li>
        <li>
          <a href="ghpal549139501s.html#26">I. El aficionado</a>
        </li>
        <li>
          <a href="ghpal549139501s.html#104">II. Propios, y
          accidentes de la pintura</a>
        </li>
        <li>
          <a href="ghpal549139501s.html#258">III. Teórica de la
          pintura</a>
        </li>
        <li>
          <a href="ghpal549139501s.html#362">Indice de los
          términos privativos</a>
        </li>
        <li>
          <a href="ghpal549139501s.html#386">Indice de las cosas
          mas notables</a>
        </li>
        <li>
          <a href="ghpal549139501s.html#422">&#160;[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
