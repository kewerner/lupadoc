<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501756s.html#6">Le cose meravigliose dell'alma
      città di Roma</a>
      <ul>
        <li>
          <a href="dg4501756s.html#8">Le sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg4501756s.html#8">La prima Chiesa è ›San
              Giovanni in Laterano‹ ◉</a>
            </li>
            <li>
              <a href="dg4501756s.html#12">Seconda Chiesa di ›San
              Pietro in Vaticano‹ ◉</a>
            </li>
            <li>
              <a href="dg4501756s.html#16">La terza Chiesa è ›San
              Paolo fuori le Mura‹ ◉</a>
            </li>
            <li>
              <a href="dg4501756s.html#17">La quarta Chiesa è
              ›Santa Maria Maggiore‹ ◉</a>
            </li>
            <li>
              <a href="dg4501756s.html#19">La quinta Chiesa È San
              Lorenzo for dalle mura ›San Lorenzo fuori le Mura‹
              ◉</a>
            </li>
            <li>
              <a href="dg4501756s.html#20">La Sesta Chiesa é ›San
              Sebastiano‹ ◉</a>
            </li>
            <li>
              <a href="dg4501756s.html#21">La Settima Chiesa é
              ›Santa Croce in Gerusalemme‹ ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501756s.html#22">Nell'›Isola Tiberina‹</a>
        </li>
        <li>
          <a href="dg4501756s.html#23">In ›Trastevere‹</a>
        </li>
        <li>
          <a href="dg4501756s.html#26">Nel ›Borgo‹</a>
        </li>
        <li>
          <a href="dg4501756s.html#28">Della Porta Flaminia fuori
          dal Popolo ›Porta del Popolo‹ fino alle radici del
          ›Campidoglio‹</a>
        </li>
        <li>
          <a href="dg4501756s.html#42">Dal ›Campidoglio‹ a mani
          sinistra verso li Monti</a>
        </li>
        <li>
          <a href="dg4501756s.html#51">Dal ›Campidoglio‹ a man
          dritta verso li Monti</a>
        </li>
        <li>
          <a href="dg4501756s.html#58">Tavola delle Chiese</a>
        </li>
        <li>
          <a href="dg4501756s.html#63">Le stationi che sono nelle
          chiese di Roma [...]</a>
          <ul>
            <li>
              <a href="dg4501756s.html#72">Le Stationi
              dell'Advento</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501756s.html#74">Trattato over modo
          d'acquistar l'indulgentie alle Stationi</a>
        </li>
        <li>
          <a href="dg4501756s.html#80">la Guida Romana per li
          forastieri, che vengono per vedere le antichità di Roma,
          a una per una, in bellissima forma, et brevità</a>
          <ul>
            <li>
              <a href="dg4501756s.html#80">[Giornata prima]</a>
              <ul>
                <li>
                  <a href="dg4501756s.html#80">Del ›Borgo‹ la
                  prima giornata</a>
                </li>
                <li>
                  <a href="dg4501756s.html#81">Del
                  ›Trastevere‹</a>
                </li>
                <li>
                  <a href="dg4501756s.html#82">Dell'Isola
                  Tiberina‹</a>
                </li>
                <li>
                  <a href="dg4501756s.html#82">Del Ponte Santa
                  Maria ›Ponte Rotto‹, Palazzo di pilato ›Casa dei
                  Crescenzi‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501756s.html#83">Del Monte Testaccio
                  ›Testaccio (Monte)‹ e di molte altre cose</a>
                </li>
                <li>
                  <a href="dg4501756s.html#84">Delle Therme
                  Antoniane ›Terme di Caracalla‹ , ed altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4501756s.html#84">Di ›San Giovanni in
                  Laterano‹, et altre cose</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501756s.html#85">Gioranta seconda</a>
              <ul>
                <li>
                  <a href="dg4501756s.html#85">Della ›Porta del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4501756s.html#86">De i Cavalli di
                  marmo che stanno a monte Cavallo ›Quirinale‹ , et
                  delle Therme Diocletiane ›Terme di
                  Diocleziano‹</a>
                </li>
                <li>
                  <a href="dg4501756s.html#87">Della strada Pia
                  ›Via XX Settembre‹</a>
                </li>
                <li>
                  <a href="dg4501756s.html#87">Della Vigna del
                  Cardinal di Ferrara</a>
                </li>
                <li>
                  <a href="dg4501756s.html#88">Della Vigna del
                  Cardinal di Carpi, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501756s.html#88">Della ›Porta
                  Pia‹</a>
                </li>
                <li>
                  <a href="dg4501756s.html#88">Di ›Sant'Agnese
                  fuori le Mura‹, et altre anticaglie</a>
                </li>
                <li>
                  <a href="dg4501756s.html#89">Del Tempio di
                  Iside, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501756s.html#89">Delle ›Sette Sale‹,
                  et del ›Colosseo‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501756s.html#90">Del tempio della
                  Pace ›Foro della Pace‹, et del onte Palatino,
                  hora detto Palazzo Maggiore ›Palatino‹, et altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4501756s.html#91">Del ›Campidoglio‹,
                  et altre cose</a>
                </li>
                <li>
                  <a href="dg4501756s.html#92">De i portichi di
                  Ottavia ›Portico d'Ottavia‹, et di Settimio
                  ›Porticus Severi‹, et del ›Teatro di Pompeo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501756s.html#92">Giornata terza</a>
              <ul>
                <li>
                  <a href="dg4501756s.html#92">Delle due Colonne
                  una di Antonio Pio, et l'altra di Traiano, et
                  altre cose ›Colonna di Antonino Pio‹ ›Colonna
                  Traiana‹</a>
                </li>
                <li>
                  <a href="dg4501756s.html#93">Della Rotonda,
                  overo ›Pantheon‹</a>
                </li>
                <li>
                  <a href="dg4501756s.html#93">Dei Bagni di
                  Agrippa, et di Nerone ›Terme di Agrippa‹ ›Terme
                  Neroniano-Alessandrine‹</a>
                </li>
                <li>
                  <a href="dg4501756s.html#93">Della piazza
                  Navona, et di mastro Pasquino ›Piazza Navona‹
                  ›Statua di Pasquino‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501756s.html#94">Summi Pontifices</a>
            </li>
            <li>
              <a href="dg4501756s.html#123">Reges et Imperatores
              Romani</a>
            </li>
            <li>
              <a href="dg4501756s.html#127">Li Re di Francia</a>
            </li>
            <li>
              <a href="dg4501756s.html#129">Li Re del Regno di
              Napoli et di Sicilia, il quali cominciorno a regnare
              l'anno di nostra salute</a>
            </li>
            <li>
              <a href="dg4501756s.html#130">Li Dugi di Vinegia</a>
            </li>
            <li>
              <a href="dg4501756s.html#133">Li Duchi di Milano</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501756s.html#134">L'antichità di Roma</a>
      <ul>
        <li>
          <a href="dg4501756s.html#135">Alli lettori</a>
        </li>
        <li>
          <a href="dg4501756s.html#136">Libro I</a>
          <ul>
            <li>
              <a href="dg4501756s.html#136">Dell'edification di
              Roma</a>
            </li>
            <li>
              <a href="dg4501756s.html#138">Del Circuito di
              Roma</a>
            </li>
            <li>
              <a href="dg4501756s.html#139">Delle Porte</a>
            </li>
            <li>
              <a href="dg4501756s.html#141">Delle vie</a>
            </li>
            <li>
              <a href="dg4501756s.html#142">Delli Ponti che sono
              sopra il Tevere, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501756s.html#143">Dell'Isola del Tevere
              ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#144">Delli Monti</a>
            </li>
            <li>
              <a href="dg4501756s.html#145">Del Monte Testaccio
              ›Testaccio (Monte)‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#145">Delle Acque, et chi le
              condusse in Roma</a>
            </li>
            <li>
              <a href="dg4501756s.html#147">Della ›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#147">Delli Acquedotti</a>
            </li>
            <li>
              <a href="dg4501756s.html#148">Delle ›Sette Sale‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#148">Delle Therme cioè
              Bagni et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501756s.html#150">Delle Naumachie, dove
              si facevano le battaglie navali, et che cose
              erano</a>
            </li>
            <li>
              <a href="dg4501756s.html#150">De' cerchi, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501756s.html#151">De' Theatri, et che
              cosa erano, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501756s.html#151">Delli Anfitheatri, et
              suoi edificatori, et che cosa erano</a>
            </li>
            <li>
              <a href="dg4501756s.html#152">De' Fori, cioè
              Piazze</a>
            </li>
            <li>
              <a href="dg4501756s.html#153">Delli Archi Trionfali,
              et a chi si davano</a>
            </li>
            <li>
              <a href="dg4501756s.html#154">De' Portichi</a>
            </li>
            <li>
              <a href="dg4501756s.html#154">De' Trofei, et Colonne
              memorande</a>
            </li>
            <li>
              <a href="dg4501756s.html#155">De' Colossi</a>
            </li>
            <li>
              <a href="dg4501756s.html#155">Delle Piramidi</a>
            </li>
            <li>
              <a href="dg4501756s.html#156">Delle Mete</a>
            </li>
            <li>
              <a href="dg4501756s.html#156">Delli Obelischi, over
              Aguglie</a>
            </li>
            <li>
              <a href="dg4501756s.html#157">Delle Statue</a>
            </li>
            <li>
              <a href="dg4501756s.html#157">›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#157">De Cavalli ›Fontana di
              Monte Cavallo‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#157">Delle Librarie</a>
            </li>
            <li>
              <a href="dg4501756s.html#158">Delli Horiuoli</a>
            </li>
            <li>
              <a href="dg4501756s.html#158">De' Palazzi</a>
            </li>
            <li>
              <a href="dg4501756s.html#159">Della Casa Aurea di
              Nerone ›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#159">Dell'altre case de'
              Cittadini</a>
            </li>
            <li>
              <a href="dg4501756s.html#160">Delle Curie, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501756s.html#161">De' Senatuli, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501756s.html#161">De' Magistrati</a>
            </li>
            <li>
              <a href="dg4501756s.html#162">De Comitij, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501756s.html#162">Delle Tribu</a>
            </li>
            <li>
              <a href="dg4501756s.html#163">Delle Regioni, cioè
              Rioni, et sue insegne</a>
            </li>
            <li>
              <a href="dg4501756s.html#163">Delle Basiliche, et
              che cosa erano</a>
            </li>
            <li>
              <a href="dg4501756s.html#163">Del ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#165">Dello Erario
              ›Aerarium‹, cioè Camera del commune, et che moneta si
              spendeva in Roma in que' tempi</a>
            </li>
            <li>
              <a href="dg4501756s.html#166">Del Gregostasi, et che
              cosa era ›Grecostasi‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#166">Della Secretaria del
              Popolo Romano ›Secretarium Senatus‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#166">Dell'›Asilo di
              Romolo‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#166">Delle Rostre, et che
              cosa erano ›Rostri‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#166">Della Colonna detta
              Miliario ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#167">Del Tempio di Carmenta
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#167">Della colonna Bellica
              ›Columna Bellica‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#167">Della Colonna Lattaria
              ›Columna Lactaria‹</a>
            </li>
            <li>
              <a href=
              "dg4501756s.html#167">Dell'›Aequimaelium‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#168">Del ›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#168">Del Tigillo Sororio
              ›Tigillum Sororium‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#168">De' Campi Forastieri
              ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#168">Della ›Villa
              Publica‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#169">Della ›Taberna
              Meritoria‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#169">Del ›Vivarium‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#169">Degli Horti</a>
            </li>
            <li>
              <a href="dg4501756s.html#170">Del ›Velabro‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#171">Delle ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#171">Delli Clivi</a>
            </li>
            <li>
              <a href="dg4501756s.html#172">De Prati</a>
            </li>
            <li>
              <a href="dg4501756s.html#172">De Granari Publici, et
              magazini del Sale</a>
            </li>
            <li>
              <a href="dg4501756s.html#172">Delle Carceri
              publiche</a>
            </li>
            <li>
              <a href="dg4501756s.html#172">Di alcune feste, et
              giochi che si solevano celebrare in Roma</a>
            </li>
            <li>
              <a href="dg4501756s.html#173">Del Sepolchro di
              Augusto, d'Adriano, et di Settimio</a>
            </li>
            <li>
              <a href="dg4501756s.html#174">De Tempij</a>
            </li>
            <li>
              <a href="dg4501756s.html#175">De' Sacerdoti delle
              Vergini Vestali, vestimenti, vasi, et altri
              instrumenti fatti per uso delli sacrificij, et suoi
              institutori</a>
            </li>
            <li>
              <a href="dg4501756s.html#178">Dell'Armamentario et
              che cosa era ›Armamentaria‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#178">Dell'Esercito Romano
              di terra, e mare e loro insegne</a>
            </li>
            <li>
              <a href="dg4501756s.html#178">De' Trionfi et a chi
              si concedevano, et chi fu il primo trionfatore, et di
              quante maniere erano</a>
            </li>
            <li>
              <a href="dg4501756s.html#179">Delle corone, et a chi
              si davano</a>
            </li>
            <li>
              <a href="dg4501756s.html#180">Del numero del Popolo
              Romano</a>
            </li>
            <li>
              <a href="dg4501756s.html#180">Delle ricchezze del
              Popolo Romano</a>
            </li>
            <li>
              <a href="dg4501756s.html#180">Della liberalità degli
              antichi Romani</a>
            </li>
            <li>
              <a href="dg4501756s.html#181">Delli Matrimonij
              antichi, et loro usanza</a>
            </li>
            <li>
              <a href="dg4501756s.html#181">Della buona creanza,
              che davano a i figliuoli</a>
            </li>
            <li>
              <a href="dg4501756s.html#182">Della separazione de
              Matrimonij</a>
            </li>
            <li>
              <a href="dg4501756s.html#183">Dell'essequie antiche,
              et sue ceremonie</a>
            </li>
            <li>
              <a href="dg4501756s.html#184">Delle Torri</a>
            </li>
            <li>
              <a href="dg4501756s.html#184">Del ›Tevere‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#185">Del Palazzo Papale
              ›Palazzo Apostolico Vaticano‹, et di Belvedere
              ›Cortile del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#186">Del ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4501756s.html#186">Recapitulatione
              dell'antichità</a>
            </li>
            <li>
              <a href="dg4501756s.html#187">De' Tempij degli
              Antichi fuori di Roma</a>
            </li>
            <li>
              <a href="dg4501756s.html#188">Quante volte è stata
              presa Roma</a>
            </li>
            <li>
              <a href="dg4501756s.html#189">Dei fuochi de gli
              Antichi, scritti da pochi autori, et cavati da alcuni
              frammenti d'Historie</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501756s.html#193">Tavola delle Antichità
          della Città di Roma</a>
        </li>
        <li>
          <a href="dg4501756s.html#195">Lettera Pastorale del
          Monsignor Illustrissimo et Reverendissimo Cardinal
          Borromeo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
