<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="eper3173920s.html#6">Dissertazione sull’antico
      tempio di Sant’Angelo situato vicino alla porta della città
      di Perugia a cui da il nome</a>
      <ul>
        <li>
          <a href="eper3173920s.html#8">Al reverendissimo
          capitolo delle chiesa cattedrale di S. Lorenzo di
          Perugia</a>
        </li>
        <li>
          <a href="eper3173920s.html#10">Agli amatori
          d'antichità</a>
        </li>
        <li>
          <a href="eper3173920s.html#15">Articolo, estratto dalla
          Gazzetta Toscana</a>
        </li>
        <li>
          <a href="eper3173920s.html#18">Dissertazione</a>
        </li>
        <li>
          <a href="eper3173920s.html#65">Spiegazione delle figure
          appartenenti al descritto Tempio</a>
        </li>
        <li>
          <a href="eper3173920s.html#73">Descrizione delle
          pitture, che sono in questo Tempio di Sant'Angelo</a>
        </li>
        <li>
          <a href="eper3173920s.html#76">Annotazioni alla
          precedente dissertazione&#160;</a>
        </li>
        <li>
          <a href="eper3173920s.html#102">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
