<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502740s.html#6">Studio di pittura, scoltura, et
      architettura, nelle Chiese di Roma</a>
      <ul>
        <li>
          <a href="dg4502740s.html#8">All'Eminentissimo e
          Reverendissimo Signore il Signor Cardinale Gasparo
          Carpegna Vicario, e Prodatario di Nostro Signore [dedica
          dell'autore]</a>
        </li>
        <li>
          <a href="dg4502740s.html#14">Al lettore [premessa
          dell'autore]</a>
        </li>
        <li>
          <a href="dg4502740s.html#16">Al Signor Abbate [sic] Titi
          Sopra le sue notizie di tutte le pitture, e scolture
          delle Chiese di Roma Sonetto Del Signor Conte C.H.
          Sanmartino Accademico Umorista</a>
        </li>
        <li>
          <a href="dg4502740s.html#17">Alle Pitture, e Scolture
          nelle Chiese di Roma Raccolte dal Signor Abbate [sic]
          Titi Nobile da Città di Castello Sonetto. Del Signor Don
          Oratio Quaranta</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502740s.html#19">Indice delle Chiese</a>
    </li>
    <li>
      <a href="dg4502740s.html#30">[Testo]</a>
      <ul>
        <li>
          <a href="dg4502740s.html#30">Di ›San Pietro in
          Vaticano‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#55">Di ›Santa Marta
          (Vaticano)‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#57">Di Santa Maria in Campo
          Santo ›Santa Maria della Pietà‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#58">Di ›Santo Spirito in
          Sassia‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#63">Di ›Sant'Onofrio al
          Gianicolo‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#66">Di San Leonardo ›Santi
          Leonardo e Romualdo (scomparsa)‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#66">Della Chiesa di Regina
          Coeli ›Santa Maria Regina Coeli‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#67">Di Santa Croce della
          Penitenza ›Santa Croce delle Scalette‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#68">Di ›San Pietro in
          Montorio‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#73">Di ›Santa Maria della
          Scala‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#75">Di ›Santa Maria in
          Trastevere‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#79">Di ›San Callisto‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#81">Di ›San Francesco a
          Ripa‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#84">Di ›Santa Maria
          dell'Orto‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#86">Di ›Santa Cecilia in
          Trastevere‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#89">Di ›San Crisogono‹ in
          Trastevere</a>
        </li>
        <li>
          <a href="dg4502740s.html#90">Di ›San Bartolomeo
          all'Isola‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#93">Di San Giovanni Collavita
          ›San Giovanni Calibita‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#94">Di ›Santa Sabina‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#95">Di ›San Paolo fuori le
          Mura‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#100">Di ›Santi Vincenzo ed
          Anastasio alle Tre Fontane‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#101">Di ›Santa Maria Scala
          Coeli‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#102">Di ›San Paolo alle Tre
          Fontane‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#103">Di ›San Sebastiano‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#107">Di ›Santi Nereo e
          Achilleo‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#108">Di ›Santa Balbina‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#109">Di ›Santa Prisca‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#110">Di San Gregorio nel Monte
          Celio ›San Gregorio Magno‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#112">Di Santa Silvia ›Oratorio
          di Santa Silvia‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#113">Di Sant'Andrea ›Oratorio
          di Sant'Andrea‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#114">Di Santa Barbara ›Oratorio
          di Santa Barbara‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#115">Di ›Santi Giovanni e
          Paolo‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#116">Di San Giovanni Battista
          Decollato ›San Giovanni Decollato‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#119">›Oratorio di San Giovanni
          Decollato‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#121">Di ›Sant'Eligio dei
          Ferrari‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#122">Di Santa Maria in Portico,
          hoggi ›Santa Galla (scomparsa)‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#124">Di ›San Nicola in
          Carcere‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#125">Di ›Sant'Angelo in
          Pescheria‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#127">Di ›Sant'Ambrogio della
          Massima‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#128">Di ›Santa Caterina dei
          Funari‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#132">Di Sant'Anna alli Funari
          ›Sant'Anna dei Falegnami‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#133">Di ›San Carlo ai
          Catinari‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#137">Di ›Santa Maria del
          Pianto‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#138">Di ›San Tommaso dei
          Cenci‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#138">Di ›San Bartolomeo dei
          Vaccinari‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#139">Dell'Oratorio per la
          Compagnia della Santissima Trinità ›Oratorio della
          Santissima Trinità dei Pellegrini‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#141">Di ›San Francesco d'Assisi
          a Ponte Sisto‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#141">Di San Giovanni de'
          Bolognesi ›Santi Giovanni Evangelista e Petronio dei
          Bolognesi‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#142">Di ›Santa Caterina da
          Siena‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#143">Di ›Sant'Eligio degli
          Orefici‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#144">Dello Spirito Santo de'
          Napolitani ›Chiesa dello Spirito Santo dei Napoletani‹, e
          di Santa Lucia della Chiavica ›Santa Lucia del
          Gonfalone‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#145">Di Santa Maria de Monte
          Serrato ›Santa Maria in Monserrato‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#146">Della Santissima Trinità,
          ò [sic] San Tommaso degl'Inglesi ›San Tommaso di
          Canterbury‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#147">Di ›Santa Caterina della
          Rota‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#148">Di ›San Girolamo della
          Carità‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#150">Della Santissima Trinità
          di Ponte Sisto ›Santissima Trinità dei Pellegrini‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#153">Di San Martino al Monte
          della Pietà ›San Martinello‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#153">Di ›San Lorenzo in
          Damaso‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#157">Di Santa Maria ›Chiesa
          Nuova‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#166">Di ›San Tommaso in
          Parione‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#166">Di Sant'Agnese in Piazza
          Navona ›Sant'Agnese in Agone‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#171">Di San Pantaleone alle
          Scuole Pie ›San Pantaleo‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#172">Di ›Sant'Elena dei
          Credenzieri‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#173">Di ›Sant'Andrea della
          Valle‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#182">Di San Giacomo de'
          Spagnoli ›Nostra Signora del Sacro Cuore‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#186">Di ›San Luigi dei
          Francesi‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#191">Di ›Sant'Eustachio‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#192">Di San Leone nella
          Sapienza ›Sant'Ivo‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#193">Delle Chiese di San Nicolò
          alle Calcare ›San Nicola ai Cesarini‹, e delle Stimmate
          ›Sante Stimmate di San Francesco‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#194">Di ›San Giovanni della
          Pigna‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#195">Di ›Santa Maria sopra
          Minerva‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#212">Di ›Sant'Ignazio‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#215">Di ›Santo Stefano del
          Cacco‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#216">Di Santa Marta incontro al
          Collegio Romano ›Santa Marta (Collegio Romano)‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#217">Del Giesù [sic]
          ›Santissimo Nome di Gesù‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#226">Di ›San Marco‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#231">Di ›Santo Stanislao dei
          Polacchi‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#232">Di ›Santa Maria in
          Campitelli‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#235">Di ›Santa Maria della
          Consolazione‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#238">Di ›Santa Maria in
          Aracoeli‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#245">Di San Gioseppe [sic] ›San
          Giuseppe dei Falegnami‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#247">Di San Luca in santa
          Martina ›Santi Luca e Martina‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#249">Di ›Sant'Adriano al Foro
          Romano‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#251">Di ›San Lorenzo in
          Miranda‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#252">De' ›Santi Cosma e
          Damiano‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#254">Di ›Santa Francesca
          Romana‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#257">Di ›Santo Stefano
          Rotondo‹, e ›Santa Maria in Domnica‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#258">Di San Giovanni Battista
          nel Fonte ›Battistero Lateranense‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#262">Di ›San Giovanni in
          Laterano‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#274">Di San Salvatore alla
          Scala Santa ›Scala Santa‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#276">Di ›Santa Croce in
          Gerusalemme‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#279">Di ›San Lorenzo fuori le
          Mura‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#282">Di ›Santa Bibiana‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#283">Di ›Sant'Eusebio‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#284">Di San Matteo in Merula
          ›San Matteo in Merulana‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#284">De ›Santi Quattro
          Coronati‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#286">Di ›San Clemente‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#287">Di ›Sant'Urbano ai
          Pantani‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#288">Dello Spirito Santo
          Monache ›Chiesa del Santo Spirito alla Colonna
          Traiana‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#289">Di Santa Maria Annuntiata
          in San Basilio ›San Basilio al Foro di Augusto‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#290">Di Santa Maria de' Monti
          ›Chiesa della Madonna dei Monti‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#295">Di ›San Francesco di
          Paola‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#296">Di ›San Pietro in
          Vincoli‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#298">Di ›Santa Lucia in
          Selci‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#299">Di ›San Martino ai
          Monti‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#302">Di ›Santa Prassede‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#306">Di ›Sant'Antonio
          Abate‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#307">Di ›Santa Maria
          Maggiore‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#311">Della Cappella di Sisto V
          ›Cappella Sistina (Santa Maria Maggiore)‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#324">Della Cappella di Paolo V
          ›Cappella Paolina (Santa Maria Maggiore)‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#335">Di ›Santa Pudenziana‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#337">De' Santi Lorenzo in Fonte
          ›San Lorenzo in Fonte‹, et in Panisperna ›San Lorenzo in
          Panisperna‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#338">Di San Bernardino alli
          Monti ›San Bernardino da Siena‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#340">Di ›Sant'Agata dei Goti‹,
          e San Domenico ›Santi Domenico e Sisto‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#342">Di Santa Caterina da Siena
          ›Santa Caterina a Magnanapoli‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#345">Di ›Santa Maria di
          Loreto‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#346">Di San Silvestro à Monte
          Cavallo ›San Silvestro al Quirinale‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#353">Di ›San Vitale‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#355">Di Santa Maria
          degl'Angioli alle Terme ›Santa Maria degli Angeli‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#358">Di Sant'Agnese fuor di
          Porta Pia ›Sant'Agnese fuori le Mura‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#359">Di ›Santa Maria della
          Vittoria‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#363">Di ›Santa Susanna‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#365">Di ›San Bernardo alle
          Terme‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#367">Di ›San Caio‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#368">Di Santa Maria Nuntiata
          ›Monastero dell'Incarnazione detto le Barberine‹, e di
          ›San Carlo alle Quattro Fontane‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#369">Di Sant'Andrea delli
          Giesuiti ›Sant'Andrea al Quirinale‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#371">Di Santa Chiara del
          Santissimo Sagramento [sic] ›Santa Chiara al
          Quirinale‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#372">Di Santa Croce de'
          Lucchesi ›Santa Croce e San Bonaventura dei Lucchesi‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#373">De ›Santi Apostoli‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#377">Di ›San Romualdo‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#378">Di ›Santa Maria in Via
          Lata‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#380">Di ›San Marcello al
          Corso‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#384">Del Crocifisso di San
          Marcello Oratorio ›Oratorio del Crocifisso‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#386">Di ›Santa Maria
          dell'Umiltà‹, e delle Vergini ›Santa Maria delle
          Vergini‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#388">Di ›Santi Vincenzo ed
          Anastasio‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#389">Di Santa Maria di
          Costantinopoli ›Santa Maria d'Itria‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#391">Di ›San Nicola da
          Tolentino‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#395">Di Sant'Antonio di Padova,
          detto della Concettione ›Santa Maria della
          Concezione‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#398">Di ›Sant'Isidoro‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#400">Di Santa Francesca ›Santa
          Francesca Romana dei Padri del Riscatto‹, e
          Sant'Ildefonso ›Santi Ildefonso e Tommaso di
          Villanova‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#401">Di San Gioseppe [sic] alle
          Fratte ›San Giuseppe a Capo le Case‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#403">Di ›Sant'Andrea delle
          Fratte‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#405">Di Propaganda Fide ›Chiesa
          dei Re Magi‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#406">Di Santa Maria in San
          Giovannino ›San Giovanni in Capite‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#408">Di San Silvestro delle
          Monache ›San Silvestro in Capite‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#410">Di Santa Maria Maddalena
          al Corso ›Santa Lucia della Colonna‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#411">Di ›Santa Maria in
          Via‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#414">Di ›Santa Maria in Trivio‹
          de Crociferi</a>
        </li>
        <li>
          <a href="dg4502740s.html#416">Di Santa Maria della Pietà
          de' Pazzarelli ›Santa Maria della Pietà (Piazza
          Colonna)‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#417">Di San Machuto, e
          Bartolomeo de' Bergamaschi ›Santi Bartolomeo e Alessandro
          dei Bergamaschi‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#418">Di Santa Maria in Equirio
          detta degl'Orfanelli ›Santa Maria in Aquiro‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#419">Di Santa Maria della
          Rotonda ›Pantheon‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#423">Della Maddalena ›Santa
          Maria Maddalena‹, e San Salvatore delle Cuppelle ›San
          Salvatore alle Coppelle‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#425">Di ›Santa Croce a
          Montecitorio‹, e San Biagio à Monte Citorio ›San Biagio
          de Monte (scomparsa)‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#426">Di Santa Maria in Campo
          Marzo ›Santa Maria della Concezione in Campo Marzio‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#428">Di ›San Lorenzo in
          Lucina‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#431">Di ›Santi Ambrogio e Carlo
          al Corso‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#435">Della Santissima Trinità
          del Monte ›Trinità dei Monti‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#444">Di ›Sant'Atanasio‹ de'
          Greci</a>
        </li>
        <li>
          <a href="dg4502740s.html#445">Della ›Chiesa di Gesù e
          Maria‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#447">Di San Giacomo
          dell'Incurabili ›San Giacomo in Augusta‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#450">Di ›Santa Maria del
          Popolo‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#455">Di ›Santa Maria di
          Montesanto‹, e de Miracoli ›Santa Maria dei Miracoli‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#457">Di San Rocco, e Martino
          ›San Rocco‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#459">Di San Girolamo de'
          Schiavoni ›San Girolamo degli Illirici‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#461">Di Sant'Antonio da Padoa
          de' Portughesi ›Sant'Antonio dei Portoghesi‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#463">Di Santa Maria in
          ›Sant'Agostino‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#471">Di Santa Maria
          dell'Apollinare ›Sant'Apollinare‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#472">Di ›San Salvatore in
          Lauro‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#476">Di ›Santa Maria
          dell'Anima‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#481">Di ›Santa Maria della
          Pace‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#488">Di ›San Biagio della
          Fossa‹, e Santi Pietro, e Paolo del Confalone ›Oratorio
          del Gonfalone‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#490">Di San Faustino ›Sant'Anna
          dei Bresciani‹, e ›Santa Maria del Suffragio‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#492">Di ›San Giovanni dei
          Fiorentini‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#498">Di San Celso, e Giuliano
          ›Santi Celso e Giuliano‹ vicino à ›Ponte Sant'Angelo‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#501">Di Sant'Angiolo
          ›Sant'Angelo al Corridoio‹, e Sant'Anna in Borgo
          ›Sant'Anna dei Palafrenieri‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#502">Di ›Santa Maria in
          Traspontina‹</a>
        </li>
        <li>
          <a href="dg4502740s.html#505">Di ›San Giacomo
          Scossacavalli‹</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
