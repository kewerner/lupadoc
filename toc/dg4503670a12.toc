<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503670a12s.html#6">Accurata e succinta
      descrizione topografica e istorica di Roma moderna</a>
      <ul>
        <li>
          <a href="dg4503670a12s.html#6">I. Parte II.</a>
          <ul>
            <li>
              <a href="dg4503670a12s.html#8">Rione IV. Di
              ›Campo Marzio‹</a>
              <ul>
                <li>
                  <a href="dg4503670a12s.html#9">Chiesa della
                  Santissima Concezione in Campo Marzo, e suo
                  Monastero ›Santa Maria della Concezione in Campo
                  Marzio‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#11">Di San
                  Niccolò de Perfetti, e suo Oratorio ›San Nicola
                  ai Prefetti‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#12">De' Santi
                  Biagio e Cecilia ›Santi Cecilia e Biagio‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#13">›San Lorenzo
                  in Lucina‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#19">Dell'Oratorio
                  di ›San Lorenzo in Lucina‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#19">Della
                  Santissima Trinità, e dell'annesso Ospizio de'
                  Padri Trinitarj del Rsiscatto ›Santissima Trinità
                  degli Spagnoli‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#20">De' Santi
                  Ambrogio, e Carlo de' Lombardi, loro
                  Archiconfraternita, e Spedale ›Santi Ambrogio e
                  Carlo al Corso‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#23">Palazzo
                  Gaetani, oggi ›Palazzo Ruspoli‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#26">›Palazzo
                  Nuñez Torlonia‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#26">›Piazza di
                  Spagna‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#27">Della
                  Santissima ›Trinità dei Monti‹, e Convento de'
                  Padri Minimi di Francia</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#28">Chiesa della
                  Santissima ›Trinità dei Monti‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#33">Palazzo degli
                  Zuccheri ›Palazzetto Zuccari‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#33">Villa del
                  Gran Duca di Toscana ›Villa Medici‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#34">Veduta di
                  ›Villa Medici‹ sul Monte Pincio ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#39">Di
                  Sant'Orsola, e del Monastero annesso delle
                  Orsoline ›Santi Giuseppe e Orsola (ex)‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#40">Di
                  ›Sant'Atanasio‹ de' Greci e loro Collegio</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#41">Della ›Chiesa
                  di Gesù e Maria‹ al Corso, e Convento degli
                  Agostiniani Scalzi</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#44">Di San
                  Giacomo degl'Incurabili ›San Giacomo in Augusta‹,
                  e suo Spedale, ed Archiconfraternita; e di Santa
                  Maria della Porta del Paradiso ›Santa Maria
                  Portae Paradisi‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#48">›Santa Maria
                  dei Miracoli‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#50">›Santa Maria
                  di Montesanto‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#53">›Piazza del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#54">›Piazza del
                  Popolo‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#56">›Santa Maria
                  del Popolo‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#62">›Porta del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#64">›Villa
                  Odescalchi‹, ›Villa Sannesi‹, e Cesi ›Villa
                  Poniatowski‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#65">›Fontana
                  dell'Acqua Acetosa‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#66">Ponte Molle
                  ›Ponte Milvio‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#67">Di
                  Sant'Andrea a Ponte Molle ›Oratorio di
                  Sant'Andrea (Via Flaminia)‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#68">Tempio di
                  Sant'Andrea nella Via Flaminia ›Oratorio di
                  Sant'Andrea (Via Flaminia)‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#70">›Santa Maria
                  delle Grazie‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#70">Di
                  Sant'Andrea Apostolo nella Via Falminia
                  ›Sant'Andrea del Vignola‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#71">Del
                  ›Conservatorio della Divina Provvidenza‹ a
                  Ripetta</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#72">De' Santi
                  Rocco e Martino, coll'annesso Ospedale, e
                  Archiconfraternita ›San Rocco‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#75">Di San
                  Girolamo degli Schiavoni o Illirici ›San Girolamo
                  degli Illirici‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#77">Veduta del
                  ›Porto di Ripetta‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#78">Del nuovo
                  ›Porto di Ripetta‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#81">›Palazzo
                  Borghese‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#82">›Palazzo
                  Borghese‹, e sua descrizione</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#88">Di San
                  Gregorio a Ripetta, coll'Oratorio de' Muratori
                  ›San Gregorio dei Muratori‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#88">›Collegio
                  Clementino‹ de' Padri Somaschi</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#90">Collegiata di
                  ›Santa Lucia della Tinta‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#90">›Sant'Ivo dei
                  Bretoni‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#91">Di
                  ›Sant'Antonio dei Portoghesi‹, sua Confraternita,
                  e Spedale</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503670a12s.html#94">Rione V. Di
              Ponte.</a>
              <ul>
                <li>
                  <a href="dg4503670a12s.html#96">Della
                  Collegiata de' ›Santi Celso e Giuliano‹ in
                  Banchi</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#98">Palazzi
                  Cicciaporci ›Palazzo Alberini‹, e Niccolini
                  ›Palazzo Gaddi‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#99">›Palazzo del
                  Banco di Santo Spirito‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#99">Di ›Santa
                  Maria della Purificazione‹ in Banchi</a>
                </li>
                <li>
                  <a href=
                  "dg4503670a12s.html#100">Dell'›Oratorio
                  dell'Arciconfraternita della Pietà dei
                  Fiorentini‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#101">Di San
                  Giovanni Batista de' Fiorentini a Strada Giulia,
                  e Spedale contiguo ›San Giovanni dei
                  Fiorentini‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#107">›Collegio
                  Bandinelli‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#107">›Palazzo
                  Sacchetti‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#109">›San Biagio
                  della Pagnotta‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#110">Di ›Santa
                  Maria del Suffragio‹, e sua
                  Archiconfraternita</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#112">De' Santi
                  Faustino e Giovita ›Sant'Anna dei Bresciani‹, e
                  della Compagnia de' Bresciani</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#113">Oratorio di
                  Santa Elisabetta della Compagnia de' Ciechi e
                  Storpi ›Santi Cosma e Damiano in Banchi‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#114">›Palazzo
                  Sforza Cesarini‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#115">›San
                  Giuliano in Banchi‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#115">De' ›Santi
                  Simone e Giuda‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#116">›Palazzo
                  Gabrielli‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#117">Di ›San
                  Salvatore in Lauro‹, oggi detto la Madonna di
                  Loreto de' Marchegiani, e suo Collegio</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#121">Di ›San
                  Simeone Profeta‹, e del Palazzo d'Acquasparta
                  ›Palazzo Cesi‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#122">›Palazzo
                  Lancellotti‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#123">Di ›Santa
                  Maria in Posterula‹ all'Orso, e Collegio annesso
                  de' Padri Celestini</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#124">Di ›San
                  Salvatore in Primicerio‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#125">Di ›San
                  Biagio della Fossa‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#128">Di ›Santa
                  Maria della Pace‹ de' Canonici Regolari
                  Lateranensi</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#135">Di ›Santa
                  Maria dell'Anima‹, e dell'Ospedale de'
                  Teutonici</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#139">Di ›San
                  Nicola dei Lorenesi‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#140">›Palazzo
                  Altemps‹, e Cappella di ›Sant'Aniceto‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#143">Veduta della
                  Chiesa, e Collegio Germanico ›Palazzo di
                  Sant'Apollinare‹ ▣ di ›Sant'Apollinare‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#144">Di Santa
                  Maria dell'Apollinare ›Sant'Apollinare‹, e del
                  Colleggio Germanico ed Ungarico ›Palazzo di
                  Sant'Apollinare‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#146">Di
                  ›Sant'Agostino‹, e Convento degli Eremitani</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#151">Oratorio di
                  San Trifone, e sua Confraternita ›San Trifone in
                  Potserula‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503670a12s.html#153">Rione VI. Di
              Parione.</a>
              <ul>
                <li>
                  <a href="dg4503670a12s.html#154">Della Chiesa
                  di Santa Barbara, di San Tommaso d'Aquino e San
                  Giovanni di Dio, e della Confraternita de'
                  Librari ›Santa Barbara dei Librari‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#156">Palazzo Pio
                  in Campo di Fiore, anticamente degli Orsini
                  ›Palazzo Pio Righetti‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#157">›Campo de'
                  Fiori‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#158">Della
                  Colleggiata di ›San Lorenzo in Damaso‹, et suo
                  Oratorio</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#163">›Palazzo
                  della Cancelleria‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#164">›Palazzo
                  della Cancelleria‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#168">Santa Maria
                  in Vallicella detta la ›Chiesa Nuova‹ ▣ con il
                  suo Oratorio</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#169">›Via del
                  Pellegrino‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#169">Di Santa
                  Maria e San Gregorio in Vallicella, detta la
                  ›Chesia Nuova‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#179">›Palazzo
                  Sora‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#179">Di ›San
                  Tommaso in Parione‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#180">›Palazzetto
                  del Collegio Nardini‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#181">›Palazzo del
                  Governo Vecchio‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#181">Della
                  Natività del Signore, e dell'Archiconfraternita
                  degli Agonizzanti a Pasquino ›Chiesa della
                  Natività di Gesù‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#183">Vestigi
                  della ›Statua di Pasquino‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#185">›Piazza
                  Navona‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#186">›Piazza
                  Navona‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#188">Di
                  Sant'Agnese in Piazza Navona ›Sant'Agnese in
                  Agone‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#193">Palazzo
                  Pamfilj a Piazza Navona ›Palazzo Pamphilj (Piazza
                  Navona)‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#194">›Collegio
                  Innocenziano‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#195">Palazzo
                  Santobuono a Pasquino ›Palazzo Braschi‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#196">›Palazzo
                  Lancellotti (piazza Navona)‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#196">Di ›San
                  Pantaleo‹ de' Padri delle Scuole Pie</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#198">Palazzo de'
                  Massimi ›Palazzo Massimo alle Colonne‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#200">Di ›Santa
                  Maria in Grottapinta (ex)‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#201">Di ›Santa
                  Elisabetta de Fornari‹, e sua Confraternita</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503670a12s.html#203">Rione VII. Della
              Regola.</a>
              <ul>
                <li>
                  <a href="dg4503670a12s.html#204">Chiesa di
                  Sant'Anna de' Funari, e sua Confraternita
                  ›Sant'Anna dei Falegnami‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#206">Di ›San
                  Carlo ai Catinari‹ dei Padri Barnabiti; e
                  dell'Oratorio degl'Infecondi</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#210">›Palazzo
                  Santacroce‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#211">Di San
                  Biagio in Cacaberis, oggi detto Santa Maria degli
                  Angioli, e sua Confraternita ›Santa Maria in
                  Cacabariis‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#212">Di ›Santa
                  Maria del Pianto‹, e Archiconfraternita della
                  Dottrina Cristiana</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#214">Di ›San
                  Tommaso dei Cenci‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#215">Di San
                  Bartolomeo de' Vaccinari, e sua Confraternita
                  ›San Bartolomeo dei Vaccinari‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#216">Di Santa
                  Maria in Arenula, detta in Monticelli ›Santa
                  Maria in Monticelli‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#217">Di San Paolo
                  Apostolo, detto San Paolino alla Regola, e
                  Convento annesso ›San Paolo alla Regola‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#219">De ›Santi
                  Vincenzo ed Anastasio alla Regola‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#219">Della
                  Chiesa, e grande Ospizio, della ›Santissima
                  Trinità dei Pellegrini (Roma)‹, e
                  Convalescenti</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#224">Di ›San
                  Salvatore in Onda‹ de' Frati Conventuali di San
                  Francesco</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#225">Di ›San
                  Francesco d'Assisi a Ponte Sisto‹, e dell'Ospizio
                  de' poveri Sacerdoti</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#227">›Fontana di
                  Ponte Sisto‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#228">Dei Santi
                  Giovanni Evangelista, Petronio dei Bolognesi, e
                  sua Confraternita ›Santi Giovanni Evangelista e
                  Petronio dei Bolognesi‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#229">Di Santa
                  Maria dell'Orazione, e della Compagnia della
                  Morte ›Santa Maria dell'Orazione e Morte‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#231">›Palazzo
                  Falconieri‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#233">Di ›Santa
                  Caterina di Siena‹, e sua Archiconfraternita</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#234">Di
                  ›Sant'Eligio degli Orefici‹, ed Argentieri, e sua
                  Confraternita</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#235">Della
                  ›Chiesa dello Spirito Santo dei Napoletani‹, e
                  sua Confraternita</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#236">›Palazzo
                  Ricci‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#237">Colleggio
                  Ghislieri ›Liceo Ginnasio Virgilio‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#237">›San Nicolò
                  degli Incoronati‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#238">Di ›San
                  Filippo Neri‹ a Strada Giulia e sua
                  Confraternita</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#239">Delle
                  ›Carceri Nuove‹</a>
                </li>
                <li>
                  <a href=
                  "dg4503670a12s.html#240">Dell'Oratorio dei
                  Santi Pietro e Paolo, ed Archiconfraternita del
                  Gonfalone ›Oratorio del Gonfalone‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#241">Di Santa
                  Lucia della Chiavica, e del Gonfalone ›Santa
                  Lucia del Gonfalone‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#242">Di Santo
                  Stefano in Pescivola ›Santo Stefano in
                  Piscinula‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#243">Di San
                  Giovanni Evangelista in Aino ›San Giovanni
                  Evangelista in Ayno‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#244">Di Santa
                  Teresa e San Giovanni della Croce, e Convento de'
                  Carmelitani Scalzi ›Santi Teresa e Giovanni della
                  Croce dei Carmelitani‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#245">Di ›Santa
                  Maria in Monserrato‹, e dell'Ospizio de'
                  Catalani, ed Aragonesi</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#247">Di San
                  Tommeso di Cantorberi e del Collegio Inglese ›San
                  Tommaso di Canterbury‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#248">Di ›Santa
                  Caterina della Rota‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#249">Di ›San
                  Girolamo della Carità‹, e sua Archiconfraternita,
                  coll'annesso Collegio de' padri dell'Oratorio</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#252">Di ›Santa
                  Brigida‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#253">›Palazzo
                  Farnese‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#254">›Palazzo
                  Farnese‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#258">Della statua
                  del Toro ›Toro Farnese‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#260">Stanze del
                  detto ›Palazzo Farnese‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#266">Palazzo
                  Pichini ›Palazzo del Gallo di Roccagiovine‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#268">Di ›Santa
                  Maria della Quercia‹, e sua Confraternita</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#269">›Palazzo
                  Spada‹, già Capodiferro</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#275">Del Sagro
                  Monte di Pietà, e sua Cappella ›Palazzo del Monte
                  di Pietà‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#279">Di ›San
                  Salvatore in Campo‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#279">Di San
                  Martino al Monte della Pietà, ora demolita, e
                  dell'Archinconfraternita della Dottrina Cristiana
                  ›San Martinello‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503670a12s.html#281">Rione VIII. Di
              Sant'Eustachio</a>
              <ul>
                <li>
                  <a href="dg4503670a12s.html#282">Chiesa
                  Collegiata di ›Sant'Esutachio‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#285">Palazzo
                  Cenci, e Lante ›Palazzo Lante‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#285">›Palazzo
                  Giustiniani‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#290">Di San
                  Salvatore alle Terme, vicino a San Luigi de'
                  Francesi ›San Salvatore in Thermis‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#296">›Palazzo
                  Patrizi‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#296">›Palazzo
                  Madama‹, detto ora del Governo nuovo</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#297">De' Santi
                  Giacomo e Idelfonso degli Spagnuoli, e suo
                  Spedale ›Nostra Signora del Sacro Cuore‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#299">›Palazzo
                  Madama‹ ▣</a>
                </li>
                <li>
                  <a href=
                  "dg4503670a12s.html#303">Dell'Archiginnasio
                  della Sapienza ›Palazzo della Sapienza‹, e sua
                  Chiesa dei Santi Luca Leone ed Ivo ›Sant'Ivo‹</a>
                </li>
                <li>
                  <a href=
                  "dg4503670a12s.html#304">Archyginnasio Romano
                  ›Palazzo della Sapienza‹ ▣</a>
                </li>
                <li>
                  <a href=
                  "dg4503670a12s.html#306">Archyginnasio della
                  Sapienza ›Palazzo della Sapienza‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#311">Di Santa
                  Caterina da Siena, già chiamata de' Neofiti
                  ›Cappella di Santa Caterina da Siena‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#312">Di ›Santa
                  Chiara‹, e suo Monastero di Monache</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#313">›Palazzo
                  Nari‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#314">Dei ›Santi
                  Benedetto e Scolastica‹, e sua Confraternita
                  all'›Arco della Ciambella‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#315">Di
                  ›Sant'Andrea della Valle‹, e Convento dei Padri
                  Teatini</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#316">Chiesa di
                  ›Sant'Andrea della Valle‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#324">›Palazzo
                  Della Valle‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#325">Di ›Santa
                  Maria in Monterone‹, e Convento dei Padri Scalzi
                  Italiani del Riscatto</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#325">Del
                  ›Santissimo Sudario di Nostro Signore Gesù
                  Cristo‹, e dell'Archiconfraternita dei
                  Savojardi</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#327">Di San
                  Giuliano ai Cesarini, e dello Spedale de'
                  Fiamminghi ›San Giuliano Ospitaliere‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#328">Dei Santi
                  Cosmo e Damiano, e della Confraternita de'
                  Barbieri ›Santi Cosma e Damiano‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#329">Di
                  ›Sant'Elena dei Credenzieri‹, e sua Compagnia</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#329">Dei Santi
                  Niccolò e Biagio alle Calcare, ovvero dei
                  Cesarini ›San Nicola ai Cesarini‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#331">›Teatro
                  Argentina‹</a>
                </li>
                <li>
                  <a href="dg4503670a12s.html#331">›Teatro
                  Valle‹</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
