<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbat74273460s.html#8">Les beaux arts reduits à un
      même principe</a>
      <ul>
        <li>
          <a href="ghbat74273460s.html#10">A Monseigneur Le
          Dauphin</a>
        </li>
        <li>
          <a href="ghbat74273460s.html#14">Avant-Propos</a>
        </li>
        <li>
          <a href="ghbat74273460s.html#27">Table des
          chapitres</a>
        </li>
        <li>
          <a href="ghbat74273460s.html#32">Les beaux arts reduits
          a un principe</a>
          <ul>
            <li>
              <a href="ghbat74273460s.html#32">premier partie</a>
            </li>
            <li>
              <a href="ghbat74273460s.html#82">seconde partie</a>
            </li>
            <li>
              <a href="ghbat74273460s.html#164">troisieme
              partie</a>
            </li>
            <li>
              <a href="ghbat74273460s.html#323">Table des
              matieres</a>
            </li>
            <li>
              <a href="ghbat74273460s.html#336">Explication du
              frontispice et des vignettes&#160;</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
