<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbio48551490s.html#8">Della nobilissima pittura,
      et della sua arte, del modo, &amp; della dottrina, di
      conseguirla, agevolmente et presto</a>
      <ul>
        <li>
          <a href="ghbio48551490s.html#10">Agli eccellentissimi
          pittori di tutta l'Europa Michelangelo Biondo&#160;</a>
        </li>
        <li>
          <a href="ghbio48551490s.html#13">L'indice del libro</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
