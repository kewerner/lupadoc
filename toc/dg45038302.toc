<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg45038302s.html#6">La ville de Rome</a>
      <ul>
        <li>
          <a href="dg45038302s.html#8">Table des titres</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45038302s.html#12">IX. Quartier de la Pigna</a>
      <ul>
        <li>
          <a href="dg45038302s.html#11">Pinea Regio IX Romana
          qualis erat anno 1777 ◉</a>
          <ul>
            <li>
              <a href="dg45038302s.html#11">Collegium Romanum
              ›Collegio Romano‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">›Palazzo Doria
              Pamphilj ‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">›Santa Maria in Via
              Lata‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">Via Cursus ›Via del
              Corso‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">›Sant'Ignazio‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">›Piazza Venezia‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">›Palazzo di Venezia‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">›San Marco‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">›Palazzo Altieri‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">›Santa Maria sopra
              Minerva‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">›San Nicola ai
              Cesarini‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">Santa Clara ›Santa
              Chiara‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">Rotunda ›Pantheon‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">Palazzo Rinuccini
              ›Palazzo Bonaparte‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#11">›Santissimo Nome di
              Gesù‹ ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038302s.html#12">Ou sont l'Eglise de la
          Rotonde ›Pantheon‹, le College Romain ›Collegio Romano‹,
          et l'Eglise du Jesus ›Santissimo Nome di Gesù‹</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45038302s.html#37">X. Quartier du Capitole</a>
      <ul>
        <li>
          <a href="dg45038302s.html#38">Capitolium Regio X Romana
          qualis erat in anno 1776 ◉</a>
          <ul>
            <li>
              <a href="dg45038302s.html#38">›Porta San
              Sebastiano‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#38">›Porta Latina‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#38">›Terme di Caracalla‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#38">Santo Stefano
              Rotondo‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#38">›Santi Giovanni e
              Paolo‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#38">Capitolium
              ›Campidoglio‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#38">›Santa Maria in
              Aracoeli‹ ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038302s.html#40">I. Partie Occidentale du
          X Quartier, ou sont le Capitole ›Campidoglio‹, le mont
          Palatin ›Palatino‹, et l'Eglise de Sainte Marie in
          Campitelli ›Santa Maria in Campitelli‹</a>
        </li>
        <li>
          <a href="dg45038302s.html#66">II. Partie Orientale du X
          Quartier, ou sont le Colisee ›Colosseo‹, l'Eglise de
          Saint Greogoire le Grand ›San Gregorio Magno‹, et la
          Villa Mattei ›Villa Celimontana‹</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45038302s.html#80">Quartier de Saint Ange</a>
      <ul>
        <li>
          <a href="dg45038302s.html#79">Sancti Angeli Regio XI
          Romana qualis erat anno 1777 ◉</a>
          <ul>
            <li>
              <a href="dg45038302s.html#79">Sant'Ambrogio della
              Massima‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">Judaourm domicilia
              ›Ghetto‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">›Sant'Angelo in
              Pescheria‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">Theatrum Marcelli
              ›Teatro di Marcello‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">›Palazzo Orsini‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">›Piazza Lovatelli‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">›Piazza Mattei‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">›Palazzo Mattei di
              Giove‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">›Santa Caterina dei
              Funari‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">›Santo Stanislao dei
              Polacchi‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">›Piazza Paganica‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#79">›San Sebastiano de'
              Mercanti‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#80">Ou sont les restes du
              Theatre de Marcellus ›Teatro di Marcello‹, le Palais
              Mattei ›Palazzo Mattei di Giove‹, et la Juiverie
              ›Ghetto‹</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45038302s.html#87">Quartier de la Ripa</a>
      <ul>
        <li>
          <a href="dg45038302s.html#87">Ou sont l'Eglise de
          Sainte Marie in Cosmedin ›Santa Maria in Cosmedin‹,
          l'Isle Saint Barthelemi ›Isola Tiberina‹, et les ruines
          des Thermes de Caracalla ›Terme di Caracalla‹</a>
        </li>
        <li>
          <a href="dg45038302s.html#88">Transtiberina Regio XIII
          qualis erat in anno 1777 ◉</a>
          <ul>
            <li>
              <a href="dg45038302s.html#88">›Terme di Caracalla‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›San Sisto Vecchio‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›San Cesareo de
              Appia‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›Santa Balbina‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›San Saba‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›Porta San Paolo‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›Piramide di Caio
              Cestio‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›Testaccio (monte)‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›Santa Prisca‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›Sant'Alessio‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›Santa Sabina‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">Sant'Anastasio
              ›Sant'Anastasia‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›San Giorgio in
              Velabro‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›Ponte Rotto‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">Ponte di San
              Bartolomeo ›Ponte Cestio‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#88">›Porta San
              Sebastiano‹ ◉</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45038302s.html#115">XIII. Quartier de
      Transtevere ›Trastevere‹</a>
      <ul>
        <li>
          <a href="dg45038302s.html#115">Ou sont le Port de
          Ripa-Grande ›Porto di Ripa Grande‹, l'Eglise de Sainte
          Marie in Trastevere ›Santa Maria in Trastevere‹, et le
          Palais Corsini ›Palazzo Corsini‹</a>
        </li>
        <li>
          <a href="dg45038302s.html#116">Transtiberina Regio XIII
          Romana qualis erat anno 1777 ◉</a>
          <ul>
            <li>
              <a href="dg45038302s.html#116">›Porta Portuensis‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#116">›Porto di Ripa
              Grande‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#116">›Villa Farnesina‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#116">›Palazzo Corsini‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#116">›Palazzo Salviati‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#116">Porta Sancti
              Pancratii ›Porta San Pancrazio‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#116">›Ponte Rotto‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#116">Pons Sixtus ›Ponte
              Sisto‹ ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038302s.html#118">Article I. Partie
          Meridionale du XIII Quartier, ou sont le Port de
          Ripa-Grande ›Porto di Ripa Grande‹, l'Eglise de Sainte
          Marie in Trastevere ›Santa Maria in Trastevere‹, et celle
          de Sainte Cecile ›Santa Cecilia‹</a>
        </li>
        <li>
          <a href="dg45038302s.html#139">Article II. Partie
          Septentrionale du XIII Quartier, ou sont le Palais
          Corsini ›Palazzo Corsini‹, la Farnesine ›Villa
          Farnesina‹, et le Palais Salviati ›Palazzo Salviati‹</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45038302s.html#151">XIV. Quartier du Bourg
      ›Borgo‹, ou du Vatican ›Vaticano‹</a>
      <ul>
        <li>
          <a href="dg45038302s.html#151">Ou sont la Basilique de
          Saint Pierre ›San Pietro in Vaticano‹, le Palais Vatican
          ›Palazzo Apostolico Vaticano›, et le Chateau Saint Ange
          ›Castel Sant'Angelo‹</a>
        </li>
        <li>
          <a href="dg45038302s.html#152">Vaticanum Suburbium
          Regio XIV. Romana qualis erat anno 1782 ◉</a>
          <ul>
            <li>
              <a href="dg45038302s.html#152">›Ponte Sant'Angelo‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#152">›Castel Sant'Angelo‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#152">›Porta Castello‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#152">›Porta Angelica‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#152">Mons Vaticanus
              ›Vaticano‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#152">›Piazza San Pietro‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#152">Palatium Pontificium
              ›Palazzo Apostolico Vaticano‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#152">›Porta Santo
              Spirito‹ ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#152">›Porta Cavalleggeri‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#152">›Porta Fabbrica‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038302s.html#152">›Villa Barberini
              (2)‹ ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038302s.html#154">I. Partie Orientale de
          XIV Quartier, ou sont le Chateau Saint Ange ›Castel
          Sant'Angelo‹, l'Eglise de Notre Dame de la Transpontine
          ›Santa Maria in Traspontina‹, et le grand Hopital du
          Saint Esprit ›Ospedale di Santo Spirito in Sassia‹</a>
        </li>
        <li>
          <a href="dg45038302s.html#165">II. Partie Occidentale
          du XIV Quartier, ou sont la Basilique de Saint Pierre
          ›San Pietro in Vaticano‹, le Palais du Vatican ›Palazzo
          Apostolico Vaticano‹ et celui de l'Inquisition ›Palazzo
          del Sant'Uffizio‹</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45038302s.html#215">[Tables]</a>
      <ul>
        <li>
          <a href="dg45038302s.html#215">Table des Planches</a>
        </li>
        <li>
          <a href="dg45038302s.html#216">Table generale des
          matieres</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
