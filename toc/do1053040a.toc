<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="do1053040as.html#6">Raccolta di statue antiche e
      moderne data in luce sotto i gloriosi auspicj della Santita
      di N. S. Papa Clemente XI. da Domenico de Rossi illustrata
      colle sposizioni a ciascheduna immagine di Pauolo Alessandro
      Maffei Patrizio volterrano e Cav. dell'ordine di S. Stefano e
      della guardia pontificia</a>
      <ul>
        <li>
          <a href="do1053040as.html#11">All'erudito lettore</a>
        </li>
        <li>
          <a href="do1053040as.html#23">Indice delle statue</a>
        </li>
        <li>
          <a href="do1053040as.html#25">Sposizioni sopra le
          statue coll'indice delle materie, che nella presente
          opera si contengono</a>
        </li>
        <li>
          <a href="do1053040as.html#103">Esposizione del
          frontespizio e degli altri ornamenti della presente
          opera</a>
          <ul>
            <li>
              <a href="do1053040as.html#103">Discorso
              I.&#160;</a>
            </li>
            <li>
              <a href="do1053040as.html#104">Discorso II.</a>
            </li>
            <li>
              <a href="do1053040as.html#106">Discorso III.</a>
            </li>
            <li>
              <a href="do1053040as.html#110">Discorso IV.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="do1053040as.html#113">Indice delle
          materie&#160;</a>
        </li>
        <li>
          <a href="do1053040as.html#127">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
