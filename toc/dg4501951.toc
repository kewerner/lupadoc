<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501951s.html#6">Le cose maravigliose dell'alma
      città di Roma ▣</a>
      <ul>
        <li>
          <a href="dg4501951s.html#8">Al Signore Nostro Papa Sisto
          Quinto</a>
        </li>
        <li>
          <a href="dg4501951s.html#10">Sixtus Papa Quintus</a>
        </li>
        <li>
          <a href="dg4501951s.html#14">[Componimento in versi]
          Girolamo Franmcino sopra le Piramidi, et Obelischi, overo
          Guglie, che sono in Roma</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501951s.html#16">Le Sette chiese principali</a>
      <ul>
        <li>
          <a href="dg4501951s.html#15">[Stampa] ▣</a>
        </li>
        <li>
          <a href="dg4501951s.html#16">›San Giovanni in Laterano‹
          ▣</a>
          <ul>
            <li>
              <a href="dg4501951s.html#20">›Obelisco Lateranense‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#21">›San Pietro in Vaticano‹
          ▣</a>
          <ul>
            <li>
              <a href="dg4501951s.html#24">›Obelisco Vaticano‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#25">›San Paolo fuori le Mura‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501951s.html#26">›Santa Maria Maggiore‹
          ▣</a>
          <ul>
            <li>
              <a href="dg4501951s.html#28">›Obelisco
              Esquilino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#29">›San Lorenzo fuori le Mura‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501951s.html#30">›San Sebastiano fuori le
          Mura‹ ▣</a>
        </li>
        <li>
          <a href="dg4501951s.html#31">›Santa Croce in
          Gerusalemme‹ ▣</a>
        </li>
        <li>
          <a href="dg4501951s.html#33">›Santa Maria del Popolo‹
          ▣</a>
          <ul>
            <li>
              <a href="dg4501951s.html#39">›Obelisco Flaminio‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#39">Nell'Isola</a>
          <ul>
            <li>
              <a href="dg4501951s.html#39">›San Giovanni Calibita‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#40">›San Bartolomeo
              all'Isola‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#41">In Trastevere</a>
          <ul>
            <li>
              <a href="dg4501951s.html#41">›Santa Maria dell'Orto‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#42">›Santa Cecilia in
              Trastevere‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#43">›San Crisogono‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#44">›Santa Maria in
              Trastevere‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#46">›San Francesco a Ripa‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#46">›San Cosimato‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#47">›San Pietro in
              Montorio‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4501951s.html#48">[Stampa] ▣</a>
                </li>
                <li>
                  <a href="dg4501951s.html#49">[Stampa] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501951s.html#50">›San Pancrazio‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#51">›Sant'Onofrio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#53">Nel Borgo</a>
          <ul>
            <li>
              <a href="dg4501951s.html#53">›Santo Spirito in
              Sassia‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#55">›Sant'Angelo al
              Corridoio‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#56">Santa Maria in Campo
              Santo ›Santa Maria della Pietà‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#56">Santo Stefano degli
              Indiani ›Santo Stefano degli Abissini‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#56">›Sant'Egidio a
              Borgo‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#56">›San Lazzaro‹ e Marta e
              Maddallena</a>
            </li>
            <li>
              <a href="dg4501951s.html#56">›Santa Caterina delle
              Cavalleròtte‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#57">›San Giacomo
              Scossacavalli‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#58">›Santa Maria in
              Traspontina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#59">Dalla Porta Flaminia fuori
          del Popolo, sino alle radici del Campidoglio</a>
          <ul>
            <li>
              <a href="dg4501951s.html#59">›Santa Maria dei
              Miracoli‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#60">›Trinità dei Monti‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#61">›San Giacomo in
              Augusta‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#61">La Chiesa di
              Sant'Ambrogio ›Santi Ambrogio e Carlo al Corso‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#62">›Sant'Atanasio‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#63">›San Rocco‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#63">›San Girolamo degli
              Illirici‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#64">›San Lorenzo in Lucina‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#65">›San Silvestro al
              Quirinale‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#65">›San Giovanni in
              Capite‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#66">›Sant'Andrea delle
              Fratte‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#66">La Chiesa delle
              Convertite ›Monastero di Santa Maria Maddalena‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#66">›Santi Apostoli‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#67">›San Marcello al Corso‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#68">›Santa Maria in Via
              Lata‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#69">›San Marco‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#70">›Santa Maria di Loreto‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#71">›Chiesa della Madonna
              dei Monti‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#73">La chiesa del Giesu
              ›Santissimo Nome di Gesù‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#78">›Santa Maria sopra
              Minerva‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4501951s.html#79">Il Christo di
                  ›Santa Maria sopra Minerva‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501951s.html#80">›Pantheon‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#81">›Santa Maria Maddalena‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#82">›Santa Maria della
              Concezione in Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#82">›Santa Maria in
              Aquiro‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#83">›San Macuto‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#84">Chiesa dell'Annunciata
              del Giesu ›Sant'Ignazio‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#85">›Sant'Eustachio‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#85">›San Luigi dei
              Francesi‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#86">›Sant'Agostino‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#87">›San Trifone in
              Posterula‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#87">›Sant'Antonio dei
              Portoghesi‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#88">›Sant'Apollinare‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#89">La chiesa di San Iacomo
              degli Spagnuoli ›Nostra Signora del Sacro Cuore‹
              ▣</a>
              <ul>
                <li>
                  <a href="dg4501951s.html#90">[Stampa] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501951s.html#91">›Santa Maria
              dell'Anima‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#92">›Santa Maria della
              Pace‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#95">›San Tommaso in
              Parione‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#96">›San Salvatore in
              Lauro‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#97">›San Giovanni dei
              Fiorentini‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#97">‹San Biagio della
              Pagnotta‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#98">La Chiesa de i Santi
              Faustino, e Iovita ›Sant'Anna dei Bresciani‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#100">La Chiesa di Santa
              Lucia della Chiavica, nel Rione di Ponte ›Santa Lucia
              del Gonfalone‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#100">›Santa Maria
              dell'Orazione e Morte‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#100">›San Girolamo della
              Carità‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#100">La Chiesa, chiamata
              Casa Santa ›Santa Brigida‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#101">›San Lorenzo in
              Damaso‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#102">›Santa Barbara dei
              Librai‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#102">›San Martinello‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#102">San Benedetto appresso
              la Regola ›Santissima Trinità dei Pellegrini‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#102">›Santa Maria in
              Monticelli‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#102">›Santi Vincenzo ed
              Anastasio alla Regola‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#102">Santa Caterina
              appresso Corte Savella ›Santa Caterina della
              Rota‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#103">›San Tommaso di
              Canterbury‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#103">›Sant'Andrea de
              Azanesi o Nazareno‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#103">›Santa Caterina di
              Siena‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#103">›Santa Maria in
              Monserrato‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#104">La chiesa di Sant'Alò
              ›Sant'Eligio degli Orefici‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#104">La chiesa di Santo
              Stefano ›San Bartolomeo dei Vaccinari‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#104">›Santi Celso e
              Giuliano‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#104">La Chiesa di San
              Biagio ›Santa Maria dei Calderari‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#104">›Santa Maria del
              Pianto‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#105">›Santa Caterina dei
              Funari‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#106">›Sant'Angelo in
              Pescheria‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#107">›San Nicola in
              Carcere‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#108">›Santa Maria in
              Aracoeli‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#109">Dal Campidoglio à man
          sinistra, verso li Monti</a>
          <ul>
            <li>
              <a href="dg4501951s.html#109">›San Pietro in
              Carcere‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#110">›Sant'Adriano al Foro
              Romano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#110">›San Lorenzo in
              Miranda‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#111">›Santi Cosma e
              Damiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#112">Santa Maria Nova
              ›Santa Francesca Romana‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#113">›San Clemente‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#114">›Santi Quattro
              Coronati‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#115">›Santi Marcellino e
              Pietro‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#116">›San Matteo in
              Merulana‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#116">›San Pietro in
              Vincoli‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#117">›San Lorenzo in
              Panisperna‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#118">›Sant'Agata dei
              Goti‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#118">›San Lorenzo in Fonte
              ‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#118">›Santa Pudenziana‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#119">San Vito in Macello
              ›Santi Vito e Modesto‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#119">›San Giuliano
              Ospitaliere‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#120">›Sant'Eusebio‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#121">›Santa Bibiana‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#121">›San Martino ai Monti‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#122">›Santa Prassede‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#123">›Sant'Antonio Abate‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#124">›Santi Quirico e
              Giulitta‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#125">›San Silvestro al
              Quirinale ‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#126">›Santa Susanna‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#127">›San Vitale‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#127">›Mausoleo di Santa
              Costanza‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#128">›Santa Maria degli
              Angeli‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#129">Dal Campidoglio à man
          dritta, verso li Monti</a>
          <ul>
            <li>
              <a href="dg4501951s.html#129">›Santa Maria
              Liberatrice‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#130">›Santa Maria della
              Consolazione‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#131">›San Giovanni
              Decollato‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#132">›Sant'Anastasia‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#133">Santa Maria in Portico
              ›Santa Galla‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#134">›San Gregorio Magno‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#135">›Santi Giovanni e
              Paolo‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#136">›Santa Maria in
              Domnica‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#137">›Santo Stefano
              Rotondo‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#138">›San Giorgio in
              Velabro‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#139">›San Sisto Vecchio‹
              ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#140">›Santa Sabina‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#141">›Sant'Alessio‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#142">›Santa Prisca‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#143">›San Saba‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#144">Santa Balbina‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#145">›San Giovanni a Porta
              Latina‹ ▣</a>
            </li>
            <li>
              <a href="dg4501951s.html#146">La chiesa di
              Sant'Anastasio ›Santi Vincenzo ed Anastasio alle Tre
              Fontane‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#146">›Santa Maria in Scala
              Coeli‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#146">›Annunziata‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#147">›Santa Maria in
              Via‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#148">Tavola delle chiese di
          Roma, che sono in questo libro</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501951s.html#151">Le stationi, che sono nelle
      chiese di Roma, sì per la Quadragesima, come per tutto
      l'Anno, con le solite Indulgentie</a>
      <ul>
        <li>
          <a href="dg4501951s.html#159">Le stationi
          dell'advento</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501951s.html#161">La guida romana per li
      forastieri, che vengono per vedere le Antichità di Roma, à
      una per una in bellissima forma, et brevità</a>
      <ul>
        <li>
          <a href="dg4501951s.html#161">Giornata prima</a>
          <ul>
            <li>
              <a href="dg4501951s.html#161">Del Borgo</a>
            </li>
            <li>
              <a href="dg4501951s.html#162">Del ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#162">Dell'›Isola
              Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#163">Del Ponte Santa Maria
              ›Ponte Rotto‹, del palazzo di Pilato, et d'altre
              cose</a>
            </li>
            <li>
              <a href="dg4501951s.html#163">De Monte Testaccio
              ›Testaccio (monte)‹, et di molte altre cose</a>
            </li>
            <li>
              <a href="dg4501951s.html#164">Delle Therme Antoniane
              ›Terme di Caracalla‹, et altre cose</a>
            </li>
            <li>
              <a href="dg4501951s.html#164">Di ›San Giovanni in
              Laterano‹, ›Santa Croce in Gerusalemme‹, et altri</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#165">Giornata seconda</a>
          <ul>
            <li>
              <a href="dg4501951s.html#165">Della ›Porta del
              Popolo‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#166">Dei cavalli di marmo,
              che stanno à monte Cavallo ›Fontana di Monte
              Cavallo‹, et delle Therme Diocletiane ›Terme di
              Diocleziano‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#167">Della strada Pia ›Via
              XX settembre‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#168">Della ›Porta Pia‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#168">Di Sant'Agnese, et
              altre anticaglie ›Sant'Agnese fuori le Mura‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#168">Del tempio d'Iside
              ›Tempio di Isis Patricia‹, et altre cose</a>
            </li>
            <li>
              <a href="dg4501951s.html#169">Delle ›Sette Sale‹,
              del ›Colosseo‹, et altre cose</a>
            </li>
            <li>
              <a href="dg4501951s.html#169">Del tempio della Pace
              ›Foro della Pace‹, et del monte ›Palatino‹ hora detto
              Palazzo maggiore, et altre cose</a>
            </li>
            <li>
              <a href="dg4501951s.html#170">Del ›Campidoglio‹, et
              altre cose</a>
            </li>
            <li>
              <a href="dg4501951s.html#171">Dei Portichi d'Ottavio
              [sic], di Settimio ›Portico d'Ottavia‹, e ›Teatro di
              Pompeo‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#171">Giornata terza</a>
          <ul>
            <li>
              <a href="dg4501951s.html#171">Delle due Colonne, una
              di Antonino Pio ›Colonna di Antonino Pio‹, e l'altra
              di Traiano ›Colonna Traiana‹, et altre cose</a>
            </li>
            <li>
              <a href="dg4501951s.html#172">Della Rotonda, overo
              ›Pantheon‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#172">De i Bagni di Agrippa
              ›Terme di Agrippa‹, et di Nerone ›Terme
              Neroniano-Alessandrine‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#172">Della ›Piazza Navona›,
              et di Mastro Pasquino ›Pasquino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#174">Indice brevissimo dei
          Pontefici Romani</a>
        </li>
        <li>
          <a href="dg4501951s.html#195">Reges, et Imperatores
          Romae</a>
        </li>
        <li>
          <a href="dg4501951s.html#199">Li Re di Francia</a>
        </li>
        <li>
          <a href="dg4501951s.html#200">Li Re del Regno di
          Napoli</a>
        </li>
        <li>
          <a href="dg4501951s.html#201">Li Dogi di Venetia</a>
        </li>
        <li>
          <a href="dg4501951s.html#203">Li Duchi di Milano</a>
        </li>
        <li>
          <a href="dg4501951s.html#204">Le Parochie dell'alma
          città di Roma che sono al numero di 108</a>
        </li>
        <li>
          <a href="dg4501951s.html#208">Compagnie, che sono
          nell'alma città di Roma</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501951s.html#212">L'antichità di Roma ▣</a>
      <ul>
        <li>
          <a href="dg4501951s.html#213">Alli lettori</a>
        </li>
        <li>
          <a href="dg4501951s.html#214">Libro I</a>
          <ul>
            <li>
              <a href="dg4501951s.html#214">Dell'edificatione di
              Roma</a>
            </li>
            <li>
              <a href="dg4501951s.html#216">Del Circuito di
              Roma</a>
            </li>
            <li>
              <a href="dg4501951s.html#217">Delle Porte</a>
            </li>
            <li>
              <a href="dg4501951s.html#218">Delle Vie</a>
            </li>
            <li>
              <a href="dg4501951s.html#219">Delli Ponti, che sono
              sopra il Tevere, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501951s.html#220">Dell'Isola del Tevere
              ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#220">Delli Monti</a>
            </li>
            <li>
              <a href="dg4501951s.html#221">Del Monte ›Testaccio
              (monte)‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#222">Delle acque, et chi le
              condusse in Roma</a>
            </li>
            <li>
              <a href="dg4501951s.html#222">Delli Acquedotti</a>
            </li>
            <li>
              <a href="dg4501951s.html#223">Della ›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#223">Delle ›Sette Sale‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#224">Delle Therme, cioè
              Bagni, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501951s.html#225">Delle Naumachie, dove
              si facevano le battaglie navali, et che cosa
              erano‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#225">Dei Cerchi, et che
              cosa erano‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#226">De i Theatri, et che
              cosa erano, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501951s.html#226">De gli Anfiteatri, et
              suoi edificatori, et che cosa erano</a>
            </li>
            <li>
              <a href="dg4501951s.html#227">De' Fori cioè
              Piazze</a>
            </li>
            <li>
              <a href="dg4501951s.html#228">Delli Archi trionfali,
              et a chi si davano</a>
            </li>
            <li>
              <a href="dg4501951s.html#228">Delli Portichi</a>
            </li>
            <li>
              <a href="dg4501951s.html#229">De i Trofei, et
              Colonne memorande</a>
            </li>
            <li>
              <a href="dg4501951s.html#229">Delli Colossi</a>
            </li>
            <li>
              <a href="dg4501951s.html#230">Delle Piramidi</a>
            </li>
            <li>
              <a href="dg4501951s.html#230">Delle Mete</a>
            </li>
            <li>
              <a href="dg4501951s.html#230">Dell'Obelischi, overi
              Aguglie</a>
            </li>
            <li>
              <a href="dg4501951s.html#231">Delle Statue</a>
            </li>
            <li>
              <a href="dg4501951s.html#231">›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#231">Dell Cavalli</a>
            </li>
            <li>
              <a href="dg4501951s.html#232">Delle Librarie</a>
            </li>
            <li>
              <a href="dg4501951s.html#232">Delli Horiuoli</a>
            </li>
            <li>
              <a href="dg4501951s.html#232">De i Palazzi</a>
            </li>
            <li>
              <a href="dg4501951s.html#233">›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#233">Dell'altre Case de'
              Cittadini</a>
            </li>
            <li>
              <a href="dg4501951s.html#234">Delle Curie, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501951s.html#234">De' Senatuli, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501951s.html#234">De' Magistrati</a>
            </li>
            <li>
              <a href="dg4501951s.html#235">De i Comitij, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501951s.html#236">Delle Tribu</a>
            </li>
            <li>
              <a href="dg4501951s.html#236">Delle Regioni, cioè
              Rioni, et sue insegne ›Regiones Quattuordecim‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#236">Delle Basiliche, et
              che cosa erano</a>
            </li>
            <li>
              <a href="dg4501951s.html#236">Del ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#237">Dello Erario ›Erario
              militare, o del Campidoglio‹, cioè Camera del
              commune, et che moneta si spendeva in Roma in quei
              tempi</a>
            </li>
            <li>
              <a href="dg4501951s.html#238">Del ›Grecostasi‹, et
              che cosa era</a>
            </li>
            <li>
              <a href="dg4501951s.html#238">Della Secretaria del
              Popolo Romano</a>
            </li>
            <li>
              <a href="dg4501951s.html#238">›Asilo di Romolo‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#239">Delli ›Rostri‹, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501951s.html#239">Della Colonna, detta
              Miliario ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#239">Del Tempio di Carmenta
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#239">Della ›Columna
              Bellica‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#239">Della ›Columna
              Lactaria‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#240">›Equimelio‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#240">›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#240">›Tigillum
              Sororium‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#240">De' Campi Forastieri
              ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#240">›Villa publica‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#241">›Taberna
              Meritoria‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#241">›Vivarium‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#241">Delli Horti</a>
            </li>
            <li>
              <a href="dg4501951s.html#242">›Velabro‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#242">›Carinae‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#243">Delli Clivi</a>
            </li>
            <li>
              <a href="dg4501951s.html#243">De' Prati</a>
            </li>
            <li>
              <a href="dg4501951s.html#243">De i Granari
              publici</a>
            </li>
            <li>
              <a href="dg4501951s.html#243">Delle Carceri
              publiche</a>
            </li>
            <li>
              <a href="dg4501951s.html#244">Di alcune feste, et
              giuochi, che si solevano celebrare in Roma</a>
            </li>
            <li>
              <a href="dg4501951s.html#244">Del Sepolcro di
              Augusto, et d'Adriano, et di Settimio ›Mausoleo di
              Augusto‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#245">De i Tempij</a>
            </li>
            <li>
              <a href="dg4501951s.html#246">De Sacerdoti delle
              Vergini Vestali, vestimenti, vasi et altri
              instrumenti fatti per uso delli sacrifici, et suoi
              institutori</a>
            </li>
            <li>
              <a href="dg4501951s.html#248">Dell'Armamentario, et
              che cosa era</a>
            </li>
            <li>
              <a href="dg4501951s.html#248">Dell'essercito (sic!)
              Romano di terra, e di mare, et di lor'insegne</a>
            </li>
            <li>
              <a href="dg4501951s.html#248">De trionfi, et a chi
              si concedevano, et chi fu il primo trionfatore, et di
              quante maniere erano</a>
            </li>
            <li>
              <a href="dg4501951s.html#249">Delle Corone, et a chi
              si davano</a>
            </li>
            <li>
              <a href="dg4501951s.html#250">Del numero del Popolo
              Romano</a>
            </li>
            <li>
              <a href="dg4501951s.html#250">Delle ricchezze del
              Popolo Romano</a>
            </li>
            <li>
              <a href="dg4501951s.html#250">Della liberalità de
              gli Antichi Romani</a>
            </li>
            <li>
              <a href="dg4501951s.html#251">Delli matrimonij
              antichi, et loro usanza</a>
            </li>
            <li>
              <a href="dg4501951s.html#251">Della buona creanza,
              che davano à'figliuoli</a>
            </li>
            <li>
              <a href="dg4501951s.html#252">Della separatione de'
              Matrimoni</a>
            </li>
            <li>
              <a href="dg4501951s.html#252">Dell'essquie (sic!)
              antiche, et sue cerimonie</a>
            </li>
            <li>
              <a href="dg4501951s.html#253">Delle Torri</a>
            </li>
            <li>
              <a href="dg4501951s.html#253">Del ›Tevere‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#254">Del Palazzo
              Papale›Palazzo Apostolico Vaticano‹, et di Belvedere
              &#160;›Cortile del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#255">›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4501951s.html#255">Recapitulatione
              dell'Antichità</a>
            </li>
            <li>
              <a href="dg4501951s.html#256">De i Tempij de gli
              antichi, fuori di Roma</a>
            </li>
            <li>
              <a href="dg4501951s.html#257">Quante volte è stata
              presa Roma</a>
            </li>
            <li>
              <a href="dg4501951s.html#258">De' fuochi de gli
              antichi, scritti da pochi Autori, cavati da alcuni
              fragmenti d'Historie</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501951s.html#261">Tavola</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
