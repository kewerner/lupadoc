<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg53630201s.html#6">Sopra gli Ornamenti di Porte e
      Finestre tratti da alcune Fabbriche insigni di Roma: con le
      Misure Piante Modini, e Profili</a>
      <ul>
        <li>
          <a href="dg53630201s.html#8">Dedica a San Clemente XI.
          ▣</a>
        </li>
        <li>
          <a href="dg53630201s.html#10">A gli Amatori delle Belle
          Arti Domenico de Rossi.</a>
        </li>
        <li>
          <a href="dg53630201s.html#12">Palazzo de' Signori
          Conservatori in Campidoglio ›Palazzo dei
          Conservatori‹</a>
          <ul>
            <li>
              <a href="dg53630201s.html#12">Pianta del Portico
              esteriore ◉</a>
            </li>
            <li>
              <a href="dg53630201s.html#14">Portico nella
              facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#16">Finestrone di mezzo
              della facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#18">Altra finesta della
              facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#20">Porta principale nel
              Portico della facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#23">Spaccato della metà
              del Portico interiore ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#26">Prospetto del portico
              interiore ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#28">Finestra nel Cortile
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#30">[›San Pietro in
          Vaticano‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#30">Parte del prospetto
              esteriore del fianco ◉</a>
            </li>
            <li>
              <a href="dg53630201s.html#32">Nicchia della
              medesima parte di prospetto con il suo ornato
              contrasegnata in esso con la lettera A. ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#34">Finestra con li suoi
              ornamenti ivi segnata con la lettera B. ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#38">Modini degli ornati
              intorno alla nicchia contrasegnata con la lettera
              A/Modini degl' ornati della finestra segnata B/Modini
              dell'Architrave Fregio, e Cornice [...] ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#40">Altra finestra
              segnata nel medesimo prospetto con la lettera C.
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#42">Finestra della
              facciata del Tempio Vaticano nel secondo ordine ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#45">Parte di prospetto
              interiore della Basilica Vaticana [...] Modini del
              prospetto [...] ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#48">Porta nella Cappella
              del Santissimo Crocifisso della Basilica Vaticana
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#50">Pianta della metà del
              Cortile [...] ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#52">[›Palazzo Farnese‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#52">Prospetto del primo
              ordine, overo Portico esteriore nel cortile del
              &#160;›Palazzo Farnese‹ ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#54">Dimostrationi delli
              Modini del antecedente foglio del Portico nel primo
              ordine ◉</a>
            </li>
            <li>
              <a href="dg53630201s.html#56">Secondo ordine del
              Portico esteriore nel cortile ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#58">Dimostrationi delli
              Modini del secondo ordine del Portico nel cortile
              ◉</a>
            </li>
            <li>
              <a href="dg53630201s.html#60">Terzo ordine del
              Portico esteriore nel cortile ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#62">Modini
              dell'antecedente finestra del terzo ordine ◉</a>
            </li>
            <li>
              <a href="dg53630201s.html#64">Modini della Cornice
              del terzo ordine ◉</a>
            </li>
            <li>
              <a href="dg53630201s.html#66">Prospetto della
              finestra del terzo ordine nella facciata ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#68">[›Porta Pia‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#68">Porta Pia, o Viminale
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#70">Finestra con
              Mezzanino laterali alla Porta Pia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#72">[›Palazzo Caffarelli
          Vidoni‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#72">Parte del Prospetto
              del Palazzo de' Signori Caffarelli alla Valle ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#74">[›Palazzo Barberini‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#74">Parte del Prospetto
              della facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#76">Porta principale del
              portico del pian terreno della facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#78">Portico che fa
              Finesta nella facciata del piano nobile, o sia
              secondo piano ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#80">Portico che fa
              finestra del terzo piano della facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#82">Finestra del piano
              terreno della facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#84">Finestra del piano
              nobile, o sia secondo piano della facciata
              (Seitenauflösung) ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#86">Finestra del terzo
              piano della facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#88">Scala a Lumaca/Modini
              delli ornati della detta scala ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#90">Spaccato di dentro
              della detta Scala ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#92">Pianta del piano di
              cima di detta Scala/Pianta del Piano terreno di detta
              scala nel ingresso del portico ◉</a>
            </li>
            <li>
              <a href="dg53630201s.html#94">Porta nel ripiano
              della scala che da l'ingresso nella sala ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#96">Porta nella sala del
              Palazzo Barberino che conduce nelli Appartamenti
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#98">Altra Porta nella
              medesima sala ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#100">Camino nella sala
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#102">Finestra del Palazzo
              Barberino della facciata che guarda il Giardino ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#104">Finetra [sic!] del
              Palazzo Barberino della facciata che guarda il
              Giardino ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#106">Porta del Giardino
              del Palazzo ▣ ?</a>
            </li>
            <li>
              <a href="dg53630201s.html#108">Porta del Teatro
              delle Commedie ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#110">[›Palazzo Chigi‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#110">Finestra del terzo
              piano con Cornicione, e Loggia ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#112">Finestra del Paino
              Nobile o sia secondo piano ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#114">Finestra del
              medesimo piano Nobile con diverso frontespizio ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#116">Finestra del piano
              terreno ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#118">Altra Finestra del
              Piano terreno ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#120">[›San Giovanni in
          Laterano‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#120">Parte del prsopetto
              della gran Nave ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#122">Prospetto d'una
              delle Nicchie che fanno ornato nè Lati della Nave
              maggiore ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#125">Finestrone con tutti
              li suoi ornamenti posto sopra la porta principale
              della Nave maggiore ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#129">Prospetto d'una
              delle finestre con li suoi ornamenti del lato destro
              interiore nella Nave maggiore ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#132">Prospetto del
              Finestrone con suoi ornamenti nel mezzo della Nave
              maggiore per fianco ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#134">Porta nella Chiesa
              che conduce nel Palazzo Papale Lateranense ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#136">Fianco
              dell'Antecedente Porta ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#138">Porta della scala
              Lumacca al lato della Porta Santa ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#140">Porta Santa ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#142">[›Collegio di Propaganda
          Fide‹ in ›Piazza di Spagna‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#142">Porta nella facciata
              principale ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#144">Parte del Prospetto
              della facciata laterale ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#146">Porta ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#148">Altra Finestra nel
              primo Ordine della Facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#150">Altra Finestra nel
              primo Ordine della Facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#152">Finestra nel secondo
              Ordine sopra la finestra principale della facciata
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#154">Cornice nella
              facciata/Porta nel piano del Cortile dirincontro alla
              Porta/Porta della Libraria nel portico superiore
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#156">Altra Finestra nel
              Secondo Ordine della facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#158">Altra Finestra nel
              secondi Ordine della facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#160">Porta della Chiesa
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#162">Porta principale per
              entrare nella sala ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#164">Ornato interiore
              della medesima porta principale dentro detta sala
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#166">Altra Porta nella
              medesima sala ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#168">[›Oratorio di San
          Filippo Neri‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#168">Porta nella facciata
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#170">Modini delli Ornati
              dell'Antecedente Porta ◉</a>
            </li>
            <li>
              <a href="dg53630201s.html#172">Finesta sopra la
              Porta della facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#174">Modini
              dell'Antecedente finestra sopra la Porta della
              facciata ◉</a>
            </li>
            <li>
              <a href="dg53630201s.html#176">Altra Finestra
              contigua à l'antecedente nella facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#178">Altra finestra nel
              mezzo di detta Facciata sopra l'Antecedente finestra
              con parapetto che fa Ringhiera ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#180">Modini
              dell'Antecedente finestra ◉</a>
            </li>
            <li>
              <a href="dg53630201s.html#182">Altra Finestra nella
              facciata ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#184">Porta della Casa de
              Padre preposto della congregazione ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#186">Porta dell'Oratorio
              dè Padri della Congregazione di San Filippo Neri
              posta nel Claustro ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#188">Porta della
              Sacrestia della Chiesa Nuova de Padri ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#190">Porta della Sala
              dell'Apartamento dell'Emminenti Signori Cardinali,
              nel Claustro superiore ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#192">Camino nella sala di
              ricreatione ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#195">[›Palazzo della
          Sapienza‹ in ›Piazza di Sant'Eustachio‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#195">Porta del palazzo
              dello studio della Sapienza con finestrone, e
              ringhiera verso la ›Piazza di Sant'Eustachio‹ ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#198">Porta nel Portico
              Superiore nel Palazzo dello studio della Sapienza che
              conduce alli appartamenti superiori ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#200">Porta della Libraria
              nel Portico superiore del Palazzo dello studio della
              Sapienza ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#202">[›Monastero del Borromeo
          alla Chiesa di San Carlo alle Quattro Fontane‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#202">Porta del Convento
              dè Padri del Riscatto della natione spagnola, alle
              quattro fontane ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#204">[›Palazzo
          Giustiniani‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#204">Porta del Palazzo
              del Signore Principe Giustiniani ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#206">Finestre della
              facciata ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#208">[›Sant'Andrea delle
          Fratte‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#208">Porta del Casino del
              Signore Marchese del Bufalo a ›Sant'Andrea delle
              Fratte‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#210">[›Palazzo
          Montecitorio‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#210">Porta principale del
              Palazzo della Curia Innocenziana con le sue porte
              minori Laterali sopra il Monte Citorio ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#212">Finestre del Pian
              Terreno della facciata del palazzo della gran Curia
              Innocentiana ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#214">[›Sant'Andrea al
          Quirinale‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#214">Porta principale di
              dentro la Chiesa di Sant'Andrea del Novitiato de PP.
              Giesuiti ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#216">[›Porta Santo
          Spirito‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#216">Porta del Bastione,
              detta di Santo Spirito alla Lungara ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#218">[›Ospedale di Santo
          Spirito in Sassia‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#218">Porta del'Hospedale
              di Santo Spirito in Sassia, dalla parte della Longara
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#220">Porta Principale
              dell'Hospaedale di Santo Spirito in Sassia ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#222">[›Santi Luca e
          Martina‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#222">Porta della facciata
              della Chiesa di Santa Martina, e San Luca Evangelista
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#224">Porta di dentro
              della Chiesa di Santa Martina, e san Luca ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#226">[Palazzo Gambirasi]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#226">Finestra del piano
              nobile del Palazzo de Signori Gambirasi ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#228">Finestre dell'altra
              facciata con altro ordine di Architettura del
              medesimo Palazzo de Signori Gambirasi ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#230">[›Casamento Colonna di
          Carbognano‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#230">Porta del palazzo dè
              Colonnesi del Signore Duca di Carbognano, in piazza
              di Sciara ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#232">[›Palazzo Pio
          Righetti‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#232">Finestra del piano
              Terreno del Palazzo del Signore Principe Pio ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#234">Finestra del piano
              nobile, o sia secondo piano ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#236">Finestra del terzo
              Piano con il Cornicione ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#238">[›Palazzo Altieri‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#238">Finestra del terzo
              appartamento con Cornicione nella facciata del
              Palazzo del Signor Principe Altieri ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#240">Finestra con
              ornamenti, e Cornicione della facciata nel Cortile
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#242">Porta al piano
              terreno in faccia all'ingresso della scala grande
              ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#244">Porta della sala al
              piano nobile ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#246">Porta della sala del
              terzo piano ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#249">[›Palazzo Grazioli‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#249">Porta del Palazzo de
              Signori Gottifredi nella Piazza di San Marco ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#252">[›Palazzo Pamphilj
          (Piazza Navona)‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#252">Porta del Palazzo
              del Signore Principe Pamfilij, della facciata verso
              la ›Piazza di Pasquino‹ ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#254">Finestra del terzo
              piano con suo cornicione del Palazzo di Santa Agnese
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#256">[›Palazzo del Collegio
          Inglese‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#256">Camino della sala di
              Ricreazione nel Collegio della Nazione Inglese ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#258">[›Palazzo Serlupi
          Crescenzi‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#258">Porta del Palazzo
              del Signore Marchese Crescentij ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#260">Finestra del Piano
              Terreno ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#262">Finestra del piano
              nobile o sia secondo piano ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#264">[›Palazzo Carpegna‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#264">Porta del Palazzo
              del Signore Marchese Baldinotti ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#266">[›Palazzo
          Lancellotti‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#266">Porta del Palazzo
              del Signore Marchese Lancellotti, alli Coronari ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#268">[›Palazzo Baccelli
          Acquari‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#268">Porta del Palazzo de
              Signori Baccelli ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#270">Fenestre della
              facciata ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#272">›Palazzo Nari‹</a>
          <ul>
            <li>
              <a href="dg53630201s.html#272">Porta del Palazzo
              del Signore Giovanni Antonio Neri, in Campo Marzo
              ›Campo Marzio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#274">[›Palazzo d'Aste
          Rinuccini Bonaparte‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#274">Finestra del Piano
              terreno dello Signori D'aste a San Marco/Finestra del
              Piano Nobile ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#276">Finestra del terzo
              piano ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#278">[›Palazzo Origo‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#278">Porta Palazzo de
              Signori Orighi ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#280">[›Palazzo Celsi‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#280">Porta del palazzo de
              Signori Celsi, als Giesù ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#282">[›Palazzo del
          Grillo‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#282">Porta verso la
              Strada principale del Palazzo del Signore Marchese
              del Grillo ▣</a>
            </li>
            <li>
              <a href="dg53630201s.html#284">Altra Porta del
              medesimo Palazzo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg53630201s.html#286">[›Palazzo Balestra già
          Muti Papazzurri‹]</a>
          <ul>
            <li>
              <a href="dg53630201s.html#286">Porta che da
              l'ingresso al Cortile del Palazzo del Signore
              Marchese Muti Papazuri, a Santi Apostoli</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
