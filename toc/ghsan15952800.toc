<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghsan15952800s.html#8">Sculpturae veteris
      admiranda sive delineatio vera perfectissimarum
      eminentissimarumque statuarum, una cum artis hujus
      nobilissimae Theoria, serenissimo ac potentissimo principi ac
      domino. Domino Carolo Comiti Palat. Rheni, S. Rom. Imp.
      Archith., et Electori, Bavariae Duci consecrata a Joachimo de
      Sandrart, in Stockau.</a>
      <ul>
        <li>
          <a href="ghsan15952800s.html#9">Serenissime ac
          potentissime princeps elector.</a>
        </li>
        <li>
          <a href="ghsan15952800s.html#13">Ad lectores.&#160;</a>
        </li>
        <li>
          <a href="ghsan15952800s.html#15">Cap. I. De regulis
          statuariae proludium.</a>
        </li>
        <li>
          <a href="ghsan15952800s.html#17">Cap. II. Statuariae
          definitio:</a>
        </li>
        <li>
          <a href="ghsan15952800s.html#20">Cap. III. De
          Statuariae Formis,</a>
        </li>
        <li>
          <a href="ghsan15952800s.html#22">Cap. IV. et ultimum,
          De planis imaginibus.</a>
          <ul>
            <li>
              <a href="ghsan15952800s.html#26">Virtus Augusti</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#30">Jupiter
              Olympius</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#34">Hercules Hydram
              vincens</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#38">Laocoon</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#42">Antinous</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#46">Faunus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#50">Alexander M. cum
              Bucephalo</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#54">Gladiator</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#58">Arria et
              Paetus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#62">Pasquinus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#66">Hercules</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#70">Hercules, a labore
              quiescens</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#74">Apollo</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#78">Silenus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#82">Centaurus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#86">Venus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#90">Gratiae tres</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#94">Fauni Duo</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#98">Minerva</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#102">Flora</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#106">Sabina</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#110">Sibylla
              Cumana</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#113">Baccha
              Bacchans</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#117">Vespasianus cum
              Domitilla et Alia</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#121">Vasa Antiqua</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#125">L. Annaeus
              Seneca</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#129">Paetus et
              Arria</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#135">Cleopatra</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#139">Cupido</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#143">Meleager</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#147">Galatea</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#151">Belisarius</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#155">Sibylla</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#159">Poesis</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#163">Faunus, Puerum
              amplectens</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#167">Apollo</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#171">Marsyas</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#175">Satyrus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#179">Ceres</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#183">Mercurius</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#187">Rotator</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#191">Pan et Natura</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#195">Aurelius, et L.
              Verus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#199">Marcus
              Aurelius</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#203">Luctatores</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#207">Corydon</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#213">Poenitentia</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#219">Leo et equus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#225">Dirce, Zetys, et
              Amphion</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#229">Antinous</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#235">Venus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#241">Endymion</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#245">Nympha et
              Faunus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#249">Paris et
              Minerva</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#253">Virgo
              Vestalis</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#257">Flora</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#261">Hygiaea</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#265">Nilus, cum
              pyramidibus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#269">Rhetorica</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#273">Marphorius, alias
              Rhenus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#277">Nilus alter</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#281">Silenus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#285">Satyrus et
              Silenus</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#289">Atalanta cum
              Fauno</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#294">Ruina Romae</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#297">Calceamenta
              Varia</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#301">Calceamenta
              similia</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#305">Gladiator
              currens</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#309">Venus Victrix</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#313">Commodus Imp.</a>
            </li>
            <li>
              <a href="ghsan15952800s.html#315">Index
              Statuarum</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
