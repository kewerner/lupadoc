<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502830s.html#6">Viaggio Sagro, e curioso Delle
      Chiese più principali di Roma ove si nota il più bello delle
      Pitture, Scolture, e altri ornamenti</a>
      <ul>
        <li>
          <a href="dg4502830s.html#8">[Dedica dall'autore al
          Monsignor di Gevres] Illustrissimo, e reverendissimo
          Signore</a>
        </li>
        <li>
          <a href="dg4502830s.html#9">[Dedica dall' autore ai
          lettori] Al Lettore</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502830s.html#10">[Text]</a>
      <ul>
        <li>
          <a href="dg4502830s.html#10">Indice</a>
        </li>
        <li>
          <a href="dg4502830s.html#14">Chiesa di San Pietro in
          Vaticano ›San Pietro in Vaticano‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#25">Della Chiesa di San Paolo
          fuor di Roma San Paolo fuori le Mura‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#31">Chiesa de' Santi Vincenzo,
          e Anastasio alle Acque Salvie ›Santi Vincenzo ed
          Anastasio alle Tre Fontane‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#32">Santa Maria detta Scala
          Coeli alle Aque Salvie Santa Maria Scala Coeli‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#32">Di San Paolo, detto alle
          tre Fontane, alle Acque Salvie San Paolo alle Tre
          Fontane‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#34">Di Santa Maria
          dell'Annuntiata detta Annuntiatella ›Santa Maria
          Annunziata‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#38">Santa Maria delle Piante di
          Nostro Signore ›Chiesa del "Domine quo vadis?"‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#42">Della Chiesa di San Sisto
          ›San Sisto Vecchio‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#44">Chuiesa de Santi Nereo, e
          Achilleo ›Santi Nereo e Achilleo‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#45">Chiesa di San Giovanni
          Laterano Basilica Costantiniana; prima di Roma, e del
          Mondo ›San Giovanni in Laterano‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#51">Del Santissimo Salvatore
          alle Scale Sante ›Scala Santa‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#53">Santa Croce in Gierusalemme
          ›Santa Croce in Gerusalemme‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#56">Di San Lorenzo fuor delle
          mura ›San Lorenzo fuori le Mura‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#60">Di Santa Maria Maggiore nel
          Monte Esquilino ›Santa Maria Maggiore‹</a>
        </li>
        <li>
          <a href="dg4502830s.html#66">Viaggio Sagro Delle Chiese
          più singolari di Roma Nelle quali il Corioso forastiere
          scorgere le cose più notabili</a>
          <ul>
            <li>
              <a href="dg4502830s.html#68">San Spirito in Sassia
              ›Santa Tecla (Santo Spirito in Sassia)‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#70">Sant'Honofrio
              ›Sant'Onofrio al Gianicolo‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#71">San Pietro in Montorio
              ›San Pietro in Montorio‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#72">Santa Maria della Scala
              ›Santa Maria della Scala‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#73">Santa Maria in
              Trastevere ›Santa Maria in Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#75">San Francesco a Ripa
              ›San Francesco a Ripa‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#76">San Grisogono ›San
              Crisogono‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#76">Santa Cecilia in
              Trastevere ›Santa Cecilia in Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#77">San Bartolomeo
              all'Isola ›San Bartolomeo all'Isola‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#78">Santa Sabina ›Santa
              Sabina‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#80">San Gregorio nel Monte
              Celio ›San Gregorio Magno‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#82">San Giovanni Battista
              Decollato ›San Giovanni Decollato‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#83">Santa Catarina de
              Funari ›Santa Caterina dei Funari‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#85">San Carlo de' Catinari
              ›San Carlo ai Catinari‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#85">San Girolamo della
              Carità ›San Girolamo della Carità‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#87">Santissima Trinità di
              Ponte Sisto ›Santissima Trinità dei Pellegrini‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#89">San Lorenzo in Damaso
              ›San Lorenzo in Damaso‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#90">Santa Maria in
              Vallicella detta Chiesa nuova ›Chiesa Nuova‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#93">Sant'Agnese in Piazza
              Navona ›Sant'Agnese in Agone‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#94">Sant'Andrea della Valle
              ›Sant'Andrea della Valle‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#96">San Giacomo degli
              Spagnoli ›Nostra Signora del Sacro Cuore‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#97">San Luigi de' Francesi
              ›San Luigi dei Francesi‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#99">Ciesa di Sant'Eustachio
              ›Sant'Eustachio‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#99">La Spaienza di Roma,
              overo luogo che serve per lo studio di Roma
              ›Sant'Ivo‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#101">Sagre Stimmate di San
              Francesco ›Sante Stimmate di San Francesco‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#101">Santa Maria della
              Minerva ›Santa Maria sopra Minerva‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#103">Sant'Ignatio, e
              Collegio Romano ›Sant'Ignazio‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#104">Delle Monache di Santa
              Marta ›Santa Marta (Collegio Romano)‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#104">Il Gesù, e Casa
              Professa della detta Compagnia ›Santissimo Nome di
              Gesù‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#106">Chiesa di San Marco
              ›San Marco‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#106">Santa Maria della
              Consolatione ›Santa Maria della Consolazione‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#107">Aracoeli in
              Campidoglio ›Santa Maria in Aracoeli‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#109">San Pietro in Carcere
              ›San Pietro in Carcere‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#110">Di Santi Martina, e
              Luca ›Santi Luca e Martina‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#111">San Lorenzo in Miranda
              ›San Lorenzo in Miranda‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#112">Santa Francesca Romana
              ›Santa Francesca Romana‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#114">Santo Stefano in
              Rotondo ›Santo Stefano Rotondo‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#115">Santa Bibiana all'Orso
              Pileato ›Santa Bibiana‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#116">Di Sant'Eusebio
              ›Sant'Eusebio‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#117">San Martino de Monti
              ›San Martino ai Monti‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#117">Santa Lucia in Selce
              ›Santa Lucia in Selci‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#118">San Pietro in Vincoli
              ›San Pietro in Vincoli‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#119">Santa Pressede ›Santa
              Prassede‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#119">San Antonio Abbate
              ›Sant'Antonio Abate‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#120">Santa Pudentiana
              ›Santa Pudenziana‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#122">Monastero di Monache a
              Magnanapoli ›Santa Caterina a Magnanapoli‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#122">Madonna di Loreto de
              Fornari ›Santa Maria di Loreto‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#123">San Silvestro a Monte
              Cavallo ›San Silvestro al Quirinale‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#124">Sant'Andrea al
              Noviziato ›Sant'Andrea al Quirinale‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#125">Santa Maria degli
              Angeli alle Terme ›Santa Maria degli Angeli‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#127">Sant'Agnese fuori di
              Roma &#160;›Sant'Agnese fuori le Mura‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#129">Madonna della Vittoria
              ›Santa Maria della Vittoria‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#131">Chiesa di San Bernardo
              ›San Bernardo alle Terme‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#131">Chiesa de' Santi
              Apostoli ›Santi Apostoli‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#132">San Romualdo ›San
              Romualdo‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#133">Chiesa di Santa Maria
              in Via Lata ›Santa Maria in Via Lata‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#135">Di San Marcello ›San
              Marcello al Corso‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#135">Santi Vincenzo, e
              Anastasio ›Santi Vincenzo ed Anastasio ‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#136">San Nicola da
              Tolentino a Capo le Case ›San Nicola da
              Tolentino‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#136">Chiesa de' Padri
              Capuccini ›Santa Maria della Concezione‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#138">Sant'Isidoro
              ›Sant'Isidoro‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#139">San Gioseffo delle
              Monache di Santa Teresa ›San Giuseppe a Capo le
              Case‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#139">Sant'Andrea delle
              Fratte ›Sant'Andrea delle Fratte‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#140">San Silvestro delle
              Monache ›San Silvestro in Capite‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#141">Chiesa delle
              Convertite ›Santa Lucia della Colonna‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#142">Chiesa della Rotonda
              ›Pantheon‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#144">Di San Lorenzo in
              Lucina ›San Lorenzo in Lucina‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#145">Di San Carlo al Corso
              ›Santi Ambrogio e Carlo al Corso‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#147">La Santissima Trinità
              de' Monti ›Trinità dei Monti‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#149">Chiesa di Gesù, e
              Maria, nella via Flaminia ›Chiesa di Gesù e
              Maria‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#150">Di San Giacomo degli
              Incurabili ›San Giacomo in Augusta‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#151">Santa Maria del Popolo
              ›Santa Maria del Popolo‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#154">Delle due Chiese nuove
              ›Santa Maria dei Miracoli‹ ›Santa Maria di
              Montesanto‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#155">Chiesa di San Rocco
              ›San Rocco‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#156">Di Sant'Agostino
              ›Sant'Agostino‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#158">Sant'Appolinare
              ›Sant'Apollinare‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#159">San Salvatore in Lauro
              ›San Salvatore in Lauro‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#160">Santa Maria dell'Anima
              ›Santa Maria dell'Anima‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#161">Santa Maria della Pace
              ›Santa Maria della Pace‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#163">San Giovanni
              Fiorentini ›San Giovanni dei Fiorentini‹</a>
            </li>
            <li>
              <a href="dg4502830s.html#164">Santa Maria della
              Traspontina ›Santa Maria in Traspontina‹</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
