<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghvas409168032s.html#8">Delle Vite de' più
      eccellenti Pittori Scultori e Architettori [...] Secondo, et
      ultimo Volume della Terza Parte</a>
      <ul>
        <li>
          <a href="ghvas409168032s.html#10">A gli artefici del
          disegno [dedica dell'autore]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvas409168032s.html#12">[Indici]</a>
      <ul>
        <li>
          <a href="ghvas409168032s.html#12">Tavola delle cose
          più notabili [...]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#24">Tavola dei ritratti
          [...]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#26">Tavola delle vite
          de gli artefici [...]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#28">Tavola de' luoghi,
          dove sono l'opere descritte [...]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#45">Tavola de' ritratti
          del museo dell'Illustrissimo et Eccellentissimo Signor
          Cosimo Duca di Fiorenza, et Siena</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#49">Anticaglie, che
          sono nella sala del Palazzo de' Pitti</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvas409168032s.html#52">[Lettera di Giovanni
      Battista Adriani a Giorgio Vasari]</a>
    </li>
    <li>
      <a href="ghvas409168032s.html#92">[Vite]</a>
      <ul>
        <li>
          <a href="ghvas409168032s.html#92">Domenico Beccafumi
          [Mecherino]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#102">Giovanni Antonio
          Lappoli</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#108">Niccolò Soggi</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#115">Tribolo [Niccolò
          di Raffaello de' Pericoli]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#137">Pierino da
          Vinci</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#144">Baccio Bandinelli
          [Bartolomeo Bandinelli]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#174">Giuliano di Piero
          di Simone Bugiardini</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#179">Cristofano
          Gherardi [Cristofano dal Doceno]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#195">Jacopo
          Pontormo</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#217">Simone Mosca</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#224">Girolamo e
          Bartolomeo Genga, Giovanni Battista Belluzzi
          [Sanmarino]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#234">Michele Sanmicheli
          [Sammicheli]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#249">Sodoma [Giovanni
          Antonio Bazzi]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#257">Bastiano da
          Sangallo [Aristotile da Sangallo]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#269">Garofalo
          [Benvenuto Tisi] e Girolamo da Carpi [ed altri artisti
          dell'Italia Settentrionale]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#290">Ridolfo, Davide e
          Benedetto Ghirlandaio [Bigordi]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#297">Giovanni da Udine
          [Giovanni Ricamatore]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#306">Battista Franco
          [Semolei]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#318">Giovan Francesco
          Rustici</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#330">Giovanni Angelo
          Montorsoli</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#346">Francesco Salviati
          [Francesco de Rossi]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#367">Daniele da
          Volterra [Daniele Ricciarelli]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#377">Taddeo Zuccari</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#406">Michelangelo
          Buonarroti</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#488">Francesco
          Primaticcio</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#496">Tiziano
          Vecellio</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#513">Iacopo Sansovino
          [Jacopo Tatti]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#531">Leone Leoni</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#540">Giulio Clovio
          [Julije Klović]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#553">Agnolo Bronzino
          [Angelo Tori] ed altri artisti dell'Accademia
          Fiorentina</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvas409168032s.html#573">[Giovan Battista Cini,
      Descrizione dell'apparato fatto in Firenze per le nozze di
      Francesco de' Medici e Giovanna d'Austria]</a>
      <ul>
        <li>
          <a href="ghvas409168032s.html#573">Descritione della
          Porta al Prato</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#581">Dell'entrata di
          Borgo ogni Santi</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#582">Del Ponte alla
          Carraia</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#586">Del Palazo degli
          Spini</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#589">Della Colonna</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#589">Del Canto a'
          Tornaquinci</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#594">Del Canto a'
          Carnesecchi</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#600">Del Canto alla
          Paglia</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#607">Di Santa Maria del
          Fiore</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#608">Del Cavallo</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#608">Del Borgo de
          Greci</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#609">Dell'Arco della
          Dogana</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#613">Della Piaza [sic],
          et del Nettunno [sic]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#613">Della Porta del
          Palazo [Palazzo Vecchio]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#616">Del Cortile del
          Palazo [Palazzo Vecchio]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#619">Della Sala, et
          della Commedia</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#627">Del Trionfo de
          Sogni, et d'altre feste</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#633">Del Castello</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#634">Della Geneologia
          [sic] degli Dei</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#638">Carro secondo di
          Cielo</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#639">Carro terzo di
          Saturno</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#641">Carro qarto [sic]
          del Sole</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#642">Carro quinto di
          Giove</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#644">Carro sesto di
          Marte</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#645">Carro settimo di
          Venere</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#646">Carro ottavo di
          Mercurio</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#647">Carro nono della
          Luna</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#648">Carro decimo di
          Minerva</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#650">Carro undicesimo
          di Vulcano</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#651">Carro duodecimo di
          Iunone</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#652">Carro tredicesimo
          di Nettunno [sic]</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#653">Carro
          quattordicesimo dell'Oceano, et di Tethyde</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#653">Carro quindicesimo
          di Pan</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#654">Carro sedicesimo
          di Plutone, et di Proserpina</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#655">Carro
          diciassettesimo di Cybele</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#656">Carro diciottesimo
          di Diana</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#657">Carro
          dicianovesimo [sic] di Cerere</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#658">Carro ventesimo di
          Bacco</a>
        </li>
        <li>
          <a href="ghvas409168032s.html#659">Carro ventunesimo,
          et ultimo</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvas409168032s.html#663">Descrizione dell'opere
      di Giorgio Vasari [...]</a>
    </li>
    <li>
      <a href="ghvas409168032s.html#693">L'autore agl'artefici
      del disegno</a>
    </li>
  </ul>
  <hr />
</body>
</html>
