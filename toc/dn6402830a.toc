<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dn6402830as.html#6">Li giardini di Roma con le
      loro piante alzate e vedute in prospettiva / disegnate ed
      intagliate da Gio. Battista Falda</a>
      <ul>
        <li>
          <a href="dn6402830as.html#8">Pianta del giardino del
          bel respiro dell'eccellente signore principe Pamphilio
          posta sul Monte ›Gianicolo‹ fuori la porta di San
          Pancratio [›Villa Doria Pamphilj‹] ◉</a>
        </li>
        <li>
          <a href="dn6402830as.html#10">Veduta del giardino di
          Belvedere del Palazzo Pontificio in Vaticano ›Giardini
          Vaticani‹ ▣</a>
        </li>
        <li>
          <a href="dn6402830as.html#12">Tavole delle cose
          notabili [›Giardini Vaticani‹] ◉</a>
        </li>
        <li>
          <a href="dn6402830as.html#14">Prospettiva del Giardio
          Pontificio sul Quirinale ›Giardino del Palazzo del
          Quirinale‹ ▣</a>
        </li>
        <li>
          <a href="dn6402830as.html#16">Pianta del Giardino
          Pontificio nel Quirinale ›Giardino del Palazzo del
          Quirinale‹ ◉</a>
        </li>
        <li>
          <a href="dn6402830as.html#18">Prospettiva del Giardino
          del serenissimo gran Duca di Toscana sul Monte ›Pincio‹
          [›Villa Medici‹] ▣</a>
        </li>
        <li>
          <a href="dn6402830as.html#20">Pianta del Giardino del
          serenissimo gran Duca di Toscana alla ›Trinità dei Monti‹
          sul Monte ›Pincio‹ [›Villa Medici‹] ◉</a>
        </li>
        <li>
          <a href="dn6402830as.html#22">Veduta del Giardino del
          serenissimo Duca di Parma sul Monte ›Palatino‹ verso
          Campo Vaccino [›Orti Farnesiani‹] ▣</a>
        </li>
        <li>
          <a href="dn6402830as.html#24">Pianta del Giardino del
          serenissimo Duca di Parma sul Monte ›Palatino‹ [›Orti
          Farnesiani‹] ◉</a>
        </li>
        <li>
          <a href="dn6402830as.html#26">Veduta del Giardino
          dell'eccellentissimo signor Prencipe Ludovisi a Porta
          Pinciana [›Villa Ludovisi‹] ▣</a>
        </li>
        <li>
          <a href="dn6402830as.html#28">Pianta del Giardino
          dell'eccellentissimo signor Prencipe Ludovisi a Porta
          Pinciana [›Villa Ludovisi‹] ◉</a>
        </li>
        <li>
          <a href="dn6402830as.html#30">Veduta del Gioardino
          dell'eminentissimo signor Cardinale Paolo Savelli Peretti
          verso Santa Maria Maggiore ▣</a>
        </li>
        <li>
          <a href="dn6402830as.html#32">Pianta et alzata del
          Giardino e vigna di Papa Sisto V. hoggi del
          emminentissimo Principe il signore Cardinale Paolo
          Savelli Peretti ◉</a>
        </li>
        <li>
          <a href="dn6402830as.html#34">Veduta e prospettiva del
          Giardino dell'eccellentissimo signor Prencipe Borghese
          fuori di Porta Pinciana [›Villa Borghese‹] ▣</a>
        </li>
        <li>
          <a href="dn6402830as.html#36">Pianta del Giardino
          dell'eccellentissimo signor Prencipe Borghese fuori di
          Porta Pinciana [›Villa Borghese‹] ◉</a>
        </li>
        <li>
          <a href="dn6402830as.html#38">Prospettiva del Giardino
          dell'eccellentissimo signor Duca Mattei alla Navicella
          sul Monte ›Celio‹ [›Villa Celimontana‹] ▣</a>
        </li>
        <li>
          <a href="dn6402830as.html#40">Pianta del Giardino
          dell'eccellentissimo signor Duca Mattei alla Navicella
          sul Monte ›Celio‹ [›Villa Celimontana‹] ◉</a>
        </li>
        <li>
          <a href="dn6402830as.html#42">Primo prospetto del
          Palazzo e Giardino Pamphilio detto del bel respiro
          [›Villa Doria Pamphilj‹] ▣</a>
        </li>
        <li>
          <a href="dn6402830as.html#44">Secondo Prospetto pero
          fianco del Palazzo con diversa veduta del Giardino del
          bel respiro dell'eccellentissmo signor Prencipe Pamphilio
          [›Villa Doria Pamphilj‹] ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
