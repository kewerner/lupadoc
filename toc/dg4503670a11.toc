<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503670a11s.html#6">I. Accurata e succinta
      descrizione topografica e istorica di Roma moderna</a>
      <ul>
        <li>
          <a href="dg4503670a11s.html#10">Avviso
          dell'Editore</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#15">Indice delle
          figure</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503670a11s.html#18">I. De' Monti</a>
      <ul>
        <li>
          <a href="dg4503670a11s.html#19">Chiesa di Santa Maria
          Nuova, e ›Santa Francesca Romana‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#21">Chiesa della Pietà
          nel Colosseo ›Santa Maria della Pietà al Colosseo‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#22">›San Tommaso in
          Formis‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#23">›Santa Maria in
          Domnica‹ detta della Navicella</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#24">Villa Mattei alla
          Navicella ›Villa Celimontana‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#27">›Santo Stefano
          Rotondo‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#28">›Santo Stefano
          Rotondo‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#30">Villa Casali
          ›Ospedale Militare del Celio‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#31">Di Sant'Andrea ›Santi
          Andrea e Bartolomeo‹ e dell'Arciospedale del Santissimo
          Salvatore ›Ospedale di San Giovanni‹ &#160;a ›San
          Giovanni in Laterano‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#32">De' Santi Giovanni
          battista, e Giovanni Evangelista in Fonte, e suo
          Battistero ›Battistero Lateranense‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#36">Delle Sante Ruffina,
          e Seconda, e di San Cipriano, e Giustina ›Cappella di
          Santa Rufina‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#37">›Cappella di San
          Venanzio‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#39">Veduta di ›San
          Giovanni in Laterano‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#40">Della Basilica di
          ›San Giovanni in Laterano‹, e sua descrizione</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#59">Del Santissimo
          Salvatore alla ›Scala Santa‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#61">Del ›Palazzo
          Lateranense‹, e dell'Ospizio Apostolico delle Povere
          Zitelle</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#62">Dell'Obelisco di San
          Giovanni in Laterano ›Obelisco Lateranense‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#64">Verso la Basilica di
          ›Santa Maria Maggiore‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#65">Verso lo Spedale
          degli Uomini ›Ospedale di San Giovanni‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#66">Verso la menzionata
          Basilicata Lateranense ›San Giovanni in Laterano‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#67">Verso la ›Scala
          Santa‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#71">Di ›Santa Croce in
          Gerusalemme‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#72">›Santa Croce in
          Gerusalemme‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#79">›San Lorenzo fuori le
          Mura‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#80">Delle Chiese di
          Sant'Elena ›Mausoleo di Sant'Elena‹, e dei ›Santi
          Marcellino e Pietro‹ fuori di Porta Maggiore</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#80">›San Lorenzo fuori le
          Mura‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#86">›Santa Bibiana‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#88">›Sant'Eusebio‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#91">Di ›San Giuliano‹
          alli Trofei di Mario</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#91">›San Matteo in
          Merulana‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#92">›Santi Marcellino e
          Pietro‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#93">Villa Giustiniani
          ›Villa Massimo‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#94">›Santa Maria
          Imperatrice‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#95">›Santi Quattro
          Coronati‹, e del Conservatorio delle Orfane ›Pia casa
          della visitazione degli orfani di S. Maria in Aquiro e
          SS. Quattro Coronati‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#98">›San Clemente‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#100">Di Sant'Andrea in
          Portogallo ›Santa Maria della Neve‹, e della Compagnia
          de' Rigattieri</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#101">Del Conservatorio
          delle povere Zitelle Mendicanti divote del Santissimo
          Sagramento ›Palazzo Silvestri-Rivaldi‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#102">Di ›San Pantaleo‹ ai
          Pantani</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#103">Di ›Santa Maria
          degli Angeli‹ nella ›Via Alessandrina‹, e della
          Confraternita de' tessitori</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#103">›Sant'Urbano ai
          Pantani‹, e suo Monastero ›Conservatorio di Santa
          Eufemia‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#105">Di San Lorenzo al
          Macello de' Corvi ›San Lorenzo de Ascesa ai Monti‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#105">Della Chiesa dello
          ›Spirito Santo a Colonna Traiana‹, e suo Monastero</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#106">Di ›Sant'Eufemia al
          Foro Traiano‹, e suo Conservatorio ›Conservatorio di
          Santa Eufemia‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#107">Santa Maria in Campo
          Carleo ›Santa Maria in Campo‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#107">Di Santa Maria
          Annunziata e San Basilio ›San Basilio al Foro di Augusto‹
          e del Monastero delle Neofite ›Monastero Domenicano della
          Santissima Annunziata‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#108">Del Collegio
          Ibernese ›Pontificio Collegio Irlandese‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#109">Dei ›Santi Quirico e
          Giulitta‹, e della Confraternita del Santissimo
          Sagramento</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#110">›San Salvatore ai
          Monti‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#111">Di Santa Maria de'
          Monti ›Madonna dei Monti (chiesa)‹, e del ›Collegio dei
          Neofiti‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#113">Della ›Santa Maria
          della Concezione delle Cappuccine‹ e suo Monastero</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#114">Dei ›Santi Sergio e
          Bacco‹, ovvero di Santa Maria del Pascolo</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#115">›San Pietro in
          Vincoli‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#120">›San Francesco di
          Paola‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#122">›Santa Maria della
          Purificazione‹ e suo Monasterio</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#123">›Santa Lucia in
          Selci‹, e suo Monasterio</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#124">Di ›Santa Maria
          Annunziata delle Turchine‹, e del Monastero delle
          Turchine</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#125">›San Martino ai
          Monti‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#129">Di ›Santa Prassede‹,
          e delle Terme di Novato ›Thermae Novati‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#133">Dei ›Santi Vito e
          Modesto‹, e dell'›Arco di Gallieno‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#134">Del Conservatorio
          della Santissima Concezione, detto comunemente delle
          Viperesche ›Conservatorio delle Viperesche‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#135">Di ›Sant'Antonio
          Abate‹, e suo ›Ospedale di Sant'Antonio Abate‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#139">›Santa Maria
          Maggiore‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#140">Della Basilica di
          ›Santa Maria Maggiore‹, e sua descrizione</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#150">Dell'Obelisco di
          Santa Maria Maggiore ›Obelisco Esquilino‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#156">›Santa
          Pudenziana‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#158">Del Conservatorio
          del Bambin Gesù ›Bambino Gesù all'Esquilino (chiesa)‹, e
          delle Scuole Pontificie per le povere Zitelle di Roma
          ›Convento delle Oblate Agostiniane Convittrici del S.
          Bambino Gesù‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#159">Di ›San Lorenzo in
          Fonte‹, e della Congregazione Urbana de' Cortigiani</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#160">›San Lorenzo in
          Panisperna‹, e suo Monastero</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#161">Di Sant'Agata alla
          Suburra ›Sant'Agata dei Goti‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#163">San Bernardino ai
          Monti ›San Bernardino da Siena‹, e suo Monastero</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#164">›Palazzo del
          Collegio Fuccioli‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#164">›Villa Doria
          Pamphilj‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#166">Dei ›Santi Domenico
          e Sisto‹, e Monastero annesso</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#168">Di ›Santa Caterina a
          Magnanapoli‹, e suo Monastero</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#170">Di San Bernardo alla
          Colonna Trajana ›San Bernardo della Compagnia‹, e della
          Confraternita del Santissimo nome di Maria</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#171">›Colonna
          Traiana‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#174">Di ›Santa Maria di
          Loreto‹, e della Confraternita de' Fornari, e suo
          Ospedale</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#176">Dell'Oratorio di
          ›Santa Maria del Carmine‹ alle tre Cannelle, e sua
          Confraternita</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#176">Di San Silvestro a
          Monte Cavallo ›San Silvestro al Quirinale‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#179">Palazzo Rospigliosi
          a Monte Cavallo ›Palazzo Pallavicini Rospigliosi‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#181">Palazzo Pontificio
          ›Palazzo del Quirinale‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#181">Chiesa della
          Maddalena ›Santa Maria Maddalena‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#181">›Palazzo della
          Consulta‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#181">Palazzo Rospigliosi
          ›Palazzo Pallavicini Rispigliosi‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#181">Stalle Pontificie e
          Corpo di Guardia ›Scuderie del Quirinale‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#183">›Palazzo della
          Consulta‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#185">›Santa Maria
          Maddalena‹ a Monte Cavallo, e suo Monastero</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#186">Santa Chiara delle
          Cappuccine ›Santa Chiara al Quirinale‹, e suo
          Monastero</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#187">Di ›Sant'Andrea al
          Quirinale‹, e del Noviziato dei Padri Gesuiti ›Noviziato
          dei Gesuiti‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#189">›San Carlo alle
          Quattro Fontane‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#189">›Palazzo Albani Del
          Drago‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#190">Di Sant'Anna alle
          Quattro Fontane ›Santi Gioacchino e Anna alle Quattro
          Fontane‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#190">Di San Carlo alle
          Quattro Fontane</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#191">›San Vitale‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#193">Di San Dionigi
          Areopgita ›San Dionisio alle Quattro Fontane‹, e di ›San
          Paolo primo Eremita‹, con gli annessi Conventi</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#194">Di ›Santa Maria
          della Sanità‹, e di ›Sant'Antonio da Padova‹, e
          dell'Ospizio dei Padri Missionari Conventuali</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#195">Di ›San Norberto‹, e
          del Monastero annesso</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#196">Villa Montalto, oggi
          ›Villa Negroni‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#199">›Santa Maria degli
          Angeli‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#203">Villa Costaguti,
          Villa Valenti indi Sciarra, ›Villa Patrizi‹ ›, ›Villa
          Bolognetti‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#204">›Sant'Agnese fuori
          le Mura‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#206">Di Santa Costanza
          fuori di Porta Pia ›Mausoleo di Santa Costanza‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#207">Fontanone a Termini
          ›Fontana del Mosè‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#208">Veduta della Fontana
          d'Acqua Felice a Termini ›Fontana del Mosè‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#210">›San Bernardo alle
          terme‹ Diocleziane</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#212">›San Caio‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#212">Chiesa
          dell'Incarnazione, e suo Monastero ›Monastero
          dell'Incarnazione detto le Barberine‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#213">Del ›Palazzo Albani
          Del Drago‹</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503670a11s.html#215">II. Di Trevi</a>
      <ul>
        <li>
          <a href="dg4503670a11s.html#217">›Fontana di Trevi‹
          ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#218">Nuovo Prospetto
          della ›Fontana di Trevi‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#221">›Santi Vincenzo e
          Anastasio‹ a Trevi</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#224">›Collegio
          Nazareno‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#225">Di San Giovanni de'
          Maroniti ›San Giovanni della Ficozza‹ e loro Collegio</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#226">›Santi Angeli
          Custodi al Tritone‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#228">Santa Maria di
          Costantinopoli ›Santa Maria d'Itria‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#229">Di ›Sant'Andrea
          degli Scozzesi‹, e dell'annesso Collegio</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#230">Santa Maria dei
          Fugliensi ›Santa Maria dei Foglianti‹, e suo Ospizio</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#230">›San Niccolò degli
          Arcioni ‹, e suo Oratorio</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#232">Del ›Collegio
          Mattei‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#232">›Santi Ildefonso e
          Tommaso di Villanova‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#233">Di ›Santa Francesca
          Romana dei Padri del Riscatto‹ in Strada Felice</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#234">Di ›Sant'Isidoro‹ a
          Capo le Case, col Colleggio annesso</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#236">Della Santissima
          Concezione, e del Convento dei Padri Cappuccini ›Santa
          Maria della Concezione‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#241">Veduta di ›Villa
          Ludovisi‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#242">Di ›San Basilio‹, e
          Ospizio annesso</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#242">›Villa Ludovisi‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#246">Di San Niccolò di
          Tolentino a Capo le Case ›San Nicola da Tolentino‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#248">›Santa Maria della
          Vittoria‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#253">›Santa Susanna‹, e
          suo Monastero</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#257">›Palazzo Barberini‹
          ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#258">Del ›Palazzo
          Barberini‹ alle Quattro Fontane</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#269">Palazzo Pontificio a
          Monte Cavallo ›Palazzo del Quirinale‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#270">Palazzo Pontificio
          di Monte Cavallo al Quirinale ›Palazzo del Quirinale‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#284">›Santa Croce e San
          Bonaventura dei Lucchesi‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#285">Palazzo del Signor
          Contestabile Colonna ›Palazzo Colonna‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#289">›Santi Apostoli‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#295">›Palazzo Odescalchi‹
          ai Santi Apostoli</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#296">Palazzo del Duca di
          Bracciano Odescalchi al Convento dei Padri di San
          Marcello ›Palazzo Odescalchi‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#302">›San Romualdo‹,
          coll'Ospizio annesso</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#303">Del Palazzo d'Aste,
          e dell'Accademia di Francia ›Palazzo Bonaparte‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#303">Palazzo Pamfilj al
          Corso ›Palazzo Doria Pamphilj‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#304">Palazzo
          dell'Accademia di Francia al Corso ›Palazzo Bonaparte‹
          ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#306">Veduta del Palazzo
          Panfilio dalla parte del Collegio Romano ›Palazzo Doria
          Pamphilj‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#309">Palazzo nuovo
          Pamfilj in Piazza di Venezia › Palazzo Doria
          Pamphilj‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#310">›Palazzo
          Verospi‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#310">Della Collegiata di
          ›Santa Maria in Via Lata‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#312">›Palazzo De
          Carolis‹, e ›Palazzo Mellini‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#313">›San Marcello al
          Corso‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#317">Dell'Oratorio di San
          Marcello, e dell'Archiconfraternita del Santissimo
          Crocifisso ›San Marcello al Corso‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#318">Di Santa Maria delle
          Vergini ›Santa Rita‹, e suo Monastero</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#319">Di ›Santa Maria
          dell'Umiltà‹, e suo Monastero</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503670a11s.html#321">III. Di Colonna</a>
      <ul>
        <li>
          <a href="dg4503670a11s.html#323">›Villa Borghese‹
          &#160;fuori di Porta Pinciana, e sua descrizione</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#324">›Villa Borghese‹
          ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#333">Di ›San Giuseppe a
          Capo le case‹, e Monastero annesso</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#335">Del ›Collegio Urbano
          di Propaganda Fide‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#336">›Collegio Urbano di
          Propaganda Fide‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#338">Di ›Sant'Andrea
          delle Fratte‹, e Convento unito</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#341">Oratorio di
          ›Sant'Andrea delle Fratte‹, e sua Confraternita</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#342">Di Santa Maria in
          San Giovanni in Campo Marzo, coll'Ospizio dei Padri
          Spagnuoli Riformati della Mercede ›San Giovanni in
          Capite‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#343">Di ›San Silvestro in
          Capite‹, e suo Monastero e dell'Odeo ›Odeon di
          Domiziano‹, e Stadio di Domiziano ›Piazza Navona‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#346">Di ›Santa Maria
          Maddalena‹ al Corso</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#348">›Palazzo
          Verospi‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#349">Di San Claudio dei
          Borgognoni, e loro Ospizio ›Santi Andrea e Claudio dei
          Borgognoni‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#350">Palazzo Conti dei
          Duchi di Poli, e Guadagnolo ›Palazzo Poli‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#351">›Santa Maria in
          Via‹, e Convento annesso dei padri Serviti, e del suo
          Oratorio</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#354">Di ›Santa Maria in
          Trivio‹, e Noviziato dei Crociferi</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#356">›Piazza Colonna‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#360">Di San Bartolomeo
          dei Bergamaschi ›Santi Bartolomeo e Alessandro dei
          Bergamaschi‹, e ›Collegio Cerasoli‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#361">›Palazzo Chigi‹ al
          Corso</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#365">Monte Citorio
          ›Piazza di Montecitorio‹ ▣</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#366">Della Curia
          Innocenziana, e del Monte Citorio ›Palazzo
          Montecitorio‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#368">›Dogana nuova di
          Terra‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#370">›San Macuto‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#370">Del Seminario Romano
          ›Palazzo Gabrielli Borromeo‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#373">Di ›Santa Maria in
          Aquiro‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#374">Dello Spedale degli
          Orfani, e del Collegio Salviati ›Pia casa della
          visitazione degli orfani di S. Maria in Aquiro e SS.
          Quattro Coronati‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#376">Del ›Collegio
          Capranica‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#378">›Piazza della
          Rotonda‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#379">Di Santa Maria ad
          Martyres, detta volgarmente la Rotonda ›Pantheon‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#387">Di ›Santa Maria
          Maddalena‹ dei Padri Ministri degli Infermo</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#390">Della Santissima
          Trinità a Monte Citorio, e della Congregazione dei
          Sacerdoti della Missione ›Santissima Trinità della
          Missione‹</a>
        </li>
        <li>
          <a href="dg4503670a11s.html#394">Di ›San Salvatore
          alle Coppelle‹</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
