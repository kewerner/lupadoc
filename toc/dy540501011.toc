<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy540501011s.html#2">dy540501011</a>
    </li>
    <li>
      <a href="dy540501011s.html#5">[Vorsatz]</a>
    </li>
    <li>
      <a href="dy540501011s.html#5">[autogr. Widmung]</a>
    </li>
    <li>
      <a href="dy540501011s.html#8">[Textband]</a>
    </li>
    <li>
      <a href="dy540501011s.html#8">[Schmutztitel] Die
      Sixtinische Kapelle/</a>
    </li>
    <li>
      <a href="dy540501011s.html#10">[Titelblatt] Die
      Sixtinische Kapelle</a>
    </li>
    <li>
      <a href="dy540501011s.html#12">Vorwort zum ersten
      Bande</a>
    </li>
    <li>
      <a href="dy540501011s.html#18">Inhalt des ersten
      Bandes</a>
    </li>
    <li>
      <a href="dy540501011s.html#28">Abschnitt I SIXTUS IV. UND
      SEIN HOF</a>
    </li>
    <li>
      <a href="dy540501011s.html#30">Kapitel I Laufbahn und
      Charakter des Sixtus IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#38">Kapitel II Die
      Bauthätigkeit Sixtus IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#54">Kapitel III Die Kardinäle
      Sixtus IV., ihre Bauthätigkeit und ihre Denkmäler in Rom</a>
    </li>
    <li>
      <a href="dy540501011s.html#76">Abschnitt II LITERATUR UND
      KUNST IN DEN JAHREN 1471-1481</a>
    </li>
    <li>
      <a href="dy540501011s.html#78">Kapitel I Die Humanisten
      unter Sixtus IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#85">Kapitel II Die Architekten
      Sixtus IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#88">Kapitel III Die Bildhauer
      Sixtus IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#96">Kapitel IV Die Maler Sixtus
      IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#144">Abschnitt III DIE
      PALASTKAPELLE SIXTUS IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#146">Kapitel I Die ältere
      Palastkapelle</a>
    </li>
    <li>
      <a href="dy540501011s.html#152">Kapitel II Die
      Baugeschichte der neuen Palastkapelle und ihr Architekt</a>
    </li>
    <li>
      <a href="dy540501011s.html#161">Kapitel III Anbauten und
      Restaurationen der Kapelle bis auf Julius II. älteste
      Beschreibungen, Stiche und Abbildungen</a>
    </li>
    <li>
      <a href="dy540501011s.html#168">Kapitel IV Der äußere Bau
      des Heiligtums. Räume über und unter der Kapelle</a>
    </li>
    <li>
      <a href="dy540501011s.html#185">Kapitel V Der Innenraum
      der Kapelle</a>
    </li>
    <li>
      <a href="dy540501011s.html#191">Kapitel VI Der
      Skultpturenschmuck der Kapelle</a>
    </li>
    <li>
      <a href="dy540501011s.html#212">Abschnitt IV BEGINN DER
      MALEREIEN IN DER KAPELLE SIXTUS IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#214">Kapitel I Die
      Deckendekoration</a>
    </li>
    <li>
      <a href="dy540501011s.html#223">Kapitel II Die
      Papstbildnisse</a>
    </li>
    <li>
      <a href="dy540501011s.html#250">Abschnitt V DER
      HISTORISCHE BILDERKREIS</a>
    </li>
    <li>
      <a href="dy540501011s.html#252">Kapitel I Ältere
      Bilderkreise in Rom</a>
    </li>
    <li>
      <a href="dy540501011s.html#258">Kapitel II Beschreibung
      des Bilderkreises in der Sixtina</a>
    </li>
    <li>
      <a href="dy540501011s.html#271">Kapitel III Huldigung des
      Papstes im Reinigungsopfer des Aussätzigen</a>
    </li>
    <li>
      <a href="dy540501011s.html#281">Kapitel IV Pharaos
      Untergang im roten Meer als Verherrlichung der Schlacht von
      Campo Morto</a>
    </li>
    <li>
      <a href="dy540501011s.html#289">Kapitel V Die Bestrafung
      Korahs, Dathans und Abirams als Denkmal der Überwindung des
      Schismas</a>
    </li>
    <li>
      <a href="dy540501011s.html#302">Abschnitt VI DIE
      UMBRISCHEN MEISTER IN DER SIXTINA</a>
    </li>
    <li>
      <a href="dy540501011s.html#304">Kapitel I Die Gemälde der
      Altarwand</a>
    </li>
    <li>
      <a href="dy540501011s.html#324">Kapitel II Die
      Beschneidung des Mosesknaben</a>
    </li>
    <li>
      <a href="dy540501011s.html#345">Kapitel III Die Taufe
      Christi</a>
    </li>
    <li>
      <a href="dy540501011s.html#360">Kapitel IV Die
      Schlüsselübergabe</a>
    </li>
    <li>
      <a href="dy540501011s.html#392">Abschnitt VII DIE
      FLORENTINER MEISTER IN DER SIXTINA</a>
    </li>
    <li>
      <a href="dy540501011s.html#394">Kapitel I Die Berufung der
      ersten Jünger</a>
    </li>
    <li>
      <a href="dy540501011s.html#419">Kapitel II Die Bergpredigt
      Christi</a>
    </li>
    <li>
      <a href="dy540501011s.html#440">Kapitel III Das letzte
      Abendmahl</a>
    </li>
    <li>
      <a href="dy540501011s.html#450">Kapitel IV Die
      Gesetzgebung auf Sinai</a>
    </li>
    <li>
      <a href="dy540501011s.html#459">Kapitel V Der Durchzug
      durchs rote Meer</a>
    </li>
    <li>
      <a href="dy540501011s.html#486">Kapitel VI Das
      Reinigungsopfer des Aussätzigen</a>
    </li>
    <li>
      <a href="dy540501011s.html#514">Kapitel VII Das
      Jugendleben des Moses</a>
    </li>
    <li>
      <a href="dy540501011s.html#523">Kapitel VIII Die
      Bestrafung der Rotte Korah</a>
    </li>
    <li>
      <a href="dy540501011s.html#540">Abschnitt VIII LUCA
      SIGNORELLI UND BARTOLOMEO DELLA GATTA</a>
    </li>
    <li>
      <a href="dy540501011s.html#542">[I] Das Testament des
      Moses</a>
    </li>
    <li>
      <a href="dy540501011s.html#572">Abschnitt IX DIE KAPELLE
      SIXTUS IV. ALS KULTUSSTÄTTE</a>
    </li>
    <li>
      <a href="dy540501011s.html#574">Kapitel I Die Weihe des
      Heiligtums</a>
    </li>
    <li>
      <a href="dy540501011s.html#580">Kapitel II Fürsorge Sixtus
      IV. für kirchlichen Schmuck, Gottesdienstordnung und
      Reorganisation des Sängerchors</a>
    </li>
    <li>
      <a href="dy540501011s.html#592">Kapitel III Die Feier der
      großen Kirchenfeste in der Kapelle Sixtus IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#614">ANHANG ZUM ERSTEN
      BANDE</a>
    </li>
    <li>
      <a href="dy540501011s.html#618">Anhang I Die Biographien
      Sixtus IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#630">Anhang II
      Porträt-Darstellungen und Medaillen Sixtus IV.</a>
    </li>
    <li>
      <a href="dy540501011s.html#648">Anhang III Dokumente</a>
    </li>
    <li>
      <a href="dy540501011s.html#700">[Anhang IV] Verzeichnis
      der benutzten Bücher</a>
    </li>
    <li>REGISTER706</li>
    <li>
      <a href="dy540501011s.html#708">I.Personenverzeichnis</a>
    </li>
    <li>
      <a href="dy540501011s.html#714">II.Ortsverzeichnis</a>
    </li>
    <li>
      <a href="dy540501011s.html#716">III.Verzeichnis der
      erwähnten Monumente und Kunstschätze</a>
    </li>
    <li>
      <a href="dy540501011s.html#730">IV.Verzeichnis der
      Abbildungen und Tafeln</a>
    </li>
    <li>
      <a href="dy540501011s.html#738">[Kolophon]</a>
    </li>
    <li>
      <a href="dy540501011s.html#740">[Hinterer Vorsatz]</a>
    </li>
  </ul>
  <hr />
</body>
</html>
