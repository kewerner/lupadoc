<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502005s.html#6">Le cose meravigliose dell'alma
      città di Roma</a>
      <ul>
        <li>
          <a href="dg4502005s.html#7">Le sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg4502005s.html#7">La prima Chiesa è ›San
              Giovanni in Laterano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502005s.html#10">La seconda Chiesa è
              ›San Pietro in Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502005s.html#13">La terza Chiesa &#160;è
              ›San Paolo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4502005s.html#14">La quarta Chiesa è
              ›Santa Maria Maggiore‹ ▣</a>
            </li>
            <li>
              <a href="dg4502005s.html#16">La quinta Chiesa è ›San
              Lorenzo fuori dalle Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4502005s.html#17">La Sesta Chiesa é ›San
              Sebastiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502005s.html#18">La Settima Chiesa é
              ›Santa Croce in Gerusalemme‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502005s.html#18">Nell'›Isola Tiberina‹</a>
        </li>
        <li>
          <a href="dg4502005s.html#19">In ›Trastevere‹</a>
        </li>
        <li>
          <a href="dg4502005s.html#21">Del Borgo</a>
        </li>
        <li>
          <a href="dg4502005s.html#23">Della Porta Flaminia overo
          dal Popolo ›Porta del Popolo‹ fino alle radici del
          ›Campidoglio‹</a>
        </li>
        <li>
          <a href="dg4502005s.html#32">Dal ›Campidoglio‹ a man
          sinistra verso i Monti</a>
        </li>
        <li>
          <a href="dg4502005s.html#38">Dal ›Campidoglio‹ a man
          dritta verso i Monti</a>
        </li>
        <li>
          <a href="dg4502005s.html#42">Le stationi che sono nelle
          chiese di Roma [...]</a>
          <ul>
            <li>
              <a href="dg4502005s.html#49">Le Stationi
              dell'Advento</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502005s.html#50">La guida romana per il
          forastieri che vengono per vedere l'Antichità di Roma, ad
          una per una in bellissima forma, et brevità</a>
          <ul>
            <li>
              <a href="dg4502005s.html#50">[Giornata prima]</a>
              <ul>
                <li>
                  <a href="dg4502005s.html#50">Del ›Borgo‹ la
                  prima giornata</a>
                </li>
                <li>
                  <a href="dg4502005s.html#51">Del
                  ›Trastevere‹</a>
                </li>
                <li>
                  <a href="dg4502005s.html#51">Dell'Isola
                  Tiberina‹</a>
                </li>
                <li>
                  <a href="dg4502005s.html#52">Del Monte Testaccio
                  ›Testaccio (Monte)‹ e di molte altre cose</a>
                </li>
                <li>
                  <a href="dg4502005s.html#53">Delle Therme
                  Antoniane ›Terme di Caracalla‹ , ed altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4502005s.html#53">Di ›San Giovanni in
                  Laterano‹, et altre cose</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502005s.html#53">Gioranta seconda</a>
              <ul>
                <li>
                  <a href="dg4502005s.html#54">Della ›Porta del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4502005s.html#54">De i Cavalli di
                  marmo che stanno a monte Cavallo ›Quirinale‹ , et
                  delle Therme Diocletiane ›Terme di
                  Diocleziano‹</a>
                </li>
                <li>
                  <a href="dg4502005s.html#55">Della strada Pia
                  ›Via XX Settembre‹</a>
                </li>
                <li>
                  <a href="dg4502005s.html#55">Della Vigna del
                  Cardinal di Ferrara</a>
                </li>
                <li>
                  <a href="dg4502005s.html#55">Della Vigna del
                  Cardinal di Carpi, et altre cose</a>
                </li>
                <li>
                  <a href="dg4502005s.html#55">Della ›Porta
                  Pia‹</a>
                </li>
                <li>
                  <a href="dg4502005s.html#55">Di ›Sant'Agnese
                  fuori le Mura‹, et altre anticaglie</a>
                </li>
                <li>
                  <a href="dg4502005s.html#56">Del Tempio di
                  Iside, et altre cose</a>
                </li>
                <li>
                  <a href="dg4502005s.html#56">Delle ›Sette Sale‹,
                  et del ›Colosseo‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4502005s.html#57">Del tempio della
                  Pace ›Foro della Pace‹, et del Monte Palatino,
                  hora detto Palazzo Maggiore ›Palatino‹, et altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4502005s.html#57">Del ›Campidoglio‹,
                  et altre cose</a>
                </li>
                <li>
                  <a href="dg4502005s.html#58">De i portichi di
                  Ottavia ›Portico d'Ottavia‹, et di Settimio
                  ›Porticus Severi‹, et del ›Teatro di Pompeo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502005s.html#58">Giornata terza</a>
              <ul>
                <li>
                  <a href="dg4502005s.html#58">Delle due Colonne
                  una di Antonio Pio, et l'altra di Traiano, et
                  altre cose ›Colonna di Antonino Pio‹ ›Colonna
                  Traiana‹</a>
                </li>
                <li>
                  <a href="dg4502005s.html#59">Della Rotonda,
                  overo ›Pantheon‹</a>
                </li>
                <li>
                  <a href="dg4502005s.html#59">Dei Bagni di
                  Agrippa, et di Nerone ›Terme di Agrippa‹ ›Terme
                  Neroniano-Alessandrine‹</a>
                </li>
                <li>
                  <a href="dg4502005s.html#59">Della piazza
                  Navona, et di mastro Pasquino ›Piazza Navona‹
                  ›Statua di Pasquino‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502005s.html#60">[Tavole]</a>
              <ul>
                <li>
                  <a href="dg4502005s.html#60">Indice de Sommi
                  Pontefici Romani</a>
                </li>
                <li>
                  <a href="dg4502005s.html#78">Reges et
                  Imperatores Romani</a>
                </li>
                <li>
                  <a href="dg4502005s.html#80">Li Re di
                  Francia</a>
                </li>
                <li>
                  <a href="dg4502005s.html#81">Li Re del Regno di
                  Napoli et di Sicilia, il quali cominciorno a
                  regnare l'anno di nostra salute</a>
                </li>
                <li>
                  <a href="dg4502005s.html#82">Li Dogi di
                  Vinegia</a>
                </li>
                <li>
                  <a href="dg4502005s.html#84">Tavola delle Chiese
                  di Roma</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502005s.html#94">L'antichità di Roma</a>
      <ul>
        <li>
          <a href="dg4502005s.html#95">Alli lettori</a>
        </li>
        <li>
          <a href="dg4502005s.html#96">[Text]</a>
          <ul>
            <li>
              <a href="dg4502005s.html#96">Dell'edificatione di
              Roma</a>
            </li>
            <li>
              <a href="dg4502005s.html#98">Del Circuito di
              Roma</a>
            </li>
            <li>
              <a href="dg4502005s.html#98">Delle Porte</a>
            </li>
            <li>
              <a href="dg4502005s.html#99">Delle vie</a>
            </li>
            <li>
              <a href="dg4502005s.html#100">Delli Ponti che sono
              sopra il Tevere, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4502005s.html#101">Dell'Isola del Tevere
              ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#101">Delli Monti</a>
            </li>
            <li>
              <a href="dg4502005s.html#102">Del Monte Testaccio
              ›Testaccio (Monte)‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#102">Delle Acque, et chi le
              condusse in Roma</a>
            </li>
            <li>
              <a href="dg4502005s.html#103">Della ›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#103">Delli Acquedotti</a>
            </li>
            <li>
              <a href="dg4502005s.html#104">Delle ›Sette Sale‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#104">Delle Terme cioè Bagni
              et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4502005s.html#105">Delle Naumachie, dove
              si facevano le battaglie navali, et che cose
              erano</a>
            </li>
            <li>
              <a href="dg4502005s.html#105">De' Cerchi, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4502005s.html#106">De' Theatri, et che
              cosa erano, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4502005s.html#106">Delli Anfiteatri, et
              suoi edificatori, et che cosa erano</a>
            </li>
            <li>
              <a href="dg4502005s.html#106">De' Fori, cioè
              Piazze</a>
            </li>
            <li>
              <a href="dg4502005s.html#107">Delli Archi Trionfali,
              et a chi si davano</a>
            </li>
            <li>
              <a href="dg4502005s.html#108">De' Portichi</a>
            </li>
            <li>
              <a href="dg4502005s.html#108">De' Trofei, et Colonne
              memorande</a>
            </li>
            <li>
              <a href="dg4502005s.html#109">De' Colossi</a>
            </li>
            <li>
              <a href="dg4502005s.html#109">Delle Piramidi</a>
            </li>
            <li>
              <a href="dg4502005s.html#109">Delle Mete</a>
            </li>
            <li>
              <a href="dg4502005s.html#109">Delli Obelischi, over
              Aguglie</a>
            </li>
            <li>
              <a href="dg4502005s.html#110">Delle Statue</a>
            </li>
            <li>
              <a href="dg4502005s.html#110">›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#110">De Cavalli ›Fontana di
              Monte Cavallo‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#110">Delle Librarie</a>
            </li>
            <li>
              <a href="dg4502005s.html#111">Delli Horioli</a>
            </li>
            <li>
              <a href="dg4502005s.html#111">De' Palazzi</a>
            </li>
            <li>
              <a href="dg4502005s.html#111">Della Casa Aurea di
              Nerone ›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#112">Dell'altre case de'
              Cittadini</a>
            </li>
            <li>
              <a href="dg4502005s.html#112">Delle Curie, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4502005s.html#112">De' Senatuli, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4502005s.html#113">De' Magistrati</a>
            </li>
            <li>
              <a href="dg4502005s.html#113">De Comitij, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4502005s.html#113">Delle Tribu</a>
            </li>
            <li>
              <a href="dg4502005s.html#114">Delle Regioni, cioè
              Rioni, et sue insegne</a>
            </li>
            <li>
              <a href="dg4502005s.html#114">Delle Basiliche, et
              che cosa erano</a>
            </li>
            <li>
              <a href="dg4502005s.html#114">Del ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#115">Dello Erario
              ›Aerarium‹, cioè Camera del commune, et che moneta si
              spendeva in Roma in que' tempi</a>
            </li>
            <li>
              <a href="dg4502005s.html#116">Del Gregostasi, et che
              cosa era ›Grecostasi‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#116">Della Secretaria del
              Popolo Romano ›Secretarium Senatus‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#116">Dell'›Asilo di
              Romolo‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#116">Delli ›Rostri‹, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4502005s.html#116">Della Colonna detta
              Miliario ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#116">Del Tempio di Carmenta
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#116">Della colonna Bellica
              ›Columna Bellica‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#117">Della Colonna Lattaria
              ›Columna Lactaria‹</a>
            </li>
            <li>
              <a href=
              "dg4502005s.html#117">Dell'›Aequimaelium‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#117">Del ›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#117">Del Tigillo Sororio
              ›Tigillum Sororium‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#117">De' Campi Forastieri
              ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#117">Della via (sic!)
              publica ›Villa Publica‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#117">Della ›Taberna
              Meritoria‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#118">Del ›Velabro‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#118">Delle ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#118">Delli Clivi</a>
            </li>
            <li>
              <a href="dg4502005s.html#119">De' Frati (sic!)</a>
            </li>
            <li>
              <a href="dg4502005s.html#119">De Granari Publici, et
              magazini del Sale</a>
            </li>
            <li>
              <a href="dg4502005s.html#119">Delle Carceri
              publiche</a>
            </li>
            <li>
              <a href="dg4502005s.html#119">Di alcune feste, et
              giochi che si solevano celebrare in Roma</a>
            </li>
            <li>
              <a href="dg4502005s.html#120">Del Sepolchro di
              Augusto, d'Adriano, et di Settimio</a>
            </li>
            <li>
              <a href="dg4502005s.html#120">De Tempij</a>
            </li>
            <li>
              <a href="dg4502005s.html#121">De' Sacerdoti delle
              Vergini Vestali, vestimenti, vasi, et altri
              instrumenti fatti per uso delli sacrificij, et suoi
              institutori</a>
            </li>
            <li>
              <a href="dg4502005s.html#123">Dell'Armamentario et
              che cosa era ›Armamentaria‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#123">Dell'Esercito Romano
              di terra, e mare e loro insegne</a>
            </li>
            <li>
              <a href="dg4502005s.html#123">De' Trionfi et a chi
              si concedevano, et chi fu il primo trionfatore, et di
              quante maniere erano</a>
            </li>
            <li>
              <a href="dg4502005s.html#123">Delle corone, et a chi
              si davano</a>
            </li>
            <li>
              <a href="dg4502005s.html#124">Del numero del Popolo
              Romano</a>
            </li>
            <li>
              <a href="dg4502005s.html#124">Delle ricchezze del
              Popolo Romano</a>
            </li>
            <li>
              <a href="dg4502005s.html#124">Della liberalità degli
              antichi Romani</a>
            </li>
            <li>
              <a href="dg4502005s.html#125">Delli Matrimonij
              antichi, et loro usanza</a>
            </li>
            <li>
              <a href="dg4502005s.html#125">Della buona creanza,
              che davano a i figliuoli</a>
            </li>
            <li>
              <a href="dg4502005s.html#125">Della separatione de
              Matrimonij</a>
            </li>
            <li>
              <a href="dg4502005s.html#126">Dell'essequie antiche,
              et sue ceremonie</a>
            </li>
            <li>
              <a href="dg4502005s.html#127">Delle Torri</a>
            </li>
            <li>
              <a href="dg4502005s.html#127">Del ›Tevere‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#127">Del Palazzo Papale
              ›Palazzo Apostolico Vaticano‹, et di Belvedere
              ›Cortile del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#128">Del ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502005s.html#128">Recapitulatione
              dell'antichità</a>
            </li>
            <li>
              <a href="dg4502005s.html#129">De' Tempi degli
              Antichi fuori di Ro ma</a>
            </li>
            <li>
              <a href="dg4502005s.html#130">Quante volte è stata
              presa Roma</a>
            </li>
            <li>
              <a href="dg4502005s.html#130">De' fuochi de gli
              antichi, scritti da pochi Auttori cavati, da alcuni
              fragmenti e Historie</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
