<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zppig30231680s.html#8">Themis Dea, Sev De Lege
      Divina / Stephani Pighii Campensis. Ad Ampliss. Antonivm
      Perrenotum Cardinalem Granuellanum. Item Mythologia Eivsdem
      In Qvatuor anni partes, ab auctore recognita</a>
      <ul>
        <li>
          <a href="zppig30231680s.html#10">Stephani Pighii
          campensis themis dea, sev de lege divina</a>
        </li>
        <li>
          <a href="zppig30231680s.html#157">Auctorum catalogus
          quorum testimoniis in hoc Dialogo usi sumus</a>
        </li>
        <li>
          <a href="zppig30231680s.html#162">Mythologia, vel anni
          partes</a>
          <ul>
            <li>
              <a href="zppig30231680s.html#164">Reverendissimo et
              illustrissimo Atrebatensium Episcopo</a>
            </li>
            <li>
              <a href="zppig30231680s.html#221">Auctorum nomina
              qui in hoc opusculo citantur</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
