<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghnel42253530s.html#8">Discorsi Di Architettura /
      Del Senatore Giovan Batista Nelli. Con la Vita del medesimo
      ... . E due Ragionamenti sopra le Cupole / di Alessandro
      Cecchini Architetto</a>
      <ul>
        <li>
          <a href="ghnel42253530s.html#10">Al Signor Bindo Simone
          Peruzzi</a>
        </li>
        <li>
          <a href="ghnel42253530s.html#14">Vita del Senatore
          Giovan Battista Nelli&#160;</a>
        </li>
        <li>
          <a href="ghnel42253530s.html#36">Del fabbricarsi i
          ponti ne' fiumi della Toscana. Disorso del Senatore
          Giovan-Batista Nelli</a>
        </li>
        <li>
          <a href="ghnel42253530s.html#60">Ragionamento sopra la
          maniera di voltar le cupole senza adoperarvi le
          centine</a>
        </li>
        <li>
          <a href="ghnel42253530s.html#88">Due disorsi sopra la
          cupola di S. Maria del Fiore di Alessandro Cecchini
          Architetto&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
