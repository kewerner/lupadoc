<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="even16103330s.html#6">Descrizione di tutte le
      pubbliche pitture della città di Venezia e isole
      circonvicine</a>
      <ul>
        <li>
          <a href="even16103330s.html#8">Illustrissimo
          Signore</a>
        </li>
        <li>
          <a href="even16103330s.html#10">Proemio che contiene
          l'istoria della pittura viniziana</a>
        </li>
        <li>
          <a href="even16103330s.html#24">Compendio delle vite, e
          maniere de' più riguardevoli pittori</a>
        </li>
        <li>
          <a href="even16103330s.html#73">Avviso a' leggitori</a>
        </li>
        <li>
          <a href="even16103330s.html#76">Tavola de' Cognomi,
          Nomi, e Patrie</a>
        </li>
        <li>
          <a href="even16103330s.html#101">Sestier di San
          Marco</a>
          <ul>
            <li>
              <a href="even16103330s.html#205">Tavola del sestier
              di San Marco</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even16103330s.html#210">Sestier di
          Castello</a>
          <ul>
            <li>
              <a href="even16103330s.html#270">Tavola del sestier
              di Castello</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even16103330s.html#274">Sestier di San
          Paolo</a>
          <ul>
            <li>
              <a href="even16103330s.html#320">Tavola del Sestier
              di San Polo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even16103330s.html#323">Sestier di Dorso
          Duro</a>
          <ul>
            <li>
              <a href="even16103330s.html#385">Tavola del sestier
              di Dorsoduro</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even16103330s.html#388">Sestier di
          Canareggio</a>
          <ul>
            <li>
              <a href="even16103330s.html#435">Tavola del Sestier
              di Canareggio</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even16103330s.html#438">Sestier della
          Croce</a>
          <ul>
            <li>
              <a href="even16103330s.html#484">Tavola del Sestier
              della Croce</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even16103330s.html#487">Tavola universale de'
          luoghi oer Alfabetto</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
