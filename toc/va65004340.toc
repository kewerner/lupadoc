<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="va65004340s.html#6">Notizie risguardanti le
      accademie di belle arti, e di archeologia esistenti in
      Roma</a>
      <ul>
        <li>
          <a href="va65004340s.html#8">A sua altezza reale
          Carlotta Federica di Danimarca</a>
        </li>
        <li>
          <a href="va65004340s.html#10">L'editore</a>
        </li>
        <li>
          <a href="va65004340s.html#12">Prefazione</a>
        </li>
        <li>
          <a href="va65004340s.html#31">Indicazione</a>
        </li>
        <li>
          <a href="va65004340s.html#62">Accademie</a>
        </li>
        <li>
          <a href="va65004340s.html#74">Pittori di istoria e di
          ritratti</a>
        </li>
        <li>
          <a href="va65004340s.html#81">Scultori</a>
        </li>
        <li>
          <a href="va65004340s.html#85">Pittori di paesi</a>
        </li>
        <li>
          <a href="va65004340s.html#88">Architetti</a>
        </li>
        <li>
          <a href="va65004340s.html#91">Incisori</a>
        </li>
        <li>
          <a href="va65004340s.html#94">Miniatori</a>
        </li>
        <li>
          <a href="va65004340s.html#97">Scultori in metallo</a>
        </li>
        <li>
          <a href="va65004340s.html#98">Incisori in rame</a>
        </li>
        <li>
          <a href="va65004340s.html#101">Restauratori di
          quadri</a>
        </li>
        <li>
          <a href="va65004340s.html#101">Maestri di lingua
          italiana ed estera</a>
        </li>
        <li>
          <a href="va65004340s.html#103">Mosaicisti e negozj</a>
        </li>
        <li>
          <a href="va65004340s.html#108">Negozi di stampe</a>
        </li>
        <li>
          <a href="va65004340s.html#112">Maestri di musica</a>
        </li>
        <li>
          <a href="va65004340s.html#114">Calligrafi</a>
        </li>
        <li>
          <a href="va65004340s.html#114">Intagliatori in legno</a>
        </li>
        <li>
          <a href="va65004340s.html#115">Pietrari scalpellini</a>
        </li>
        <li>
          <a href="va65004340s.html#118">Legatore di scatole in
          pietra</a>
        </li>
        <li>
          <a href="va65004340s.html#118">Formatori in gesso</a>
        </li>
        <li>
          <a href="va65004340s.html#118">Astucciari</a>
        </li>
        <li>
          <a href="va65004340s.html#119">Negozi di venditori</a>
        </li>
        <li>
          <a href="va65004340s.html#120">Nozioni dei giornale
          artisitici</a>
        </li>
        <li>
          <a href="va65004340s.html#123">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
