<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebol672940s.html#8">Il Claustro di San Michele in
      Bosco di Bologna</a>
      <ul>
        <li>
          <a href="ebol672940s.html#10">[Dedica del autore]
          Serenissima Altezza</a>
        </li>
        <li>
          <a href="ebol672940s.html#12">[Dedica ai lettori]
          Lettore Gentilissimo</a>
        </li>
        <li>
          <a href="ebol672940s.html#12">[Dedica di Paulus
          Hieronymus Giacconus all'autore] Al Signore Giacomo
          Giovannini Disegnatore, et Intagliatore dell'opera</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ebol672940s.html#14">[Text]</a>
      <ul>
        <li>
          <a href="ebol672940s.html#6">[1.] ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#18">2. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#20">[2a.] ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#23">3. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#29">[Sonetto di Agostino
          Caracci] Sonetto in lode di Nicolò Bolognese&#160;</a>
        </li>
        <li>
          <a href="ebol672940s.html#30">4. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#32">5. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#35">6. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#42">7. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#44">8. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#47">9. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#54">10. ▣&#160;</a>
        </li>
        <li>
          <a href="ebol672940s.html#56">11. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#59">12. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#66">13. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#68">14. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#74">15. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#76">16. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#82">18. [sic!] ▣&#160;</a>
        </li>
        <li>
          <a href="ebol672940s.html#84">17. ▣</a>
        </li>
        <li>
          <a href="ebol672940s.html#90">[19.] ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
