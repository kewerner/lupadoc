<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katmrom1053090s.html#10">&#160;Musaeum
      Kircherianum</a>
      <ul>
        <li>
          <a href="katmrom1053090s.html#12">Excellentissime
          princeps</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#18">Index classium</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#22">Proemium</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#25">I Continens Idola,
          &amp; Instrumenta, ad Sacrificia Ethnicorum
          Spectantia.&#160;</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#104">II Continens
          Tabellas Votivas, &amp; Anathemata.</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#132">III Continens
          Sepulchra, &amp; Inscriptiones Sepulchrales.</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#188">IV Continens
          Lucernas Sepulchrales.&#160;</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#236">V Fragmenta eruditae
          Antiquitatis</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#290">VI Continens
          Lapides, Fossilia, aliasque glebas, à Natura effigie
          aliqua donatas.</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#322">VII Apparatus Rerum
          Peregrinarum, ex variis Orbis Plagis collectus.</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#356">VIII Exponuntur
          Plantae Marinae, frutices, &amp; Animalia tum Marina, tum
          Terrestria.</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#410">IX Instrumenta
          Mathematica.</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#430">X Indicantur Tabulae
          pictae signa marmorea, &amp; Numismata diversi
          Generis.</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#438">XI Continens
          micrographiam curiosam</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#534">XII Continens
          Animalia Testacea.&#160;</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#668">Index rerum
          notabilium.</a>
        </li>
        <li>
          <a href="katmrom1053090s.html#676">[Tavola]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
