<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="wavel3202580as.html#14">Mémoire de Velazquez sur
      quarante et un tableaux, envoyés par Philippe IV à
      l’Escurial</a>
      <ul>
        <li>
          <a href="wavel3202580as.html#16">Introduction</a>
        </li>
        <li>
          <a href="wavel3202580as.html#26">Mémoire sur les
          tableaux</a>
          <ul>
            <li>
              <a href="wavel3202580as.html#28">Mémoire de
              Velazquez</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="wavel3202580as.html#54">Memoria de las
          pintures</a>
          <ul>
            <li>
              <a href="wavel3202580as.html#56">Memoria de
              Velazquez</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
