<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="bb7802650s.html#6">Wegh-wyser door Italien, of
      beschrijvinge der landen en steden van Italien</a>
      <ul>
        <li>
          <a href="bb7802650s.html#8">Aen de Wel-Edele Heer De
          Heer Wilhelm de Beveren, Heeren Cornelisz</a>
        </li>
        <li>
          <a href="bb7802650s.html#14">Afscheit van Italien, op de
          Alpes gezongen</a>
        </li>
        <li>
          <a href="bb7802650s.html#16">Op den Wegh-Wyzer van D. L.
          v. Bos</a>
        </li>
        <li>
          <a href="bb7802650s.html#20">Vaersen op de Italiaensche
          Steden</a>
        </li>
        <li>
          <a href="bb7802650s.html#22">Aen de Beminders der
          Vreemdigheden</a>
        </li>
        <li>
          <a href="bb7802650s.html#30">Van Oudt Italien in 't
          gemeen</a>
        </li>
        <li>
          <a href="bb7802650s.html#42">Reys Door het Over-Alpische
          Vranckrijck, Picenum, Toscanen en Campagno di Roma, tot
          Rome toe&#160;</a>
        </li>
        <li>
          <a href="bb7802650s.html#352">Wegh van Milanen na
          Cremona, Mantua en Ferrara, tot Rimini toe</a>
        </li>
        <li>
          <a href="bb7802650s.html#405">Pesaro</a>
        </li>
        <li>
          <a href="bb7802650s.html#407">Fano</a>
        </li>
        <li>
          <a href="bb7802650s.html#410">Reyse van Fano tot
          Foligno</a>
        </li>
        <li>
          <a href="bb7802650s.html#413">Wegh van Fano op Foligno,
          en Roma, die beqummer, doch langer is&#160;</a>
        </li>
        <li>
          <a href="bb7802650s.html#506">Reyse van Roma nae
          Napels&#160;</a>
        </li>
        <li>
          <a href="bb7802650s.html#549">Wegh naer Pozzuolo</a>
        </li>
        <li>
          <a href="bb7802650s.html#608">Genoa</a>
        </li>
        <li>
          <a href="bb7802650s.html#620">Bladwyser</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
