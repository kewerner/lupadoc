<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="fa1703810c1s.html#8">Voyage pittoresque ou
      description des Royaumes de Naples et de Sicile première
      partie du premier volume</a>
      <ul>
        <li>
          <a href="fa1703810c1s.html#10">[Dédicace a la reine]
          ▣</a>
        </li>
        <li>
          <a href="fa1703810c1s.html#12">Avant-Propos</a>
        </li>
        <li>
          <a href="fa1703810c1s.html#18">Explication des
          fleurons, vignettes et ornemens répandus dans ce premier
          volume</a>
        </li>
        <li>
          <a href="fa1703810c1s.html#22">Table des chapitres
          contenus dans ce premier volume</a>
        </li>
        <li>
          <a href="fa1703810c1s.html#25">Traduction du passage
          de florus</a>
        </li>
        <li>
          <a href="fa1703810c1s.html#26">Peinture du Royaume de
          Naples</a>
        </li>
        <li>
          <a href="fa1703810c1s.html#29">Cartes des Royaumes de
          Naples et de Sicile, pays anciennement appellé Grande
          Grèce ◉</a>
        </li>
        <li>
          <a href="fa1703810c1s.html#32">Précis historique des
          révolutions de Naples et de Sicile</a>
          <ul>
            <li>
              <a href="fa1703810c1s.html#32">I. Chapitre</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#39">II. Chapitre</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#51">III. Chapitre</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#62">IV. Chapitre</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c1s.html#70">Voyage de Marseille a
          Naples</a>
        </li>
        <li>
          <a href="fa1703810c1s.html#88">I. Cartes géographiques
          du Royaume de Naples. Plan topographique, et vues
          générales de cette ville prises de différentes
          aspects</a>
          <ul>
            <li>
              <a href="fa1703810c1s.html#91">Carte de la
              première partie du Royaume de Naples ◉</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#95">Carte des environs
              de la ville et du Golfe de Naples ◉</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#98">Carte de la terre
              de Labour</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#99">Plan de la ville de
              Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#100">Plan de la ville
              de Naples ◉</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#105">Vues de la ville
              de Naples, prises sur le bord de la Mer</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#106">Première Vue de la
              ville de Naples, prise di Faubourg de Chiaya ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#108">Seconde Vue de la
              ville de Naples, prise du Bastion appellé: il
              Torrione del Carmine ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#111">Vue de la ville de
              Naples, prise du Palais de Capo di Monte ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#112">Vues de Naples,
              prise du Palais Capo di Monte</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#113">Vue de Naples,
              prise du Château Saint-Elme</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#114">Vue d'une partie
              de la Ville, et du Golfe de Naples, prise du Château
              Saint-Elme ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c1s.html#119">II. Églises de Naples,
          Palais, Tombeaux, et différentes vues prises dans
          l'interieur de cette ville</a>
          <ul>
            <li>
              <a href="fa1703810c1s.html#121">Église de
              Saint-Janvier</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#124">Vue de l'intérieur
              de l'Église Cathédrale de Saint-Janvier à Naples
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#127">Vue de l'Entrée et
              du Portail de l'Église de Saint Philippe de Néri à
              Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#128">Portail de
              l'Église de Saint-Philippe de Néri, à Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#129">Vue de l'intérieur
              de l'Église de Saint-Philippe de Néri</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#130">Vue intérieure de
              l'Église de Saint Philippe de Néri à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#133">Vue de l'intérieur
              du Cloître de la Chartreuse de Saint-Martin à Naples
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#134">Vue du Cloître des
              Chartreux à Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#135">Vue du Palais du
              Roi [et] vue de la Reine Jeanne, à Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#136">Vue de la Place et
              du Palais du Roi à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#139">Ruines d'un ancien
              Palais bâti par la Reine Jeanne près de Naples sur le
              bord de la Mer du côte du Pausilippe ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#141">Vue des catacombes
              de Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#142">1. Première Vue
              prise dans les Catacombes de Naples 2. Seconde vue
              dessinée d'après Nature dans les Catacombes de Naples
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#145">1. Vue du Mosle de
              Naples ou For de la Lanterne 2. Vue de l'Entrée de la
              Grotte de Pausilippe, prise eu y arrivans du côte de
              Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#146">Vues de la Grotte
              de Pausilippe et du Môle de Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#148">1. Vue de l'entrée
              de la Grotte du Pausilippe près de Naples 2. Vue d'un
              Chemin Creux qui conduit à la Grotte du Pausilippe
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#151">1. Vue du Temple
              de Venus, située sur le bord de la Mer dans le Golphe
              de Baijes près de Pouzzols 2. Vue du Tombeau de
              Virgile, près de Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#152">Vue du Tombeau de
              Virgile</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#155">1. Tombeau de
              Sannazar dans l'Église de Saint-Maria del Parto 2.
              Tombeau de Jean de Carracioli Grand Sénéchal du
              Royaume de Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#156">Tombeaux de Jean
              de Carracioli, Grand Sénéchal du Royaume de Naples et
              du Poète Sannasar</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#159">Tombeau du Roi
              André</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#160">1. Tombeau du Roi
              André mari de la fameuse Reine Jeanne de Naples 2.
              Vue d'un Caveau découvert à Pompeii près du
              Vesuve</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#163">1. Tombeau de
              Ferdinand d'Avalos Monsieur de Pescaire Église de San
              Domenico Grande à Naples 2. Tombeau d'Odel de Foix et
              Maréchal de Lautrec Église de Santa Maria la Nuova à
              Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#164">Tombeaux du
              Maréchal Comte de Lautrec et du Marquis de
              Pescaire</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#165">Vue de l'Hopital
              de l'Annonziata, à Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#166">1. Vue de la
              fontaine de l'Hopital de l'Annonziata, et d'une des
              Portes de Naples appellée Porta Nolana 2. Vue prise
              au dessus de la Grotte de Pausilippe ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#169">1. Vue dessinée
              d'après Nature le long de la Côte de Pausilippe à
              Naples 2. Grotte creusée dans la Montagne de
              Pizzofalcone à Naples appellée la Grotte des Cordiers
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#170">Vue de la Grotte
              des Cordiers, à Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#171">Vue du Palais de
              la Roccella</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#172">1. Vue du Vesuve
              prise du côte de Mare Piano près de Pouzzuole 2. Vue
              d'un ancien Palais du Prince della Rocella situé sur
              le bord de la Mer à Naples près du Palais de la reine
              Jeanne ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#175">Vue d'une partie
              de la Coste de Pausilippe, prise du dessus de la
              Grotte de Pouzzoles ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#177">Vue de l'Extrémité
              du Luay et du fauxbourg de Chiaia à &#160;Naples
              prise de l'endroit appellé Capo di Mergellina ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#178">Vue de la côte de
              Pausilippe [et] vue du quai de Pausilippe</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c1s.html#180">III. Tableaux et
          Peintures les plus remarquables des Églises et Palais de
          Naples</a>
          <ul>
            <li>
              <a href="fa1703810c1s.html#182">Choix de quelques
              Peintures et Tableaux des Églises et Pailais de
              Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#193">Héliodore chassé
              du Temple, Peinture de Solimène, dans l'Église de
              Giesu-Nuovo, à Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#194">Héliodore chassé
              du Temple ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#197">Les Vendeurs
              chassés du Temple ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#198">Les Vendeurs
              chassés du Temple, composition peinte par Luca
              Giordano, dans l'Église de Saint-Philippe de Néri</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#201">1. Tableau du
              Schidone dans le Palais de Capo di Monte à Naples 2.
              Tableau de l'Espagnolet dans la Sacristie de l'Église
              des Chartreux à Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#202">Tableaux de
              l'Espagnolet et du Schedone, à Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#203">Peintures de
              l'Espagnolet dans l'Église des Chartreux [et]
              Tableaux de Luca Giordano</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#204">Prophetes on
              Apôtres peints par l'Espagnolet dans les Archivoltes
              de la Nef de l'Église des Chartreux à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#206">1. Tableau de Luca
              Giordano qui est dans l'Église du Saint-Esprit à
              Naples 2. Tableau de Luca Giordano qui est dans
              l'Église des Saints Apôtres à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#209">Compositions
              peintes par le Lanfranc dans les Pendantifs du Dôme
              des Saints Apôtres à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#210">Peintures du
              Lanfranc aux Saints Apôtres</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#211">Tableaux du
              Poussin, pris dans le Palais de La Torré, à
              Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#212">Tableaux du
              Poussin aus Palais des Ducs Corre à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#215">Plafond peint par
              Solimène dans la Scristie de l'Église des Dominiquins
              à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#216">Plafond peint par
              Solimène dans la Sacristie de l'Église des
              Dominicains à Naples</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#217">Tableaux du
              Schedone et du Poussin</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#218">1. Tableau du
              poussin, Palais Cozze à Naples 2. Tableau de
              Schedone, Palais de Capo di Monte à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#221">Plafonds peints
              par le Cavalier Mattia Preti, connu sous le nom du
              Calabrese ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#222">Plafonds du
              Calabrèse [et] Peintures du Dominiquin et d'Annibal
              Carrache</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#224">1. Un des Angles
              de la Coupole de la Chapelle de Saint-Janvier dans
              l'Église Cathédrale de Naples 2. Peint par Annibal
              Carrache au Palais de Capo di Monte à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#227">Figures
              allégoriques peintes par Solimène dans la Sacristie
              de l'Église de Saint Paul à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#229">Figures
              allégoriques peintes par Solimène dans la Sacristie
              de l'Église de Saint Paul à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#230">Peintures
              allégoriques de Solimène</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c1s.html#232">IV. Poètes et
          Musiciens célèbres, de Naples; avec unte notice abrégée
          sur leurs vies et leurs ouvrages</a>
          <ul>
            <li>
              <a href="fa1703810c1s.html#234">Des Poètes
              Napolitains les plus célèbres</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#236">Précis de la vie
              du Tasse</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#246">De l'Aminte du
              Tasse</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#248">Imitation de
              l'Aminte. Chœur a la fin du première acte</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#250">Précis de la vie
              du Cavaliere Marini</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#252">Analyse du poème
              de l'Adone</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#258">Précis de la vie
              de Sannazar, avec une notice sommaire de ses
              ouvrages</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#263">Précis de la
              Thébaïde de Stace. Poème</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#268">Du Poète Ovide</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#272">Des Musiciens de
              Naples les plus célèbres</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c1s.html#282">V. Vues et
          Descriptions du Vesuve et de ses environs; avec
          l'historie abrégée de ses éruptions, depuis l'an 79
          jusqu'en 1780</a>
          <ul>
            <li>
              <a href="fa1703810c1s.html#284">Essai sur le
              Vésuve, ou histoire abrégée de ses éruptions les plus
              memorables</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#319">Vue de la Sommité
              et du Crater du Vesuve, au moment de la dernière
              Eruption arrivée le 8 d'août 1779, à 9 heures du soir
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#321">Vue du Vesuve et
              d'une partie du Golphe de Naples prise de l'endroit
              appellée Dogana di Cezza près le Pous de la ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#322">Vue du Vésuve dans
              un état de calme</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#323">Vue du Vésuve en
              Éruption</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#324">Éruption du Mont
              Vésuve, du 14. Mai 1771 ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#327">Vue du Vésuve,
              prise du Mont Sant-Angelo, où est située une Maison
              de Camaldules</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#328">Vue du Vésuve,
              prise du Mont Sant-Angelo, où est située une Maison
              de Camaldules ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#331">Vue du Bourg et du
              Village de Torre dell'Annonziata et de Torre del
              Graeco, situés au pied du Vésuve</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#332">Vue du Village et
              de la Porte de Torre dell'Annonziata au pied di
              Vesuve ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#334">Vue de la grande
              Rue du Bourg de l'Annonziata au pied du Vesuve ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#336">Vue du Bourg de
              Torre del Greco situé au pied du Vesuve ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#339">Vue de la Maison
              de Solimène sur le Vésuve</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#340">Maison de Campagne
              de Solimène située près du Vesuve</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#343">1. Vue du Vesuve
              prise sur le bord de la Mer et du côté de Portici 2.
              Vue de l'église de Resina, petit Village situé au
              pied du Vesuve ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#344">Vue du Village de
              Resina situé au pied du Vésuve, avec une autre vue de
              ce Volcan, prise sur le bord de la Mer</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#345">Vue des Laves du
              Vésuve, prise sur le bord de la Mer près de
              Portici</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#346">Vue des Laves
              anciennement sorties du Vesuve ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c1s.html#350">VI. Des Usages, du
          Caractère et des Costumes des Napolitains, avec une idée
          du Gouvernement, du Commerce et des Productions
          naturelles du Royaume de Naples</a>
          <ul>
            <li>
              <a href="fa1703810c1s.html#352">De la Noblesse
              Napolitaine, du Clergé et de la Bourgeoisie de
              Naples, ainsi que de quelques Usages des ces
              différentes Classes de Citoyens</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#360">Des Environs de
              Naples et de ses Productions naturelles, de son
              Commerce et de l'Industrie de ses Habitants</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#365">Du Caractère, des
              Mœurs des Napolitains, et en particulier de la Partie
              du Peuple de Naples appellée les Lazaroni</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#369">Des Costumes du
              Peuple de Naples et de ses Fêtes de Madone au Tems de
              Noël</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#370">1. Danse de la
              Carantele à Capo di Pausilipo près de Naples 2.
              Concert de Calabrois devant une Madone à Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#373">Mazanielle
              haranguant le Peuple de Naples pendant la fameuse
              Sédition de 1647</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#374">Mazanielle
              haranguant le Peuple de Naples dans la Place du
              Marché des Carmes ▣</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#382">Fêtes du Carnaval
              à Naples. Pillage de la Cocagne</a>
            </li>
            <li>
              <a href="fa1703810c1s.html#384">Vue du Pillage de
              la Cocagne, à Naples dans la Place appellée il Largo
              del Castello ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
