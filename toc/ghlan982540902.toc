<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlan982540902s.html#8">Storia pittorica della
      Italia; tomo II. Ove si descrive la scuola romana e
      napolitana&#160;</a>
      <ul>
        <li>
          <a href="ghlan982540902s.html#10">Compartimento di
          questo tomo secondo</a>
        </li>
        <li>
          <a href="ghlan982540902s.html#12">Della storia
          pittorica della Italia inferiore</a>
          <ul>
            <li>
              <a href="ghlan982540902s.html#12">III. Scuola
              romana</a>
              <ul>
                <li>
                  <a href="ghlan982540902s.html#19">I. Gli
                  antichi&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540902s.html#51">II.
                  Raffaello e la sua scuola&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540902s.html#112">III. La
                  pittura dopo le pubbliche sciagure di Roma va
                  decadendo, e sempre più di poi si
                  ammaniera&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540902s.html#154">IV. Il
                  Barocci ed altri, parte dello stato, parte
                  esteri, riconducono il buon gusto nella scuola
                  romana&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540902s.html#223">V. I
                  Cortoneschi male imitando Pietro pregiudicano
                  alla pittura. Il Maratta ed altri la
                  sostengono&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ghlan982540902s.html#292">IV. Scuola
              napolitana</a>
              <ul>
                <li>
                  <a href="ghlan982540902s.html#292">I. Gli
                  antichi&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540902s.html#311">II. Dalla
                  scuola di Raffaello e da quella di Michelangiolo
                  si deriva in Napoli il moderno stile&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540902s.html#328">III. Il
                  Corenzio, il Ribera, il Caracciolo primeggiano in
                  Napoli. Forestieri che competerono con
                  loro&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540902s.html#359">IV. Il
                  Giordano, il Solimene, e gli allievi
                  loro&#160;</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
