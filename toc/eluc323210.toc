<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="eluc323210s.html#6">Vincentii Marchio Il
      forestiere informato delle cose di Lucca</a>
      <ul>
        <li>
          <a href="eluc323210s.html#8">Illustrissimo Sig. Sig.
          Prone Colendiss.</a>
        </li>
        <li>
          <a href="eluc323210s.html#13">A chi legge</a>
        </li>
        <li>
          <a href="eluc323210s.html#15">Sommarj, e Tavola de i
          Capitoli di tutta l'Opera</a>
        </li>
        <li>
          <a href="eluc323210s.html#22">Il forestiere informato
          delle cose di Lucca</a>
        </li>
        <li>
          <a href="eluc323210s.html#320">Note</a>
        </li>
        <li>
          <a href="eluc323210s.html#324">Calendario</a>
        </li>
        <li>
          <a href="eluc323210s.html#335">Tavola de i Pittori, e
          degli scultori</a>
        </li>
        <li>
          <a href="eluc323210s.html#339">Tavola generale</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
