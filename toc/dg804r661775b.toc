<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg804r661775bs.html#10">Roma antica, media, e
      moderna</a>
      <ul>
        <li>
          <a href="dg804r661775bs.html#12">Giornata prima, da
          Ponte Sant'Angelo a San Pietro in Vaticano</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#37">Giornata seconda, da
          Santo Spirito per il Trastevere</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#55">Giornata terza, da
          strada Giulia all'Isola di San Bartolomeo</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#67">Giornata quarta, da
          San Lorenzo in Damaso al Monte Aventino</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#83">Giornata quinta,
          dalla Piazza di Monte Giordano per i Mento Celio, e
          Palatino</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#110">Giornata sesta, da
          Salvatore in Lauro per Campidoglio, e per le Carine</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#135">Giornata settima,
          dalla Piazza di Sant'Agostino per i Monti Viminale, e
          Quirinale</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#173">Giornata ottava,
          dalla strada dell'Orso a Monte Cavallo, e alle Terme
          Diocleziano</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#185">Giornata nona, dal
          Palazzo Borghese a Porta del Popolo, a Piazza di
          Spagna</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#200">Giornata decima,
          dal Monte Citorio alla Porta Pia, e al Monte Pincio</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#216">Cronologia di tutti
          li sommi Pontefici</a>
        </li>
        <li>
          <a href="dg804r661775bs.html#230">Indice delle cose
          piu notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
