<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dm51534702s.html#10">Joannis Ciampini romani
      Vetera monimenta, in quibus praecipuè musiva opera sacrarum,
      profanarumque aedium structura, ac nonnulli antiqui ritus,
      dissertationibus, iconibusque illustrantur, secunda pars</a>
      <ul>
        <li>
          <a href="dm51534702s.html#12">Eminentissimo ac
          reverendissimo Principi Carolo Victorio Amedeo</a>
        </li>
        <li>
          <a href="dm51534702s.html#24">Joannis Ciampini Romani
          Vetera Monimenta</a>
        </li>
        <li>
          <a href="dm51534702s.html#317">Index rerum
          memorabilium</a>
        </li>
        <li>
          <a href="dm51534702s.html#327">Appendix ad Joannis
          Ciampini veterum monimentorum</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
