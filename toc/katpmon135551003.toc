<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katpmon135551003s.html#6">The Mond Collection [3]
      Portfolio : list of plates</a>
    </li>
    <li>
      <a href="katpmon135551003s.html#8">[Index]</a>
    </li>
    <li>
      <a href="katpmon135551003s.html#10">[The Venetian
      School]</a>
      <ul>
        <li>
          <a href="katpmon135551003s.html#10">I. Giovanni
          Bellini, Pietà</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#12">II. &#160;Giovanni
          Bellini, Virgin and Child</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#14">III. Gentile
          Bellini, Enthroned Virgin and Child</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#16">IV. Cima, Saint
          Sebastian</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#18">V. Cima, Saint
          James</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#20">VI. Catena, Virgin
          and Child with Saints and Donors</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#22">VII. Bissolo,
          Madonna and Child with Saints</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#24">VIII. Palma,
          Portrait of a Lady</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#26">IX. Titian,
          Madonna and Child</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#28">X. Polidoro da
          Lanzano, Holy Family with Infant, Saint John and
          Donor</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#30">XI. Giuseppe
          Porta, Justice</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#32">XII. Sebastiano
          Ricci, Rape of Briseis</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#34">XIII. Diziani,
          Flight into Egypt: Riposo</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#36">XIV. Alessandro
          Longhi, Portrait of the Wife of Temanza</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="katpmon135551003s.html#38">[Mantegna and the
      Veronese School]</a>
      <ul>
        <li>
          <a href="katpmon135551003s.html#38">XV. Mantegna,
          Imperator Mundi</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#40">XVI. Girolamo dai
          Libri, Saint Peter and Saint John</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#42">XVII. Girolamo dai
          Libri, Adoration of the Infant Christ</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#44">XVIII. Torbido,
          Portrair of Fracastoro</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="katpmon135551003s.html#46">[Leonardo da Vinci and
      the Lombard School]</a>
      <ul>
        <li>
          <a href="katpmon135551003s.html#46">XIX. Leonardo da
          Vinci, Head of the Virgin</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#48">XX. Luini, Madonna
          and Child with Infant Saint John</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#50">XXI. Luini, Saint
          Catherine [of Alexandria]</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#52">XXII. Luini,
          Venus</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#54">XXIII. Gaudenzio
          Ferrari, Saint Andrew</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#56">XXIV. Sacchi,
          Saint Paul</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#58">XXV. Sodoma,
          Madonna and Child</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#60">XXVI. Sodoma,
          Saint Jerome</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#62">XXVII. Boltraffio,
          Portrait of a Man</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#64">XXVIII.
          Giampietrino, Salome</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="katpmon135551003s.html#66">[Tuscan Schools]</a>
      <ul>
        <li>
          <a href="katpmon135551003s.html#66">XXX. Botticelli,
          Miracles of Saint Zenobius</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#68">XXXI. Tuscan
          School, Portrait of a Lady</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#70">XXXII. Fra
          Bartolommeo, The Adoration of the Child</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#72">XXXIII. Italian
          School, Portrait of Alberto Pio</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="katpmon135551003s.html#74">[Umbrian School]</a>
      <ul>
        <li>
          <a href="katpmon135551003s.html#74">XXXIV. Raphael,
          The Crucifixion</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#76">XXXV. Raphael,
          Head of Christ; Detail from the Crucifixion</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#78">XXXVI. Raphael,
          Head of Saint John; Detail from the Crucifixion</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#80">XXXVII. Genga;
          Coriolanus with Volumnia and Veturia</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="katpmon135551003s.html#82">[Bolognese and
      Ferrarese Schools]</a>
      <ul>
        <li>
          <a href="katpmon135551003s.html#82">XXXVIII. Francia,
          Virgin and Child with an Angel</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#84">XXXIX. Garofalo,
          Sacrifice to Ceres</a>
        </li>
        <li>
          <a href="katpmon135551003s.html#86">XL. Dosso Dossi,
          Adoration of the Magi</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="katpmon135551003s.html#88">[Spanish School]</a>
      <ul>
        <li>
          <a href="katpmon135551003s.html#88">XLI. Murillo,
          Saint John the Baptist</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
