<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghpas69713950s.html#6">Del metodo di studiare la
      pittura, e delle cagioni di sua decadenza, tomo I.</a>
      <ul>
        <li>
          <a href="ghpas69713950s.html#8">A sua Altezza Nicola
          Esterhazy de Galanta&#160;</a>
        </li>
        <li>
          <a href="ghpas69713950s.html#18">L'autore a chi
          legge</a>
        </li>
        <li>
          <a href="ghpas69713950s.html#38">Dialoghi sopra la
          decadenza della pittura</a>
          <ul>
            <li>
              <a href="ghpas69713950s.html#38">Dialogo I. Mengs,
              e l'Ab. Winckelmann&#160;</a>
            </li>
            <li>
              <a href="ghpas69713950s.html#87">Dialogo II.
              Winckelmann, e Mengs</a>
            </li>
            <li>
              <a href="ghpas69713950s.html#137">Dialogo III.
              Mengs, e Winckelmann</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghpas69713950s.html#198">Del metodo di studiare la
      pittura, e delle cagioni di sua decadenza, tomo II.</a>
      <ul>
        <li>
          <a href="ghpas69713950s.html#200">Dialoghi sopra la
          decadenza della pittura</a>
          <ul>
            <li>
              <a href="ghpas69713950s.html#200">Dialogo IV.
              Mengs, e Winckelmann</a>
            </li>
            <li>
              <a href="ghpas69713950s.html#264">Dialogo V. Mengs,
              e Winckelmann</a>
            </li>
            <li>
              <a href="ghpas69713950s.html#326">Dialogo VI.
              Winckelmann, e Mengs</a>
            </li>
            <li>
              <a href="ghpas69713950s.html#390">Piano di
              regolamenti, e statuti&#160;</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
