<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghcar16452330s.html#6">Diálogos de la pintura su
      defensa, origen, essencia, definicion, modos y
      diferencias</a>
      <ul>
        <li>
          <a href="ghcar16452330s.html#8">Senor</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#10">Aprobacion</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#12">Aprobacion de Iulio
          Cesar Firrusino</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#17">El Maestro Ioseph de
          Valdiuielso</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#20">A los lectores</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#24">Dialogos de la pintura
          entre maestro, y dicipulo</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#24">Dialogo primero</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#74">Dialogo segundo del
          origen de la pintura</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#98">Dialogo tercero de la
          definicion y essencia de la pintura, y sus
          diferencias</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#116">Dialogo quarto de la
          pintura teorica, de la practica, y simple imitacion
          [...]</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#152">Dialogo quinto
          tratase del modo del juzgar de las pinturas [...]</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#190">Dialogo sexto trata
          de las diferencias de modos de pintar y si se puede
          oividar [...]</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#236">Dialogo septimo de
          las diferencias, y modos de pintar los sucessos y
          historias [...]</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#282">Dialogo octavo de lo
          practico del arte [...]</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#348">Memorial informatorio
          por los pintores</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#355">Del licenciado
          Antonio de Leon</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#376">Engracia del arte
          noble de la pintura</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#392">Dicho y deposicion de
          Don Lorenco Vanderhamen y Leon</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#399">Don Ivan de
          Iauregui</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#428">Por los pintores y su
          exempcion</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#462">Parecer del Doctor
          Ivan Rodriguez de Leon</a>
        </li>
        <li>
          <a href="ghcar16452330s.html#482">Tabla de las cosas
          notables, que se contienen en este libro</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
