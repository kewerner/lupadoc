<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbal15253650s.html#6">Raccolta di alcuni opuscoli
      sopra varie materie di pittura, scultura e architettura</a>
      <ul>
        <li>
          <a href="ghbal15253650s.html#8">Al molto illustre ed
          onorando Signore il Signore Cosimo Siries</a>
        </li>
        <li>
          <a href="ghbal15253650s.html#12">Indice degli
          Opuscoli</a>
        </li>
        <li>
          <a href="ghbal15253650s.html#14">Lettera di Filippo
          Baldinucci Fiorentino nella quale risponde ad alcuni
          quesiti in materie di Pittura</a>
          <ul>
            <li>
              <a href="ghbal15253650s.html#16">Illustrissimo e
              clarissimo Signore mio padrone colendissimo</a>
            </li>
            <li>
              <a href="ghbal15253650s.html#38">La Veglia</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghbal15253650s.html#78">Lezione di Filippo
          Baldinucci nell'Accademia della Crusca illustrato</a>
          <ul>
            <li>
              <a href="ghbal15253650s.html#80">Serenissimo Signor
              Principe</a>
            </li>
            <li>
              <a href="ghbal15253650s.html#82">Lezione
              accademica</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghbal15253650s.html#110">Lettera di Filippo
          Baldinucci a Lorenzo Gualtieri</a>
        </li>
        <li>
          <a href="ghbal15253650s.html#118">Eccellenza della
          Statua del San Giorgio di Dnatello</a>
          <ul>
            <li>
              <a href="ghbal15253650s.html#120">Al serenissimo
              Cosimo de' Medici Granduca di Toscana</a>
            </li>
            <li>
              <a href="ghbal15253650s.html#122">Alla Accademia
              fiorentina del disegno&#160;</a>
            </li>
            <li>
              <a href="ghbal15253650s.html#124">Ragionamento</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
