<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501690s.html#6">Le Antichità Della Citta Di
      Roma</a>
      <ul>
        <li>
          <a href="dg4501690s.html#8">Al magnanimo et eccellente
          Signore il S. Ottavio Sammarco</a>
        </li>
        <li>
          <a href="dg4501690s.html#12">All'illustrissimi et
          eccellentissimo Signor il Signor Don Francesco de' Medici
          Principe di Fiorenze, e di Siena</a>
        </li>
        <li>
          <a href="dg4501690s.html#18">Giovanni Varisco a'
          lettori</a>
        </li>
        <li>
          <a href="dg4501690s.html#20">Di M. Benedetto
          Varchi&#160;</a>
        </li>
        <li>
          <a href="dg4501690s.html#22">Il primo libro</a>
          <ul>
            <li>
              <a href="dg4501690s.html#22">Del luogo dove fu
              edificata Roma, e del vario accresciemtno d'essa,
              incominciando da Romulo&#160;</a>
            </li>
            <li>
              <a href="dg4501690s.html#40">Delle colle des
              Campidoglio, prima detto Capitolino</a>
            </li>
            <li>
              <a href="dg4501690s.html#58">Del Foro Romano et de
              gli altri Fori e edificij che vi sono appresso</a>
            </li>
            <li>
              <a href="dg4501690s.html#135">Del colle Palatino</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501690s.html#140">Il secondo libro</a>
          <ul>
            <li>
              <a href="dg4501690s.html#140">Del Foro Olitorio e
              Boario, e di tutto quello che è restato nella valle,
              che è tra il Campidoglio e il Palatino</a>
            </li>
            <li>
              <a href="dg4501690s.html#198">Del colle Aventino</a>
            </li>
            <li>
              <a href="dg4501690s.html#204">Del Monte Celio et
              Celiolo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501690s.html#217">Il terzo libro</a>
          <ul>
            <li>
              <a href="dg4501690s.html#217">Del colle
              dell'Esquilie</a>
            </li>
            <li>
              <a href="dg4501690s.html#263">Del Quirinale et del
              colle de gli Hortoli</a>
            </li>
            <li>
              <a href="dg4501690s.html#318">Del Campo Martio</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501690s.html#356">Il quarto libro</a>
          <ul>
            <li>
              <a href="dg4501690s.html#356">Del Trastevere</a>
            </li>
            <li>
              <a href="dg4501690s.html#390">Del Vaticano</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
