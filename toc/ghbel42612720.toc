<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbel42612720s.html#8">Le vite de’ pittori,
      scultori et architetti moderni; Tomo I.</a>
      <ul>
        <li>
          <a href="ghbel42612720s.html#10">Illustrissimo et
          eccellentissimo Signore</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#13">Lettore</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#16">Filostrato il
          Giovine</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#17">Tavola delle Vite
          descritte in questa prima Parte</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#18">L'idea del pittore,
          dello scultore, e dell'archtietto</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#32">[Vite]</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#32">Vita di Annibale
          Caracci</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#116">Vita di Agostino
          Caracci</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#150">Vita di Domenico
          Fontana</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#178">VIta di Federico
          Barocci</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#210">Vita di Michelangelo
          Merisi da Caravaggio</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#230">Vita di Pietro Paolo
          Rubens</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#262">VIta di Antonio van
          Dyck d'Anversa</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#278">Vita di Francesco di
          Quesnoy</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#298">Vita di Domenico
          Zampieri</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#374">Vita di Giovanni
          Lanfranco</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#396">Vita di Alessandro
          Algardi</a>
        </li>
        <li>
          <a href="ghbel42612720s.html#418">Vita di Nicolò
          Pussino</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
