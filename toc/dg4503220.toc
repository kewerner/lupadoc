<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503220s.html#8">Roma ricercata nel suo sito</a>
      <ul>
        <li>
          <a href="dg4503220s.html#10">[Dedica al Monsignor Carlo
          Collicola] Illustrissimo, et Reverendissimo Signore
          [...]</a>
        </li>
        <li>
          <a href="dg4503220s.html#12">[Dedica al] Lettore
          Forstiero</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503220s.html#16">[Text]</a>
      <ul>
        <li>
          <a href="dg4503220s.html#16">Giornata Prima</a>
          <ul>
            <li>
              <a href="dg4503220s.html#16">Da ›Ponte Sant'Angelo‹
              à ›San Pietro in Vaticano‹</a>
              <ul>
                <li>
                  <a href="dg4503220s.html#16">[›Castel
                  Sant'Angelo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#23">[›San Pietro in
                  Vaticano: Baldacchino di San Pietro‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#24">[›San Pietro in
                  Vaticano: Cattedra di San Pietro‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#25">[›San Pietro in
                  Vaticano: Mosaico della Navicella‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#30">Giornata Seconda</a>
          <ul>
            <li>
              <a href="dg4503220s.html#30">Per
              &#160;›Trastevere‹</a>
              <ul>
                <li>
                  <a href="dg4503220s.html#30">Palazzo di Santo
                  Spirito ›Palazzo del Banco di Santo Spirito‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#31">[›San Pietro in
                  Vaticano‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#38">›Ospizio Apostolico
                  di San Michele a Ripa Grande (ex)‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#39">›Dogana nuova di
                  Terra‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#40">›Santa Cecilia in
                  Trastevere‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#42">Giornata Terza</a>
          <ul>
            <li>
              <a href="dg4503220s.html#42">Da strada Giulia ›Via
              Giulia‹ al Palazzo di Farnese ›Palazzo Farnese‹ ▣, et
              all'Isola di San Bartolomeo ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4503220s.html#51">›San Bartolomeo
              all'Isola‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#52">Giornata Quarta</a>
          <ul>
            <li>
              <a href="dg4503220s.html#52">Da ›San Lorenzo in
              Damaso‹ ▣, ò Cancellaria ›Palazzo della Cancelleria‹
              ▣ al Monte ›Aventino‹</a>
            </li>
            <li>
              <a href="dg4503220s.html#57">›Porta San Paolo‹ ▣
              [›Piramide di Caio Cestio‹] ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#58">›Terme di
              Caracalla‹</a>
            </li>
            <li>
              <a href="dg4503220s.html#58">[›Anfiteatro
              Castrense‹] ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#60">Chiesa, e Convento di
              ›Sant'Alessio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#61">Giornata Quinta</a>
          <ul>
            <li>
              <a href="dg4503220s.html#61">Dalla Piazza di ›Monte
              Giordano‹, à quella di Pasquino ›Piazza di Pasquino‹
              per li Monti ›Celio‹, e ›Palatino‹ [›Statua di
              Pasquino‹] ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#64">Palazzo de'Savelli
              ›Palazzo Orsini‹ ▣ [›Teatro di Marcello‹] ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#66">Portico detto Giano
              Quadrifronte ›Arco di Giano‹ ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#71">Palazzo Maggiore
              Imperiale ›Palatino‹ ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#73">Chiesa di ›Santo
              Stefano Rotondo‹ ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#77">Campo Vaccino ›Foro
              Romano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#80">Giornata Sesta</a>
          <ul>
            <li>
              <a href="dg4503220s.html#80">Da San Salvatore del
              lauro a ›Campidoglio‹ ▣, e per le ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4503220s.html#82">›Piazza Navona‹ ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#86">Chiesa d'Aracoeli
              ›Santa Maria in Aracoeli‹ ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#87">›Tempio della
              Concordia‹ ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#90">Tempio di Pace ›Foro
              della Pace‹ ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#91">›Arco di Costantino‹
              ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#92">Il ›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dg4503220s.html#98">›Colonna Traiana‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#100">Giornata Settima</a>
          <ul>
            <li>
              <a href="dg4503220s.html#100">Dalla ›Piazza di
              Sant'Agostino‹ per i Monti ›Viminale‹, e
              ›Quirinale‹</a>
              <ul>
                <li>
                  <a href="dg4503220s.html#100">Chiesa di
                  ›Sant'Agostino‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#102">Chiesa di ›San
                  Luigi dei Francesi‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#103">Palazzo de' Medici
                  in Piazza Madama ›Palazzo Madama‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#104">Chiesa, e Convento
                  della Minerva ›Santa Maria sopra Minerva‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#107">›Porta Maggiore‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#111">Palazzo della
                  Curia Innocenziana nel Monte Citorio ›Palazzo
                  Montecitorio‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#114">Monte Cavallo
                  ›Piazza del Quirinale‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#116">›Fontana di Piazza
                  della Rotonda‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#117">La Rotonda
                  ›Pantheon‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#118">Giornata Ottava</a>
          <ul>
            <li>
              <a href="dg4503220s.html#118">Da ›Piazza Nicosia‹ à
              Monte Cavallo ›Quirinale‹, et alle Terme Diocletiane
              ›Terme di Diocleziano‹</a>
              <ul>
                <li>
                  <a href="dg4503220s.html#118">Aqua Foelix
                  ›Fontana del Mosè‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#118">Victoriae ›Santa
                  Maria della Vittoria‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#122">Dogana nuova a
                  Piazza di Pietra ›Dogana nuova di Terra‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#126">›Santa Maria degli
                  Angeli‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#129">Colonna Coclide
                  d'Antonino Imperatore ›Colonna di Antonino Pio‹
                  ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#130">Giornata Nona</a>
          <ul>
            <li>
              <a href="dg4503220s.html#130">Dalla Piazza del
              Principe Borghese ›Piazza Borghese‹ &#160;alle Porte
              del Popolo ›Porta del Popolo‹, e Pinciana ›Porta
              Pinciana‹</a>
              <ul>
                <li>
                  <a href="dg4503220s.html#130">›Palazzo Borghese‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#131">›Porto di Ripetta‹
                  ▣</a>
                  <ul>
                    <li>
                      <a href="dg4503220s.html#131">[›San Girolamo
                      degli Illirici‹] ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4503220s.html#132">Le due Chiese del
                  Cardinal Gastaldi ›Santa Maria dei Miracoli‹ ▣
                  ›Santa Maria in Montesanto‹ ▣ › ›Fontana di
                  Piazza del Popolo / Obelisco Flaminio‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#136">Porta Flaminia,
                  oggi detta del Popolo ›Porta del Popolo‹ ▣ ›Santa
                  Maria del Popolo‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#144">›Trinità dei
                  Monti‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503220s.html#145">Giardino de'
                  Medici ›Villa Medici‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#146">Giornata Decima</a>
          <ul>
            <li>
              <a href="dg4503220s.html#146">Per le Nove Chiese</a>
              <ul>
                <li>
                  <a href="dg4503220s.html#146">Chiesa di ›San
                  Pietro in Vaticano‹</a>
                  <ul>
                    <li>
                      <a href="dg4503220s.html#146">›San Pietro in
                      Vaticano: Cattedra di San Pietro‹ ▣</a>
                    </li>
                    <li>
                      <a href="dg4503220s.html#150">A ›San Paolo
                      fuori le Mura‹</a>
                      <ul>
                        <li>
                          <a href="dg4503220s.html#152">Chiesa di
                          ›San Paolo fuori le Mura‹ ▣</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="dg4503220s.html#154">Alle Tre
                      Fontane ›Abbazia delle Tre Fontane‹</a>
                    </li>
                    <li>
                      <a href="dg4503220s.html#155">All'Annunziata
                      ›Annunziatella‹</a>
                    </li>
                    <li>
                      <a href="dg4503220s.html#155">A ›San
                      Sebastiano‹</a>
                      <ul>
                        <li>
                          <a href="dg4503220s.html#156">Chiesa di
                          ›San Sebastiano‹ ▣</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="dg4503220s.html#157">Alla Basilica
                      di ›San Giovanni in Laterano‹</a>
                    </li>
                    <li>
                      <a href="dg4503220s.html#161">Chiesa di ›San
                      Giovanni in Laterano‹ ▣ ›Obelisco
                      Lateranense‹ ▣</a>
                    </li>
                    <li>
                      <a href="dg4503220s.html#166">Scala Sancta
                      Sancti Ioannis ›Scala Santa‹ ▣</a>
                    </li>
                    <li>
                      <a href="dg4503220s.html#167">›Santa Croce
                      in Gerusalemme‹</a>
                      <ul>
                        <li>
                          <a href="dg4503220s.html#167">Chiesa di
                          ›Santa Croce in Gerusalemme‹ ▣</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="dg4503220s.html#168">›San Lorenzo
                      fuori le Mura‹</a>
                      <ul>
                        <li>
                          <a href="dg4503220s.html#169">Chiesa di
                          ›San Lorenzo fuori le Mura‹ ▣</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="dg4503220s.html#170">›Santa Maria
                      Maggiore‹</a>
                      <ul>
                        <li>
                          <a href="dg4503220s.html#171">Chiesa di
                          ›Santa Maria Maggiore‹ ▣</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#173">Notitia delle Porte,
          Monti, e Rioni della Città di Roma</a>
          <ul>
            <li>
              <a href="dg4503220s.html#173">Porte della Città</a>
            </li>
            <li>
              <a href="dg4503220s.html#173">In ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4503220s.html#174">In ›Borgo‹</a>
            </li>
            <li>
              <a href="dg4503220s.html#174">Monti dentro la
              Città</a>
            </li>
            <li>
              <a href="dg4503220s.html#175">Rioni</a>
            </li>
            <li>
              <a href="dg4503220s.html#175">Piazze, nelle quali si
              vendono vettovaglie, dette anticamente Macelli</a>
            </li>
            <li>
              <a href="dg4503220s.html#175">Piazze, e Contrade,
              dove risiedono diverse arti, e si fanno Fiere, e
              Mercati</a>
            </li>
            <li>
              <a href="dg4503220s.html#177">Strade principali
              della Città</a>
            </li>
            <li>
              <a href="dg4503220s.html#179">Luoghi, dove al
              presente stanno le Porte dentro la Città di Roma</a>
            </li>
            <li>
              <a href="dg4503220s.html#179">Delle Ville più
              celebri, che sono ne' contorni di Roma</a>
              <ul>
                <li>
                  <a href="dg4503220s.html#180">Ville di
                  Frascati</a>
                </li>
                <li>
                  <a href="dg4503220s.html#180">Villa
                  Aldobrandina</a>
                </li>
                <li>
                  <a href="dg4503220s.html#180">Villa
                  Ludovisia</a>
                </li>
                <li>
                  <a href="dg4503220s.html#181">Villa Borghese</a>
                </li>
                <li>
                  <a href="dg4503220s.html#181">Villa Borghese di
                  Monte Dragone</a>
                </li>
                <li>
                  <a href="dg4503220s.html#182">Giardino d'Este in
                  Tivoli</a>
                </li>
                <li>
                  <a href="dg4503220s.html#182">Giardino di
                  Bagnaia</a>
                </li>
                <li>
                  <a href="dg4503220s.html#183">Palazzo, e
                  Giardino di Caprarola</a>
                </li>
                <li>
                  <a href="dg4503220s.html#183">Giardino, e
                  Palazzo de' Signori Ginnetti in Velletri</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503220s.html#184">Cronologia de' Sommi
          Pontefici Romani</a>
        </li>
        <li>
          <a href="dg4503220s.html#207">Indice delle Chiese</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
