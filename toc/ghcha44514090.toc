<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghcha44514090s.html#6">Idea della perfezione della
      pittura</a>
      <ul>
        <li>
          <a href="ghcha44514090s.html#8">Al Signore Giovanni
          Degli Alessandri</a>
        </li>
        <li>
          <a href="ghcha44514090s.html#10">Prefazione del Can.
          Domenico Moreni</a>
        </li>
        <li>
          <a href="ghcha44514090s.html#24">Indice degli Articoli
          di questo Trattato</a>
        </li>
        <li>
          <a href="ghcha44514090s.html#26">A Monsignore,
          Monsignor lo Duca d'Orleans</a>
        </li>
        <li>
          <a href="ghcha44514090s.html#28">Prefazione</a>
        </li>
        <li>
          <a href="ghcha44514090s.html#34">Avvertimento al
          lettore</a>
        </li>
        <li>
          <a href="ghcha44514090s.html#38">Idea della perfezione
          della pittura</a>
        </li>
        <li>
          <a href="ghcha44514090s.html#134">Riflessioni sopra
          Michelangelo Buonarroti del Cav. Onofrio Boni</a>
        </li>
        <li>
          <a href="ghcha44514090s.html#192">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
