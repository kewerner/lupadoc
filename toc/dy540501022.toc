<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy540501022s.html#8">Die Sixtinische Kapelle
      ›Cappella Sistina‹. 2, Tafeln [Michelangelo. Siebenzig Tafeln
      nach Acquarellen und Stichen, sowie nach Original-Aufnahmen
      von Domenico Anderson]</a>
    </li>
    <li>
      <a href="dy540501022s.html#12">Inhalt</a>
    </li>
    <li>
      <a href="dy540501022s.html#14">[Tafeln]</a>
      <ul>
        <li>
          <a href="dy540501022s.html#14">[I.] Porträt
          Michelangelos. Nach einem Stich von Giulio Bonasone im
          Besitz des Herrn Geheimrat Ruland in Weimar ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#17">II. Übersichtsplan der
          Decke ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#20">III. Atlanten über Joel
          und Delphica ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#22">IV. Atlanten über
          Erythraea und Jesaias ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#24">V. Atlanten über
          Ezechiel und Cumaea ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#26">VI. Atlanten über
          Persica und Daniel ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#28">VII. Atlanten über
          Jeremias und Libica ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#30">VIII. David und Goliath
          ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#30">VIII. Enthauptung des
          Holofernes ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#32">IX. Kreuzigung Hamans
          ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#32">IX. Erhöhung der
          ehernen Schlange ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#34">X. Die Trunkenheit
          Noahs ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#36">XI. Die Sündflut ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#38">XII. Das Opfer Noahs
          ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#40">XIII. Der Sündenfall
          ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#42">XIV. Köpfe von Adam und
          Eva im Sündenfall ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#44">XV. Die Erschaffung
          Evas ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#46">XVI. Die Erschaffung
          Adams ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#48">XVII. Erschaffung
          Adams. Farbenlichtdruck der Verlagsanstalt Bruckmann,
          München. 1905 ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#50">XVIII. Kopf des Adam
          ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#52">XIX. Gottvater schwebt
          segnend über den Wassern (der fünfte Schöpfungstag) ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#54">XX. Erschaffung der
          Pflanzen und Gestirne ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#56">XXI. Erster
          Schöpfungsakt ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#58">XXII. Der Prophet
          Zacharias ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#60">XXIII. Der Prophet Joel
          ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#62">XXIV. Der Prophet
          Jesaias ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#64">XXV. Der Prophet
          Jesaias. Farbenlichtdruck der Verlagsanstalt Bruckmann,
          München. 1905 ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#66">XXVI. Der Prophet
          Ezechiel ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#68">XXVII. Kopf des
          Ezechiel ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#70">XXVIII. Der Prophet
          Daniel ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#72">XXIX. Der Prophet
          Jeremias ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#74">XXX. Prophet Jeremias.
          Farbenlichtdruck der Verlagsanstalt Bruckmann, München.
          1905 ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#76">XXXI. Der Prophet Jonas
          ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#78">XXXII. Die Delphische
          Sibylle ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#80">XXXIII. Delphische
          Sibylle. Farbenlichtdruck der Verlagsanstalt Bruckmann,
          München. 1905 ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#82">XXXIV. Die
          Erythraeische Sibylle ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#84">XXXV. Kopf der
          Erythraea ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#86">XXXVI. Die Cumaeische
          Sibylle ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#88">XXXVII. Die Persische
          Sibylle ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#90">XXXIX [sic!]. Libische
          Sibylle. Farbenlichtdruck der Verlagsanstalt Bruckmann,
          München. 1905 ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#92">XXXVIII [sic!]. Die
          Libische Sibylle ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#94">XL. Josias- und
          Zorohabel-Gruppe ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#96">XLI. Ezechias- und
          Osia-Gruppe ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#98">XLII. Asa- und
          Roboam-Gruppe ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#100">XLIII. Jesse- und
          Salomon-Gruppe ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#102">XLIV.
          Jakob-Joseph-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#104">XLV.
          Eleazar-Mathan-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#106">XLVI.
          Achim-Eliud-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#108">XLVII.
          Azor-Sadoc-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#110">XLVIII.
          Zorobabel-Abiud-Eliachim-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#112">XLIX.
          Josias-Jechonias-Sealthiel-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#114">L.
          Ezechias-Manasse-Amon-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#116">LI.
          Osias-Joatham-Achas-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#118">LII.
          Asa-Josaphat-Joram-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#120">LIII.
          Roboam-Abias-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#122">LIV.
          Jesse-David-Salomo-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#124">LV.
          Salmon-Booz-Obeth-Lünette ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#126">LVI. Naasson-Lünette
          ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#128">LVII. Aminadab-Lünette
          ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#130">LVIII. Vorfahren
          Christi an der Altarwand nach dem Stich von W.Y. Ottley
          im British Museum ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#132">LIX. Zwei Köpfe von
          Atlanten ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#134">LX. Zwei Köpfe
          Gottvaters ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#136">LXI. Zwei
          Prophetenköpfe ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#140">LXII. Köpfe der Engel
          neben Joel und Jesaias ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#142">LXIII. Köpfe der Engel
          neben Cumaea und Ezechiel ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#145">LXIV/LXV. Das Jüngste
          Gericht. Originalaufnahme nach dem Gemälde in der
          Sixtinischen Kapelle ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#148">LXVI. Kopie des
          Jüngsten Gerichts von Marcello Venusti in Neapel ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#150">LXVIII [sic!]. Das
          Jüngste Gericht nach einem Stich des Giulio Bonasone im
          Besitz des Herrn Geheimrat Ruland in Weimar ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#152">LXVII [sic!]. Köpfe
          von Adam und Beatrice. Details aus dem Jüngsten Gericht
          ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#154">LXIX. Die
          Auferstehenden nach einem Stich des Nicolò Della Casa im
          Besitz des Herrn Geheimrat Ruland in Weimar ▣</a>
        </li>
        <li>
          <a href="dy540501022s.html#156">LXX. Die Verdammten
          nach einem Stich des Nicolò Della Casa im Besitz des
          Herrn Geheimrat Ruland in Weimar ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
