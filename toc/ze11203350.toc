<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ze11203350s.html#6">Degli antichi teatri, e
      anfiteatri</a>
      <ul>
        <li>
          <a href="ze11203350s.html#8">A' lettori</a>
        </li>
        <li>
          <a href="ze11203350s.html#10">All'illustrissimo Signor
          Marchese Luigi Sale nobile Vicentino</a>
        </li>
        <li>
          <a href="ze11203350s.html#28">Lettera del Signor Conte
          Giovanni Montenari</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
