<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="be275039101s.html#6">L’Etruria Pittrice Ovvero
      Storia Della Pittura Toscana Dedotta Dai Suoi Monumenti Che
      Si Esibiscono In Stampa Dal Secolo X. Fino Al Presente; Tomo
      I.</a>
      <ul>
        <li>
          <a href="be275039101s.html#10">Nota de' Signori
          associati all'Etruria Pittrice</a>
        </li>
        <li>
          <a href="be275039101s.html#12">Al Signor Cavaliere
          Giovanni Macpherson</a>
        </li>
        <li>
          <a href="be275039101s.html#14">Avviso a chi legge</a>
        </li>
        <li>
          <a href="be275039101s.html#18">Catalogo de' Pittori</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
