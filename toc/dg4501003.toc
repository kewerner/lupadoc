<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501003s.html#7">[Titelblatt] [Historia et
      descriptio/ ital. Ausgabe]</a>
    </li>
    <li>
      <a href="dg4501003s.html#8">[I] [Verzeichnis römischer
      Herrscher]</a>
      <ul>
        <li>
          <a href="dg4501003s.html#8">[1] [Romulo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#13">[2] [Numa]</a>
        </li>
        <li>
          <a href="dg4501003s.html#13">[3] [Tullo Hostilio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#13">[4] [Anco Martio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#14">[5] [Tarquibo prisco]</a>
        </li>
        <li>
          <a href="dg4501003s.html#14">[6] [Servio Tullio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#14">[7] [Tarquinio superco]</a>
        </li>
        <li>
          <a href="dg4501003s.html#14">[8] [Consuli da Iunio Bruto
          et P. Valerio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#15">[9] [Iulio Cesare]</a>
        </li>
        <li>
          <a href="dg4501003s.html#16">[10] [Octaviano
          Augusto]</a>
          <ul>
            <li>
              <a href="dg4501003s.html#17">[1] [Lanno xl del
              prfaro Augusto]</a>
            </li>
            <li>
              <a href="dg4501003s.html#17">[2]</a>
            </li>
            <li>
              <a href="dg4501003s.html#18">[3] [anno xliiii de
              limperio de Augusto]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501003s.html#18">[11] [Tyberio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#19">[12] [Calligula]</a>
        </li>
        <li>
          <a href="dg4501003s.html#20">[13] [Claudio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#21">[14] [Nerone]</a>
        </li>
        <li>
          <a href="dg4501003s.html#22">[15] [Galba Sergio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#22">[16] [Vespasiano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#22">[17] [Tito]</a>
        </li>
        <li>
          <a href="dg4501003s.html#23">[18] [Domitiano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#23">[19] [Nerva]</a>
        </li>
        <li>
          <a href="dg4501003s.html#23">[20] [Traiano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#24">[21] [Hadriano Elio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#24">[22] [Antonio Pio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#24">[23] [Marco]</a>
        </li>
        <li>
          <a href="dg4501003s.html#24">[24] [Commodo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#25">[25] [Elio pertinace]</a>
        </li>
        <li>
          <a href="dg4501003s.html#25">[26] [Severo pertinace]</a>
        </li>
        <li>
          <a href="dg4501003s.html#25">[27] [Antonio
          caracallo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#25">[28] [Martino]</a>
        </li>
        <li>
          <a href="dg4501003s.html#25">[29] [Aurelio Ant.
          Heligabolo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#26">[30] [Alexandro Mammeo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#26">[31] [Maximino]</a>
        </li>
        <li>
          <a href="dg4501003s.html#26">[32] [Gordiano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#27">[33] [Philippo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#27">[34] [Decio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#27">[35] [Gallo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#28">[36] [Valeriano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#28">[37] [Claudio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#28">[38] [Aureliano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#29">[39] [Tacito]</a>
        </li>
        <li>
          <a href="dg4501003s.html#29">[40] [Probo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#29">[41] [Caro]</a>
        </li>
        <li>
          <a href="dg4501003s.html#30">[42] [Domiciano con
          Maximiano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#30">[43] [Constantino]</a>
        </li>
        <li>
          <a href="dg4501003s.html#31">[44] [Maxentio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#32">[45] [COnstantino
          magno]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501003s.html#42">[II] Indulgentie dele vii
      chiesie principali di Roma</a>
      <ul>
        <li>
          <a href="dg4501003s.html#43">SANCTUS IOANNES
          EWANGELISTA</a>
        </li>
        <li>
          <a href="dg4501003s.html#44">[Einführung]</a>
        </li>
        <li>
          <a href="dg4501003s.html#44">[1] [Santo Giovanni in
          laterano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#50">SANCTUS PETRUS</a>
        </li>
        <li>
          <a href="dg4501003s.html#51">[2] [San Pietro in
          Vaticano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#55">SANCTUS PAULUS</a>
        </li>
        <li>
          <a href="dg4501003s.html#56">[3] [Santo Paulo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#59">[4] [Santa Maria
          magiore]</a>
        </li>
        <li>
          <a href="dg4501003s.html#61">SANCTUS LAURENTIUS</a>
        </li>
        <li>
          <a href="dg4501003s.html#62">[5] [santo Lorenzo fuori
          delle mura]</a>
        </li>
        <li>
          <a href="dg4501003s.html#64">[6] [San Fabiano e santo
          Sebastiano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#67">[7] [Santa Croce in
          Hierusalem]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501003s.html#68">[III] De indulgentie et
      reliquie de laltre chiesie di Roma</a>
      <ul>
        <li>
          <a href="dg4501003s.html#68">[1] [SAnta Maria in
          transtevero]</a>
        </li>
        <li>
          <a href="dg4501003s.html#70">[2] [SAnto Grisogono]</a>
        </li>
        <li>
          <a href="dg4501003s.html#70">[3] [SAnta Cecilia
          virgine]</a>
        </li>
        <li>
          <a href="dg4501003s.html#71">[4] [SAnto Bartholomeo nell
          isola tibertina]</a>
        </li>
        <li>
          <a href="dg4501003s.html#71">[5] [SAnto Giovanni nell
          isola]</a>
        </li>
        <li>
          <a href="dg4501003s.html#72">[6] [SAnto Nicolao in
          carcere Tulliano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#72">[7] [SAnta Maria in
          portico]</a>
        </li>
        <li>
          <a href="dg4501003s.html#74">[8] [SAnto Gregorio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#75">[9] [SAnta Anastasia]</a>
        </li>
        <li>
          <a href="dg4501003s.html#75">[10] [SAnta Maria scola
          greca]</a>
        </li>
        <li>
          <a href="dg4501003s.html#76">[11] [SAnta Sabina]</a>
        </li>
        <li>
          <a href="dg4501003s.html#77">[12] [SAnto Alexio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#79">[13] [SAnta Prisca]</a>
        </li>
        <li>
          <a href="dg4501003s.html#79">[14] [SAnto Sabba]</a>
        </li>
        <li>
          <a href="dg4501003s.html#80">[15] [SAnto Anastasio ale
          tre fontane]</a>
        </li>
        <li>
          <a href="dg4501003s.html#80">[16] [SAnta Maria scala
          celi]</a>
        </li>
        <li>
          <a href="dg4501003s.html#81">[17] [SAnta Maria
          annunciata]</a>
        </li>
        <li>
          <a href="dg4501003s.html#81">[18] [Domine quo vadis]</a>
        </li>
        <li>
          <a href="dg4501003s.html#82">[19] [SAnto Giovanni ante
          porta latina]</a>
        </li>
        <li>
          <a href="dg4501003s.html#82">[20] [SAnto Sixto]</a>
        </li>
        <li>
          <a href="dg4501003s.html#82">[21] [SAnta Balbina]</a>
        </li>
        <li>
          <a href="dg4501003s.html#82">[22] [SAnto Gregorio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#84">[23] [SAnti Giovanni et
          Paulo in monte Celio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#84">[24] [SAnto Stephano in
          Celiomonte]</a>
        </li>
        <li>
          <a href="dg4501003s.html#85">[25] [SAnta maria dela
          Navicula]</a>
        </li>
        <li>
          <a href="dg4501003s.html#85">[26] [SAnta maria
          imperatrice]</a>
        </li>
        <li>
          <a href="dg4501003s.html#86">[27] [SAnti Pietro et
          Marcellino]</a>
        </li>
        <li>
          <a href="dg4501003s.html#86">[28] [SAnti quatro
          coronati]</a>
        </li>
        <li>
          <a href="dg4501003s.html#87">[29] [SAnto Clemente]</a>
        </li>
        <li>
          <a href="dg4501003s.html#89">[30] [SAnta Maria nova]</a>
        </li>
        <li>
          <a href="dg4501003s.html#90">[31] [SAn Cosimo et
          Damiano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#91">[32] [SAnta Maria libera
          nos a penis inferni]</a>
        </li>
        <li>
          <a href="dg4501003s.html#91">[33] [SAnta Maria dele
          consolatione]</a>
        </li>
        <li>
          <a href="dg4501003s.html#92">[34] [SAnta Maria dele
          gratie]</a>
        </li>
        <li>
          <a href="dg4501003s.html#92">[35] [SAnto Pietro in
          carceredritto a Campidoglio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#92">[36] [SAnto Hadriano]</a>
        </li>
        <li>
          <a href="dg4501003s.html#93">[37] [SAnto Marco nella
          regione della pigna]</a>
        </li>
        <li>
          <a href="dg4501003s.html#94">[38] [SAnti apostoli]</a>
        </li>
        <li>
          <a href="dg4501003s.html#95">[39] [SAnta Maria in via
          lata]</a>
        </li>
        <li>
          <a href="dg4501003s.html#95">[40] [SAnto Marcello]</a>
        </li>
        <li>
          <a href="dg4501003s.html#95">[41] [SAnto Silvestro]</a>
        </li>
        <li>
          <a href="dg4501003s.html#96">[42] [SAnto Lorenzo in
          Lucina]</a>
        </li>
        <li>
          <a href="dg4501003s.html#97">[43] [SAnto Tripho.]</a>
        </li>
        <li>
          <a href="dg4501003s.html#97">[44] [SAnto Augustino]</a>
        </li>
        <li>
          <a href="dg4501003s.html#98">[45] [SAnto Apollinare]</a>
        </li>
        <li>
          <a href="dg4501003s.html#98">[46] [SAnta MAria di
          populo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#100">[47] [SAnta Vibiana]</a>
        </li>
        <li>
          <a href="dg4501003s.html#101">[48] [SAnto Eusebio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#101">[49] [SAnto Matheo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#102">[50] [SAnto Iuliano di
          rimpeto a santo Eusebio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#102">[51] [SAnto Vito in
          macelli]</a>
        </li>
        <li>
          <a href="dg4501003s.html#103">[52] [Santa Praxeda]</a>
        </li>
        <li>
          <a href="dg4501003s.html#104">[53] [SAnto Martino nelli
          monti]</a>
        </li>
        <li>
          <a href="dg4501003s.html#104">[54] [SAnto Cyriaco]</a>
        </li>
        <li>
          <a href="dg4501003s.html#105">[55] [SAnta Susanna]</a>
        </li>
        <li>
          <a href="dg4501003s.html#105">[56] [SAnta
          Potentiana]</a>
        </li>
        <li>
          <a href="dg4501003s.html#106">[57] [SAnta Indola]</a>
        </li>
        <li>
          <a href="dg4501003s.html#107">[58] [SAnto Vitale]</a>
        </li>
        <li>
          <a href="dg4501003s.html#107">[59] [SAnto Laurenzo in
          palisperna]</a>
        </li>
        <li>
          <a href="dg4501003s.html#107">[60] [SAnta Eufemia]</a>
        </li>
        <li>
          <a href="dg4501003s.html#108">[61] [Santo Laurenzo in
          carcere]</a>
        </li>
        <li>
          <a href="dg4501003s.html#108">[62] [Santo Pietro ad
          vincula]</a>
        </li>
        <li>
          <a href="dg4501003s.html#109">[63] [SAnta Maria
          araceli]</a>
        </li>
        <li>
          <a href="dg4501003s.html#110">[64] [SAnto angelo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#110">[65] [SAnta Maria della
          Minerva]</a>
        </li>
        <li>
          <a href="dg4501003s.html#111">[66] [SAnta Maria
          rotunda]</a>
        </li>
        <li>
          <a href="dg4501003s.html#111">[67] [Santa Maria
          magdalena]</a>
        </li>
        <li>
          <a href="dg4501003s.html#111">[68] [SAnto Eustachio]</a>
        </li>
        <li>
          <a href="dg4501003s.html#112">[69] [Santo Salvatore]</a>
        </li>
        <li>
          <a href="dg4501003s.html#112">[70] [Santa Maria di
          monticelli]</a>
        </li>
        <li>
          <a href="dg4501003s.html#112">[71] [Santo
          Martinello]</a>
        </li>
        <li>
          <a href="dg4501003s.html#112">[72] [Santo Andrea]</a>
        </li>
        <li>
          <a href="dg4501003s.html#113">[73] [SAnto Biagio dala
          anello]</a>
        </li>
        <li>
          <a href="dg4501003s.html#113">[74] [SAnta Barbara]</a>
        </li>
        <li>
          <a href="dg4501003s.html#113">[75] [SAnto Iacobo deli
          Spagnoli]</a>
        </li>
        <li>
          <a href="dg4501003s.html#114">[76] [SAnta MAria de
          lanima]</a>
        </li>
        <li>
          <a href="dg4501003s.html#114">[77] [Santa Maria dila
          pace]</a>
        </li>
        <li>
          <a href="dg4501003s.html#114">[78] [Santo Lorenzo in
          Damaso]</a>
        </li>
        <li>
          <a href="dg4501003s.html#115">[79] [Santo Biagio dela
          Paneta]</a>
        </li>
        <li>
          <a href="dg4501003s.html#115">[80] [Santo Celso in
          banchi]</a>
        </li>
        <li>
          <a href="dg4501003s.html#115">[81] [Santa Maria
          transpontina]</a>
        </li>
        <li>
          <a href="dg4501003s.html#116">[82] [Santo Iacobo nel
          mezo dila via da san Pietro e castello santo Angelo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#116">[83] [Santa Katherina]</a>
        </li>
        <li>
          <a href="dg4501003s.html#116">[84] [Santo spirito]</a>
        </li>
        <li>
          <a href="dg4501003s.html#117">[85] [CAmpo santo]</a>
        </li>
        <li>
          <a href="dg4501003s.html#117">[86] [Santo Pancratio in
          transtevero]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501003s.html#119">[IV] [Stationes]</a>
      <ul>
        <li>
          <a href="dg4501003s.html#119">[1] Statione della
          quadragesima</a>
        </li>
        <li>
          <a href="dg4501003s.html#121">[2] Statione dopo
          pasca</a>
        </li>
        <li>
          <a href="dg4501003s.html#122">[3] Statione nello
          Advento</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
