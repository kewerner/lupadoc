<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caamm101820as.html#14">Lettera di Messer
      Bartolommeo Ammannati scultore, e architetto fiorentino
      scritta agli accademici del disegno l’anno MDLXXXII, colla
      quale volle mostrare quanto pericolosa cosa sia all’anime
      degli artefici di pittura, e scultura, l’esercitare l’arti
      loro in rappresentar figure meno che oneste, ed il danno, che
      quindi può derivare all’altr’anime de’ fedeli</a>
      <ul>
        <li>
          <a href="caamm101820as.html#16">Gloriosissima Regina
          del cielo, e della terra</a>
        </li>
        <li>
          <a href="caamm101820as.html#22">Lettera di Messer
          Bartolommeo Ammannati</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
