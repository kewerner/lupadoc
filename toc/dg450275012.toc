<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg450275012s.html#6">Santuario overo Menologio
      romano perpetuo per la visita delle Chiese, feste,
      indulgenze, stazioni, reliquie de Santi, e cose sacre
      memorabili di Roma</a>
      <ul>
        <li>
          <a href="dg450275012s.html#8">[Dedica dell'autore al
          Cardinale Gaspare Carpegna]</a>
        </li>
        <li>
          <a href="dg450275012s.html#14">All'erudito, e divoto
          Lettore l'Autore</a>
        </li>
        <li>
          <a href="dg450275012s.html#22">Autori da' quali si
          sono cavate le cose esposte nel presente Santuario Romano
          [Index]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg450275012s.html#26">Parte prima</a>
      <ul>
        <li>
          <a href="dg450275012s.html#26">Gennaro</a>
          <ul>
            <li>
              <a href="dg450275012s.html#72">Domeniche di questo
              mese</a>
            </li>
            <li>
              <a href="dg450275012s.html#75">Avvertimento delle
              reliquie</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#78">Febraro</a>
          <ul>
            <li>
              <a href="dg450275012s.html#108">Osservazione per
              tutto l'anno</a>
            </li>
            <li>
              <a href="dg450275012s.html#109">Domeniche di
              questo mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#110">Marzo</a>
          <ul>
            <li>
              <a href="dg450275012s.html#139">Domeniche di
              questo mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#140">Aprile</a>
          <ul>
            <li>
              <a href="dg450275012s.html#168">Domeniche di
              questo mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#170">Maggio</a>
          <ul>
            <li>
              <a href="dg450275012s.html#204">Domeniche di
              questo mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#208">Giugno</a>
          <ul>
            <li>
              <a href="dg450275012s.html#237">Domeniche di
              questo mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#238">Luglio</a>
          <ul>
            <li>
              <a href="dg450275012s.html#274">Domeniche di
              questo mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#275">Agosto</a>
          <ul>
            <li>
              <a href="dg450275012s.html#319">Domeniche di
              questo mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#320">Settembre</a>
          <ul>
            <li>
              <a href="dg450275012s.html#356">Domeniche di
              questo mese</a>
            </li>
            <li>
              <a href="dg450275012s.html#358">Tempora di questo
              mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#360">Ottobre</a>
          <ul>
            <li>
              <a href="dg450275012s.html#390">Domeniche di
              questo mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#392">Novembre</a>
          <ul>
            <li>
              <a href="dg450275012s.html#441">Domeniche di
              questo mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#444">Decembre</a>
          <ul>
            <li>
              <a href="dg450275012s.html#504">Annotazione</a>
            </li>
            <li>
              <a href="dg450275012s.html#510">Domeniche di
              questo mese</a>
            </li>
            <li>
              <a href="dg450275012s.html#511">Tempora occorrenti
              in questo mese</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#512">Feste, e fonzioni
          mobili et esercizij di pietà, che si fanno tutti li
          giorni della settimana per tutto l'anno</a>
          <ul>
            <li>
              <a href="dg450275012s.html#512">Domenica</a>
            </li>
            <li>
              <a href="dg450275012s.html#514">Lunedì</a>
            </li>
            <li>
              <a href="dg450275012s.html#515">Martedì</a>
            </li>
            <li>
              <a href="dg450275012s.html#520">Giovedì</a>
            </li>
            <li>
              <a href="dg450275012s.html#521">Venerdì</a>
            </li>
            <li>
              <a href="dg450275012s.html#523">Sabbato [sic!]</a>
            </li>
            <li>
              <a href="dg450275012s.html#525">Prime domeniche
              del mese</a>
            </li>
            <li>
              <a href="dg450275012s.html#526">Seconde domeniche
              del mese</a>
            </li>
            <li>
              <a href="dg450275012s.html#527">Terze domeniche
              del mese</a>
            </li>
            <li>
              <a href="dg450275012s.html#527">Quarte domeniche
              del mese</a>
            </li>
            <li>
              <a href="dg450275012s.html#528">Ultime domeniche
              del mese</a>
            </li>
            <li>
              <a href="dg450275012s.html#529">Annotazione</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg450275012s.html#530">Parte seconda</a>
      <ul>
        <li>
          <a href="dg450275012s.html#530">Delle Stazioni, e
          Chiese Stazionali di Roma</a>
        </li>
        <li>
          <a href="dg450275012s.html#535">Stazione della
          Quaresima</a>
          <ul>
            <li>
              <a href="dg450275012s.html#535">I. ›Santa
              Sabina‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#543">II. ›San Giorgio
              in Velabro‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#545">III. ›Santi
              Giovanni e Paolo‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#550">IV. Santi
              Agostino, e Trifone ›Sant'Agostino‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#552">V. ›San Giovanni
              in Laterano‹ [1]</a>
            </li>
            <li>
              <a href="dg450275012s.html#557">VI. ›San Pietro in
              Vincoli‹ [1]</a>
            </li>
            <li>
              <a href="dg450275012s.html#560">VII.
              ›Sant'Anastasia‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#562">VIII. ›Santa Maria
              Maggiore‹ [1]</a>
            </li>
            <li>
              <a href="dg450275012s.html#565">IX. ›San Lorenzo
              in Panisperna‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#567">X. ›Santi
              Apostoli‹ [1]</a>
            </li>
            <li>
              <a href="dg450275012s.html#569">XI. ›San Pietro in
              Vaticano‹ [1]</a>
            </li>
            <li>
              <a href="dg450275012s.html#573">XII. Santa Maria
              Della Navicella ›Santa Maria in Domnica‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#576">XIII. ›San
              Clemente‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#581">XIV. ›Santa
              Balbina‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#583">XV. Santa Cecilia
              ›Santa Cecilia in Trastevere‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#588">XVI. ›Santa Maria
              in Trastevere‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#591">XVII. ›San
              Vitale‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#593">VIII [sic]. Santi
              Pietro, e Marcellino ›Santi Marcellino e Pietro‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#595">XIX. San Lorenzo
              ›San Lorenzo fuori le Mura‹ [1]</a>
            </li>
            <li>
              <a href="dg450275012s.html#598">XX. ›San
              Marco‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#600">XXI. ›Santa
              Pudenziana‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#606">XXII. ›San Sisto
              Vecchio‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#609">XXIII. ›Santi
              Nereo e Achilleo‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#612">XXIV. ›Santi Cosma
              e Damiano‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#614">XXV. ›San Lorenzo
              in Lucina‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#618">XXVI. ›Santa
              Susanna‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#629">XXVII. ›Santa
              Croce in Gerusalemme‹ [1]</a>
            </li>
            <li>
              <a href="dg450275012s.html#635">XXVIII. ›Santi
              Quattro Coronati‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#637">XIX [sic]. ›San
              Lorenzo in Damaso‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#643">XXX. ›San Paolo
              fuori le Mura‹ [1]</a>
            </li>
            <li>
              <a href="dg450275012s.html#650">XXXI. ›San Martino
              ai Monti‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#659">XXXII.
              ›Sant'Eusebio‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#664">XXXIII. ›San
              Nicola in Carcere‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#667">XXXIV. ›San Pietro
              in Vaticano‹ [2]</a>
            </li>
            <li>
              <a href="dg450275012s.html#676">XXXV. ›San
              Crisogono‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#678">XXXVI. ›Santi
              Quirico e Giulitta‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#681">XXXVII. ›San
              Marcello al Corso‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#684">XXXVIII.
              ›Sant'Apollinare‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#686">XXXVIII. ›Santo
              Stefano Rotondo‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#688">XL. ›San Giovanni
              a Porta Latina‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#691">XLI. ›San Giovanni
              in Laterano‹ [2]</a>
            </li>
            <li>
              <a href="dg450275012s.html#695">XLII. ›Santa
              Prassede‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#699">XLIII. ›Santa
              Prisca‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#704">XLIV. ›Santa Maria
              Maggiore‹ [2]</a>
            </li>
            <li>
              <a href="dg450275012s.html#707">XLV. ›San Giovanni
              in Laterano‹ [3]</a>
            </li>
            <li>
              <a href="dg450275012s.html#711">XLVI. ›Santa Croce
              in Gerusalemme‹ [2]</a>
            </li>
            <li>
              <a href="dg450275012s.html#717">XLVII. ›San
              Giovanni in Laterano‹ [4]</a>
            </li>
            <li>
              <a href="dg450275012s.html#721">XLVIII. ›Santa
              Maria Maggiore‹ [3]</a>
            </li>
            <li>
              <a href="dg450275012s.html#724">XLIX. ›San Pietro
              in Vaticano‹ [3]</a>
            </li>
            <li>
              <a href="dg450275012s.html#731">L. ›San Paolo
              fuori le Mura‹ [2]</a>
            </li>
            <li>
              <a href="dg450275012s.html#733">LI. ›San Lorenzo
              fuori le Mura‹ [2]</a>
            </li>
            <li>
              <a href="dg450275012s.html#735">LII. ›Santi
              Apostoli‹ [2]</a>
            </li>
            <li>
              <a href="dg450275012s.html#737">LIII. Santa Maria
              della Ritonda ›Pantheon‹</a>
            </li>
            <li>
              <a href="dg450275012s.html#741">LIV. ›San Giovanni
              in Laterano‹ [5]</a>
            </li>
            <li>
              <a href="dg450275012s.html#742">LV. ›San
              Pancrazio‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#745">Stazioni dopo l'ottava
          di Pasqua</a>
          <ul>
            <li>
              <a href="dg450275012s.html#745">LVI. ›Santa Maria
              Maggiore‹ [4]</a>
            </li>
            <li>
              <a href="dg450275012s.html#747">LVII. ›San
              Giovanni in Laterano‹ [6]</a>
            </li>
            <li>
              <a href="dg450275012s.html#748">LVIII. ›San Pietro
              in Vaticano‹ [4]</a>
            </li>
            <li>
              <a href="dg450275012s.html#748">LIX. ›San Pietro
              in Vaticano‹ [5]</a>
            </li>
            <li>
              <a href="dg450275012s.html#749">LX. ›San Giovanni
              in Laterano‹ [7]</a>
            </li>
            <li>
              <a href="dg450275012s.html#751">LXVI [sic]. ›San
              Pietro in Vaticano‹ [6]</a>
            </li>
            <li>
              <a href="dg450275012s.html#753">LXII. ›San Pietro
              in Vincoli‹ [2]</a>
            </li>
            <li>
              <a href="dg450275012s.html#754">LXIII.
              ›Sant'Anastasia‹ [2]</a>
            </li>
            <li>
              <a href="dg450275012s.html#754">LXIV. ›Santa Maria
              Maggiore‹ [5]</a>
            </li>
            <li>
              <a href="dg450275012s.html#755">LXV. ›San Lorenzo
              fuori le Mura‹ [3]</a>
            </li>
            <li>
              <a href="dg450275012s.html#756">LXVI. ›Santi
              Apostoli‹ [3]</a>
            </li>
            <li>
              <a href="dg450275012s.html#758">LXVII. ›San Pietro
              in Vaticano‹ [7]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450275012s.html#758">Domenica della
          Santissima Trinità</a>
        </li>
        <li>
          <a href="dg450275012s.html#760">Festa del Corpo del
          Signore [Corpus Domini]</a>
        </li>
        <li>
          <a href="dg450275012s.html#762">Stazioni dell'Avvento,
          sino à [sic] Quaresima</a>
          <ul>
            <li>
              <a href="dg450275012s.html#762">LXVIII. ›Santa
              Maria Maggiore‹ [6]</a>
            </li>
            <li>
              <a href="dg450275012s.html#764">LXIX. ›Santa Croce
              in Gerusalemme‹ [3]</a>
            </li>
            <li>
              <a href="dg450275012s.html#764">LXX. ›San Pietro
              in Vaticano‹ [8]</a>
            </li>
            <li>
              <a href="dg450275012s.html#765">LXXI. [›Santi
              Apostoli‹ [4]]</a>
            </li>
            <li>
              <a href="dg450275012s.html#766">LXXII. [›San
              Lorenzo fuori le Mura‹ [3]]</a>
            </li>
            <li>
              <a href="dg450275012s.html#767">LXXIII. [›San
              Paolo fuori le Mura‹ [3]]</a>
            </li>
            <li>
              <a href="dg450275012s.html#768">LXXIV. [›San
              Pietro in Vaticano‹ [9]]</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
