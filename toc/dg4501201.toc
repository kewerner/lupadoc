<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501201s.html#6">Mirabilia Rome</a>
      <ul>
        <li>
          <a href="dg4501201s.html#7">Epistola Cornelius Cymbalus
          Francisco Albertino Floren Pontificii Doctori</a>
        </li>
        <li>
          <a href="dg4501201s.html#8">Tabula Cornelii
          Distichon</a>
        </li>
        <li>
          <a href="dg4501201s.html#10">Epistola humilis devotus
          Franciscus Albertinus florentinus</a>
        </li>
        <li>
          <a href="dg4501201s.html#13">Proemium</a>
        </li>
        <li>
          <a href="dg4501201s.html#14">[Mirabilia Rome]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
