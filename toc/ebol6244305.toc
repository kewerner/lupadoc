<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebol6244305s.html#6">Vite dei pittori ed artefici
      bolognesi</a>
      <ul>
        <li>
          <a href="ebol6244305s.html#8">Parte quinta</a>
          <ul>
            <li>
              <a href="ebol6244305s.html#11">Vita di Guido
              Reni</a>
            </li>
            <li>
              <a href="ebol6244305s.html#39">Donducci Giovanni
              Andrea detto il Mastelletta</a>
            </li>
            <li>
              <a href="ebol6244305s.html#47">Agostino Tassi</a>
            </li>
            <li>
              <a href="ebol6244305s.html#48">Leonello Spada</a>
            </li>
            <li>
              <a href="ebol6244305s.html#79">Pietro Desani</a>
            </li>
            <li>
              <a href="ebol6244305s.html#81">Alessandro
              Tiarini</a>
            </li>
            <li>
              <a href="ebol6244305s.html#102">Francesco
              Carboni</a>
            </li>
            <li>
              <a href="ebol6244305s.html#102">Giacomo
              Cavedoni</a>
            </li>
            <li>
              <a href="ebol6244305s.html#111">Girolamo Curti
              detto il Dentone</a>
            </li>
            <li>
              <a href="ebol6244305s.html#130">Lorenzo
              Garbieri</a>
            </li>
            <li>
              <a href="ebol6244305s.html#137">Giovanni Luigi
              Valesio</a>
            </li>
            <li>
              <a href="ebol6244305s.html#140">Francesco
              Albani</a>
            </li>
            <li>
              <a href="ebol6244305s.html#179">Domenico Zampieri
              detto Domenichino</a>
            </li>
            <li>
              <a href="ebol6244305s.html#218">Francesco
              Gessi</a>
            </li>
            <li>
              <a href="ebol6244305s.html#224">Giovanni Giacomo
              Sementi</a>
            </li>
            <li>
              <a href="ebol6244305s.html#226">Giovanni Battista
              Ruggieri</a>
            </li>
            <li>
              <a href="ebol6244305s.html#229">Ercole
              Ruggieri</a>
            </li>
            <li>
              <a href="ebol6244305s.html#230">Vincenzo
              Spisani</a>
            </li>
            <li>
              <a href="ebol6244305s.html#233">Gabriele
              Ferrantini</a>
            </li>
            <li>
              <a href="ebol6244305s.html#237">Vita di Giovanni
              Francesco Barbieri detti il Guercino</a>
            </li>
            <li>
              <a href="ebol6244305s.html#288">Simone
              Cantarini</a>
            </li>
            <li>
              <a href="ebol6244305s.html#299">Girolamo Rossi</a>
            </li>
            <li>
              <a href="ebol6244305s.html#299">Lorenzo
              Pasinelli</a>
            </li>
            <li>
              <a href="ebol6244305s.html#300">Flaminio Torri</a>
            </li>
            <li>
              <a href="ebol6244305s.html#301">Giulio Cesare
              Milani</a>
            </li>
            <li>
              <a href="ebol6244305s.html#302">Giovanni
              Paderna</a>
            </li>
            <li>
              <a href="ebol6244305s.html#304">Andrea
              Seghizzi</a>
            </li>
            <li>
              <a href="ebol6244305s.html#306">Agostino
              Mitelli</a>
            </li>
            <li>
              <a href="ebol6244305s.html#336">Angelo Michele
              Colonna</a>
            </li>
            <li>
              <a href="ebol6244305s.html#360">Baldassarre
              Bianchi&#160;</a>
            </li>
            <li>
              <a href="ebol6244305s.html#363">Domenico Santi</a>
            </li>
            <li>
              <a href="ebol6244305s.html#364">Gioachino
              Pizzoli</a>
            </li>
            <li>
              <a href="ebol6244305s.html#365">Enrico ed Antonio
              Haffner</a>
            </li>
            <li>
              <a href="ebol6244305s.html#368">Giovanni Andrea
              Sirani</a>
            </li>
            <li>
              <a href="ebol6244305s.html#376">Elisabetta
              Sirani</a>
            </li>
            <li>
              <a href="ebol6244305s.html#405">Anna e Barbara
              Sirani</a>
            </li>
            <li>
              <a href="ebol6244305s.html#406">Ginevra
              Cantofoli</a>
            </li>
            <li>
              <a href="ebol6244305s.html#411">Scultori ed
              architetti bolognesi de' secoli XVI. e
              XVII.&#160;</a>
            </li>
            <li>
              <a href="ebol6244305s.html#420">Indice</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
