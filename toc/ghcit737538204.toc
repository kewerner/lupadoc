<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghcit737538204s.html#6">Catalogo istorico de’
      pittori e scultori ferraresi e delle opere loro; tomo IIII.
      [sic]</a>
      <ul>
        <li>
          <a href="ghcit737538204s.html#12">Nomi de' Professori
          di pittura, e scoltura</a>
        </li>
        <li>
          <a href="ghcit737538204s.html#17">[Catalogo istorico
          de' pittori e scultori ferraresi e delle opere
          loro]&#160;</a>
        </li>
        <li>
          <a href="ghcit737538204s.html#328">Brevissime aggiunte
          all'opera</a>
        </li>
        <li>
          <a href="ghcit737538204s.html#366">Registro</a>
        </li>
        <li>
          <a href="ghcit737538204s.html#368">Catalogo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
