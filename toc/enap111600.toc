<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="enap111600s.html#6">Descrittione De I Lvoghi Sacri
      Della Città Di Napoli, Con Li Fondatori Di Essi, Reliqvie,
      Sepoltvre, Et Epitaphii Scelti Che In Qvelle Si Ritrovano</a>
      <ul>
        <li>
          <a href="enap111600s.html#8">Del magnifico Cola Anello
          Pacca</a>
        </li>
        <li>
          <a href="enap111600s.html#9">Giacomo Palombo a i
          lettori</a>
        </li>
        <li>
          <a href="enap111600s.html#10">Del reverendo Donno
          Francesco di Gaudio</a>
        </li>
        <li>
          <a href="enap111600s.html#12">Cola Anello Pacca di
          Napoli, a i lettori</a>
        </li>
        <li>
          <a href="enap111600s.html#16">Proemio</a>
        </li>
        <li>
          <a href="enap111600s.html#19">I. Delle chiese de
          Preti&#160;</a>
        </li>
        <li>
          <a href="enap111600s.html#162">II. Delle chiese de
          Claustrali</a>
        </li>
        <li>
          <a href="enap111600s.html#346">III. De li Monasteri de
          Donne</a>
        </li>
        <li>
          <a href="enap111600s.html#379">IV. De le pietre sparse
          per la Città&#160;</a>
        </li>
        <li>
          <a href="enap111600s.html#390">Del Magnifico Sebastiano
          de Aiello Philosopho e Medico</a>
        </li>
        <li>
          <a href="enap111600s.html#391">Tabula de i luohi sacri
          de la Città di Napoli</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
