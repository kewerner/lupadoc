<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dr8803500s.html#6">De obelisco Caesaris Augusti e
      Campi Martii ruderibus nuper eruto commentarius</a>
      <ul>
        <li>
          <a href="dr8803500s.html#12">Praefatio</a>
        </li>
        <li>
          <a href="dr8803500s.html#26">Index capitum</a>
        </li>
        <li>
          <a href="dr8803500s.html#27">Index epistolarum</a>
        </li>
        <li>
          <a href="dr8803500s.html#28">De obelisco Augusti
          Caesaris e Campi Martii ruderibus nuper eruto</a>
        </li>
        <li>
          <a href="dr8803500s.html#142">Clarorum virorum epistolae
          atque opuscula</a>
        </li>
        <li>
          <a href="dr8803500s.html#250">Index rerum notabilium,
          quae in hoc opere continentur</a>
        </li>
        <li>
          <a href="dr8803500s.html#256">Lettera del Signor Ernesto
          Freeman al Signor Abate Angelo Maria Bandini</a>
        </li>
        <li>
          <a href="dr8803500s.html#261">Lettera del P. Ruggiero
          Giuseppe Boscovichi della Compagnia di Gesù al Signor Ab.
          Angelo Maria Bandini</a>
        </li>
        <li>
          <a href="dr8803500s.html#276">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
