<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502500s.html#6">Trattato Nvovo Delle Cose
      Maravigliose Dell’Alma Città Di Roma, Diuiso in due parti</a>
      <ul>
        <li>
          <a href="dg4502500s.html#6">L’Antichità dell’Alma Città
          Di Roma</a>
          <ul>
            <li>
              <a href="dg4502500s.html#8">Alli benigni et
              amorevoli lettori</a>
            </li>
            <li>
              <a href="dg4502500s.html#10">Le nove chiese
              privilegiate, et principali, dell'alma città di
              Roma</a>
            </li>
            <li>
              <a href="dg4502500s.html#36">Nell'isola</a>
            </li>
            <li>
              <a href="dg4502500s.html#37">In Trastevere</a>
            </li>
            <li>
              <a href="dg4502500s.html#44">Nel Borgo</a>
            </li>
            <li>
              <a href="dg4502500s.html#49">Dalla Porta Flaminia
              fuori della Porta à mano dritta, e sinistra, fin'alla
              Madonna de' Monti</a>
            </li>
            <li>
              <a href="dg4502500s.html#61">Dal Giesu, in Parione,
              strada Giulia alla Regola, e restante insino
              Aracaeli&#160;</a>
            </li>
            <li>
              <a href="dg4502500s.html#83">Dal Campidoglio da ogni
              parte, finendo à S. Agnese di Porta Pia</a>
            </li>
            <li>
              <a href="dg4502500s.html#104">Le Stationi che sono
              nelle Chiese dentro, e fuori di Roma</a>
            </li>
            <li>
              <a href="dg4502500s.html#114">Indice brevissimo de
              Pontefici Romani e delle Scisme, e Concilij
              generali</a>
            </li>
            <li>
              <a href="dg4502500s.html#134">Tavola di tutte le
              Chiese dell'alma città di Roma</a>
            </li>
            <li>
              <a href="dg4502500s.html#140">Li Titoli de li
              Cardinali di S. Chiesa</a>
            </li>
            <li>
              <a href="dg4502500s.html#142">Dell'origine et
              progresso dell'alma Città di Roma, et sue
              antichità</a>
            </li>
            <li>
              <a href="dg4502500s.html#238">Catalogo delle Re, et
              Imperatori Romani, e de molti altri Prencipi</a>
            </li>
            <li>
              <a href="dg4502500s.html#249">Tavola dell'Antichità
              di Roma</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
