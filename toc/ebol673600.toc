<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebol673600s.html#6">Facciata della Cattedrale di
      San Pietro a Bologna &#160;</a>
      <ul>
        <li>
          <a href="ebol673600s.html#7">[Dedica dell'autore]
          Eminentissimo e reverendissimo Principe&#160;</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ebol673600s.html#11">[Tavole]</a>
      <ul>
        <li>
          <a href="ebol673600s.html#11">3. Facciata della
          Cattedrale di San Pietro ▣&#160;</a>
        </li>
        <li>
          <a href="ebol673600s.html#15">4. Spaccato della
          Facciata di dentro ▣</a>
        </li>
        <li>
          <a href="ebol673600s.html#19">5. Spaccato della parte
          dell'Altare Maggiore della Cattedrale di San Pietro di
          Bologna ▣</a>
        </li>
        <li>
          <a href="ebol673600s.html#23">6. Spaccato per il lungo
          della Cattedrale di San Pietro di Bologna ▣</a>
        </li>
        <li>
          <a href="ebol673600s.html#27">7. [Pianta] ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ebol673600s.html#30">Memorie che si leggono nella
      Cattedrale di Bologna</a>
    </li>
  </ul>
  <hr />
</body>
</html>
