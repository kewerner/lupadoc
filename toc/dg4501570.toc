<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501570s.html#4">[Titelblatt] LE COSE
      MARAVIGLIOSE DELL'ALMA CITTA DI ROMA. Dove si tratta delle
      Chiese, Stationi, Indulgenze, &amp; Re liquie de i Corpi
      Santi, che sonno in essa. Con un breve Trattato delle
      Antichità, chiamato La Guida Romana. Et i nomi de i Sommi
      Pontifici, de gl'Imperadori, de i Re di Francia, Re di
      Napoli, de i Dogi di Venetia, &amp; Duchi di Milano:
      ultimamente ristampate.</a>
    </li>
    <li>
      <a href="dg4501570s.html#6">[Cap. I.] DELLA EDIFICATIO di
      Roma, &amp; successo fino alla conversio ne di Constantino
      Magno Impera- tore, &amp; de la donatione fatta a li Sommi
      Pontifici della S.R.Ecclesia.</a>
    </li>
    <li>
      <a href="dg4501570s.html#12">[Cap.II.] DE LE SETTE CHIESIE
      principali.</a>
    </li>
    <li>
      <a href="dg4501570s.html#30">[Cap. III.] NE L'ISOLA.</a>
    </li>
    <li>
      <a href="dg4501570s.html#31">[Cap. IV.] IN TRASTEVERE.</a>
    </li>
    <li>
      <a href="dg4501570s.html#34">[Cap. V.] NEL BORGO.</a>
    </li>
    <li>
      <a href="dg4501570s.html#37">[Cap. VI.] DALLA PORTA
      FLAMMINIA fora del Popolo fino alle radici del
      Campidoglio.</a>
    </li>
    <li>
      <a href="dg4501570s.html#52">[Cap. VII.] DAL CAMPIDOGLIO A
      MAN manca ne li monti.</a>
    </li>
    <li>
      <a href="dg4501570s.html#62">[Cap. VIII.] DAL CAMPIDOGLIO A
      MAN diritta.</a>
    </li>
    <li>
      <a href="dg4501570s.html#69">LE STATIONI Indulgentie, &amp;
      gratie spirituali, che sono in le Chiese di Roma, si per
      tutta la Quadragesima, come per tutto l'anno novamente poste
      in luce da M. Andrea Palladio.</a>
      <ul>
        <li>
          <a href="dg4501570s.html#69">[1] El mese di Zenaro.</a>
        </li>
        <li>
          <a href="dg4501570s.html#71">[2] El mese di Febraro.</a>
        </li>
        <li>
          <a href="dg4501570s.html#72">[3] Il mese di Marzo.</a>
        </li>
        <li>
          <a href="dg4501570s.html#79">[4] El mese d'Aprile.</a>
        </li>
        <li>
          <a href="dg4501570s.html#79">[5] El mese di maggio.</a>
        </li>
        <li>
          <a href="dg4501570s.html#82">[6] El mese di Zugno.</a>
        </li>
        <li>
          <a href="dg4501570s.html#83">[7] El mese di Luglio.</a>
        </li>
        <li>
          <a href="dg4501570s.html#84">[8] El mese d'Agosto.</a>
        </li>
        <li>
          <a href="dg4501570s.html#87">[9] El mese di
          Settembre.</a>
        </li>
        <li>
          <a href="dg4501570s.html#88">[10] El mese
          d'Ottobrio.</a>
        </li>
        <li>
          <a href="dg4501570s.html#88">[11] El mese di
          Novembre.</a>
        </li>
        <li>
          <a href="dg4501570s.html#90">[12] Il mese di
          Decembre.</a>
        </li>
        <li>
          <a href="dg4501570s.html#94">[13] Queste sono speciali
          indulgentie &amp; statione in diverse chiese in Roma,
          concesse per li Sommi Pontefici oltre le soprascitte.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501570s.html#97">[Widmung] LA GUIDA ROMANA. Per
      tutti i Forastieri che vengono per vedere le antichita di
      Roma, a una per una, in bellissima forma &amp; brevita.
      L'Auttore alli Lettori carissimi.</a>
    </li>
    <li>
      <a href="dg4501570s.html#98">[Nachwort]</a>
      <ul>
        <li>
          <a href="dg4501570s.html#98">[I]</a>
          <ul>
            <li>
              <a href="dg4501570s.html#98">[1] DEL BORGHO.</a>
            </li>
            <li>
              <a href="dg4501570s.html#99">[2] Del Trastevere.</a>
            </li>
            <li>
              <a href="dg4501570s.html#100">[3] Dell'Isola
              Tyberina.</a>
            </li>
            <li>
              <a href="dg4501570s.html#100">[4] Del Ponte S.Maria,
              Palazzo di Pilato, &amp; altre cose.</a>
            </li>
            <li>
              <a href="dg4501570s.html#101">[5] Del monte
              Testaccio, &amp; di molte altre cose.</a>
            </li>
            <li>
              <a href="dg4501570s.html#102">[6] Delle Therme
              Antoniane, &amp; altre cose.</a>
            </li>
            <li>
              <a href="dg4501570s.html#103">[7] Di San Giovanni
              Laterano, &amp; altre cose.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501570s.html#104">[II] GIORNATA SECONDA.</a>
          <ul>
            <li>
              <a href="dg4501570s.html#104">[1] Del Sepolchro
              d'Augusto, &amp; altre cose</a>
            </li>
            <li>
              <a href="dg4501570s.html#105">[2] De i Cavalli di
              marmo che stanno a monte Cavallo, &amp; delle Therme
              Diocleriane</a>
            </li>
            <li>
              <a href="dg4501570s.html#107">[3] Del Tempio de
              Iside, &amp; altre cose.</a>
            </li>
            <li>
              <a href="dg4501570s.html#108">[4] Delle sette Sale,
              &amp; del Colisseo, &amp; altre cose.</a>
            </li>
            <li>
              <a href="dg4501570s.html#109">[5] Del Tempio della
              Pace, &amp; del Palazzo mag- giore, &amp; altre
              cose.</a>
            </li>
            <li>
              <a href="dg4501570s.html#110">[6] Del Campidoglio,
              &amp; altre cose.</a>
            </li>
            <li>
              <a href="dg4501570s.html#111">[7] De i portichi di
              Ottavia, &amp; di Settimo, &amp; del Theatro di
              Pompeo.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501570s.html#111">[III] GIORNATA TERZA.</a>
          <ul>
            <li>
              <a href="dg4501570s.html#111">[1] Delle doi Colonne
              una di Antonio Pio, &amp; l'al- tra di Traiano, &amp;
              altre cose.</a>
            </li>
            <li>
              <a href="dg4501570s.html#112">[2] Della Rotonda,
              overo Pantheon.</a>
            </li>
            <li>
              <a href="dg4501570s.html#112">[3] Dei Bagni di
              Agrippa, &amp; di Nerone.</a>
            </li>
            <li>
              <a href="dg4501570s.html#112">[4] Della piazza
              Nagona, &amp; di M.Palaquino.</a>
            </li>
            <li>
              <a href="dg4501570s.html#113">[5] Da poi Pranzo.
              Dell'Anticaglie di Monsignor d'Aquino, &amp; del
              palazzo di san Giorgio, &amp; quello di Monte
              Pulciano</a>
            </li>
            <li>
              <a href="dg4501570s.html#113">[6] Di Belvedere.</a>
            </li>
            <li>
              <a href="dg4501570s.html#114">[7] Delle donne
              Romane.</a>
            </li>
            <li>
              <a href="dg4501570s.html#114">´[8] Di Mons.di
              Piasenza, del Sepolch. di Baccho, &amp; vigna di Papa
              Iulio.</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
