<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501483s.html#3">[Exlibris] GRAF CORONINI
      CRONBERG. S. PETER. N. 3638.</a>
    </li>
    <li>
      <a href="dg4501483s.html#6">[Titelblatt] De Italia
      illustrata e Roma restaurata da Biondo Flavio di Forli
      tradotti da Messer Lucio Fauno Gaetano.</a>
    </li>
    <li>
      <a href="dg4501483s.html#8">PAULUS PAPA III. PAULUS PAPA
      III.</a>
    </li>
    <li>
      <a href="dg4501483s.html#10">[Imprimatur]</a>
    </li>
    <li>
      <a href="dg4501483s.html#11">[Widmung] AL MOLTO MAGNIFICO
      SIGNO- re messer Daniel Veniero del clarissimo Messer
      Marc'Antonio dottore.</a>
    </li>
    <li>
      <a href="dg4501483s.html#13">[Index I] TAVOLA SOPRA ROMA
      RESTAU- rata di Biondo da Forli.</a>
    </li>
    <li>
      <a href="dg4501483s.html#24">[Index II] TAVOLA DE LUOCHI, NE
      L'ITA- lia illustrata di Biondo da Forli.</a>
    </li>
    <li>
      <a href="dg4501483s.html#35">BIONDO FLAVIO DA FORLI à Papa
      Eugenio quarto. BIONDO FLAVIO DA FORLI à Papa Eugenio
      quarto.</a>
    </li>
    <li>
      <a href="dg4501483s.html#38">ROMA RISTAURATA.</a>
      <ul>
        <li>
          <a href="dg4501483s.html#38">LIBRO PRIMO. ROMA
          RISTAURATA DI BIONDO DA FORLI.</a>
        </li>
        <li>
          <a href="dg4501483s.html#86">LIBRO SECONDO. DI ROMA
          RISTAURATA LIBRO SECONDO. De le terme in universale.</a>
        </li>
        <li>
          <a href="dg4501483s.html#133">LIBRO TERZO. DI ROMA
          RISTAURATA LIBRO TERZO.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501483s.html#163">ITALIA ILLUSTRATA.</a>
      <ul>
        <li>
          <a href="dg4501483s.html#163">BIONDO DA FORLI, IN ITALIA
          ILLUSTRATA.</a>
        </li>
        <li>
          <a href="dg4501483s.html#165">L'ITALIA SI DIVIDE IN
          XVIII. REGIONI, O PROVINCIE. L'ITALIA SI DIVIDE IN XVIII.
          REGIONI, O PROVINCIE.</a>
        </li>
        <li>
          <a href="dg4501483s.html#166">ITALIA ILLUSTRATA DI
          BIONDO DA FORLI. ITALIA ILLUSTRATA DI BIONDO DA
          FORLI.</a>
        </li>
        <li>
          <a href="dg4501483s.html#172">IL GENOESATO DETTO GIA LA
          LIGURIA. REGIONE PRIMA. IL GENOESATO DETTO GIA LA
          LIGURIA. REGIONE PRIMA.</a>
        </li>
        <li>
          <a href="dg4501483s.html#184">La Toscana, gia detta
          Etruria. La Toscana, gia detta Etruria. Regione 2.</a>
        </li>
        <li>
          <a href="dg4501483s.html#223">I LATINI, CH' E STATA POI
          CAMPAGNA DI ROMA DETTA REGIONE TERZA. I LATINI, CH' E
          STATA POI CAMPAGNA DI ROMA DET- TA REGIONE TERZA.</a>
        </li>
        <li>
          <a href="dg4501483s.html#262">IL DUCATO DI SPOLETI CHE
          CHIAMARON GLI ANTICHI UMBRIA. REGIONE IIII. IL DUCATO DI
          SPOLETI CHE CHIAMARON GLI ANTICHI UMBRIA. REGIONE
          IIII.</a>
        </li>
        <li>
          <a href="dg4501483s.html#280">LA MARCA D'ANCONA CHIAMATA
          DA GLI ANTICHI PICENO, REGIONE V. LA MARCA D'ANCONA
          CHIAMATA DA GLI ANTICHI PICENO, REGIONE V.</a>
        </li>
        <li>
          <a href="dg4501483s.html#300">ROMAGNA DETTA ANCO
          ROMAGNOLA, E DA LI ANTICHI FLAMINIA. REGIONE VI. ROMAGNA
          DETTA ANCO ROMA- GNOLA, E DA LI ANTICHI FLA- MINIA.
          REGIONE VI.</a>
        </li>
        <li>
          <a href="dg4501483s.html#338">LA LOMBARDIA. REGIONE
          SETTIMA. LA LOMBARDIA. REGIO- NE SETTIMA.</a>
        </li>
        <li>
          <a href="dg4501483s.html#375">LA CONTRADA DI VINEGGIA,
          REGIONE OTTAVA. LA CONTRADA DI VINEGGIA, REGIONE
          OTTAVA.</a>
        </li>
        <li>
          <a href="dg4501483s.html#392">LA MARCA TRIVIGIANA,
          REGIONE NONA. LA MARCA TRIVIGIANA, REGIONE NONA.</a>
        </li>
        <li>
          <a href="dg4501483s.html#420">IL FRIULI, REGIONE DECIMA.
          IL FRIULI, REGIONE DECIMA.</a>
        </li>
        <li>
          <a href="dg4501483s.html#426">L'ISTRIA, REGIONE
          UNDECIMA. L'ISTRIA, REGIONE UNDECIMA.</a>
        </li>
        <li>
          <a href="dg4501483s.html#433">L'ABRUZZO DETTO GIA
          SANNIO, REGIONE DUODECIMA. L'ABRUZZO DETTO GIA SANNIO,
          REGIONE DUODECIMA.</a>
        </li>
        <li>
          <a href="dg4501483s.html#480">TERRA DI LAVORO, GIA DETTA
          CAMPANIA, REGIONE XIII. TERRA DI LAVORO, GIA DETTA
          CAMPANIA, REGIONE XIII.</a>
        </li>
        <li>
          <a href="dg4501483s.html#514">LA PUGLIA. REGIONE
          XIIII.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
