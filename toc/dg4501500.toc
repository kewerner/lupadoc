<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501500s.html#4">[Titelblatt] Urbis Romae
      Topographia, ad Franciscum Gallorum Regem, eiusdem ur- bis
      Liberatorem invictum, libris quinque com- prehensa,
      BARTHOLOMAEO MARLIANO EQUITE D.Petri auctore. ADIECTA PRIORI
      EIUSDEM AUCTORIS Topographiae editioni,in hoc Opere sunt:
      Urbis,atque insignium in ea aedificiorum descriptiones,
      compluraque alia memoratu digna. Errores nonnulli sublati.
      Tituli, inscriptionesque non aliter,quam ipsis in erant
      marmoribus,emendatissime expressi: qui ab alijs ha- ctenus
      neglecto ordine,&amp;perperam in lu- cem editi
      inveniuntur.</a>
    </li>
    <li>
      <a href="dg4501500s.html#6">[Widmung]Praefatio FRANCISCO
      REGI GALLO- rum,urbis Romae liber atori invicto,Bartho-
      lomaeus Marlianus S.D. .</a>
    </li>
    <li>
      <a href="dg4501500s.html#8">[Autorenverzeichnis] AUTORUM
      CATALOGUS, QUO- rum testimonijs in hisce Topographiae li-
      bris nititur Marlianus.</a>
    </li>
    <li>
      <a href="dg4501500s.html#8">[Primärquellenverzeichnis]
      LOCORUM QUAE IN HOC OPERE A MARLIANO VEL citantur,vel
      explicantur,Catalogus:in quo repetitus numerus, plures
      eiusdem autoris locos in eodem folio citatos,ostendit.</a>
    </li>
    <li>
      <a href="dg4501500s.html#9">Index INDEX IN MARLIANI TOPOGRA-
      phiam urbis foecundissimus.</a>
    </li>
    <li>
      <a href="dg4501500s.html#20">LIBER PRIMUS BARTHOLOMEI
      MARLIANI EQUITIS D. PETRI,URBIS RO- mae Topographia.</a>
      <ul>
        <li>
          <a href="dg4501500s.html#20">Cap. I. DE ORIGINE
          URBIS.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#22">In hac figura,atque in
              maiori,proprium cuiusque loci situm ibi esse
              intelligo,ubi prima eius dictionis litera collocata
              est.Neque puto admonendum esse lectorem, in Divo- rum
              nominibus templi nomen subintelligi.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#22">Cap. II. DE URBE
          ROMULI.</a>
        </li>
        <li>
          <a href="dg4501500s.html#24">Cap. III. DE PORTIS URBIS
          ROMULI.</a>
        </li>
        <li>
          <a href="dg4501500s.html#25">Cap. IIII. DE VARIO URBIS
          AMBITU, REGUM consulumque tempore.</a>
          <ul>
            <li>
              <a href=
              "dg4501500s.html#28">SEPTEMTRIO,ORTUS,MERIDIES,OCCASUS.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#29">Cap. V. DE VARIO AMBITU
          URBIS SUB IMPERA- toribus.</a>
        </li>
        <li>
          <a href="dg4501500s.html#32">Cap. VI. DE PORTIS
          GENERATIM.</a>
        </li>
        <li>
          <a href="dg4501500s.html#34">Cap. VII. DE SITU
          URBIS.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#35"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#35">Cap. VIII. Sequitur
          universalis ipsius urbis Romae typus. DE PORTIS NUNC
          EXTANTIBUS, AC PRIMO de Flumentana.</a>
        </li>
        <li>
          <a href="dg4501500s.html#42">Cap. IX. DE MOENIBUS
          URBIS.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501500s.html#42">LIBER SECUNDUS</a>
      <ul>
        <li>
          <a href="dg4501500s.html#42">Cap. I. DE CAPITOLIO.</a>
        </li>
        <li>
          <a href="dg4501500s.html#44">Cap. II. DE TEMPLIS IOVIS
          FERETRII, IOVIS CU- stodis,Veiouis:&amp;de Curia
          Calabra,Senaculo,atque Asylo.</a>
        </li>
        <li>
          <a href="dg4501500s.html#45">Cap. III. DE
          CLIVO,VARIISQUE ADITIBUS CAPITOLII, DOMO Milonis,porta
          Stercoraria:ac de templis Iovis
          Tonantis,&amp;Fortunae.</a>
        </li>
        <li>
          <a href="dg4501500s.html#47">Cap. IIII. DE RUPE TARPEIA:
          TEMPLIS, SATURNI, IOVIS
          Opt.Max.Iunonis,Minervae,Inventutis,&amp;Fidei.</a>
        </li>
        <li>
          <a href="dg4501500s.html#50">Cap. V. DE ARCE, TEMPLO
          IUNONIS MONETAE: AC DE domo T.Tatij,M.Manlij:atque de
          Saxo Carmentae.</a>
        </li>
        <li>
          <a href="dg4501500s.html#51">Cap. VI. DE TEMPLIS, ET
          QUIBUSDAM ALIIS AEDIFI- cijs in Capitolio
          extructis,quorum certus locus ignoratur.</a>
        </li>
        <li>
          <a href="dg4501500s.html#53">Cap. VII. DE STATUIS,
          IMAGINIBUS,ET COLUMNIS.</a>
        </li>
        <li>
          <a href="dg4501500s.html#54">Cap. VIII. De TABULIS
          PUBLICIS.</a>
        </li>
        <li>
          <a href="dg4501500s.html#56">Cap. IX. DE QUIBUSDAM
          STATUIS, ALIISQUE REBUS antiquis,quae hodie sunt in
          Capitolio.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#56"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#57">Cap. X. DE TEMPLO
          CONCORDIAE, SENACULO, Curia:ac de Tabernis publicis.</a>
        </li>
        <li>
          <a href="dg4501500s.html#60">Cap. XI. DE TEMPLIS IOVIS
          STATORIS, ET VESTAE, eiusque Luco:ac de Regia
          Numae,&amp;domo Tarquinij Superbi.</a>
        </li>
        <li>
          <a href="dg4501500s.html#61">Cap. XII. DE TEMPLO
          QUIRINI: ET DE LUPERCALI, &amp;ficu Ruminali.</a>
        </li>
        <li>
          <a href="dg4501500s.html#63">Cap. XIII. DE VICO IUGARIO,
          AC THUSCO:ET DE via Nova.</a>
        </li>
        <li>
          <a href="dg4501500s.html#64">Cap. XIIII. DE ARCUBUS
          ROMULI, ET DE DOMO- Ovidij.</a>
        </li>
        <li>
          <a href="dg4501500s.html#65">Cap. XV. DE PALATINI MONTIS
          ETYMOLOGIA, PALA- tio,eiusque atrio,&amp;porta.</a>
        </li>
        <li>
          <a href="dg4501500s.html#65">Cap. XVI. DE GRAEGOSTASIE;
          AEDE CONCORDIAE, SE- naculo,&amp;Basilica Opimij.</a>
        </li>
        <li>
          <a href="dg4501500s.html#66">Cap. XVII. DE CASA
          FAUSTULI, DOMO CATILINAE,AC SCAU- ri:quid sit
          Germalus,&amp;quid Velia.</a>
        </li>
        <li>
          <a href="dg4501500s.html#67">Cap. XVIII. DE TEMPLIS
          VICTORIAE, IUNONIS SOSPITAE, Penatium,Orci,Magnae
          Matris:ac de domo Publicolae,&amp;Ser. Tulij.</a>
        </li>
        <li>
          <a href="dg4501500s.html#68">Cap. XIX. DE TEMPLO FIDEI,
          APOLLINIS, EIUSQUE BI- bliotheca:ac de Balneis
          Palatinis.</a>
        </li>
        <li>
          <a href="dg4501500s.html#69">Cap. XX. DE DOMIBUS,
          AUGUSTI,TIBERII, CICERONIS, M.
          Flacci,Cra�i,Q.Catuli,&amp;Romuli:ac de
          templis,Victoriae,&amp;Libertatis.</a>
        </li>
        <li>
          <a href="dg4501500s.html#69">Cap. XXI. DE VARIIS
          AEDIFICIIS, REBUSQUE NONNUL- lis quae in Palatino monte
          olim fuere.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501500s.html#70">LIBER TERTIUS</a>
      <ul>
        <li>
          <a href="dg4501500s.html#70">Cap. I. DE FORO ROMANO:
          TEMPLIS, CAESARIS, CA- storum,&amp;Augusti:ac de
          Rostris.</a>
        </li>
        <li>
          <a href="dg4501500s.html#72">Cap. II. DE CARCERE
          TULLIANO, ARCU SEPTIMII, MIL- liario Aureo,equo
          Domitiani,&amp;de lacu Curtij,Ianique sacello.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#73"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#74">Cap. III. DE MARFORI
          STATUA, SECRETARIO PO. RO. templo Saturni,Aerario,at de
          officina Monetae.</a>
        </li>
        <li>
          <a href="dg4501500s.html#76">Cap. IIII. DE BASILICA
          PAULI AEMILII,TEM- plo Faustinae,arcuque Fabiano.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#76">DIVO ANTONI- NO,ET
              DIVAE FAUSTI- NAE,EX s.c. .</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#77">Cap. V. DE VARIIS REBUS
          QUAE IN FORO FUIS- se dicuntur.</a>
        </li>
        <li>
          <a href="dg4501500s.html#78">Cap. VI. DE COMITIO, TEMPLO
          QUIRINI, COLUMNA Moenia,basilica Porcia,Curia
          Hostilia,&amp;veteri,ac de Rostris vete- ribus.</a>
        </li>
        <li>
          <a href="dg4501500s.html#81">Cap. VII. DE DOMO CAESARIS,
          PORTICU LIVIAE, PACIS templo,ac de Ficu Ruminali.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#82">Templi huius etsi pars
              quaedam adhuc extat,ta- men quia plena operis or-
              thographia ex ea percipi non potuit,ichnographi- am
              apposuimus:secuti ex- emplar quorundam mecha-
              nicorum,qui eam in lucem ediderant,existimantes sa-
              tis accurate id eos fecisse. Postea vero deprehendi-
              mus,eius latitudinem,non CLXX,ut ab eis tradi- tum
              est,sed CC.pedum es- se:longitudinem CCC. Id ne
              lectorem fallat,ad- monere volui- mus.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#83">Cap. VIII. DE ARCU
          VESPASIANI: TEMPLIS, VULCANI Solisque ac Lunae:atque de
          Via Sacra.</a>
        </li>
        <li>
          <a href="dg4501500s.html#84">Cap. IX. DE FORO AUGUSTI,
          NERVAE, ET CAESA- ris:ac de templis Martis
          Ultoris,Iani,ac Veneris Genitricis.</a>
        </li>
        <li>
          <a href="dg4501500s.html#86">Cap. X. DE TEMPLO TELLURIS:
          ET DE DOMO SP. CAS- sij,&amp;M.Antonij:ac de Sororio
          Tigillo,vico Scelerato:atque de bustis Gallicis.</a>
        </li>
        <li>
          <a href="dg4501500s.html#87">Cap. XI. DE THEATRO
          MARCELLI: CURIA, PORTICU &amp;Schola Octaviae:ac de
          templo Iunonis,Apollinisque de lubro.</a>
        </li>
        <li>
          <a href="dg4501500s.html#88">Cap. XII. DE FORO OLITORIO:
          TEMPLIS, IUNONIS
          Matutae,Spei,Iani,Pietatis,&amp;Carmentae:ac de carcere
          Pl.Ro.Sacrario Numae,et de Columna Lactaria.</a>
        </li>
        <li>
          <a href="dg4501500s.html#89">Cap. XIII. DE ARGILETO:
          DOMO SP. MELII, ET AFRI- cani:&amp;de Aequimelio,Tabernis
          Bibliopolarum,Basilica a Sempronij,ac de Asylo.</a>
        </li>
        <li>
          <a href="dg4501500s.html#90">Cap. XIIII. DE VELABRO,
          FORO BOARIO, ARCU SEPTI- mij, Vertumni signo:&amp;de Iano
          quadrifronte,ac de Sepulchro Accae Larentiae.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#93"></a>
            </li>
            <li>
              <a href="dg4501500s.html#94">HERCULIS SIMU-
              LACHRUM.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#95">Cap. XV. DE AEDE, ARA, ET
          SIMULACRO HERCULIS ac templis
          Pudicitiae,Matutae,Fortunae,&amp;Murtiae.</a>
        </li>
        <li>
          <a href="dg4501500s.html#96">Cap. XVI. DE FORO
          PISCARIO:TEMPLIS,FORTUNAE VIRI- lis,&amp;Vestae:&amp;de
          Salinis,atque Arcu Horatij Coclitis.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#97">In figura Urbis commu-
              nem opinionem secuti,hoc templum Pudicitiae ascri-
              psimus.Sed esse Fortunae, praeter verba Dionysij,e-
              ius quoque structura facit ut credamus:quae cum sit
              Io- nica,media inter Corin- thiam&amp; Doricam, huic
              potius deae convenit.Nam &amp; ipsa media est,bona
              sci- licet,&amp;mala. Templi longitudo est ped. LVI.
              latitudo XXVI.</a>
            </li>
            <li>
              <a href="dg4501500s.html#98"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#99">Cap. XVII. CIRCUS QUID SIT,
          UNDE DICTUS, CUIUS rei causa institutus:&amp;de eo qui
          Maximus dicitur.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#101"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#102">Cap. XVIII. DE TEMPLO
          CONSI, EIUS QUE ARA: ET DE aede Neptuni,ac
          Iuventutis:atque de Fornice Sertinij.</a>
        </li>
        <li>
          <a href="dg4501500s.html#103">Cap. XIX. DE OBELISCIS,ET
          DE EO QUIIN CIRCO Maximo iacet.</a>
        </li>
        <li>
          <a href="dg4501500s.html#104">Cap. XX. DE TEMPLIS,
          SOLIS, FLORAE, LIBERI, LIBERAE QUE,
          Cereris,Veneris,&amp;Mercurij:&amp;de Lupanari,ac domo
          Pompeij.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501500s.html#105">LIBER QUARTUS</a>
      <ul>
        <li>
          <a href="dg4501500s.html#105">Cap. I. DE SALINIS,
          LIGNARIIS, VITRIARIIS; FIGULIS, Venere Myrtea,ac de Circo
          Intimo,atque de Testaceo.</a>
        </li>
        <li>
          <a href="dg4501500s.html#106">Cap. II. DE HORREIS PO.
          RO. SEPULCRO C. CESTII: &amp;de Hylerna luco.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#107">OPUS. APSOLUTUM. EX.
              TESTAMENTO. DIEBUS. CCC: XXX. ARBITRATU PONTI. P. F.
              CALMELAE. HAEREDIS. ET. PONTHI. L.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#108">Cap. III. DE AVENTINO,
          TEMPLO BONAE DEAE, ET Herculis,&amp;de Armilustro,ac
          Remoria.</a>
        </li>
        <li>
          <a href="dg4501500s.html#109">Cap. IIII. DE TEMPLO
          DIANAE, ET IUNONIS REGINAE: spelunca Caci,Clivo pub.ac de
          Thermis Traiani,&amp;Decij:&amp; de scalis Gemonijs.</a>
        </li>
        <li>
          <a href="dg4501500s.html#110">Cap. V. DE QUIBUSDAM
          TEMPLIS, AEDIFICIIS QUE ALIIS, QUAE erant in
          Aventino,quorum certus locus ignoratur.</a>
        </li>
        <li>
          <a href="dg4501500s.html#111">Cap. VI. DE ARCU
          CONSTANTINI, ET DE SEPTI- zonio Severi Imp.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#112"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#112">Cap. VII. DE THERMIS, ET
          PALATIO ANTONINI Caracallae.</a>
        </li>
        <li>
          <a href="dg4501500s.html#114">Cap. VIII. DE TEMPLIS,
          ISIDIS, HONORIS, VIRTUTIS, Quirini,&amp;Dianae:ac de
          Coeliolo.</a>
        </li>
        <li>
          <a href="dg4501500s.html#115">Cap. IX. DE COELIO MONTE,
          CURIA HOSTILIA, MANSIONIBUS
          Albanorum:Templis,Fauni,Veneris,&amp;Cupidinis:Castris
          peregrinis:domo Laterano- rum:&amp;de Amphitheatro
          Statilij Tauri.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#116"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#117">Cap. X. DE AQUIS URBIS,
          EARUMQUE ductibus.</a>
        </li>
        <li>
          <a href="dg4501500s.html#123">Cap. XI. DE AQUA CLAUDIA,
          EIUSQUE DU- ctu.</a>
        </li>
        <li>
          <a href="dg4501500s.html#125">Cap. XII. DE IIS QUAE IN
          COELIO ERANT, SED CER- tus eorum locus ignoratur.</a>
        </li>
        <li>
          <a href="dg4501500s.html#126">Cap. XIII. DE META, QUAE
          SUDANS DICTA EST: de Amphitheatro Titi:&amp;de
          Templis,Fortunae,atque Quietis.</a>
        </li>
        <li>
          <a href="dg4501500s.html#127">Cap. XIIII. DE EXQUILIIS,
          CARINIS, CURIA: DE THERMIS, &amp;domo Titi Imp.ac de
          Statua Laocoontis:&amp;de domo Balbini,atque Pom- peij
          Magni.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#129"></a>
            </li>
            <li>
              <a href="dg4501500s.html#130"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#131">Cap. XV. "DE TABERNOLA,
          DOMO AELIORUM, BASILICA CAII &amp;Lucij,Clivo
          Suburrano,Arcu Gallieni,Trophaeis Marij:ac de Thermis,
          &amp;domo Gordiani Imp.</a>
        </li>
        <li>
          <a href="dg4501500s.html#132">Cap. XVI. DE AQUA
          MARTIA.</a>
        </li>
        <li>
          <a href="dg4501500s.html#133">Cap. XVII. DE AQUA IULIA,
          ET TE- pula.</a>
        </li>
        <li>
          <a href="dg4501500s.html#134">Cap. XVIII. DE CLIVO
          URBICO, DOMO SER. TULLII, &amp;Neronis:ac de aede
          Fortunae Seiae.</a>
        </li>
        <li>
          <a href="dg4501500s.html#135">Cap. XIX. DE AGGERE
          TARQUINII, DOMO POMPEII, ET Virgilij:&amp;de
          Turri,&amp;Hortis Mecoenatis:ac de campo&amp; foro
          Exquilino.</a>
        </li>
        <li>
          <a href="dg4501500s.html#136">Cap. XX. DE SUBURRA,
          TEMPLO SYLVANI ET DE Testamento Iocundi militis.</a>
        </li>
        <li>
          <a href="dg4501500s.html#138">Cap. XXI. DE COLLE
          VIMINALI, LAVACRO AGRIPPINAE: de Thermis
          Olympiadis,Novati,&amp;Diocletiani:&amp;Bibliotheca
          Vulpia,ac de Domo Caij Aquilij.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#139">Long.ped.CCXCIII.
              Lat.ma.LXXXIIII. Minor,XXVIII. Crassitudo muri,V.
              Pilarum, IIII.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#140">Cap. XXII. DE MONTE
          QUIRINALI, AEDE FORTUNAE, APOL-
          linis,Iovis,Iunonis,Minervae:&amp;de Turri
          Militiarum,Balneis Pauli Aemilij,Thermis Constantini:ac
          de domo Cor- neliorum.</a>
        </li>
        <li>
          <a href="dg4501500s.html#141">Cap. XXIII. DE DOMO
          POMPONII ATTICI, ET FLAVII SABINI: Templo Quirini,Alta
          Semita,Vico,&amp;Statua Mamurri:&amp;de Foro,&amp;Hortis
          Salustij,atque de Campo Scelerato.</a>
        </li>
        <li>
          <a href="dg4501500s.html#143">Cap. XXIIII. DE TEMPLIS,
          SALUTIS, FORTUNAE PRIMIGENIAE, Gentis Flaviae,
          Herculis,Honoris,Florae:&amp;de Senatulo mulierum.</a>
        </li>
        <li>
          <a href="dg4501500s.html#143">Cap. XXV. DE FORO
          ARCHEMORIO, PILA TIBURTINA, DOMO Martialis, Circo
          Florae:&amp;de Minij officinis.</a>
        </li>
        <li>
          <a href="dg4501500s.html#144">Cap. XXVI. DE COLLE
          HORTULORUM, DOMO PINCII Senatoris,ac de Sepulcro
          Neronis.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501500s.html#145">LIBER QUINTUS</a>
      <ul>
        <li>
          <a href="dg4501500s.html#145">Cap. I. DE FORO, PORTICU,
          ARCU, ET COLUM- na Cochlidis Traiani.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#146"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#147">Cap. II. DE SEPULCRO C.
          POBLICII, DOMO CORVINA, VIA Lata:&amp;de
          Isidis&amp;Minervae templo,atque de foro Suario.</a>
        </li>
        <li>
          <a href="dg4501500s.html#148">Cap. III. DE CAMPO MARTIO,
          ET AGRIPPAE: AC DE aede Larium.</a>
        </li>
        <li>
          <a href="dg4501500s.html#149">Cap. IIII. DE ANTONINI PII
          PALATIO, ET PORTICU, DE- que eiusdem Antonini Columna.
          Praeterea de Septis,Monte Citatorum, &amp;Villa
          Publica.</a>
        </li>
        <li>
          <a href="dg4501500s.html#150">Cap. V. DE AQUA VIRGINE,
          AEDE ET LACU IU- turnae,templo Pietatis:&amp;de
          Amphitheatro Claudij Imp.</a>
        </li>
        <li>
          <a href="dg4501500s.html#152">Cap. VI. DE ARCU
          DOMITIANI, TEMPLO IUNONIS Lucinae:&amp;de Obelisco campi
          Martij.</a>
        </li>
        <li>
          <a href="dg4501500s.html#153">Cap. VII. DE VALLE MARTIA,
          NAUMACHIA DOMITIANI, Templo Gentis Flaviae,&amp;Mausolaeo
          Aug.De duobus Obeliscis, ac de quibusdam sepulcris.</a>
        </li>
        <li>
          <a href="dg4501500s.html#154">Cap. VIII. DE TEMPLIS,
          BELLONAE, APOLLINIS, &amp;Martis:&amp;de Porticu
          Mercurij,atque Columna bellica.</a>
        </li>
        <li>
          <a href="dg4501500s.html#155">Cap. IX. DE CIRCO
          FLAMINIO: ET DE AEDIBUS
          sacris,Neptuni,Vulcani,Herculis,Iovis Statoris,
          &amp;Castoris.</a>
        </li>
        <li>
          <a href="dg4501500s.html#157">Cap. X. DE PORTICU
          OCTAVII: ET DE THEATRO, Domo,Curia,&amp;Porticu
          Pompeij,ac de Veneris Victricis templo.</a>
        </li>
        <li>
          <a href="dg4501500s.html#159">Cap. XI. DE THERMIS
          AGRIPPAE: TEMPLO BONI Eventus:&amp;de Pantheo,eiusque
          Porticu.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#161"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#162">Cap. XII. DE THERMIS
          NERONIS, ET ALEXANDRI, ET DE Circo qui nunc Agon
          dicitur,templo Neptuni,ara Ditis,&amp;de Ca- prea
          palude.</a>
        </li>
        <li>
          <a href="dg4501500s.html#163">Cap. XIII. DE TIBERINO
          FLUMINE, ET DE NA- valibus.</a>
        </li>
        <li>
          <a href="dg4501500s.html#164">Cap. XIIII. DE PONTE
          SACRO, AC SU- blicio</a>
        </li>
        <li>
          <a href="dg4501500s.html#165">Cap. XV. DE PONTE
          SENATORUM, ET CLOACA Max.</a>
        </li>
        <li>
          <a href="dg4501500s.html#166">Cap. XVI. DE INSULA
          TIBERINA: TEMPLIS, AESCU- lapij,Iovis,&amp;Fauni.</a>
        </li>
        <li>
          <a href="dg4501500s.html#168">Cap. XVII. DE PONTIBUS,
          FABRICIO CESTIO, &amp;Aurelio.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#168"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#169">Cap. XVIII. DE PONTIBUS,
          TRIUMPHALI, Haelio,&amp;Milvio.</a>
        </li>
        <li>
          <a href="dg4501500s.html#170">Cap. XIX. DE TRANSTIBERINA
          REGIONE: ET DE TEMPLIS, Ravenatium,&amp;Fortis
          Fortunae:ac de Thermis
          Severi,&amp;Aureliani:Naumachia,&amp; Hortis Caesaris:nec
          non de Aqua Alsietina,Pratisque Mutij.</a>
        </li>
        <li>
          <a href="dg4501500s.html#171">Cap. XX. DE SEPULCRO
          NUMAE, ET CECILII POETAE: TRI- bunali Aurelio, Monte
          Ianiculo,&amp;de Hortis cuiusdam Martialis.</a>
        </li>
        <li>
          <a href="dg4501500s.html#172">Cap. XXI. DE MONTE, ET
          CAMPO VATICANO: TEMPLIS APOL- linis&amp;Martis:&amp;de
          Naumachia,Circo,&amp;hortis Neronis:ac Obelisco,qui est
          in eodem circiloco.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#173"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#174">Cap. XXII. DE AQUA
          SABATINA: ET DE SEPULCRO SCIPIO- nis,&amp;Hadriani:ac
          Pratis Quintijs.</a>
        </li>
        <li>
          <a href="dg4501500s.html#175">Cap. XXIII. DE VIIS,
          FLAMINIA, AEMILIA, CLAUDIA,ET Ca�ia:ac de villa
          Caesarum.</a>
        </li>
        <li>
          <a href="dg4501500s.html#176">Cap. XXIIII. DE VIIS,
          COLLATINA ET SALARIA: TEMPLIS,
          Veneris,Cereris,&amp;Honoris:ac de sepulcris
          Licinij,Marij,&amp; Iliae:&amp;de ponte Salario.</a>
        </li>
        <li>
          <a href="dg4501500s.html#178">Cap. XXV. DE VIA
          NUMENTANA, NAENIAE SACELLO, TEM- plo Bacchi,Suburbano
          Phanoti,&amp;de castro Custodiae.</a>
          <ul>
            <li>
              <a href="dg4501500s.html#179"></a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501500s.html#179">Cap. XXVI. DE VIIS,
          PRAENESTINA, LABICANA, CAMPANA, Gabina:&amp;de
          Pontibus,Mamaeo,&amp;Lucano:Aniene veteri,&amp;nova, ac
          de aqua Appia.</a>
        </li>
        <li>
          <a href="dg4501500s.html#182">Cap. XXVII. DE VIIS,
          VALERIA, LATINA ET APPIA: TEMPLIS, FOR- tunae
          Muliebris,Martis,Tempestatis,Spei,Ridiculi:item de
          sacrario Bonae deae:luco&amp;aede Camoenarum,Circo
          Antonini:&amp;de Aqua Mercurij.</a>
        </li>
        <li>
          <a href="dg4501500s.html#185">Cap. XXVIII. DE VIIS,
          OSTIENSI, PORTUENSI, AURELIA, ET Vitellia:&amp;de Hortis
          Galbae.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
