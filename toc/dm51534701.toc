<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dm51534701s.html#10">Joannis Ciampini romani
      Vetera monimenta, in quibus praecipuè musiva opera sacrarum,
      profanarumque aedium structura, ac nonnulli antiqui ritus,
      dissertationibus, iconibusque illustrantur, prima pars</a>
      <ul>
        <li>
          <a href="dm51534701s.html#12">Eminentissimo ac
          Reverendissimo Principi Alexandro S. R. E. Diacono
          Cardinali Albano</a>
        </li>
        <li>
          <a href="dm51534701s.html#16">Lectori Antiquitatis
          Studioso Carolus Giannini</a>
        </li>
        <li>
          <a href="dm51534701s.html#17">Joannis Ciampini operum
          editorum elenchus</a>
        </li>
        <li>
          <a href="dm51534701s.html#18">Operum ineditorum
          elenchus</a>
        </li>
        <li>
          <a href="dm51534701s.html#20">Praefatio auctoris</a>
        </li>
        <li>
          <a href="dm51534701s.html#26">Joannis Justini Ciampini
          Vita</a>
        </li>
        <li>
          <a href="dm51534701s.html#41">Benedictus PP. XIV. ad
          futuram rei Memoriam</a>
        </li>
        <li>
          <a href="dm51534701s.html#42">Index Capitum</a>
        </li>
        <li>
          <a href="dm51534701s.html#43">Index tabularum, aere
          incisarum</a>
        </li>
        <li>
          <a href="dm51534701s.html#46">Joannis Ciampini Romani
          Vetera monimenta</a>
        </li>
        <li>
          <a href="dm51534701s.html#474">Index rerum et verborum
          memorabilium</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
