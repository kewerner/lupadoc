<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501540s.html#8">Descritione De Le Chiese,
      Stationi, Indulgenze &amp; Reliquie de Corpi Sancti, che
      sonno in la Città de Roma</a>
      <ul>
        <li>
          <a href="dg4501540s.html#10">Alli lettori</a>
        </li>
        <li>
          <a href="dg4501540s.html#14">Della edificatione di Roma,
          et successo sino alla conversione di Constantino Magno
          Imperatore, et de la donatione fatta alli Sommi Pontifici
          de la S. R. Ecclesia&#160;</a>
        </li>
        <li>
          <a href="dg4501540s.html#20">De le sette Chiesie
          principiali</a>
        </li>
        <li>
          <a href="dg4501540s.html#38">Ne l'Isola</a>
        </li>
        <li>
          <a href="dg4501540s.html#38">In Trastevere</a>
        </li>
        <li>
          <a href="dg4501540s.html#42">Nel Borgo</a>
        </li>
        <li>
          <a href="dg4501540s.html#44">Dalla Porta Flaminia fora
          del Populo fino alle radici del Campidoglio</a>
        </li>
        <li>
          <a href="dg4501540s.html#59">Dal Campidoglio a man manca
          ne li monti</a>
        </li>
        <li>
          <a href="dg4501540s.html#67">Dal Campidoglio a mano
          &#160;dritta</a>
        </li>
        <li>
          <a href="dg4501540s.html#76">Le Stationi Indulgentie, et
          grazie spirituali, che sono in le chiese di Roma, si per
          tutta la Quadragesima, come per tutto l'anno nuovamente
          poste in luce&#160;</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501540s.html#108">L’ Antichità Di Roma</a>
      <ul>
        <li>
          <a href="dg4501540s.html#110">Alli lettori</a>
        </li>
        <li>
          <a href="dg4501540s.html#111">Tavola</a>
        </li>
        <li>
          <a href="dg4501540s.html#113">Delle Antichità della
          città di Roma</a>
          <ul>
            <li>
              <a href="dg4501540s.html#113">Dell'edificatione di
              Roma&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#115">Del Circuito di
              Roma</a>
            </li>
            <li>
              <a href="dg4501540s.html#116">Delle Porte&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#117">Delle Vie</a>
            </li>
            <li>
              <a href="dg4501540s.html#118">Delli Ponti, che sono
              sopra il Tevere, et suoi edificatori&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#119">Dell'Isola del
              Tevere</a>
            </li>
            <li>
              <a href="dg4501540s.html#119">Delli Monti</a>
            </li>
            <li>
              <a href="dg4501540s.html#120">Del Monte di
              Testaccio</a>
            </li>
            <li>
              <a href="dg4501540s.html#121">Delle Acque, et chi le
              condusse in Roma&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#121">Della Cloacha</a>
            </li>
            <li>
              <a href="dg4501540s.html#122">Delli Acquedotti</a>
            </li>
            <li>
              <a href="dg4501540s.html#122">Delle sette Sale</a>
            </li>
            <li>
              <a href="dg4501540s.html#123">Delle Therme, cioe
              bagni, et suoi edificatori&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#124">Delle Naumachie, dove
              si facevano le battaglie navali, et che cose
              erano&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#124">De' Cerchi, et che
              cosa erano&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#125">De Theatri, et che
              cosa erano, et suoi edificatori&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#125">De' Anfitheatri, et
              suoi edificatori, et che cose erano&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#125">De' Fori, cioe
              Piazze</a>
            </li>
            <li>
              <a href="dg4501540s.html#126">Delli Archi Trionfali,
              et a chi davano</a>
            </li>
            <li>
              <a href="dg4501540s.html#127">De' Portichi</a>
            </li>
            <li>
              <a href="dg4501540s.html#127">De' Trofei, et colonne
              memorande</a>
            </li>
            <li>
              <a href="dg4501540s.html#128">De' Colossi</a>
            </li>
            <li>
              <a href="dg4501540s.html#128">Delle Piramidi</a>
            </li>
            <li>
              <a href="dg4501540s.html#128">Delle Mete</a>
            </li>
            <li>
              <a href="dg4501540s.html#129">Delli Obelischi, overo
              Aguglie</a>
            </li>
            <li>
              <a href="dg4501540s.html#129">Delle Statue</a>
            </li>
            <li>
              <a href="dg4501540s.html#129">Del Marforio</a>
            </li>
            <li>
              <a href="dg4501540s.html#129">De Cavalli</a>
            </li>
            <li>
              <a href="dg4501540s.html#130">Delle Librarie</a>
            </li>
            <li>
              <a href="dg4501540s.html#130">Delli Horivoli</a>
            </li>
            <li>
              <a href="dg4501540s.html#130">De' Palazzi</a>
            </li>
            <li>
              <a href="dg4501540s.html#131">Della Casa Aurea di
              Nerone</a>
            </li>
            <li>
              <a href="dg4501540s.html#131">Dell'altre case de'
              Cittadini</a>
            </li>
            <li>
              <a href="dg4501540s.html#132">Delle Curie et che
              cosa erano&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#132">De' Senatuli, et che
              cosa erano&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#132">De' Magistrati</a>
            </li>
            <li>
              <a href="dg4501540s.html#133">Dei Comiti et che cosa
              erano&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#133">Delle Tribu</a>
            </li>
            <li>
              <a href="dg4501540s.html#134">Delle regioni, cioe
              rioni, et sue insegne&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#134">Delle Basiliche, et
              che cosa erano&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#134">Del Campidoglio</a>
            </li>
            <li>
              <a href="dg4501540s.html#135">Dello Erario, cioe
              camera del commune, e che moneta si spendeva in Roma
              in quei tempi</a>
            </li>
            <li>
              <a href="dg4501540s.html#136">Del Gregostasi, et che
              cosa era&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#136">Della Secretaria del
              popolo Romano</a>
            </li>
            <li>
              <a href="dg4501540s.html#136">Dell'Asilio</a>
            </li>
            <li>
              <a href="dg4501540s.html#136">Delle Rostre, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501540s.html#136">Della Colonna detta
              Miliario</a>
            </li>
            <li>
              <a href="dg4501540s.html#137">Del Tempio di
              Carmenta</a>
            </li>
            <li>
              <a href="dg4501540s.html#137">Della Colonna
              bellica</a>
            </li>
            <li>
              <a href="dg4501540s.html#137">Della Colonna
              lattaria</a>
            </li>
            <li>
              <a href="dg4501540s.html#137">Dell'Equimelio</a>
            </li>
            <li>
              <a href="dg4501540s.html#137">Del Campo Marzo</a>
            </li>
            <li>
              <a href="dg4501540s.html#137">Del Tigillo
              Sororio</a>
            </li>
            <li>
              <a href="dg4501540s.html#138">De' Campi
              forastieri</a>
            </li>
            <li>
              <a href="dg4501540s.html#138">Della Villa
              publica</a>
            </li>
            <li>
              <a href="dg4501540s.html#138">Della Taberna
              meritoria</a>
            </li>
            <li>
              <a href="dg4501540s.html#138">Del Vivario</a>
            </li>
            <li>
              <a href="dg4501540s.html#138">De gli Horti</a>
            </li>
            <li>
              <a href="dg4501540s.html#139">Del Velabro</a>
            </li>
            <li>
              <a href="dg4501540s.html#139">Delle Carine</a>
            </li>
            <li>
              <a href="dg4501540s.html#139">Delli Clivi</a>
            </li>
            <li>
              <a href="dg4501540s.html#140">De' Prati</a>
            </li>
            <li>
              <a href="dg4501540s.html#140">De' granari publici,
              et magazini del sale</a>
            </li>
            <li>
              <a href="dg4501540s.html#140">Delle carceri
              publiche</a>
            </li>
            <li>
              <a href="dg4501540s.html#140">Di alcune feste, et
              gli ochi che si solavano celebrare in Roma</a>
            </li>
            <li>
              <a href="dg4501540s.html#141">Del Sepolcro di
              Augusto, d'Adriano, et di Settimio&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#142">De' Tempii</a>
            </li>
            <li>
              <a href="dg4501540s.html#143">De' Sacerdoti delle
              Vergini</a>
            </li>
            <li>
              <a href="dg4501540s.html#145">Dell'Armamentario et
              che cosa era</a>
            </li>
            <li>
              <a href="dg4501540s.html#145">Dell'Esercito romano
              di terra, et di mare, et loro insegne</a>
            </li>
            <li>
              <a href="dg4501540s.html#145">De' Trionfi, et a chi
              si concedevano, et chi fu il primo trionfatore, et di
              quante maniere erano</a>
            </li>
            <li>
              <a href="dg4501540s.html#146">Delle Corone, et a chi
              davano</a>
            </li>
            <li>
              <a href="dg4501540s.html#146">Del numero del popolo
              Romano&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#146">Delle ricchezze del
              popolo Romano&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#147">DellalLiberalita de li
              antichi Romani&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#147">Delli matrimonii
              antichi, et loro usanza</a>
            </li>
            <li>
              <a href="dg4501540s.html#148">Della buono creanza,
              che davano a i figliuoli</a>
            </li>
            <li>
              <a href="dg4501540s.html#148">Della separatione de'
              matromonii&#160;</a>
            </li>
            <li>
              <a href="dg4501540s.html#149">Dell'essequie antiche,
              et sue ceremonie</a>
            </li>
            <li>
              <a href="dg4501540s.html#150">Delle Torri</a>
            </li>
            <li>
              <a href="dg4501540s.html#150">Del Tevere</a>
            </li>
            <li>
              <a href="dg4501540s.html#150">Del Palazzo Papale et
              di Belvedere</a>
            </li>
            <li>
              <a href="dg4501540s.html#151">Delö Trastevere</a>
            </li>
            <li>
              <a href="dg4501540s.html#151">Recapitulatione
              dell'antichita</a>
            </li>
            <li>
              <a href="dg4501540s.html#152">De' Tempii de gli
              Antichi fuori di Roma</a>
            </li>
            <li>
              <a href="dg4501540s.html#154">Quante volte e stata
              presa Roma</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
