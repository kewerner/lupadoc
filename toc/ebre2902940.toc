<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebre2902940s.html#6">Vago, e curioso ristretto
      profano, e sagro dell’historia bresciana</a>
      <ul>
        <li>
          <a href="ebre2902940s.html#10">A chi vuol leggere.
          L'Autore</a>
        </li>
        <li>
          <a href="ebre2902940s.html#14">Parte prima profana</a>
        </li>
        <li>
          <a href="ebre2902940s.html#182">Parte seconda sagra</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
