<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="hh84972140s.html#6">Breve relatione della vita, et
      gesti del reverendiss.P. M. Enrico Silvio Astergiano,
      generale della religione della gloriosa Verg. Maria del
      Carmine</a>
      <ul>
        <li>
          <a href="hh84972140s.html#8">Al sereniss.mo Prencipe
          Mauritio cardinale di Savoia</a>
        </li>
        <li>
          <a href="hh84972140s.html#180">Informatione d'alcune
          imagini miracolose della madre di Dio, et d'altre cose
          attinenti alla sudetta religione della gloriosa Vergine
          Maria del Carmine</a>
        </li>
        <li>
          <a href="hh84972140s.html#340">Catalogo de Generali
          Latini della religione Carmelitana</a>
        </li>
        <li>
          <a href="hh84972140s.html#384">Tavola delle cose
          notabili contenute nell'antecedente relatione</a>
        </li>
        <li>
          <a href="hh84972140s.html#402">Errori da correggere</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
