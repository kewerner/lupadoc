<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="esor113270s.html#6">Memorie istoriche massimamente
      sacre della città di Sora</a>
      <ul>
        <li>
          <a href="esor113270s.html#8">Catalogo</a>
        </li>
        <li>
          <a href="esor113270s.html#14">All'illustrissimo, ed
          Eccellentissimo Signore D. Antonio Buoncompagni Ludovisi
          Duca di Sora</a>
        </li>
        <li>
          <a href="esor113270s.html#20">A qualunque Lettore</a>
        </li>
        <li>
          <a href="esor113270s.html#26">Parte prima</a>
          <ul>
            <li>
              <a href="esor113270s.html#26">I. Dal principio di
              Sora all'anno primo di Cristo</a>
            </li>
            <li>
              <a href="esor113270s.html#33">II. Dall'anno I. di
              Cristo al 200</a>
            </li>
            <li>
              <a href="esor113270s.html#42">III. Dall'anno 200.
              di Cristo al 1000</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="esor113270s.html#67">Parte seconda</a>
          <ul>
            <li>
              <a href="esor113270s.html#67">I. Dall'anno 1000 di
              Cristo al 1031&#160;</a>
            </li>
            <li>
              <a href="esor113270s.html#85">II. Dall'anno 1031 di
              Cristo al 1100&#160;</a>
            </li>
            <li>
              <a href="esor113270s.html#100">III. Dell'[sic!]
              anno 1100 di Cristo al 1200&#160;</a>
            </li>
            <li>
              <a href="esor113270s.html#109">IV. Dall'anno 1200
              di Cristo al 1300&#160;</a>
            </li>
            <li>
              <a href="esor113270s.html#125">V. Dall'anno 1300 di
              Cristo al 1400&#160;</a>
            </li>
            <li>
              <a href="esor113270s.html#132">VI. Dall'anno 1400
              di Cristo al 1500&#160;</a>
            </li>
            <li>
              <a href="esor113270s.html#145">VII. Dall'anno 1500
              di Cristo al 1572&#160;</a>
            </li>
            <li>
              <a href="esor113270s.html#160">VIII. Dall'anno 1572
              di Cristo al 1600</a>
            </li>
            <li>
              <a href="esor113270s.html#196">IX. Dall'anno 1600
              di Cristo al 1607</a>
            </li>
            <li>
              <a href="esor113270s.html#241">X. Dall'anno 1607 di
              Cristo al 1644</a>
            </li>
            <li>
              <a href="esor113270s.html#259">XI. Dall'anno 1644
              di Cristo al 1700</a>
            </li>
            <li>
              <a href="esor113270s.html#287">XII. Dall'anno 1700
              di Cristo al 1721</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
