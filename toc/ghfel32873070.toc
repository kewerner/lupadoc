<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghfel32873070s.html#6">Les plans et les
      descriptions de deux des plus belles maisons de Campagne de
      Pline le Consul</a>
      <ul>
        <li>
          <a href="ghfel32873070s.html#8">Avertissement</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#12">Le Laurentin</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#73">La maison de
          Toscane</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#135">Dissertation touchant
          l'architecture antique et l'architecture gotique</a>
          <ul>
            <li>
              <a href="ghfel32873070s.html#147">Tables des
              matieres</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghfel32873070s.html#164">L'idée du peintre
      parfait</a>
      <ul>
        <li>
          <a href="ghfel32873070s.html#174">I. Du genie</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#176">II. Qu'il est bon de
          se servir des études d'autruy sans aucun serupule</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#179">III. De la nature</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#181">IV. En quel sens on
          peut dire que l'Art est au dessins de la nature</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#182">V. De l'Antique</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#184">VI. Du grand Goût</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#185">VII. De l'Essence de
          la Peinture</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#186">VIII. Si la fidélité
          de l'Histoire est de l'Essence de la Peinture</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#189">IX. Des Idées
          imparfait de la Peinture</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#190">X. Comment les restes
          de l'Idee imparfait de la Peinture se sont conservez
          [...]</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#195">XI. Composition.
          Premiére Partie de la Peinture</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#196">XII. Dessein. Seconde
          Partie de la Peinture</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#196">XIII. Des
          Attitudes</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#197">XIV. Des
          Expressions</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#197">XV. Des
          Extrémitez</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#198">XVI. Des
          Draperies</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#200">XVII. Du Paisage</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#201">XVIII. De la
          Perspective</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#202">XIX. Coloris.
          Troisiéme Partie de la Peinture</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#203">XX. De l'Accord des
          Coleurs</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#204">XXI. Du Pinceau</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#205">XXII. Des
          Licences</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#205">XXIII. De quelle
          autorité les Peintres ont réprésenté sous des Figures
          humaines [...]</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#210">XXIV. Des Figures
          nues, et où l'on peut s'en servir</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#212">XXV. De la Grace</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#214">XXVI. Des
          Desseins</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#222">XXVII. De l'utilité
          des Estampes, et de leur usage</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#235">XXVIII. De la
          Connoissance des Tableaux</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#245">Du Gout et de da
          diversité, par rapport aux différentes Nations</a>
        </li>
        <li>
          <a href="ghfel32873070s.html#251">Catalogue de livres
          nouveaux</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
