<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy540501021s.html#2">dy540501021</a>
    </li>
    <li>
      <a href="dy540501021s.html#5">[Vorsatz]</a>
    </li>
    <li>
      <a href="dy540501021s.html#8">[Widmung]</a>
    </li>
    <li>
      <a href="dy540501021s.html#12">[Textband]</a>
    </li>
    <li>
      <a href="dy540501021s.html#12">[Schmutztitel] DIE
      SIXTINISCHE KAPELLE</a>
    </li>
    <li>
      <a href="dy540501021s.html#14">[Titelblatt] DIE
      SIXTINISCHE KAPELLE</a>
    </li>
    <li>
      <a href="dy540501021s.html#16">[Vorwort] Vorwort zum
      zweiten Bande</a>
    </li>
    <li>
      <a href="dy540501021s.html#23">[Inhaltsverzeichnis] Inhalt
      des zweiten Bandes</a>
    </li>
    <li>
      <a href="dy540501021s.html#32">Teil I DIE AUSMALUNG DER
      DECKE UNTER JULIUS II.</a>
    </li>
    <li>
      <a href="dy540501021s.html#34">Abschnitt I JULIUS II.
      POLITIK UND PERSÖNLICHER CHARAKTER</a>
    </li>
    <li>
      <a href="dy540501021s.html#36">Kapitel I Der Karneval des
      Jahres 1513 in Rom</a>
    </li>
    <li>
      <a href="dy540501021s.html#45">Kapitel II Die Neugründung
      des Kirchenstaates</a>
    </li>
    <li>
      <a href="dy540501021s.html#55">Kapitel III Italiens
      Befreiung von der Fremdherrschaft</a>
    </li>
    <li>
      <a href="dy540501021s.html#62">Kapitel IV Charakter und
      Persönlichkeit Julius II.</a>
    </li>
    <li>
      <a href="dy540501021s.html#74">Abschnitt II JULIUS II. UND
      DIE KUNST</a>
    </li>
    <li>
      <a href="dy540501021s.html#78">Kapitel I Der Papst als
      Büchersammler und sein Verhältnis zur Literatur</a>
    </li>
    <li>
      <a href="dy540501021s.html#83">Kapitel II Die Bautätigkeit
      Julius II. Bramante</a>
    </li>
    <li>
      <a href="dy540501021s.html#101">Kapitel III Guliano da
      Sangallo und Julius II. Bautätigkeit der Kardinäle</a>
    </li>
    <li>
      <a href="dy540501021s.html#110">Kapitel IV Das Antiquarium
      Julius II.</a>
    </li>
    <li>
      <a href="dy540501021s.html#115">Kapitel V Die Entwicklung
      der Plastik unter Julius II.</a>
    </li>
    <li>
      <a href="dy540501021s.html#129">Kapitel VI Der Aufschwung
      der Freskomalerei unter Julius II. Die umbrische Schule</a>
    </li>
    <li>
      <a href="dy540501021s.html#136">Kapitel VII Die
      sienesischen Maler</a>
    </li>
    <li>
      <a href="dy540501021s.html#143">Kapitel VIII Raffael und
      die Stanza della Segnatura</a>
    </li>
    <li>
      <a href="dy540501021s.html#151">Kapitel IX Fresken der
      Stana d'Eliodoro</a>
    </li>
    <li>
      <a href="dy540501021s.html#166">Abschnitt III JULIUS II.
      UND MICHELANGELO. DIE ENTSTEHUNGSGESCHICHTE DER
      DECKENGEMÄLDE</a>
    </li>
    <li>
      <a href="dy540501021s.html#168">Kapitel I Die Anfänge
      Michelangelos in Rom. Das Juliusdenkmal. Das Zerwürfnis mit
      dem Papst</a>
    </li>
    <li>
      <a href="dy540501021s.html#183">Kapitel II Die Versöhnung.
      Die Bronzestatue Julius II. und ihr Schicksal</a>
    </li>
    <li>
      <a href="dy540501021s.html#189">Kapitel III Der neue
      Auftrag, die Sixtinadecke auszumalen. Vorbereitungen,
      Gehilfen und Gerüste</a>
    </li>
    <li>
      <a href="dy540501021s.html#203">Kapitel IV Beginn und
      Fortgang der Arbeit</a>
    </li>
    <li>
      <a href="dy540501021s.html#215">Kapitel V Enthüllung der
      fertigen Fresken am 14. August 1511. Das letzte Arbeitsjahr.
      Zweite Enthüllungsfeier am 31. Oktober 1512</a>
    </li>
    <li>
      <a href="dy540501021s.html#232">Abschnitt IV PLAN DES
      WERKES, KRITIK DER QUELLEN, AUSFÜHRUNG UND ENTWICKLUNGSGANG
      DER ARBEIT</a>
    </li>
    <li>
      <a href="dy540501021s.html#234">Kapitel I Erste Pläne und
      Entwürfe</a>
    </li>
    <li>
      <a href="dy540501021s.html#241">Kapitel II Beschreibung
      von Condivi und Vasari und ihre Ergänzung</a>
    </li>
    <li>
      <a href="dy540501021s.html#254">Kapitel III
      Entstehungsperioden der Deckenmalerei</a>
    </li>
    <li>
      <a href="dy540501021s.html#264">Abschnitt V DER MENSCH ALS
      DEKORATIVES ELEMENT DER MALEREI</a>
    </li>
    <li>
      <a href="dy540501021s.html#266">Kapitel I Michelangelos
      Verhältnis zur Antike</a>
    </li>
    <li>
      <a href="dy540501021s.html#276">Kapitel II Die
      Atlanten</a>
    </li>
    <li>
      <a href="dy540501021s.html#296">Kapitel III Die
      Bronzemedaillons</a>
    </li>
    <li>
      <a href="dy540501021s.html#308">Kapitel IV Die
      Karyatidenkinder</a>
    </li>
    <li>
      <a href="dy540501021s.html#316">Kapitel V Die Knaben mit
      den Namenschildern der Propheten und Sibyllen</a>
    </li>
    <li>
      <a href="dy540501021s.html#319">Kapitel VI Die liegenden
      Erd- und Flussgötter</a>
    </li>
    <li>
      <a href="dy540501021s.html#322">Abschnitt VI DIE
      HISTORIENBILDER</a>
    </li>
    <li>
      <a href="dy540501021s.html#324">Kapitel I Die vier
      Geschichtsbilder in den Ecken</a>
    </li>
    <li>
      <a href="dy540501021s.html#333">Kapitel II Die
      Mittelbilder. Die drei Historien von Noah</a>
    </li>
    <li>
      <a href="dy540501021s.html#355">Kapitel III Die
      Erschaffung des Menschen und sein Fall</a>
    </li>
    <li>
      <a href="dy540501021s.html#374">Kapitel IV Gottvater und
      die Erschaffung der Welt</a>
    </li>
    <li>
      <a href="dy540501021s.html#382">Abschnitt VII PROPHETEN
      UND SIBYLLEN</a>
    </li>
    <li>
      <a href="dy540501021s.html#384">Kapitel I Wertschätzung
      der Sehergestalten Michelangelos. Quellen seiner Kunst. Die
      vier ersten Propheten</a>
    </li>
    <li>
      <a href="dy540501021s.html#408">Kapitel II Die drei
      letzten Propheten. Der Prophet Daniel</a>
    </li>
    <li>
      <a href="dy540501021s.html#425">Kapitel III Die
      Sibyllen</a>
    </li>
    <li>
      <a href="dy540501021s.html#464">Abschnitt VIII Die
      Vorfahren Christi</a>
    </li>
    <li>
      <a href="dy540501021s.html#466">Kapitel I Historische
      Grundlagen und künstlerische Überlieferung</a>
    </li>
    <li>
      <a href="dy540501021s.html#471">Kapitel II Die
      Kompositionen in den Stichkappen</a>
    </li>
    <li>
      <a href="dy540501021s.html#482">Kapitel III Die
      Kompositionen in den Lünetten der Eingangswand</a>
    </li>
    <li>
      <a href="dy540501021s.html#490">Kapitel IV Die
      Kompositionen in den Lünetten der Langwände</a>
    </li>
    <li>
      <a href="dy540501021s.html#499">Kapitel V Die Kompostionen
      in den Lünetten dert Altarwand</a>
    </li>
    <li>
      <a href="dy540501021s.html#504">Teil II DIE AUSMALUNGEN
      DER ALTARWAND MIT DEM JÜNGSTEN GERICHT UNTER PAUL III.</a>
    </li>
    <li>
      <a href="dy540501021s.html#506">Abschnitt I
      ENTSTEHUNGSGESCHICHTE</a>
    </li>
    <li>
      <a href="dy540501021s.html#508">Kapitel I Michelangelo und
      Tommaso Cavalieri</a>
    </li>
    <li>
      <a href="dy540501021s.html#525">Kapitel II Paul III. und
      Michelangelo</a>
    </li>
    <li>
      <a href="dy540501021s.html#537">Kapitel III Beginn und
      Fortgang der Arbeit. Äussere Erlebnisse. Michelangelo und
      Pietro Aretino</a>
    </li>
    <li>
      <a href="dy540501021s.html#548">Kapitel IV Michelangelo
      und Vittoria Colonna</a>
    </li>
    <li>
      <a href="dy540501021s.html#560">Kapitel V Letzte
      Arbeitsjahre und Enthüllung des Jüngsten Gerichtes am 31.
      Oktober 1541. Bewunderung der Zeitgenossen</a>
    </li>
    <li>
      <a href="dy540501021s.html#566">Kapitel VI Schicksale des
      Jüngsten Gerichtes. Kopien</a>
    </li>
    <li>
      <a href="dy540501021s.html#572">Abschnitt II BESCHREIBUNG
      UND DEUTUNG/</a>
    </li>
    <li>
      <a href="dy540501021s.html#574">Kapitel I Michelangelos
      Verhältnis zur künstlerischen Tradition des Weltgerichtes</a>
    </li>
    <li>
      <a href="dy540501021s.html#580">Kapitel II Das
      Christusideal Michelangelos. Die Madonna</a>
    </li>
    <li>
      <a href="dy540501021s.html#585">Kapitel III Beschreibung
      des Jüngsten Gerichtes. Das Paradies</a>
    </li>
    <li>
      <a href="dy540501021s.html#598">Kapitel IV Der mittlere
      und untere Teil der Komposition</a>
    </li>
    <li>
      <a href="dy540501021s.html#605">Kapitel V Der Dialog des
      Gilio da Fabriano</a>
    </li>
    <li>
      <a href="dy540501021s.html#610">Kapitel VI Michelangelos
      Verhältnis zu Dante</a>
    </li>
    <li>
      <a href="dy540501021s.html#620">Kapitel VII Dantes
      Einfluss auf das Jüngste Gericht</a>
    </li>
    <li>
      <a href="dy540501021s.html#638">ANHANG ZUM ZWEITEN
      BANDE</a>
    </li>
    <li>
      <a href="dy540501021s.html#640">Anhang I Die Zeichnungen
      Michelangelos zu den Sixtinafresken. Kritischer Katalog</a>
    </li>
    <li>
      <a href="dy540501021s.html#736">Anhang II Dokumente,
      herausgegeben von Heinrich Pogatscher</a>
    </li>
    <li>
      <a href="dy540501021s.html#844">REGISTER</a>
    </li>
    <li>
      <a href="dy540501021s.html#844">I. Personenverzeichnis</a>
    </li>
    <li>
      <a href="dy540501021s.html#849">II. Ortsverzeichnis</a>
    </li>
    <li>
      <a href="dy540501021s.html#850">III. Verzeichnis der
      erwähnten Monumente und Kunstschätze</a>
    </li>
    <li>
      <a href="dy540501021s.html#862">BEMERKUNGEN ZU DEN
      ABBILDUNGEN</a>
    </li>
    <li>
      <a href="dy540501021s.html#863">BERICHTIGUNGEN</a>
    </li>
    <li>
      <a href="dy540501021s.html#864">[Kolophon]</a>
    </li>
    <li>
      <a href="dy540501021s.html#865">[Hinterer Vorsatz]</a>
    </li>
  </ul>
  <hr />
</body>
</html>
