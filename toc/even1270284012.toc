<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="even1270284012s.html#8">Il Ritratto Di
      Venezia</a>
      <ul>
        <li>
          <a href="even1270284012s.html#10">[Parte prima]</a>
          <ul>
            <li>
              <a href="even1270284012s.html#10">Serenissimo
              Prencipe</a>
            </li>
            <li>
              <a href="even1270284012s.html#14">Avisi al
              Lettore</a>
            </li>
            <li>
              <a href="even1270284012s.html#16">Abbozzo del
              ritratto di Venezia</a>
            </li>
            <li>
              <a href="even1270284012s.html#30">Sestiero di San
              Marco</a>
            </li>
            <li>
              <a href="even1270284012s.html#240">Sestiero di
              Canaregio</a>
            </li>
            <li>
              <a href="even1270284012s.html#318">Sestiere della
              Croce</a>
            </li>
            <li>
              <a href="even1270284012s.html#342">Sestiero di
              San Polo</a>
            </li>
            <li>
              <a href="even1270284012s.html#392">Sestiero di
              Dorsoduro</a>
            </li>
            <li>
              <a href="even1270284012s.html#484">Isole
              circonvicine a Venezia</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even1270284012s.html#488">Parte seconda del
          ritratto di Venezia</a>
          <ul>
            <li>
              <a href=
              "even1270284012s.html#490">Introduzione</a>
            </li>
            <li>
              <a href="even1270284012s.html#494">Governo della
              Replulica</a>
            </li>
            <li>
              <a href="even1270284012s.html#523">Magistrati di
              Rialto</a>
            </li>
            <li>
              <a href="even1270284012s.html#533">Catalogo delli
              Dogi, o' Prencipi di Venezia</a>
            </li>
            <li>
              <a href="even1270284012s.html#552">Nota d'alcuni
              scrittori</a>
            </li>
            <li>
              <a href="even1270284012s.html#553">Fabriche
              publiche di Venezia</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even1270284012s.html#634">Indice della prima
          parte</a>
        </li>
        <li>
          <a href="even1270284012s.html#641">Indice della
          seconda parte</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
