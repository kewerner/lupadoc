<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katpstr536951202s.html#12">Pièces de choix de la
      collection Comte Grégoire Stroganoff à Rome II</a>
      <ul>
        <li>
          <a href="katpstr536951202s.html#12">Seconde partie:
          Moyen-Age - Renaissance - Epoque moderne</a>
          <ul>
            <li>
              <a href="katpstr536951202s.html#14">La
              peinture</a>
            </li>
            <li>
              <a href="katpstr536951202s.html#120">La
              sculpture</a>
            </li>
            <li>
              <a href="katpstr536951202s.html#140">Les
              bronzes</a>
            </li>
            <li>
              <a href="katpstr536951202s.html#152">Les
              sculptures sur bois</a>
            </li>
            <li>
              <a href="katpstr536951202s.html#166">Les
              ivoires</a>
            </li>
            <li>
              <a href="katpstr536951202s.html#188">Les
              Faiences</a>
            </li>
            <li>
              <a href=
              "katpstr536951202s.html#200">L'orfévrerie</a>
            </li>
            <li>
              <a href="katpstr536951202s.html#230">Art
              chinois</a>
            </li>
            <li>
              <a href="katpstr536951202s.html#234">Tables</a>
            </li>
            <li>
              <a href="katpstr536951202s.html#246">Planches</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
