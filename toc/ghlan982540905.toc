<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlan982540905s.html#6">Storia pittorica della
      Italia; tomo V. Ove si descrivono le scuole bolognese e
      ferrarese, e quelle di Genova e del Piemonte</a>
      <ul>
        <li>
          <a href="ghlan982540905s.html#10">Compartimento di
          questo tomo quinto</a>
        </li>
        <li>
          <a href="ghlan982540905s.html#12">Della storia
          pittorica della Italia superiore</a>
          <ul>
            <li>
              <a href="ghlan982540905s.html#12">III. Scuola
              bolognese</a>
              <ul>
                <li>
                  <a href="ghlan982540905s.html#16">I. Gli
                  antichi&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540905s.html#49">II. Maniere
                  diverse dal Francia fino a' Caracci&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540905s.html#83">III. I
                  Caracci, gli allievi loro, e i lor successori
                  fino al Cignani&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540905s.html#178">IV. Il
                  Pasinelli e più di esso il Cignani fan
                  cangiamento nella pittura bolognese. Accademia
                  clementina, e socj di essa&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ghlan982540905s.html#230">IV. Scuola
              ferrarese</a>
              <ul>
                <li>
                  <a href="ghlan982540905s.html#230">I. Gli
                  antichi&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540905s.html#247">II. I
                  Ferraresi dal tempo di Alfonso I fino ad Alfonso
                  II, ultimo degli estensi in Ferrara, emulando i
                  migliori stili d'Italia</a>
                </li>
                <li>
                  <a href="ghlan982540905s.html#269">III. I
                  Ferraresi derivano varj stili dalla scuola di
                  Bologna. Decadenza dell'arte, e fondazione di
                  un'Accademia per sollevarla&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ghlan982540905s.html#295">V. Scuola
              genovese</a>
              <ul>
                <li>
                  <a href="ghlan982540905s.html#295">I. Gli
                  antichi</a>
                </li>
                <li>
                  <a href="ghlan982540905s.html#303">II. Perino
                  e i seguaci suoi</a>
                </li>
                <li>
                  <a href="ghlan982540905s.html#322">III. La
                  pittura decaduta per poco tempo si rinvigorisce
                  per opera del Paggi e di alcuni esteri</a>
                </li>
                <li>
                  <a href="ghlan982540905s.html#349">IV.
                  Succendo agli stili patri il Romano e il
                  Parmenese. Stabilimento di un'Accademia</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ghlan982540905s.html#368">VI. La pittura
              in Piemonte e nelle sue adjacenze</a>
              <ul>
                <li>
                  <a href="ghlan982540905s.html#368">I. Principj
                  dell'arte e progressi fino al secolo XVI.</a>
                </li>
                <li>
                  <a href="ghlan982540905s.html#384">II. Pittori
                  del secolo XVII e prima fondazione
                  dell'Accademia</a>
                </li>
                <li>
                  <a href="ghlan982540905s.html#398">III. Scuola
                  di Beaumont, e rinnovazione dell'Accademia</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
