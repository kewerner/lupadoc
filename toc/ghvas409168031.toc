<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghvas409168031s.html#8">Delle Vite de' più
      eccellenti Pittori Scultori e Architettori [...] Primo Volume
      della Terza Parte</a>
    </li>
    <li>
      <a href="ghvas409168031s.html#8">[Indici]</a>
      <ul>
        <li>
          <a href="ghvas409168031s.html#10">Tavola delle vite
          de gli artefici [...]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#12">Tavola de' luoghi
          dove sono l'opere descritte [...]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#27">Tavola de' ritratti
          [...]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#29">Tavola delle cose
          più notabili [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvas409168031s.html#43">Proemio</a>
    </li>
    <li>
      <a href="ghvas409168031s.html#48">[Vite]</a>
      <ul>
        <li>
          <a href="ghvas409168031s.html#48">Leonardo da
          Vinci</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#59">Giorgione [Giorgio
          da Castelfranco]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#63">Correggio [Antonio
          Allegri]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#67">Piero di Cosimo</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#74">Donato Bramante</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#82">Fra Carnevale
          [Bartolomeo Corradini]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#89">Mariotto
          Albertinelli</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#94">Raffaellino del
          Garbo</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#98">Pietro
          Torrigiano</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#102">Giuliano da
          Sangallo e Antonio da Sangallo il Vecchio [Giuliano e
          Antonio Giamberti]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#111">Raffaello
          Sanzio</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#137">Guglielmo di
          Pietro de Marcillat [Guglielmo da Marcilla; Guillaume de
          Marseille]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#143">Cronaca [Simone di
          Tomaso del Pollaiuolo]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#150">Domenico Puligo
          [Domenico Ubaldini]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#154">Andrea di Piero
          Ferrucci [Andrea da Fiesole] [ed altri artisti
          fiesolani]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#158">Vincenzo di
          Benedetto di Chele Tamagni [Vincenzo da San Gimignano] e
          Timoteo Viti [Timoteo da Urbino]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#163">Andrea Sansovino
          [Andrea dal Monte San Savino]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#170">Benedetto da
          Rovezzano</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#173">Baccio da
          Montelupo [Bartolomeo Sinibaldi] e Raffaello da Montelupo
          [Raffaele di Bartolomeo Sinibaldi]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#177">Lorenzo di
          Credi</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#180">Lorenzo di
          Ludovico di Guglielmo Lotti [Lorenzetto] e Boccaccio
          Boccaccino</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#184">Baldassarre
          Peruzzi</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#192">Giovan Francesco
          Penni [Fattore] e Pellegrino Aretusi [Pellegrino da
          Modena]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#196">Andrea del Sarto
          [Andrea d'Agnolo]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#218">Properzia Rossi
          [Properzia de' Rossi]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#222">Alfonso Lombardi
          [Alfonso Cittadella], Michelangelo Anselmi [Scalabrino],
          Girolamo Santacroce, Dosso e Battista Dossi [Giovanni e
          Battista de' Luteri]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#230">Bernardino Licinio
          [ed altri artisti friulani]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#237">Giovannantonio
          Sogliani [Giovanni Antonio Sogliani]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#242">Girolamo Pennacchi
          da Treviso il Giovane</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#244">Polidoro Caldara
          [Polidoro da Caravaggio] e Maturino da Firenze</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#251">Rosso Fiorentino
          [Giovanni Battista Rosso]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#260">Bartolomeo
          Ramenghi il Vecchio [Bagnacavallo] [ed altri artisti
          emiliani]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#265">Franciabigio
          [Francesco Bigi]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#270">Morto da Feltre
          [Pietro Luzzi; Antonio da Feltro] e Andrea Feltrini
          [Andrea del Fornaio; Andrea di Cosimo]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#275">Marco Cardisco
          [Marco Calabrese]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#277">Parmigianino
          [Girolamo Francesco Mazzola]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#286">Jacopo Palma il
          Vecchio [Jacopo d'Antonio Negreti] e Lorenzo Lotto</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#291">Fra' Giocondo
          [Giovanni Giocondo da Verona] e Liberale da Verona [ed
          altri artisti veronesi]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#306">Francesco
          Bonsignori [Francesco Monsignori]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#314">Giovanni Maria
          Falconetto [Gian Maria Falconetti]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#318">Francesco e
          Girolamo dai Libri [Francesco Vecchio; Hieronymus a
          Libris]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#323">Francesco
          Granacci</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#326">Bartolomeo
          Baglioni [Baccio d'Agnolo]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#332">Valerio Belli
          [Valerio Vicentino], Giovanni Desiderio Bernardi
          [Giovanni da Castel Bolognese] e Matteo dal Nassaro [ed
          altri intagliatori di camei e gemme]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#341">Marcantonio
          Raimondi [Francia] [ed altri incisori di stampe]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#360">Antonio da
          Sangallo il Giovane [Antonio Cordiani]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#371">Giulio Romano</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#387">Sebastiano del
          Piombo [Sebastiano Luciani]</a>
        </li>
        <li>
          <a href="ghvas409168031s.html#395">Perino del Vaga
          [Pietro Buonaccorsi]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
