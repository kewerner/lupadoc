<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebas1723750s.html#8">Notizie intorno alla vita e
      alle opere de’ pittori, scultori e intagliatori della città
      di Bassano</a>
      <ul>
        <li>
          <a href="ebas1723750s.html#10">Agli ornatissimi e
          nobilissimi Accademici anistamici di Belluno</a>
        </li>
        <li>
          <a href="ebas1723750s.html#12">Tavola de' pittori,
          scultori, e intagliatori</a>
        </li>
        <li>
          <a href="ebas1723750s.html#16">Notizie sopra la pittura
          bassanese</a>
        </li>
        <li>
          <a href="ebas1723750s.html#293">Notizie sopra gli
          scultori, e intagliatori bassanesi</a>
        </li>
        <li>
          <a href="ebas1723750s.html#328">Tavola I. Delle pitture
          e sculture che sono nelle chiese di Bassano</a>
        </li>
        <li>
          <a href="ebas1723750s.html#335">Tavola II. Delle
          pitture e sculture, che sono nelle chiese delle città, e
          de' villaggi accenati nell'Opera</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
