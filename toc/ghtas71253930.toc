<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghtas71253930s.html#6">Vite De’ Pittori, Scultori
      E Architetti Bergamaschi</a>
      <ul>
        <li>
          <a href="ghtas71253930s.html#6">Tomo I.</a>
          <ul>
            <li>
              <a href="ghtas71253930s.html#8">Prefazione</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#34">Paxino, o Pecino
              de Nova pittore es isnardo comenduno suo scolare</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#39">Pietro de Nova
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#41">Giovanni
              Campilione scultore ad architetto</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#45">Andreolo de'
              Bianchi</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#47">Uguetto da
              Vertova</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#49">Paxino di Volla
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#50">Bartolomeo Buono
              scultore ed architetto</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#55">Guglielmo
              architetto</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#59">Francesco, e
              Bartolomeo di Gandino</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#61">Giacomo de Balsamo
              miniatore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#62">Giorgio Guido
              Defendente, e Bernardo da San Piligrino&#160;</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#62">Giacomo de'
              Scanardi d'Averara</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#64">Bertolasio Moroni,
              Leonardo, Peccino, e Venturino suoi figliuoli, ed
              Antonio figliuolo di Venturino, ed Andrea della
              stessa famiglia</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#66">Giovanni
              Cariano</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#72">Andrea
              Previtali</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#77">Giovanni Giacomo
              Gavasio</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#78">Agostino
              Gavasio</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#78">Agostino
              Facheris</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#79">Agostino
              Caversegno</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#80">Giacomo detto
              Iacopino de' Scipioni d'Averara</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#83">Antonio
              Boselli</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#85">Giovanni Battista
              Averaria</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#89">Francesco Rizo</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#90">Girolamo da Santa
              Croce</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#92">Giovanni
              Galizi</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#92">Fra Damiano
              Domenicano</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#97">Giovanni Francesco
              Capodiferro, Zinino suo filgiuolo, e Pietro suo
              fratello maetri di Tarsia</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#100">Alfonso de'
              Codiferri pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#101">Giovanni, Iacomo,
              Andrea, ed Alessandro Belli scultori</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#106">Giuseppe Belli
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#107">Andrea Ziliolo
              architetto</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#107">Pietro de'
              Maffeis scultore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#108">Giovanni
              Francesco Zabello</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#109">Polidoro Caldara
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#118">Bernardo Zenale
              pittore, ed architetto</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#124">Iacopo Palma il
              Vecchio pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#139">Antonio Palma</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#140">Giacomo Palma il
              Giovane pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#149">Lorenzo Lotto
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#164">Pietro Isabello
              architetto, Marcantio, e Leonardo suoi figliuoli</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#169">Giampaolo
              Lolmo</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#173">Filippo Zanchi
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#173">Francesco Zanchi
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#174">Giambattista
              Guarinoni pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#176">Girolamo Coleoni
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#183">Bartolomeo,
              Niccolino, Giuliano, e Cabrino de' Cabrini
              pittori</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#185">Troilo, e Valerio
              Lupi pittori</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#187">Giambattista
              Castello detto il Bergamsco, pittore, scultore, ed
              architetto</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#193">Granello, E
              Fabrizio figliuoli di Givanno Battista Castello
              pittori</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#195">Giambattista
              Moroni pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#205">Giovanni ed
              Antonio Moroni</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#206">Francesco Terzi
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#214">Fancesco Cozzi
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#216">Cristoforo
              Baschenis,ed altri pittori della stessa
              famiglia&#160;</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#220">Giacomo Anselmi
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#221">Pietro Ronzelli
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#222">Fabio Ronzelli
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#223">Francesco Zucco
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#226">Giovanni Paolo
              Cavagna</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#243">Francesco Cavagna
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#244">Girolamo
              Grifoni</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#245">Enea Salmeggia,
              detto il Talpino pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#256">Francesco
              Salmeggia pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#257">Chiara Salmeggia
              pittrice</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#259">Giovanni Giacomo
              Assonica pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#259">Marcantonio
              Cesare pittore, e Giuseppe suo filgiuolo</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#261">Giambattista
              Viola pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#262">Andrea Zambelli
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#262">Domenico
              Carpinone pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#266">Prete Evaristo
              Baschenis pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#270">Prete Giacomo
              Cotta, e Giovanni Battista Azzanelli suo scolare</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#273">Carlo Ceresa
              pittore</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#280">Giuseppe e
              Antonio Ceresi pittori</a>
            </li>
            <li>
              <a href="ghtas71253930s.html#282">Indice</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghtas71253930s.html#286">Tomo II.</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#288">Cavalier Cosimo
          Fansago scultore ed architetto</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#305">Carlo Fansago
          scultore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#306">Domenico Ghislandi
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#308">Pietro Paolo Raggi
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#311">Marzial Carpinoni
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#312">Giovanni Giuseppe
          Picini scultore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#314">Alessandro Lanfranchi
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#317">Cristoforo Tasca
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#319">Antonio Zifrondi
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#326">Prete Giuseppe
          Roncelli</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#342">Fra Vittore Ghislandi
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#359">Marco Olmo</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#361">Giovanni Carobbio
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#363">Giambattista Caniana
          scultore ed architetto</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#367">Bartolomeo Nazari
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#382">Giovanni Sanz
          scultore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#387">Antonio Perovani
          scultore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#388">Giovanni Raggi
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#392">Bernardo Fedrighini
          architetto</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#395">Enrico Alberici
          pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#400">Indice</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#402">Supplemento</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#410">Giovanni Battista
          Azzola pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#414">Benedetto Adolfi, e
          Giacomo, Ciro, e Nicola suoi figli, pittori</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#416">Antonio Mara detto lo
          Scarpetta pittore</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#417">Achille, Marco, e
          Filippo Alessandri</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#419">Alessandro Benedetto
          e Pier Giuseppe Fratelli Possenti scultori</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#423">Francesco Dagiu'
          detto il Capella</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#425">Conte Nicolino de'
          Conti di Calepio</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#427">Jacopo Quarenghi
          architetto</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#440">L'editore a' cortesi
          lettori</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#444">Trattato scientifico
          di fortificazione sopra la storia particolare di
          Bergamo</a>
        </li>
        <li>
          <a href="ghtas71253930s.html#520">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
