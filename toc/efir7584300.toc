<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efir7584300s.html#8">La piazza del granduca di
      Firenze co’ suoi monumenti</a>
    </li>
    <li>
      <a href="efir7584300s.html#10">[Text]</a>
      <ul>
        <li>
          <a href="efir7584300s.html#10">I. Veduta della Piazza
          del Granduca dall'angolo della Posta&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#12">II. Veduta del Portico
          degli Ufficj dalla Ringhiera sull'Arno&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#14">III. Veduta della Piazza
          del Granduca dentro la Loggia de'Lanzi&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#16">IV. La Giudetta di
          Donatello e i Termini di Palazzo Vecchio&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#18">V. La Statua del David
          ed Ercole e Caco innanzi al Palazzo Vecchio</a>
        </li>
        <li>
          <a href="efir7584300s.html#22">VI. Il Leone della
          Ringhiera del Palazzo e gli altri della Loggia
          de'Lanzi</a>
        </li>
        <li>
          <a href="efir7584300s.html#24">VII; VIII. Le sei Statue
          antiche sotto la Loggia de'Lanzi&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#26">IX. Perseo</a>
        </li>
        <li>
          <a href="efir7584300s.html#28">X. Gruppo delle
          Sabine</a>
        </li>
        <li>
          <a href="efir7584300s.html#30">XI. Bassi rilievi del
          Cellini e di Giambologna</a>
        </li>
        <li>
          <a href="efir7584300s.html#32">XII. La Statua equestre
          di Cosimo I.&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#34">XIII; XIV; XV. Bassi
          rilievi intorno la Statua equestre di Cosimo I.&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#36">XVI.; XVII.; XVIII; XIX;
          XX. La Fontana della Piazza del Granduca</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="efir7584300s.html#38">Indice</a>
    </li>
    <li>
      <a href="efir7584300s.html#40">[Tavole]</a>
      <ul>
        <li>
          <a href="efir7584300s.html#40">I. Veduta della Piazza
          del Granduca di Firenze ▣&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#42">II. Veduta dei Regij
          Uffizi di Firenze presa da Lungo l'Arno ▣&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#44">III. Piazza del Granduca
          presa sotto la Loggia de'Lanzi ▣&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#46">IV. [La Giudetta di
          Donatello e i Termini di Palazzo Vecchio] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#48">V. [La Stauta del David
          ed Ercole e Caco innanzi al Palazzo Vecchio] ▣&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#50">VI. [Il Leone della
          Ringhiera del Palazzo Vecchio] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#52">VII. e VIII. [Le sei
          Statue antiche sotto la Loggia de'Lanzi] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#56">IX. [Perseo] ▣&#160;</a>
        </li>
        <li>
          <a href="efir7584300s.html#58">X. [Gruppo delle Sabine]
          ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#60">XI. [Bassi rilievi
          intorno del Cellini di Giambologna] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#62">XII. [La Statue equestre
          di Cosimo I.] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#64">XIII. [Bassi rilievi
          intorno la Statua equestre di Cosimo I.] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#66">XIV. [Bassi rilievi
          intorno la Statua equestre di Cosimo I.] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#68">XV. [Bassi rilievi
          intorno la Statua equestre di Cosimo I.] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#70">XVI. [La Fontana della
          Piazza del Granduca] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#72">XVII. [La Fontana della
          Piazza del Granduca] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#74">XVIII. [La Fontana della
          Piazza del Granduca] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#76">XIX. [La Fontana della
          Piazza del Granduca] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#78">XX. [La Fontana della
          Piazza del Granduca] ▣</a>
        </li>
        <li>
          <a href="efir7584300s.html#80">XXI. Veduta della Piazza
          del Granduca di Firenze come era nel Secolo XVI. ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
