<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="gd4633310s.html#8">Ritratti Di Alcuni Celebri
      Pittori Del Secolo XVII</a>
      <ul>
        <li>
          <a href="gd4633310s.html#10">Illustrissimo Signore</a>
        </li>
        <li>
          <a href="gd4633310s.html#13">Fausto Amidei a chi
          legge</a>
        </li>
        <li>
          <a href="gd4633310s.html#18">Vita del Cavalier Ottavio
          Lioni</a>
        </li>
        <li>
          <a href="gd4633310s.html#24">Vita di Ludovico Lioni</a>
        </li>
        <li>
          <a href="gd4633310s.html#30">Vita di Tomasso Salini</a>
        </li>
        <li>
          <a href="gd4633310s.html#36">Vita di Cristoforo
          Roncalli&#160;</a>
        </li>
        <li>
          <a href="gd4633310s.html#46">Vita di Antonio
          Tempesta</a>
        </li>
        <li>
          <a href="gd4633310s.html#60">Vita di Marcello
          Provenzale&#160;</a>
        </li>
        <li>
          <a href="gd4633310s.html#66">Vita del Cavaliere Gioseppe
          Cesari d'Arpino</a>
        </li>
        <li>
          <a href="gd4633310s.html#84">Vita di Simone Vovet</a>
        </li>
        <li>
          <a href="gd4633310s.html#96">Vita del Cavaliere Gio.
          Baglione</a>
        </li>
        <li>
          <a href="gd4633310s.html#110">Vita di Francesco Barbieri
          detto il Guercino da Cento</a>
        </li>
        <li>
          <a href="gd4633310s.html#160">Vita del Cavaliere Gio.
          Lorenzo Bernino</a>
        </li>
        <li>
          <a href="gd4633310s.html#186">Vita di Carlo Maratti</a>
        </li>
        <li>
          <a href="gd4633310s.html#292">Dafne trasformata in lauro
          pittura del Signor Carlo Maratti</a>
          <ul>
            <li>
              <a href="gd4633310s.html#294">Illustrissimo
              Signore</a>
            </li>
            <li>
              <a href="gd4633310s.html#296">Dafne trasformata in
              lauro</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
