<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg45043803s.html#8">Roma nell'anno
      MDCCCXXXVIII</a>
      <ul>
        <li>
          <a href="dg45043803s.html#10">Prefazione</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45043803s.html#20">Parte prima moderna</a>
      <ul>
        <li>
          <a href="dg45043803s.html#20">I. Delle Basiliche, delle
          Chiese, ed altri luoghi sacri</a>
        </li>
        <li>
          <a href="dg45043803s.html#46">›Sant'Adriano al Foro
          Romano‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#51">Sant'Agata detta alla
          Suburra ›Sant'Agata dei Goti‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#56">›Sant'Agata‹ in
          Trastevere</a>
        </li>
        <li>
          <a href="dg45043803s.html#57">Sant'Agnese in Piazza
          Navona ›Sant'Agnese in Agone‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#62">Sant'Agnese sulla Via
          Nomentana ›Sant'Agnese fuori le Mura‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#70">›Sant'Agostino‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#78">Santi Alessio, e
          Bonifacio ›Sant'Alessio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#86">›Santi Ambrogio e Carlo
          al Corso‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#91">›Sant'Ambrogio della
          Massima‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#92">›Sant'Anastasia‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#96">›Sant'Andrea delle
          Fratte‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#99">Sant'Andrea in Laterano
          ›Santi Andrea e Bartolomeo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#100">Sant'Andrea a
          Montecavallo ›Sant'Andrea al Quirinale‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#101">Sant'Andrea a Ponte
          Molle ›Oratorio di Sant'Andrea (via Flaminia)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#102">Sant'Andrea fuori di
          Porta del Popolo ›Sant'Andrea del Vignola‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#103">Sant'Andrea in
          Portogallo ›Santa Maria della Neve‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#104">Sant'Andrea degli
          Scozzesi ›Sant'Andrea‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#105">›Sant'Andrea della
          Valle‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#112">›Sant'Andrea in
          Vincis‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#113">Sant'Angelo in Borgo
          ›Sant'Angelo al Corridoio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#113">Sant'Angelo Custode
          ›Santi Angeli Custodi al Tritone‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#114">›Sant'Angelo in
          Pescheria‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#117">›Sant'Aniano‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#117">Sant'Anna detta De'
          Calzettari ›Sant'Anna de Marmorata‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#117">Sant'Anna De' Funari
          ›Sant'Anna dei Falegnami‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#118">Sant'Anna alle Quattro
          Fontane ›Santi Gioacchino e Anna alle Quattro
          Fontane‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#119">Sant'Annunziata a'
          Pantani ›San Basilio al Foro di Augusto‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#119">Sant'Annunziata a Tor
          de' Specchi ›Santa Maria de Curte‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#120">Sant'Annunziata detta
          delle Turchine ›Santa Maria Annunziata delle
          Turchine‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#120">›Sant'Antonio Abate‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#122">›Sant'Antonio dei
          Portoghesi‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#125">›Sant'Apollinare‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#127">›Santi Apostoli‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#136">›Sant'Atanasio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#137">›Santa Balbina‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#138">›Bambino Gesù
          all'Esquilino (chiesa)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#139">›Santa Barbara dei
          Librari‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#141">›Santi Bartolomeo e
          Alessandro dei Bergamaschi‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#142">›San Bartolomeo
          all'Isola‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#147">›San Bartolomeo dei
          Vaccinari‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#147">›San Basilio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#147">›San Benedetto in
          Piscinula‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#148">San Bernardino a' Monti
          ›San Bernardino da Siena‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#149">›San Bernardo alle
          Terme‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#151">San Biagio de'
          Materassari ›Santi Cecilia e Biagio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#151">›San Biagio della
          Pagnotta‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#153">›Santa Bibiana‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#156">›Santa Bonosa
          (scomparsa)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#157">›Santa Brigida‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#158">›San Bonaventura‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#159">›San Caio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#159">›San Callisto‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#160">›San Carlo ai
          Catinari‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#166">›San Carlo alle Quattro
          Fontane‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#167">›Santa Caterina dei
          Funari‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#169">›Santa Caterina della
          Rota‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#171">Santa Caterina da Siena
          al Quirinale ›Santa Caterina a Magnanapoli‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#172">›Santa Caterina da
          Siena‹ in Via Giulia</a>
        </li>
        <li>
          <a href="dg45043803s.html#174">›Santa Cecilia in
          Trastevere‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#185">›Santi Celso e
          Giuliano‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#186">›San Cesareo de
          Appia‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#188">›Santa Chiara al
          Quirinale‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#189">San Claudio, detto de'
          Borgognoni ›Santi Andrea e Claudio dei Borgognoni‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#189">›San Clemente‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#196">Concezione de'
          Cappuccini ›Santa Maria della Concezione‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#200">›Santi Cosma e Damiano
          de' Barbieri‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#201">›Santi Cosma e Damiano‹
          in Campo Vaccino</a>
        </li>
        <li>
          <a href="dg45043803s.html#208">Santi Cosma e Damiano in
          Trastevere ›San Cosimato‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#209">›Mausoleo di Santa
          Costanza‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#209">›San Crisogono‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#213">›Santa Croce in
          Gerusalemme‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#225">Santa Croce de' Lucchesi
          ›Santa Croce e San Bonaventura dei Lucchesi‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#226">Santa Croce alla Lungara
          ›Santa Croce delle Scalette‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#227">Santa Croce a Monte
          Mario</a>
        </li>
        <li>
          <a href="dg45043803s.html#227">›San Dioniso alle
          Quattro Fontane‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#228">›Santi Domenico e
          Sisto‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#229">›Santa Dorotea‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#230">›Sant'Egidio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#231">›Sant'Elena dei
          Credenzieri‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#231">›Sant'Eligio dei
          Ferrari‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#232">›Sant'Eligio degli
          Orefici‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#233">›Sant'Eusebio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#234">›Sant'Eustachio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#236">Santi faustino e Giovita
          ›Sant'Anna dei Bresciani‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#237">›San Filippo Neri ‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#238">›San Francesco a
          Ripa‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#240">›San Francesco di
          Paola‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#241">›Santa Galla
          (scomparsa)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#243">San Gallicano ›Santi
          Maria e Gallicano‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#243">›Santissimo Nome di
          Gesù‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#249">›Chiesa di Gesù e
          Maria‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#250">San Giacomo
          degl'incurabili ›San Giacomo in Augusta‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#251">San Giacomo alla Lungara
          ›San Giacomo in Settignano‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#252">›San Giacomo
          Scossacavalli‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#253">›San Giorgio in
          Velabro‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#255">›San Giovanni
          Evangelista in Ayno‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#255">›San Giovanni
          Decollato‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#256">San Giovanni di Dio ›San
          Giovanni Calibita‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#257">›San Giovanni dei
          Fiorentini‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#259">›San Giovanni Battista
          dei Genovesi‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#261">Basilica Lateranense
          ›San Giovanni in Laterano‹ ▣</a>
        </li>
        <li>
          <a href="dg45043803s.html#262">›San Giovanni in
          Laterano‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#283">›San Giovanni in
          Fonte‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#287">›Santi Giovanni e
          Paolo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#289">›Santi Giovanni
          Evangelista e Petronio dei Bolognesi‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#289">›San Giovanni della
          Pigna‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#290">›San Giovanni a Porta
          Latina‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#291">›Oratorio di San
          Giovanni in Oleo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#292">›San Girolamo della
          Carità‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#294">San Girolamo degli
          Schiavoni ›San Girolamo degli Illirici‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#295">›San Giuliano in
          Banchi‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#296">San Giuliano dei
          Fiamminghi ›San Giuliano Ospitaliere‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#296">›San Giuliano‹ a'
          Monti</a>
        </li>
        <li>
          <a href="dg45043803s.html#297">›San Giuseppe a Capo le
          Case‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#298">›San Giuseppe dei
          Falegnami‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#299">›San Pietro in
          Carcere‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#299">›San Giuseppe alla
          Lungara‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#300">San Gregorio al Monte
          Celio ›San Gregorio Magno‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#303">›San Gregorio dei
          Muratori‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#304">San Gregorio a Ponte
          Quattro Capi ›San Gregorio della Divina Pietà‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#304">›Santi Ildefonso e
          Tommaso di Villanova‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#304">›Sant'Ignazio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#310">›Sant'Isidoro‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#311">›Sant'Ivo dei
          Bretoni‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#311">›San Lazzaro‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#312">San Leonardo ›Santi
          Leonardo e Romualdo (scomparsa)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#312">›San Lorenzo in
          Damaso‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#316">›San Lorenzo in
          Fonte‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#317">›San Lorenzo fuori le
          Mura‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#322">›San Lorenzo in
          Lucina‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#324">›San Lorenzo in
          Miranda‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#325">San Lorenzo a Macel de'
          Corvi ›San Lorenzo de Ascesa ai Monti‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#326">›San Lorenzo in
          Panisperna‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#327">›San Lorenzo in
          Piscibus‹, ossia in Borgo</a>
        </li>
        <li>
          <a href="dg45043803s.html#329">›Santa Lucia alle
          Botteghe Oscure‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#330">Santa Lucia della
          Chiavica, o del Gonfalone ›Santa Lucia del Gonfalone‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#331">›Santa Lucia in
          Selci‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#332">›Santa Lucia della
          Tinta‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#332">›San Luigi dei
          Francesi‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#337">›San Macuto‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#337">›San Marcello al
          Corso‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#342">›San Marco‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#349">›Santa Margherita‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#350">›Santa Maria degli
          Angeli‹ alle Terme di Diocleziano</a>
        </li>
        <li>
          <a href="dg45043803s.html#358">Santa Maria degli Angeli
          alle Colonnaccie ›Santa Maria in Macello Martyrum‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#358">›Santa Maria in
          Cacabariis‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#359">›Santa Maria in
          Aquiro‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#362">›Santa Maria in
          Aracoeli‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#378">Santa Maria in Portico
          in Campitelli ›Santa Maria in Campitelli‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#381">›Santa Maria in Campo‹
          Carleo</a>
        </li>
        <li>
          <a href="dg45043803s.html#382">Santa Maria di Campo
          Marzio ›Santa Maria della Concezione in Campo Marzio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#383">›Santa Maria
          dell'Anima‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#386">›Santa Maria del
          Carmine‹ alle Tre Cannelle</a>
        </li>
        <li>
          <a href="dg45043803s.html#387">›Santa Maria della
          Consolazione‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#388">›Santa Maria in
          Cosmedin‹, o Scuola Greca</a>
        </li>
        <li>
          <a href="dg45043803s.html#390">Santa Maria di
          Costantinopoli ›Santa Maria d'Itria‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#391">›Santa Maria in
          Cappella‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#392">›Santa Maria in Domnica‹
          alla Navicella</a>
        </li>
        <li>
          <a href="dg45043803s.html#394">›Santa Maria
          Egiziaca‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#395">›Santa Maria delle
          Grazie alle Fornaci‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#396">›Santa Maria delle
          Grazie‹ a Porta Angelica</a>
        </li>
        <li>
          <a href="dg45043803s.html#397">›Santa Maria in
          Grottapinta (ex)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#397">›Santa Maria
          Imperatrice‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#398">Santa Maria Liberatrice
          ›Santa Maria Antiqua‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#399">›Santa Maria di Loreto‹
          de' fornari</a>
        </li>
        <li>
          <a href="dg45043803s.html#401">›Santa Maria
          Maggiore‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#403">Basilica Liberiana
          ›Santa Maria Maggiore‹ ▣</a>
        </li>
        <li>
          <a href="dg45043803s.html#428">›Santa Maria in San
          Marco‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#429">Santa Maria ad Martyres
          ›Pantheon‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#437">›Santa Maria sopra
          Minerva‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#456">›Santa Maria dei
          Miracoli‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#457">›Santa Maria in
          Monserrato‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#461">›Santa Maria in
          Monterone‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#462">›Santa Maria in
          Monticelli‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#464">›Santa Maria di
          Montesanto‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#466">Santa Maria de'Monti
          ›Chiesa della Madonna dei Monti‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#468">›Santa Maria
          dell'Orazione e Morte‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#469">›Santa Maria
          dell'Orto‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#471">›Santa Maria della
          Pace‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#476">Santa Maria delle Palme
          o delle Piante ›Chiesa del "Domine quo vadis?"‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#477">›Santa Maria del
          Pianto‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#478">›Santa Maria della
          Pietà‹ in Campo Santo</a>
        </li>
        <li>
          <a href="dg45043803s.html#479">›Santa Maria del
          Popolo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#495">›Santa Maria Portae
          Paradisi‹, o in Augusta</a>
        </li>
        <li>
          <a href="dg45043803s.html#495">›Santa Maria in
          Posterula‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#496">›Santa Maria del
          Priorato‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#497">›Santa Maria in
          Publicolis‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#498">Santa Maria della
          Purificazione in Banchi ›Santo Stefano de Pila‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#498">›Santa Maria della
          Purificazione‹ ai Monti</a>
        </li>
        <li>
          <a href="dg45043803s.html#498">›Santa Maria della
          Quercia‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#499">›Santa Maria Regina
          Coeli‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#500">›Santa Maria del
          Rosario‹ a Monte Mario</a>
        </li>
        <li>
          <a href="dg45043803s.html#502">›Santa Maria della
          Scala‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#504">Santa Maria della Porta
          del Cielo ›Santa Maria Scala Coeli‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#505">›Santa Maria dei Sette
          Dolori‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#506">›Santa Maria del
          Sole‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#506">›Santa Maria del
          Suffragio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#508">Santa Maria della Torre
          ›Chiesa della Madonna del Buon Viaggio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#508">›Santa Maria in
          Traspontina‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#511">›Santa Maria in
          Trastevere‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#528">›Santa Maria in
          Trivio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#531">Santa Maria e San
          Gregorio in Vallicella, detta la ›Chiesa Nuova‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#538">›Santa Maria delle
          Vergini‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#539">›Santa Maria in Via‹ e
          suo oratorio</a>
        </li>
        <li>
          <a href="dg45043803s.html#542">›Santa Maria in Via
          Lata‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#548">›Santa Maria della
          Visitazione‹, e San Francesco di Sales</a>
        </li>
        <li>
          <a href="dg45043803s.html#548">›Santa Maria della
          Vittoria‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#556">›Santa Maria Maddalena‹
          de' Padri Ministri degl'Infermi</a>
        </li>
        <li>
          <a href="dg45043803s.html#559">›Santa Maria Maddalena
          al Quirinale‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#559">›Santa Marta (Collegio
          Romano)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#561">›Santa Marta
          (Vaticano)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#562">›Santi Luca e
          Martina‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#566">›Santi Martino e
          Sebastiano degli Svizzeri‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#566">›San Martino ai
          Monti‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#576">›Santi Michele e Magno‹
          in Borgo</a>
        </li>
        <li>
          <a href="dg45043803s.html#577">San Michele alle Fornaci
          ›Sant'Angelo del Torrione‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#577">Santissima Natività di
          Gesù Cristo ›Chiesa della Natività di Gesù‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#578">›Santi Nereo e
          Achilleo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#580">›San Niccolò degli
          Arcioni‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#580">›San Nicola in
          Carcere‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#582">›San Nicola ai
          Cesarini‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#583">›San Nicolò degli
          Incoronati‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#584">›San Nicola dei
          Lorenesi‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#585">›San Nicola ai
          Prefetti‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#585">›San Nicola da
          Tolentino‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#587">›Santissimo Nome di
          Maria‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#588">›San Norberto‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#588">›Sant'Omobono‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#589">›Sant'Onofrio al
          Gianicolo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#591">Oratorio di San Marcello
          ›Oratorio del Crocifisso‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#592">Sant'Orsola e suo
          Monastero ›Santi Giuseppe e Orsola (ex)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#592">›San Pancrazio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#595">›San Pantaleo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#598">›San Paolo Primo
          Eremita‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#599">›San Paolo alla
          Regola‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#602">San Paolo sulla Via
          Ostiense ›San Paolo fuori le Mura‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#610">›San Paolo alle Tre
          Fontane‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#611">›San Pellegrino‹ degli
          Svizzeri</a>
        </li>
        <li>
          <a href="dg45043803s.html#611">›Santi Marcellino e
          Pietro‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#612">›San Pietro in
          Montorio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#617">›Vaticano‹ ◉</a>
        </li>
        <li>
          <a href="dg45043803s.html#619">›San Pietro in Vaticano‹
          ▣</a>
        </li>
        <li>
          <a href="dg45043803s.html#622">›San Pietro in
          Vaticano‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#692">›San Pietro in
          Vincoli‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#699">›Santa Prassede‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#706">›Santa Pudenziana‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#710">Santi Quaranta Martiri
          ›San Pasquale Baylon‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#711">›Santi Quattro
          Coronati‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#713">›Santi Quirico e
          Giulitta‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#714">›Santa Rita da Cascia
          (ex)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#714">›San Rocco‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#716">›San Romualdo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#716">›Sante Rufina e
          Seconda‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#717">›San Saba‹ abbate</a>
        </li>
        <li>
          <a href="dg45043803s.html#717">›Santa Sabina‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#722">›San Salvatore in
          Campo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#722">›San Salvatore alle
          Coppelle‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#723">San Salvatore in Corte,
          o ›Santa Maria della Luce‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#724">›San Salvatore in
          Lauro‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#727">›San Salvatore ai
          Monti‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#727">›San Salvatore in
          Onda‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#727">San Salvatore a Ponte
          Rotto ›San Salvatore de pede pontis (scomparsa)‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#728">›San Salvatore in
          Primicerio‹ e San Trifone</a>
        </li>
        <li>
          <a href="dg45043803s.html#728">Santissimo Salvatore
          alla ›Scala Santa‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#733">San Salvatore alle Terme
          ›San Salvatore in Thermis‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#733">›San Sebastiano‹ fuori
          delle mura</a>
        </li>
        <li>
          <a href="dg45043803s.html#740">Sam Sebastiano all'Olmo
          ›San Sebastiano de' Mercanti‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#740">San Sebastiano alla
          Polveriera ›San Sebastiano al Palatino‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#741">›Santi Sergio e Bacco‹ o
          Santa Maria del Pascolo</a>
        </li>
        <li>
          <a href="dg45043803s.html#742">›San Silvestro in
          Capite‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#745">›San Silvestro al
          Quirinale‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#748">›Santi Simone e
          Giuda‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#748">›San Simeone
          Profeta‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#748">San Sisto Papa ›San
          Sisto Vecchio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#750">›Santo Spirito in
          Sassia‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#752">›Chiesa dello Spirito
          Santo dei Napoletani‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#753">›Santo Stanislao dei
          Polacchi‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#754">›Santo Stefano del
          Cacco‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#755">Santo Stefano de' mori
          ›Santo Stefano degli Abissini‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#755">Santo Stefano in
          Pescinula ›Santo Stefano de Piscina‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#756">›Santo Stefano
          Rotondo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#759">›Sante Stimmate di San
          Francesco‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#761">Santissimo Sudario de'
          Savojardi ›Santissimo Sudario di Nostro Signore Gesù
          Cristo‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#761">›Santa Susanna‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#765">›San Teodoro‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#767">›Santa Teresa alle
          Quattro Fontane‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#767">›San Tommaso di
          Canterbury‹ al Collegio Inglese</a>
        </li>
        <li>
          <a href="dg45043803s.html#767">›San Tommaso in
          Formis‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#768">›San Tommaso in
          Parione‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#769">›Santissima Trinità
          degli Spagnoli‹ in Via Condotti</a>
        </li>
        <li>
          <a href="dg45043803s.html#770">›Santissima Trinità
          della Missione‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#771">›Trinità dei Monti‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#777">›Santissima Trinità dei
          Pellegrini‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#779">›Sant'Urbano‹ alla
          Caffarella</a>
        </li>
        <li>
          <a href="dg45043803s.html#779">Sant'Urbano a Campo
          Carleo ›Sant'Urbano ai Pantani‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#780">Santi Venanzio, Ruffina
          e Seconda al Laterano ›Cappella di San Venanzio‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#783">Santi Venanzio ed
          Ansuino de' Camerinesi ›San Giovanni in Mercatello‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#784">›Santi Vincenzo ed
          Anastasio alla Regola‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#784">›Santi Vincenzo ed
          Anastasio alle Tre Fontane‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#786">›Santi Vincenzo ed
          Anastasio‹ a Trevi</a>
        </li>
        <li>
          <a href="dg45043803s.html#787">›San Vitale‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#789">›Santi Vito e
          Modesto‹</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45043803s.html#792">Appendice</a>
      <ul>
        <li>
          <a href="dg45043803s.html#792">›Santa Chiara‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#792">›Santa Francesca Romana‹
          in Campo Vaccino, o Santa Maria Nuova</a>
        </li>
        <li>
          <a href="dg45043803s.html#795">Santa Francesca Romana
          in Via Felice ›Santa Francesca Romana dei Padri del
          Riscatto‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#796">›Oratorio del
          Gonfalone‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#797">Oratorio di San
          Francesco detto del Caravita ›Oratorio del Caravita‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#797">Oratorio di ›San Lorenzo
          in Lucina‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#797">›Oratorio di Santa Maria
          Addolorata in Trastevere‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#798">›Oratorio del Monte di
          Pietà‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#798">›Oratorio
          dell'Arciconfraternita della Pietà dei Fiorentini‹</a>
        </li>
        <li>
          <a href="dg45043803s.html#799">›Oratorio della
          Santissima Trinità dei Pellegrini‹</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45043803s.html#800">Indice degli argomenti
      contenuti in questo volume</a>
    </li>
    <li>
      <a href="dg45043803s.html#801">Indice delle materie
      contenute in questo volume</a>
    </li>
    <li>
      <a href="dg45043803s.html#812">Indice delle tavole</a>
    </li>
  </ul>
  <hr />
</body>
</html>
