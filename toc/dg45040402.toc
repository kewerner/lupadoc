<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg45040402s.html#6">Itinerario istruttivo di Roma
      antica e moderna</a>
      <ul>
        <li>
          <a href="dg45040402s.html#8">Indice</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45040402s.html#10">[Text]</a>
      <ul>
        <li>
          <a href="dg45040402s.html#10">Quinta giornata</a>
          <ul>
            <li>
              <a href="dg45040402s.html#10">Collegio della
              Sapienza ›Palazzo della Sapienza‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#12">Palazzo del
              Governo</a>
            </li>
            <li>
              <a href="dg45040402s.html#12">Palazzo del Governo
              ›Palazzo Madama‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#13">›Palazzo
              Giustiniani‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#19">Chiesa di ›San Luigi
              de' Francesi‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#21">Chiesa di
              ›Sant'Agostino‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#24">Chiesa di
              ›Sant'Apollinare‹, e Collegio Germanico</a>
            </li>
            <li>
              <a href="dg45040402s.html#25">›Palazzo Altemps‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#25">Chiesa di
              ›Sant'Antonio dei Portoghesi‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#26">›Palazzo
              Lancellotti‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#27">Chiesa di ›San
              Salvatore in Lauro‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#28">Chiesa dei ›Santi
              Celso e Giuliano‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#30">›Palazzo del Banco di
              Santo Spirito‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#30">Palazzo Gabrielli
              ›Palazzo Taverna‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#30">Chiesa di Santa Maria
              in Vallicella, communemente detta ›Chiesa Nuova‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#34">›Palazzo Sora‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#34">Chiesa di ›Santa
              Maria della Pace‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#36">Chiesa di ›Santa
              Maria dell'Anima‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#37">Chiesa di ›San Nicola
              dei Lorenesi‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#38">›Piazza Navona‹</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#40">›Piazza Navona‹
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45040402s.html#42">Chiesa di
              ›Sant'Agnese in Agone‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#45">Chiesa di San Giacomo
              degli Spagnoli ›Nostra Signora del Sacro Cuore‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#46">›Piazza di
              Pasquino‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#47">Chiesa di ›San
              Pantaleo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#47">Palazzo Massimi
              ›Palazzo Massimo Istoriato‹</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#48">›Palazzo Massimo
                  Istoriato‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45040402s.html#50">Chiesa di
              ›Sant'Andrea della Valle‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#53">Chiesa del
              ›Santissimo Sudario di Nostro Signore Gesù
              Cristo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#54">Chiesa di San
              Giuliano dei Fiamminghi ›San Giuliano
              Ospitaliere‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#54">Chiesa di ›San Nicola
              ai Cesarini‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#55">Chiesa di ›Santa
              Lucia alle Botteghe Oscure‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#56">Chiesa di ›Santo
              Stanislao dei Polacchi‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#56">Palazzo Mattei
              ›Palazzo Caetani‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#60">›Palazzo
              Costaguti‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#60">Chiesa, e Monastero
              di ›Sant'Ambrogio della Massima‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#61">Chiesa di ›Santa
              Caterina dei Funari‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#62">Cgiesa di ›Santa
              Maria in Campitelli‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#64">Casa detta di Tor di
              Specchi ›Monastero di Tor de' Specchi‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#65">›Portico di
              Ottavia‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#66">Chiesa di
              ›Sant'Angelo in Pescheria‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#66">›Teatro di Marcello‹,
              ora ›Palazzo Orsini‹</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#68">›Teatro di
                  Marcello‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45040402s.html#70">Chiesa di ›San Nicola
              in Carcere‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#71">Chiesa di ›Santa
              Maria della Consolazione‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#72">Chiesa di ›San
              Giovanni Decollato‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#73">L'arco di Giano
              Quadrifronte ›Arco di Giano‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#74">›Arco di Giano‹
              Quadrifronte</a>
            </li>
            <li>
              <a href="dg45040402s.html#76">Chiesa di San
              Giorgio, detta in Velabro ›San Giorgio in
              Velabro‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#76">›Arco di Settimio
              Severo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#77">›Cloaca Maxima‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#79">Chiesa di
              ›Sant'Anastasia‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#80">›Circo Massimo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#82">Chiesa di ›San
              Gregorio Magno‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#85">Chiesa di ›Santa
              Balbina‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#85">›Terme di
              Caracalla‹</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#86">›Terme di
                  Caracalla‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45040402s.html#89">Chiesa dei ›Santi
              Nereo e Achilleo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#91">›Porta Latina‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#91">›Arco di Druso‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#92">›Sepolcro degli
              Scipioni‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#92">›Porta San
              Sebastiano‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#94">Fiumicello
              ›Almone‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#95">Basilica di ›San
              Sebastiano‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#97">Equirie del Circo di
              Caracalla ›Scuderie del Circo di Caracalla‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#98">›Tomba di Cecilia
              Metella‹ ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#100">›Tomba di Cecilia
              Metella‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#101">Circo di Caracalla
              ›Circo di Massenzio‹</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#102">Circo di
                  Caracalla ›Circo di Massenzio‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45040402s.html#105">Tempio delle Camene,
              volgarmente detto di Bacco ›Camenarum, Fons et
              Lucus‹, in oggi Chiesa di ›Sant'Urbano‹</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#106">Tempio di Bacco
                  ›Sant'Urbano‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45040402s.html#108">Grotta della Ninfa
              Egeria ›Ninfeo di Egeria‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#109">›Tempio della
              Fortuna Muliebre‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#109">Chiesa di ›San Paolo
              alle Tre Fontane‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#110">›Ninfeo di Egeria‹
              ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#112">›Tempio della
              Fortuna Muliebre‹ ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#115">Basiliche di ›San
              Paolo fuori le Mura‹</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#116">›San Paolo fuori
                  le Mura‹ ▣</a>
                </li>
                <li>
                  <a href="dg45040402s.html#120">Interno di ›San
                  Paolo fuori le Mura‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45040402s.html#123">›Porta San
              Paolo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#123">›Piramide di Caio
              Cestio‹</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#124">›Piramide di
                  Caio Cestio‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45040402s.html#127">Chiesa di ›San
              Saba‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#127">Chiesa di ›Santa
              Prisca‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#129">Chiesa di ›Santa
              Sabina‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#129">Chiesa di
              ›Sant'Alessio‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#130">Chiesa di ›Santa
              Maria del Priorato‹ di Malta</a>
            </li>
            <li>
              <a href="dg45040402s.html#132">›Testaccio
              (Monte)‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#133">Antichi Navali, in
              oggi detti la Marmorata ›Navalia‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#134">›Ponte Sublicio‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#135">Chiesa di ›Santa
              Maria in Cosmedin‹, già ›Tempio della Pudicitia
              Patricia‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#137">Tempio di Vesta
              ›Tempio di Hercules Olivarius‹, in oggi Chiesa di
              ›Santa Maria del Sole‹</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#138">Tempio di Vesta
                  ›Tempio di Hercules Olivarius‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45040402s.html#140">Tempio della Fortuna
              Virile ›Tempio di Portunus‹, in oggi Chiesa di ›Santa
              Maria Egiziaca‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#142">Ponte Palatino,
              detto in oggi ›Ponte Rotto‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45040402s.html#144">Sesta giornata</a>
          <ul>
            <li>
              <a href="dg45040402s.html#144">›Ponte Fabricio‹, in
              oggi detto Quattro Capi</a>
            </li>
            <li>
              <a href="dg45040402s.html#145">›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#146">Chiesa di ›San
              Giovanni Calibita‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#146">Chiesa di ›San
              Bartolomeo all'Isola‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#148">›Ponte Cestio‹, in
              oggi detto di San Bartolommeo</a>
            </li>
            <li>
              <a href="dg45040402s.html#149">Chiesa di ›San
              Benedetto in Piscinula‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#149">Chiesa di ›Santa
              Cecilia in Trastevere‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#151">Chiesa di ›Santa
              Maria dell'Orto‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#152">›Porto di Ripa
              Grande‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#152">›Ospizio Apostolico
              di San Michele a Ripa Grande (ex)‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#153">Porta Portese ›Porta
              Portuensis‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#154">›Porto di Ripa
              Grande‹ ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#156">Chiesa di ›San
              Francesco a Ripa‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#157">Chiesa dei Santi
              Quaranta, o di ›San Pasquale Baylon‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#158">Chiesa di ›San
              Callisto‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#159">›Santa Maria in
              Trastevere‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#161">Chiesa di ›San
              Crisogono‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#162">Chiesa di ›Santa
              Maria della Scala‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#163">Monte
              ›Gianicolo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#164">Chiesa di San
              Pietro, detta in Montorio ›San Pietro in
              Montorio‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#166">Fontana Paolina,
              volgarmente detta di San Pietro in Montorio ›Fontana
              dell'Acqua Paola (via Garibaldi)‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#167">›Porta San
              Pancrazio‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#167">Casino della ›Villa
              Giraud‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#168">Fontana Paolina
              ›Fontana dell'Acqua Paola (via Garibaldi)‹ ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#170">Villa Corsini
              ›Palazzo Corsini‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#170">›Villa Doria
              Pamphilj‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#171">Chiesa di ›San
              Pancrazio‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#173">›Porta
              Settimiana‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#173">›Palazzo
              Corsini‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#179">Casino Farnese.
              detto la Farnesina ›Villa Farnesina‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#182">Chiesa di
              ›Sant'Onofrio al Gianicolo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#184">›Porta Santo
              Spirito‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#185">Chiesa di ›Santa
              Dorotea‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#185">›Ponte Sisto‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45040402s.html#186">Settima giornata</a>
          <ul>
            <li>
              <a href="dg45040402s.html#186">Fontana di Ponte
              Sisto ›Fontana dell'Acqua Paola (Piazza
              Trilussa)‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#186">L'Ospizio
              Ecclesiastico, detto dei Cento Preti ›Ospizio dei
              Cento Preti‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#187">Chiesa della
              Trinità, e Ospizio dei Pellegrini</a>
            </li>
            <li>
              <a href="dg45040402s.html#187">Chiesa della
              ›Santissima Trinità dei Pellegrini‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#190">›Palazzo del Monte
              di Pietà‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#191">Chiesa di San Paolo,
              detto San Paolino alla Regola ›San Paolo alla
              Regola‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#192">›Palazzo
              Santacroce‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#193">Chiesa di ›Santa
              Maria in Cacabariis‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#194">Chiesa di ›Santa
              Maria della Visitazione e San Francesco di Sales‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#194">›San Carlo ai
              Catinari‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#196">›Santa Barbara dei
              Librari‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#196">Piazza ›Campo de'
              Fiori‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#197">›Palazzo Pio
              Righetti‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#198">›Palazzo della
              Cancelleria‹ ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#200">›Palazzo della
              Cancelleria‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#201">›Palazzo
              Farnese‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#202">›Palazzo Farnese‹
              ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#208">Chiesa dei ›Santi
              Giovanni Evangelista e Petronio dei Bolognesi‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#208">›Palazzo Spada‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#212">Chiesa di ›Santa
              Maria dell'Orazione e Morte‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#213">Chiesa di ›Santa
              Caterina da Siena‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#213">Chiesa di ›Santa
              Caterina della Rota‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#214">Chiesa di ›San
              Girolamo della Carità‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#215">Chiesa di San
              Tommaso, e Collegio degl'Inglesi ›San Tommaso di
              Canterbury‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#216">›Chiesa dello
              Spirito Santo dei Napoletani‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#217">Chiesa di San Lucia,
              detta della Chiavica ›Santa Lucia del Gonfalone‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#218">Chiesa di ›Santa
              Maria del Suffragio‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#219">Chiesa di ›San
              Giovanni dei Fiorentini‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#221">Vestigi del Ponte
              Trionfale ›Pons Neronianus‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45040402s.html#224">Ottava giornata</a>
          <ul>
            <li>
              <a href="dg45040402s.html#224">›Ponte
              Sant'Angelo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#225">Mausoleo d'Adriano
              in oggi ›Castel Sant'Angelo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#226">Antico stato del
              Mausoleo d'Adriano ›Castel Sant'Angelo‹ ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#230">Ponte e Castel
              Sant'Angelo ›Ponte Sant'Angelo‹ ›Castel
              Sant'Angelo‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#232">›Ospedale di Santo
              Spirito in Sassia‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#233">›Santo Spirito in
              Sassia‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#234">Chiesa di ›Santa
              Maria in Traspontina‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#236">Palazzo Giraud
              ›Palazzo Castellesi‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#236">Chiesa di ›San
              Giacomo Scossacavalli‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#238">›Piazza San Pietro‹
              in Vaticano</a>
            </li>
            <li>
              <a href="dg45040402s.html#239">L' ›Obelisco
              Vaticano‹</a>
            </li>
            <li>
              <a href="dg45040402s.html#240">Piazza e Basilica di
              San Pietro ›Piazza San Pietro‹ ›San Pietro in
              Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#243">Basilica di ›San
              Pietro in Vaticano‹</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#246">›San Pietro in
                  Vaticano: Facciata‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#249">›San Pietro in
                  Vaticano: Interno‹</a>
                  <ul>
                    <li>
                      <a href="dg45040402s.html#250">›San Pietro
                      in Vaticano: Interno‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg45040402s.html#254">›San Pietro in
                  Vaticano: Confessione‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#256">›San Pietro in
                  Vaticano: Cupola‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#259">›San Pietro in
                  Vaticano: Cattedra di San Pietro‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#262">›San Pietro in
                  Vaticano: Navata destra‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#265">›San Pietro in
                  Vaticano: Cappella Clementina‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#266">›San Pietro in
                  Vaticano: Cappella del Coro‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#267">›San Pietro in
                  Vaticano: Cappella della Presentazione‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#268">›San Pietro in
                  Vaticano: Fonte battesimale‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#269">›San Pietro in
                  Vaticano: Cappella della Pietà‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#270">›San Pietro in
                  Vaticano: Cappella di San Sebastiano‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#270">›San Pietro in
                  Vaticano: Cappella del Sacramento‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#272">Cappella della
                  Madonna ›San Pietro in Vaticano: Cappella
                  Gregoriana‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#275">›San Pietro in
                  Vaticano: Sacre Grotte Vaticane‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#277">›San Pietro in
                  Vaticano: Sagrestia‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#281">›Palazzo
                  Apostolico Vaticano‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#284">›Cappella
                  Sistina‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#287">›Vaticano:
                  Loggia di Raffaello‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#290">›Vaticano:
                  Stanze di Raffaello‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#291">›Vaticano: Sala
                  di Costantino‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#293">Seconda Camera
                  di Raffaello ›Vaticano: Stanza di Eliodoro‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#295">Terza Camera di
                  Raffaello ›Vaticano: Stanza della Segnatura‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#297">Quarta Camera di
                  Raffaello ›Vaticano: Stanza dell'Incendio‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#299">›Biblioteca
                  Vaticana‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#304">›Vaticano: Museo
                  Pio Clementino‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#305">›Vaticano:
                  Vestibolo Quadrato‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#307">›Vaticano:
                  Vestibolo Rotondo‹</a>
                  <ul>
                    <li>
                      <a href="dg45040402s.html#308">›Vaticano:
                      Vestibolo Rotondo‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg45040402s.html#311">Portico attorno
                  il Cortile ›Vaticano: Cortile ottagono‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#312">Portico del
                  Cortile del Museo Vaticano ›Vaticano: Cortile
                  ottagono‹ ▣</a>
                </li>
                <li>
                  <a href="dg45040402s.html#322">›Vaticano: Sala
                  degli Animali‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#329">›Vaticano:
                  Galleria delle Statue‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#330">Galleria della
                  Cleopatra, Museo Vaticano ›Vaticano: Galleria
                  delle Statue‹ ▣</a>
                </li>
                <li>
                  <a href="dg45040402s.html#338">Prima stanza de'
                  Busti ›Vaticano: Galleria dei Busti‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#340">Seconda stanza
                  de' Busti ›Vaticano: Galleria dei Busti‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#341">Terza stanza de'
                  Busti ›Vaticano: Galleria dei Busti‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#342">›Vaticano:
                  Loggia scoperta‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#344">Gabinetto del
                  Museo Vaticano ›Vaticano: Gabinetto delle
                  Maschere‹ ▣</a>
                </li>
                <li>
                  <a href="dg45040402s.html#346">›Vaticano:
                  Gabinetto delle Maschere‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#349">›Vaticano: Sala
                  delle Muse‹</a>
                  <ul>
                    <li>
                      <a href="dg45040402s.html#350">Camera delle
                      Muse, del Museo Vaticano ›Vaticano: Sala
                      delle Muse‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg45040402s.html#357">›Vaticano: Sala
                  Rotonda‹</a>
                  <ul>
                    <li>
                      <a href="dg45040402s.html#358">Sala Rotonda
                      del Museo Vaticano ›Vaticano: Sala Rotonda‹
                      ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg45040402s.html#361">›Vaticano: Sala
                  a Croce Greca‹</a>
                  <ul>
                    <li>
                      <a href="dg45040402s.html#362">›Vaticano:
                      Sala a Croce Greca‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg45040402s.html#367">Scala Principale
                  del Museo ›Vaticano: Scala Simonetti‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#367">Scala Principale
                  del Museo ›Vaticano: Scala Simonetti‹</a>
                  <ul>
                    <li>
                      <a href="dg45040402s.html#368">Scala
                      Principale del Museo ›San Pietro in Vaticano:
                      Scala Simonetti‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg45040402s.html#370">Camera della
                  Biga ›Vaticano: Sala della Biga‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#371">›Vaticano:
                  Galleria dei Candelabri‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#380">Giardino
                  Pontificio, detto di Belvedere ›Cortile del
                  Belvedere‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#383">Chiesa di ›Santa
                  Marta (Vaticano)‹</a>
                </li>
                <li>
                  <a href="dg45040402s.html#385">Chiesa di ›Santa
                  Maria della Pietà‹ in Campo Santo</a>
                </li>
                <li>
                  <a href="dg45040402s.html#387">›Monte
                  Mario‹</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45040402s.html#390">Descrizione delle
          adjacenze di Roma</a>
          <ul>
            <li>
              <a href="dg45040402s.html#390">Città di Tivoli</a>
            </li>
            <li>
              <a href="dg45040402s.html#391">Ponte della
              Solfatara</a>
            </li>
            <li>
              <a href="dg45040402s.html#392">›Ponte Mammolo‹
              ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#394">Villa Adriana</a>
            </li>
            <li>
              <a href="dg45040402s.html#396">Sepolcro della
              Famiglia Plauzia ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#398">Tempio della
              Sibilla, a Tivoli ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#400">Tempio della
              Sibilla</a>
            </li>
            <li>
              <a href="dg45040402s.html#401">Grotta di
              Nettuno</a>
            </li>
            <li>
              <a href="dg45040402s.html#401">Ponte Lupo</a>
            </li>
            <li>
              <a href="dg45040402s.html#402">Grotta di Nettuno, a
              Tivoli ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#404">Gran Caduta del
              Fiume Aniene</a>
            </li>
            <li>
              <a href="dg45040402s.html#404">Grotta delle
              Sirene</a>
            </li>
            <li>
              <a href="dg45040402s.html#405">Cascatelle di
              Tivoli</a>
            </li>
            <li>
              <a href="dg45040402s.html#405">Villa d'Este</a>
            </li>
            <li>
              <a href="dg45040402s.html#406">Cascata di Tivoli
              ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#408">Cascatelle di Tivoli
              ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#410">Città di Subiaco</a>
            </li>
            <li>
              <a href="dg45040402s.html#412">Città di
              Palestrina</a>
            </li>
            <li>
              <a href="dg45040402s.html#413">Città di
              Frascati</a>
            </li>
            <li>
              <a href="dg45040402s.html#415">Grottaferrata</a>
            </li>
            <li>
              <a href="dg45040402s.html#416">Marino</a>
            </li>
            <li>
              <a href="dg45040402s.html#416">Castel Gandolfo</a>
            </li>
            <li>
              <a href="dg45040402s.html#418">Sepolcro di
              Ascanio</a>
            </li>
            <li>
              <a href="dg45040402s.html#420">Città d'Albano</a>
            </li>
            <li>
              <a href="dg45040402s.html#421">Riccia</a>
            </li>
            <li>
              <a href="dg45040402s.html#421">Gensano</a>
            </li>
            <li>
              <a href="dg45040402s.html#422">Sepolcro degli
              Orazj, e dei Curiazj ›Tomba dei Curiazi‹ ▣</a>
            </li>
            <li>
              <a href="dg45040402s.html#424">Nemi</a>
            </li>
            <li>
              <a href="dg45040402s.html#424">Civita Lavinia</a>
            </li>
            <li>
              <a href="dg45040402s.html#425">Città di
              Velletri</a>
            </li>
            <li>
              <a href="dg45040402s.html#426">Plaudi Pontine</a>
            </li>
            <li>
              <a href="dg45040402s.html#427">Nettuno</a>
            </li>
            <li>
              <a href="dg45040402s.html#428">Tempio d'Ercole a
              Cora ▣</a>
              <ul>
                <li>
                  <a href="dg45040402s.html#430">Interno del
                  Tempio d'Ercole a Cora ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45040402s.html#432">Ostia</a>
            </li>
            <li>
              <a href="dg45040402s.html#433">Civitavecchia</a>
            </li>
            <li>
              <a href="dg45040402s.html#434">Palazzo di
              Caprarola</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45040402s.html#436">[Indici]</a>
      <ul>
        <li>
          <a href="dg45040402s.html#436">Indice generale delle
          materie</a>
        </li>
        <li>
          <a href="dg45040402s.html#472">Registro de' rami
          contenuti nella presente opera</a>
        </li>
        <li>
          <a href="dg45040402s.html#476">Catalogo delle opere del
          Cavalier Giuseppe Vasi e di altri autori</a>
          <ul>
            <li>
              <a href="dg45040402s.html#476">Vedute di Roma</a>
            </li>
            <li>
              <a href="dg45040402s.html#478">Statue</a>
            </li>
            <li>
              <a href="dg45040402s.html#479">Pitture
              all'acquarella</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
