<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy540501012s.html#10">Die Sixtinische Kapelle
      ›Cappella Sistina‹. 1, Tafeln [Bau und Schmuck del Kapelle
      unter Sixtus IV. Vierunddreissig Tafeln nach
      architektonischen Entwürfen von Giovanni Battista Giovenale
      und photographischen Aufnahmen von Domenico Anderson]</a>
    </li>
    <li>
      <a href="dy540501012s.html#12">Inhalt</a>
    </li>
    <li>
      <a href="dy540501012s.html#14">[Tafeln]</a>
      <ul>
        <li>
          <a href="dy540501012s.html#14">I. Rovere-Wappen in der
          Kapelle Sixtus IV. ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#16">II. Rekonstruktion der
          Kapelle Sixtus IV. Aussenansicht ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#18">III. Gegenwärtige
          Aussenansicht der Kapelle ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#20">IV. Querschnitt der
          Kapelle. Gegenwärtiger Zustand ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#22">V. Grundriss des
          befestigten Daches ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#24">VI. Grundriss des
          Zwischenstockes ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#26">VII. Rekonstruktion der
          Kapelle Sixtus IV. Innenansicht ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#28">VIII. Grundriss der
          Kapelle. Gegenwärtiger Zustand ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#30">IX. Durchschnitt der
          Kapelle in der Fensterhöhe. Rekonstruktion des Paviments
          ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#32">X. Ausschnitt aus dem
          Opus Alexandrinum ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#34">XI. Die Sängertribüne.
          Florentiner Werkstatt-Arbeit ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#36">XII. Cancellata:
          Eckplatte (links). Werkstatt des Dalmata ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#36">XII. Cancellata:
          Eckplatte (rechts). Werkstatt des Mino ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#38">XIII. Cancellata:
          Mittelplatte (links). Werkstatt des Dalmata ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#38">XIII. Cancellata:
          Mittelplatte (rechts). Werkstatt des Dalmata ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#40">XIV. Cancellata:
          Eckplatte (links). Werkstatt des Mino ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#40">XIV. Cancellata:
          Eckplatte (rechts). Werkstatt des Mino ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#42">XV. Aufriss der
          Kapellenwand im Innern (A) ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#44">XVI. Aufriss der
          Kapellenwand im Innern (B) ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#46">XVII. Sanctus Anicetus
          von Fra Diamante ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#46">XVII. Sanctus
          Eleutherus von Fra Diamante ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#48">XVIII. Sanctus Pius von
          Domenico Ghirlandaio ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#48">XVIII. Sanctus Victor
          von Domenico Ghirlandaio ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#50">XIX. Sanctus Stephanus
          von Sandro Botticelli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#50">XIX. Sanctus Soter von
          Sandro Botticelli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#52">XX. Sanctus Callistus
          von Cosimo Rosselli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#52">XX. Sanctus Dionysius
          von Cosimo Rosselli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#54">XXI. Sanctus Felix von
          Ghirlandaio ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#54">XXI. Sanctus Urbanus
          von Fra Diamante ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#54">XXI. Sanctus Sixtus II.
          von Botticelli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#56">XXII. Die Beschneidung
          des Mosesknaben von Perugino und Pinturicchio ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#58">XXIII. Das Jugendleben
          des Moses von Sandro Botticelli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#60">XXIV. Israels Errettung
          und Pharaos Untergang im Roten Meer von Pier di Cosimo
          und einem unbekannten Schüler Rossellis ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#62">XXV. Die Gesetzgebung
          auf Sinai von Cosimo Rosselli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#64">XXVI. Die Bestrafung
          der Rotte Korah von Sandro Botticelli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#66">XXVII. Das Testament
          des Moses von Luca Signorelli und Bartolomeo della Gatta
          ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#68">XXVIII. Die Taufe
          Christi von Perugino und Pinturicchio ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#70">XXIX. Die Versuchung
          Christi und das Reinigungsopfer des Aussätzigen von
          Sandro Botticelli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#72">XXX. Die Berufung der
          ersten Jünger von Domenico Ghirlandaio ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#74">XXXI. Die Bergpredigt
          Christi von Cosimo Rosselli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#76">XXXII. Die
          Schlüsselübergabe von Pietro Perugino und Luca Signorelli
          ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#78">XXXIII. Das letzte
          Abendmahl von Cosimo Rosselli ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#80">XXXIV. Ein Kultusakt in
          der Kapelle Sixtus IV. Stich von Lorenzo Vaccari, 1578
          ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#82">Sixtina. Tafel X.
          Ausschnitt aus dem Opus Alexandrinum Rapporto in 0.050
          Professore Giovanni Stern [disegno originale su carta]
          ▣</a>
        </li>
        <li>
          <a href="dy540501012s.html#84">Sixtina Tafel I.
          Roverewappen in der Kapelle Sixtus IV. Wappen Sixtus IV.
          Decke der Mappe und der Einbandes. Professor Giovanni
          Stern Roma [disegno originale su carta] ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
