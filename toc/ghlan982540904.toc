<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlan982540904s.html#8">Storia pittorica della
      Italia; tomo IV. Ove si descrivono le scuole lombarde di
      Mantova, Modena, Parma, Cremona, e Milano</a>
      <ul>
        <li>
          <a href="ghlan982540904s.html#10">Compartimento di
          questo tomo quarto</a>
        </li>
        <li>
          <a href="ghlan982540904s.html#12">Della storia
          pittorica della Italia superiore</a>
          <ul>
            <li>
              <a href="ghlan982540904s.html#12">II. Delle scuole
              lombarde</a>
              <ul>
                <li>
                  <a href="ghlan982540904s.html#15">I. Scuola
                  mantovana&#160;</a>
                  <ul>
                    <li>
                      <a href="ghlan982540904s.html#15">I. Il
                      Mantegna e i suoi successori&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#22">II.
                      Giulio Romano e la sua scuola</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#32">III.
                      Decadenza della scuola e fondazione di
                      un'accademia per avvivarla</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="ghlan982540904s.html#37">II. Scuola
                  modenese</a>
                  <ul>
                    <li>
                      <a href="ghlan982540904s.html#37">I. Gli
                      Antichi&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#45">II. Nel
                      secolo XVI. s'imitano Raffaello e il
                      Coreggio&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#57">III. I
                      Modenesi del secolo XVII sieguono per lo più
                      i Bolognesi&#160;</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="ghlan982540904s.html#71">III. Della
                  scuola di Parma</a>
                  <ul>
                    <li>
                      <a href="ghlan982540904s.html#71">I. Gli
                      antichi&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#75">II. Il
                      Coreggio e i successori della sua
                      scuola&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#117">III.
                      Parmigiani ellievi de' Caracci e di altri
                      esteri fino alla fondazione
                      dell'Accademia&#160;</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="ghlan982540904s.html#128">IV. Scuola
                  cremonese</a>
                  <ul>
                    <li>
                      <a href="ghlan982540904s.html#128">I. Gli
                      antichi&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#139">II.
                      Camillo Boccaccino, il Sojaro, i
                      Campi&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#154">III. La
                      scuola de' Campi va alterandosi. Il Trotti ed
                      altri la sostengono&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#164">IV.
                      Maniere estere in Cremona&#160;</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="ghlan982540904s.html#173">V. Scuola
                  milanese</a>
                  <ul>
                    <li>
                      <a href="ghlan982540904s.html#173">I. Gli
                      antichi fino alla venuta del Vinci&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#198">II. Il
                      Vinci stabilisce Accademia di disegno in
                      Milano, allievi di esso, e de' miglior
                      nazionali fino a Gaudenzio&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#234">III. I
                      Procaccini ed altri pittori esteri e
                      cittadini stabiliscono in Milano nuova
                      Accademia e nuovi stili&#160;</a>
                    </li>
                    <li>
                      <a href="ghlan982540904s.html#251">IV.
                      Dopo Daniele Crespi la pittura va
                      peggiorando. Fondasi una terza Accademia per
                      migliorarla&#160;</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
