<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ever30234904s.html#6">Notizie Storiche Delle
      Chiese Di Verona; libro quarto</a>
      <ul>
        <li>
          <a href="ever30234904s.html#182">Supplementi alle
          notizie delle chiese di Verona</a>
        </li>
        <li>
          <a href="ever30234904s.html#184">A' leggitori</a>
        </li>
        <li>
          <a href="ever30234904s.html#466">Indice generale delle
          chiese di Verona</a>
        </li>
        <li>
          <a href="ever30234904s.html#469">Indice degli spedali
          di Verona</a>
        </li>
        <li>
          <a href="ever30234904s.html#470">Indice degli spedali
          del territorio</a>
        </li>
        <li>
          <a href="ever30234904s.html#470">Indice delle chiese
          fuori di Verona</a>
        </li>
        <li>
          <a href="ever30234904s.html#472">Indice de' Vescovi di
          Verona</a>
        </li>
        <li>
          <a href="ever30234904s.html#475">Indice di alcune più
          notabili cose&#160;</a>
        </li>
        <li>
          <a href="ever30234904s.html#521">Indice delle cose
          aggiunte</a>
        </li>
        <li>
          <a href="ever30234904s.html#522">Indice de' Monumenti
          contenuti in quest'opera</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
