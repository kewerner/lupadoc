<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghpis4413780s.html#6">Dialoghi tra Claro, e
      Sarpiri per istruire chi desidera d’essere un eccellente
      pittore figurista</a>
      <ul>
        <li>
          <a href="ghpis4413780s.html#8">A Sua Eccellenza il
          Signor Principe D. Giovanni Lambertini</a>
        </li>
        <li>
          <a href="ghpis4413780s.html#10">Proemio</a>
        </li>
        <li>
          <a href="ghpis4413780s.html#14">I. Sopra la Nobiltà
          dell'Arte, e che cosa è Pittura</a>
        </li>
        <li>
          <a href="ghpis4413780s.html#39">II. Sopra le Statue
          Greche, Simmetrìa, e Notomìa</a>
        </li>
        <li>
          <a href="ghpis4413780s.html#65">III. Avvertenze nel
          disegnare il Nudo, della Prospettiva, ed
          Architettura&#160;</a>
        </li>
        <li>
          <a href="ghpis4413780s.html#92">IV. Dell'Invenzione, e
          Disposizione delle Figure, e del Chiaroscuro</a>
        </li>
        <li>
          <a href="ghpis4413780s.html#114">V. Gli studij che si
          devono fare avanti di principiare un'operazione, così
          pure sopra il Panegiamento, e Colorito</a>
        </li>
        <li>
          <a href="ghpis4413780s.html#136">VI. Sopra il Colorito,
          e la Pratica</a>
        </li>
        <li>
          <a href="ghpis4413780s.html#174">Indice della
          Materia</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
