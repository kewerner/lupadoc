<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="evar123700bs.html#6">Il Santuario di Varallo, in
      cui si contemplano gli alti misteri della nascità, vita,
      passione, morte, e risurrezione del nostro Signore Gesù
      Cristo</a>
      <ul>
        <li>
          <a href="evar123700bs.html#9">[Pianta, e salita del
          sacro Monte] ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#10">Pianta, e salita del
          sacro Monte, sopra di cui è fabbricato il Santuario di
          Varallo</a>
        </li>
        <li>
          <a href="evar123700bs.html#13">[La nuova Gerusalemme
          di Varallo] ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#14">La nuova Gerusalemme di
          Varallo</a>
        </li>
        <li>
          <a href="evar123700bs.html#15">Indulgenze, e
          Privilegj</a>
        </li>
        <li>
          <a href="evar123700bs.html#18">Al pio Lettore</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="evar123700bs.html#10">[Text]</a>
      <ul>
        <li>
          <a href="evar123700bs.html#26">Porta Maggiore della
          nuova Gerusalemme ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#28">La trasgressione di
          Adamo ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#31">L' Annunziata ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#34">La Visitazione ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#37">Sogno di San Giuseppe
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#40">Andata de' Magi ad
          adorare Gesù ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#43">Nascita di Gesù ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#46">Visita de' Pastori
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#49">Presentazione al Tempio
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#52">Avviso della Fuga ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#55">Fuga in Egitto ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#58">Strage degl' Innocenti
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#61">Il Battesimo di Cristo
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#64">Tentazione nel Deserto
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#67">La Samaritana ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#70">Il Paralitico ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#73">Gesù risuscita il
          figlio della Vedova ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#76">Transfigurazione sul
          Taborre ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#79">Gesù risuscita Lazaro
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#82">Entrata in Gerusalemme
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#85">Porta Aurea ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#86">Porta Aurea</a>
        </li>
        <li>
          <a href="evar123700bs.html#89">[Piazza Maggiore] ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#90">Piazza Maggiore</a>
        </li>
        <li>
          <a href="evar123700bs.html#91">Cena di Gesù con gli
          apostoli ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#94">Orazione all' orto
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#98">Li tre dormienti ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#101">Gesù preso da Giudei
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#105">[Piazza di Tribunali]
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#106">Piazza di
          Tribunali</a>
        </li>
        <li>
          <a href="evar123700bs.html#107">Gesù in Casa d' Anna
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#110">Gesù in Casa di Caifas
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#113">San Pietro piange il
          suo errore ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#115">Gesù in Casa di Pilato
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#118">Gesù in Casa di Erode
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#121">Gesù ricondotto a
          Pilato ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#126">Gesù flagelato ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#128">Gesù coronato di spine
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#131">Gesù, che sale la
          scala santa ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#134">Scala Santa ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#135">Scala Santa</a>
        </li>
        <li>
          <a href="evar123700bs.html#137">Gesù mostrato al
          popolo ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#140">Pilato si lava le mani
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#143">Gesù sentenziato a
          morte ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#146">Gesù porta la croce
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#150">Gesù inchiodato in
          croce sul Monte Calvario ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#153">Gesù levato in croce
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#157">Gesù deposto dalla
          croce ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#160">La pietà ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#163">Gesù nella sindone
          ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#166">San Francesco ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#169">Gesù posto nel
          sepolcro ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#173">Epilogo</a>
        </li>
        <li>
          <a href="evar123700bs.html#178">Chiesa del San
          Sepolcro ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#179">Chiesa del San
          Sepolcro</a>
        </li>
        <li>
          <a href="evar123700bs.html#181">B. Bernardino ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#182">B. Bernardino</a>
        </li>
        <li>
          <a href="evar123700bs.html#184">San Carlo ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#188">Gesù risorto con le
          cinque piaghe ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#190">[Lettera di Pilato a
          Cesare Tiberio]</a>
        </li>
        <li>
          <a href="evar123700bs.html#193">Serie della Nascita,
          Vita, e fine miserabile di Pilato</a>
        </li>
        <li>
          <a href="evar123700bs.html#195">Santa Anna ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#197">Annunziazione al
          transito di Maria Vergine ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#199">Sepolcro di Maria
          Vergine ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#203">La Santissima Madre di
          Dio assonta in cielo ▣</a>
        </li>
        <li>
          <a href="evar123700bs.html#204">La Santissima Madre di
          Gesù assonta in cielo</a>
        </li>
        <li>
          <a href="evar123700bs.html#206">Sonetto</a>
        </li>
        <li>
          <a href="evar123700bs.html#211">Tavola delle Chiese,
          Cappelle, e Misteri</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
