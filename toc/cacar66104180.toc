<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cacar66104180s.html#8">Elogio di Rosalba Carriera
      scritto da Girolamo Zanetti</a>
      <ul>
        <li>
          <a href="cacar66104180s.html#10">Alla nobile Donna
          Maria Lippomano Querini Stampalia</a>
        </li>
        <li>
          <a href="cacar66104180s.html#14">Elogio di Rosalba
          Carriera letto da Girolamo Zanetti in una privata
          Sessione dell'Accademia di belle Lettere ed Arti in
          Padova</a>
        </li>
        <li>
          <a href="cacar66104180s.html#28">Annotazioni</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
