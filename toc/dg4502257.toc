<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502257s.html#6">Specchio Overo Compendio
      Dell’Antichità di Roma</a>
      <ul>
        <li>
          <a href="dg4502257s.html#8">Illustrissimo, e
          Reverendissimo Signore</a>
        </li>
        <li>
          <a href="dg4502257s.html#10">Al lettore</a>
        </li>
        <li>
          <a href="dg4502257s.html#12">Tavola dell'Antichità Sacre
          di Roma</a>
        </li>
        <li>
          <a href="dg4502257s.html#18">I. Dove si tratta
          dell'Antichità Sacre, cioè delle Chiese Antiche di
          Roma</a>
          <ul>
            <li>
              <a href="dg4502257s.html#18">I. Delle nove Chiese
              della Città di Roma: e principalmente delle cinque
              Chiese Patriarcali</a>
            </li>
            <li>
              <a href="dg4502257s.html#62">II. Delle altre Chiese
              antiche più principali</a>
            </li>
            <li>
              <a href="dg4502257s.html#124">Appendice</a>
            </li>
            <li>
              <a href="dg4502257s.html#130">Aggiunte alle
              Antichità sacre</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502257s.html#138">II. Dove si tratta
          dell'Antichità Profane, che ancora restano in piedi</a>
        </li>
        <li>
          <a href="dg4502257s.html#188">Appendice delle
          abbreviature, e note antiche, più ordinarie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
