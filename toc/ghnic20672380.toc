<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghnic20672380s.html#10">La perspective
      curieuse</a>
      <ul>
        <li>
          <a href="ghnic20672380s.html#12">A monseigneur Georges
          Bolognetti</a>
        </li>
        <li>
          <a href="ghnic20672380s.html#18">Sommaire</a>
        </li>
        <li>
          <a href="ghnic20672380s.html#23">Preface et
          advertissement</a>
        </li>
        <li>
          <a href="ghnic20672380s.html#28">Preludes
          geometriques</a>
        </li>
        <li>
          <a href="ghnic20672380s.html#40">La premier livre</a>
        </li>
        <li>
          <a href="ghnic20672380s.html#79">La second livre</a>
        </li>
        <li>
          <a href="ghnic20672380s.html#103">La troisiesme
          livre</a>
        </li>
        <li>
          <a href="ghnic20672380s.html#129">La quatriesme
          livre</a>
        </li>
        <li>
          <a href="ghnic20672380s.html#150">Fautes laissees en
          l'impression</a>
        </li>
        <li>
          <a href="ghnic20672380s.html#151">Privilege du Roy</a>
        </li>
        <li>
          <a href="ghnic20672380s.html#152">[Tables]</a>
          <ul>
            <li>
              <a href="ghnic20672380s.html#152">I</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#154">II-V</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#156">VI-VII</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#158">VIII-XI</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#160">XII-XV</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#162">XVI-XIX</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#164">XX-XXII</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#166">XXIII-XXVI</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#168">XXVII-XXVIII</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#170">XXIX-XXX</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#172">XXXI-XXXII</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#174">XXXVI-XXXIX</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#176">XL-XLII</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#178">XLIII-XLV</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#180">XLVI-XLVIII</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#182">XLIX-LI</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#184">LII-LVI</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#188">LVII-LVIII</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#190">LIX</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#192">LX-LXI</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#194">LXII-LXIII</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#196">LXIV-LXVIII</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#198">LXIX-LXXI</a>
            </li>
            <li>
              <a href="ghnic20672380s.html#200">LXXII-LXXIV</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
