<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghmar75753830s.html#6">Quattro discorsi di
      pittura, scultura, e architettura, che possono servire di
      risposta a quanto scrisse, scrive, e scriverà in biasimo
      della scuola, e de’ maestri veneziana, il Cav. Giousè
      Reynolds</a>
      <ul>
        <li>
          <a href="ghmar75753830s.html#8">A Sua Eccellenza il
          Signor Daniele Farsetti</a>
        </li>
        <li>
          <a href="ghmar75753830s.html#13">L'editore a chi
          legge</a>
        </li>
        <li>
          <a href="ghmar75753830s.html#16">Introduzione</a>
        </li>
        <li>
          <a href="ghmar75753830s.html#29">[Ragionamento I.]</a>
          <ul>
            <li>
              <a href="ghmar75753830s.html#29">Argomento
              primo</a>
            </li>
            <li>
              <a href="ghmar75753830s.html#30">Discorso primo</a>
            </li>
            <li>
              <a href="ghmar75753830s.html#50">Annotazioni
              critiche al primo Ragionamento</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghmar75753830s.html#58">Ragionamento II.</a>
          <ul>
            <li>
              <a href="ghmar75753830s.html#59">Argomento
              secondo</a>
            </li>
            <li>
              <a href="ghmar75753830s.html#60">Discorso
              secondo</a>
            </li>
            <li>
              <a href="ghmar75753830s.html#94">Annotazione al
              secondo Ragionamento</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghmar75753830s.html#106">Ragionamento III.</a>
          <ul>
            <li>
              <a href="ghmar75753830s.html#107">Argomento
              terzo</a>
            </li>
            <li>
              <a href="ghmar75753830s.html#108">Discorso
              terzo</a>
            </li>
            <li>
              <a href="ghmar75753830s.html#151">Annotazioni al
              terzo Ragionamento</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghmar75753830s.html#158">Ragionamento IV.</a>
          <ul>
            <li>
              <a href="ghmar75753830s.html#159">Argomento
              quarto</a>
            </li>
            <li>
              <a href="ghmar75753830s.html#160">Discorso quarto,
              ed ultimo</a>
            </li>
            <li>
              <a href="ghmar75753830s.html#202">Annotazione al
              Ragionamento ultimo&#160;</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
