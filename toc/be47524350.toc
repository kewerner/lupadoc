<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="be47524350s.html#8">Architecture moderne de la
      Sicile, ou Recueil des plus beaux monumens religieux, et des
      édifices publics et particuliers les plus remarquables de la
      Sicile</a>
      <ul>
        <li>
          <a href="be47524350s.html#10">A Monsieur Joseph
          Lecointe</a>
        </li>
        <li>
          <a href="be47524350s.html#12">Préface</a>
        </li>
        <li>
          <a href="be47524350s.html#14">Architecture moderne de la
          Sicile</a>
          <ul>
            <li>
              <a href="be47524350s.html#14">Précis historique sur
              l'architecture moderne de la Sicile et sur l'origine
              de l'arc aigu comme principe de l'architecture
              ogivale&#160;</a>
              <ul>
                <li>
                  <a href="be47524350s.html#35">Appendice</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be47524350s.html#38">Description des
              planches</a>
            </li>
            <li>
              <a href="be47524350s.html#78">Table des matières</a>
            </li>
            <li>
              <a href="be47524350s.html#80">Liste des
              souscripteurs</a>
            </li>
            <li>
              <a href="be47524350s.html#82">[Tables]</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
