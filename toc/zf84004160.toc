<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zf84004160s.html#14">Memoires de Lady Hamilton,
      ambassadrice d’Angleterre à la cour de Naples, ou Choix
      d’anecdotes curieuses sur cette femme célèbre, tirées des
      relations anglaises les plus authentiques</a>
      <ul>
        <li>
          <a href="zf84004160s.html#11">[Ritratto di Lady
          Hamilton] ▣</a>
        </li>
        <li>
          <a href="zf84004160s.html#16">[Prefazione] Préface de
          l'éditeur</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="zf84004160s.html#20">[Text]</a>
      <ul>
        <li>
          <a href="zf84004160s.html#20">I. Naissance et premières
          anées d'Emma</a>
        </li>
        <li>
          <a href="zf84004160s.html#27">II. Arrivée d'Emma dans la
          capitale de l'Empire britannique. - Bassesse de son état.
          - Aurore de ses talens. - Elle est la dupe de son bon
          couer, et, d'erreurs en erreurs, elle tombe dans le
          dernier degré d'abjection et d'avilissement</a>
        </li>
        <li>
          <a href="zf84004160s.html#38">III. Changement arrivé
          dans la destinée d'Emma. - Elle est associée aux
          pratiques d'un charlatan, et devient bientôt après la
          favorite d'un peintre célèbre, auquel elle a servi de
          modèle</a>
        </li>
        <li>
          <a href="zf84004160s.html#42">IV. Précis sur Romney. -
          Son inconduite envers les siens. - Son enthouisiasme pour
          Emma, qu'il représente sous toutes les formes de
          l'histoire et de la fable. - Jugement sur quelques-uns de
          ces tableaux</a>
        </li>
        <li>
          <a href="zf84004160s.html#50">V. Nouvelle phase de la
          vie d'Emma. - Sa liaison avec M. Greville. Manière dont
          elle se termine. - Singulier marché entre un oncle et un
          neveu. - Départ d'Emma pour l'Italie</a>
        </li>
        <li>
          <a href="zf84004160s.html#60">VI. Sir William Hamilton.
          - Caractère de cet ambassadeur. - Ses occupations à
          Naples. - Influence de cette contrée sur Emma. - Elle y
          devient antiquaire par analogie de goûts avec son
          protecteur. - Plusiers personnages eu scène, dont le nom
          a figuré dans le monde</a>
        </li>
        <li>
          <a href="zf84004160s.html#70">VII. Retour ou plutôt
          voyage d'Emma en Angleterre. - Elle y devient lady
          Hamilton. - Mot du roi GeorgesIII à sir William, sur le
          sujet de ce mariage scandaleux. - Réponse concluante du
          baronnet. - Nouveau culte rendu par Romnery à son
          ancienne idole. - Une de ses lettres où est empreinte
          toute sa folie. - Autres extraits de sa correspondance
          sur le même texte</a>
        </li>
        <li>
          <a href="zf84004160s.html#81">VIII. Retour de
          l'ambassadeur anglais à Naples, - Quelques anecdotes sur
          le roi Ferdinand IV et sur lord Bristol. - Mort de ce
          dernier</a>
        </li>
        <li>
          <a href="zf84004160s.html#89">IX. Début de lady Hamilton
          dans la carrière politique. - Lord Nelson envoyé à Naples
          par lord Hood. - Impression que font sur lui les charmes
          de l'ambassadrice. - Affaires de Teulon (1)</a>
        </li>
        <li>
          <a href="zf84004160s.html#97">X. Cour de Naples. - Le
          chevalier Acton. - Embarras de toute l'Europe, à l'époque
          désastreuse de 1794. - Réflexions ressortant de la
          considération de ce sujet. - Calomnies réfutées, ayant
          pour objets de très-hauts personnages. - Sir Williams
          Hamilton justifié du reproche de faire un mauvais emploi
          de son temps</a>
        </li>
        <li>
          <a href="zf84004160s.html#108">XI. L'Italie spoliée des
          chefs-d'oeuvre des arts, par les généraux français. -
          Expédition de Buonaparte en Egypte. - Il preud l'ile de
          Malte en passant. - Lord Nelson, détaché à sa poursuite,
          avec treize vaisseaux, le manque, retourne, et gagne la
          bataille d'Aboukir. - Participation de lady Hamilton à ce
          grand évènement</a>
        </li>
        <li>
          <a href="zf84004160s.html#119">XII. Arrivée de l'escadre
          anglaise à Naples après la bataille d'Aboukir. - Fêtes
          données à Nelson, parmi lesquelles celles de lady
          Hamilton surpassent en éclat toutes les autres. -
          Retraite du ministre de la république française. -
          Artifices de lady Hamilton pour capter le vainqueur du
          Nil. - Puérilités rapportées comme des prodiges. -
          Lettres de lord Nelson. - Réflexions qu'elles
          suggèrent</a>
        </li>
        <li>
          <a href="zf84004160s.html#136">XIII. Fête &#160;donnée
          par lord Nelson à bord du vaisseau le Van-Guard. -
          Dégoûts qu'il éprouve à l'inspection de ses propres
          préparatifs. - Politique de l'Europe. - Le jeune
          Nesbitt</a>
        </li>
        <li>
          <a href="zf84004160s.html#144">XIV. Irruption des
          Français dans le midi de l'Italie. - Départ de la famille
          royale de Naples avec toutes ses circonstances
          remarquables. - Son arrivée à Palerme, décrite par la
          plume de lord Nelson lui-même</a>
        </li>
        <li>
          <a href="zf84004160s.html#155">XV. Profusions qu'on
          essaye de réparer, comme c'est d'usage, par des
          injustices. - Beau mot de Ferdinand IV. - Il comble
          Nelson d'honneurs. - Triomphe des Français à Naples, où
          ils sont servis par la trahison de plusieurs chefs. - Les
          eigneur sont pour eux, le peuple est pour le roi</a>
        </li>
        <li>
          <a href="zf84004160s.html#162">XVI. Destruction rapide
          de la république Parthénope, qui ramène les Anglais à
          Naples. - Transactions délicates, où lady Hamilton n'a
          que trop part. - Le prince Francisco de CDarracioli</a>
        </li>
        <li>
          <a href="zf84004160s.html#168">XVII. Courte notice sur
          le prince Francisco de Caracioli. - Il est jugé et
          condamné comme coupable de lèse-majesté ou de
          haute-trahison. - Observations sur la justice de sa
          sentence. - Conduite de lord Nelson à son suejt. -
          Barbarie de lady Hamilton. - Son corps venant demander
          pour lui, après sa mort, la sépulture, qui lui avait été
          refusée. - Supplice du médicin Cyrillo. - Mauvaise
          apologie de lady Hamilton sur ces actes cruels auxquels
          elle a assité. - Dissertation sur la politique de ces
          temps-là et de ceux où nous vivons</a>
        </li>
        <li>
          <a href="zf84004160s.html#188">XVIII. Lord Nelson
          reconduit le roi Deux-Siciles de Naples à Palerme. - Vie
          indigne de lui qu'il mène dans cette dernière cite. -
          Représentations franches et hardies du capitaine
          Troubridge à son amiral</a>
        </li>
        <li>
          <a href="zf84004160s.html#196">XIX. Persévérance de lord
          Nelson dans la même conduite. - Il est rappelé par
          l'amirauté anglaise. - Sir William est également obligé
          de faire place à un successeur. - La reine Caroline les
          accompagne à Vienne. - Anecdotes sur ce long voyage</a>
        </li>
        <li>
          <a href="zf84004160s.html#209">XX. Lord Nelson n'ayant
          point reçu de lettres de l'amirauté, à laquelle il avait
          demandé une frégate, s'embarque dans un paquebot avec sir
          Williams et lady Hamilton. - Effet de son premier séjour
          à Londres sur l'opinion publique. - Il se sépare de sa
          femme, pour se vouer entièrement à sa maîtresse</a>
        </li>
        <li>
          <a href="zf84004160s.html#224">XXI. Naissance de la
          petite Horatia. - Acquisition de Merton-Place, par lord
          Nelson. - Vie que l'on menait dans cette résidence
          champêtre. - Nouveaux évènemens en France, où Buonaparte
          se fait illégalement élire empereur</a>
        </li>
        <li>
          <a href="zf84004160s.html#246">XXII. Mort du chevalier
          Hamilton. - Son testament. - Conduite de ses héritiers
          envers sa veuve. Manière que prend celle-ci pour se
          venger de feu sin mari. - Lettre citée. - Naissance d'un
          second enfant. - Courte durée de sa vie. - Inquiétudes de
          lord Nelson sur sa première fille. - Moyens qu'il avise
          pour la mettre à l'abri des extravagances de la mère</a>
        </li>
        <li>
          <a href="zf84004160s.html#277">XXIII. Retour de lord
          Nelson en Agleterre, apès sa vaine pousuite des flottes
          française et espagnole. - Il ne voit point sa femme. -
          Ruses et perfidies ordinaires de lady Hamilton, mises au
          jour sur le sujet de sa fille. - Nelson gagne la bataille
          de Trafalgar, et demeure enseveli dans son trophée. - Les
          dernières volontés, ou prières adressées à son pays, en
          faveur de lady Hamilton. - Leur inefficacité. - Turpitude
          du lord Nelson actuel</a>
        </li>
        <li>
          <a href="zf84004160s.html#296">XXIV. Dernières
          apparitions de lady Hamilton sur la scène du monde. -
          Ingratitude qu'elle éprouve. - Triste choix de ses
          sociétées. - Excès de ses malheurs et sa fiu
          chrétienne</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="zf84004160s.html#307">Tables des chapitres</a>
    </li>
  </ul>
  <hr />
</body>
</html>
