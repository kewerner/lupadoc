<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502690s.html#6">Le cose ma ravigliose dell'alma
      città di Roma</a>
      <ul>
        <li>
          <a href="dg4502690s.html#8">Le sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg4502690s.html#8">La prima Chiesa è ›San
              Giovanni in Laterano‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#10">[Scala Santa] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#17">La seconda Chiesa è
              ›San Pietro in Vaticano‹</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#14">[›San Pietro in
                  Vaticano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502690s.html#15">[›San Pietro in
                  Vaticano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502690s.html#20">San Pietro in
                  Vaticano: Baldacchino di San Pietro</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#23">La terza Chiesa &#160;è
              ›San Paolo fuori le Mura‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#25">[›Piramide di Caio
                  Cestio‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#25">La quarta Chiesa è
              ›Santa Maria Maggiore‹</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#26">[›Santa Maria
                  Maggiore‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#28">La quarta (sic!) Chiesa
              é ›San Lorenzo fuori le Mura‹</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#29">›San Lorenzo fuori
                  le Mura‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#29">La Sesta Chiesa é ›San
              Sebastiano‹</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#30">›San Sebastiano‹
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#31">La Settima Chiesa é
              ›Santa Croce in Gerusalemme‹ ▣</a>
            </li>
            <li>
              <a href="dg4502690s.html#32">Dell'›Isola
              Tiberina‹</a>
            </li>
            <li>
              <a href="dg4502690s.html#33">In ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502690s.html#37">›Borgo‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502690s.html#39">Della Porta Flaminia overo
          dal Popolo ›Porta del Popolo‹ fino alle radici del
          ›Campidoglio‹</a>
          <ul>
            <li>
              <a href="dg4502690s.html#45">[›Colonna Traiana‹]
              ▣</a>
            </li>
            <li>
              <a href="dg4502690s.html#48">[›Colonna di Antonino
              Pio‹] ▣</a>
            </li>
            <li>
              <a href="dg4502690s.html#52">›San Carlo ai Catinari‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502690s.html#55">Dal ›Campidoglio‹ a man
          sinistra verso i Monti</a>
          <ul>
            <li>
              <a href="dg4502690s.html#56">[›Foro Romano‹] ▣</a>
            </li>
            <li>
              <a href="dg4502690s.html#59">[›Trofei di Mario‹]
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502690s.html#62">Dal ›Campidoglio‹ a man
          dritta verso il Trastevere</a>
          <ul>
            <li>
              <a href="dg4502690s.html#62">[›Palatino‹] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502690s.html#67">Stationi delle Chiese di
          Roma</a>
          <ul>
            <li>
              <a href="dg4502690s.html#73">Le Stationi
              dell'Avvento</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502690s.html#74">Guida romana per li forastieri
      che vogliono vedere l'Antichità di Roma una per una</a>
      <ul>
        <li>
          <a href="dg4502690s.html#74">Del ›Borgo‹ prima
          giornata</a>
          <ul>
            <li>
              <a href="dg4502690s.html#75">Del ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502690s.html#76">Dell'Isola Tiberina‹, e
              Licaonia ▣</a>
            </li>
            <li>
              <a href="dg4502690s.html#77">Del Ponte di Santa
              Maria ›Ponte Rotto‹, del Palazzo di Pilato ›Casa dei
              Crescenzi‹, et altre cose</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#77">[›Tempio di
                  Portunus‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502690s.html#77">[›Santa Maria in
                  Cosmedin‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502690s.html#77">[›Tempio di
                  Hercules Olivarius‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#78">Del Monte Testaccio
              ›Testaccio (monte)‹, et altre cose</a>
            </li>
            <li>
              <a href="dg4502690s.html#79">Delle Terme Antoniane,
              et altre cose ›Terme di Caracalla‹</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#79">›Arco di Giano‹
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#79">Di ›San Giovanni in
              Laterano‹, ›Santa Croce in Gerusalemme‹, et altre
              cose</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#80">[›Anfiteatro
                  Castrense‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#74">[›Castel Sant'Angelo‹]
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502690s.html#80">Gioranta seconda</a>
          <ul>
            <li>
              <a href="dg4502690s.html#80">Della ›Porta del
              Popolo‹</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#81">[›Porta Maggiore‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502690s.html#81">[›Piazza del
                  Popolo‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#82">Del Monte Cavallo,
              detto Quirinale, e de i Cavalli ›Fontana di Monte
              Cavallo‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#82">›Piazza del
                  Quirinale‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502690s.html#82">›Palazzo del
                  Quirinale‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502690s.html#83">[›Torre Mesa‹]
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#83">Della Strada Pia ›Via
              XX Settembre‹, e della Vigna, ch'era già del Cardinal
              di Ferrara</a>
            </li>
            <li>
              <a href="dg4502690s.html#84">Della Porta Pia di
              Sant'Agnese ›Porta Nomentana‹ ▣, et altre
              anticaglie</a>
            </li>
            <li>
              <a href="dg4502690s.html#85">Delle Terme Diocletiane
              ›Terme di Diocleziano‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#86">[›Terme di
                  Diocleziano‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502690s.html#87">[›Colosseo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502690s.html#88">[›Arco di
                  Costantino‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#90">Del Monte ›Palatino‹,
              hoggi detto Palazzo Maggiore, del Tempio della Pace
              ›Foro della Pace‹, et altre cose</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#89">[›Palatino‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#91">Del ›Foro di Nerva‹</a>
              <ul>
                <li>
                  <a href="dg4502690s.html#90">[›Foro di Nerva‹]
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502690s.html#91">Dell'Arco Trionfale di
              Settimio Severo ›Arco di Settimio Severo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502690s.html#92">[›Campidoglio‹] ▣</a>
            </li>
            <li>
              <a href="dg4502690s.html#93">De' Portici d'Ottavia
              ›Portico di Ottavia‹, di Settimio ›Porticus Severi‹,
              e ›Teatro di Pompeo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502690s.html#93">Giornata terza</a>
          <ul>
            <li>
              <a href="dg4502690s.html#93">Delle Colonne una
              d'Antonio Pio ›Colonna di Antonino Pio‹, l'altra di
              Traiano ›Colonna Traiana‹</a>
            </li>
            <li>
              <a href="dg4502690s.html#94">Della Rotonda, overo
              ›Pantheon‹ ▣</a>
            </li>
            <li>
              <a href="dg4502690s.html#95">De Bagni d'Agrippa
              &#160;›Terme di Agrippa‹, e di Nerone ›Terme
              Neroniano-Alessandrine‹ ▣</a>
            </li>
            <li>
              <a href="dg4502690s.html#96">Della ›Piazza Navona‹
              ▣. e Pasquino</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502690s.html#96">[Indici]</a>
          <ul>
            <li>
              <a href="dg4502690s.html#96">Indice de' sommi
              Pontefici Romani</a>
            </li>
            <li>
              <a href="dg4502690s.html#108">Reges, et Imperatores
              Romani</a>
            </li>
            <li>
              <a href="dg4502690s.html#111">Li Re di Francia</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502690s.html#113">Le sette maraviglie del
      mondo</a>
      <ul>
        <li>
          <a href="dg4502690s.html#113">I. Delle mura di Babilonia
          ▣</a>
        </li>
        <li>
          <a href="dg4502690s.html#114">II. Della torre di Faros
          ▣</a>
        </li>
        <li>
          <a href="dg4502690s.html#115">III. Della Statua di Giove
          ▣</a>
        </li>
        <li>
          <a href="dg4502690s.html#116">IV. Del Colosso di Rodi
          ▣</a>
        </li>
        <li>
          <a href="dg4502690s.html#117">V. Del Tempio di Diana
          ▣</a>
        </li>
        <li>
          <a href="dg4502690s.html#118">VI. Del Mausoleo
          d'Artemisia ▣</a>
        </li>
        <li>
          <a href="dg4502690s.html#119">VII. Delle Piramidi
          d'Egitto ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502690s.html#120">Le principali Poste
      d'Italia</a>
    </li>
  </ul>
  <hr />
</body>
</html>
