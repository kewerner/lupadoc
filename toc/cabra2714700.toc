<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cabra2714700s.html#8">I capi d’arte di Bramante da
      Urbino nel Milanese</a>
      <ul>
        <li>
          <a href="cabra2714700s.html#10">Prefazione</a>
        </li>
        <li>
          <a href="cabra2714700s.html#16">I.</a>
        </li>
        <li>
          <a href="cabra2714700s.html#21">II.</a>
        </li>
        <li>
          <a href="cabra2714700s.html#25">III.</a>
        </li>
        <li>
          <a href="cabra2714700s.html#29">IV.</a>
        </li>
        <li>
          <a href="cabra2714700s.html#43">V.</a>
        </li>
        <li>
          <a href="cabra2714700s.html#54">VI.</a>
        </li>
        <li>
          <a href="cabra2714700s.html#73">VII.</a>
        </li>
        <li>
          <a href="cabra2714700s.html#87">VIII.</a>
        </li>
        <li>
          <a href="cabra2714700s.html#98">Documenti</a>
          <ul>
            <li>
              <a href="cabra2714700s.html#100">Documenti
              spettanti alla chiesa di Santa Maria presso San
              Satiro</a>
            </li>
            <li>
              <a href="cabra2714700s.html#105">Documenti per la
              chiesa di Santa Maria presso San Celso</a>
            </li>
            <li>
              <a href="cabra2714700s.html#115">Documenti per la
              chiesa di Sant'Ambrogio</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
