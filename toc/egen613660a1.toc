<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="egen613660a1s.html#10">Instruzione di quanto può
      vedersi di più bello in Genova in pittura, scultura, ed
      architettura ecc; tomo I.</a>
      <ul>
        <li>
          <a href="egen613660a1s.html#12">A sua eccelenza il
          Signor Girolamo Durazzo</a>
        </li>
        <li>
          <a href="egen613660a1s.html#16">L'autore a chi
          legge</a>
        </li>
        <li>
          <a href="egen613660a1s.html#21">Sonetto</a>
        </li>
        <li>
          <a href="egen613660a1s.html#22">Origine, e progressi
          della città di Genova</a>
        </li>
        <li>
          <a href="egen613660a1s.html#59">Situazione della
          città di Genova</a>
        </li>
        <li>
          <a href="egen613660a1s.html#66">Prima giornata</a>
        </li>
        <li>
          <a href="egen613660a1s.html#136">Seconda giornata</a>
        </li>
        <li>
          <a href="egen613660a1s.html#276">Terza giornata</a>
        </li>
        <li>
          <a href="egen613660a1s.html#376">Quarta giornata</a>
        </li>
        <li>
          <a href="egen613660a1s.html#415">Del Sobborgo
          d'Albaro e suo contorno</a>
        </li>
        <li>
          <a href="egen613660a1s.html#429">Del Sobborgo di
          Sampierdarena&#160;</a>
        </li>
        <li>
          <a href="egen613660a1s.html#437">Tavola delle
          Materie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
