<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501760s.html#8">Le cose meravigliose dell'alma
      città di Roma</a>
      <ul>
        <li>
          <a href="dg4501760s.html#10">Delle sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg4501760s.html#10">[›San Giovanni in
              Laterano‹] ▣</a>
            </li>
            <li>
              <a href="dg4501760s.html#13">Seconda Chiesa di ›San
              Pietro in Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501760s.html#15">La terza Chiesa si è
              ›San Paolo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4501760s.html#16">La quarta Chiesa è
              ›Santa Maria Maggiore‹ ▣</a>
            </li>
            <li>
              <a href="dg4501760s.html#17">Santo Lorenzo fuora
              delle mura, si è la quinta chiesa ›San Lorenzo fuori
              le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4501760s.html#18">La Sesta Chiesa é ›San
              Sebastiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501760s.html#18">La Settima Chiesa é
              ›Santa Croce in Gerusalemme‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501760s.html#19">Nell'›Isola Tiberina‹</a>
        </li>
        <li>
          <a href="dg4501760s.html#20">In ›Trastevere‹</a>
        </li>
        <li>
          <a href="dg4501760s.html#22">Nel ›Borgo‹</a>
        </li>
        <li>
          <a href="dg4501760s.html#23">Della Porta Flaminia fuori
          dal Popolo ›Porta del Popolo‹ fino alle radici del
          ›Campidoglio‹</a>
        </li>
        <li>
          <a href="dg4501760s.html#33">Dal ›Campidoglio‹ a mani
          sinistra verso li Monti</a>
        </li>
        <li>
          <a href="dg4501760s.html#38">Dal ›Campidoglio‹ a man
          dritta verso li Monti</a>
        </li>
        <li>
          <a href="dg4501760s.html#43">Le stationi che sono nelle
          chiese di Roma [...]</a>
          <ul>
            <li>
              <a href="dg4501760s.html#49">Le Stationi
              dell'Advento</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501760s.html#52">Trattato over modo
          d'acquistar l'indulgentie alle Stationi</a>
        </li>
        <li>
          <a href="dg4501760s.html#56">La Guida Romana per tutti i
          &#160;forastieri, che vengano per vedere le antichità di
          Roma, a una per una, in bellissima forma, et brevità</a>
          <ul>
            <li>
              <a href="dg4501760s.html#56">[Giornata prima]</a>
              <ul>
                <li>
                  <a href="dg4501760s.html#56">Del ›Borgo‹ la
                  prima giornata</a>
                </li>
                <li>
                  <a href="dg4501760s.html#57">Del
                  ›Trastevere‹</a>
                </li>
                <li>
                  <a href="dg4501760s.html#57">Dell'Isola
                  Tiberina‹</a>
                </li>
                <li>
                  <a href="dg4501760s.html#58">Del Ponte Santa
                  Maria ›Ponte Rotto‹, Palazzo di pilato ›Casa dei
                  Crescenzi‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501760s.html#58">Del Monte Testaccio
                  ›Testaccio (Monte)‹ e di molte altre cose</a>
                </li>
                <li>
                  <a href="dg4501760s.html#59">Delle Therme
                  Antoniane ›Terme di Caracalla‹ , ed altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4501760s.html#59">Di ›San Giovanni in
                  Laterano‹, et altre cose</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501760s.html#59">Gioranta seconda</a>
              <ul>
                <li>
                  <a href="dg4501760s.html#60">Della ›Porta del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4501760s.html#60">De i Cavalli di
                  marmo che stanno a monte Cavallo ›Quirinale‹ , et
                  delle Therme Diocletiane ›Terme di
                  Diocleziano‹</a>
                </li>
                <li>
                  <a href="dg4501760s.html#61">Della strada Pia
                  ›Via XX Settembre‹</a>
                </li>
                <li>
                  <a href="dg4501760s.html#61">Della Vigna del
                  Cardinal di Ferrara</a>
                </li>
                <li>
                  <a href="dg4501760s.html#61">Della Vigna del
                  Cardinal di Carpi, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501760s.html#61">Della ›Porta
                  Pia‹</a>
                </li>
                <li>
                  <a href="dg4501760s.html#61">Di ›Sant'Agnese
                  fuori le Mura‹, et altre anticaglie</a>
                </li>
                <li>
                  <a href="dg4501760s.html#62">Del Tempio di
                  Iside, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501760s.html#62">Delle ›Sette Sale‹,
                  et del ›Colosseo‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501760s.html#63">Del tempio della
                  Pace ›Foro della Pace‹, et del Monte Palatino,
                  hora detto Palazzo Maggiore ›Palatino‹, et altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4501760s.html#63">Del ›Campidoglio‹,
                  et altre cose</a>
                </li>
                <li>
                  <a href="dg4501760s.html#64">De i portichi di
                  Ottavia ›Portico d'Ottavia‹, et di Settimio
                  ›Porticus Severi‹, et del ›Teatro di Pompeo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501760s.html#64">Giornata terza</a>
              <ul>
                <li>
                  <a href="dg4501760s.html#64">Delle due Colonne
                  una di Antonio Pio, et l'altra di Traiano, et
                  altre cose ›Colonna di Antonino Pio‹ ›Colonna
                  Traiana‹</a>
                </li>
                <li>
                  <a href="dg4501760s.html#65">Della Rotonda,
                  overo ›Pantheon‹</a>
                </li>
                <li>
                  <a href="dg4501760s.html#65">Dei Bagni di
                  Agrippa, et di Nerone ›Terme di Agrippa‹ ›Terme
                  Neroniano-Alessandrine‹</a>
                </li>
                <li>
                  <a href="dg4501760s.html#65">Della piazza
                  Navona, et di mastro Pasquino ›Piazza Navona‹
                  ›Statua di Pasquino‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501760s.html#66">[Tavole]</a>
              <ul>
                <li>
                  <a href="dg4501760s.html#66">Tavola delle
                  Chiese</a>
                </li>
                <li>
                  <a href="dg4501760s.html#68">Summi
                  Pontifices</a>
                </li>
                <li>
                  <a href="dg4501760s.html#87">Reges et
                  Imperatores Romani</a>
                </li>
                <li>
                  <a href="dg4501760s.html#91">Li Re di
                  Francia</a>
                </li>
                <li>
                  <a href="dg4501760s.html#92">Li Re del Regno di
                  Napoli et di Sicilia, il quali cominciorno a
                  regnare l'anno di nostra salute</a>
                </li>
                <li>
                  <a href="dg4501760s.html#92">Li Dogi di
                  Vinegia</a>
                </li>
                <li>
                  <a href="dg4501760s.html#95">Li Duchi di
                  Milano</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501760s.html#96">L'antichità di Roma</a>
      <ul>
        <li>
          <a href="dg4501760s.html#98">Alli lettori</a>
        </li>
        <li>
          <a href="dg4501760s.html#99">Tavola</a>
        </li>
        <li>
          <a href="dg4501760s.html#101">Libro I</a>
          <ul>
            <li>
              <a href="dg4501760s.html#101">Dell'edificatione di
              Roma</a>
            </li>
            <li>
              <a href="dg4501760s.html#103">Del Circuito di
              Roma</a>
            </li>
            <li>
              <a href="dg4501760s.html#104">Delle Porte</a>
            </li>
            <li>
              <a href="dg4501760s.html#105">Delle vie</a>
            </li>
            <li>
              <a href="dg4501760s.html#106">Delli Ponti che sono
              sopra il Tevere, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501760s.html#107">Dell'Isola del Tevere
              ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#107">Delli Monti</a>
            </li>
            <li>
              <a href="dg4501760s.html#108">Del Monte Testaccio
              ›Testaccio (Monte)‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#109">Delle Acque, et chi le
              condusse in Roma</a>
            </li>
            <li>
              <a href="dg4501760s.html#110">Della ›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#110">Delli Acquedotti</a>
            </li>
            <li>
              <a href="dg4501760s.html#111">Delle ›Sette Sale‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#111">Delle Therme cioè
              Bagni et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501760s.html#112">Delle Naumachie, dove
              si facevano le battaglie navali, et che cose
              erano</a>
            </li>
            <li>
              <a href="dg4501760s.html#112">De' cerchi, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501760s.html#113">De' Theatri, et che
              cosa erano, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501760s.html#113">Delli Anfiteatri, et
              suoi edificatori, et che cosa erano</a>
            </li>
            <li>
              <a href="dg4501760s.html#114">De' Fori, cioè
              Piazze</a>
            </li>
            <li>
              <a href="dg4501760s.html#115">Delli Archi Trionfali,
              et a chi si davano</a>
            </li>
            <li>
              <a href="dg4501760s.html#115">De' Portichi</a>
            </li>
            <li>
              <a href="dg4501760s.html#116">De' Trofei, et Colonne
              memorande</a>
            </li>
            <li>
              <a href="dg4501760s.html#116">De' Colossi</a>
            </li>
            <li>
              <a href="dg4501760s.html#117">Delle Piramidi</a>
            </li>
            <li>
              <a href="dg4501760s.html#117">Delle Mete</a>
            </li>
            <li>
              <a href="dg4501760s.html#117">Delli Obelischi, over
              Aguglie</a>
            </li>
            <li>
              <a href="dg4501760s.html#118">Delle Statue</a>
            </li>
            <li>
              <a href="dg4501760s.html#118">›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#118">De Cavalli ›Fontana di
              Monte Cavallo‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#118">Delle Librarie</a>
            </li>
            <li>
              <a href="dg4501760s.html#118">Delli Horiuoli</a>
            </li>
            <li>
              <a href="dg4501760s.html#119">De' Palazzi</a>
            </li>
            <li>
              <a href="dg4501760s.html#119">Della Casa Aurea di
              Nerone ›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#120">Dell'altre case de'
              Cittadini</a>
            </li>
            <li>
              <a href="dg4501760s.html#120">Delle Curie, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501760s.html#121">De' Senatuli, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501760s.html#121">De' Magistrati</a>
            </li>
            <li>
              <a href="dg4501760s.html#122">De Comitij, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501760s.html#122">Delle Tribu</a>
            </li>
            <li>
              <a href="dg4501760s.html#122">Delle Regioni, cioè
              Rioni, et sue insegne</a>
            </li>
            <li>
              <a href="dg4501760s.html#122">Delle Basiliche, et
              che cosa erano</a>
            </li>
            <li>
              <a href="dg4501760s.html#123">Del ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#124">Dello Erario
              ›Aerarium‹, cioè Camera del commune, et che moneta si
              spendeva in Roma in que' tempi</a>
            </li>
            <li>
              <a href="dg4501760s.html#124">Del Gregostasi, et che
              cosa era ›Grecostasi‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#125">Della Secretaria del
              Popolo Romano ›Secretarium Senatus‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#125">Dell'›Asilo di
              Romolo‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#125">Delle Rostre, et che
              cosa erano ›Rostri‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#125">Della Colonna detta
              Miliario ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#125">Del Tempio di Carmenta
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#125">Della colonna Bellica
              ›Columna Bellica‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#126">Della Colonna Lattaria
              ›Columna Lactaria‹</a>
            </li>
            <li>
              <a href=
              "dg4501760s.html#126">Dell'›Aequimaelium‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#126">Del ›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#126">Del Tigillo Sororio
              ›Tigillum Sororium‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#126">De' Campi Forastieri
              ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#126">Della ›Villa
              Publica‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#127">Della ›Taberna
              Meritoria‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#127">Del ›Vivarium‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#127">Degli Horti</a>
            </li>
            <li>
              <a href="dg4501760s.html#128">Del ›Velabro‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#128">Delle ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#128">Delli Clivi</a>
            </li>
            <li>
              <a href="dg4501760s.html#129">De' Frati (sic!)</a>
            </li>
            <li>
              <a href="dg4501760s.html#129">De Granari Publici, et
              magazini del Sale</a>
            </li>
            <li>
              <a href="dg4501760s.html#129">Delle Carceri
              publiche</a>
            </li>
            <li>
              <a href="dg4501760s.html#129">Di alcune feste, et
              giochi che si solevano celebrare in Roma</a>
            </li>
            <li>
              <a href="dg4501760s.html#130">Del Sepolchro di
              Augusto, d'Adriano, et di Settimio</a>
            </li>
            <li>
              <a href="dg4501760s.html#131">De Tempij</a>
            </li>
            <li>
              <a href="dg4501760s.html#132">De' Sacerdoti delle
              Vergini Vestali, vestimenti, vasi, et altri
              instrumenti fatti per uso delli sacrificij, et suoi
              institutori</a>
            </li>
            <li>
              <a href="dg4501760s.html#134">Dell'Armamentario et
              che cosa era ›Armamentaria‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#134">Dell'Esercito Romano
              di terra, e mare e loro insegne</a>
            </li>
            <li>
              <a href="dg4501760s.html#134">De' Trionfi et a chi
              si concedevano, et chi fu il primo trionfatore, et di
              quante maniere erano</a>
            </li>
            <li>
              <a href="dg4501760s.html#135">Delle corone, et a chi
              si davano</a>
            </li>
            <li>
              <a href="dg4501760s.html#135">Del numero del Popolo
              Romano</a>
            </li>
            <li>
              <a href="dg4501760s.html#135">Delle ricchezze del
              Popolo Romano</a>
            </li>
            <li>
              <a href="dg4501760s.html#136">Della liberalità degli
              antichi Romani</a>
            </li>
            <li>
              <a href="dg4501760s.html#136">Delli Matrimonij
              antichi, et loro usanza</a>
            </li>
            <li>
              <a href="dg4501760s.html#137">Della buona creanza,
              che davano a i figliuoli</a>
            </li>
            <li>
              <a href="dg4501760s.html#137">Della separatione de
              Matrimonij</a>
            </li>
            <li>
              <a href="dg4501760s.html#138">Dell'essequie antiche,
              et sue ceremonie</a>
            </li>
            <li>
              <a href="dg4501760s.html#139">Delle Torri</a>
            </li>
            <li>
              <a href="dg4501760s.html#139">Del ›Tevere‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#139">Del Palazzo Papale
              ›Palazzo Apostolico Vaticano‹, et di Belvedere
              ›Cortile del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#140">Del ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4501760s.html#140">Recapitulatione
              dell'antichità</a>
            </li>
            <li>
              <a href="dg4501760s.html#141">De' Tempij degli
              Antichi fuori di Roma</a>
            </li>
            <li>
              <a href="dg4501760s.html#143">Quante volte è stata
              presa Roma</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
