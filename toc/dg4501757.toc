<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501757s.html#6">M. Attilii Serrani De Septem
      Vrbis Ecclesiis</a>
      <ul>
        <li>
          <a href="dg4501757s.html#8">S. D. N. Gregorio XIII
          Pontifex Maximus M. Attilius Serranus</a>
        </li>
        <li>
          <a href="dg4501757s.html#12">Auctores ecclesiastici;
          praeter sacrosancta Concilia, et Summorum Pontificum
          epistolas, ex quorum fontibus haec hausimus</a>
        </li>
        <li>
          <a href="dg4501757s.html#14">De septem urbis
          ecclesiis</a>
          <ul>
            <li>
              <a href="dg4501757s.html#16">De Ecclesia S. Pietro
              in Vaticano</a>
            </li>
            <li>
              <a href="dg4501757s.html#45">De ecclesa Sancti Pauli
              via Ostiensi</a>
              <ul>
                <li>
                  <a href="dg4501757s.html#52">De ecclesia
                  sanctorum Vincentij, et Anastasii ad tres fontes,
                  sive ad aquas salvias</a>
                </li>
                <li>
                  <a href="dg4501757s.html#56">De ecclesia Sancta
                  Mariae Annuntiata&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501757s.html#56">De ecclesia Sancti
              Sebastiani, via Appia</a>
              <ul>
                <li>
                  <a href="dg4501757s.html#62">De Oratio, Domine
                  quo vadis? via Appia</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501757s.html#65">De sacrosancta divi
              Ioannis Lateranensi basilica in monte Carlo</a>
            </li>
            <li>
              <a href="dg4501757s.html#89">De ecclesia Sanctae
              Crucis in Hierusalem</a>
            </li>
            <li>
              <a href="dg4501757s.html#98">De ecclesia Sancti
              Laurentij extra urbis maenia, in agro Verano</a>
            </li>
            <li>
              <a href="dg4501757s.html#108">De ecclesia Sanctae
              Mariae Maioris</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501757s.html#126">Reliquiea septem urbis
          ecclesiarum</a>
        </li>
        <li>
          <a href="dg4501757s.html#142">Stationes et indulgentiae
          septem urbis ecclesiarum</a>
        </li>
        <li>
          <a href="dg4501757s.html#153">Index rerum et
          sententiarum</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
