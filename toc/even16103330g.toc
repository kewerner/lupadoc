<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="even16103330gs.html#8">Della pittura
      veneziana</a>
      <ul>
        <li>
          <a href="even16103330gs.html#8">Tomo primo</a>
          <ul>
            <li>
              <a href="even16103330gs.html#10">Lo stampatore a
              chi legge</a>
            </li>
            <li>
              <a href="even16103330gs.html#15">Noi Reformatori
              dello studio di Padova</a>
            </li>
            <li>
              <a href="even16103330gs.html#16">Sestiere di San
              Marco</a>
            </li>
            <li>
              <a href="even16103330gs.html#137">Sestiere di
              Castello</a>
            </li>
            <li>
              <a href="even16103330gs.html#200">Sestiere di San
              Paolo</a>
            </li>
            <li>
              <a href="even16103330gs.html#250">Tavola delli
              sestieri contenuti in questo primo tomo&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="even16103330gs.html#264">Tomo secondo</a>
          <ul>
            <li>
              <a href="even16103330gs.html#266">Sestiere di
              Dorso Duro</a>
            </li>
            <li>
              <a href="even16103330gs.html#333">Sestiere di
              Canareggio</a>
            </li>
            <li>
              <a href="even16103330gs.html#383">Sestiere della
              Croce</a>
            </li>
            <li>
              <a href="even16103330gs.html#433">Tavola delli
              sestieri</a>
            </li>
            <li>
              <a href="even16103330gs.html#440">Istoria della
              pittura veneziana</a>
            </li>
            <li>
              <a href="even16103330gs.html#452">Compendio</a>
            </li>
            <li>
              <a href="even16103330gs.html#497">Avviso a'
              leggitori</a>
            </li>
            <li>
              <a href="even16103330gs.html#499">Tavola de'
              cognomi, nomi, e patrie</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
