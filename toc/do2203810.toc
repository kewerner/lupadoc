<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="do2203810s.html#8">[Sammlung von Stichen nach
      Grabfiguren des 17. Jahrhunderts in römischen Kirchen:
      Ludovica Albertoni von Giovanni Lorenzo Bernini, etc.]</a>
      <ul>
        <li>
          <a href="do2203810s.html#8">B. Ludovica Albertoni.
          Statua del Cavalier Bernini in Roma nella Chiesa di ›San
          Francesco a Ripa‹ ▣</a>
        </li>
        <li>
          <a href="do2203810s.html#10">Santa Martina Statua di
          Nicolò Menghino [›Santi Luca e Martina‹] ▣</a>
        </li>
        <li>
          <a href="do2203810s.html#12">Santa Anastasia Statua di
          Francesco Aprile [›Sant'Anastasia‹] ▣</a>
        </li>
        <li>
          <a href="do2203810s.html#14">Santa Cecilia Statua di
          Stefano Maderno [›Santa Cecilia in Trastevere‹] ▣</a>
        </li>
        <li>
          <a href="do2203810s.html#16">Sant'Alessio C. Statua di
          Andrea Bergondi [›Sant'Alessio‹] ▣</a>
        </li>
        <li>
          <a href="do2203810s.html#18">Santo Stanislao Kostka
          Statua di Pietro Legros nella Casa della Missione in
          Sant'Andrea a Monte Cavallo [›Sant'Andrea al Quirinale‹]
          ▣</a>
        </li>
        <li>
          <a href="do2203810s.html#20">Santo Pio Quinto
          Bassorilievo di Pietro Legros in Roma nella Chiesa di
          ›Santa Maria Maggiore‹ ▣</a>
        </li>
        <li>
          <a href="do2203810s.html#22">Santo Sebastiano M. Statua
          di Antonio Giorgetti nella Chiesa die detto Santo fuori
          le Mura di Roma [›San Sebastiano‹] ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
