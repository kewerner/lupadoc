<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg45043801s.html#10">Roma nell'anno
      MDCCCXXXVIII</a>
      <ul>
        <li>
          <a href="dg45043801s.html#12">A sua Eccellenza il
          Signor Paolo di Dèmidoff</a>
        </li>
        <li>
          <a href="dg45043801s.html#16">Prefazione</a>
        </li>
        <li>
          <a href="dg45043801s.html#25">Tavola I.</a>
          <ul>
            <li>
              <a href="dg45043801s.html#25">Porta Flaminia ›Porta
              del Popolo‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Pinciana‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Salaria‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Pia‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Nomentana‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Chiusa‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Tiburtina‹ ◉,
              o San Lorenzo</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Collatina‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Maggiore‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta San Giovanni‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Asinaria‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Metroni (sic!)
              ›Porta Metronia‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Latina‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Appia o ›Porta
              San Sebastiano‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Ardeatina‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Ruduscula
              ›Porta Raudusculana‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Nevia ›Porta
              Naevia‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Ostiense ›Porta
              San Paolo‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Lavernale
              ›Porta Lavernalis‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Navale ›Porta
              Portuensis‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Minucia‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Portese ›Porta
              Portuensis‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Trigemina
              ›Porta Trigemina‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Gianicolense
              ›Porta San Pancrazio‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Flumentana‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Trionfale‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Carmentale
              ›Porta Carmentalis‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Radumena ›Porta
              Ratumena‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Catularia‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Sanquiale
              ›Porta Sanqualis‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Salutare ›Porta
              Salutaris‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Piaculare
              ›Porta Piacularis‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Collina‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Viminale ›Porta
              Viminalis‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Esquilina ›Arco
              di Gallieno‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Muzia‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Querquetulana‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Celimontana
              ›Porta Caelimontana‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Fontinale
              ›Porta Fontinalis‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Ferentina
              ›Porta Latina‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Capena‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta San Pancrazio‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Castello ›Porta
              Castello‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Angelica‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Pertusa‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Porta Fabbrica‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Porta Cavalleggieri
              ›Porta Cavalleggeri‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Castra Praetoria‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Campo Viminale
              ›Campus Viminalis‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Campo Esquilino
              ›Campus Esquilinus‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Cispio ›Cespeus,
              Cespius, Cispius mons‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Oppius ›Oppio‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Viminale‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Quirinale‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Subura ›Suburra‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Carine ›Carinae‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Celio‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Vico Patrizio ›Vicus
              Patricius‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Vico Ciprio ›Vicus
              Ciprius‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Via Sacra‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Palatino‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Valle ›Murcia‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Arx‹ Capitolino
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Velabro‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Argileto ›Argiletum‹
              ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Prati Muzii ›Prata
              Mucia‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Aventino‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Navalia‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Prati Quinzii ›Prata
              Quinctia‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Pincio‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">Campo Marzio‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Gianicolo‹ ◉</a>
            </li>
            <li>
              <a href="dg45043801s.html#25">›Vaticano‹ ◉</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45043801s.html#26">[Text]</a>
      <ul>
        <li>
          <a href="dg45043801s.html#26">Introduzione</a>
          <ul>
            <li>
              <a href="dg45043801s.html#26">I. Della topografia
              fisica del suolo di Roma, dei colli e delle valli, e
              de' nomi che ebbero ne' tempi antichi</a>
              <ul>
                <li>
                  <a href="dg45043801s.html#30">›Tevere‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#32">›Palatino‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#34">Capitolio
                  ›Campidoglio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#38">›Aventino‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#42">›Celio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#46">›Esquilino‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#48">›Viminale‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#50">›Quirinale‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#52">›Pincio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#54">›Monte Giordano‹,
                  Citorio ›Piazza di Montecitorio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#56">›Monte dei
                  Cenci‹, Savelli ›Piazza di Monte Savello‹,
                  ›Testaccio (monte)‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#58">›Testaccio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#60">›Vaticano‹,
                  ›Gianicolo‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#62">›Campo
                  Marzio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#66">Palude Caprea
                  ›Caprae Palus‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#68">Argileto
                  ›Argiletum‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#70">›Velabro‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#72">Valle
                  ›Murcia‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#74">›Via Sacra‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#78">›Carinae‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#80">Foro ›Foro
                  Romano‹, Subura ›Suburra‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#84">Subura, Vico
                  Patrizio ›Vicus Patricius‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#86">›Isola
                  Tiberina‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#90">Campo Bruziano
                  ›Campus Bruttianus‹ e Codetano ›Codeta‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45043801s.html#91">II. Della fondazione
              di Roma e degl'ingrandimenti del suo recinto: del
              Pomerio ›Pomerium‹: delle porte: e del ponti</a>
              <ul>
                <li>
                  <a href="dg45043801s.html#107">Porta Romana
                  ›Porta Mugonia‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#110">›Porta
                  Romanula‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#111">Porta Ianuale
                  ›Ianus Geminus, aedes‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#112">›Porta
                  Carmentalis‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#116">Recinto di
                  Servio ›Mura Serviane‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#123">Pomerio
                  ›Pomerium‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#128">›Porta
                  Flumentana‹, ›Porta Trionfale‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#129">›Porta
                  Ratumena‹, ›Porta Catularia‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#130">Porta Saneuale
                  ›Porta Sanqualis‹, Porta Salutare ›Porta
                  Salutaris‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#131">Porta Piacolare
                  ›Porta Piacularis‹, ›Porta Collina‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#132">›Porta
                  Viminale‹, ›Porta Esquilina‹, ›Porta Muzia‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#133">›Porta
                  Querquetulana‹, ›Porta Caelimontana‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#134">›Porta
                  Fontinalis‹, Porta Ferentina ›Porta Latina‹,
                  ›Porta Capena‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#135">›Porta Naevia‹,
                  ›Porta Raudusculana‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#136">Porta Lavernale
                  ›Porta Lavernalis‹, Porta Navale ›Porta
                  Portuensis‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#137">›Porta Minucia‹,
                  ›Porta Trigemina‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#139">Recinto di
                  Aureliano ›Mura Aureliane‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#141">Recinto di
                  Onorio ›Mura Aureliane‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#151">Mura Leoniane
                  ›Mura Leonine‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#161">Porta Aurelia
                  ›Porta San Pancrazio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#163">Porta Flaminia
                  ›Porta del Popolo‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#166">›Porta
                  Pinciana‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#167">›Porta
                  Salaria‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#168">›Porta Pia‹, e
                  ›Porta Nomentana‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#169">›Porta
                  Tiburtina‹ o San Lorenzo</a>
                </li>
                <li>
                  <a href="dg45043801s.html#170">Porta
                  Prenestina, Labicana, o ›Porta Maggiore‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#171">›Porta San
                  Giovanni‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#172">›Porta
                  Asinaria‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#173">›Porta
                  Metronia‹, ›Porta Latina‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#174">Porta Appia
                  ›Porta San Sebastiano‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#176">›Porta
                  Ardeatina‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#176">Porta Ostiense
                  ›Porta San Paolo‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#178">Porta Portese
                  ›Porta Portuensis‹, ›Porta San Pancrazio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#179">›Porta
                  Settimiana‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#181">›Porta
                  Cavalleggeri‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#182">›Porta
                  Fabbrica‹, ›Porta Angelica‹, ›Porta Castello‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#184">Ponte Aelius, o
                  ›Ponte Sant'Angelo‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#192">Ponte Cestius, o
                  Gratiani ›Ponte Cestio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#199">Ponte Fabricius
                  ›Ponte Fabricio‹, o Quattro Capi</a>
                </li>
                <li>
                  <a href="dg45043801s.html#203">Ponte
                  Ianiculensis, o ›Ponte Sisto‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#209">Ponte Molvius, o
                  Molle ›Ponte Milvio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#218">Ponte Palatinus
                  ›Ponte Palatino‹, o Rotto</a>
                </li>
                <li>
                  <a href="dg45043801s.html#224">Ponte Sublicius
                  ›Ponte Sublicio‹</a>
                </li>
                <li>
                  <a href="dg45043801s.html#230">Ponte Vaticanus
                  ›Pons Neronianus‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45043801s.html#232">III. Ricerche sulla
              popolazione di Roma antica, e sullínnalzamento
              successivo del suolo. Divisione antica e moderna
              della città.</a>
            </li>
            <li>
              <a href="dg45043801s.html#259">IV. Dei materiali
              impiegati nelle fabbriche antiche di Roma, delle
              costruzioni, e dello stile tanto presso gli antichi
              quanto presso i moderni</a>
            </li>
            <li>
              <a href="dg45043801s.html#325">V. Tavole
              cronologiche della Storia e delle Arti di Roma</a>
              <ul>
                <li>
                  <a href="dg45043801s.html#325">Sezione I.
                  Cronologia storica</a>
                  <ul>
                    <li>
                      <a href="dg45043801s.html#325">Fatti dalla
                      fondazione di Roma fino alla battaglia di
                      Azio</a>
                    </li>
                    <li>
                      <a href="dg45043801s.html#335">Serie
                      cronologica degl'Imperadori</a>
                    </li>
                    <li>
                      <a href="dg45043801s.html#337">Serie
                      cronologica de' primi Re d'Italia</a>
                    </li>
                    <li>
                      <a href="dg45043801s.html#337">Imperadori
                      di Oriente che ebbero dominato in Roma dopo
                      la morte di Teja, ed Esarchi di Ravenna loro
                      Vicarii in Italia</a>
                    </li>
                    <li>
                      <a href="dg45043801s.html#338">Serie
                      Cronologica de' Papi da San Pietro al
                      regnante Gregorio XVI. coll'anno della loro
                      elezione</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg45043801s.html#343">Sezione II.
                  Cronologia degli artisti</a>
                  <ul>
                    <li>
                      <a href="dg45043801s.html#343">Che hanno
                      fiorito o lavorato in Roma dal Risorgimento
                      delle Arti fino ai giorni nostri, disposti
                      per ordine alfabetico</a>
                      <ul>
                        <li>
                          <a href=
                          "dg45043801s.html#343">Pittori</a>
                        </li>
                        <li>
                          <a href=
                          "dg45043801s.html#349">Scultori</a>
                        </li>
                        <li>
                          <a href=
                          "dg45043801s.html#351">Architetti</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45043801s.html#354">Parte Prima antica</a>
        </li>
        <li>
          <a href="dg45043801s.html#354">I. Degli acquedotti
          antichi e de' monumenti superstiti relativi alle acque,
          come Castelli di Divisione, Fontane, Ninfèi, Piscine,
          ec.</a>
          <ul>
            <li>
              <a href="dg45043801s.html#364">Alessandrina ›Aqua
              Alexandrina‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#366">Antoniana ›Aqua
              Marcia‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#368">Claudia ›Acquedotto
              Claudio‹ ed Aniene Nuova ›Anio novus
              (acquedotto)‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#377">Monumento dell'Acqua
              Claudia ›Castello dell'Acqua Claudia‹ ▣</a>
            </li>
            <li>
              <a href="dg45043801s.html#382">Giulia ›Acqua
              Iulia‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#388">›Acqua Marcia‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#390">›Acqua Tepula‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#392">›Acqua Traiana‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#394">›Acqua
              Vergine/Acquedotto Vergine‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#396">›Ninfeo di
              Alessandro‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#397">Acquedotti, Meta
              Sudante ›Meta Sudans‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45043801s.html#400">II. Degli Anfiteatri e
          particolarmente dell'›Anfiteatro Castrense‹.
          dell'Anfiteatro Flavio volgarmente detto ›Colosseo‹, e di
          quello di Statilio Tauro ›Anfiteatro di Statilio Tauro‹,
          oggi Monte Citorio</a>
          <ul>
            <li>
              <a href="dg45043801s.html#424">›Anfiteatro
              Castrense‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#426">Anfiteatro Flavio
              ›Colosseo‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#450">Anfiteatro Flavio
              ›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dg45043801s.html#458">Sezione
              dell'Anfiteatro Flavio ›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dg45043801s.html#463">Anfiteatro Flavio
              ›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dg45043801s.html#465">›Anfiteatro di
              Statilio Tauro‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45043801s.html#467">III. Degli Archi
          trionfali, monumentali, e di transito</a>
          <ul>
            <li>
              <a href="dg45043801s.html#472">Arco di Claudio
              ›Arcus Claudii (a. 43 d.C.)‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#477">›Arco di Costantino‹
              ▣</a>
            </li>
            <li>
              <a href="dg45043801s.html#478">›Arco di
              Costantino‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#492">›Arco di Dolabella‹
              e Silano</a>
            </li>
            <li>
              <a href="dg45043801s.html#493">›Arco di Druso‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#494">›Arco di Druso‹
              ▣</a>
            </li>
            <li>
              <a href="dg45043801s.html#498">Arco Fabiano ›Fornix
              Fabianus‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#500">›Arco di
              Gallieno‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#504">›Arco di Giano‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#507">Arco di Lentulo
              ›Arcus Lentuli et Crispini‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#509">Arco di Marco
              Aurelio ›Arcus Marci Aurelii‹ ▣</a>
            </li>
            <li>
              <a href="dg45043801s.html#510">Arco di Marco
              Aurelio ›Arcus Marci Aurelii‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#515">›Arco di Settimio
              Severo‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#516">›Arco di Settimio
              Severo‹ ▣</a>
            </li>
            <li>
              <a href="dg45043801s.html#528">Arco di Settimio
              Severo al Velabro ›Arco degli Argentari‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#531">›Arco di Tito‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#532">›Arco di Tito‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45043801s.html#538">IV. Del Capitolio
          ›Campidoglio‹ dalla fondazione di Roma fino al secolo
          XV</a>
          <ul>
            <li>
              <a href="dg45043801s.html#560">Salita de' Cento
              Gradi ›Centum Gradus‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#562">Salita del Clivo
              Capitolino ›Clivus Capitolinus‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#566">Salita del Clivo
              Sacro ›Clivus Sacer‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#568">Carcere ›Carcer‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#574">›Tempio della
              Concordia‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#584">Tempio di Giove
              Tonante ›Iuppiter Tonans, Aedes‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#588">Edicola di Faustina
              Giuniore ›Faustina, Diva, Aedicula‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#588">Area, Taberne e
              ›Portico degli Dei Consenti‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#591">›Tempio della
              Fortuna‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#594">Portico e Tabulario
              ›Tabularium‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#598">›Tempio di Giove
              Capitolino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45043801s.html#618">V. De' ›Castra
          Praetoria‹</a>
        </li>
        <li>
          <a href="dg45043801s.html#626">VI. De' Circhi</a>
          <ul>
            <li>
              <a href="dg45043801s.html#640">›Circo di
              Adriano‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#642">Circo di Alessandro
              ›Piazza Navona‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#646">Circo di Cajo e
              Nerone ›Circo di Caligola‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#649">Circo di Elagabalo,
              o Variano ›Circo Variano‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#650">›Circo Flaminio‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#656">›Circo di Flora‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#661">›Circo Massimo‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#675">Circo di Romolo
              ›Circo di Massenzio‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#679">Circo di Romolo
              ›Circo di Massenzio‹ ▣</a>
            </li>
            <li>
              <a href="dg45043801s.html#689">Circo di Sallustio
              ›Horti Sallustiani‹</a>
            </li>
            <li>
              <a href="dg45043801s.html#693">Circo di Sallustio
              ›Horti Sallustiani‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45043801s.html#695">VII. Delle Cloache</a>
          <ul>
            <li>
              <a href="dg45043801s.html#702">›Cloaca Maxima‹
              ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45043801s.html#706">[Indici]</a>
      <ul>
        <li>
          <a href="dg45043801s.html#706">Indice degli articoli
          contenuti in questo volume</a>
        </li>
        <li>
          <a href="dg45043801s.html#708">Indice delle materie
          contenute in questo volume</a>
        </li>
        <li>
          <a href="dg45043801s.html#716">Indice delle tavole</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
