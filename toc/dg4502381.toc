<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502381s.html#6">Memoria fatta dal Signor Gaspare
      Celio dell'habito di Christo [...]</a>
      <ul>
        <li>
          <a href="dg4502381s.html#8">Al Signor Pavolo [sic]
          Giordani Canonico di Santa Maria Inviolata [dedica
          dell'editore]</a>
        </li>
        <li>
          <a href="dg4502381s.html#12">Al Molto Illustre Signore
          il Signor Giovan Vittorio De Rossi [dedica
          dell'autore]</a>
        </li>
        <li>
          <a href="dg4502381s.html#17">Sonetto del Signor Giovan
          Vittorio de Rossi. Al Signor Cavalier Gaspare Celio
          [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502381s.html#18">Memoria Da chi siano state
      depinte alcune Pitture, le quali sono in alcune Chiese, e
      Palazzi, e facciate di Roma, con alcune statue, e nomi
      d'Architetti</a>
      <ul>
        <li>
          <a href="dg4502381s.html#18">Le Chiese vanno per
          Alfabeto</a>
          <ul>
            <li>
              <a href="dg4502381s.html#18">Sant'Ambrosio nella via
              Flaminia hoggi il Corso ›Santi Ambrogio e Carlo al
              Corso‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#19">›Sant'Agostino‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#22">Sant'Aloigi della
              natione Francesa ›San Luigi dei Francesi‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#24">Sant'Antonio della
              natione Portughese ›Sant'Antonio dei Portoghesi‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#25">›Sant'Andrea della
              Valle‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#26">›Santi Apostoli‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#27">›Sant'Andrea delle
              Fratte‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#27">Sant'Antonio vicino à
              Santa Maria Maggiore ›Sant'Antonio Abate‹, edificato
              con le sue entrate dal Cardinal Capotio Romano</a>
            </li>
            <li>
              <a href="dg4502381s.html#28">›Sant'Apollinare‹ del
              Colleggio [sic] Germanico</a>
            </li>
            <li>
              <a href="dg4502381s.html#28">›Sant'Atanasio‹, Chiesa
              del Colleggio [sic] Greco</a>
            </li>
            <li>
              <a href="dg4502381s.html#29">L'Annuntiata del
              Colleggio Romano ›Sant'Ignazio‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#30">San Bartolomeo in San
              Mauto della natione Bergamasca ›San Macuto‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#30">›San Bartolomeo dei
              Vaccinari‹ nella Regola</a>
            </li>
            <li>
              <a href="dg4502381s.html#31">›San Bernardo alle
              Terme‹ Diocletiane</a>
            </li>
            <li>
              <a href="dg4502381s.html#31">›Santa Caterina dei
              Funari‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#32">›San Carlo ai
              Catinari‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#32">›Sant'Eustachio‹ alla
              Dogana</a>
            </li>
            <li>
              <a href="dg4502381s.html#33">›San Francesco a Ripa‹
              grande</a>
            </li>
            <li>
              <a href="dg4502381s.html#34">San Francesco delli
              mendicanti al Fontanone in capo à strada Giulia ›San
              Francesco d'Assisi a Ponte Sisto‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#34">›San Giovanni in
              Laterano‹, nel monte Celio</a>
            </li>
            <li>
              <a href="dg4502381s.html#37">›Santi Giovanni e
              Paolo‹ in monte Celio</a>
            </li>
            <li>
              <a href="dg4502381s.html#37">San Gregorio nel monte
              Celio ›San Gregorio Magno‹, ove è la Chiesa di
              Sant'Andrea edificata dal Santo nelle proprie Case
              ›Oratorio di Sant'Andrea‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#37">San Giacomo della
              natione Spagnuola ›Nostra Signora del Sacro
              Cuore‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#40">San Geronimo in strada
              Giulia ›San Girolamo della Carità‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#40">San Geronimo in Ripetta
              ›San Girolamo degli Illirici‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#40">San Giacomo
              dell'Incurabili nella via Flaminia, hoggi il Corso
              ›San Giacomo in Augusta‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#42">San Giovanne della
              natione Fiorentina in strada Giulia nella ripa del
              Tevere ›San Giovanni dei Fiorentini‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#43">San Giovanne Decollato
              della natione Fiorentina presso il foro Boario ›San
              Giovanni Decollato‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#44">Il Giesù nella Piazza
              delli Altieri ›Santissimo Nome di Gesù‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#48">Sant'Honofrio nella
              Porta del Monte Aureo verso il Vatticano
              ›Sant'Onofrio al Gianicolo‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#49">San Lorenzo contiguo
              alla Cancellaria ›San Lorenzo in Damaso‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#49">›San Lorenzo in
              Panisperna‹ nel Colle Vimtnale [sic]</a>
            </li>
            <li>
              <a href="dg4502381s.html#50">›Santa Maria del
              Popolo‹ vicino la Porta Flaminia</a>
            </li>
            <li>
              <a href="dg4502381s.html#52">›Santa Maria
              dell'Anima‹ della natione Germanica vicina al Circo
              Agone</a>
            </li>
            <li>
              <a href="dg4502381s.html#53">›Santa Maria della
              Pace‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#56">Santa Maria in
              Vallicella presso la via dell'orefeci, detto il
              Pellegrino ›Chiesa Nuova‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#57">Santa Maria della
              Navicella nel Monte Celio ›Santa Maria in
              Domnica‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#58">Santa Maria della
              Consolatione al piede della rupe Tarpeia ›Santa Maria
              della Consolazione‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#58">Santa Maria in via
              lata, hoggi del Corso ›Santa Maria in Via Lata‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#59">›Santa Maria in
              Aracoeli‹ nel Monte Capitolino</a>
            </li>
            <li>
              <a href="dg4502381s.html#60">Santa Maria delli Monti
              ›Chiesa della Madonna dei Monti‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#61">Santa Maria di Loreta
              delli Fornari vicino alla Colonna Traiana ›Santa
              Maria di Loreto‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#62">›Santa Maria dell'Orto‹
              verso Ripa grande</a>
            </li>
            <li>
              <a href="dg4502381s.html#62">›Santa Maria in
              Traspontina‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#63">›Santa Maria della
              Scala‹ in Trastevere</a>
            </li>
            <li>
              <a href="dg4502381s.html#63">›Santa Maria in
              Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#64">›Santa Maria Maggiore‹
              nel Colle Escquilino [sic]</a>
              <ul>
                <li>
                  <a href="dg4502381s.html#67">Per la Chiesa</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502381s.html#68">›Santa Maria degli
              Angeli‹ nelle terme Diocletiane</a>
            </li>
            <li>
              <a href="dg4502381s.html#69">Santa Maria in Minerba
              vicino al Panteon ›Santa Maria sopra Minerva‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#71">Santa Maria in Rotonda
              già Panteone ›Pantheon‹, vi sono molti depositi di
              persone virtuose</a>
            </li>
            <li>
              <a href="dg4502381s.html#72">San Marcello nella via
              latea, hoggi il Corso ›San Marcello al Corso‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#73">›Santa Maria in Via‹
              presso la Colonna Antonina</a>
            </li>
            <li>
              <a href="dg4502381s.html#74">Oratorij di
              Confraternità ›Oratorio del Crocifisso‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#74">L'Oratorio dell [sic]
              Carmine ›Santa Maria del Carmine‹ dopò Santi
              Apostoli</a>
            </li>
            <li>
              <a href="dg4502381s.html#75">L'›Oratorio del
              Gonfalone‹ à strada Giulia</a>
            </li>
            <li>
              <a href="dg4502381s.html#75">›San Pietro in
              Vaticano‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#82">San Pietro nel monte
              Aureo ›San Pietro in Montorio‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#84">›San Pietro in Vincoli‹
              nelle [sic] Colle Esquilinio</a>
            </li>
            <li>
              <a href="dg4502381s.html#85">›Santa Prassede‹ nel
              Colle Escqui linio [sic], dove si pensa, che fosse la
              Torre di Mecenate ›Turris Maecenatiana‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#86">Santa Podentiana ›Santa
              Pudenziana‹ alle radici del Colle Viminale, et in
              capo al vico Patritio ›Vicus Patricius‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#88">La Pietà ›Santa Maria
              della Pietà (Piazza Colonna)‹, Chiesa delli Pazzi,
              nella Piazza della Colonna Antonina</a>
            </li>
            <li>
              <a href="dg4502381s.html#89">San Paolo fuora di
              Roma, à l'acque Salvie ›San Paolo alle Tre
              Fontane‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#90">›San Rocco‹ à
              Ripetta</a>
            </li>
            <li>
              <a href="dg4502381s.html#91">Rifettorio di ›San
              Salvatore in Lauro‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#92">›Santa Susanna‹ detta
              fra li due lauri vicino alle Terme Diocletiane</a>
            </li>
            <li>
              <a href="dg4502381s.html#93">›San Silvestro al
              Quirinale‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#95">Santo Stefano nel Monte
              Celio dove era il Tempio di Fauno ›Santo Stefano
              Rotondo‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#96">›Santa Sabina‹ nel
              Monte Aventino</a>
            </li>
            <li>
              <a href="dg4502381s.html#96">›Santo Spirito in
              Sassia‹ nel Borgo delli Sassonij</a>
            </li>
            <li>
              <a href="dg4502381s.html#98">Santissima Trinità nel
              Monte Pincio ›Trinità dei Monti‹</a>
              <ul>
                <li>
                  <a href="dg4502381s.html#103">Le Pitture del
                  Claustro di essa Chiesa</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502381s.html#105">›Santissima Trinità
              dei Pellegrini‹ vicino al Ponte Sisto. et alla
              Regola</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502381s.html#105">Palazzi in Roma, e fuori
          con giardini</a>
          <ul>
            <li>
              <a href="dg4502381s.html#105">Palazzo Pontificio in
              Vaticano con sue Pitture, e Statue ›Palazzo
              Apostolico Vaticano‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#106">Dentro la Cappella di
              Sisto Quarto ›Cappella Sistina‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#108">Pitture nella ›Sala
              Regia‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#111">Pitture dentro la
              Pavolina [sic] ›Cappella Paolina (Città del
              Vaticano)‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#111">Pitture dento la
              Saletta ›Vaticano: Sala Ducale‹, che segue dopo la
              Reggia ›Vaticano: Sala regia‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#112">Pitture in la seconda
              Saletta, dove si fa la lavatione delli piedi
              ›Vaticano: Sala Ducale‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#113">Pitture nelle due
              stanze seguenti ›Vaticano: Sala del Concistoro‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#113">Pitture nella loggia
              al medesimo piano ›Vaticano: Loggia di Raffaello‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#114">Pitture nella Sala del
              medesimo piano ›Vaticano: Appartamento Borgia‹, che
              va à Torre Borgia ›Vaticano: Torre Borgia‹ Palazzo
              vecchio</a>
            </li>
            <li>
              <a href="dg4502381s.html#115">Pitture nella Loggia
              del secondo piano, etiam di Leone Decimo ›Vaticano:
              Loggia di Raffaello‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#117">Pitture della loggia,
              che segue nel medesimo piano di Gregorio Decimoterzo
              ›Vaticano: Loggia di Raffaello‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#118">Pitture della loggia
              di sopra di Gregorio Decimoterzo ›Vaticano: Loggia di
              Raffaello‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#119">Pitture nella loggia
              del medesimo piano di Pio Quarto ›Vaticano: Galleria
              delle Carte geografiche‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#119">Tornando al piano
              della loggia di Leone per andare nell'appartamento
              vecchio, entrando dalla porta verso le scale
              ›Vaticano: Loggia di Raffaello‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#120">Quello nella stanza,
              che segue ›Vaticano: Stanze di Raffaello‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#120">Pitture nella Sala,
              che segue detta di Costantino ›Vaticano: Sala di
              Costantino‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#121">Pitture nella seguente
              stanza ›Vaticano: Stanza di Eliodoro‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#122">Pitture della seconda
              stanza ›Vaticano: Stanza della Segnatura‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#122">Pitture nella terza
              stanza ›Vaticano: Stanza dell'Incendio‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#124">Pitture della Galleria
              che fece fare Gregorio Decimoterzo ›Vaticano:
              Galleria delle Carte geografiche‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#125">Pitture nelle loggie
              non fenite nel secondo piano ›Vaticano: Loggia di
              Raffaello‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#125">Pitture della Sala
              Clementina nel medesimo Piano ›Vaticano: Sala
              Clementina‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#125">Pitture nella sala
              seguente, e sue Camere</a>
            </li>
            <li>
              <a href="dg4502381s.html#126">Si cala à basso, e si
              trova il Corritore, che và à Belvedere ›Vaticano:
              Corridoio del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#127">Si passa per un
              corritore stretto, et si và verso Tramontana.
              nell'appartamento, che fece fare Innocentio
              Ottavo</a>
            </li>
            <li>
              <a href="dg4502381s.html#128">Si torna fuora di esso
              appartamento, et si entra nel picciolo giardino dove
              sono le statue antiche</a>
            </li>
            <li>
              <a href="dg4502381s.html#128">Si passa avanti
              nell'Appartamento, che fece Pio Quarto</a>
            </li>
            <li>
              <a href="dg4502381s.html#129">Palazzo de Signori
              Barberini. nella calata del Monte Quirinale verso
              Tramontana ›Palazzo Barberini‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#130">Casino sopra il Monte
              Aureo, che guarda dalla loggia verso il Tevere ›Villa
              Lante‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#131">Pitture fatte nel
              Palazzo di Agostino Gisi ›Villa Farnesina‹ nella via
              detta Longara ›Via della Lungara‹ verso il Tevere</a>
            </li>
            <li>
              <a href="dg4502381s.html#134">Pitture fatte nel
              Palazzo in strada Giulia vicino al Tevere, del
              Cardinal Montepulciano ›Palazzo Sacchetti‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#134">Pitture nel Palazzo
              detto del Duca ›Palazzo Farnese‹, dove si crede fosse
              il ›Teatro di Pompeo‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#138">Pitture del Palazzo
              della Cancellaria ›Palazzo della Cancelleria‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#139">Pitture nel Palazzo
              vecchio, del Signor Marchese Matthei ›Palazzo
              Caetani‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#140">Quelle del Palazzo
              nuovo del medesimo a Santa Caterina de funari
              ›Palazzo Mattei di Giove‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#141">Pitture nel Casino del
              Signor Principe Peretti nel Colle Esquilino ›Villa
              Negroni‹, già Orti di Mecenate ›Horti Maecenatiani‹,
              presso Santa Maria Maggiore</a>
            </li>
            <li>
              <a href="dg4502381s.html#142">Pitture nel Palazzo
              del Signor Principe Borghese ›Palazzo Borghese‹, et
              nella Vigna posta in loco detto Pariolo ›Villa
              Borghese‹, fuora di ›Porta Pinciana‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#143">Pitture del Casino del
              Signor Prencipe Aldobrandino nel Monte detto
              Magnanapoli ›Villa Aldobrandini‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#143">Palazzo con giardino
              del Gran Duca di Toscana sopra il Monte Pincio ›Villa
              Medici‹</a>
              <ul>
                <li>
                  <a href="dg4502381s.html#144">Le Statue antiche
                  sono</a>
                </li>
                <li>
                  <a href="dg4502381s.html#144">Nel Giardino vi
                  sono</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502381s.html#145">Nel Casino del Signor
              Duca Sennesio ›Palazzo Cavalieri‹, nel Monte Santo
              Spirito, vi sono molte pitture di artefici nominati,
              et altre cose curiose</a>
            </li>
            <li>
              <a href="dg4502381s.html#145">Nel Palazzo del Sign.
              Marchese Giustiniano ›Palazzo Giustiniani‹ vicino à
              Sant'Aloisi ›San Luigi dei Francesi‹, vi sono molte
              pitture, et altre cose curiose, di Statue, et
              Medaglie</a>
            </li>
            <li>
              <a href="dg4502381s.html#145">Pitture del Palazzo
              del Senato Romano ›Palazzo Senatorio‹, nel Monte
              Tarpeio, loco detto ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4502381s.html#148">Pitture sopra le
              facciate delle case di Roma</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502381s.html#158">Al signor Giovanni Vittorio de
      Rossi. Il Cavalier Gaspar Celio [sonetto]</a>
    </li>
  </ul>
  <hr />
</body>
</html>
