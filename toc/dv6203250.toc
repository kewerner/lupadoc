<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dv6203250s.html#6">L'Anfiteatro Flavio ›Colosseo‹
      descritto e delineato [...] ▣</a>
      <ul>
        <li>
          <a href="dv6203250s.html#8">Proemio generale</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dv6203250s.html#9">Indice dei libri</a>
    </li>
    <li>
      <a href="dv6203250s.html#10">Introduzione. Dell'origine,
      della fabrica, e dei [sic] edificatori dei teatri ed
      anfiteatri</a>
      <ul>
        <li>
          <a href="dv6203250s.html#10">I. Della prima invenzione,
          o primo stabilimento, del teatro</a>
        </li>
        <li>
          <a href="dv6203250s.html#12">II. Differenza che è fra 'l
          teatro e l'anfiteatro</a>
        </li>
        <li>
          <a href="dv6203250s.html#14">III. Materia e copertura
          dei teatri nella loro origine</a>
        </li>
        <li>
          <a href="dv6203250s.html#16">IV. Della significazione
          della scena e sue qualità</a>
        </li>
        <li>
          <a href="dv6203250s.html#18">V. Delle machine solite ad
          usarsi ne' teatri</a>
        </li>
        <li>
          <a href="dv6203250s.html#20">VI. Del modo ch'usavano gli
          antichi per far sentire i suoni, e le voci, ne'
          teatri</a>
        </li>
        <li>
          <a href="dv6203250s.html#22">VII. Dell'orchestra, dei
          sedili, e del modo e ragione di sedersi ne' teatri
          appresso li Romani</a>
        </li>
        <li>
          <a href="dv6203250s.html#24">VIII. Dell'ornamento de'
          teatri</a>
        </li>
        <li>
          <a href="dv6203250s.html#26">IX. Consecrazione e
          dedicazione nei tempi primieri dell'origine dei
          teatri</a>
        </li>
        <li>
          <a href="dv6203250s.html#28">X. Dei [sic] edificatori
          dei teatri appresso i Romani</a>
        </li>
        <li>
          <a href="dv6203250s.html#30">XI. Del teatro di Marco
          Scauro ›Theatrum Scauri‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#32">XII. Del teatro di Marco
          Curio</a>
        </li>
        <li>
          <a href="dv6203250s.html#34">XIII. Del ›Teatro di
          Pompeo‹</a>
          <ul>
            <li>
              <a href="dv6203250s.html#34">Articolo I. Del vero
              sito del ›Teatro di Pompeo‹</a>
            </li>
            <li>
              <a href="dv6203250s.html#35">Articolo II. Della
              magnificenza, e disegno, del ›Teatro di Pompeo‹</a>
            </li>
            <li>
              <a href="dv6203250s.html#36">Articolo III. Della
              scena del ›Teatro di Pompeo‹, e dei suoi
              risarcimenti</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dv6203250s.html#38">XIV. Del ›Teatro di
          Marcello‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#40">XV. Del ›Teatro di
          Balbo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#42">XVI. Degli anfiteatri posti
          in diversi luoghi e provincie</a>
        </li>
        <li>
          <a href="dv6203250s.html#44">XVII. Dell'anfiteatro di
          marmo posto in Verona</a>
        </li>
        <li>
          <a href="dv6203250s.html#46">XVIII. Conclusione di
          questa introduzione</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dv6203250s.html#48">Libro primo. Del [sic] stato
      presente dell'Anfiteatro Flavio ›Colosseo‹, cioè descrizzione
      [sic] della sua residual parte, che hora è in piedi</a>
      <ul>
        <li>
          <a href="dv6203250s.html#48">Proemio</a>
        </li>
        <li>
          <a href="dv6203250s.html#51">Pianta Terrena delle
          residuali Parti che si trovano in piedi dell'Anfiteatro
          Flavio l'anno 1708 ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#52">I. Ichnografia terrena
          della residual parte che è in essere dell'Anfiteatro
          Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#55">Pianta delle Residuali
          Parti dell'Anfiteatro Flavio dal Secondo piano delli
          Ambulacri e declivio dove erano li Sedili sino al Podio
          Reggio contigue alla Rena o Cavea ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#56">II. Ichnografia del secondo
          piano residuale, che è in essere, dell'Anfiteatro Flavio
          ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#59">Pianta delle Residuali
          Parti del terzo Piano dell'Anfiteatro Flavio dove
          terminano li sedili e Precensiri ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#60">III. Ichnografia della
          residual parte del terzo piano dell'Anfiteatro Flavio
          ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#63">Prospetto residuale
          dell'Anfiteatro Flavio verso Tramontana come si trova di
          presente ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#63">Prospetto residuale
          dell'Anfiteatro Flavio verso Ponente, come si trova di
          presente ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#64">IV. Delli prospetti, e
          residuali vestigie esteriori, dell'Anfiteatro Flavio
          ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#67">Prospetto ò vero settione
          della residuale parte interna dell'Anfiteatro Flavio
          verso tramontana come si trova di presente ›Colosseo‹
          ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#67">Prospetto ò vero settione
          della residuale parte interna dell'Anfiteatro Flavio
          verso Levante come si trova di Presente ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#68">V. Profili e vedute delle
          residuali parti interne dell'Anfiteatro Flavio
          ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#70">VI. Dei profili, overo
          settioni, che dimostrano lo stato presente, e l'antico
          nel suo essere, dell'Anfiteatro Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#72">Profilo o vero Settione
          verso Tramontana dell'Anfiteatro Flavio quando era nel
          suo primiero Stato ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#75">Pianta del Primo ordine
          Terreno dell'Anfiteatro Flavio quando era tutto in essere
          nel Tempo de Cesare ›Colosseo‹ ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dv6203250s.html#76">Libro secondo. Del [sic] stato
      antico dell'Anfiteatro Flavio ›Colosseo‹; cioè, descrizzione
      [sic] delle sue diverse parti, nel suo primiero essere</a>
      <ul>
        <li>
          <a href="dv6203250s.html#76">I. Ichnographia overo
          conformazione del primo ordine terreno dell'Anfiteatro
          Flavio ›Colosseo‹, nel suo primiero stato</a>
        </li>
        <li>
          <a href="dv6203250s.html#81">Pianta del secondo Ordine
          sotto li Gradi dell'Anfiteatro Flavio nel suo Essere
          primiero ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#82">II. Ichnographia overo
          conformazione del secondo ordine dell'Anfiteatro Flavio
          ›Colosseo‹, nel suo primiero stato</a>
        </li>
        <li>
          <a href="dv6203250s.html#85">Pianta del Terzo Ordine del
          Anfiteatro Flavio nel primiero stato che spiccava sopra
          l'Ambito delli Sedili Precensioni Podio Reggio er Arena
          ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#86">III. Ichnographia overo
          conformazione del terzo ordine dell'Anfiteatro Flavio
          ›Colosseo‹, nel suo primiero stato</a>
        </li>
        <li>
          <a href="dv6203250s.html#89">Pianta del quarto Ordine
          dell'Anfiteatro Flavio nel suo Primiero stato Sopra il
          quale spiccava l'ultimi Ordini ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#90">IV. Ichnografia overo
          conformazione del quarto ordine dell'Anfiteatro Flavio
          ›Colosseo‹, nel suo primiero stato</a>
        </li>
        <li>
          <a href="dv6203250s.html#93">Pianta del quinto Ordine
          dell'Anfiteatro Flavio nel suo Primiero stato ›Colosseo‹
          ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#94">V. Ichnografia overo
          conformazione del quinto ordine dell'Anfiteatro Flavio
          ›Colosseo‹, nel suo primiero stato</a>
        </li>
        <li>
          <a href="dv6203250s.html#96">VI. Ichnografia overo
          conformazione del sesto ordine, che faceva finimento
          all'Anfiteatro Flavio ›Colosseo‹, nel suo primiero stato;
          con la situazione e figura della tenda</a>
        </li>
        <li>
          <a href="dv6203250s.html#98">Pianta ó Finimento del
          Teatro Flavio con la Tenda e sua Figura quando era in
          Opera ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#101">Delle Proportioni, e
          Modulatorie per la Conformatine (sic!) dell'Anfiteatro
          Flavio ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#102">VII. Delle proporzioni e
          modulatorie per la conformazione dell'Anfiteatro Flavio
          ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#105">Settione Maggiore
          dell'Anfiteatro Flavio ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#105">Prospetto Maggiore
          dell'Anfiteatro ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#106">VIII. Del prospetto e
          profilo della maggior longitudine dell'Anfiteatro Flavio
          ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#109">Settione interna minore
          dell'Anfiteatro Flavio ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#109">Prospetto minore
          dell'Anfiteatro Flavio ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#110">IX. Del prospetto e
          profilo della minor longitudine dell'Anfiteatro Flavio
          ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#112">X. De gli ornati e parieti
          esteriori dell'Anfiteatro Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#114">Parte dell'Ordini
          Esteriori dell'Anfiteatro Flavio ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#117">Profilo ò Sessione di una
          Parte dell'Anfiteatro verso Tramontana ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#117">Il Modo proprio come si
          poneva la Tenda ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#118">XI. Profilo, overo
          sessione [sic], che dimostra una parte dell'Anfiteatro
          ›Colosseo‹ nel suo essere verso tramontana</a>
        </li>
        <li>
          <a href="dv6203250s.html#121">Podio Reggio Gradi ò
          Sedili è Scale dell'Anfiteatro Flavio in Proportione
          maggiore ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#122">XII. Veduta delle porzioni
          dei gradi, e del podio regio, dell'Anfiteatro Flavio
          ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#126">XIII. Settione, e veduta,
          della metà dell'Anfiteatro ›Colosseo‹, verso levante, nel
          suo primiero stato</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dv6203250s.html#130">Libro terzo. Erudizioni
      profane intorno all'Anfiteatro Flavio ›Colosseo‹; cioè, della
      sua erezzione [sic], e dell'uso delle sue diverse parti; ed
      anche delle feste che vi si celebravano</a>
      <ul>
        <li>
          <a href="dv6203250s.html#130">Proemio</a>
        </li>
        <li>
          <a href="dv6203250s.html#132">I. Situazione, erezzione
          [sic], magnificenza, ed altre famose qualità,
          dell'Anfiteatro di Tito, volgarmente detto ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#134">II. Più distinta notizia
          dell'erezzione [sic], e dedicazione, dell'Anfiteatro
          Flavio ›Colosseo‹, con i suoi ristoramenti fatti in
          diversi tempi</a>
        </li>
        <li>
          <a href="dv6203250s.html#138">III. Della cavea, e loco
          detto arena, nell'Anfiteatro Flavio ›Colosseo‹, e perché
          così si nominasse</a>
        </li>
        <li>
          <a href="dv6203250s.html#140">IV. Del podio regio, e
          prima precensione, dell'Anfiteatro Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#142">V. Delli sedili, o gradi,
          e precensioni dell'Anfiteatro Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#144">VI. Dell'ordine e
          divisione de' luoghi dell'Anfiteatro Flavio
          ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#148">VII. Della quantità del
          popolo fosse capace Anfiteatro Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#150">VIII. Della vela, o sia
          tela, che copriva l'Anfiteatro Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#152">IX. Antri e spelonche, che
          servivano di serraglio e di passaggio alle fiere
          nell'Anfiteatro Flavio ›Colosseo‹; col luogo detto
          Spogliario</a>
        </li>
        <li>
          <a href="dv6203250s.html#154">X. Delle cloache che
          conducevano l'acque nell'Anfiteatro ›Colosseo‹, et di
          quelle che servivano per mondarlo; col di loro Euripo</a>
        </li>
        <li>
          <a href="dv6203250s.html#156">XI. Del bombo e rimbombo,
          dell'eco, e de' varii effetti della voce, si ne' teatri
          come negli anfiteatri, particolarmente nell'Anfiteatro
          Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#160">XII. Del croco odorifero
          pratticato nell'Anfiteatro Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#162">XIII. A quali deità fosse
          dedicato l'Anfiteatro Flavio ›Colosseo‹, secondo le varie
          opinioni degli autori; e delle feste che vi si
          celebravano</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dv6203250s.html#164">Libro quarto. Erudizioni sacre
      intorno all'Anfiteatro Flavio ›Colosseo‹; cioè dei santi che
      in esso furono martirizzati</a>
      <ul>
        <li>
          <a href="dv6203250s.html#164">Proemio. Del modo col
          quale si tormentavano i christiani, nell'Anfiteatro
          Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#166">I. Di Sant'Ignatio
          martire, martirizzato nell'Anfiteatro Flavio</a>
        </li>
        <li>
          <a href="dv6203250s.html#168">II. Di Sant'Eustachio, e
          di Teofista sua moglie, condannati ai leoni
          nell'Anfiteatro Flavio</a>
        </li>
        <li>
          <a href="dv6203250s.html#170">III. Di Santa Taziana
          condannata alle bestie nell'Anfiteatro Flavio</a>
        </li>
        <li>
          <a href="dv6203250s.html#172">IV. De' santi Abdon, e
          Sennen, martirizzati nel Colosseo</a>
        </li>
        <li>
          <a href="dv6203250s.html#174">V. D'altri santi martiri,
          condotti alla ›Pietra scellerata‹ nel Colosseo</a>
        </li>
        <li>
          <a href="dv6203250s.html#176">VI. Di Sant'Eufemia,
          martirizzata nell'Anfiteatro Flavio Colosseo</a>
        </li>
        <li>
          <a href="dv6203250s.html#178">VII. Di San Placido
          martire</a>
        </li>
        <li>
          <a href="dv6203250s.html#180">VIII. Di San Zenone, e di
          X milia e CCIII martiri, parte de' quali furono uccisi
          nei publici spettacoli</a>
        </li>
        <li>
          <a href="dv6203250s.html#182">IX. De' santi Sempronio,
          Olimpio, Exuperia, e Teodolo, martirizzati avanti
          l'Anfiteatro Flavio</a>
        </li>
        <li>
          <a href="dv6203250s.html#184">X. Di CCLXII martiri,
          trucidati nell'Anfiteatro Flavio</a>
        </li>
        <li>
          <a href="dv6203250s.html#186">XI. Di San Ginnesio,
          martirizzato nell'Anfiteatro Flavio</a>
        </li>
        <li>
          <a href="dv6203250s.html#188">XII. D'una santa vergine
          nominata Dominica, condannata alle bestie</a>
        </li>
        <li>
          <a href="dv6203250s.html#190">XIII. De' santi martiri,
          Vito, Modesto, e Crescenzia, condannati alle bestie</a>
        </li>
        <li>
          <a href="dv6203250s.html#192">XIV. Di Sant'Almachio
          coronato del martirio nel Colosseo</a>
        </li>
        <li>
          <a href="dv6203250s.html#194">XV. De' santi martiri
          Marcello, et Eusebio, decollati sopra la ›Pietra
          scellerata‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#196">XVI. Di San Restituto
          martire, sepolto avanti l'Anfiteatro Flavio</a>
        </li>
        <li>
          <a href="dv6203250s.html#198">XVII. Di Santa Martina
          vergine e martire condannata all'Anfiteatro Flavio</a>
        </li>
        <li>
          <a href="dv6203250s.html#200">XVIII. Di Santa Felicita,
          tormentata nel Colosseo</a>
        </li>
        <li>
          <a href="dv6203250s.html#202">XIX. Di Santo Menna
          martire</a>
        </li>
        <li>
          <a href="dv6203250s.html#204">XX. Di San Giovanni prete,
          martirizzato nell'Anfiteatro, avanti la statua del Sole
          ›Colosso di Nerone‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#206">XXI. Conclusione
          dell'erudizioni sacre dell'Anfiteatro Flavio</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dv6203250s.html#208">Libro quinto. Del restituir
      l'onore all'Anfiteatro Flavio ›Colosseo‹; cioè, descrizzione
      [sic] dei [sic] edificii sacri da fare nelle sua residual
      parte</a>
      <ul>
        <li>
          <a href="dv6203250s.html#208">I. Edificii templari per
          restituire la venerazione che merita l'Anfiteatro Flavio
          ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#213">Pianta dell'Anfiteatro
          come di presente si trova con l'edifitio Temptare che si
          propone da Ergersi ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#214">II. Pianta generale
          dell'Anfiteatro Flavio ›Colosseo‹, colla giunta dei nuovi
          edificii, con suoi prospetti e profili</a>
        </li>
        <li>
          <a href="dv6203250s.html#217">Pianta Generale delli
          Edifitij antecedenti proposti da farsi dentro
          l'Anfiteatro ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#218">III. Pianta generale degli
          edificii antecedentemente proposti da farsi dentro
          l'Anfiteatro Flavio ›Colosseo‹</a>
        </li>
        <li>
          <a href="dv6203250s.html#221">Pianta del detto Tempio di
          quadrupla Proportione ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#222">IV. Pianta del detto
          Tempio quadrupla proporzione</a>
        </li>
        <li>
          <a href="dv6203250s.html#225">Prospetto del Tempio di
          quadrupla Proportione ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#226">V. Prospetto del Tempio di
          quadrupla proporzione</a>
        </li>
        <li>
          <a href="dv6203250s.html#229">Settione interna del detto
          Tempio di quadrupla Proportione ▣</a>
        </li>
        <li>
          <a href="dv6203250s.html#230">VI. Settione interna del
          detto Tempio di quadrupla proporzione</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dv6203250s.html#232">Indice dei libri e capitoli di
      questa opera</a>
    </li>
  </ul>
  <hr />
</body>
</html>
