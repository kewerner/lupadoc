<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ecre1524190s.html#4">Dettaglio delle chiese di
      Cremona con in fine il catalogo della gerarchia celeste di
      nostra patria &#160;</a>
      <ul>
        <li>
          <a href="ecre1524190s.html#6">Alli nobilissimi e
          veneratissimi Signori</a>
        </li>
        <li>
          <a href="ecre1524190s.html#11">Al lettore</a>
        </li>
        <li>
          <a href="ecre1524190s.html#12">[Dettaglio delle chiese
          di Cremona con in fine il catalogo della gerarchia
          celeste di nostra patria] &#160;</a>
        </li>
        <li>
          <a href="ecre1524190s.html#180">Catalogo della
          gerarchia celeste di nostra Patria ed altre pie
          persone</a>
        </li>
        <li>
          <a href="ecre1524190s.html#198">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
