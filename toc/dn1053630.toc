<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dn1053630s.html#6">Descrizione del Colosseo Romano,
      del Panteo e del Tempio Vaticano</a>
      <ul>
        <li>
          <a href="dn1053630s.html#8">Descrizione dell'Anfiteatro
          di Roma detto il ›Colosseo‹</a>
          <ul>
            <li>
              <a href="dn1053630s.html#9">Pianta dell'Anfiteatro,
              e sua Dimostrazione</a>
            </li>
            <li>
              <a href="dn1053630s.html#10">Prospetto esterno, ed
              interno</a>
            </li>
            <li>
              <a href="dn1053630s.html#12">[Pianta
              dell'Anfiteatro, e sua Dimostrazione ›Colosseo‹]
              ◉</a>
            </li>
            <li>
              <a href="dn1053630s.html#14">[Prospetto esterno, ed
              interno ›Colosseo‹] ◉</a>
            </li>
            <li>
              <a href="dn1053630s.html#16">Ordini
              dell'Anfiteatro</a>
            </li>
            <li>
              <a href="dn1053630s.html#18">[Ordini dell'Anfiteatro
              ›Colosseo‹] ▣</a>
            </li>
            <li>
              <a href="dn1053630s.html#22">[Ordini dell'Anfiteatro
              ›Colosseo‹] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn1053630s.html#24">Descrizione del famoso
          Tempio del Panteo ›Pantheon‹&#160;</a>
          <ul>
            <li>
              <a href="dn1053630s.html#25">Pianta del Panteo</a>
            </li>
            <li>
              <a href="dn1053630s.html#26">[Pianta del ›Pantheon‹]
              ▣</a>
            </li>
            <li>
              <a href="dn1053630s.html#28">Prospetto esterno, ed
              interno del Tempio</a>
            </li>
            <li>
              <a href="dn1053630s.html#30">[Prospetto esterno, ed
              interno del Tempio ›Pantheon‹] ▣</a>
            </li>
            <li>
              <a href="dn1053630s.html#32">Corrispondenza
              dell'elevazione del Tempio col Piano, e del Portico
              colla sua Pianta</a>
            </li>
            <li>
              <a href="dn1053630s.html#34">[Corrispondenza
              dell'elevazione del ›Pantheon‹ col Piano, e del
              Portico colla sua Pianta] ▣</a>
            </li>
            <li>
              <a href="dn1053630s.html#36">Ordine nella Tribuna, e
              Tabernacoli ne' Pilastri del Tempio</a>
            </li>
            <li>
              <a href="dn1053630s.html#38">[Ordine nella Tribuna,
              e Tabernacoli ne' Pilastri del ›Pantheon‹] ▣</a>
            </li>
            <li>
              <a href="dn1053630s.html#40">[Ordine nella Tribuna,
              e Tabernacoli ne' Pilastri del ›Pantheon‹] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn1053630s.html#42">Descrizione del Tempio
          Vaticano ›San Pietro in Vaticano‹</a>
          <ul>
            <li>
              <a href="dn1053630s.html#43">Pianta del Tempio,
              secondo il disegno del Bonarroti, e sue misure</a>
            </li>
            <li>
              <a href="dn1053630s.html#44">Aggiunta del
              Maderni</a>
            </li>
            <li>
              <a href="dn1053630s.html#46">[Pianta del Tempio ›San
              Pietro in Vaticano‹] ◉</a>
            </li>
            <li>
              <a href="dn1053630s.html#48">Piazza, e Colonnato
              avanti il Tempio</a>
            </li>
            <li>
              <a href="dn1053630s.html#50">[Piazza, e Colonnato
              avanti il Tempio ›San Pietro in Vaticano‹] ◉</a>
            </li>
            <li>
              <a href="dn1053630s.html#52">Prospetto esterno del
              Tempio Vaticano</a>
            </li>
            <li>
              <a href="dn1053630s.html#54">Prospetto del Tempio
              Vaticano ›San Pietro in Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dn1053630s.html#57">Prospetto del Tempio
              Vaticano, e sue principali Misure</a>
            </li>
            <li>
              <a href="dn1053630s.html#58">Prospetto interno del
              Tempio, e Corrispondenza della sua Pianta</a>
            </li>
            <li>################ Seite fehlerhaft/Plan fehlt!</li>
            <li>
              <a href="dn1053630s.html#62">Ornato esteriore del
              Tempio Vaticano, e suoi Ordini Dorico, Jonico, e
              Corintio secondo il presente Sistema</a>
            </li>
            <li>
              <a href="dn1053630s.html#64">[Ornato esteriore del
              Tempio Vaticano, e suo Ordine Corintio ›San Pietro in
              Vaticano‹] ▣</a>
            </li>
            <li>
              <a href="dn1053630s.html#68">[Ornato esteriore del
              Tempio Vaticano, e suoi Ordini Dorico e Jonico ›San
              Pietro in Vaticano‹] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dn1053630s.html#70">Indice de' Nomi colla loro
          Versione Latina e delle Cose più notabili che sono nel
          presente Trattato</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
