<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502505s.html#8">Roma Illustrata, sive
      Antiqvitatum Romanarum Breviarivm &#160;</a>
      <ul>
        <li>
          <a href="dg4502505s.html#8">Roma Illustrata, sive
          Antiqvitatum Romanarum Breviarivm &#160;</a>
          <ul>
            <li>
              <a href="dg4502505s.html#10">Amplißimo Viro, atque
              omni genere Virtutis et Eruditionis excultissimo, D.
              Joachimo A Vvevelichoven</a>
            </li>
            <li>
              <a href="dg4502505s.html#14">Justi Lipsi de
              Magistratibus Veteris Pop. Rom.</a>
            </li>
            <li>
              <a href="dg4502505s.html#58">De Militia Romana</a>
              <ul>
                <li>
                  <a href="dg4502505s.html#58">I. De
                  Dilectu&#160;</a>
                </li>
                <li>
                  <a href="dg4502505s.html#68">II. De
                  Ordine&#160;</a>
                </li>
                <li>
                  <a href="dg4502505s.html#82">III. De
                  Armis&#160;</a>
                </li>
                <li>
                  <a href="dg4502505s.html#104">VI. [sic] De
                  Acie&#160;</a>
                </li>
                <li>
                  <a href="dg4502505s.html#123">V. De
                  Disciplina&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502505s.html#175">I. De Machinis,
              Tormentis, Telis&#160;</a>
              <ul>
                <li>
                  <a href="dg4502505s.html#184">II. De
                  Oppugnatione diuturna&#160;</a>
                </li>
                <li>
                  <a href="dg4502505s.html#194">III. De
                  Tormentis&#160;</a>
                </li>
                <li>
                  <a href="dg4502505s.html#204">IV. De
                  Telis&#160;</a>
                </li>
                <li>
                  <a href="dg4502505s.html#211">V. De
                  Repugnatione&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502505s.html#218">De
              Amphitheatro&#160;</a>
              <ul>
                <li>
                  <a href="dg4502505s.html#234">I. De
                  Gladiatoribus&#160;</a>
                </li>
                <li>
                  <a href="dg4502505s.html#246">II. De Homnibus
                  Pugnantibus&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502505s.html#262">I. Admiranda, sive De
              Magnitudine Romana&#160;</a>
              <ul>
                <li>
                  <a href="dg4502505s.html#276">II. De Opibus
                  Romanis&#160;</a>
                </li>
                <li>
                  <a href="dg4502505s.html#303">III. De Operibus
                  Romanis</a>
                </li>
                <li>
                  <a href="dg4502505s.html#338">IV. De Virtute
                  Romana&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502505s.html#368">I. De Cruce&#160;</a>
              <ul>
                <li>
                  <a href="dg4502505s.html#375">II. De Modo
                  Crucifigendi&#160;</a>
                </li>
                <li>
                  <a href="dg4502505s.html#383">III. De Modo
                  figendi raro&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502505s.html#391">De Vesta et Vestalibus
              Syntagma</a>
            </li>
            <li>
              <a href="dg4502505s.html#407">De Bibliothecis
              Syntagma</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502505s.html#416">Georgii Fabricii
          Chemnicensis Roma</a>
          <ul>
            <li>
              <a href="dg4502505s.html#560">Index rerum et
              verborum</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
