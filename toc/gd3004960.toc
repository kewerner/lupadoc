<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="gd3004960s.html#6">Quellenbuch zur Kunstgeschichte
      des abendländischen Mittelalters</a>
      <ul>
        <li>
          <a href="gd3004960s.html#8">Vorwort</a>
        </li>
        <li>
          <a href="gd3004960s.html#10">Einleitung</a>
        </li>
        <li>
          <a href="gd3004960s.html#25">Inhaltsübersicht</a>
        </li>
        <li>
          <a href="gd3004960s.html#28">Erstes Buch. Christliches
          Alterthum und frühes Mittelalter (Cap. I.-XXII.)</a>
          <ul>
            <li>
              <a href="gd3004960s.html#30">I. Parallelgemälde des
              alten und neuen Testaments im IV. Jahrhundert.
              Prudentius 248-410), Dittochaeon. Saec. IV.&#160;</a>
            </li>
            <li>
              <a href="gd3004960s.html#38">II. Martyriengemälde
              des IV. Jahrhunderts. Prudentius, Peristephanon,
              hymnus IX. u. XI.</a>
            </li>
            <li>
              <a href="gd3004960s.html#40">III. Die Basiliken zu
              Primuliacum, zu Nola und Fundi. (Anfang des V.
              Jahrhunderts) S. Paulini (353-431) opera</a>
            </li>
            <li>
              <a href="gd3004960s.html#57">IV. Gemäldetituli des
              fünften (?) Jahrhunderts. A. Dem heil. Ambrosius
              (gest. 397) zugeschrieben. B. Fälschlich Claudian (um
              400) beigelegt&#160;</a>
            </li>
            <li>
              <a href="gd3004960s.html#59">V. Tituli der Basilica
              des heil. Martin zu Tours. Unter B. Perpetuus, um
              460</a>
            </li>
            <li>
              <a href="gd3004960s.html#61">VI. Typologischer
              Cyclus des Alten und Neuen Testaments. Dem Rusticus
              Helpidius (gest. 533?) beigelegt</a>
            </li>
            <li>
              <a href="gd3004960s.html#64">VII. Gemälde in der
              Cathedrale von Tours. Venentius Fortunatus (um 565),
              Carmina, L.X.,6</a>
            </li>
            <li>
              <a href="gd3004960s.html#69">VIII. Die ersten Bauten
              im Frankenreiche. Gregor von Tours (gest. 594),
              Historia Francorum</a>
            </li>
            <li>
              <a href="gd3004960s.html#71">IX. Aus dem Leben des
              h. Eligius. (gest. gegen 665) S. Audoenus (gest.
              683), Vita s. Eligii</a>
            </li>
            <li>
              <a href="gd3004960s.html#74">X. Früheste
              Kunstthätigkeit bei den Angelsachsen. Beda (gest.
              735), Historia abbatum Wiremuthensium (Vita s.
              Bernedicti Biscopi)</a>
            </li>
            <li>
              <a href="gd3004960s.html#76">XI. Gesetz König
              Liutprands (713-744) über die Bauleute. Memoratorium
              de mercedibus Commacinorum</a>
            </li>
            <li>
              <a href="gd3004960s.html#77">XII. Die Kirchen des h.
              Landes. Adamnani abbatis Hiiensis libri III. de locis
              sanctis [...] (Anfang des VIII. Jhdts.)</a>
            </li>
            <li>
              <a href="gd3004960s.html#86">XIII. Kunstthätigkeit
              in Rom con Constantin M. bis zum Schisma. Liber
              pontificalis Romanus</a>
            </li>
            <li>
              <a href="gd3004960s.html#127">XIV. Kunstthätigkeit
              in Ravenna bis ins sechste Jahrhundert. Agnellus (um
              839). Liber pontificalis ecclesiae Ravennatis</a>
            </li>
            <li>
              <a href="gd3004960s.html#143">XV. Angilberts
              Denkschrift über S. Riquier (Centula). Angilberti de
              ecclesia Centulensis libellus</a>
            </li>
            <li>
              <a href="gd3004960s.html#148">XVI. Die Tische
              Theodulfs. Theodulfi (gest. 821) carmina</a>
            </li>
            <li>
              <a href="gd3004960s.html#153">XVII. Die Wandgemälde
              von Ingelheim. Ermoldus Nigellus, (um 826) In honorem
              Hludowici L. IV.</a>
            </li>
            <li>
              <a href="gd3004960s.html#156">XVIII. Die
              Klosterbauten in St . Wandrille bei Ruoen (unter Abt
              Ansegis 807-833). Gesta abbatum Fontanellensium</a>
            </li>
            <li>
              <a href="gd3004960s.html#158">XIX. Die Gemälde der
              Klosterkirche von St. Gallen. Carmina Sangallensia n.
              VII.</a>
            </li>
            <li>
              <a href="gd3004960s.html#161">XX. Die
              Theodorichstatue in Aachen. Walafriedi Strabonis
              (gest. 849) Versus in Aquisgrani palatio editi anno
              Hludowici imperatoris XVI. de imagine Tetrici</a>
            </li>
            <li>
              <a href="gd3004960s.html#166">XXI. Die Bildnisse
              Gregors des Gr. und seiner Eltern. Johannes Diaconus
              (Ende des IX. Jahrh.) Vita Gregorii M.</a>
            </li>
            <li>
              <a href="gd3004960s.html#168">XXII. Die Bauten Abt
              Witigowo's im Kloster im Kloster Reichenau. Purchardi
              Gesta Witigowonis (u, 994-006)</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gd3004960s.html#172">Zweites Buch. Hohes
          Mittelalter (Cap. XXIII.-XXXIX.)</a>
          <ul>
            <li>
              <a href="gd3004960s.html#174">XXIII. Kunstthätigkeit
              des h. Bernward von Hildesheim. (gest. 1022) Thangmar
              (der Lehrer Bernwards), Vita Bernwardi episcopi</a>
            </li>
            <li>
              <a href="gd3004960s.html#176">XXIV. Der Maler
              Johannes aus Italien. Vita Balderici episcopi
              Leodiensis (1008-1018, geschrieben um 1050)</a>
            </li>
            <li>
              <a href="gd3004960s.html#178">XXV. Die
              Künstlerlegende des Tuotilo von St. Gallen. Ekkehardi
              IV. Casus s. Galii (Anfang des XI. Jahrh.)</a>
            </li>
            <li>
              <a href="gd3004960s.html#180">XXVI. Die Legende des
              h. Gallus im Kreuzgange von St. Gallen. Tituli, von
              Ekkehard IV. auf Wunsch Abt Purchards II. (1001-1022)
              gedichtet</a>
            </li>
            <li>
              <a href="gd3004960s.html#185">XXVII. Ekkehard IV.
              Tituli für den Dom zu Mainz. Gedichtet auf Wunsch
              Erzb. Aribos von Mainz (gest. 1031)</a>
            </li>
            <li>
              <a href="gd3004960s.html#209">XXVIII.
              Kunstthätigkeit im Kloster Fleury unter Abt Gauzlin.
              (gest. 1030) Andreae Floriacensis Vita Gauzlini
              abbatis (geschrieben um 1041)</a>
            </li>
            <li>
              <a href="gd3004960s.html#216">XXIX. Bauordnung von
              Farfa. (1039-1048) Disciplina Farfensis Lib. II.,
              cap. I</a>
            </li>
            <li>
              <a href="gd3004960s.html#219">XXX. Die Kunst in
              Monte Cassino. Leo von Ostia (gest. gegen 1117) und
              sein Fortsetzer Petrus Diaconus (um 1140), Chronicon
              monasterii Casinensis</a>
            </li>
            <li>
              <a href="gd3004960s.html#245">XXXI. Das Schlafgemach
              der Gräfin Adele von Blois. Baudri, Abt von Borgeuil,
              Gedicht an Gräfin Adele (vor 1107)</a>
            </li>
            <li>
              <a href="gd3004960s.html#259">XXXII. Kunstleben im
              Kloster Petershausen bei Konstanz. (X.-XII. Jahrh.)
              Casus Petrihusensis monasterii (um 1156)</a>
            </li>
            <li>
              <a href="gd3004960s.html#267">XXXIII.
              Kunstthätigkeit im Kloster St. Trond. (XI. und XII.
              Jahrh.) Gesta abbatum Trudonensium</a>
            </li>
            <li>
              <a href="gd3004960s.html#279">XXXIV. Tractat des
              Gervasius über die Cathedrale von Canterbury.
              Gervasii Cantuariensis tractatus de combustione et
              reparatione Cantuariensis ecclesiae. (Anfang des
              XIII. Jahrh.)</a>
            </li>
            <li>
              <a href="gd3004960s.html#293">XXXV. St. Bernhard
              über den kirchlichen Luxus seiner Zeit. (1091-1153)
              S. Bernhardi Apologia ad Guillelmum abbatem s.
              Theoderici</a>
            </li>
            <li>
              <a href="gd3004960s.html#295">XXXVI. Suger's Bericht
              über seine Bauten in St. Denis (Mitte des XII.
              Jahrhunderts. Sugerii abbatis s. Dionysii Liber de
              rebus in administrazione sua gestis.</a>
            </li>
            <li>
              <a href="gd3004960s.html#317">XXXVII. Der Sattel der
              Enîte. Aus Hartmann von Aue (um 1200) Erec.</a>
            </li>
            <li>
              <a href="gd3004960s.html#321">XXXVIII. Der
              Kirchenschatz von Mainz im zwölften Jahrhundert.
              Christian, Erzbischof von Mainz (1249-1251).
              Chronicon Moguntinum&#160;</a>
            </li>
            <li>
              <a href="gd3004960s.html#325">XXXIX. Ein gothisccher
              Thronstuhl. "Die Erlösung", mittelhochdeutsches
              Gedicht um 1250</a>
            </li>
            <li>
              <a href="gd3004960s.html#328">XL. Der Tempel des h.
              Grals. Aus dem jüngeren Titurel des Albrecht von
              Scharfenberg (um 1270)</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gd3004960s.html#342">Drittes Buch. Vierzehntes
          und fünfzehntes Jahrhundert (Cap- XLI.-LIV.)</a>
          <ul>
            <li>
              <a href="gd3004960s.html#344">XLI. Die Glasgemälde
              von St. Albans. (Ende des XIV. Jahrhunderts?) Titutli
              "ex vetere m. s. in bibl. Boldieana I. E. 31."</a>
            </li>
            <li>
              <a href="gd3004960s.html#349">XLII. Der Palast der
              Fama. Chaucer, The house of Fame (geschrieben im
              Jahre 1383(</a>
            </li>
            <li>
              <a href="gd3004960s.html#352">XLIII. Die Wandgemälde
              der Bibliothek des Prämonstratenserstiftes in
              Brandenburg. (Anfang des XV. Jahrh.) Hartmann
              Schedel, Miscellancodex no. 418 der Münchener
              Bibliothek (geschrieben im Jahre 1466)&#160;</a>
            </li>
            <li>
              <a href="gd3004960s.html#358">XLIV. Allegorien des
              ausgehenden Mittelalters. Hartmann Schedel, Cod. lat.
              418. fol. 247, der Münchener Hof-und
              Staatsbibliothek&#160;</a>
            </li>
            <li>
              <a href="gd3004960s.html#361">XLV. Henri Baude's
              Verse auf Arazzi (2. Hälfte des XV. Jahrh.)&#160;</a>
            </li>
            <li>
              <a href="gd3004960s.html#369">XLVI. Beschreibung
              eines Palastes. A. Ordericus Vitalis von St. Evroult
              (Normandie), Historia ecclesiastica (geschr.
              1121-1141), Pars I., Lib. II, c. 14. B La
              intelligenzia, Poema in Nona Rima (del sec. XIV.)</a>
            </li>
            <li>
              <a href="gd3004960s.html#375">XLVII. Novelle von
              Giotto und Dante (1306). Benvenuto da Imola (um
              1350), Commentarius ad Dantis Comoediam&#160;</a>
            </li>
            <li>
              <a href="gd3004960s.html#376">XLVIII. Die Tafelrunde
              von San Miniato (um 1358). Franco Sacchetti
              (1335-1405). Novella 136</a>
            </li>
            <li>
              <a href="gd3004960s.html#378">XLIX. Petrarca's
              Sonette auf Simone Martini. Petrarca (gest. 1374),
              Sonetti e canzoni in vita di Madonna Laura</a>
            </li>
            <li>
              <a href="gd3004960s.html#379">L. Höfische Kunst im
              Trecento. Boccaccio (gest. 1375) Amorosa Visione</a>
            </li>
            <li>
              <a href="gd3004960s.html#394">LI. Die Tendenzgemälde
              des Cola di Rienzo in Rom. Vita di Cola di Rienzo (in
              altromaneskem Dialect, vor 1354</a>
            </li>
            <li>
              <a href="gd3004960s.html#397">LII. Filippo Villanis
              Lob der florentinischen Maler. Filippo Villani (um
              1404), de famosis civibus</a>
            </li>
            <li>
              <a href="gd3004960s.html#400">LIII. Lorenzo
              Ghibertis Nachrichten über die toscanischen Künstler
              des Trecento (Um 1452). Ghiberti, Commentario II.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gd3004960s.html#412">Nachträge und
          Berichtigungen</a>
        </li>
        <li>
          <a href="gd3004960s.html#415">Verzeichnis der
          Autoren</a>
        </li>
        <li>
          <a href="gd3004960s.html#416">II. Ortsregister</a>
        </li>
        <li>
          <a href="gd3004960s.html#419">III. Sachregister</a>
        </li>
        <li>
          <a href="gd3004960s.html#422">IV. Verzeichnis der
          Künstlernamen</a>
        </li>
        <li>
          <a href="gd3004960s.html#423">V. Verzeichnis der
          technischen Ausdrücke</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
