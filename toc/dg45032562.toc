<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg45032562s.html#7">Trattato delle cose più
      memorabili di Roma tanto antiche come moderne, che in esse di
      presente si trovano</a>
      <ul>
        <li>
          <a href="dg45032562s.html#10">Amico lettore</a>
        </li>
        <li>
          <a href="dg45032562s.html#11">[Trattato delle cose
          memorabili di Roma]</a>
        </li>
        <li>
          <a href="dg45032562s.html#462">Tavola delle cose più
          notabili che si contengono in questo secondo Tomo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
