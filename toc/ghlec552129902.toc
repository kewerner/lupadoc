<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlec552129902s.html#12">Cabinet Des Singularitez
      D’Architecture, Peinture, Sculpture, Et Graveure Ou
      Introduction A La Connoissance des plus beaux Arts, figurés
      sous les Tableaux les Statuës, &amp; les Estampes; Tome
      II.</a>
      <ul>
        <li>
          <a href="ghlec552129902s.html#14">A Monseigneur Jules
          Hardouin Mansart</a>
        </li>
        <li>
          <a href="ghlec552129902s.html#16">Preface</a>
        </li>
        <li>
          <a href="ghlec552129902s.html#20">Avertissement</a>
        </li>
        <li>
          <a href="ghlec552129902s.html#22">Reflexions sur le
          contenu en ce Volume</a>
        </li>
        <li>
          <a href="ghlec552129902s.html#26">Table des principaux
          sujets et noms des Peintres, Sculpteurs, et Graveurs,
          dont j'ay parlé dans ce Volume</a>
        </li>
        <li>
          <a href="ghlec552129902s.html#38">Le Cabinet des
          Singularitez d'Architecture, Peinture, Sculpture, et
          Graveure ou l'Introduction a la Connoissance des plus
          beaux arts&#160;</a>
          <ul>
            <li>
              <a href="ghlec552129902s.html#76">Ecole
              romaine</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#140">Ecoles de
              Lombardie et Venitienne&#160;</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#207">Ecole de
              Bologne, ou des Carraches</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#263">Peintres
              flamands, allemans, et hollandois</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#377">Explication de
              la Planche qui suit, où sont marqués des Caracteres
              de quelques Estampes d'Italie&#160;</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#382">Mariques
              d'autres Peintres et Graveurs italiens, dont voicy
              l'explication</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#387">Explication de
              la Planche qui suit, où sont marchés des Caracteres
              de quelques Estampes de Flandres et autres
              Ultramontains</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#391">Marques
              d'autres Peintres et Graveur ultramontains, dont
              voicy l'explication</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#394">Oeuvre de
              Claude Mellan</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#434">Oeuvre de
              Tempeste</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#452">Oeuvre de
              Guillelme Baurn</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#456">Catalogue de
              l'Oeuvre d'Abraham Bloemaert</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#480">Oeuvre de
              Jaques Callot</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#500">Catalogue
              d'Etienne de la Belle</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#516">Catalogue des
              Sadelers</a>
            </li>
            <li>
              <a href="ghlec552129902s.html#522">Catalogue de ce
              qui a été gravé d'aprés Monsieur Poussin</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
