<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dv24003840s.html#6">Piante, elevazioni profili e
      spaccati degli edifici della villa suburbana di Giulio III.
      Pontefice Massimo fuori la porta Flaminia</a>
      <ul>
        <li>
          <a href="dv24003840s.html#8">[Dedica a Pio Sesto]</a>
        </li>
        <li>
          <a href="dv24003840s.html#10">Prefazione</a>
        </li>
        <li>
          <a href="dv24003840s.html#13">Approvazione</a>
        </li>
        <li>
          <a href="dv24003840s.html#15">[Descripzioni e Tavole di
          ›Villa Giulia‹]</a>
          <ul>
            <li>
              <a href="dv24003840s.html#15">Tavola I.</a>
            </li>
            <li>
              <a href="dv24003840s.html#16">I. Pianta generale del
              grand'Edificio ◉</a>
            </li>
            <li>
              <a href="dv24003840s.html#19">II. Prospetto
              esteriore, o sia Facciata principale del Palazzo
              ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#20">Tavola II.</a>
            </li>
            <li>
              <a href="dv24003840s.html#21">Tavola III.</a>
            </li>
            <li>
              <a href="dv24003840s.html#22">III. Portone in grande
              del Palazzo con l'Ordine, e Loggia superiore ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#25">Tavola IV.</a>
            </li>
            <li>
              <a href="dv24003840s.html#26">IV. Profili, e
              spaccati colle Misure dei due Ordini della Facciata
              esteriore del Palazzo ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#29">Tavola V.</a>
            </li>
            <li>
              <a href="dv24003840s.html#30">V. Prospetto interiore
              del Palazzo ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#33">VI. Spaccato del
              Palazzo per il mezzo dell'Ingresso ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#34">Tavola VI.</a>
            </li>
            <li>
              <a href="dv24003840s.html#35">Tavola VII.</a>
            </li>
            <li>
              <a href="dv24003840s.html#36">VII. Pianta del
              Sotterraneo del Palazzo ◉</a>
            </li>
            <li>
              <a href="dv24003840s.html#39">Tavola VIII.</a>
            </li>
            <li>
              <a href="dv24003840s.html#40">VIII. Pianta del Piano
              terreno del Palazzo ◉</a>
            </li>
            <li>
              <a href="dv24003840s.html#43">IX. Pianta del Piano
              superiore del Palazzo ◉</a>
            </li>
            <li>
              <a href="dv24003840s.html#44">Tavola IX.</a>
            </li>
            <li>
              <a href="dv24003840s.html#45">Tavola X.</a>
            </li>
            <li>
              <a href="dv24003840s.html#46">X. [Profili dei
              capitelli e Piante] ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#49">Tavola XI.</a>
            </li>
            <li>
              <a href="dv24003840s.html#50">XI. Le due Finestre
              della Facciata principale del Palazzo ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#53">Tavola XII.</a>
            </li>
            <li>
              <a href="dv24003840s.html#54">XII. Profili,
              Spaccati, e Piante delle due Finestre antecedenti
              ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#57">Tavola XIII.</a>
            </li>
            <li>
              <a href="dv24003840s.html#58">XIII. Due Finestre
              grandi, che sono nel Mezzo delle Facciate principali
              del Palazzo ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#61">Tavola XIV.</a>
            </li>
            <li>
              <a href="dv24003840s.html#62">XIV. Profili,
              Spaccati, e Piante delle due antecedenti Finestre
              ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#65">Tavola XV.</a>
            </li>
            <li>
              <a href="dv24003840s.html#66">XV. Prospetto laterale
              per lungo della Piazza interna, che attacca col
              Palazzo ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#69">Tavola XVI.</a>
            </li>
            <li>
              <a href="dv24003840s.html#70">XVI. Parte media in
              grande del Prospetto laterale della Piazza interna
              ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#73">Tavola XVII.</a>
            </li>
            <li>
              <a href="dv24003840s.html#74">XVII. Prospetto
              interno della Piazza, di rimpetto al Palazzo ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#77">Tavola XVIII.</a>
            </li>
            <li>
              <a href="dv24003840s.html#78">XVIII. Pianta
              superiore dell'Edificio, che segue ◉</a>
            </li>
            <li>
              <a href="dv24003840s.html#81">Tavola XIX.</a>
            </li>
            <li>
              <a href="dv24003840s.html#82">XIX. Pianta media del
              sudetto Edificio ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#85">XX. Prospetto interno
              dell'Atrio aperto, da cui si scende al Ninfeo ove al
              di sotto sono i Bagni ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#86">Tavola XX.</a>
            </li>
            <li>
              <a href="dv24003840s.html#87">Tavola XXI.</a>
            </li>
            <li>
              <a href="dv24003840s.html#88">XXI. Altro Prospetto
              incontro al precedente nell'interno del Ninfeo con la
              Loggia superiore ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#91">Tavola XXII.</a>
            </li>
            <li>
              <a href="dv24003840s.html#92">XXII. Spaccato per
              lungo, che dall'Atrio coperto si scende al Ninfeo, e
              poi ai Bagni ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#95">XXIII. Modini diversi
              ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#96">Tavola XXIII.</a>
            </li>
            <li>
              <a href="dv24003840s.html#97">Tavola XXIV.</a>
            </li>
            <li>
              <a href="dv24003840s.html#98">XXIV. Porte laterali
              ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#101">XXV. Pianta interiore
              ove sono i Bagni ◉</a>
            </li>
            <li>
              <a href="dv24003840s.html#102">Tavola XXV.</a>
            </li>
            <li>
              <a href="dv24003840s.html#103">Tavola XXVI.</a>
            </li>
            <li>
              <a href="dv24003840s.html#104">XXVI. Spaccato per
              mezzo agl'Ingressi dei Bagni coperti ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#107">Tavola XXVII.</a>
            </li>
            <li>
              <a href="dv24003840s.html#108">XXVII. Pianta del
              Tempietto di Sant'Andrea Apostolo annesso alla Villa
              ◉</a>
            </li>
            <li>
              <a href="dv24003840s.html#111">XXVIII. Prospetto
              esteriore del Tempietto sulla Via Flaminia ▣</a>
            </li>
            <li>
              <a href="dv24003840s.html#112">Tavola XXVIII.</a>
            </li>
            <li>########## Seite fehlerhaft!</li>
            <li>
              <a href="dv24003840s.html#116">Tavola XXIX.</a>
            </li>
            <li>
              <a href="dv24003840s.html#117">Tavola XXX.</a>
            </li>
            <li>
              <a href="dv24003840s.html#118">Iscrizioni incise in
              Marmo ed esistenti una incontro l'altra nell'interno
              del Ninfeo...</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dv24003840s.html#120">Indice delle Tavole</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
