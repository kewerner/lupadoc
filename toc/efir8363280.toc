<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efir8363280s.html#6">Descrizione della Cappella di
      S. Antonino, arcivescovo di Firenze dell’ordine de’
      predicatori</a>
      <ul>
        <li>
          <a href="efir8363280s.html#8">Beatissimo Padre</a>
        </li>
        <li>
          <a href="efir8363280s.html#10">Descrizione della
          Cappella di Santo Antonino arcivescovo di Firenze</a>
        </li>
        <li>
          <a href="efir8363280s.html#56">Descrizione della
          traslazione del corpo di Santo Antonino</a>
        </li>
        <li>
          <a href="efir8363280s.html#83">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
