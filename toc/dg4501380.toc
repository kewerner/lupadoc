<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501380s.html#6">Pomponii Laeti De Antiqvitatibvs
      Vrbis Romae Libellus longè utilissimus</a>
      <ul>
        <li>
          <a href="dg4501380s.html#8">Rerum ac verborum in his de
          Antiquitatibus Romae libris memorabilium Index</a>
        </li>
        <li>
          <a href="dg4501380s.html#42">Amphitheatrum</a>
        </li>
        <li>
          <a href="dg4501380s.html#69">Epistome. Liber primus</a>
          <ul>
            <li>
              <a href="dg4501380s.html#69">I. De situ urbis</a>
            </li>
            <li>
              <a href="dg4501380s.html#76">II. De urbis Romae
              conditoribus, Regibus, atque cultoribus&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#80">III. De forma ac
              magnitudine urbis Romuli</a>
            </li>
            <li>
              <a href="dg4501380s.html#81">IIII. [sic!] De Portis
              urbis Romuli</a>
            </li>
            <li>
              <a href="dg4501380s.html#83">V. De vario ambitu, et
              moenibus urbis</a>
            </li>
            <li>
              <a href="dg4501380s.html#84">VI. De urbis
              pomerio</a>
            </li>
            <li>
              <a href="dg4501380s.html#85">VII. De Portis Romae
              antiquis quae hodie non extant et quae
              extant&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501380s.html#92">Liber secundus</a>
          <ul>
            <li>
              <a href="dg4501380s.html#92">I. De variis nomini bus
              capitolii</a>
            </li>
            <li>
              <a href="dg4501380s.html#93">II. Capitoli fundamenta
              quis primus iecerit, quis absolverit, quoties
              incensum, et aquibus restauratum</a>
            </li>
            <li>
              <a href="dg4501380s.html#94">III. De templis Iovis
              Feretrij, Iovis Optimus Maximus Iunonis et
              Minervae&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#96">IIII. [sic!] De Saturni
              templo, Iovis Tonantis, et Iovis Custodis, Fortunae,
              Vaeiovis, Misericordiae, Iuononis Monetae, et Iani
              templo, Orestis sepulchro, Aerario publico, Domo
              Manlij, fanoq, Carmentae</a>
            </li>
            <li>
              <a href="dg4501380s.html#100">V. De temlis deorum
              incertam sedem nunc habentibus in Capitolio</a>
            </li>
            <li>
              <a href="dg4501380s.html#103">VI. De statuis quae in
              Capitolio olim suere, vel etiamnum sunt&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#106">VII. Statue Viris
              illustribus in Capitolio con stitutae&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#108">VIII. De Clivo
              Capitolino, Tarpeia Rupe, Saxo Carmentali, et Porta
              Stercoraria&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#109">IX. De tabulis,
              columnis aeneis, ansere argento</a>
            </li>
            <li>
              <a href="dg4501380s.html#111">X. De quibus
              aedificijs in Capitolio, alijsque rebus in genere</a>
            </li>
            <li>
              <a href="dg4501380s.html#114">XI. De Concordiae et
              Iovis Statoris templo: denique quid sit Curia</a>
            </li>
            <li>
              <a href="dg4501380s.html#116">XII. De templo ac luco
              Vestae, et templo Fidei, atque Romuli</a>
            </li>
            <li>
              <a href="dg4501380s.html#117">XIII. De Lupercali
              sicuque Ruminali</a>
            </li>
            <li>
              <a href="dg4501380s.html#120">XIIII. [sic!] De via
              Nova, vico Iugario, et Thusco vico</a>
            </li>
            <li>
              <a href="dg4501380s.html#121">XV. De arcubus Romuli,
              Senatulo, et Ovidij domo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501380s.html#122">Liber tertius &#160;</a>
          <ul>
            <li>
              <a href="dg4501380s.html#122">I. De varia
              palatii</a>
            </li>
            <li>
              <a href="dg4501380s.html#124">II. De Victoriae,
              Cereris, Iunonis Sopitae, Matris deum, et Libtertatis
              templo, et eiusdem porticu ac de Caßij domo</a>
            </li>
            <li>
              <a href="dg4501380s.html#126">III. De Fano et Ara
              Febris, Aede Larum, dea Viriplacae sacello, templo
              Fidei, et Iovis Victoris aede&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#127">IIII. [sic!] De
              Heliogabali, Apollinis, et Penatium templo, de aede
              Orci, loco Palladij, et templo Augusti</a>
            </li>
            <li>
              <a href="dg4501380s.html#129">V. De domibus
              Palatinis, et Saliorum Curia</a>
            </li>
            <li>
              <a href="dg4501380s.html#131">VI. De Summa Velia,
              Statuis palatinis, alijsque rebus in genere</a>
            </li>
            <li>
              <a href="dg4501380s.html#133">VII. De Palatio nunc
              Maiori, Atrio, et veteri ipsius Palatij porta</a>
            </li>
            <li>
              <a href="dg4501380s.html#134">VIII. De templo Iani,
              Augusti, et Faustinae</a>
            </li>
            <li>
              <a href="dg4501380s.html#136">IX. De Castoris et
              Pollucis templo et Rostris</a>
            </li>
            <li>
              <a href="dg4501380s.html#137">X. De Caesaris,
              Veneris genitricis: et Martis Ultoris templo, et
              Atrio Veneris</a>
            </li>
            <li>
              <a href="dg4501380s.html#138">XI. De Vulcani,
              Concordiae, Pacis, Romulus, ac Veneris Cloacinae
              templo</a>
            </li>
            <li>
              <a href="dg4501380s.html#140">XII. De Telluris,
              Victoriae, Solis et Lunae templis, de domo Caßij,
              deque Armamentario</a>
            </li>
            <li>
              <a href="dg4501380s.html#142">XIII. De Foro Traiani,
              deque ijs quae in eo sunt, vel olimj suere</a>
            </li>
            <li>
              <a href="dg4501380s.html#144">XIIII. [sic!] De foro,
              Palatio, et Porticu Nervae&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#145">XV. De foro Romano,
              Comitio, Carcere Tulliano, et Marforij
              simulachro&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#147">XVI. De Secretario
              Popolo Romano Arcu Septimij, templo Saturnicae de
              Aerario, et offinina cudendi pecuniam</a>
            </li>
            <li>
              <a href="dg4501380s.html#149">XVII. De Miliario
              Aureo, Ponte Caliguae, et de Rostris ac Curia</a>
            </li>
            <li>
              <a href="dg4501380s.html#152">XVIII. De Lacu Curtio,
              Cloaca Maxima, et Doliola</a>
            </li>
            <li>
              <a href="dg4501380s.html#154">XIX. De Comitio,
              templo Veneris Genitricis, statuis et simulachris,
              quae in Comitio, et foro Romano fuere, ac de Caesaris
              statua atque columna</a>
            </li>
            <li>
              <a href="dg4501380s.html#156">XX. De Columna Moenia,
              Pila Horatia, de Caesaris, Constantini, et Domitiani
              equo deq templo ac area eiusdem Caesaris&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#157">XXI. De foro
              Caesaeris, Basilica Pauli, et eiusdem Bibliotheca</a>
            </li>
            <li>
              <a href="dg4501380s.html#158">XXII. De foro Augusti,
              Porticus Antonini, Faustinae, et Liviae, ac de
              Caesaris domo</a>
            </li>
            <li>
              <a href="dg4501380s.html#160">XXIII. De Sororio
              Tigillo, Vico Cyprio, Scelerato et Patri cio, atque
              M. Antonij domo</a>
            </li>
            <li>
              <a href="dg4501380s.html#161">XXIIII. [sic!] De
              Bustis Gallicis, et Acquimelio</a>
            </li>
            <li>
              <a href="dg4501380s.html#161">XXV. De via Sacra,
              Arcu Fabiano et Vespa siano</a>
            </li>
            <li>
              <a href="dg4501380s.html#163">XXVI. De Graecostasi,
              Senaculo, Curia, et Basilica Opimij et Portij</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501380s.html#165">Liber quartus</a>
          <ul>
            <li>
              <a href="dg4501380s.html#165">I. De Temploiani et
              pietatis, ac de carcere plebis Romanae&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#169">II. De foro Holitorio,
              Aede Iunonis Matutae, et Spei, Sacrario Numae,
              columna Lactaira, ac ara, et Fano Carmentae</a>
            </li>
            <li>
              <a href="dg4501380s.html#170">III. De foro Piscario,
              et templo Fortunae Virilis</a>
            </li>
            <li>
              <a href="dg4501380s.html#171">IIII. [sic!] De templo
              Vestae, Salinis, Coclitis arcu</a>
            </li>
            <li>
              <a href="dg4501380s.html#172">V. De foro Boario</a>
            </li>
            <li>
              <a href="dg4501380s.html#173">VI. De templo Herculis
              Victoris, et ara Maxima</a>
            </li>
            <li>
              <a href="dg4501380s.html#174">VII. De Pudicitiae,
              Fortunae Prosperae, Matutae, Fortis Fortunae aede, ac
              de Vico publico, Velabro, et Accae Laurentiae
              sepulchro</a>
            </li>
            <li>
              <a href="dg4501380s.html#177">VIII. De Vertumno,
              eiusquam templo, Iano Quadrifronte, aedibus Africani,
              Basilica Sempronia, ac de Laneis Tabernis</a>
            </li>
            <li>
              <a href="dg4501380s.html#178">IX. De Argileto, domo
              Cornelij, Aequimelio, et Socordiae sacello</a>
            </li>
            <li>
              <a href="dg4501380s.html#180">X. Circus quid sit,
              unde dectus, et civis rei causa institutus</a>
            </li>
            <li>
              <a href="dg4501380s.html#182">XI. De Circo
              Maximo&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#184">XII. De templis et
              aris quae in circo Maximo, vel in circuitu fuiße
              leguntur</a>
            </li>
            <li>
              <a href="dg4501380s.html#186">XIII. De Naumachia
              circi Maximi</a>
            </li>
            <li>
              <a href="dg4501380s.html#186">XIIII. [sic!] De
              Obeliscis duobus circi Maximi</a>
            </li>
            <li>
              <a href="dg4501380s.html#188">XV. De Fornice
              Sertinij in Circo, Loco Tuberonum in circi, et
              theatris, Lupanaribus, Pompeij domo, et luturnae
              fonte</a>
            </li>
            <li>
              <a href="dg4501380s.html#189">XVI. De Septizonio
              Severi</a>
            </li>
            <li>
              <a href="dg4501380s.html#191">XVII. De arcu
              triumphali Constantini Imperatoris</a>
            </li>
            <li>
              <a href="dg4501380s.html#192">XVIII. De Coelio monte
              et Coeliolo</a>
            </li>
            <li>
              <a href="dg4501380s.html#193">XIX. De Fauni,
              Veneris, et Cupidinis templo, Curia Hostilia, Castris
              peregrinis, domo Lateranorum, de Constantini et
              Sesorianij Palatio, ac de L. Veri statua euqestri</a>
            </li>
            <li>
              <a href="dg4501380s.html#194">XX. De Amphitheatris,
              et in primis Statilij Tauri</a>
            </li>
            <li>
              <a href="dg4501380s.html#196">XXI. Acquaeductus cur
              inventi, quibus modis acquae ducerectur, cui usui,
              quis primus deduxerit, et quot acquae deductae</a>
            </li>
            <li>
              <a href="dg4501380s.html#199">XXIII. [sic!] De
              aquaeductu aquae Claudiae&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#200">XXIII. De ijs quae
              incertam sedem nunc in Coelio monte habent</a>
            </li>
            <li>
              <a href="dg4501380s.html#201">XXIIII [sic!] De Appia
              Nova via, ac de Isidis, Virtutis et Honoris, Quirini
              sive Martis aede, et Almone flumine</a>
            </li>
            <li>
              <a href="dg4501380s.html#203">XXV. De Thermis in
              genere</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501380s.html#206">Liber quintus</a>
          <ul>
            <li>
              <a href="dg4501380s.html#206">I. De Aventini Montis
              aetymologia et iis quae in eo memoratu digna</a>
            </li>
            <li>
              <a href="dg4501380s.html#207">II. De Dianae, Bonae
              deae, Herculis Victoris, Iunonis Reginae, Monetae,
              Lunae, Victoriae, Minervae, et Libertatis templo, et
              ara Elicij Iovis</a>
            </li>
            <li>
              <a href="dg4501380s.html#210">De Caco, eiusque
              spelunca&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#211">IIII. [sic!] De
              Armilustrio et quibusdam alijs rebus in genere</a>
            </li>
            <li>
              <a href="dg4501380s.html#212">V. De Testaceo monte,
              et deijs quae in eodem ambitu fuere&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#214">VI. De Pyramidibus, et
              sepulchro C. Cestij, ac Horeis Populum Romanum</a>
            </li>
            <li>
              <a href="dg4501380s.html#215">VII. De meta quae
              Sudans dicta est, et Iovis simulachro</a>
            </li>
            <li>
              <a href="dg4501380s.html#217">VIII. De Amphitheatro
              Titi Vespasiani, ac de templo Fortunae et Quietis</a>
            </li>
            <li>
              <a href="dg4501380s.html#218">IX. De
              Esquilijs&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#219">X. Da Carinis, Curia
              Veteri, et Nova</a>
            </li>
            <li>
              <a href="dg4501380s.html#219">XI. De thermis Titi,
              et Philippi, Statua Laocoontis, Palatio Vespasiant,
              domo Balbini et Pompeij</a>
            </li>
            <li>
              <a href="dg4501380s.html#221">XII. De Clivio Virbio,
              domibus Ser. Tullij, Aurea Neronis, Vergilij, ac de
              turri Mecoenatis et eiusdem Hortis, simulque de
              Fortunae et Felicitatis templo</a>
            </li>
            <li>
              <a href="dg4501380s.html#223">XIII. De Sisimini
              Basilica, Capo et foro Esquilino, Luco Querquetulano,
              Iunonis Laciniae, et Martis, de Fano et ara malae
              Fortunae</a>
            </li>
            <li>
              <a href="dg4501380s.html#224">XIIII. [sic!] Declivo
              Suburrano, Arcu Galieni, Macello Libyae, via
              Praenestina, et Marij trophaeis</a>
            </li>
            <li>
              <a href="dg4501380s.html#226">XV. De domo Aeliorum,
              Sacello Mariano, regione Tabernola, Thermis et domo
              Gordiani, Basilica Caij et Lucij, ac de Palatio
              Liciniano</a>
            </li>
            <li>
              <a href="dg4501380s.html#227">XVI. De acqua Martia
              sive Traiana, et Isidis templo</a>
            </li>
            <li>
              <a href="dg4501380s.html#228">XVII. De Suburra, domo
              Caesaris, et Leliae, ac de vico Patricio</a>
            </li>
            <li>
              <a href="dg4501380s.html#228">XVIII. De Suburra
              Plana, templo Sylvani</a>
            </li>
            <li>
              <a href="dg4501380s.html#229">XIX. De Viminali,
              Palatio Decij, Lavacro Aggripinae, Thermis
              Olympiadis, et Novati, ac de domo Q. Catuli, Craßi,
              et Aquilij</a>
            </li>
            <li>
              <a href="dg4501380s.html#230">XX. De thermis
              Dioclitiani, Bibliotheca Vulpia, Campo Viminalis,
              porta Interaggeres, et Quirinali valle</a>
            </li>
            <li>
              <a href="dg4501380s.html#232">XXI. De monte Caballo,
              Quirinalis etymologia, Turri Militiarum, Balineis
              Pauli, Sacello Neptuni Thermis Constantini, de domo
              et vico Corneliorum</a>
            </li>
            <li>
              <a href="dg4501380s.html#233">XXII. De Saturni,
              Bacchi, Solis, et Quirini templo, et Porticu,
              Capitolio veteris, Sacello Iovis, Iuononis, et
              Minervae, et de domo Pomp. Attici</a>
            </li>
            <li>
              <a href="dg4501380s.html#235">XXIII. De Alta Semita,
              domo Sabini, Vico et Statua Mamurri, foro et hortis
              Sallustij, ac de Campo Scelerato</a>
            </li>
            <li>
              <a href="dg4501380s.html#237">XXIIII. [sic!] De
              templo Salutis, Dij Fidij, Fortunae Primigeniae,
              Honoris, Herculis, Quirini, et Senatulo mulierum</a>
            </li>
            <li>
              <a href="dg4501380s.html#238">XXXV. De Foro
              Archimonij, Pila Tirburtina, domo Martialis, Circo
              Floralium, Florae templo, Officinis minij, et Clivio
              publico</a>
            </li>
            <li>
              <a href="dg4501380s.html#239">XXXVI. De Colle
              Hortulorum, templo Solis, et sepulchro Neronis</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501380s.html#240">Liber sextus</a>
          <ul>
            <li>
              <a href="dg4501380s.html#240">I. De urbe plana et
              theatro in genere</a>
            </li>
            <li>
              <a href="dg4501380s.html#241">II. De Theatro
              Marcelli, et Bibliotheca, de porticu et curia
              Ovtaviae</a>
            </li>
            <li>
              <a href="dg4501380s.html#243">III. De Circo
              Flaminio, et templo Apolinis</a>
            </li>
            <li>
              <a href="dg4501380s.html#244">IIII. [sic!] De
              Vulcani, Martis, Bellonae, Herculis, Iovis Statoris
              templo, de Columna Bellica, Ara Neptuni, Porticu
              Corinthia, et Colosso Martis</a>
            </li>
            <li>
              <a href="dg4501380s.html#246">V. De Porticu
              Mercurij, Octavij theatro, domo, atrio, et Porticu
              eiusdem, ac de templo Veneris Victricis</a>
            </li>
            <li>
              <a href="dg4501380s.html#247">VI. De Thermis
              Agrippinis, Pantheone, eiusque porticu, ac Boni
              Eventus templo</a>
            </li>
            <li>
              <a href="dg4501380s.html#249">VII. De thermis
              Neronianis, et Alexandrinis, et de Circo qui Agon
              dicitur</a>
            </li>
            <li>
              <a href="dg4501380s.html#250">IX. De Neptuni templo,
              Terento, Ara Ditis, ac Palude Caprea</a>
            </li>
            <li>
              <a href="dg4501380s.html#251">X. De domo Corvina,
              via Lata, et templo Isidis</a>
            </li>
            <li>
              <a href="dg4501380s.html#252">XI. De arcu Camili,
              templo Minervae, et foro Suario</a>
            </li>
            <li>
              <a href="dg4501380s.html#253">XII. De Campo Martio,
              sive Tyberino</a>
            </li>
            <li>
              <a href="dg4501380s.html#254">XIII. De porticu,
              templo, columna, et Palatio antonini Oij, ac de
              Septis, sive Ovilibus</a>
            </li>
            <li>
              <a href="dg4501380s.html#255">XIIII. [sic!] De monte
              Citatorum, Villa Publica, Templo Neptuni, et Ponte in
              Campo Martio</a>
            </li>
            <li>
              <a href="dg4501380s.html#256">XV. De acqua Virgine,
              Lacu et Aede Iuturnae, ac Pietatis</a>
            </li>
            <li>
              <a href="dg4501380s.html#257">XVI. De arcu
              Domitiani, Obelisco campi Martij, et Amphitheatro
              Claudij Imperatoris</a>
            </li>
            <li>
              <a href="dg4501380s.html#258">XVII. De Valle Martia,
              Palatio, et Porticu Augusti, de Domitiani Naumachia,
              et Flaviae gentis templo&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#259">XVIII. De Augusti
              Mausoleo, et iuxta eum Obeliscis duobus, deque
              Marcelli sepulchro</a>
            </li>
            <li>
              <a href="dg4501380s.html#260">XIX. De Via Flaminia,
              Trohaeis Marij, et quibusdam alijs ornamentis campi
              Martij in genere</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501380s.html#261">Liber septimus</a>
          <ul>
            <li>
              <a href="dg4501380s.html#261">I. De Tyberino
              Flumine</a>
            </li>
            <li>
              <a href="dg4501380s.html#262">II. De Pontibus supra
              Tyberim extructis</a>
            </li>
            <li>
              <a href="dg4501380s.html#263">III. De Ponte
              Milvio</a>
            </li>
            <li>
              <a href="dg4501380s.html#264">IIII. [sic!] De ponte
              Aelio, nunc S. Angeli, Vaticano, sive Triumphali, ac
              laniculensi, sive Aureliano</a>
            </li>
            <li>
              <a href="dg4501380s.html#265">V. De Ponte Fabricio,
              et Cestio</a>
            </li>
            <li>
              <a href="dg4501380s.html#265">VI. De Insula
              Tyberina</a>
            </li>
            <li>
              <a href="dg4501380s.html#266">VII. De Ponte
              Senatorum. sive Palatino, et ponte Sublicio</a>
            </li>
            <li>
              <a href="dg4501380s.html#268">VIII. De Transtyberina
              regione, Ravenatium, et Fortis Fortunae templo,
              Thermis Saverianis, hortis Caesaris, ac de aqua
              Alfietina, et de pratis Mutijs</a>
            </li>
            <li>
              <a href="dg4501380s.html#269">IX. De sepulchro
              Numae, et Caecilij poetae, Tribunali Aureliano,
              Ianiculo, et hortis Martialis</a>
            </li>
            <li>
              <a href="dg4501380s.html#270">X. De monte et Campo
              Vaticano, templo Apollinis, et Martis, Naumachia,
              Circo, et hortis ac, de Obelisco Caesaris</a>
            </li>
            <li>
              <a href="dg4501380s.html#271">XI. De via Triumphali,
              acqua Sabbatina, sepulchro Scipionis</a>
            </li>
            <li>
              <a href="dg4501380s.html#272">XII. De Mole Hadriani
              et Pratis Quintijs</a>
            </li>
            <li>
              <a href="dg4501380s.html#273">XIII. De ijs quae
              extrs portam Flumentana, et Collatinam fuere, vel
              nunc sunt&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#274">XIIII. [sic!] De ijs
              extra portam fuiße traduntur</a>
            </li>
            <li>
              <a href="dg4501380s.html#275">XV. De ijs quae extra
              Nomentanam portam, et Interaggeres fuere, vel hodie
              sunt&#160;</a>
            </li>
            <li>
              <a href="dg4501380s.html#276">XVI. De ijs quae extra
              portam Esquilinam, Naeviam, Coelimon tanam, et
              Gabiusam, fuere, vel sunt</a>
            </li>
            <li>
              <a href="dg4501380s.html#277">XVII. De ijs quae
              extra portam Latinam et Capenam fuere vel sunt</a>
            </li>
            <li>
              <a href="dg4501380s.html#279">XVIII. De ijs quae
              extra portam Hostiensem, et reliquas in genere</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501380s.html#280">Pub Victoris de urbis Romae
      regionibus et locis libellus</a>
    </li>
  </ul>
  <hr />
</body>
</html>
