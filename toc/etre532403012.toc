<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="etre532403012s.html#8">Memorie trevigiane sulle
      opere di disegno dal mille e cento al mille ottocento; volume
      primo</a>
      <ul>
        <li>
          <a href="etre532403012s.html#14">Prefazione</a>
        </li>
        <li>
          <a href="etre532403012s.html#46">Prima
          parte&#160;</a>
          <ul>
            <li>
              <a href="etre532403012s.html#49">Indice</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="etre532403012s.html#202">Parte seconda</a>
          <ul>
            <li>
              <a href="etre532403012s.html#209">Indice</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="etre532403012s.html#292">Memorie trevigiane
      sulle opere di disegno dal mille e cento al mille ottocento;
      volume secondo</a>
      <ul>
        <li>
          <a href="etre532403012s.html#294">Prefazione</a>
        </li>
        <li>
          <a href="etre532403012s.html#297">Indice</a>
        </li>
        <li>
          <a href="etre532403012s.html#412">Parte
          terza&#160;</a>
          <ul>
            <li>
              <a href="etre532403012s.html#414">Prefazione</a>
            </li>
            <li>
              <a href="etre532403012s.html#417">Indice</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="etre532403012s.html#528">Indice primo dei
      Documenti riguardanti le Belle Arti, riportati ne' due Volumi
      delle Memorie Trevigiane</a>
    </li>
    <li>
      <a href="etre532403012s.html#535">Indice secondo degli
      artisti, e fautori delle belle arti</a>
    </li>
    <li>
      <a href="etre532403012s.html#548">Indice terzo delle
      chiese, luoghi pubblici, e Palazzi della città di Trevigi</a>
    </li>
    <li>
      <a href="etre532403012s.html#553">Indice quarto delle
      città, castella, e villaggi del Trevigiano</a>
    </li>
    <li>
      <a href="etre532403012s.html#563">Indice quinto delle
      cose più notabili</a>
    </li>
    <li>
      <a href="etre532403012s.html#569">Indice sesto delle
      Tavole imprese, e delle Figure</a>
    </li>
    <li>
      <a href="etre532403012s.html#570">Indice degl' Indici</a>
    </li>
  </ul>
  <hr />
</body>
</html>
