<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="bb7801920s.html#8">Monumentorum Italiae, quae hoc
      nostro saeculo &amp; à Christianis posita sunt, libri
      quatuor</a>
      <ul>
        <li>
          <a href="bb7801920s.html#10">Illustrissimo ac
          reverendissimo in Christo Principi ac Domino, Domino
          Iohanni Adolpho</a>
        </li>
        <li>
          <a href="bb7801920s.html#22">Liber primus</a>
          <ul>
            <li>
              <a href="bb7801920s.html#24">Tridentum</a>
            </li>
            <li>
              <a href="bb7801920s.html#30">Patavium</a>
            </li>
            <li>
              <a href="bb7801920s.html#93">Rhodigium</a>
            </li>
            <li>
              <a href="bb7801920s.html#93">Garopholum</a>
            </li>
            <li>
              <a href="bb7801920s.html#94">Ferrraria</a>
            </li>
            <li>
              <a href="bb7801920s.html#110">Bononia</a>
            </li>
            <li>
              <a href="bb7801920s.html#156">Florentiola</a>
            </li>
            <li>
              <a href="bb7801920s.html#156">Scarparia</a>
            </li>
            <li>
              <a href="bb7801920s.html#156">Florentia</a>
            </li>
            <li>
              <a href="bb7801920s.html#172">Pistorium</a>
            </li>
            <li>
              <a href="bb7801920s.html#173">Luca</a>
            </li>
            <li>
              <a href="bb7801920s.html#179">Pisae</a>
            </li>
            <li>
              <a href="bb7801920s.html#185">Labro</a>
            </li>
            <li>
              <a href="bb7801920s.html#185">Certaldum</a>
            </li>
            <li>
              <a href="bb7801920s.html#185">Volterrae</a>
            </li>
            <li>
              <a href="bb7801920s.html#186">Senae</a>
            </li>
            <li>
              <a href="bb7801920s.html#201">Monsalcinoi</a>
            </li>
            <li>
              <a href="bb7801920s.html#201">Sanctus Quiricus</a>
            </li>
            <li>
              <a href="bb7801920s.html#201">Aquapendens</a>
            </li>
            <li>
              <a href="bb7801920s.html#201">Vulsinia</a>
            </li>
            <li>
              <a href="bb7801920s.html#201">Oropytum</a>
            </li>
            <li>
              <a href="bb7801920s.html#202">Mons Falsicorum</a>
            </li>
            <li>
              <a href="bb7801920s.html#202">Viterbium</a>
            </li>
            <li>
              <a href="bb7801920s.html#209">Roncillonum</a>
            </li>
            <li>
              <a href="bb7801920s.html#210">Tybur</a>
            </li>
            <li>
              <a href="bb7801920s.html#212">Farfa</a>
            </li>
            <li>
              <a href="bb7801920s.html#212">Sublaco</a>
            </li>
            <li>
              <a href="bb7801920s.html#212">Praeneste</a>
            </li>
            <li>
              <a href="bb7801920s.html#213">Tusculum</a>
            </li>
            <li>
              <a href="bb7801920s.html#213">Alba Longa</a>
            </li>
            <li>
              <a href="bb7801920s.html#214">Ostia</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801920s.html#216">Librus secundus</a>
          <ul>
            <li>
              <a href="bb7801920s.html#218">Illustrissimo ac
              reverendissimo in Christo Principi ac Domino, Domino
              Henrico Iulio</a>
            </li>
            <li>
              <a href="bb7801920s.html#224">Roma</a>
            </li>
            <li>
              <a href="bb7801920s.html#439">Lamum</a>
            </li>
            <li>
              <a href="bb7801920s.html#439">Velitrae</a>
            </li>
            <li>
              <a href="bb7801920s.html#439">Sermonetta</a>
            </li>
            <li>
              <a href="bb7801920s.html#439">Setia</a>
            </li>
            <li>
              <a href="bb7801920s.html#439">Privernum</a>
            </li>
            <li>
              <a href="bb7801920s.html#440">Forum Apii</a>
            </li>
            <li>
              <a href="bb7801920s.html#440">Anxur</a>
            </li>
            <li>
              <a href="bb7801920s.html#440">Fundi</a>
            </li>
            <li>
              <a href="bb7801920s.html#441">Itrium</a>
            </li>
            <li>
              <a href="bb7801920s.html#441">Formiae</a>
            </li>
            <li>
              <a href="bb7801920s.html#442">Minturnae</a>
            </li>
            <li>
              <a href="bb7801920s.html#442">Rocca</a>
            </li>
            <li>
              <a href="bb7801920s.html#443">Linternum</a>
            </li>
            <li>
              <a href="bb7801920s.html#443">Neapolis</a>
            </li>
            <li>
              <a href="bb7801920s.html#515">Vesuvius</a>
            </li>
            <li>
              <a href="bb7801920s.html#516">Atella</a>
            </li>
            <li>
              <a href="bb7801920s.html#517">Capua Vetus</a>
            </li>
            <li>
              <a href="bb7801920s.html#517">Capua Nova</a>
            </li>
            <li>
              <a href="bb7801920s.html#519">Vulturnum</a>
            </li>
            <li>
              <a href="bb7801920s.html#519">Suessa</a>
            </li>
            <li>
              <a href="bb7801920s.html#520">Caieta</a>
            </li>
            <li>
              <a href="bb7801920s.html#521">Antium</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801920s.html#526">Liber tertius</a>
          <ul>
            <li>
              <a href="bb7801920s.html#528">Illustrissimo ac
              reverendissimo in Christo Principi ac Domino, Domino
              Philippo Sigismundo</a>
            </li>
            <li>
              <a href="bb7801920s.html#532">A Castelnovo</a>
            </li>
            <li>
              <a href="bb7801920s.html#532">Otriculum</a>
            </li>
            <li>
              <a href="bb7801920s.html#532">Narnia</a>
            </li>
            <li>
              <a href="bb7801920s.html#532">Interamne</a>
            </li>
            <li>
              <a href="bb7801920s.html#533">Spoletum</a>
            </li>
            <li>
              <a href="bb7801920s.html#535">Fulginia</a>
            </li>
            <li>
              <a href="bb7801920s.html#537">Assisium</a>
            </li>
            <li>
              <a href="bb7801920s.html#538">Perusia</a>
            </li>
            <li>
              <a href="bb7801920s.html#547">Faberiana</a>
            </li>
            <li>
              <a href="bb7801920s.html#548">Matelica</a>
            </li>
            <li>
              <a href="bb7801920s.html#548">Sanseverinum</a>
            </li>
            <li>
              <a href="bb7801920s.html#548">Ricinetum</a>
            </li>
            <li>
              <a href="bb7801920s.html#549">Loretum</a>
            </li>
            <li>
              <a href="bb7801920s.html#551">Ancona</a>
            </li>
            <li>
              <a href="bb7801920s.html#557">Camerinum</a>
            </li>
            <li>
              <a href="bb7801920s.html#557">Firmium</a>
            </li>
            <li>
              <a href="bb7801920s.html#558">Senogallia</a>
            </li>
            <li>
              <a href="bb7801920s.html#558">Fanum Fortunae</a>
            </li>
            <li>
              <a href="bb7801920s.html#561">Forum Sempronium</a>
            </li>
            <li>
              <a href="bb7801920s.html#562">Pisarum</a>
            </li>
            <li>
              <a href="bb7801920s.html#563">Urbinum</a>
            </li>
            <li>
              <a href="bb7801920s.html#569">Tolentinum</a>
            </li>
            <li>
              <a href="bb7801920s.html#570">Ariminum</a>
            </li>
            <li>
              <a href="bb7801920s.html#572">Cervia</a>
            </li>
            <li>
              <a href="bb7801920s.html#573">Ravenna</a>
            </li>
            <li>
              <a href="bb7801920s.html#579">Primaro</a>
            </li>
            <li>
              <a href="bb7801920s.html#579">Fossa Clodia&#160;</a>
            </li>
            <li>
              <a href="bb7801920s.html#579">Venetiae</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801920s.html#640">Liber quartus</a>
          <ul>
            <li>
              <a href="bb7801920s.html#642">Illustri ac generoso
              Domino, Domino Simono, Cimiti Lippiae</a>
            </li>
            <li>
              <a href="bb7801920s.html#648">Vicentia</a>
            </li>
            <li>
              <a href="bb7801920s.html#657">Verona</a>
            </li>
            <li>
              <a href="bb7801920s.html#669">Mantua</a>
            </li>
            <li>
              <a href="bb7801920s.html#688">Cremona</a>
            </li>
            <li>
              <a href="bb7801920s.html#694">Brixia</a>
            </li>
            <li>
              <a href="bb7801920s.html#701">Bergomum</a>
            </li>
            <li>
              <a href="bb7801920s.html#705">Crema</a>
            </li>
            <li>
              <a href="bb7801920s.html#706">Laus Pompeia</a>
            </li>
            <li>
              <a href="bb7801920s.html#708">Ticinum</a>
            </li>
            <li>
              <a href="bb7801920s.html#720">Mediolanum</a>
            </li>
            <li>
              <a href="bb7801920s.html#739">Comum</a>
            </li>
            <li>
              <a href="bb7801920s.html#742">Novaria</a>
            </li>
            <li>
              <a href="bb7801920s.html#743">Vercellae</a>
            </li>
            <li>
              <a href="bb7801920s.html#747">Taurinum</a>
            </li>
            <li>
              <a href="bb7801920s.html#756">Cherium</a>
            </li>
            <li>
              <a href="bb7801920s.html#758">Asta</a>
            </li>
            <li>
              <a href="bb7801920s.html#759">Alba Pompeia</a>
            </li>
            <li>
              <a href="bb7801920s.html#761">Mons Regalis</a>
            </li>
            <li>
              <a href="bb7801920s.html#762">Savona</a>
            </li>
            <li>
              <a href="bb7801920s.html#763">Genua</a>
            </li>
            <li>
              <a href="bb7801920s.html#777">Alexandria</a>
            </li>
            <li>
              <a href="bb7801920s.html#781">Alboscho</a>
            </li>
            <li>
              <a href="bb7801920s.html#781">Dorthona</a>
            </li>
            <li>
              <a href="bb7801920s.html#783">Placentia</a>
            </li>
            <li>
              <a href="bb7801920s.html#789">Parma</a>
            </li>
            <li>
              <a href="bb7801920s.html#794">Regium Lepidi</a>
            </li>
            <li>
              <a href="bb7801920s.html#800">Mutina</a>
            </li>
            <li>
              <a href="bb7801920s.html#805">Imola</a>
            </li>
            <li>
              <a href="bb7801920s.html#808">Faventia</a>
            </li>
            <li>
              <a href="bb7801920s.html#811">Forum Livii</a>
            </li>
            <li>
              <a href="bb7801920s.html#814">Cesena</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="bb7801920s.html#817">Exemplum cuiusdam
          membranae de moribus Italorum</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
