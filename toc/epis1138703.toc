<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="epis1138703s.html#6">Pisa illustrata nelle arti
      del disegno; tomo terzo</a>
      <ul>
        <li>
          <a href="epis1138703s.html#8">Prefazione</a>
        </li>
        <li>
          <a href="epis1138703s.html#14">Indice dei titoli di
          questo terzo tomo</a>
        </li>
        <li>
          <a href="epis1138703s.html#22">Parte I. Storia delle
          chiese, e di altri pubblici edifizj per cio che spetta
          alle bell'arti, all'antiquitaria, ed alla cronologia</a>
          <ul>
            <li>
              <a href="epis1138703s.html#22">I. Fabbriche
              attenenti al sacro militar'ordine di Santo
              Stefano&#160;</a>
            </li>
            <li>
              <a href="epis1138703s.html#68">II. San
              Francesco</a>
            </li>
            <li>
              <a href="epis1138703s.html#126">III. Santa
              Caterina</a>
            </li>
            <li>
              <a href="epis1138703s.html#147">IV. San
              Frediano</a>
            </li>
            <li>
              <a href="epis1138703s.html#163">V. San Niccola</a>
            </li>
            <li>
              <a href="epis1138703s.html#179">VI. San Michele in
              Borgo</a>
            </li>
            <li>
              <a href="epis1138703s.html#205">VII. San
              Matteo</a>
            </li>
            <li>
              <a href="epis1138703s.html#215">VIII. San
              Silvestro, e Santa Marta</a>
            </li>
            <li>
              <a href="epis1138703s.html#224">IX. San Torpe', ed
              altre chiese</a>
            </li>
            <li>
              <a href="epis1138703s.html#242">X. San Sisto, ed
              altre chiese</a>
            </li>
            <li>
              <a href="epis1138703s.html#257">Santa Cecilia, San
              Paolo all'Orto, e Sant'Andrea</a>
            </li>
            <li>
              <a href="epis1138703s.html#267">XII. San Pietro in
              Vinculis, ed altre chiese</a>
            </li>
            <li>
              <a href="epis1138703s.html#288">XIII. San Martino,
              ed altre chiese</a>
            </li>
            <li>
              <a href="epis1138703s.html#306">XIV. Santa Maria
              del Carmine, ed altre chiese</a>
            </li>
            <li>
              <a href="epis1138703s.html#325">XV. San Paolo a
              Ripa d'Arno, ed altre chiese</a>
            </li>
            <li>
              <a href="epis1138703s.html#344">XVI. Santa Maria
              della Spina, Santa Cristina, e San Domenico</a>
            </li>
            <li>
              <a href="epis1138703s.html#362">XVII. Logge di
              Banchi, ed altri efifizj</a>
            </li>
            <li>
              <a href="epis1138703s.html#389">XVIII. Fabbriche
              appartenenti all'Università</a>
            </li>
            <li>
              <a href="epis1138703s.html#403">XIX. Chiese
              suburbane</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="epis1138703s.html#434">Parte II. Pisa Colonia
          Romana, e di poi Repubblica</a>
          <ul>
            <li>
              <a href="epis1138703s.html#434">I. Bagno secco,
              detto di Nerone&#160;</a>
            </li>
            <li>
              <a href="epis1138703s.html#445">II. Inidizj di
              altre romane fabbriche</a>
            </li>
            <li>
              <a href="epis1138703s.html#462">III. Marmi antichi
              figurati e scritti</a>
            </li>
            <li>
              <a href="epis1138703s.html#481">IV. Indizj di
              romane fabbriche nel Territorio pisano</a>
            </li>
            <li>
              <a href="epis1138703s.html#493">V. Pisa
              Repubblica</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="epis1138703s.html#538">Appendice al Capitolo
          III. della parte I. del II. tomo</a>
        </li>
        <li>
          <a href="epis1138703s.html#542">Ordine alfabetico dei
          Professori del disegno</a>
        </li>
        <li>
          <a href="epis1138703s.html#552">Indice delle cose più
          notabili contenute nei tre tomi della presente
          opera&#160;</a>
        </li>
        <li>
          <a href="epis1138703s.html#566">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
