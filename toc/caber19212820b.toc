<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caber19212820bs.html#6">Vita Del Cavalier Gio.
      Lorenzo Bernino</a>
      <ul>
        <li>
          <a href="caber19212820bs.html#8">Eminentissimo, e
          Reverendissimo Signore</a>
        </li>
        <li>
          <a href="caber19212820bs.html#14">L'autore al
          lettore</a>
        </li>
        <li>
          <a href="caber19212820bs.html#18">Indice de
          Capitoli</a>
        </li>
        <li>
          <a href="caber19212820bs.html#22">Della vita del
          Cavalier Gio. Lorenzo Bernini</a>
          <ul>
            <li>
              <a href="caber19212820bs.html#22">I. Nascità,
              Educazione, e Studj di Gio. Lorenzo Bernini nella
              città di Napoli fin'all'età di dieci Anni, e sua
              venuta in Roma</a>
            </li>
            <li>
              <a href="caber19212820bs.html#28">II. Prima
              entrata nel Palazzo Pontificio, suo abboccamento, e
              successi con Paolo Quinto, e alcuni Cardinali di
              quella Corte, e suoi primi studii in Roma</a>
            </li>
            <li>
              <a href="caber19212820bs.html#38">III. Alcune
              Opere ad istanza di Paolo V. e applausi, che ne
              ricevè</a>
            </li>
            <li>
              <a href="caber19212820bs.html#42">IV. Morte di
              Paolo Quinto, Creazione di Gregorio XV. Dimostrazioni
              du lui verso Gio. Lorenzo [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#45">V. Assunzione al
              Pontificato del Card. Maffeo Barberino col nome di
              Urbano Ottavo, sua lode al Cavalier Bernino,
              [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#58">VI. Opera delle
              quattro Colonne di Metallo, detta la Confessione di
              San Pietro, Ritratti del Papa, Figura di S. Bibiana
              [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#68">VII. Malattia
              del Cavaliere, e dimostrazione verso di lui del Papa,
              che viene in Casa sua. Suo accasamento, Figliuolanza,
              e Comedie</a>
            </li>
            <li>
              <a href="caber19212820bs.html#78">VIII. Invenzione
              del Cavaliere ne' disegni di molte Fontane: Altre sue
              operazioni di quel tempo, e elevazione di un de' due
              Campanili nella facciata di San Pietro</a>
            </li>
            <li>
              <a href="caber19212820bs.html#85">IX. Opere del
              Bernino ad istanza del Rè di Spagna, del Duca di
              Modena, del Rè di Spagna, e Regina d'Inghilterra,
              [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#93">X. Sepolcro di
              Urbano VIII. fatto dal Cavaliere, e morte del detto
              Pontefice</a>
            </li>
            <li>
              <a href="caber19212820bs.html#96">XI. Creazione
              d'Innocenzo Decimo, Emoli del Cavaliere appresso di
              lui, e demolizione del Campanile di San Pietro,
              [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#105">XII.
              Riconciliazione del Papa col Cavaliere, dimostrazione
              di stima, che gli fa [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#116">XIII. Concetto
              di stima del Pontefice Alessandro VII., e della Corte
              di Roma verso il Cavaliere in quel tempo [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#123">XIV. Arrivo in
              Roma della Regina &#160;Cristina di Svezia, e suoi
              successi col Cavaliere [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#130">XV. Opera della
              Cathedrale di San Pietro, e alcune notizie degli
              Allievi del Bernino</a>
            </li>
            <li>
              <a href="caber19212820bs.html#136">XVI. Istanze al
              Papa di Luigi il Grande Rè di Francia per havere il
              Cavaliere al suo servizio in Parigi [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#145">XVII. Partenza
              da Roma del Cavaliere. Honori, che riceve nel suo
              Viaggio da' Principi d'Italia [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#149">XVIII.
              Operazioni del Cavaliere in Parigi, e suoi successi
              col Rè, e altri Grandi di quella Corte</a>
            </li>
            <li>
              <a href="caber19212820bs.html#161">XIX. Il
              Cavaliere prende licenza dal Rè, doni, che ne riceve,
              suo arrivo in Roma [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#168">XX. Descrizione
              del gran Colosso del Rè Luigi il Grande a Cavallo, e
              riflessioni sopra di esso [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#176">XXI.
              Successione al Ponteficato di Clemente IX., e poi di
              Clemente X. [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#186">XXII. Creazione
              d'Innocenzo XI., e stima in cui era appresso a lui il
              Cav. Bernino [...]</a>
            </li>
            <li>
              <a href="caber19212820bs.html#190">XXIII.
              Sentimenti di divozione. Malattia, e morte del
              Cavaliere</a>
            </li>
            <li>
              <a href="caber19212820bs.html#198">XXIV. Alcune
              riflessioni sopra la Vita, e le Opere del Cavalier
              Gio. Lorenzo Bernino</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="caber19212820bs.html#202">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
