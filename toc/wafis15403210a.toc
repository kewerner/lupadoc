<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="wafis15403210as.html#6">Entwurff Einer
      Historischen Architectur, In Abbildung unterschiedener
      berühmten Gebäude, des Alterthums, und fremder Völcker, Umb
      aus den Geschicht-büchern, Gedächtnüß-müntzen, Ruinen, und
      eingeholten wahrhafften Abrißen, vor Augen zu stellen</a>
      <ul>
        <li>
          <a href="wafis15403210as.html#8">Dem
          Allerdurchleuchtigsten, Großmächtigsten, und
          Unüberwindlichsten Fürsten und Herrn, Herrn Carolo dem
          Sechsten</a>
        </li>
        <li>
          <a href="wafis15403210as.html#10">Preface</a>
        </li>
        <li>
          <a href="wafis15403210as.html#12">Erstes Buch, von
          einigen Gebäuden der Alten Juden, Egyptier, Syrer, Perser
          und Griechen</a>
        </li>
        <li>
          <a href="wafis15403210as.html#88">Andres Buch, von
          einigen alten unbekanten Römischen Gebäuden</a>
        </li>
        <li>
          <a href="wafis15403210as.html#134">Drittes Buch, von
          einigen Gebäuden, der Araber und Türcken, wie auch neuen
          Persianischen, Siamitischen, Sinesischen, und
          Japonesischen Bau-art</a>
        </li>
        <li>
          <a href="wafis15403210as.html#168">Viertes Buch,
          einige Gebäude von des Autoris Erfindung und
          Zeichnung</a>
        </li>
        <li>
          <a href="wafis15403210as.html#214">Divers Vases
          Antiques Agyptiens, Grecs, Romains, et Modernes: avec
          quelches uns de l'invention de l'Auteur</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
