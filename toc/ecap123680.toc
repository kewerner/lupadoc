<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ecap123680s.html#6">The Ruins of Paestum,
      otherwise Posidonia, in Magna Graecia</a>
      <ul>
        <li>
          <a href="ecap123680s.html#8">[Dedica dell'autore a
          George Montagu] To his Grace George Duke of Montagu,
          Marquis of Monthermer, Earl of Cardigan [...]</a>
        </li>
        <li>
          <a href="ecap123680s.html#10">Suscribers to the Ruins
          of Paestum</a>
        </li>
        <li>
          <a href="ecap123680s.html#12">[Dedica ai lettori] To
          the Reader&#160;</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ecap123680s.html#14">[Text]</a>
      <ul>
        <li>
          <a href="ecap123680s.html#14">An enquiry into the
          origin and ancient state Posidonia, or Paestum</a>
        </li>
        <li>
          <a href="ecap123680s.html#28">A description of the
          Temples&#160;</a>
          <ul>
            <li>
              <a href="ecap123680s.html#32">The Hexastyle
              Hypaethral, or Ipetral Temple</a>
            </li>
            <li>
              <a href="ecap123680s.html#35">The Hexastyle
              Peripteral Temple&#160;</a>
            </li>
            <li>
              <a href="ecap123680s.html#35">The Pseudodipteral
              Temple, or Basilica&#160;</a>
            </li>
            <li>
              <a href="ecap123680s.html#38">A dissertation on the
              Coins and Medals of Posidonia, or Paestum</a>
            </li>
            <li>
              <a href="ecap123680s.html#42">Description of the
              Coins and Medals</a>
            </li>
            <li>
              <a href="ecap123680s.html#50">Table of Posidonian
              and Paestan Coins, from whence taken, and in whose
              Collection</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ecap123680s.html#52">Explication of the Plates</a>
    </li>
    <li>
      <a href="ecap123680s.html#56">[Tavole]</a>
      <ul>
        <li>
          <a href="ecap123680s.html#56">I. A general view of the
          Ruined City of Paestum ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#58">II. View of the three
          Temples, taken from the East ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#60">III. A north view of the
          City of Paestum, taken from under the Gate ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#62">IV. A vVew of the Gate,
          taken from within the Walls ▣&#160;</a>
        </li>
        <li>
          <a href="ecap123680s.html#64">V. A View of the Gate,
          from without the Walls ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#66">VI. Plam of the Hexastyle
          Ipetral Temple ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#68">VII. A View if the
          Hexastyle Ipetral Temple, taken from the South ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#70">VIII. A View of the
          Hexastyle Ipetral Temple taken from the South West ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#72">IX. Internal View of the
          Hexastyle Ipetral Temple, taken from the South ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#74">X. Elevation of the
          Hexastyle Ipetral Temple ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#76">XI. Section of the
          Hexastyle Ipetral Temple ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#78">XII. Members and Measures
          of the Hexastyle Ipetral Temple ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#80">XIII. Plan of the
          Hexastyle Peripteral Temple ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#82">XIV. A View of t
          Hexastyle Peripteral Temple, taken from the South ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#84">XV. A View of the
          Hexastyle Peripteral Temple, taken from the North West
          ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#86">XVI. Internal View of the
          Hexastyle Peripteral Temple, taken from the North
          ▣&#160;</a>
        </li>
        <li>
          <a href="ecap123680s.html#88">XVII. Elevation of the
          Hexastyle Peripteral Temple ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#90">XVIII. Plan of the
          Pseudodipteral Temple σ</a>
        </li>
        <li>
          <a href="ecap123680s.html#92">XIX. A. A View of the
          Pseudodipteral Temple or Basilica, taken from the North
          ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#94">XIX. B. A View of the
          Pseudodipteral Temple or Basilica, taken from the North
          West ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#96">XX. Internal View of the
          Pseudodipteral Temple or Basilica, taken from the South
          ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#98">XXI. Internal Vie of the
          Pseudodipteral Temple or Basilica, taken from the North
          ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#100">XXII. Elevation of the
          Pseudodipteral Temple or Basilica ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#102">XXIII. Members and
          Measures of the Peripteral and Pseudodipteral Temples
          ▣</a>
        </li>
        <li>
          <a href="ecap123680s.html#104">XXIV. Coins and Medals
          of Pastum or Posidonia ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
