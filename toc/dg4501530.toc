<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501530s.html#6">Libro Di M. Pyrrho Ligori
      Napolitano, Delle Antichità Di Roma</a>
      <ul>
        <li>
          <a href="dg4501530s.html#7">Iulius Papa III.</a>
        </li>
        <li>
          <a href="dg4501530s.html#10">All'illustrissimo et
          Reverendissimo Monsignore, il Signor Cardinale di
          Ferrara</a>
        </li>
        <li>
          <a href="dg4501530s.html#14">Libro di Pyrrho Ligori
          delle Antichità di Roma, nel qual si tratta de i Circi,
          Theatri, et Amphitheatri d'essa città</a>
        </li>
        <li>
          <a href="dg4501530s.html#62">Paradosse di Pyrrho Ligori
          napolitano.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
