<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlec552129901s.html#10">Cabinet Des Singularitez
      D’Architecture, Peinture, Sculpture, Et Graveure Ou
      Introduction A La Connoissance des plus beaux Arts, figurés
      sous les Tableaux les Statuës, &amp; les Estampes; Tome
      I.</a>
      <ul>
        <li>
          <a href="ghlec552129901s.html#14">A Monseigneur Jules
          Hardouin Mansart</a>
        </li>
        <li>
          <a href="ghlec552129901s.html#20">Preface</a>
        </li>
        <li>
          <a href="ghlec552129901s.html#34">Sommaire historique
          d'architecture et des architectes, dont les ouvrages ont
          le plus éclaté dans la France</a>
        </li>
        <li>
          <a href="ghlec552129901s.html#82">Le Cabinet des
          Tableaux, des Statues et des Estampes ou l'introduction a
          la connoissance des arts d'architecture, de peinture, de
          sculpture, et de graveure</a>
          <ul>
            <li>
              <a href="ghlec552129901s.html#270">Table des
              principaux sujets, et des noms des Peintres dont j'ay
              parlé dans ce Volume</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#276">Differens
              catalogues que je donne au Public de tout ce que j'ai
              crû de plus utile, de plus curieux, et qui eût même
              plus de rapport à ce Volume</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#304">Catalogue de
              Marot pere et fils</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#318">Estampes du
              Cabinet du Roy, ou le catalogue des Tableaux du
              Cabinet du Roy</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#338">Oeuvre du Sieur
              Antoine Francois Vander-meulen Peintre des Conquêtes
              du Roy</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#348">Gallerie du
              Palais Royal</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#354">Catalogue des
              Tableaux presentez le premier jour de May à
              Nôtre-Dame</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#376">Catalogue des
              Tableaux de differens Maîtres</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#382">Oeuvre de
              Pierre Paul Rubens</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#416">Oeuvre
              d'Antoine Vandick&#160;</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#440">Oeuvre des
              Caraches</a>
            </li>
            <li>
              <a href="ghlec552129901s.html#464">Oeuvre de
              Robert Nanteuil, natif de Reims, Peintre en Pastel et
              Graveur du Roy</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
