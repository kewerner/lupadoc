<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghpas122733002s.html#6">Vite de’ pittori,
      scultori, ed architetti moderni</a>
      <ul>
        <li>
          <a href="ghpas122733002s.html#8">Sire</a>
        </li>
        <li>
          <a href="ghpas122733002s.html#20">L'autore a chi
          legge</a>
        </li>
        <li>
          <a href="ghpas122733002s.html#27">Proemio</a>
        </li>
        <li>
          <a href="ghpas122733002s.html#40">Vite de' pittori</a>
          <ul>
            <li>
              <a href="ghpas122733002s.html#42">Di Giambatista
              Calandra</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#54">Di Bernardino
              Gagliardi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#65">D'Antonino
              Barbalunga Alberti</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#76">Di Mario
              Nuzzi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#84">Di Francesco
              Cozza</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#94">Di Francesco
              Lauri</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#110">Di Pietro Del
              Po</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#122">Di Mattia
              Preti</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#133">Di Gianangelo
              Canini</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#145">Di Giammaria
              Morandi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#156">Di Filippo
              Lauri</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#172">Di Lazzaro
              Baldi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#182">Di Carlo
              Cesi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#195">Di Cesare
              Pronti</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#207">Di Gianandrea
              Carloni</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#218">Di Giuseppe
              Ghezzi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#230">Di Giovanni
              Bonati</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#242">Di Giambatista
              Benaschi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#254">Di Luigi
              Garzi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#264">Di Andrea
              Pozzo</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#295">Di Giambatista
              Buoncuore</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#306">Di Antonio
              Gherardi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#317">Di Lodovico
              Gimignani</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#327">Di Diacinto
              Calandrucci</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#336">Di Daniel
              Seiter</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#350">Di Buonaventura
              Lamberti</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#358">Di Carlo di
              Voglar</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#368">Di Cristiano
              Reder</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#376">Di Cristiano
              Bernetz</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#387">Di Francesco
              Varnertam&#160;</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#397">Di Bastiano
              Ricci</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#405">Di Giovanni
              Odasi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#418">Di Andrea
              Procaccini&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghpas122733002s.html#436">Vite degli
          scultori</a>
          <ul>
            <li>
              <a href="ghpas122733002s.html#438">Di Francesco
              Mochi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#450">Di Giuliano
              Finelli</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#463">D'Andrea
              Bolgi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#472">Di Lazzaro
              Morelli</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#484">Di Paolo
              Naldini</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#494">Di Jacopantonio
              Fancelli</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#504">Di Giuseppe
              Mazzuoli</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#514">Di Pietro
              Monnot</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghpas122733002s.html#526">Vite degli
          architetti</a>
          <ul>
            <li>
              <a href="ghpas122733002s.html#528">Di Carlo
              Maderno</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#537">D'Onorio
              Lunghi</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#549">Di Giambatista
              Soria&#160;</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#559">Di Giambatista
              Gisleni</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#569">Di Carlo
              Fontana</a>
            </li>
            <li>
              <a href="ghpas122733002s.html#578">Di Giambatista
              Contini</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghpas122733002s.html#588">Tavola per ordine
          d'alfabeto</a>
        </li>
        <li>
          <a href="ghpas122733002s.html#592">Tavola per ordine
          d'età, e di professione</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
