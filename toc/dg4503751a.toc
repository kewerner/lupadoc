<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503751as.html#8">Roma antica, media, e
      moderna</a>
      <ul>
        <li>
          <a href="dg4503751as.html#11">Novae Urbis
          Delineatio</a>
        </li>
        <li>
          <a href="dg4503751as.html#12">Giornata prima</a>
          <ul>
            <li>
              <a href="dg4503751as.html#12">Da ›Ponte
              Sant'Angelo‹ à ›San Pietro in Vaticano‹ per ›Borgo‹
              Vaticano</a>
              <ul>
                <li>
                  <a href="dg4503751as.html#12">[›Castel
                  Sant'Angelo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#17">[San Pietro in
                  Vaticano‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#18">[San Pietro in
                  Vaticano‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503751as.html#37">Giornata seconda</a>
          <ul>
            <li>
              <a href="dg4503751as.html#37">Da ›Santo Spirito in
              Sassia‹ per il ›Trastevere‹</a>
              <ul>
                <li>
                  <a href="dg4503751as.html#43">[›Fontana
                  dell'Acqua Paola (via Garibaldi)‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503751as.html#55">Giornata Terza</a>
          <ul>
            <li>
              <a href="dg4503751as.html#55">Da Strada Giulia ›Via
              Giulia‹ all'Isola di San bartolomeo ›Isola
              Tiberina‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503751as.html#67">Giornata Quarta</a>
          <ul>
            <li>
              <a href="dg4503751as.html#67">Da ›San Lorenzo in
              Damaso‹ al Monte Aventino</a>
              <ul>
                <li>
                  <a href="dg4503751as.html#77">Chiesa di ›San
                  Paolo fuori le Mura‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#78">[›San Paolo fuori
                  le Mura‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503751as.html#83">Giornata Quinta</a>
          <ul>
            <li>
              <a href="dg4503751as.html#83">Dalla Piazza di
              ›Monte Giordano‹, per li Monti ›Celio‹, e
              ›Palatino‹</a>
              <ul>
                <li>
                  <a href="dg4503751as.html#84">›Statua di
                  Pasquino‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#91">[›San
                  Sebastiano‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#92">Veduta della
                  Chiesa di ›San Sebastiano‹ ▣ fuori delle Mura</a>
                </li>
                <li>
                  <a href="dg4503751as.html#97">[›San Giovanni in
                  Laterano‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#98">Veduta di ›San
                  Giovanni in Laterano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#104">[›Santo Stefano
                  Rotondo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#105">[›Santa Maria in
                  Domnica‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503751as.html#110">Giornata Sesta</a>
          <ul>
            <li>
              <a href="dg4503751as.html#110">Da ›San Salvatore in
              Lauro‹ per ›Campidoglio‹, e per le ›Carinae‹</a>
              <ul>
                <li>
                  <a href="dg4503751as.html#113">[›Fontana dei
                  Fiumi‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#117">[›Campidoglio‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#124">[›Foro della
                  Pace‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#126">[›Colosseo‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#132">[›Colonna
                  Traiana‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503751as.html#135">Giornata Settima</a>
          <ul>
            <li>
              <a href="dg4503751as.html#135">Dalla ›Piazza di
              Sant'Agostino‹ per il Monte ›Viminale‹, e
              ›Quirinale‹</a>
              <ul>
                <li>
                  <a href="dg4503751as.html#137">[›San Luigi dei
                  Francesi‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#148">Chiesa di ›Santa
                  Croce in Gerusalemme‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#152">›San Lorenzo
                  fuori le Mura‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#157">[›Santa Maria
                  Maggiore‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#158">›Santa Maria
                  Maggiore‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503751as.html#170">[›Pantheon‹]
                  ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503751as.html#173">Giornata Ottava</a>
          <ul>
            <li>
              <a href="dg4503751as.html#173">Dalla strada
              dell'Orso ›Via dell'Orso‹ a Monte Cavallo
              ›Quirinale‹, e alle Terme Diocleziane ›Terme di
              Diocleziano‹</a>
              <ul>
                <li>
                  <a href="dg4503751as.html#175">[›Fontana di
                  Trevi‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503751as.html#185">Giornata Nona</a>
          <ul>
            <li>
              <a href="dg4503751as.html#185">Dal ›Palazzo
              Borghese‹ a ›Porta del Popolo‹, a ›Piazza di
              Spagna‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503751as.html#200">Giornata Decima</a>
          <ul>
            <li>
              <a href="dg4503751as.html#200">Dal Monte Citorio
              ›Piazza di Montecitorio‹ alla ›Porta Pia‹, e al Monte
              ›Pincio‹</a>
              <ul>
                <li>
                  <a href="dg4503751as.html#201">[›Colonna di
                  Antonino Pio‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503751as.html#216">Cronologia di tutti li
          sommi Pontefici</a>
        </li>
        <li>
          <a href="dg4503751as.html#230">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
