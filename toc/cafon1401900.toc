<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cafon1401900s.html#6">Della trasportatione
      dell’obelischo Vaticano et delle fabriche di Nostro Signore
      Papa Sisto V.; Tomo I.</a>
      <ul>
        <li>
          <a href="cafon1401900s.html#10">Al santissimo et
          beatissimo Padre et Signor Nostro Papa Sisto
          Quinto&#160;</a>
        </li>
        <li>
          <a href="cafon1401900s.html#12">Del modo tenuto in
          transferire l'obelisco vaticano, et delle fabriche fatte
          da nostro Signore Papa Sisto Quinto, co' disegni loro</a>
        </li>
        <li>
          <a href="cafon1401900s.html#12">A' lettori</a>
        </li>
        <li>
          <a href="cafon1401900s.html#15">Narratione del modo
          tenuto in transferire l'obelisco vaticano</a>
        </li>
        <li>
          <a href="cafon1401900s.html#18">Copia della sustantia
          del privilegio</a>
        </li>
        <li>
          <a href="cafon1401900s.html#24">Regola per misurare le
          Guglie quadrate et per sapere il peso loro&#160;</a>
        </li>
        <li>
          <a href="cafon1401900s.html#27">Descrizione della forma
          del Castello fatto per lazare la Guglia</a>
        </li>
        <li>
          <a href="cafon1401900s.html#38">Discorso sopra il modo,
          che dovettero tenere gli antichi per alzare la Guglia, e
          sopra la sua rottura</a>
        </li>
        <li>
          <a href="cafon1401900s.html#72">Alzatura. et
          aggiustatura della Guglia</a>
        </li>
        <li>
          <a href="cafon1401900s.html#73">Descittione della
          processione fatta per purgare, e benedire la Guglia, e
          per consacrarvi sopra la Croce</a>
        </li>
        <li>
          <a href="cafon1401900s.html#80">Descrizione della
          fabrica del Palazzo fatto nella Vigna di Nostro Signore
          mentre era Cardinale&#160;</a>
        </li>
        <li>
          <a href="cafon1401900s.html#84">Descrittione della
          Fabrice della Capella del Presepio</a>
        </li>
        <li>
          <a href="cafon1401900s.html#106">Narratione del modo
          tenuto in trasportare la Capella vecchia del Presepio
          tutta intiera</a>
        </li>
        <li>
          <a href="cafon1401900s.html#114">Descrittione
          dell'Acqua Felice condotta à Monte Cavallo</a>
        </li>
        <li>
          <a href="cafon1401900s.html#120">Descrittione della
          fabrica della Loggia delle Benedittioni fabricata à San
          Giovanni Laterano&#160;</a>
        </li>
        <li>
          <a href="cafon1401900s.html#124">Descrittione del Gran
          Palazzo Apostolico fabricato à San Giovanni Laterano</a>
        </li>
        <li>
          <a href="cafon1401900s.html#148">Trasportatione della
          Scala Santa a canto al Sanct Sanctorum</a>
        </li>
        <li>
          <a href="cafon1401900s.html#149">Conduttura, et
          erettione del gran d'Obelisco di Costantino à S. Giovanni
          Laterano</a>
        </li>
        <li>
          <a href="cafon1401900s.html#157">Della Piazza di San
          Giovanni Laterano</a>
        </li>
        <li>
          <a href="cafon1401900s.html#158">Conduttura, et
          erettione della Guglia della Madonna del Populo</a>
        </li>
        <li>
          <a href="cafon1401900s.html#162">Conduttura, et
          erettione della Guglia di Santa Maria Maggiore</a>
        </li>
        <li>
          <a href="cafon1401900s.html#172">Descrittione della
          fabrica dell'ospitale de' Mendicanti a Ponte
          Sisto&#160;</a>
        </li>
        <li>
          <a href="cafon1401900s.html#176">Descrittione della
          fabrica della gran Libreria del Vaticano&#160;</a>
        </li>
        <li>
          <a href="cafon1401900s.html#208">Delle statue di S.
          Pietro et di San Pauolo drizzate sopra le colonne
          Traiana, e Antonina, et della restauratione d'essa
          Colonna Antonina&#160;</a>
        </li>
        <li>
          <a href="cafon1401900s.html#212">Della piazza, et
          palazzo fabricato à Monte Cavallo</a>
        </li>
        <li>
          <a href="cafon1401900s.html#212">Delle strade nuove
          aperte da Nostro Signore</a>
        </li>
        <li>
          <a href="cafon1401900s.html#213">Della fabrica mirabile
          di San Pietro&#160;</a>
        </li>
        <li>
          <a href="cafon1401900s.html#214">Descrittione della
          fabrica del lavatoro fatto sopra la piazza delle
          Terme</a>
        </li>
        <li>
          <a href="cafon1401900s.html#215">Descrittione della
          Scala secreta, che scende dal Palazzo Apostolico in San
          Pietro</a>
        </li>
        <li>
          <a href="cafon1401900s.html#215">Descrizione della
          restauratione della chiesa di Santa Sabina</a>
        </li>
        <li>
          <a href="cafon1401900s.html#216">Restauratione del
          Torrone di Belvedere</a>
        </li>
        <li>
          <a href="cafon1401900s.html#216">Del Purgo pubblico
          alla fontana di Trevi</a>
        </li>
        <li>
          <a href="cafon1401900s.html#216">Del Collegio de
          Marchiani à Bologna</a>
        </li>
        <li>
          <a href="cafon1401900s.html#217">Della città di
          Loreto</a>
        </li>
        <li>
          <a href="cafon1401900s.html#217">Della città di
          Mont'alto</a>
        </li>
        <li>
          <a href="cafon1401900s.html#217">Dell'Acqua condotta à
          Cività Vecchia</a>
        </li>
        <li>
          <a href="cafon1401900s.html#228">Tavola delle cose più
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
