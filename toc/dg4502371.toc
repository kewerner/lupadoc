<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502371s.html#6">Grandezze della città di
      Roma</a>
      <ul>
        <li>
          <a href="dg4502371s.html#8">Signor Giovanni Alto
          Interprete della natione Allemanna Svizzero de Lucerna
          Soldato in guardia di Nostro Signora</a>
        </li>
        <li>
          <a href="dg4502371s.html#10">Al molto illustre e molto
          Reverendo Signore e Padrone mio Osservandissimo il Signor
          D. Francesco Maria Torrigio Romano, Teologo, e
          Filosofo</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502371s.html#12">[Text]</a>
      <ul>
        <li>
          <a href="dg4502371s.html#12">›Foro Romano‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#13">Marche Romain</a>
            </li>
            <li>
              <a href="dg4502371s.html#14">›Foro Romano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#15">Il ›Foro di Traiano‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#16">Le Marche de Traian</a>
            </li>
            <li>
              <a href="dg4502371s.html#17">Foro di Traiano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#18">Il ›Foro di Nerva‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#19">Le Marche de Nerva</a>
            </li>
            <li>
              <a href="dg4502371s.html#20">›Foro di Nerva‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#21">Le Terme Diocletiane ›Terme
          di Diocleziano‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#22">Les Thermes de
              Diocletian</a>
            </li>
            <li>
              <a href="dg4502371s.html#23">›Terme di Diocleziano‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#24">Le Terme Antoniane ›Terme
          di Caracalla‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#25">Les Thermes
              Antoniens</a>
            </li>
            <li>
              <a href="dg4502371s.html#26">›Terme di Caracalla‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#27">L'›Arco di Settimio
          Severo‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#28">L'Arc de Septimus
              Severus</a>
            </li>
            <li>
              <a href="dg4502371s.html#29">›Arco di Settimio
              Severo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#30">L'›Arco di Costantino‹
          Magno</a>
          <ul>
            <li>
              <a href="dg4502371s.html#31">L'Arc de Constantin le
              Grand</a>
            </li>
            <li>
              <a href="dg4502371s.html#32">›Arco di Costantino‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#33">›Porta Maggiore‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#34">La Porte Maieure</a>
            </li>
            <li>
              <a href="dg4502371s.html#35">›Porta Maggiore‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#36">L'›Arco di Tito‹, e
          Vespasiano</a>
          <ul>
            <li>
              <a href="dg4502371s.html#37">L'Arc de Titus et
              Vespasien</a>
            </li>
            <li>
              <a href="dg4502371s.html#38">›Arco di Tito‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#39">L'Arco di San Giorgio ›Arco
          degli Argentari‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#40">L'Arc de Saint
              George</a>
            </li>
            <li>
              <a href="dg4502371s.html#41">Arcus Septimii Severi
              ›Arco degli Argentari‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#42">L'›Arco di Portogallo‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#43">L'Arc de Portugal</a>
            </li>
            <li>
              <a href="dg4502371s.html#44">Arcus Domitiani
              Imperatoris ›Arco di Portogallo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#45">L'Anfiteatro detto
          ›Colosseo‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#46">L'Anphitheatre dir
              Colisee</a>
            </li>
            <li>
              <a href="dg4502371s.html#47">›Colosseo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#48">L'›Anfiteatro di Statilio
          Tauro‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#49">L'Amphitheatre de
              Statilius Taurus</a>
            </li>
            <li>
              <a href="dg4502371s.html#50">›Anfiteatro di Statilio
              Tauro‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#51">Il ›Teatro di Marcello‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#52">Le Theatre de
              Marcellus</a>
            </li>
            <li>
              <a href="dg4502371s.html#52">Il ›Teatro di Marcello‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#54">Il Tempio della Rotonda
          ›Pantheon‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#55">Le Temple diet la
              Rotonde</a>
            </li>
            <li>
              <a href="dg4502371s.html#56">Templum Marci Agrippae
              ›Pantheon‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#57">Il Tempio in Pace ›Foro
          della Pace‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#58">Le temple de la
              Paix</a>
            </li>
            <li>
              <a href="dg4502371s.html#59">Templum Pacis ›Foro
              della Pace‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#60">Il ›Tempio della
          Concordia‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#61">Le Temple de la
              Concorde</a>
            </li>
            <li>
              <a href="dg4502371s.html#62">›Tempio della
              Concordia‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#63">Del Tempio di Giano ›Arco
          di Giano‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#64">Du Temple de Ianus</a>
            </li>
            <li>
              <a href="dg4502371s.html#65">Templum Iani ›Arco di
              Giano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#66">La Basilica d'Antonino Pio
          ›Tempio di Antonino e Faustina‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#67">L'Hostel ou Maison
              Publique d'Antonin Pie</a>
            </li>
            <li>
              <a href="dg4502371s.html#68">Porticus Antonini Pii
              Imperatoris ›Tempio di Antonino e Faustina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#69">Il Tempio della Fortuna
          ›Tempio di Portunus‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#70">Le Temple de la
              Fortune</a>
            </li>
            <li>
              <a href="dg4502371s.html#71">Templum Fortunae
              Virilis ›Tempio di Portunus‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#72">Il ›Tempio di Antonino e
          Faustina‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#73">Le Temple d'Antonin, et
              Faustine</a>
            </li>
            <li>
              <a href="dg4502371s.html#74">›Tempio di Antonino e
              Faustina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#75">Il ›Tempio di Giove
          Statore‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#76">Le Temple de Iupiter
              surnomme Stator</a>
            </li>
            <li>
              <a href="dg4502371s.html#77">›Tempio di Giove
              Statore‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#78">Il Tempio del Sole ›Tempio
          di Serapide‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#79">Le Temple du Soleil</a>
            </li>
            <li>
              <a href="dg4502371s.html#79">Templum Solis › ›Tempio
              di Serapide‹▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#81">›Castel Sant'Angelo‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#82">Le Chasteau Sant
              Ange</a>
            </li>
            <li>
              <a href="dg4502371s.html#83">Moles Hadriani
              Imperatoris ›Castel Sant'Angelo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#84">Il sepolchro di Metella
          ›Tomba di Cecilia Metella‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#85">Le Sepulchre de
              Metella</a>
            </li>
            <li>
              <a href="dg4502371s.html#86">Sepulchrum Metellorum
              ›Tomba di Cecilia Metella‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#87">La sepoltura di Cestio
          ›Piramide di Caio Cestio‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#88">Le Sepulchre de
              Cestin</a>
            </li>
            <li>
              <a href="dg4502371s.html#89">Sepulchrum Caii Cestii
              ›Piramide di Caio Cestio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#90">›Campidoglio‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#91">Le Capitole</a>
            </li>
            <li>
              <a href="dg4502371s.html#92">›Campidoglio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#93">Il Cerchio di Caracalla
          ›Circo di Massenzio‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#94">Le Cirque de
              Caracalla</a>
            </li>
            <li>
              <a href="dg4502371s.html#95">Circus Antonini
              Caracallae ›Circo di Massenzio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#96">Li ›Trofei di Mario‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#97">Les Trophees de
              Marius</a>
            </li>
            <li>
              <a href="dg4502371s.html#98">Marii Trophaea ›Trofei
              di Mario‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#99">Palazzo Maggiore
          ›Palatino‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#100">Le Palais Maieur</a>
            </li>
            <li>
              <a href="dg4502371s.html#101">Palatium Maius
              ›Palatino‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#102">L'›Isola Tiberina‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#103">L'Isle du Tybre</a>
            </li>
            <li>
              <a href="dg4502371s.html#104">›Isola Tiberina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#105">La ›Colonna Traiana‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#106">La Colonne de
              Traian</a>
            </li>
            <li>
              <a href="dg4502371s.html#107">›Colonna Traiana‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#108">La Colonna d'Antonino
          ›Colonna Antonina‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#109">La Colonne
              d'Antonin</a>
            </li>
            <li>
              <a href="dg4502371s.html#110">›Colonna Antonina‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#111">Guglia di San Pietro
          ›Obelisco Vaticano‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#112">Esguille de Sainct
              Pierre</a>
            </li>
            <li>
              <a href="dg4502371s.html#113">Obeliscus Sancti Petri
              ›Obelisco Vaticano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#114">Guglia di San Giovanni
          Laterano ›Obelisco Lateranense‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#115">Esguille, ou Obelisque
              de Sainct Iehan Lateran</a>
            </li>
            <li>
              <a href="dg4502371s.html#116">Obeliscus Sancti
              Ioannis Lateranensis ›Obelisco Lateranense‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#117">Guglia di Santa Maria
          Maggiore ›Obelisco Esquilino‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#118">Esguille de Saincte
              Marie Maieure</a>
            </li>
            <li>
              <a href="dg4502371s.html#119">Obeliscum Sanctae
              Mariae Maioris ›Obelisco Esquilino‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#120">Guglia della Madonna del
          Popolo ›Obelisco Flaminio‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#121">Esguilla de Nostre
              Dame du Peuple</a>
            </li>
            <li>
              <a href="dg4502371s.html#122">Obeliscus Sanctae
              Mariae de Populo ›Obelisco Flaminio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#123">Guglia di San Mauto
          ›Obelisco di Ramsses II (1)‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#124">Esguille de Sainct
              Maute</a>
            </li>
            <li>
              <a href="dg4502371s.html#125">Obeliscus Sancti Mauti
              ›Obelisco di Ramsses II (1)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#126">Guglia nel Giardino de
          Medici ›Obelisco di Boboli‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#127">Esguille, ou
              Obelisque, qui est dans le Iardin de Medicis</a>
            </li>
            <li>
              <a href="dg4502371s.html#128">Obeliscus in Horto
              Mediceo ›Obelisco di Boboli‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#129">Guglia nel Giardino de'
          Mattei ›Obelisco di Ramsses II (2)‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#130">Esguilla du Iardin des
              Matthei</a>
            </li>
            <li>
              <a href="dg4502371s.html#131">Obeliscus in Viridario
              Mattheiorum ›Obelisco di Ramsses II (2)‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#132">Colonna di Santa Maggiore
          ›Fontana di Piazza Santa Maria Maggiore‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#133">Colonne de Sancte
              Marie Maieure</a>
            </li>
            <li>
              <a href="dg4502371s.html#134">Columnae Sanctae
              Mariae Maioris ›Fontana di Piazza Santa Maria
              Maggiore‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#135">La facciata di ›San Pietro
          in Vaticano: Facciata‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#135">Le Frontispice de
              Sainct Pierre</a>
            </li>
            <li>
              <a href="dg4502371s.html#135">›San Pietro in
              Vaticano: Facciata‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#138">›Scala Santa‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#139">L'Eschelle Saincte</a>
            </li>
            <li>
              <a href="dg4502371s.html#140">›Scala Santa‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#141">Palazzo Pontificio di San
          Pietro ›Palazzo Apostolico Vaticano‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#142">Le Palais du Pape a
              Sainct Pierre</a>
            </li>
            <li>
              <a href="dg4502371s.html#143">Aedes Vaticana
              Romanorum Pontificum ›Palazzo Apostolico Vaticano‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#144">Palazzo Pontificio di
          Monte Cavallo ›Palazzo del Quirinale‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#145">Le Palais du Pape sur
              le Mont Quirinal dict Monte Cavallo ›Palazzo del
              Quirinale‹</a>
            </li>
            <li>
              <a href="dg4502371s.html#146">Aedes Pontificia in
              Quirinale Monte ›Palazzo del Quirinale‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#147">›Collegio Romano‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#148">Le College Romain</a>
            </li>
            <li>
              <a href="dg4502371s.html#149">Collegium Romanum
              ›Collegio Romano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#150">La Sapientia ›Palazzo
          della Sapienza‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#151">La Sapience ou
              l'Universite</a>
            </li>
            <li>
              <a href="dg4502371s.html#152">Gymnasium Sapientiae
              ›Palazzo della Sapienza‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#153">›Palazzo della
          Cancelleria‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#154">Le Palais dela
              Chancellerie</a>
            </li>
            <li>
              <a href="dg4502371s.html#155">Aedes Cancellariae
              Apostolicae ›Palazzo della Cancelleria‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#156">›Palazzo Farnese‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#157">Le Palais de
              Farnese</a>
            </li>
            <li>
              <a href="dg4502371s.html#158">Facies aedis
              Farnesianae ›Palazzo Farnese‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#159">›Palazzo Borghese‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#160">Palais de Borghese</a>
            </li>
            <li>
              <a href="dg4502371s.html#161">Aedes Burghesiana
              ›Palazzo Borghese‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#162">Giardino di Borghese
          ›Villa Borghese‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#163">Le Jardin de
              Borghese</a>
            </li>
            <li>
              <a href="dg4502371s.html#164">›Villa Borghese‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#165">Giardino di Fiorenza
          ›Villa Medici‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#166">Le Jardin de
              Florence</a>
            </li>
            <li>
              <a href="dg4502371s.html#167">Viridarium Mediceorum
              ›Villa Medici‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#168">Giardino de Mattei ›Villa
          Celimontana‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#169">Jardin des Matthei</a>
            </li>
            <li>
              <a href="dg4502371s.html#170">Viridarium Mattheiorum
              ›Villa Celimontana‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#171">›Fontana di San Pietro in
          Montorio (scomparsa)‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#172">Fontaine de Saint
              Pierre Montorio</a>
            </li>
            <li>
              <a href="dg4502371s.html#173">Fons Sancti Petri
              Montisaurei ›Fontana di San Pietro in Montorio
              (scomparsa)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#174">Fontana di Terme ›Fontana
          del Mosè‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#175">Fontaine de Terme</a>
            </li>
            <li>
              <a href="dg4502371s.html#176">Fons Thermarum
              ›Fontana del Mosè‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#177">Fontana di San Pietro in
          Vaticano ›Piazza San Pietro: Fontana (N)‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#178">Fontaine de Saint
              Pierre au Vatican</a>
            </li>
            <li>
              <a href="dg4502371s.html#179">Fons Sancti Petri in
              Vaticano ›Piazza San Pietro: Fontana (N)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502371s.html#180">›Piazza Navona‹</a>
          <ul>
            <li>
              <a href="dg4502371s.html#181">Le Place Navone</a>
            </li>
            <li>
              <a href="dg4502371s.html#181">Platea Agonalis
              ›Piazza Navona‹ ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
