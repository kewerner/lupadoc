<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katmrom10539601s.html#6">Sculture del palazzo
      della ›Villa Borghese‹ detta Pinciana. Parte I [Tavole] ▣</a>
      <ul>
        <li>
          <a href="katmrom10539601s.html#8">Portico</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#8">1. Pan o
              piuttosto Marsia con flauto ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#10">2. La Musa
              Terpsicore ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#12">3. Venere ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#14">4. Giove
              coll'aquila a' piedi ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#16">5. Bacco Statua
              minore del naturale ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#18">6. Seguace di
              Bacco con frutta nel grembo della clamide ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#20">7. Mercurio
              fanciullo ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#22">8. Seguace di
              Bacco Statua minore del naturale ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#24">9. Venere
              marina ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#26">10. Genio ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#28">11. Ercole
              giovine ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#30">12. Seguace di
              Bacco con maschera ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#32">13. Quattro
              forze d'Ercole Il Leone nemeo, l'Idra, il Toro
              Cretense, la Cerva Arcadica ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#34">14. Tizio
              lacerato dall'avvoltojo ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#36">15. Fregio con
              arnesi da sagrifizio ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#38">16. Imprese
              d'Ercole ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#40">17. Leda con
              Giove in cigno ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#42">18. Busto
              Romano barbato ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#44">19. Ritratto
              incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#46">20. Busto
              simigliante Claudio ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#48">21. Scipione
              Affricano ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#50">22. Giulia
              Paola moglie d'Elagabalo ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#52">23. Donna
              Romana incognita ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#54">24. Ritratto
              muliebre incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#56">25. Annibale
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#58">26. Busto di
              Giove ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#60">27. Busto di
              atleta vincitore ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#62">28. Busto
              Romano incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#64">29. Busto
              giovanile incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#66">30. Busto
              femminile incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#68">31. Donna
              Romana incognita ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#70">32. Annio Vero
              Cesare ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#72">33. Venere
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#74">34. Incognito
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#76">35. Demostene
              Ateniese ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#78">36. Busto
              Romano incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#80">37. Busto
              giovanile incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#82">38. Busto
              femminile con simiglianza d'Agrippina Maggiore ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#84">39. Scipione
              Affricano [sic] ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#86">40. Busto
              Romano incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#88">41. Busto
              Romano incognito del secondo o terzo secolo ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#90">42. Busto
              Romano incognito del secondo secolo ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#92">43. Settimio
              Severo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539601s.html#94">Stanza I</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#94">1. Livia
              Augusta in forma di Cerere ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#96">2. Mercurio
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#98">3. Statua in
              abito barbarico forse di Tiridate Re d'Armenia ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#100">4. Elio Cesare
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#102">5. La Musa
              Euterpe ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#104">6. Agrippina
              Minore in sembianza di Musa ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#106">7. Statua
              Romana all'eroica ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#108">8. Donna
              orante risarcita per Musa ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#110">9. Achille
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#112">10. Statua
              d'Imperatore deificato la cui fisionomia è quella di
              Pertinace ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#114">11. Ministre
              con oblazioni ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#116">12. Venere
              Anadiomene in mezzo a Tritoni e Nereidi ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#118">13. Sagrifizio
              trionfale Bassorilievo con figure maggiori del
              naturale ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#120">14. Le
              Danzatrici ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#122">15. Il
              cadavere d'Ettore riscattato e portato in Troja ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#124">16. I figli
              d'Anfione, e di Niobe saettati da Apollo, e da Diana
              presenti i loro genitori ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#126">17. Favola di
              Prometeo rappresentanze simboliche della vita umana
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#128">18. Frammento
              di gran bassorilievo Risarcito per Curzio che si
              gitta nella voragine ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#130">19. Busto di
              Vespasiano ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#132">20. Busto di
              Vitellio ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#134">21. Busto di
              Salvio Otone ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#136">22. Busto di
              Galba ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#138">23. Busto di
              Nerone ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#140">24. Busto di
              Tiberio Claudio ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#142">25. Busto
              d'Ottaviano Augusto ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#144">26. Busto di
              Tiberio ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#146">27. Busto di
              Caligola ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#148">28. Busto di
              Cesare ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#150">29. Busto di
              Domiziano ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#152">30. Busto di
              Tito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#154">31. Pittaco
              Mitileneo ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#156">32. Epicuro
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#158">33. Zenone
              Stoico ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#160">34. Alceo di
              Mitilene ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539601s.html#162">Stanza II</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#162">1. Bacco ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#164">2. David colla
              fionda Scultura del Cavalier Lorenzo Bernini ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#166">3. Minerva
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#168">4. Venere
              sorgente dal bagno ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#170">5. Apollo
              detto il Saurottono Invenzione di Prassitele ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#172">6. Ministro di
              Sagrifizi ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#174">7. Ministro di
              sagrifizi ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#176">8. Fauno in
              atto di sonar lo scabillo ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#178">9. Gran
              cratere marmoreo con Baccanale ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#180">10. Baccanale,
              con Bacco, Sileno, Fauni, e Menadi, scolpito attorno
              al gran vaso di marmo Pentelico trovato agli orti
              Sallustiani ›Horti Sallustiani‹ ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#182">11. Sagrifizio
              Bacchico ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#184">12. Cippo
              Sepolcrale ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#186">13. Cippo
              sepolcrale ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#188">14. Menade con
              daino messo in brani ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#190">15.
              Bassorilievo sepolcrale con Mercurio e 'l Sonno ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#192">16. Muse
              bassorilievo ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#194">17. Città
              turrite in atto di sagrifizio ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#196">18. Genj
              d'Ercole ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#198">19. Amore
              vincitore sull'aquila di Giove ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#200">20. Busto
              femminile incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#202">21. Baccante
              busto ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#204">22. Busto di
              matrona velata ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#206">23. Ritratto
              incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#208">24. Busto di
              Tiberio ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#210">25. Testa
              Romana incognita del primo secolo ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#212">26. Busto
              Romano incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#214">27. Incognito
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#216">28. Incognito
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#218">29. Augusto
              [illustrazione ripetuta; v. infra] ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#220">30. Incognito
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539601s.html#222">Stanza III</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#222">1. Bacco
              Giacente ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#224">2. Statua
              radiata del Sole ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#226">3. Igia, o la
              Salute ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#228">4. Psiche
              perseguitata da Venere ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#230">5. Ercole
              infante ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#232">6. Le Ninfe, o
              le Grazie ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#234">7. Genio
              Bacchico con otre ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#236">8. Bacco ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#238">9. Ercole in
              riposo ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#240">10. Statua di
              marmo nero risarcito per Seneca nel bagno ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#242">11. Marte
              barbato ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#244">12. Sarcofago
              rappresentante la morte di Meleagro, e le caggioni
              [sic] della medesima ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#246">13. Vaso con
              maschere Bacchiche ed ara Quindecemvirale ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#248">14. Ara col
              tripode de quindecemviri, e la corona degli Arvali
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#250">15. Ritratto
              incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#252">16. Cesare
              velato ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#254">17. Ritratto
              incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#256">18. Lucio Vero
              Augusto ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#258">19. Marco
              Aurelio ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#260">20. Ritratto
              Romano incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#262">21. Giulia Pia
              ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#264">22. Busto di
              Giunone velata ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#266">23. Busto di
              Tiberio Cesare ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#268">24. Ritratto
              Romano incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#270">25. Ritratto
              Romano incognito ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#272">26. Busto di
              Tiberio Cesare ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#274">27. Busto
              d'Antonino Caracalla ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#276">28. Busto di
              Caracalla ▣</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#278">29. Busto di
              Commodo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539601s.html#280">Stanza II
          [sic]</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#280">29. Augusto
              [illustrazione ripetuta; v. supra] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539601s.html#282">Stanza V</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#282">15. Lucio Vero
              Augusto [illustrazione ripetuta; v. Parte II] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539601s.html#284">Stanza VII</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#284">5. Cerere
              &#160;[illustrazione ripetuta; v. Parte II] ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="katmrom10539601s.html#288">Sculture del palazzo
      della ›Villa Borghese‹ detta Pinciana brevemente descritte.
      Parte prima [Testo]</a>
      <ul>
        <li>
          <a href="katmrom10539601s.html#290">Al lettore</a>
        </li>
        <li>
          <a href="katmrom10539601s.html#292">Indice delle
          sculture descritte in questo libro</a>
        </li>
        <li>
          <a href="katmrom10539601s.html#302">Portico</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#302">1.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#303">2.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#304">3.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#305">4.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#305">5.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#306">6.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#306">7.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#306">8.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#307">9.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#307">10.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#308">11.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#308">12.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#309">13.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#309">14.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#310">15.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#311">16.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#311">17.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#311">18.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#312">19.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#312">20.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#312">21. 25.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#312">22. 23.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#313">24. 26.
              27.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#313">28. 29.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#313">30. 31.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#314">32. 33.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#314">34. 35.
              36.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#314">37. 38.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#315">39.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#315">40. 41.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#315">42.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#315">43.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539601s.html#316">Stanza I</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#318">1.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#319">2.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#319">3.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#320">4.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#321">5.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#321">6.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#322">7.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#322">8.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#323">9.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#324">10.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#325">11.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#325">12.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#326">13.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#327">14.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#327">15.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#328">16.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#329">17.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#330">18.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#331">19. [fino a]
              30.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#331">31.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#331">32.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#332">33.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#332">34.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539601s.html#333">Stanza II</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#336">1.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#336">2.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#337">3.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#337">4.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#338">5.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#339">6. 7.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#340">8.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#341">9. 10.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#342">11.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#342">12.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#343">13.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#344">14.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#344">15.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#345">16.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#346">17.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#347">18. 19.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#347">20. 21.
              22.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#347">23. 24. 25.
              26.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#348">27. 28. 29.
              30.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmrom10539601s.html#349">Stanza III</a>
          <ul>
            <li>
              <a href="katmrom10539601s.html#350">1.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#351">2.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#352">3.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#353">4.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#354">5. 7.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#355">6.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#356">8.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#356">9.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#357">10.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#358">11.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#358">12.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#360">13.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#362">14.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#363">15. 16.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#363">17. 18.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#363">19. 20.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#364">21. 22.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#364">23. 24.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#365">25. 26.</a>
            </li>
            <li>
              <a href="katmrom10539601s.html#365">27. 28.
              29.</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
