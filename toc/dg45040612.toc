<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg45040612s.html#6">Itineraire instructif de Rome
      ancienne et moderne</a>
    </li>
    <li>
      <a href="dg45040612s.html#8">II . Tome</a>
      <ul>
        <li>
          <a href="dg45040612s.html#8">Table des object
          principaux de Roma contenus dans le second Tome</a>
        </li>
        <li>
          <a href="dg45040612s.html#10">Cinquieme journée</a>
          <ul>
            <li>
              <a href="dg45040612s.html#10">L'Eglise de Saint
              Eustache ›Sant'Eustachio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#11">Collége de la
              Sapience ›Palazzo della Sapienza‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#13">Palais du Governement
              ›Palazzo Madama‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#15">Palais Giustiniani
              ›Palazzo Giustiniani‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#21">L'Eglise de Saint
              Louis des Francais ›San Luigi dei Francesi‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#23">L'Eglise de Saint
              Augustin ›Sant'Agostino‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#26">L'Eglise de Saint
              Apollinaire ›Sant'Apollinare‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#27">Palais Altemps
              ›Palazzo Altemps‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#27">L'Eglise de Saint
              Antoine des Portugals ›Sant'Antonio dei
              Portoghesi‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#29">Palais Lancellotti
              ›Palazzo Lancellotti‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#29">L'Eglise de Saint
              Sauveur in Lauro ›San Salvatore in Lauro‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#30">L'Eglise de Saint
              Celse et de Saint Julien ›Santi Celso e Giuliano‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#32">Banque du Saint
              Esprit ›Palazzo del Banco di Santo Spirito‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#32">Palais Gabrielli
              ›Palazzo Gabrielli‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#32">L'Eglise de Sainte
              Marie in Vallicella appellée l'Eglise Neuve ›Chiesa
              Nuova‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#36">Palais Sora ›Palazzo
              Sora‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#37">L'Eglise de Sainte
              Marie de la Paix ›Santa Maria della Pace‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#39">L'Eglise de Sainte
              Marie de l'Ame ›Santa Maria dell'Anima‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#40">L'Eglise de Saint
              Nicolas des Lorrains ›San Nicola dei Lorenesi‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#41">Place Navone ›Piazza
              Navona‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#43">L'Eglise de Sainte
              Agnès ›Sant'Agnese in Agone‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#46">L'Eglise de Saint
              Jacques des Espagnols ›Nostra Signora del Sacro
              Cuore‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#47">Place de Pasquin
              ›Statua di Pasquino‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#48">L'Eglise de Saint
              Pantaleon ›San Pantaleo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#48">Palais Massimi
              ›Palazzo Massimo alle Colonne‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#49">L'Eglise de Saint
              André della Valle ›Sant'Andrea della Valle‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#52">L'Eglise di Saint
              Suaire ›Santissimo Sudario di Nostro Signore Gesù
              Cristo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#53">L'Eglise de Saint
              Julien des Flamands ›San Giuliano Ospitaliere‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#53">L'Eglise de Saint
              Nicolas des Césarini ›San Nicola ai Cesarini‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#54">L'Eglise de Sainte
              Lucie aux Boutiques Obscures ›Chiesa di Santa Lucia
              alle Botteghe Oscure‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#55">Palais Mattei
              ›Palazzo Mattei di Giove‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#59">Palais Costaguti
              ›Palazzo Costaguti‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#59">L'Eglise et le
              Monastere de Saint Ambroise ›Sant'Ambrogio della
              Massima‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#60">L'Eglise de Sainte
              Cathérine des Funari ›Santa Caterina dei Funari‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#62">L'Eglise de Sainte
              Marie in Campitelli ›Santa Maria in Campitelli‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#63">Maison dite de Tor di
              Specchi ›Monastero di Tor de' Specchi‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#64">Portique d'Octavie
              ›Portico d'Ottavia‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#65">L'Eglise de Saint
              Ange in Pescheria ›Sant'Angelo in Pescheria‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#66">Théatre de Marcellus
              ›Teatro di Marcello‹, aujourd'hui Palais Orsini
              ›Palazzo Orsini‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#68">L'Eglise de Saint
              Nicolas in Carcere ›San Nicola in Carcere‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#69">L'Eglise de Sainte
              Marie de la Consolation ›Santa Maria della
              Consolazione‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#69">L'Eglise de Saint
              Jean Decollé ›San Giovanni Decollato‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#70">L'Arc de Janus
              Quadrifrons ›Arco di Giano‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#72">L'Eglise de Saint
              Géorge in Velabro ›San Giorgio in Velabro‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#72">L'Arc de Septime
              Sévère ›Arco di Settimio Severo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#73">Grande Cloaque
              ›Cloaca Maxima‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#75">L'Eau de Juturne
              ›Fonte di Giuturna‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#75">L'Eglise de Sainte
              Anastasie ›Sant'Anastasia‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#76">Grand Cirque ›Circo
              Massimo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#80">L'Eglise de Saint
              Grégoire ›San Gregorio Magno al Celio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#83">L'Eglise de Sainte
              Balbine ›Santa Balbina‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#83">Thermes de Caracalla
              ›Terme di Caracalla‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#85">L'Eglise de Saint
              Nérée et de Saint Achillée ›Santi Nereo e
              Achilleo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#87">Porte Latine ›Porta
              Latina‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#88">L'Arc de Drusus ›Arco
              di Druso‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#88">Tombeau des Scipions
              ›Sepolcro degli Scipioni‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#90">Porte Saint Sebastien
              ›Porta San Sebastiano‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#92">L'Eglise de Domine
              quo Vadis ›Chiesa del "Domine quo vadis?"‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#93">Basilique de Saint
              Sébastien ›San Sebastiano‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#95">Equiries du Cirque de
              Caracalla ›Scuderie del Circo di Caracalla ‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#96">Tombeau de Cécile
              Metella ›Tomba di Cecilia Metella‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#97">Cirque de Caracalla
              ›Circo di Massenzio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#100">Temple des Camènes,
              vulgairment dit de Bacchus, aujourd'hui l'Eglise de
              Saint Urban ›Sant'Urbano‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#100">La Grotte ou la
              Fontaine d'Egerie ›Camenarum, Fons et Lucus‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#101">Temple de la Fortune
              Muliebre ›Tempio della Fortuna Muliebre‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#102">L'Eglise de Saint
              Paul aux Trois Fontaines ›San Paolo alle Tre
              Fontane‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#104">Basilique de Saint
              Paul ›San Paolo fuori le Mura‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#107">Porte Saint Paul
              ›Porta San Paolo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#108">Pyramide de Cajus
              Cestius ›Piramide di Caio Cestio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#110">L'Eglise de Saint
              Sabas Abbè ›San Saba‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#110">L'Eglise de Sainte
              Prisque ›Santa Prisca‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#111">Mont Aventin
              ›Aventino‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#112">L'Eglise de Sainte
              Sabina ›Santa Sabina‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#112">L'Eglise de Saint
              Alexis ›Sant'Alessio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#113">L'Eglise de Sainte
              Marie du Prieuré de Malthe ›Santa Maria del
              Priorato‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#115">Monte Testaccio
              ›Testaccio (monte)‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#116">Anciens Navalia
              ›Navalia‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#117">Pont Sublicius
              ›Ponte Sublicio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#119">Temple de la
              Pudicité Patricienne ›Tempio della Pudicitia
              Patricia‹ , aujourd'hui l'Eglise de Sainte Marie in
              Cosmedin ›Santa Maria in Cosmedin‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#121">Temple de Vesta
              ›Tempio di Vesta (Foro Boario)‹ , aujourd'hui
              L'Eglise de Sainte Marie du Soleil ›Santa Maria del
              Sole‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#122">Temple de la Fortune
              Virile ›Tempio della Fortuna Virile‹ jourd'hui
              l'Eglise de Sainte Marie Egyptienne ›Santa Maria
              Egiziaca ‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#123">Maison dite
              vulgairement de Pilate ›Casa dei Crescenzi‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#123">Pont Palatin,
              aujourd'hui appellé Ponte-Rotto ›Ponte Rotto‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#124">L'Eglise et
              l'Hospice de Sainte Galle ›Santa Galla
              (scomparsa)‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45040612s.html#126">Sixieme journée</a>
          <ul>
            <li>
              <a href="dg45040612s.html#126">Pont Fabrice,
              appellé aujourd'hui Quattro Capi ›Ponte (dei) Quattro
              Capi‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#127">L'Ile du Tiere
              ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#128">L'Eglise de Saint
              Barthélemi ›San Bartolomeo all'Isola‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#129">L'Eglise de Saint
              Jean Colabite, dite des Benefratelli ›San Giovanni
              Calabita‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#130">Pont Cestius
              aujourd'hui dit de Saint Barthélemi ›Ponte
              Cestio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#130">L'Eglise de Saint
              Benoit in Pescivola ›San Benedetto in Piscinula‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#131">L'Eglise de Sainte
              Cécile ›Santa Cecilia in Trastevere‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#132">L'Eglise de Sainte
              Marie dell'Orto ›Santa Maria dell'Orto‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#133">Port de Ripa-Grande
              ›Porto di Ripa Grande‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#134">L'Hospice de Saint
              Michel ›Ospizio Apostolico di San Michele a Ripa
              Grande (ex)‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#134">Porte Portese ›Porta
              Portuensis‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#135">L'Eglise de Saint
              Francois a Ripa ›San Francesco a Ripa‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#136">L'Eglise des
              Quarante Saints, et de Saint Pascal ›San Pasquale
              Baylon‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#137">L'Eglise de Saint
              Calixte ›San Callisto‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#138">L'Eglise de Sainte
              Marie in Trastevere ›Santa Maria in Trastevere‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#140">L'Eglise de Saint
              Chrysogone ›San Crisogono‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#141">L'Eglise de Sainte
              Marie della Scala ›Santa Maria della Scala‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#142">Mont Janiculus
              ›Gianicolo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#143">L'Eglise de Saint
              Pierre in Montorio ›San Pietro in Montorio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#144">Fontaine Pauline,
              appellèe de Saint Pierre in Montorio ›Fontana
              dell'Acqua Paola (via Garibaldi)‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#145">Porte Saint Pancrace
              ›Porta San Pancrazio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#145">Casin de la Villa
              Giraud ›Villa Giraud‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#146">Villa Corsini
              ›Palazzo Corsini‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#146">Villa Pamfili Doria
              ›Villa Doria Pamphilj‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#147">L'Eglise de Saint
              Pancrace ›San Pancrazio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#149">Porte Septimienne
              ›Porta Settimiana‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#149">Palais Corsini
              ›Palazzo Corsini‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#155">Casin Farnese, dit
              la Farnesine ›Villa Farnesina‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#160">L'Eglise de Saint
              Onuphre ›Sant'Onofrio al Gianicolo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#162">Porte Saint Esprit
              ›Porta Santo Spirito‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#162">L'Eglise de Sainte
              Dorothée ›Santa Dorotea‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#163">Pont Sixte ›Ponte
              Sisto‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45040612s.html#164">Septieme journée</a>
          <ul>
            <li>
              <a href="dg45040612s.html#164">Fontaine du Pont
              Sixte ›Fontana dell'Acqua Paola (Piazza
              Trilussa)‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#164">L'Hospice
              Ecclésiastique, dit des Cent Prétres ›Ospizio dei
              Cento Preti‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#165">L'Eglise et
              l'Hospice de la Trinité des Pelegrins ›SS. Trinità
              dei Pellegrini‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#167">Mont de Piété
              ›Palazzo del Monte di Pietà‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#168">L'Eglise de Saint
              Paul, dite Saint Paulin à la Regola ›San Paolo alla
              Regola‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#169">Palais Santacroce
              ›Palazzo Santacroce‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#170">L'Eglise de Sainte
              Marie in Cacaberis ›Santa Maria in Cacabariis‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#171">L'Eglise et le
              Monastere de la Visitation ›Santa Maria della
              Visitazione‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#172">L'Eglise de Saint
              Charles aux Catinari ›San Carlo ai Catinari‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#173">L'Eglise de Sainte
              Barbe ›Santa Barbara dei Librari‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#174">Place de Campo di
              Fiori ›Campo de' Fiori‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#174">Palais Pio ›Palazzo
              Pio Righetti‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#175">Palais de la
              Chancellerie Apostolique ›Palazzo della
              Cancelleria‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#177">Palais Farnèse
              ›Palazzo Farnese‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#182">L'Eglise de Saint
              Petron ›Santi Giovanni Evangelista e Petronio dei
              Bolognesi‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#182">Palais Spada
              ›Palazzo Spada‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#186">L'Eglise de Sainte
              Marie de l'Oraison, dite de la Mort ›Santa Maria
              dell'Orazione e Morte‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#187">L'Eglise de Sainte
              Catherine des Siennais ›Cappella di Santa Caterina da
              Siena‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#187">L'Eglise de Sainte
              Catherine de la Roue ›Santa Caterina della Rota‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#188">L'Eglise de Saint
              Jerome de la Charité ›San Girolamo della Carità‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#190">L'Eglise de Saint
              Thomas et le Collège des Anglais ›San Tommaso di
              Canterbury‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#191">L'Eglise de Sainte
              Lucie, dite della Chiavica ›Santa Lucia del
              Gonfalone‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#192">L'Eglise de Sainte
              Marie du Suffrage ›Santa Maria del Suffragio‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#193">L'Eglise de Saint
              Jean des Florentins ›San Giovanni dei Fiorentini‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#195">Pont Triomphal ›Pons
              Neronianus‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45040612s.html#198">Huitieme Journèe</a>
          <ul>
            <li>
              <a href="dg45040612s.html#198">Pont Saint Ange
              ›Ponte Sant'Angelo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#199">Mausolée d'Adrien,
              aujourd'hui Chateau Saint Ange ›Castel
              Sant'Angelo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#201">L'Hopital du Saint
              Esprit in Saxia ›Ospedale di Santo Spirito in
              Sassia‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#203">L'Eglise du Saint
              Esprit in Saxia ›Santo Spirito in Sassia‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#204">L'Eglise de Sainte
              Marie de la Transpontine ›Santa Maria in
              Traspontina‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#205">Palais Giraud
              ›Palazzo Castellesi‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#205">L'Eglise de Saint
              Jacques Scosciacavalli ›San Giacomo
              Scossacavalli‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#206">Place de Saint
              Pierre ai Vatican ›Piazza San Pietro‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#208">L'Obélisque du
              Vatican ›Obelisco Vaticano‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#210">Basilique de Saint
              Pierre au Vatican ›San Pietro in Vaticano‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#214">Facade de la
              Basilique de Saint Pierre ›San Pietro in Vaticano:
              facciata‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#217">L'Interieur de la
              Basilique de Saint Pierre ›San Pietro in Vaticano:
              interno‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#220">Confession de Saint
              Pierre ›San Pietro in Vaticano: Tomba di Pietro‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#221">Maitre Autel ›San
              Pietro in Vaticano: Baldacchino di San Pietro‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#222">Grande Coupole ›San
              Pietro in Vaticano: Cupola‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#226">Tribune et Chaire de
              Saint Pierre ›San Pietro in Vaticano: Cattedra di San
              Pietro‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#229">Bas-cóté à droite de
              la Tribune ›San Pietro in Vaticano: navata
              destra‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#231">Croisée Méridionale
              ›San Pietro in Vaticano: Transetto sinistro‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#232">Chapelle Clémentine
              ›San Pietro in Vaticano: Cappella Clementina‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#233">Chapelle du Choeur
              ›San Pietro in Vaticano: Cappella del Coro‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#234">Chapelle de la
              Présentation ›San Pietro in Vaticano: Cappella della
              Presentazione‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#235">Chapelle des Fonts
              Baptismaux ›San Pietro in Vaticano: Battistero‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#236">Chapelle de la Pieté
              ›San Pietro in Vaticano: Cappella della Pietà‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#238">Chapelle de Saint
              Sebastien ›San Pietro in Vaticano: Cappella di San
              Sebastiano‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#239">Chapelle du
              Sacrament ›San Pietro in Vaticano: Cappella del
              Sacramento‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#240">Chapelle de la
              Vierge ›San Pietro in Vaticano: Cappella
              Gregoriana‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#243">Souterrain de la
              Basilique ›San Pietro in Vaticano: Sacre Grotte
              Vaticane‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#245">Sacristie de Saint
              Pierre ›San Pietro in Vaticano: Sagrestia‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#249">Palais du Vatican
              ›Palazzo Apostolico Vaticano‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#252">Chapelle Sixtine
              ›Cappella Sistina‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#255">Loges de Raphael
              ›Vaticano: Loggia di Raffaello‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#259">Chambres de Raphael
              ›Vaticano: Stanze di Raffaello‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#260">Salle de Constantin
              ›Vaticano: Sala di Costantino‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#268">Bibliotheque du
              Vatican ›Biblioteca Apostolica Vaticana‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#275">Musée Pie-Clementin
              ›Vaticano: Museo Pio-Clementino‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#277">Premier Vestibule
              Quarré ›Vaticano: Vestibolo Quadrato‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#279">Vestibule Rond
              ›Vaticano: Vestibolo Rotondo‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#289">Salle des Animaux
              ›Vaticano:Sala degli Animali‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#296">Galerie des Statues
              ›Vaticano: Galleria delle Statue‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#300">Premiere Chambre des
              Bustes ›Vaticano: Galleria dei Busti‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#306">Cabinet ›Vaticano:
              Gabinetto delle Maschere‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#313">Sale Ronde
              ›Vaticano: Sala Rotonda‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#315">Chambre a Croix
              Grecque ›Vaticano: Sala a Croce Greca‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#332">L'Eglise de Sainte
              Marthe ›Santa Marta (Vaticano)‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#335">L'Eglise de Sainte
              Marie de la Pitié in Campo Santo ›Santa Maria della
              Pietà‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#337">›Monte Mario‹</a>
            </li>
            <li>
              <a href="dg45040612s.html#338">›Villa Madama‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45040612s.html#340">Description des environs
          de Rome</a>
          <ul>
            <li>
              <a href="dg45040612s.html#340">Ville de Tivoli</a>
            </li>
            <li>
              <a href="dg45040612s.html#341">Pont de la
              Solfatare</a>
            </li>
            <li>
              <a href="dg45040612s.html#342">Tombeau de la
              Famille Plautie</a>
            </li>
            <li>
              <a href="dg45040612s.html#343">Villa Adrienne</a>
            </li>
            <li>
              <a href="dg45040612s.html#344">Temple de la
              Sibylle</a>
            </li>
            <li>
              <a href="dg45040612s.html#345">Grotte de
              Neptune</a>
            </li>
            <li>
              <a href="dg45040612s.html#346">Grande Cascade de
              l'Aniene</a>
            </li>
            <li>
              <a href="dg45040612s.html#347">Grotte des
              Sirenes</a>
            </li>
            <li>
              <a href="dg45040612s.html#347">Cascatelles de
              Tivoli</a>
            </li>
            <li>
              <a href="dg45040612s.html#348">Villa d'Est</a>
            </li>
            <li>
              <a href="dg45040612s.html#349">Ville de
              Palestrine</a>
            </li>
            <li>
              <a href="dg45040612s.html#350">Ville de
              Frascati</a>
            </li>
            <li>
              <a href="dg45040612s.html#353">Grotta-Ferrata</a>
            </li>
            <li>
              <a href="dg45040612s.html#354">Marino</a>
            </li>
            <li>
              <a href="dg45040612s.html#354">Castel-Gandolfo</a>
            </li>
            <li>
              <a href="dg45040612s.html#356">Ville d'Albano</a>
            </li>
            <li>
              <a href="dg45040612s.html#357">Riccia</a>
            </li>
            <li>
              <a href="dg45040612s.html#358">Gensano</a>
            </li>
            <li>
              <a href="dg45040612s.html#358">Nemi</a>
            </li>
            <li>
              <a href="dg45040612s.html#359">Civita Lavinia</a>
            </li>
            <li>
              <a href="dg45040612s.html#360">Ville de
              Velletri</a>
            </li>
            <li>
              <a href="dg45040612s.html#361">Cora, Bourg</a>
            </li>
            <li>
              <a href="dg45040612s.html#361">Marais Pontins</a>
            </li>
            <li>
              <a href="dg45040612s.html#362">Nettuno</a>
            </li>
            <li>
              <a href="dg45040612s.html#363">Ostie, Ville</a>
            </li>
            <li>
              <a href="dg45040612s.html#365">Civitavecchia</a>
            </li>
            <li>
              <a href="dg45040612s.html#365">Palais de
              Caprarola</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45040612s.html#367">Table Générale dea
          Matieres</a>
        </li>
        <li>
          <a href="dg45040612s.html#402">Note des Planches dans
          cet ouvrage</a>
        </li>
        <li>
          <a href="dg45040612s.html#406">Catalogue des ouvres du
          Chevalier Joseph Vasi</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
