<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502290s.html#6">Le cose meravigliose dell'alma
      città di Roma</a>
      <ul>
        <li>
          <a href="dg4502290s.html#7">Le sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg4502290s.html#7">La prima Chiesa è ›San
              Giovanni in Laterano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#15">La seconda Chiesa è
              ›San Pietro in Vaticano‹</a>
              <ul>
                <li>
                  <a href="dg4502290s.html#13">[›San Pietro in
                  Vaticano‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502290s.html#20">La terza Chiesa &#160;è
              ›San Paolo fuori le Mura‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4502290s.html#22">[›Piramide di Caio
                  Cestio‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502290s.html#23">La quarta Chiesa è
              ›Santa Maria Maggiore‹ ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#26">La quinta Chiesa è ›San
              Lorenzo fuori dalle Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#27">La Sesta Chiesa é ›San
              Sebastiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#29">La Settima Chiesa é
              ›Santa Croce in Gerusalemme‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#30">Nell'›Isola Tiberina‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#30">In ›Trastevere‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#35">In ›Borgo‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#37">Della Porta Flaminia overo
          dal Popolo ›Porta del Popolo‹ fino alle radici del
          ›Campidoglio‹</a>
          <ul>
            <li>
              <a href="dg4502290s.html#41">[›Colonna Traiana‹]
              ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#44">[›Colonna di Antonino
              Pio‹] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#50">Dal ›Campidoglio‹ a man
          sinistra verso i Monti</a>
          <ul>
            <li>
              <a href="dg4502290s.html#51">›Foro Romano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#54">[›Anfiteatro
              Castrense‹] ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#57">[›Terme di
              Diocleziano‹] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#57">Dal ›Campidoglio‹ a man
          dritta verso i Monti</a>
        </li>
        <li>
          <a href="dg4502290s.html#62">Le stationi che sono nelle
          chiese di Roma [...]</a>
          <ul>
            <li>
              <a href="dg4502290s.html#68">Le Stationi
              dell'Advento</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#69">Guida romana per li
          Forastieri che vogliono veder l'Antichità di Roma una per
          una</a>
          <ul>
            <li>
              <a href="dg4502290s.html#69">Del ›Borgo‹, prima
              giornata</a>
              <ul>
                <li>
                  <a href="dg4502290s.html#69">[›Castel
                  Sant'Angelo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502290s.html#70">[›San Pietro in
                  Vaticano‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502290s.html#70">Del
                  ›Trastevere‹</a>
                </li>
                <li>
                  <a href="dg4502290s.html#71">Dell'Isola
                  Tiberina‹ ▣, e Licaonia</a>
                </li>
                <li>
                  <a href="dg4502290s.html#72">Del Ponte di Santa
                  Maria ›Ponte Rotto‹, del Palazzo di Pilato ›Casa
                  dei Crescenzi‹, et altre cose</a>
                  <ul>
                    <li>
                      <a href="dg4502290s.html#72">[›Teatro di
                      Marcello‹]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502290s.html#73">Del Monte Testaccio
                  ›Testaccio (Monte)‹ e di molte altre cose</a>
                  <ul>
                    <li>
                      <a href="dg4502290s.html#73">[›Piramide di
                      Caio Cestio‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4502290s.html#74">[›Arco di
                      Giano‹] ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502290s.html#74">Delle Therme
                  Antoniane ›Terme di Caracalla‹ , ed altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4502290s.html#75">Di ›San Giovanni in
                  Laterano‹, ›Santa Croce in Gerusalemme‹ ▣, et
                  altri</a>
                  <ul>
                    <li>
                      <a href="dg4502290s.html#75">[›Anfiteatro
                      Castrense‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4502290s.html#76">[›Porta
                      Maggiore‹] ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502290s.html#76">Giornata seconda</a>
              <ul>
                <li>
                  <a href="dg4502290s.html#76">Della ›Porta del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4502290s.html#77">Del Monte Cavallo,
                  già detto ›Quirinale‹, e de i Cavalli di Marmo
                  ›Fontana di Monte Cavallo‹ ▣</a>
                  <ul>
                    <li>
                      <a href="dg4502290s.html#77">[›Palazzo del
                      Quirinale‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4502290s.html#78">[›Torre Mesa‹]
                      ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502290s.html#78">Della strada Pia
                  ›Via XX Settembre‹, e della Vigna che era già del
                  Cardinal di Ferrara</a>
                </li>
                <li>
                  <a href="dg4502290s.html#79">Della Vigna del
                  Cardinal di Carpi, et altre cose</a>
                </li>
                <li>
                  <a href="dg4502290s.html#79">Della ›Porta Pia‹,
                  di ›Sant'Agnese fuori le Mura‹, et altre
                  anticaglie</a>
                </li>
                <li>
                  <a href="dg4502290s.html#80">Delle ›Terme di
                  Diocleziano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502290s.html#81">Del ›Colosseo‹ ▣,
                  delle ›Sette Sale‹, et altre cose</a>
                  <ul>
                    <li>
                      <a href="dg4502290s.html#82">[›Arco di
                      Costantino‹] ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502290s.html#83">Del Monte
                  ›Palatino‹, hora detto Palazzo Maggiore. E del
                  Tempio della Pace ›Foro della Pace‹, et altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4502290s.html#84">Del ›Foro di Nerva‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502290s.html#85">Del ›Campidoglio‹,
                  et altre cose ▣</a>
                  <ul>
                    <li>
                      <a href="dg4502290s.html#86">[›Teatro di
                      Marcello‹] ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502290s.html#86">De i portichi di
                  Ottavia ›Portico d'Ottavia‹, et di Settimio
                  ›Porticus Severi‹, et del ›Teatro di Pompeo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502290s.html#87">Giornata terza</a>
              <ul>
                <li>
                  <a href="dg4502290s.html#87">Delle due Colonne
                  una di Antonio Pio, et l'altra di Traiano, et
                  altre cose ›Colonna di Antonino Pio‹ ›Colonna
                  Traiana‹</a>
                </li>
                <li>
                  <a href="dg4502290s.html#87">Della Rotonda,
                  overo ›Pantheon‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502290s.html#88">Dei Bagni di
                  Agrippa, e di Nerone ›Terme di Agrippa‹ ›Terme
                  Neroniano-Alessandrine‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502290s.html#89">Della piazza
                  Navona, e di mastro Pasquino ›Piazza Navona‹ ▣
                  ›Statua di Pasquino‹</a>
                </li>
                <li>
                  <a href="dg4502290s.html#90">[Tavole]</a>
                  <ul>
                    <li>
                      <a href="dg4502290s.html#90">Indice de Sommi
                      Pontefici Romani</a>
                    </li>
                    <li>
                      <a href="dg4502290s.html#102">Reges et
                      Imperatores Romani</a>
                    </li>
                    <li>
                      <a href="dg4502290s.html#105">Li Re di
                      Francia</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#106">Le Stte (sic!) maraviglie
          del mondo</a>
          <ul>
            <li>
              <a href="dg4502290s.html#106">I. Delle Mura di
              Babilonia ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#107">II. Della Torre di
              Faros ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#108">III. Della Statua di
              Giove ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#109">IV. Del Colosso di
              Rodi ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#110">V. Del Tempio di Diana
              ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#111">VI. Del Mausoleo di
              Artemisia ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#112">VII. Delle Piramidi
              d'Egitto ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#113">Le Principali Poste
          d'Italia</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502290s.html#118">Le antichità dell'alma città
      di Roma</a>
      <ul>
        <li>
          <a href="dg4502290s.html#119">Dell'edificatione di
          Roma</a>
        </li>
        <li>
          <a href="dg4502290s.html#120">Del Circuito di Roma</a>
        </li>
        <li>
          <a href="dg4502290s.html#121">Delle Porte</a>
          <ul>
            <li>
              <a href="dg4502290s.html#122">Porta Maggiore [›Arco
              di Costantino‹] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#123">Delle vie</a>
        </li>
        <li>
          <a href="dg4502290s.html#123">Delli Ponti che sono sopra
          il Tevere, e suoi edificatori</a>
        </li>
        <li>
          <a href="dg4502290s.html#124">Dell'Isola del Tevere
          ›Isola Tiberina‹</a>
          <ul>
            <li>
              <a href="dg4502290s.html#125">›Isola Tiberina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#125">Delli Monti</a>
        </li>
        <li>
          <a href="dg4502290s.html#125">Palazzo Maggiore
          ›Palatino‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#126">Monte ›Quirinale‹, hoggi
          detto Monte Cavallo</a>
        </li>
        <li>
          <a href="dg4502290s.html#126">Del Monte Testaccio
          ›Testaccio (Monte)‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#126">Delle Acque, et chi le
          condusse à Roma</a>
        </li>
        <li>
          <a href="dg4502290s.html#127">Della ›Cloaca Maxima‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#127">Delli Acquedotti</a>
        </li>
        <li>
          <a href="dg4502290s.html#128">[›Palatino‹] ▣</a>
        </li>
        <li>
          <a href="dg4502290s.html#128">Delle ›Sette Sale‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#129">Delle Terme cioè Bagni et
          suoi edificatori</a>
          <ul>
            <li>
              <a href="dg4502290s.html#130">›Terme Diocleziane‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#131">Delle Naumachie, dove si
          facevano le battaglie navali, et che cose erano</a>
        </li>
        <li>
          <a href="dg4502290s.html#131">De' Cerchi, et che cosa
          erano</a>
        </li>
        <li>
          <a href="dg4502290s.html#132">De' Theatri, et che cosa
          erano, et suoi edificatori</a>
          <ul>
            <li>
              <a href="dg4502290s.html#132">Del ›Teatro di
              Marcello‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#133">Delli Anfiteatri, et suoi
          edificatori, e &#160;che cosa erano</a>
          <ul>
            <li>
              <a href="dg4502290s.html#133">›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502290s.html#134">Anfiteatro di Statilio
              Tauro ›Teatro di Marcello‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#134">De' Fori, cioè Piazze</a>
          <ul>
            <li>
              <a href="dg4502290s.html#135">Foro Boario, hoggi
              detto Campo Vaccino &#160;›Foro Romano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#136">Delli Archi Trionfali, et
          a chi si davano</a>
          <ul>
            <li>
              <a href="dg4502290s.html#136">[›Arco di Costantino‹]
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#137">De' Portichi</a>
          <ul>
            <li>
              <a href="dg4502290s.html#137">[›Pantheon‹] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502290s.html#138">De' Trofei, et Colonne
          memorande</a>
        </li>
        <li>
          <a href="dg4502290s.html#138">De' Colossi</a>
        </li>
        <li>
          <a href="dg4502290s.html#139">Delle Piramidi</a>
        </li>
        <li>
          <a href="dg4502290s.html#139">Delle Mete</a>
        </li>
        <li>
          <a href="dg4502290s.html#139">Delli Obelischi</a>
        </li>
        <li>
          <a href="dg4502290s.html#139">Delle Statue</a>
        </li>
        <li>
          <a href="dg4502290s.html#139">›Statua di Marforio‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#140">De Cavalli ›Fontana di
          Monte Cavallo‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#140">De' palazzi</a>
        </li>
        <li>
          <a href="dg4502290s.html#140">Della Casa Aurea di Nerone
          ›Domus Aurea‹, e di Antonino</a>
        </li>
        <li>
          <a href="dg4502290s.html#141">Dell'altre case de'
          Cittadini</a>
        </li>
        <li>
          <a href="dg4502290s.html#141">Delle Librarie</a>
        </li>
        <li>
          <a href="dg4502290s.html#141">Delli Horioli</a>
        </li>
        <li>
          <a href="dg4502290s.html#142">Delle Curie, et che cosa
          erano</a>
        </li>
        <li>
          <a href="dg4502290s.html#142">De' Senatuli, et che cosa
          erano</a>
        </li>
        <li>
          <a href="dg4502290s.html#142">De' Magistrati</a>
        </li>
        <li>
          <a href="dg4502290s.html#143">De Comitij, et che cosa
          erano</a>
        </li>
        <li>
          <a href="dg4502290s.html#143">Delle Tribu</a>
        </li>
        <li>
          <a href="dg4502290s.html#143">Delle Regioni, cioè Rioni,
          et sue insegne</a>
        </li>
        <li>
          <a href="dg4502290s.html#143">Delle Basiliche, et che
          cosa erano</a>
        </li>
        <li>
          <a href="dg4502290s.html#143">Della Segretaria del
          Popolo Romano ›Secretarium Senatus‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#144">Dell'›Asilo di Romolo‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#144">Del ›Campidoglio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502290s.html#145">Dello Erario ›Aerarium‹,
          cioè Camera del commune, et che moneta si spendeva in
          Roma in que' tempi</a>
        </li>
        <li>
          <a href="dg4502290s.html#146">Del Gregostasi, et che
          cosa era ›Grecostasi‹ ▣</a>
        </li>
        <li>
          <a href="dg4502290s.html#146">Delli ›Rostri‹, et che
          cosa erano</a>
        </li>
        <li>
          <a href="dg4502290s.html#147">Della Colonna detta
          Miliario ›Miliarium Aureum‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#147">Della colonna Bellica
          ›Columna Bellica‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#147">Della Colonna Lattaria
          ›Columna Lactaria‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#147">Del Tempio di Carmenta
          ›Carmentis, Carmenta‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#147">Dell'›Aequimaelium‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#147">Del ›Campo Marzio‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#147">Del Sigillo Sororio
          ›Tigillum Sororium‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#148">De' Campi Forastieri
          ›Castra Peregrina‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#148">Del ›Vivarium‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#148">Della ›Villa Pubblica‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502290s.html#149">Della ›Taberna
          Meritoria‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#149">De gli Horti</a>
        </li>
        <li>
          <a href="dg4502290s.html#149">Delle ›Carinae‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#150">Del ›Velabro‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#150">Delli Clivi</a>
        </li>
        <li>
          <a href="dg4502290s.html#151">De' Prati</a>
        </li>
        <li>
          <a href="dg4502290s.html#151">De' Granari publichi</a>
        </li>
        <li>
          <a href="dg4502290s.html#151">Delle Carceri publiche</a>
        </li>
        <li>
          <a href="dg4502290s.html#151">Di alcune feste e giuochi,
          che si solevano celebrare in Roma</a>
        </li>
        <li>
          <a href="dg4502290s.html#152">Del Sepolcro d'Adriano,
          d'Augusto, e di Settimio ›Castel Sant'Angelo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502290s.html#153">De' Tempij</a>
        </li>
        <li>
          <a href="dg4502290s.html#154">De' Sacerdoti, delle
          Vergini, vestimenti, vasi et altri instrumenti fatti per
          uso delli Sacrifici, e suoi institutori</a>
        </li>
        <li>
          <a href="dg4502290s.html#155">Dell'Armamentario
          ›Armamentaria‹, et che cosa era</a>
        </li>
        <li>
          <a href="dg4502290s.html#155">Dell'Essercito di Terra, e
          di Mare, e loro insegne</a>
        </li>
        <li>
          <a href="dg4502290s.html#156">De' Trionfi, et à chi si
          concedevano, chi fu il primo trionfatore, e di quante
          maniere erano</a>
        </li>
        <li>
          <a href="dg4502290s.html#156">Delle Corone, et a chi si
          davano</a>
        </li>
        <li>
          <a href="dg4502290s.html#156">Del numero del Popolo
          Romano</a>
        </li>
        <li>
          <a href="dg4502290s.html#157">Delle ricchezze del Popolo
          Romano</a>
        </li>
        <li>
          <a href="dg4502290s.html#157">Della liberalità degli
          antichi Romani</a>
        </li>
        <li>
          <a href="dg4502290s.html#157">Delli Matrimonij antichi e
          loro usanza</a>
        </li>
        <li>
          <a href="dg4502290s.html#158">Della buona creanza, che
          davano a' figliuoli</a>
        </li>
        <li>
          <a href="dg4502290s.html#158">Della separatione de'
          Matrimonij</a>
        </li>
        <li>
          <a href="dg4502290s.html#158">Dell'Essequie antiche, e
          sue cerimonie</a>
        </li>
        <li>
          <a href="dg4502290s.html#159">Delle Torri</a>
        </li>
        <li>
          <a href="dg4502290s.html#159">Del ›Tevere‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#160">Del Palazzo Papale
          ›Palazzo Apostolico Vaticano‹, e Belvedere ›Cortile del
          Belvedere‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#160">Del ›Trastevere‹</a>
        </li>
        <li>
          <a href="dg4502290s.html#161">Epilogo dell'Antichità</a>
        </li>
        <li>
          <a href="dg4502290s.html#161">De' Tempij de gli antichi
          fuori di Roma</a>
        </li>
        <li>
          <a href="dg4502290s.html#162">[›Arco di Costantino‹]
          ▣</a>
        </li>
        <li>
          <a href="dg4502290s.html#163">Quante volte Roma è stata
          presa</a>
        </li>
        <li>
          <a href="dg4502290s.html#163">De' fuochi de gli antichi,
          scritti da pochi autori, cavati d'alcuni fragmenti, et
          historie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
