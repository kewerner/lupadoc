<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502440s.html#6">Roma ricercata nel suo sito, e
      nalla scuola di tutti gli Antiquarij</a>
      <ul>
        <li>
          <a href="dg4502440s.html#8">Al Signor Cavalier Cassiano
          dal Pozzo</a>
        </li>
        <li>
          <a href="dg4502440s.html#11">Al lettore forastiero</a>
        </li>
        <li>
          <a href="dg4502440s.html#19">Indice delle giornate</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502440s.html#20">[Text]</a>
      <ul>
        <li>
          <a href="dg4502440s.html#20">Gioranta prima</a>
          <ul>
            <li>
              <a href="dg4502440s.html#20">Per il ›Borgo‹
              Vaticano</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502440s.html#33">Giornata seconda</a>
          <ul>
            <li>
              <a href="dg4502440s.html#33">Per il ›Trastevere‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502440s.html#40">Giornata Terza</a>
          <ul>
            <li>
              <a href="dg4502440s.html#40">Da strada Giulia ›Via
              Giulia‹ all'Isola di San Bartolomeo ›Isola
              Tiberina‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502440s.html#49">Giornata quarta</a>
          <ul>
            <li>
              <a href="dg4502440s.html#49">Da ›San Lorenzo in
              Damaso‹ al Monte ›Aventino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502440s.html#58">Giornata quinta</a>
          <ul>
            <li>
              <a href="dg4502440s.html#58">Dalla ›Piazza di
              Pasquino‹ per li monti ›Celio‹ e ›Palatino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502440s.html#74">Giornata sesta</a>
          <ul>
            <li>
              <a href="dg4502440s.html#74">Da ›San Salvatore in
              Lauro‹ per Campo Vaccino ›Foro Romano‹ e per le
              ›Carinae‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502440s.html#92">Giornata settima</a>
          <ul>
            <li>
              <a href="dg4502440s.html#92">Della ›Piazza di
              Sant'Apollinare ‹ per il Monte ›Viminale‹, e
              ›Quirinale‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502440s.html#113">Giornata ottava</a>
          <ul>
            <li>
              <a href="dg4502440s.html#113">Da ›Piazza Nicosia‹
              alle Terme Diocletiane ›Terme di Diocleziano‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502440s.html#121">Giornata nona</a>
          <ul>
            <li>
              <a href="dg4502440s.html#121">Da ›Piazza Borghese‹ à
              ›Porta Pinciana‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502440s.html#132">Giornata decima</a>
          <ul>
            <li>
              <a href="dg4502440s.html#132">Per le nove Chiese</a>
              <ul>
                <li>
                  <a href="dg4502440s.html#144">A ›San Paolo fuori
                  le Mura‹</a>
                </li>
                <li>
                  <a href="dg4502440s.html#153">Alle tre Fontane
                  ›Abbazia delle Tre Fontane‹</a>
                </li>
                <li>
                  <a href=
                  "dg4502440s.html#155">All'›Annunziata‹</a>
                </li>
                <li>
                  <a href="dg4502440s.html#156">A ›San
                  Sebastiano‹</a>
                </li>
                <li>
                  <a href="dg4502440s.html#158">Alla Basilica di
                  ›San Giovanni in Laterano‹</a>
                </li>
                <li>
                  <a href="dg4502440s.html#185">A ›Santa Croce in
                  Gerusalemme‹</a>
                </li>
                <li>
                  <a href="dg4502440s.html#187">A ›San Lorenzo
                  fuori le Mura‹</a>
                </li>
                <li>
                  <a href="dg4502440s.html#188">A ›Santa Maria
                  Maggiore‹</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502440s.html#192">Notita delle Porte, Monti,
          e Rioni della Città</a>
          <ul>
            <li>
              <a href="dg4502440s.html#192">Porte della Città</a>
            </li>
            <li>
              <a href="dg4502440s.html#193">In ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502440s.html#194">In ›Borgo‹</a>
            </li>
            <li>
              <a href="dg4502440s.html#194">Monti dentro la
              Città</a>
            </li>
            <li>
              <a href="dg4502440s.html#196">Rioni</a>
            </li>
            <li>
              <a href="dg4502440s.html#196">Piazze, nelle quali si
              vendono vettovaglie, anticamente dette Macelli</a>
            </li>
            <li>
              <a href="dg4502440s.html#198">Piazze, e Contrade,
              dove risiedono diverse arti, e si fanno Fiere, e
              Mercati</a>
            </li>
            <li>
              <a href="dg4502440s.html#202">Strade principali
              delle Città</a>
            </li>
            <li>
              <a href="dg4502440s.html#208">Errori da
              correggersi</a>
            </li>
            <li>
              <a href="dg4502440s.html#212">Indice delle cose più
              notabili</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
