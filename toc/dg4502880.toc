<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502880s.html#8">Ritratto di Roma antica
      [...]</a>
      <ul>
        <li>
          <a href="dg4502880s.html#6">[Antiporta] ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#10">All'Eminentissimo e
          Reverendissimo Principe, il Signor Cardinal Carlo
          Barberini [dedica dell'editore]</a>
        </li>
        <li>
          <a href="dg4502880s.html#14">Al prudente lettore</a>
        </li>
        <li>
          <a href="dg4502880s.html#16">[Imprimatur]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502880s.html#16">[Testo dell'opera]</a>
      <ul>
        <li>
          <a href="dg4502880s.html#18">Geneaologia [sic] di Romolo
          ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#24">Del ›Fico Ruminale‹ ▣,
          della Casa di Faustulo ›Casa di Faustolo‹ ▣, di Catilina
          ›Domus Catilinae‹ ▣, di Scauro ›Casa di Scauro‹ ▣, e
          della ›Velia‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#26">Delle Mura della città di
          Romolo ›Muro di Romolo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#28">Delle Porte di Roma nel
          tempo di Romolo</a>
        </li>
        <li>
          <a href="dg4502880s.html#29">Delle diverse
          circonferenze, che hebbero dopo Romolo, le mura di
          Roma</a>
        </li>
        <li>
          <a href="dg4502880s.html#33">Dove si distendessero le
          mura del Re Servio ›Mura Serviane‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#37">Delle mura
          dell'Imperador'Aureliano ›Mura Aureliane‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#39">Del numero delle Porte di
          Roma</a>
        </li>
        <li>
          <a href="dg4502880s.html#40">Delli sette Colli, ò [sic]
          Monti di Roma, e primieramente del ›Palatino‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#43">Del Monte ›Capitolino
          (Monte)‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#45">Del Monte ›Celio‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#45">Alloggiamenti de
              Soldati forastieri ›Castra Peregrina‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#45">Aloggiamenti [sic] di
              Albani ›Mansiones Albanae‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#48">Del Monte ›Aventino‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#48">Tempio della
              Vittoria›Victoria, Aedes‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#48">Tempio della Luna
              ›Luna, Aedes‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#48">›Tempio di Giunone
              Regina‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#48">›Tempio di Libertas‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#50">Del Monte ›Quirinale‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#53">Del Monte ›Viminale‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#55">Del Monte ›Esquilino‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#58">Del Monte ›Gianicolo‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#59">Del Monte, e Campo
          ›Vaticano‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#60">Delle porte antiche, e
          moderne di Roma, e primieramente di quelle del Rè Servio
          Tullio</a>
        </li>
        <li>
          <a href="dg4502880s.html#69">Porte
          dell'Imperador'Aureliano</a>
        </li>
        <li>
          <a href="dg4502880s.html#73">Romolo primo Rè di Roma
          ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#76">Delle Tavole, ò [sic]
          vogliamo dire, Libri publici</a>
        </li>
        <li>
          <a href="dg4502880s.html#78">Re de' Romani ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#78">Di Numa Pompilio secondo
          Rè</a>
        </li>
        <li>
          <a href="dg4502880s.html#80">Di Tullo Ostilio terzo
          Rè</a>
        </li>
        <li>
          <a href="dg4502880s.html#83">Di Anco Martio quarto
          Rè</a>
        </li>
        <li>
          <a href="dg4502880s.html#84">Di Tarquinio Prisco quinto
          Rè</a>
        </li>
        <li>
          <a href="dg4502880s.html#85">Di Servio Tullio sesto
          Rè</a>
        </li>
        <li>
          <a href="dg4502880s.html#87">Di Tarquinio Superbo
          settimo, et ultimo Rè</a>
        </li>
        <li>
          <a href="dg4502880s.html#89">Del ›Campidoglio‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#93">Del ›Tempio di Giove
          Capitolino‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#99">Dell'Asilo ›Asilo di
          Romolo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#101">Delli Sponsali, de gli
          antichi Romani ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#104">Accompagnamento della
          Sposa, nella Casa del suo Sposo ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#106">Del Divortio</a>
        </li>
        <li>
          <a href="dg4502880s.html#107">Del ›Grecostasi‹ ▣, del
          ›Tempio della Concordia‹ ▣, del Senatulo ›Senaculum‹ ▣, e
          della ›Basilica Opimia‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#109">Del ›Tempio di Bellona‹ ▣,
          e della Colonna Bellica ›Columna Bellica‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#111">Dello Stipendio, e de'
          Premij delli soldati Romani ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#114">Dell'Insegne Militari del
          Popolo Romano ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#117">Delle Corone, che si
          davano in premio a' vincitori ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#120">Del Sacrificio Militare
          de' Trionfanti ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#122">Vasi, et altri
          Instrumenti, che anticamente servivano per l'uso de'
          Sacrificij ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#125">Del Tripode instrumento
          antico ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#126">Della Colonna Milliaria
          [sic] ›Miliarium Aureum‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#128">Della ›Colonna Menia‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#129">Colonna Lattaria ›Columna
          Lactaria‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#130">Della Colonna Rostrata
          ›Rostri‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#133">Della ›Curia Hostilia‹, e
          dell'altre Curie ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#135">De Porti, et Armate
          marittime, e Militie Romane ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#137">De' Colossi, e
          descrittione loro ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#140">Delli Palazzi più
          riguardevoli</a>
        </li>
        <li>
          <a href="dg4502880s.html#140">Delle Case più nobili, de'
          Cittadini</a>
        </li>
        <li>
          <a href="dg4502880s.html#141">Della ›Statua di Marforio‹
          ▣, e della Segretaria del Popolo Romano ›Secretarium
          Senatus‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#143">De' Tempij della Fede
          ›Fides, Templum‹ ▣, e d'Apollo ›Tempio di Apollo Aziaco‹
          ▣, e della Libreria Palatina ›Biblioteca Palatina‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#147">Di ›Roma Quadrata‹ ▣, e
          de' ›Bagni Palatini‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#149">Del Palazzo di Augusto,
          overo Maggiore ›Domus Augustana‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#153">Della Casa di Pomponio
          Attico ›Domus Pomponii‹ ▣, di quella di Flavio Sabino
          ›Domus Flavia (Quirinale)‹ ▣, e del ›Tempio di Quirino‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#155">Del Tempio ›Tempio di
          Vesta (Foro Romano)‹ ▣, e Selva della Dea Vesta ›Lucus
          Vestae‹ ▣, e del Palazzo di Numa Pompilio ›Regia‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#155">Sepolcri Vestali
              ›Atrium Vestae‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#157">Dell'Argileto ›Argiletum‹,
          della Casa di Spurio Melio ›Aequimaelium‹ ▣, e di
          Scipione Africano ›Casa di Scipione l'Africano‹ ▣:
          dell'Equimelio ›Aequimaelium‹, della Basilica di
          Sempronio ›Basilica Sempronia‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#159">Della Casa Aurea di Nerone
          ›Domus Aurea‹ ▣, e di quella di Servio Tullio ›Casa di
          Servio Tullio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#163">Della Torre delle Militie
          ›Torre delle Milizie‹ ▣, e della Casa de' Cornelij ›Domus
          L. Cornelius Pusio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#165">Dell'Argine di Tarquinio
          Superbo ›Argine di Tarquinio‹ ▣; della ›Casa di Pompeo‹
          ▣, e di Virgilio ›Casa di Virgilio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#167">Giardini di Lucullo ›Horti
          Luculliani‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#169">Alloggiamenti de' Soldati,
          Peregrini ›Castra Peregrina‹; de gli Albani ›Mansiones
          Albanae‹, et altri</a>
        </li>
        <li>
          <a href="dg4502880s.html#171">Del ›Campo Marzio‹ ▣, del
          Campo di Agrippa ›Campus Agrippae‹ ▣, e del Tempio de'
          Lari, ò Dei domestici ›Tempio dei Lari Permarini‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#177">Delli due Portici Ottavij
          ›Portico di Ottavia‹ ›Portico di Ottavio‹ ▣, del Portico
          ›Portico di Pompeo‹ ▣, e ›Teatro di Pompeo‹, e della sua
          Curia ›Curia Pompeia‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#177">Tempio di Venere
              Vittrice [sic] ›Venus Victrix, Aedes‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#181">Della Sepoltura di Numa
          Pompilio ›Sepulcrum Numae Pompilii‹ ▣, de gl'Horti di
          Martiale ›Horti di Giulio Marziale‹ ▣, e del Tribunale di
          Aurelio ›Tribunal Aurelium‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#183">Del Colle degli Hortuli
          ›Pincio‹ ▣, della Casa di Pincio Senatore ›Domus
          Pinciana‹ ▣, e del Sepolcro di Nerone ›Sepulcrum Domitii‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#185">Del ›Foro Romano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#192">Del ›Foro Olitorio‹ ▣, del
          Tempio di Giunone Matuta ›Tempio di Giunone Sospita (Foro
          Olitorio)‹ ▣, della Speranza ›Tempio alla Speranza‹ ▣,
          della Pietà ›Tempio della Pietas‹ ▣, e della Prigione
          della Plebe ›Carcere Mamertino‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#195">Del Foro Archimonio ›Forum
          Archimonii‹ ▣, del ›Tempio di Flora‹ ▣, della Casa di
          Martiale ›Domus M.V. Martialis‹ ▣, e della ›Pila
          Tiburtina‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#197">Del ›Foro di Augusto‹, di
          Nerva ›Foro di Nerva‹ ▣, di Cesare ›Foro di Cesare‹ ▣ del
          ›Tempio di Marte Ultore‹ ▣, e di Venere Genitrice ›Tempio
          di Venere Genitrice‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#202">Del Foro Traiano ›Foro di
          Traiano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#207">De gli Horti di Salustio
          ›Horti Sallustiani‹ ▣; e del Campo Scelerato ›Campus
          Sceleratus‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#210">Della Basilica, overo
          Tempio d'Antonino Pio ›Tempio di Adriano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#212">Dell'Anfiteatro chiamato
          ›Colosseo‹ ▣, e de gli ornamenti di quello</a>
        </li>
        <li>
          <a href="dg4502880s.html#216">De' Spettacoli, e Caccie
          [sic] del ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#218">Della Meta Sudante ›Meta
          Sudans‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#220">Dell'›Anfiteatro di
          Statilio Tauro‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#222">Del ›Teatro di Marcello‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#224">De gli Archi Trionfali, e
          primieramente di quello, di Settimio Severo ›Arco di
          Settimio Severo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#226">Dell'Arco di Settimio
          Severo, à San Giorgio ›Arco degli Argentari‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#226">›Arco di Settimio
              Severo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#228">Dell'›Arco di Tito‹
          Vespasiano ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#231">Dell'›Arco di Costantino‹
          Magno ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#234">Dell'Arco di Domitiano,
          già detto di Portogallo ›Arco di Portogallo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#236">Dell'›Arco di Gallieno‹ ▣,
          e de' ›Trofei di Mario‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#239">Dell'Arco di Camigliano
          ›Arco di Camilliano‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#240">Dell'›Acqua Claudia‹, e
          suo Aquedotto [sic] ›Acquedotto Claudio‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#240">›Porta Maggiore‹
              &#160;▣ e condotto dell'›Acqua Claudia‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#244">Dell'›Acqua Vergine‹ ▣,
          hoggi detta di Trevi, dell'Appia ›Acqua Appia‹, Tepula
          ›Acqua Tepula‹, Martia ›Acqua Marcia‹, Giulia ›Acqua
          Iulia‹, Alsietina ›Acqua Alsietina‹, et altre</a>
          <ul>
            <li>
              <a href="dg4502880s.html#244">Lago Ioturno ›Fonte di
              Giuturna‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#244">Acqua Vergine [sic:
              per Acqua Felice] ›Mostra dell'Acqua Felice‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#244">Acqua Felice [sic: per
              Acqua Vergine] ›Arcus Claudii (Via del Nazareno)‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#249">Delle Chiaviche</a>
        </li>
        <li>
          <a href="dg4502880s.html#249">Dell'antiche Vie de'
          Romani</a>
        </li>
        <li>
          <a href="dg4502880s.html#251">Del Ponte ›Pons
          Neronianus‹ ▣, et Arco Trionfale ›Arcus Arcadii, Honorii
          et Theodosii‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#253">Del Trionfo de' Romani, e
          sua descrittione ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#257">Dichiaratione del Trionfo,
          per ordine della Figura</a>
        </li>
        <li>
          <a href="dg4502880s.html#259">Delli Re, e Regine
          condotte in Trionfo, e d'alcune vittorie, de' Romani
          ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#261">Della Consacratione
          dell'Imperadori dopo la morte, e sua descrittione ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#264">Dichiaratione della
          Consacratione, per ordine della Figura</a>
        </li>
        <li>
          <a href="dg4502880s.html#265">Dell'›Isola Tiberina‹ ▣,
          del ›Tempio di Esculapio‹ ▣, di Giove ›Tempio di Iuppiter
          Iurarius‹ ▣, e di Fauno ›Tempio di Fauno‹ ▣, ›Ponte
          Fabricio‹ ▣, e del Cestio ›Ponte Cestio‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#265">Tempio di Berecintia
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#269">De' Ponti, Senatorio
          ›Ponte Rotto‹ ▣, Sublicio ›Ponte Sublicio‹ ▣, Ianiculense
          ›Ponte Sisto‹, Elio ›Ponte S. Angelo‹, Milvio ›Ponte
          Milvio‹, et altri</a>
        </li>
        <li>
          <a href="dg4502880s.html#273">Del ›Tevere‹ ▣, sua
          denominatione, et origine, e de' Navali antichi
          ›Emporium‹ di Ripa ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#277">Delle Saline ›Salinae‹ ▣,
          e del ›Monte Testaccio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#279">Della Mole d'Adriano
          ›Castel S. Angelo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#281">Del ›Mausoleo di Augusto‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#283">Delli vestigij, del
          Mausoleo d'Augusto ›Mausoleo di Augusto‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#285">Del Settizonio, vecchio
          ›Septizodium (1)‹ ▣, e di quello di Seuero Imperadore
          ›Septizodium (2)‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#287">Delli Granaij del Popolo
          Romano ›Horrea‹. Del Sepolcro di C. Cestio ›Piramide di
          Caio Cestio‹ ▣, e della Selva Hilerna ›Lucus
          Helernus‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#290">Della Sepoltura di Metella
          ›Tomba di Cecilia Metella‹ ▣, e d'altri Sepolcri
          antichi</a>
          <ul>
            <li>
              <a href="dg4502880s.html#290">Custodia de' Soldati
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#292">De gli Horti ›Horti
          Maecenatiani‹ ▣, e Torre di Mecenate ›Turris
          Maecenatiana‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#294">Del Tempio della Pace
          ›Foro della Pace‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#297">De' Vestigij del Tempio,
          della Pace › Foro della Pace‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#299">Del Tempio di Vulcano
          ›Volcanal‹ ▣, del Sole, e della Luna ›Tempio di Venere e
          Roma‹ ▣, e della ›Via Sacra‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#301">Del Tempio di Diana
          ›Tempio di Diana (Aventino)‹ ▣, e della Spelonca di Cacco
          ›Atrium Caci‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#303">Del Tempio ›Aedes Herculis
          Invicti‹ ▣, Altare ›Ara massima di Ercole‹ ▣, e Statua di
          Ercole ›Statua di Ercole in bronzo dorato‹ ▣: del del
          [sic] Tempio della Pudicitia Patritia ›Tempio della
          Pudicitia Patricia‹ ▣, di Matuta ›Tempio di Mater Matuta‹
          ▣, e della Fortuna ›Tempio della Fortuna‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#305">Del ›Pantheon‹, hoggi
          detto, la Rotonda ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#308">Del ›Tempio della
          Concordia‹ ▣, del Senatulo ›Senaculum‹, e della Scuola
          Xanta ›Schola Xanthi‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#311">Del Tempio della Fortuna
          Virile ›Tempio di Portunus‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#313">De' Vestigij del Tempio
          del Sole, ò della Salute ›Tempio di Serapide‹ ▣, nel
          Quirinale</a>
        </li>
        <li>
          <a href="dg4502880s.html#315">Del ›Tempio di Saturno‹ ▣,
          e dell'Erario Publico ›Aerarium‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#318">Della Basilica di Paolo
          Emilio ›Basilica Aemilia‹ ▣, e della Giulia ›Basilica
          Iulia‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#320">Del ›Tempio di Antonino e
          Faustina‹ ▣, e di quello, di Romolo e Remo ›Tempio del
          Divo Romolo‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#322">Del ›Tempio di Giove
          Statore‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#324">Del Tempio di Giano
          Quadrifronte ›Arco di Giano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#326">Delli Tempij del Sole
          ›Tempio del Sole e della Luna‹ ▣, di Mercurio ›Tempio di
          Mercurio‹, Bacco, Cerere, Proserpina ›Tempio di Cerere,
          Libero e Libera‹ ▣, et altri</a>
          <ul>
            <li>
              <a href="dg4502880s.html#326">Tempio di Flora
              ›Flora, Aedes‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#328">Del Tempio d'Apollo
          ›Tempio del Sole‹ ▣, di Giove, Minerva ›Capitolium vetus‹
          ▣, et altri</a>
          <ul>
            <li>
              <a href="dg4502880s.html#328">Tempio di Giunone
              ›Capitolium vetus‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#328">Tempio della Fortuna
              ›Santuario di Fortuna (Vicus Longus)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#330">Del Sepolcro di Caio
          Poblicio ›Sepolcro di Gaio Poplicio Bibulo‹ ▣, della Casa
          de'Corvini ▣, del Tempio d'Iside ›Tempio di Iside e
          Serapide‹ ▣, e di Minerva ›Tempietto di Minerva‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#334">Della Contrada di
          ›Suburra‹, del Tempio di Silvano ›Portico del dio
          Silvano‹ ▣, e del Testamento di Giocondo Soldato ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#338">Del Tempio del Dio Conso
          ›Tempio di Consus‹ ▣, overo del Consiglio, di Nettuno
          ›Ara di Consus‹ ▣, e di quello della Gioventù ›Tempio di
          Iuventas‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#340">Delli Tempij, della
          Fortuna Muliebre ›Ara della Fortuna Redux‹ ▣, di Tempesta
          ›Tempio delle Tempeste‹, e di Marte ›Aedes Martis extra
          Portam Capenam‹ ▣; e della Pietra Manale ›Lapis Manalis
          ▣‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#343">De' Tempij d'Iside ›Tempio
          di Isis Athenodoria‹ ▣, dell'Honore, della Virtù ›Tempio
          di Honos et Virtus‹ ▣, di Quirino›Aedes Martis extra
          Portam Capenam‹ ▣, e di Diana ›Sacello di Diana‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#345">Del Tempio delle Muse,
          overo Camene ›Fons et Lucus Camenarum‹ ▣, e del Dio
          Ridicolo ›Tempio di Rediculus‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#345">Selva Egeria ›Fons et
              Lucus Camenarum‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#347">Del Tempio della Fortuna
          Primigenia ›Tempio della Fortuna Primigenia "in Colle"‹
          ▣, del Tempio della Salute ›Tempio della Salus‹ ▣, e del
          Senatulo delle Donne ›Senaculum Mulierum‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#349">Del Tempio di Nenia
          ›Santuario di Naenia‹ ▣, di Bacco ›Mausoleo di Santa
          Costanza‹ ▣, della ‹Villa di Faonte‹ ▣, del Campo della
          Custodia ›Castra nova equitum singularium‹ ▣, e
          dell'Hippodromo ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#351">Del Tempio di Fauno ›Santo
          Stefano Rotondo‹ ▣, di Venere, e Cupidine ›Tempio di
          Venere e Cupido‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#353">Delle Terme Romane, e
          primieramente di quelle, di Marco [Vipsanio] Agrippa
          ›Terme di Agrippa‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#355">Delle Terme di Nerone
          ›Terme Neroniano-Alessandrine‹ ▣, e della Palude Caprea
          ›Caprae Palus‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#358">Delle ›Terme di
          Costantino‹ Magno ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#358">›Bagni di Paolo
              Emilio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#362">Delle Carine ›Carinae‹,
          delle ›Terme di Tito‹ ▣, di Traiano ›Terme di Traiano‹, e
          di Filippo ›Terme di Filippo‹ Imperadori, e delle ›Sette
          Sale‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#365">Delle Terme di Diocletiano
          ›Terme di Diocleziano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#368">Delle Terme di Gordiano
          ›Parco dei Gordiani‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#370">Delle Terme di Antonino
          Caracalla ›Terme di Caracalla‹, hoggi dette le
          Antoniniane ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#372">Delle Terme di Decio
          ›Terme Deciane‹ ▣, Adriano, Domitiano, et altri Principi
          Romani</a>
          <ul>
            <li>
              <a href="dg4502880s.html#372">Scale Gemoníe ›Scalae
              Gemoniae‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#372">Clivo Publico ›Clivo
              dei Publicii‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#375">Delle Terme d'Aureliano
          ›Thermae Aurelianae‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#375">Tempio della Fortuna
              forte ›Tempio della Fors Fortuna‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#375">Terme di Settimio
              Severo ›Thermae Septimianae‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#377">Del Circo Agonale ›Stadio
          di Domiziano‹, hora chiamato ›Piazza Navona‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#379">Del ›Circo Flaminio‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#379">›Tempio di Vulcano
              (Circo Flaminio)‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#379">›Tempio di Nettuno‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#382">Del ›Circo Massimo‹</a>
          <ul>
            <li>
              <a href="dg4502880s.html#382">Monte ›Palatino‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#382">Monte ›Aventino‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#387">Del Circo di Antonino
          Caracalla ›Circo di Massenzio‹ ▣, di Eliogabalo ›Circo
          Variano‹, di Nerone ›Circo di Caligola‹, et altri</a>
        </li>
        <li>
          <a href="dg4502880s.html#390">Della Naumachia di Nerone
          ›Naumachia di Traiano‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#390">Cerchio di Nerone
              ›Circo di Caligola‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#392">Della Naumachia di
          Domitiano ›Naumachia Domitiani‹ ▣, e del Tempio della
          Famiglia Flavia ›Aedes gentis Flaviae‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#394">Delle Naumachie di Cesare
          ›Naumachia Caesaris‹ ▣ , d'Augusto ›Naumachia Augusti‹, e
          della Naumachia Vecchia ›Vetus Naumachia‹</a>
          <ul>
            <li>
              <a href="dg4502880s.html#394">Prati di Mutio Scevola
              ›Prata Mutia‹ ▣</a>
            </li>
            <li>
              <a href="dg4502880s.html#394">›Horti di Cesare‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#398">Del ›Porto di Claudio
          (Ostia Antica)‹ ▣, e di Traiano ›Porto di Traiano (Ostia
          Antica)‹ ▣ Imperadori</a>
        </li>
        <li>
          <a href="dg4502880s.html#401">Della Colonna d'Antonino
          Pio ›Colonna di Marco Aurelio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#404">Della ›Colonna Traiana‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#408">Delli Obelischi, overo
          Guglie di Roma ▣</a>
          <ul>
            <li>
              <a href="dg4502880s.html#409">Dell'›Obelisco
              Vaticano‹</a>
            </li>
            <li>
              <a href="dg4502880s.html#413">Dell'Obelisco di San
              Giovanni in Laterano ›Obelisco Lateranense‹</a>
            </li>
            <li>
              <a href="dg4502880s.html#415">Dell'Obelisco di Santa
              Maria Maggiore ›Obelisco Esquilino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#418">Dell'Obelisco di Santa
          Maria del Popolo ›Obelisco Flaminio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#421">Dell'Obelisco di Piazza
          Navona ›Obelisco agonale‹</a>
        </li>
        <li>
          <a href="dg4502880s.html#423">Delli Obelischi, di Santa
          Maria sopra Minerva ›Pulcin della Minerva‹, di San Mauto
          ›Obelisco di Ramsses II (1)‹ ▣, e delli Giardini, Mediceo
          ›Obelisco di Boboli‹ ▣, Ludovisio ›Obelisco Sallustiano‹,
          e Mattei ›Obelisco di Ramsses II (2)‹ ▣</a>
        </li>
        <li>
          <a href="dg4502880s.html#428">Altre notitie dell'Imperio
          Romano e de' costumi delle sue Genti</a>
          <ul>
            <li>
              <a href="dg4502880s.html#429">Quali fossero, i
              confini dell'Imperio Romano, e della sua
              duratione</a>
            </li>
            <li>
              <a href="dg4502880s.html#430">Delle Legioni, et
              altre Melitie [sic] Romane</a>
            </li>
            <li>
              <a href="dg4502880s.html#432">Delle Colonie</a>
            </li>
            <li>
              <a href="dg4502880s.html#433">Della moltitudine de'
              Romani</a>
            </li>
            <li>
              <a href="dg4502880s.html#435">Delle Gabelle,
              dell'Imperio Romano</a>
            </li>
            <li>
              <a href="dg4502880s.html#437">Delle Ricchezze,
              cavate dalli Trionfi</a>
            </li>
            <li>
              <a href="dg4502880s.html#438">Delle Spese, fatte ne'
              Soldati, ne' Magistrati, e nel Popolo</a>
            </li>
            <li>
              <a href="dg4502880s.html#440">Delle Spese de'
              Giuochi</a>
            </li>
            <li>
              <a href="dg4502880s.html#441">Delle Spese per i
              Dottori Romani</a>
            </li>
            <li>
              <a href="dg4502880s.html#442">De' Doni, di Giulio
              Cesare</a>
            </li>
            <li>
              <a href="dg4502880s.html#443">Delli Doni, di Ottavio
              Augusto</a>
            </li>
            <li>
              <a href="dg4502880s.html#443">Di Quelli, di Nerone;
              e d'altri Principi Romani</a>
            </li>
            <li>
              <a href="dg4502880s.html#445">Delle Ricchezze,
              d'alcuni Cittadini privati</a>
            </li>
            <li>
              <a href="dg4502880s.html#446">Delle Case, e Ville
              de' Privati</a>
            </li>
            <li>
              <a href="dg4502880s.html#447">Della Magnificenza
              delle Fabriche</a>
            </li>
            <li>
              <a href="dg4502880s.html#449">Delli Borghi, e del
              Popolo numeroso di Roma</a>
            </li>
            <li>
              <a href="dg4502880s.html#450">Distintioni, de'
              Romani</a>
            </li>
            <li>
              <a href="dg4502880s.html#451">Delle Virtù de'
              Romani</a>
            </li>
            <li>
              <a href="dg4502880s.html#452">Della Giustitia de'
              Romani</a>
            </li>
            <li>
              <a href="dg4502880s.html#454">Della Fortezza
              militare de' Romani</a>
            </li>
            <li>
              <a href="dg4502880s.html#456">Della Pietà, e
              Costanza de' Romani</a>
            </li>
            <li>
              <a href="dg4502880s.html#460">Della Sobrietà, e
              moderatione de' Romani</a>
            </li>
            <li>
              <a href="dg4502880s.html#463">Della Liberalità</a>
            </li>
            <li>
              <a href="dg4502880s.html#465">Della Pudicitia</a>
            </li>
            <li>
              <a href="dg4502880s.html#466">Di Alcuni Vitij
              biasimevoli, de' Romani</a>
            </li>
            <li>
              <a href="dg4502880s.html#470">De' Triclinij, overo
              Mense de' Romani, e di alune [sic: per alcune]
              curiosità, spettanti alle medesime</a>
            </li>
            <li>
              <a href="dg4502880s.html#474">Di alcune Vesti
              Principali, de' Romani</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502880s.html#478">Conclusione dell'
          Opera</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502880s.html#480">Per le Rovine, di Roma Antica.
      Sonetto del Signor Girolamo Preti</a>
    </li>
    <li>
      <a href="dg4502880s.html#482">Indice delle cose notabili, di
      Roma Antica</a>
    </li>
  </ul>
  <hr />
</body>
</html>
