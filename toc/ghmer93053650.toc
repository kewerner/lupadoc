<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghmer93053650s.html#8">La théologie des peintres,
      sculpteurs, graveurs et dessinateurs, où l’on explique les
      principes &amp; les véritables règles [...]</a>
      <ul>
        <li>
          <a href="ghmer93053650s.html#10">Préface</a>
        </li>
        <li>
          <a href="ghmer93053650s.html#22">Table des chapitres et
          des articles</a>
        </li>
        <li>
          <a href="ghmer93053650s.html#32">Premiere partie</a>
        </li>
        <li>
          <a href="ghmer93053650s.html#88">Seconde partie</a>
        </li>
        <li>
          <a href="ghmer93053650s.html#150">Troisieme partie</a>
        </li>
        <li>
          <a href="ghmer93053650s.html#232">Quatrieme partie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
