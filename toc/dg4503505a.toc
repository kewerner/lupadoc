<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503505as.html#10">Descrizione di Roma e
      dell'Agro Romano</a>
      <ul>
        <li>
          <a href="dg4503505as.html#12">Eminentissimo, e
          Reverendissimo Principe [dedica al Cardinale Antonio
          Saverio Gentili da parte dell'editore-libraio Domenico
          Francioli]</a>
        </li>
        <li>
          <a href="dg4503505as.html#18">Prefazione</a>
        </li>
        <li>
          <a href="dg4503505as.html#22">Indice de capi</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503505as.html#24">Esposizione della carta
      topografica Cingolana dell'Agro Romano</a>
      <ul>
        <li>
          <a href="dg4503505as.html#24">I. Nella quale si
          premettono alcune notizie universali, e necessarie
          all'intelligenza dell'Opera.</a>
          <ul>
            <li>
              <a href="dg4503505as.html#24">Proemio.</a>
            </li>
            <li>
              <a href="dg4503505as.html#26">II. Del Circuito di
              Roma, e sue Porte.</a>
            </li>
            <li>
              <a href="dg4503505as.html#37">III. Delle Vie.</a>
            </li>
            <li>
              <a href="dg4503505as.html#40">Titi Arcus ›Arco di
              Tito‹ ▣</a>
            </li>
            <li>
              <a href="dg4503505as.html#47">IV. De Rioni.</a>
            </li>
            <li>
              <a href="dg4503505as.html#51">V. De i Monti, e
              Colli principali di Roma.</a>
            </li>
            <li>
              <a href="dg4503505as.html#53">V [sic]. Del
              ›Tevere‹, e suoi Ponti.</a>
            </li>
            <li>
              <a href="dg4503505as.html#61">VII. Delli Tempj, e
              cose Sagre.</a>
            </li>
            <li>
              <a href="dg4503505as.html#66">VIII. De' Cimiterj, e
              Sepolcri.</a>
            </li>
            <li>
              <a href="dg4503505as.html#81">IX. De i Cimiteri
              particolari de' Gentili.</a>
            </li>
            <li>
              <a href="dg4503505as.html#84">X. Sistema della
              Polizia de' Romani.</a>
              <ul>
                <li>
                  <a href="dg4503505as.html#84">I. Delle
                  Tribù.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#85">II. Delle
                  Curie.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#85">III. De
                  Senatori.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#86">IV. Dell'Ordine
                  Equestre.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#87">V. Della
                  Plebe.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#87">VI. Dei
                  Comizj.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#88">VII. Del
                  Censo.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#89">VIII. De i
                  Magistrati, e primitiva de' Consoli.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#90">IX. De
                  Pretori.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#91">X.
                  Degl'Edili.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#92">XI. De i Tribuni
                  della Plebe.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#93">XII. Dei
                  Questori.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#93">XIII. Degl'altri
                  Magistrati minori ordinarj.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#94">XIV. De i
                  Magistrati minori straordinarj</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503505as.html#95">XI. Delle Monete.</a>
            </li>
            <li>
              <a href="dg4503505as.html#97">XII. Delle cose
              pertinenti alla Guerra.</a>
              <ul>
                <li>
                  <a href="dg4503505as.html#97">I. Di quelli, che
                  si eleggevano per la Milizia.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#99">II. Degl'altri
                  generi di Soldati.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#99">III. Dell'Ordine
                  de' Soldati.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#100">IV. Dell'ordine
                  de Capitani.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#102">V. De' Capitani,
                  che presedevano [sic] a tutto l'Esercito.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#103">VI. Delle
                  Armi.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#104">VII. Delle
                  Machine.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#105">IV [sic]. Delle
                  Insegne.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#106">IX. Della forma
                  ordinaria dell'Esercito.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#107">X. Di quelle
                  cose, che precedevano la Battaglia.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#108">XI. Degli
                  Steccati, e primieramente della loro parte
                  superiore.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#109">XII. Della parte
                  Inferiore.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#109">XIII. Delle
                  altre parti dell'Armata.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#110">XIV. Dell'Offizj
                  Militari.</a>
                </li>
                <li>
                  <a href="dg4503505as.html#113">XV.
                  Degl'Esercizj Militari.</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503505as.html#115">XIII. De i Premj, e
              delle Pene, e del Trionfo de i Soldati Romani.</a>
            </li>
            <li>
              <a href="dg4503505as.html#122">XIV. Delle
              Abitazioni, e fabbriche de' Romani.</a>
            </li>
            <li>
              <a href="dg4503505as.html#126">XV. De' Teatri, e
              Anfiteatri.</a>
            </li>
            <li>
              <a href="dg4503505as.html#131">›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dg4503505as.html#135">[Palazzi imperiali
              sul ›Palatino‹] ▣</a>
            </li>
            <li>
              <a href="dg4503505as.html#136">XVI. Del Palazzo
              degl'Imperadori.</a>
            </li>
            <li>
              <a href="dg4503505as.html#139">XVII. Del Circo.</a>
            </li>
            <li>
              <a href="dg4503505as.html#141">Theatri Marcelli
              ›Teatro di Marcello‹ vestigia ▣</a>
            </li>
            <li>
              <a href="dg4503505as.html#149">XVIII. De' Ninfei, e
              Portici.</a>
            </li>
            <li>
              <a href="dg4503505as.html#151">XIX. Delle
              Terme.</a>
            </li>
            <li>
              <a href="dg4503505as.html#157">Tempio di tutti li
              Dei oggi detto la Rotonda ›Pantheon‹ ▣</a>
            </li>
            <li>
              <a href="dg4503505as.html#165">XX. Dell'Acque,
              Acquedotti, e Cloache</a>
            </li>
            <li>
              <a href="dg4503505as.html#175">XXI.
              Degl'Obelischi.</a>
            </li>
            <li>
              <a href="dg4503505as.html#182">XXII. Distruzioue
              [sic: per Distruzione] dell'Antica Roma.</a>
            </li>
            <li>
              <a href="dg4503505as.html#188">XXIII. Del Vitto
              degl'Antichi Romani.</a>
            </li>
            <li>
              <a href="dg4503505as.html#196">XXIV. Delle vesti
              degl'Antichi Romani.</a>
            </li>
            <li>
              <a href="dg4503505as.html#203">XXV. Costumi de'
              Romani in Generale.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503505as.html#212">II. In questa nuova
          edizione corretta, ed accresciuta.</a>
          <ul>
            <li>
              <a href="dg4503505as.html#212">I. ›Porta del
              Popolo‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#240">II. ›Porta
              Pinciana‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#243">III. ›Porta
              Salaria‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#249">IV. ›Porta Pia‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#261">V. Porta di San
              Lorenzo. ›Porta Tiburtina‹</a>
            </li>
            <li>
              <a href="dg4503505as.html#277">VI. ›Porta
              Maggiore‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#292">VII. ›Porta San
              Giovanni‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#311">VIII. ›Porta
              Latina‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#321">IX. ›Porta San
              Sebastiano‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#342">X. ›Porta San
              Paolo‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#355">XI. Porta Portese.
              ›Porta Portuensis‹</a>
            </li>
            <li>
              <a href="dg4503505as.html#361">XII. ›Porta San
              Pancrazio‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#365">XIII. ›Porta
              Cavalleggeri‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#374">XIV. ›Porta
              Angelica‹.</a>
            </li>
            <li>
              <a href="dg4503505as.html#377">XV. Descrizione
              delle misure dell'Agro Romano, ed altre misure.</a>
            </li>
            <li>
              <a href="dg4503505as.html#379">XVI. Osservazioni
              sopra l'Agro Romano, e sopra la Coltivazione del
              medesimo.</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503505as.html#393">Catalogo per alfabeto delle
      tenute dell'Agro Romano Con i nomi de moderni Possessori
      [...] sino al presente Anno MDCCL</a>
    </li>
    <li>
      <a href="dg4503505as.html#423">Indice Delle materie di
      quest'Opera</a>
    </li>
  </ul>
  <hr />
</body>
</html>
