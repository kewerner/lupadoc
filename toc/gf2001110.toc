<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="gf2001110s.html#6">M. Vitruvius per Iocundum
      Castigatior factus cum figuris et tabula ut iam legi et
      intelligi possit</a>
      <ul>
        <li>
          <a href="gf2001110s.html#8">Beatissimo Iulio ii
          Pontefici Maximo</a>
        </li>
        <li>
          <a href="gf2001110s.html#10">[Index librorum]</a>
        </li>
        <li>
          <a href="gf2001110s.html#14">Liber primus</a>
        </li>
        <li>
          <a href="gf2001110s.html#37">Liber secundus</a>
        </li>
        <li>
          <a href="gf2001110s.html#54">Liber tertius</a>
        </li>
        <li>
          <a href="gf2001110s.html#76">Liber quartus</a>
        </li>
        <li>
          <a href="gf2001110s.html#101">Liber quintus</a>
        </li>
        <li>
          <a href="gf2001110s.html#126">Liber sextus</a>
        </li>
        <li>
          <a href="gf2001110s.html#148">Liber septimus</a>
        </li>
        <li>
          <a href="gf2001110s.html#162">Liber octavus</a>
        </li>
        <li>
          <a href="gf2001110s.html#178">Liber nonus</a>
        </li>
        <li>
          <a href="gf2001110s.html#200">Liber decimus</a>
        </li>
        <li>
          <a href="gf2001110s.html#232">Liber decimus explicit</a>
        </li>
        <li>
          <a href="gf2001110s.html#234">[Index rerum
          memorabilium]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
