<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501510s.html#6">Georgii Fabricii Chemnicensis
      Roma</a>
      <ul>
        <li>
          <a href="dg4501510s.html#8">Georgius Fabricius Volfgango
          Uvertero, Viro claro et docto</a>
        </li>
        <li>
          <a href="dg4501510s.html#17">Capita huius libri</a>
        </li>
        <li>
          <a href="dg4501510s.html#19">Auctores veteres et novi,
          quorum inventis et testimonijs vel ad confirmandum, vel
          ad explicandum utimur</a>
        </li>
        <li>
          <a href="dg4501510s.html#22">&#160;Georgii Fabricii
          Roma</a>
          <ul>
            <li>
              <a href="dg4501510s.html#22">I. De auctoribus, qui
              de Roma scripserunt&#160;</a>
            </li>
            <li>
              <a href="dg4501510s.html#25">II. De situ, et ambitu,
              et gloria atcque interitur urbis</a>
            </li>
            <li>
              <a href="dg4501510s.html#30">III. De montibus, a
              quibus urbi additi, quae loca adiacentia habeant,
              quot nomini. et unde appellati</a>
            </li>
            <li>
              <a href="dg4501510s.html#39">III. De portis veteris
              novaeque urbis, et quam varijs nominibus aedem
              appellatae</a>
            </li>
            <li>
              <a href="dg4501510s.html#45">V. De viis intra et
              extra urbem</a>
            </li>
            <li>
              <a href="dg4501510s.html#60">VI. De regionibus et
              tribubus</a>
            </li>
            <li>
              <a href="dg4501510s.html#67">VII. De Foris, et
              hominum necessitatem factis</a>
            </li>
            <li>
              <a href="dg4501510s.html#74">VIII. De campis</a>
            </li>
            <li>
              <a href="dg4501510s.html#78">IX. De templis
              gentilium, in templa Divorum mutatis</a>
            </li>
            <li>
              <a href="dg4501510s.html#100">X. De templis
              sacellisque veteribus, quae ab alijs omissa sunt</a>
            </li>
            <li>
              <a href="dg4501510s.html#111">XI. De domibus
              quorundam doctorum</a>
            </li>
            <li>
              <a href="dg4501510s.html#115">XII. De theatris,
              circis, odeis, stadio, xysto, item de factionibus
              aurigantium</a>
            </li>
            <li>
              <a href="dg4501510s.html#127">XIII. De
              porticibus</a>
            </li>
            <li>
              <a href="dg4501510s.html#140">XIIII. De arcubus
              triumphalibus, et trophaeis</a>
            </li>
            <li>
              <a href="dg4501510s.html#145">XV. De tiberi, et
              fluviis urbis, et de ornamentis fontium, ac
              naumachijs</a>
            </li>
            <li>
              <a href="dg4501510s.html#152">XVI. De pontibus
              tiberinis</a>
            </li>
            <li>
              <a href="dg4501510s.html#156">XVII. De
              aquaeductibus, quorum vestigia nunc sciuntur
              extare</a>
            </li>
            <li>
              <a href="dg4501510s.html#162">XVIII. De balneis et
              nympheis</a>
            </li>
            <li>
              <a href="dg4501510s.html#168">XIX. De thermis</a>
            </li>
            <li>
              <a href="dg4501510s.html#174">XX. De locis
              sepulchrorum, et de insignibus quibusdam sepulchris:
              item de sepulchrorum ornamentis, ac ratione
              conditarum urnarum&#160;</a>
            </li>
            <li>
              <a href="dg4501510s.html#185">XXI. De iis, quae nunc
              in urbe sunt praecipue visenda&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501510s.html#198">Liber unus, quo haec
          contenitur: Iter Romanum primum, [...]&#160;</a>
          <ul>
            <li>
              <a href="dg4501510s.html#200">[Epistola]</a>
            </li>
            <li>
              <a href="dg4501510s.html#208">Iter Romanum
              primum</a>
            </li>
            <li>
              <a href="dg4501510s.html#215">Iter Neapolitanum</a>
            </li>
            <li>
              <a href="dg4501510s.html#221">Iter Romanum
              secundum</a>
            </li>
            <li>
              <a href="dg4501510s.html#234">Iter Patavinum</a>
            </li>
            <li>
              <a href="dg4501510s.html#246">Iter Chemnicense</a>
            </li>
            <li>
              <a href="dg4501510s.html#259">Iter
              Argentoratense</a>
            </li>
            <li>
              <a href="dg4501510s.html#268">Locorum veteres et
              recentes appellationes&#160;</a>
            </li>
            <li>
              <a href="dg4501510s.html#288">Christophoro
              Leuschnero on Italiam eunti</a>
            </li>
            <li>
              <a href="dg4501510s.html#291">Index rerum
              memorabilium, quae in locorum appellationibus poni
              non potuerunt</a>
            </li>
            <li>
              <a href="dg4501510s.html#304">Index in Georg.
              Fabrcij Romam</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
