<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katpmon135551001s.html#14">The Mond
      collection</a>
      <ul>
        <li>
          <a href="katpmon135551001s.html#16">Preface</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#20">Contents</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#24">List of
          illustrations</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#28">Introduction</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#78">[Text]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="katpmon135551001s.html#78">The venetian
      school</a>
      <ul>
        <li>
          <a href="katpmon135551001s.html#80">Michele Giambono,
          Saint Mark</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#82">Saint Mark
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#92">Plate A ▣</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#92">Giambono,
              Virgin and Child</a>
            </li>
            <li>
              <a href="katpmon135551001s.html#92">Giambono,
              Saint Michael</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#94">Giovanni Bellini,
          Madonna and Child</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#96">Madonna and
              Child ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#102">Plate B ▣</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#102">Bellini,
              Virgin and Child</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#105">Giovanni Bellini;
          Pietà</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#116">Plate C ▣</a>
              <ul>
                <li>
                  <a href="katpmon135551001s.html#116">Bellini,
                  Pietà</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551001s.html#118">Plate D ▣</a>
              <ul>
                <li>
                  <a href="katpmon135551001s.html#118">Bellini,
                  Pietà</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551001s.html#120">Plate E ▣</a>
              <ul>
                <li>
                  <a href="katpmon135551001s.html#120">Bellini,
                  Pietà</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#126">Giovanni Bellini,
          The Child receives the apple from the Virgin</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#136">Gentile Bellini,
          Enthroned Virgin and Child</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#144">Cima, Saint James
          and Saint Sebastian</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#152">Plate F ▣</a>
              <ul>
                <li>
                  <a href="katpmon135551001s.html#152">Cima,
                  Annunciation</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#155">Catena Virgin and
          Child with Saints and Donors</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#162">Bissolo, Madonna
          and Child with two Saints</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#166">Plate G ▣</a>
              <ul>
                <li>
                  <a href="katpmon135551001s.html#166">Bissolo,
                  Madonna and Child with Saints</a>
                </li>
                <li>
                  <a href="katpmon135551001s.html#166">Crivelli,
                  Saint George</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#169">Carlo Crivelli,
          Saint Peter and Saint Paul</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#172">Saint Paul
              and Saint Peter ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#176">Palma, Portrait
          of a Lady</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#186">Titian, Madonna
          and Child</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#178">Plate H ▣</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#178">Titian,
              Flora</a>
            </li>
            <li>
              <a href="katpmon135551001s.html#178">Titian,
              Madonna and Child with Saint John And Saint
              Catherine</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#194">Tintoretto,
          venetian Gunboats</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#198">After Titian,
          Portrait of a Lady and her Son</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#196">Portait of a
              Lady and her Son ▣</a>
            </li>
            <li>
              <a href="katpmon135551001s.html#220">Plate J ▣</a>
              <ul>
                <li>
                  <a href="katpmon135551001s.html#220">Portraits
                  of Isabella d'Este</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#223">Venetian School,
          Portrait of a Man</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#224">Polidoro da
          Lanzano, Holy Family with infants Saint John and
          Donor</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#231">Giuseppe Porta,
          Justice</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#239">Venetian School,
          Portrait of a Man</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#240">Francesco
          Vecellio, Two Portrait-heads of Venetian Patricians</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#241">Parrasio, Death
          of Lucretia</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#242">Lucretia
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#247">Venetian School,
          Portrait of Giovanni Gritti</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#250">Venetian School,
          The finding of Moses</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#254">Gregorio
          Lazarini, Portrait of Antonio Correr</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#256">Portrait of
              Antonio Corraro ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#261">Sebastiano Ricci,
          Rape of Briseis</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#268">Diziani, Flight
          into Egypt: Riposo</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#272">Alessandro
          Longhi, Portrait of the Wife of Temanza</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#278">School of Tiepolo
          unkown subject</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#282">Luca Carlevaris,
          The Piazzetta di San Marco, Venice</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#280">Piazzetta di
              San Marco, Venice ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#289">Antonio Canale,
          Fair on the Piazza San Marco, Venice</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#290">Fair on the
              Piazza San Marco, Venice ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#295">School of Canale,
          View in Venice</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#305">Guardi, A papal
          recption in Venice</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#306">A Papal
              Reception in Venice</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="katpmon135551001s.html#316">Mantegna, the
      Veronese School and Montagna</a>
      <ul>
        <li>
          <a href="katpmon135551001s.html#318">Mantegna,
          Imperator Mundi</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#328">Mantegna, Virgin
          and child with Saints ▣</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#330">Mantegna,
          Imperator Mundi ▣</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#330">Adoration of the
          Virgin-Birth ▣</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#335">Girolamo dai
          Libri</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#342">Saint John
              and Saint Peter ▣</a>
            </li>
            <li>
              <a href="katpmon135551001s.html#342">Christ
              crucified, with Saint John and the Virgin ▣</a>
            </li>
            <li>
              <a href="katpmon135551001s.html#347">Adoration of
              the Inafnt Christ</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#354">Francesco
          Carotto</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#356">Madonna and
              Child with Saint John ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#364">Francesco
          Torbido</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#360">Portrait of
              Fracastoro ▣</a>
            </li>
            <li>
              <a href="katpmon135551001s.html#360">Virgin and
              Child with Saint John ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#376">Paolo
          Farinato</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#378">Saint John
              the Baptist ▣</a>
            </li>
            <li>
              <a href="katpmon135551001s.html#384">Saint Michel
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551001s.html#387">Battista
          Zelotto</a>
        </li>
        <li>
          <a href="katpmon135551001s.html#392">Bartolommeo
          Montagna</a>
          <ul>
            <li>
              <a href="katpmon135551001s.html#394">Virgin and
              Child with Donor ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
