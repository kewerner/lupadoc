<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghest21593550s.html#6">Dialogues sur les arts,
      entre un artiste amériquain et un amateur françois</a>
      <ul>
        <li>
          <a href="ghest21593550s.html#8">Preface</a>
        </li>
        <li>
          <a href="ghest21593550s.html#12">Table premier
          dialogue</a>
        </li>
        <li>
          <a href="ghest21593550s.html#14">Dialogues sur les
          arts</a>
          <ul>
            <li>
              <a href="ghest21593550s.html#14">Premier
              dialogue</a>
            </li>
            <li>
              <a href="ghest21593550s.html#75">Second
              dialogue</a>
            </li>
            <li>
              <a href="ghest21593550s.html#116">Trosieme
              dialogue&#160;</a>
            </li>
            <li>
              <a href="ghest21593550s.html#183">Quatrieme
              dialogue&#160;</a>
            </li>
            <li>
              <a href="ghest21593550s.html#214">Discours dur les
              obsatcles que les Artistes ont à surmonter pour
              produire de beaux Ouvrages</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
