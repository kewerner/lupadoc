<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efan293350s.html#4">Solenni Esequie di Maria
      Clementina Sobieski Regina dell'Inghilterra celebrate nella
      chiesa di San Paterniano in Fano</a>
      <ul>
        <li>
          <a href="efan293350s.html#6">Ragguaglio</a>
        </li>
        <li>
          <a href="efan293350s.html#9">Mariae Clementinae Jacobi
          Ludovici Subieski Filiae Joannis Poloniae Regis</a>
        </li>
        <li>
          <a href="efan293350s.html#10">[San Paterniano a Fano
          ▣]</a>
        </li>
        <li>
          <a href="efan293350s.html#12">Jacobus Beni Episcopus
          Fanensis Civium onmium frequentia, &amp; lacrymis justa
          persolvit</a>
        </li>
        <li>
          <a href="efan293350s.html#21">[Monumento per Maria
          Clementina Sobieski ▣]</a>
        </li>
        <li>
          <a href="efan293350s.html#28">Alla Santita di N.S. Papa
          Clemente XII</a>
        </li>
        <li>
          <a href="efan293350s.html#30">Delle Lodi di Maria
          Clementina Sobieski Regina d'Inghilterra Orazione</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
