<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katpnel33255030s.html#6">Klassisch-antike
      Goldschmiedearbeiten im Besitze Sr. Excellenz A. J. von
      Nelidow</a>
      <ul>
        <li>
          <a href="katpnel33255030s.html#8">Préface</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#10">Einleitung</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#16">Inhalt</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#18">I. Kraenze&#160;</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#21">II. Diademe</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#30">III. Nadeln</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#35">IV.
          Lockenhalter&#160;</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#36">V. Todtenmaske</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#37">VI. Ohrringe</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#124">VII. A)
          Colliers</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#151">VII. B)
          Brustschmuck&#160;</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#152">VIII.
          Armbaender</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#157">IX. Ringe</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#183">X. A) Fibel</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#184">X. B) Agraffe</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#185">XI.
          Gewandschmuck</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#192">XII. Statuetten</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#196">XIII. Vasen</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#198">XIV.
          Weberraedchen</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#199">XV. Anhaengsel</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#202">XVI. Amulete</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#203">XVII. Appliquen</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#212">XVIII. Varia</a>
        </li>
        <li>
          <a href="katpnel33255030s.html#216">[Tafeln]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
