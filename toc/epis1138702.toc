<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="epis1138702s.html#6">Pisa illustrata nelle arti
      del disegno; tomo secondo&#160;</a>
      <ul>
        <li>
          <a href="epis1138702s.html#8">Prefazione</a>
        </li>
        <li>
          <a href="epis1138702s.html#16">Indice de' titoli di
          qeusto secondo tomo</a>
        </li>
        <li>
          <a href="epis1138702s.html#22">Pisa illustrata nelle
          arti del disegno</a>
          <ul>
            <li>
              <a href="epis1138702s.html#22">Parte I. Storia
              dell'arte antica de' Pisani</a>
              <ul>
                <li>
                  <a href="epis1138702s.html#22">I. Idea dello
                  stato della Scultura nelle diverse sue
                  epoche&#160;</a>
                </li>
                <li>
                  <a href="epis1138702s.html#46">II. L'arte
                  pisana nel XI. e XII. secolo</a>
                </li>
                <li>
                  <a href="epis1138702s.html#115">III. La
                  pittura nei primi tre secoli dopo il Mille</a>
                </li>
                <li>
                  <a href="epis1138702s.html#189">IV. La
                  scultura fiorente nel secolo XIV.</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="epis1138702s.html#264">Parte II. Storia
              de' Pisani artefici dopo i secoli bassi a tutto il
              secolo XVII.</a>
              <ul>
                <li>
                  <a href="epis1138702s.html#264">I. Artefici
                  del secolo XV.</a>
                </li>
                <li>
                  <a href="epis1138702s.html#270">II. Pisani
                  artefici del secolo XVI.</a>
                </li>
                <li>
                  <a href="epis1138702s.html#313">III. Artefici
                  del secolo XVII.</a>
                </li>
                <li>
                  <a href="epis1138702s.html#331">IV. Brevi
                  notizie intorno al secolo XVIII.</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="epis1138702s.html#346">Appendice ai
              paragrafi della parte I.</a>
            </li>
            <li>
              <a href="epis1138702s.html#351">Ordine alfabetico
              dei professori del disegno</a>
            </li>
            <li>
              <a href="epis1138702s.html#360">&#160;[Tavole]</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
