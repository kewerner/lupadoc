<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501441s.html#6">Le Cose Maravigliose Della Città
      Di Roma</a>
      <ul>
        <li>
          <a href="dg4501441s.html#19">Massentio non imperatore ma
          più presto tiranno Romano e reputato&#160;</a>
        </li>
        <li>
          <a href="dg4501441s.html#25">Indulgentie delle sette
          Chiese principali de Roma&#160;</a>
        </li>
        <li>
          <a href="dg4501441s.html#34">Delle induglentie et
          reliquie dell'altre Chiese di Roma</a>
        </li>
        <li>
          <a href="dg4501441s.html#61">Summi Pontefici</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
