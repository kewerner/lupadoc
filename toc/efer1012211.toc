<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efer1012211s.html#6">Compendio historico
      dell’origine, accrescimento, e prerogative delle chiese, e
      luoghi pij della città, e diocesi di Ferrara, e delle memorie
      di que’ personaggi di pregio, che in esse son sepelliti
      [...]</a>
      <ul>
        <li>
          <a href="efer1012211s.html#8">Alli santissimi martiri
          Giorgio, e Maurelio</a>
        </li>
        <li>
          <a href="efer1012211s.html#11">Al benigno
          Lettore&#160;</a>
        </li>
        <li>
          <a href="efer1012211s.html#14">Libro primo</a>
        </li>
        <li>
          <a href="efer1012211s.html#52">Libro secondo</a>
        </li>
        <li>
          <a href="efer1012211s.html#86">Libro terzo</a>
        </li>
        <li>
          <a href="efer1012211s.html#208">Libro quarto</a>
        </li>
        <li>
          <a href="efer1012211s.html#314">Libro quinto</a>
        </li>
        <li>
          <a href="efer1012211s.html#398">Libro sesto</a>
        </li>
        <li>
          <a href="efer1012211s.html#490">Tavola delle chiese,
          oratori, spedali, [...]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
