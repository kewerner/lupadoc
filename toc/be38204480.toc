<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="be38204480s.html#8">L’ archeologo nell’Abruzzo
      Ulteriore Secondo ovvero Prospetto storico intorno i
      monumenti antichi e moderni, le vicende civili e religiose,
      le scienze, le lettere e le arti belle della Provincia e
      città di Aquila</a>
      <ul>
        <li>
          <a href="be38204480s.html#12">La provincia e città di
          Aquila</a>
          <ul>
            <li>
              <a href="be38204480s.html#12">I. I primi popoli che
              stanziarono nell'Abbruzzo ulteriore secondo</a>
            </li>
            <li>
              <a href="be38204480s.html#20">II. Impressione e
              rimembranze di alcune città nella provincia di
              Aquila</a>
            </li>
            <li>
              <a href="be38204480s.html#70">III. Avanzi di
              monumenti antichi e fatti memorandi</a>
            </li>
            <li>
              <a href="be38204480s.html#84">IV. Origine e
              progressi della città di Aquila</a>
            </li>
            <li>
              <a href="be38204480s.html#133">V. Le
              disavventure</a>
            </li>
            <li>
              <a href="be38204480s.html#157">VI. I valorosi nelle
              armi e nelle cariche eminenti</a>
            </li>
            <li>
              <a href="be38204480s.html#165">VII. Gli
              scienziati</a>
            </li>
            <li>
              <a href="be38204480s.html#188">VIII. Gli artisti</a>
            </li>
            <li>
              <a href="be38204480s.html#202">IX. Le chiese o le
              viventi glorie di Aquila</a>
            </li>
            <li>
              <a href="be38204480s.html#226">X. Una visita alla
              chiesa di S. Bernardino</a>
            </li>
            <li>
              <a href="be38204480s.html#237">XI. Basilica di S.
              Maria di Collemaggio</a>
            </li>
            <li>
              <a href="be38204480s.html#249">XII. Panorama
              artistico</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be38204480s.html#264">Indice de' capitoli
          contenuti in questo volume</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
