<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="bs12603440s.html#6">Numismata Romanorum Pontificum
      Praestantiora A Martino V. Ad Benedictum XIV</a>
      <ul>
        <li>
          <a href="bs12603440s.html#8">Eminentissimo, ac
          Reverendissimo Principi Silvio Cardinali Valenti</a>
        </li>
        <li>
          <a href="bs12603440s.html#12">Praefatio</a>
        </li>
        <li>
          <a href="bs12603440s.html#42">Series Numismatum
          Pontificiorum</a>
        </li>
        <li>
          <a href="bs12603440s.html#410">Catalogus Monetarum, et
          Emblematum</a>
        </li>
        <li>
          <a href="bs12603440s.html#414">Index rerum
          notabilium&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
