<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghved66752620s.html#6">Raccolta de pittori,
      scultori, et architetti modonesi più celebri, Nella quale si
      leggono l'Opere loro insigni, e dove l'hanno fatte</a>
      <ul>
        <li>
          <a href="ghved66752620s.html#8">L'autore alla virtuosa
          Accademia de' Pittori Modonesi</a>
        </li>
        <li>
          <a href="ghved66752620s.html#10">A' benigni lettori</a>
        </li>
        <li>
          <a href="ghved66752620s.html#18">De gli Architetti, i
          quali hanno fabricato la Torre di Modona</a>
        </li>
        <li>
          <a href="ghved66752620s.html#24">LIS Bononiae,
          Cremonae, ac Mutinae ad Palladis puteal</a>
        </li>
        <li>
          <a href="ghved66752620s.html#25">Di Lanfranco Facci,
          overo Romengardi Architetto Modonese, e Clarte Viligelmo
          Scoltore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#32">Di Serafino Serafini
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#33">Di Tomaso Bassini
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#33">Christofano da Modona
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#34">Di quattro belle
          Ancone, che sono nella Chiese di S. Domenico di
          Modona</a>
        </li>
        <li>
          <a href="ghved66752620s.html#35">Di Francesco Maria
          Castaldi</a>
        </li>
        <li>
          <a href="ghved66752620s.html#36">Di Christofaro,
          Lorenzo, e Bernardino de'Lendenari Mastri di Tarsia</a>
        </li>
        <li>
          <a href="ghved66752620s.html#37">Di Bartolomeo Bonasia
          Maestro anch'esso di Tarsia</a>
        </li>
        <li>
          <a href="ghved66752620s.html#37">Di Guido Mazzoni,
          overo Paganini Scoltore famosissimo</a>
        </li>
        <li>
          <a href="ghved66752620s.html#44">Di trè Donne Scultrici
          Modonesi, e prima d'Isabella Discalzi</a>
        </li>
        <li>
          <a href="ghved66752620s.html#45">La seconda Scultrice
          figliuola di Guido</a>
        </li>
        <li>
          <a href="ghved66752620s.html#46">Di Propertia figliuola
          di Giovan Martino Rossi da Modona</a>
        </li>
        <li>
          <a href="ghved66752620s.html#49">Di Pellegrino Aretusi
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#49">Di Francesco Magagnolo
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#50">Di Francesco Bianchi,
          alias Frari</a>
        </li>
        <li>
          <a href="ghved66752620s.html#52">Di Giovanni Munari</a>
        </li>
        <li>
          <a href="ghved66752620s.html#52">Di Pellegrino Munari
          Pittore eccellente</a>
        </li>
        <li>
          <a href="ghved66752620s.html#55">Di Nicoletto da Modona
          Pittore &amp; Incisore in rame</a>
        </li>
        <li>
          <a href="ghved66752620s.html#56">Di Gio: Battista
          Porto</a>
        </li>
        <li>
          <a href="ghved66752620s.html#57">Di Nicolò
          Cavalerino</a>
        </li>
        <li>
          <a href="ghved66752620s.html#57">Antonio figliuolo di
          Giuliano Begarelli</a>
        </li>
        <li>
          <a href="ghved66752620s.html#64">Di Lodovico
          Begarelli</a>
        </li>
        <li>
          <a href="ghved66752620s.html#65">Di Cecchino Setti
          Pittori</a>
        </li>
        <li>
          <a href="ghved66752620s.html#65">Di Gasparo figliuolo
          di Silvestro Pagani Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#66">Di Giacopino
          Lancilotto</a>
        </li>
        <li>
          <a href="ghved66752620s.html#67">Di cinque Pittori
          insigni</a>
        </li>
        <li>
          <a href="ghved66752620s.html#69">Ugo da Carpi primo
          inventore delle Stampe di legno</a>
        </li>
        <li>
          <a href="ghved66752620s.html#70">Di Andrea Architetto,
          detto per sopra nome il Formigine</a>
        </li>
        <li>
          <a href="ghved66752620s.html#72">Di GIacomo
          Tagliapietra, e Paolo suo figliuolo, e figliuoli di
          questo</a>
        </li>
        <li>
          <a href="ghved66752620s.html#73">Di Ambrogio
          Tagliapietra Architetto, e Scultore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#73">Di Nicolò figliuolo di
          Giovanni Abbate Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#79">Pietro Paolo Abbate
          Fratello di Nicolò, Pittore anch'esso celeberrimo</a>
        </li>
        <li>
          <a href="ghved66752620s.html#80">Alberto Fontana
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#81">Di Gio: Battista
          Modonese</a>
        </li>
        <li>
          <a href="ghved66752620s.html#82">Di Frà Giacomo
          Seghizzi Ingegniere</a>
        </li>
        <li>
          <a href="ghved66752620s.html#83">Vita di Giacomo
          Barozzi da Vignola Architetto, &amp; Prospettivo
          eccellentissimo</a>
        </li>
        <li>
          <a href="ghved66752620s.html#93">Di Francesco Capelli
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#94">D'Angelo da Modona
          Scrittore eccellentissimo</a>
        </li>
        <li>
          <a href="ghved66752620s.html#94">Di Girolamo Comi
          Pittore, e gran Maestro di Prospettiva</a>
        </li>
        <li>
          <a href="ghved66752620s.html#96">Di trè Fratelli
          Pittori, detti i Taraschi</a>
        </li>
        <li>
          <a href="ghved66752620s.html#97">Di Geminiano da
          Modona</a>
        </li>
        <li>
          <a href="ghved66752620s.html#97">Di Gio: Buonomi</a>
        </li>
        <li>
          <a href="ghved66752620s.html#99">Di Gio: Antonio
          Scacciera</a>
        </li>
        <li>
          <a href="ghved66752620s.html#100">Vita di Giovanni
          Guerra, Gasparo, e Gio: Battista Fratelli Pittori</a>
        </li>
        <li>
          <a href="ghved66752620s.html#103">D'alcuni altri
          eccellenti Architetti</a>
        </li>
        <li>
          <a href="ghved66752620s.html#103">Di Galasso Alghisi da
          Carpi Architetto</a>
        </li>
        <li>
          <a href="ghved66752620s.html#104">Di Gio: Abbate
          Scultore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#105">D'Ercole Setti
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#107">Di Francesco
          Madonnina Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#108">Di Pietro,
          Christofaro, e Gio: Tomaso Sudenti Fonditori di
          metallo</a>
        </li>
        <li>
          <a href="ghved66752620s.html#109">Del S. Oratio
          Ghirlinzoni</a>
        </li>
        <li>
          <a href="ghved66752620s.html#109">Di Paolo Emilio
          Carara Ingegniere</a>
        </li>
        <li>
          <a href="ghved66752620s.html#110">Di Domenico Carnevale
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#113">Di Girolamo
          Cavalerino</a>
        </li>
        <li>
          <a href="ghved66752620s.html#114">D'Hercole Abbate
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#117">Del secondo Pietro
          Paolo Abbate Pittore figliuolo d'Hercole</a>
        </li>
        <li>
          <a href="ghved66752620s.html#118">Del Sig. Gio.
          Battista Capodibue</a>
        </li>
        <li>
          <a href="ghved66752620s.html#119">Di Bartolomeo
          Schidoni Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#123">Di Gregorio Rossi
          Scoltore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#124">Di Stefano Gavassete
          Scultore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#124">Di Camillo Gavassete
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#126">Di Luigi Gavassete
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#127">Di Bernardo Cervi</a>
        </li>
        <li>
          <a href="ghved66752620s.html#128">Di Gio: Battista
          Ingoni Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#129">Di Giulio Secchiari
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#130">DI Paolo, Camillo,
          &amp; Andrea Bisogni</a>
        </li>
        <li>
          <a href="ghved66752620s.html#131">Di Girolamo
          Corridore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#132">Di Giacomo Cavedoni
          da Sassuolo</a>
        </li>
        <li>
          <a href="ghved66752620s.html#133">Di Leonardo Ricchetti
          Architetto</a>
        </li>
        <li>
          <a href="ghved66752620s.html#134">Di Marco Meloni
          Scultore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#135">Di Alessandro Aretusi
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#135">Del Sig. Donino
          Ingoni</a>
        </li>
        <li>
          <a href="ghved66752620s.html#136">Di Lodovico Bertucci
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#138">Di Francesco Modonino
          Architetto</a>
        </li>
        <li>
          <a href="ghved66752620s.html#139">Del Capitano Frate
          Architetto</a>
        </li>
        <li>
          <a href="ghved66752620s.html#139">Di Bernardino
          Cassani</a>
        </li>
        <li>
          <a href="ghved66752620s.html#140">Di Gasparo Baldovini
          Architetto</a>
        </li>
        <li>
          <a href="ghved66752620s.html#140">Di Rafaello Menia
          Architetto</a>
        </li>
        <li>
          <a href="ghved66752620s.html#141">Di Alessandro
          Seraglia Scultore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#142">Di Paolo
          Sevatico&#160;</a>
        </li>
        <li>
          <a href="ghved66752620s.html#144">Del Sig. Gio: Antonio
          Carandini Scultore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#144">Del Signor Lodovico
          Lana Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#146">Di Gio: Battista
          Pesari Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#147">Del Signor Gio:
          Battista Levizani Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#148">Di Alessandro Bagni
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#148">Di Gio: Battista
          Modonino Pittore&#160;</a>
        </li>
        <li>
          <a href="ghved66752620s.html#150">D'Hercole Mani
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#150">Del Signor Giosesso
          Zarlati</a>
        </li>
        <li>
          <a href="ghved66752620s.html#151">D'Annibal Passari
          Pittore</a>
        </li>
        <li>
          <a href="ghved66752620s.html#152">Del Signor Francesco
          Manzuoli Pittore&#160;</a>
        </li>
        <li>
          <a href="ghved66752620s.html#153">Di Giacomo Chiavena
          Orefice eccellente</a>
        </li>
        <li>
          <a href="ghved66752620s.html#154">Del Sig. Gio:
          Battista Spaccini</a>
        </li>
        <li>
          <a href="ghved66752620s.html#155">Di Christofaro
          Galaverna Architetto</a>
        </li>
        <li>
          <a href="ghved66752620s.html#156">Di Geminiano
          Bortolomasi</a>
        </li>
        <li>
          <a href="ghved66752620s.html#156">Del Signor Marchese
          Tomaso Guidoni</a>
        </li>
        <li>
          <a href="ghved66752620s.html#157">Del Sig. Paolo
          Carandini</a>
        </li>
        <li>
          <a href="ghved66752620s.html#160">Tavola di tutti li
          Pittori, Scultori, &amp; Architetti Modonesi, che nel
          presente Libro si contengono</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
