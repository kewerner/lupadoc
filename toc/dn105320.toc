<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dn105320s.html#8">[Le sette basiliche di Roma]</a>
      <ul>
        <li>
          <a href="dn105320s.html#8">Basilica di ›San Pietro in
          Vaticano‹ ▣</a>
        </li>
        <li>
          <a href="dn105320s.html#10">›Santa Maria Maggiore‹ ▣</a>
        </li>
        <li>
          <a href="dn105320s.html#12">›San Giovanni in Laterano‹
          ▣</a>
        </li>
        <li>
          <a href="dn105320s.html#14">›San Lorenzo fuori le Mura‹
          ▣</a>
        </li>
        <li>
          <a href="dn105320s.html#16">›Santa Croce in Gerusalemme‹
          ▣</a>
        </li>
        <li>
          <a href="dn105320s.html#18">›San Paolo fuori le Mura‹
          ▣</a>
        </li>
        <li>
          <a href="dn105320s.html#20">›San Sebastiano‹ ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
