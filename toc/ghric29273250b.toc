<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghric29273250bs.html#10">The works of Jonathan
      Richardson</a>
    </li>
    <li>
      <a href="ghric29273250bs.html#12">To Sir Joshua Reynolds,
      Dedication</a>
    </li>
    <li>
      <a href="ghric29273250bs.html#14">Contents</a>
    </li>
    <li>
      <a href="ghric29273250bs.html#16">The theory of
      painting</a>
    </li>
    <li>
      <a href="ghric29273250bs.html#122">The connoisseur: An
      essay on the whole art of criticism as it relates to
      painting.</a>
    </li>
    <li>
      <a href="ghric29273250bs.html#199">A discourse on the
      Dignity, Certainty, Pleasure, and Advantage of the science of
      a connoisseur.&#160;</a>
    </li>
    <li>
      <a href="ghric29273250bs.html#293">An essay on prints</a>
    </li>
    <li>
      <a href="ghric29273250bs.html#315">Introduction to the
      chronological list, &amp;c.&#160;</a>
    </li>
  </ul>
  <hr />
</body>
</html>
