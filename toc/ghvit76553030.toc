<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghvit76553030s.html#10">Osservazioni sopra il
      libro della ’Felsina pittrice’, per difesa di Raffaello da
      Urbino, dei Caracci, e della loro scuola</a>
      <ul>
        <li>
          <a href="ghvit76553030s.html#12">Al cortese lettore</a>
        </li>
        <li>
          <a href="ghvit76553030s.html#26">Lettera prima</a>
        </li>
        <li>
          <a href="ghvit76553030s.html#33">Lettera seconda</a>
        </li>
        <li>
          <a href="ghvit76553030s.html#54">Lettera terza</a>
        </li>
        <li>
          <a href="ghvit76553030s.html#76">Lettera quarta</a>
        </li>
        <li>
          <a href="ghvit76553030s.html#85">Lettera quinta</a>
        </li>
        <li>
          <a href="ghvit76553030s.html#108">Lettera sesta</a>
        </li>
        <li>
          <a href="ghvit76553030s.html#120">Lettera settima</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
