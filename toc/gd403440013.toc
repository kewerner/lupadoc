<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="gd403440013s.html#8">Nuova raccolta di lettere
      sulla pittura, scultura ed architettura scritte da’ più
      celebri personaggi dei secoli XV a XIX</a>
      <ul>
        <li>
          <a href="gd403440013s.html#8">Serie prima</a>
          <ul>
            <li>
              <a href="gd403440013s.html#10">Programma</a>
            </li>
            <li>
              <a href="gd403440013s.html#12">Prefazione</a>
            </li>
            <li>
              <a href="gd403440013s.html#14">Memorie originali
              di belle arti</a>
            </li>
            <li>
              <a href="gd403440013s.html#192">Appendice</a>
            </li>
            <li>
              <a href="gd403440013s.html#209">Indice</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gd403440013s.html#212">Serie seconda</a>
          <ul>
            <li>
              <a href="gd403440013s.html#214">Memorie originali
              di belle arti</a>
            </li>
            <li>
              <a href="gd403440013s.html#390">Appendice</a>
            </li>
            <li>
              <a href="gd403440013s.html#410">Indice</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gd403440013s.html#412">Indice delle serie I.
          e II.</a>
        </li>
        <li>
          <a href="gd403440013s.html#422">Serie terza</a>
          <ul>
            <li>
              <a href="gd403440013s.html#424">Memorie originali
              di belle arti</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gd403440013s.html#574">Appendici</a>
          <ul>
            <li>
              <a href="gd403440013s.html#574">Serie prima</a>
            </li>
            <li>
              <a href="gd403440013s.html#576">Serie seconda</a>
            </li>
            <li>
              <a href="gd403440013s.html#582">Serie terza</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="gd403440013s.html#598">Indice primo</a>
        </li>
        <li>
          <a href="gd403440013s.html#617">Agli amatori ed
          assiciati delle memorie originali di belle arti</a>
        </li>
        <li>
          <a href="gd403440013s.html#620">Indice delle memorie
          contenute nella serie terza</a>
        </li>
        <li>
          <a href="gd403440013s.html#622">Nuova raccolta di
          lettere sulla pittura scultura ed architettura</a>
          <ul>
            <li>
              <a href="gd403440013s.html#624">Manifesto</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
