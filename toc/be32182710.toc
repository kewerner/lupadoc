<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="be32182710s.html#8">Latium</a>
      <ul>
        <li>
          <a href="be32182710s.html#9">Joannes Paulus Oliva</a>
        </li>
        <li>
          <a href="be32182710s.html#10">[Dedicatio] Beatissimo
          Patri Clementi X. Pontifex Optimus Maximus</a>
        </li>
        <li>
          <a href="be32182710s.html#15">[Stampa] Nova et Exacta
          Chorographia Latii ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="be32182710s.html#18">Liber I. Chronologicus</a>
      <ul>
        <li>
          <a href="be32182710s.html#18">Praefatio</a>
        </li>
        <li>
          <a href="be32182710s.html#20">Pars I., De Colonijs
          Latii</a>
          <ul>
            <li>
              <a href="be32182710s.html#20">I. De primis Latij
              Colonis, eorumque continuata successione</a>
            </li>
            <li>
              <a href="be32182710s.html#21">II. De prima
              Janigenarium expeditione in Latium</a>
            </li>
            <li>
              <a href="be32182710s.html#24">III. De Aboriginibus
              Oenotrijs, Italis, Sicanis, Umbris, Pelasgis, Latij,
              Veteris Colonis</a>
            </li>
            <li>
              <a href="be32182710s.html#29">IV. De Secunda in
              Latium expeditione, quae Pelasgorum fuit</a>
            </li>
            <li>
              <a href="be32182710s.html#31">V. De Tertia Expedione
              Pelasgorum Arcadum in Latium sub Evandro Duce
              facta</a>
            </li>
            <li>
              <a href="be32182710s.html#34">VI. De Aequivocatione
              nominum, et Analogia gestarum rerum à primis post
              diluvium &#160;Janigenis, quae posteris posteà
              populorum Ducibus applicata fuerunt</a>
              <ul>
                <li>
                  <a href="be32182710s.html#36">Chronologia Regum
                  Latinorum, sive Albanorum, Romanorum,
                  Reipublicae, et Imperatorum</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#38">VII. De Terminis
              Prisci Latii</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="be32182710s.html#46">Liber II. De particularium
      locorum descriptione</a>
      <ul>
        <li>
          <a href="be32182710s.html#46">Praefatio</a>
        </li>
        <li>
          <a href="be32182710s.html#47">Pars I. Ager Latinus</a>
          <ul>
            <li>
              <a href="be32182710s.html#47">I. De locis
              memorabilibus Albanum inter et Romam in Via Appia
              occurrentibus</a>
              <ul>
                <li>
                  <a href="be32182710s.html#47">›Via Appia‹</a>
                </li>
                <li>
                  <a href="be32182710s.html#47">Hippodromus
                  Caracallae ›Circo di Massenzio‹</a>
                </li>
                <li>
                  <a href="be32182710s.html#47">Egeriae fons
                  ›Camenarum, Fons et Lucus‹</a>
                </li>
                <li>
                  <a href="be32182710s.html#47">›Fossa
                  Cluilia‹</a>
                </li>
                <li>
                  <a href="be32182710s.html#47">Monumentum
                  Horatiorum ubi situm fuit</a>
                </li>
                <li>
                  <a href="be32182710s.html#48">›Fossa Cluilia‹
                  ubi sita</a>
                </li>
                <li>
                  <a href="be32182710s.html#48">›Acquedotto
                  Claudio‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#49">II. De Origine,
              Antiquitate, et Situ Albae Longa</a>
              <ul>
                <li>
                  <a href="be32182710s.html#50">Veteris Albani
                  Lacus ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#54">III. De Monte Albano,
              eius situ, natura, proprietate</a>
            </li>
            <li>
              <a href="be32182710s.html#57">IV. Lacus Albanus,
              eiusque origo, et mirabilia</a>
              <ul>
                <li>
                  <a href="be32182710s.html#58">Delineatio
                  Crateris Lacus Albani ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#63">V. De Urbe hodiè
              Albano nuncupato, ejusque Territorio</a>
            </li>
            <li>
              <a href="be32182710s.html#66">VI. De Aricia, et
              Nemore, eiusque Territorio</a>
            </li>
            <li>
              <a href="be32182710s.html#68">VII. De Nemore
              Aricino, et Lacu Dianae</a>
              <ul>
                <li>
                  <a href="be32182710s.html#70">Situs Lacus
                  Nemorensis ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#74">Lacus Arcini
                  Prisca Constitutio ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#77">VIII. De Lanuvio,
              vulgò Cività de la vigna</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be32182710s.html#80">Pars II. Ager Tusculanus,
          et Circumjacentia loca</a>
          <ul>
            <li>
              <a href="be32182710s.html#80">I. De Veteri Tusco, et
              Montibus, Tusculanis</a>
              <ul>
                <li>
                  <a href="be32182710s.html#82">Tusci veteris
                  Situs et Constitutio ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#84">II. Crypta Ferrata,
              seu, Villa Ciceronis</a>
            </li>
            <li>
              <a href="be32182710s.html#91">III. De Sepulcro
              veterum Furiorum in monte Tusculano, non ita pridem
              detecto</a>
              <ul>
                <li>
                  <a href="be32182710s.html#92">Vera delinatio
                  veteris sepulchri Furiorum ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#96">Depositoria
                  cineraria, una cum inscriptionibus in Sepulchro
                  Furiorum reperta ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#99">IV. Algidus Mons, et
              oppidum; Roboraria, Corbio, Ad decimum Terentinum,
              caeteraque Tusculani Agri oppida</a>
              <ul>
                <li>
                  <a href="be32182710s.html#100">Tusculani
                  Territorii topographia ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#103">V. Tusculanum Lucii
              Luculli</a>
              <ul>
                <li>
                  <a href="be32182710s.html#104">Ichnographia
                  substructiorum, et habitaculorum Luclulli ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#105">Sub Villa
                  Varesiana Piscina sive Colyruphydra Lulliani sub
                  hac forma spectatur ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#106">Lucullanum in
                  agro Tusculano, Vulgo la Grotta di Lucullo, o il
                  Centrone ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#108">Ruinae Villarum
                  Lucullanarum passim inter Marinum et Tusculum in
                  Campis obviae ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#108">VI. De Tusculo
              posterorum temporum, quod Frascatium hodiè
              dicitur</a>
              <ul>
                <li>
                  <a href="be32182710s.html#111">Schematismus
                  villarum Tusculanarum ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#115">Burghesianum una
                  cum villis ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#119">VII. Marinum, sive
              Marianum aut Ferentium</a>
              <ul>
                <li>
                  <a href="be32182710s.html#120">Lapidis in Marino
                  Agro inventi, Apotheosin Homeri exprimentis,
                  Interpretatio ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#128">VIII. Ardeatinus
              Ager, aliorumque locorum adjacentium. Campus
              Solonius, Tellenae, Ficana</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be32182710s.html#131">Pars III. Praeneste, sive
          Ager Praenestinus</a>
          <ul>
            <li>
              <a href="be32182710s.html#131">I. De origine, et
              antiquitate, situque Praenestinae urbis, vulgò
              Palestrina</a>
            </li>
            <li>
              <a href="be32182710s.html#134">II. De Fanis et
              Delubris Veterum Praenestinorum</a>
            </li>
            <li>
              <a href="be32182710s.html#135">III. Praenestine
              Fortunae, Primigeniae, Templum</a>
              <ul>
                <li>
                  <a href="be32182710s.html#136">Descriptio Templi
                  Primigeniae Fortunae Praenestinae</a>
                </li>
                <li>
                  <a href="be32182710s.html#137">Fragmentari
                  Musiva arte concinnatum, quod tamen in nostro
                  Lit</a>
                </li>
                <li>
                  <a href="be32182710s.html#139">Orthographia
                  Templi Fortunae Praenestae ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#139">Sciographia
                  Templi Fortunae Praenestae ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#142">De variis
                  Fortunae Primigeniae symbolicis Schematismis</a>
                </li>
                <li>
                  <a href="be32182710s.html#144">[Fortunae
                  symbola] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#145">IV. De significatione
              Figurarum, quae in hoc Lithostrato spectantur</a>
              <ul>
                <li>
                  <a href="be32182710s.html#145">Interpretatio
                  Lithostroti Praenestini</a>
                </li>
                <li>
                  <a href="be32182710s.html#147">Monumenti
                  Vetustissimi in Praenestinis Primigeniae Fortunae
                  ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#150">De fine Veterum
                  in exstruendo Fortunae Templo</a>
                </li>
                <li>
                  <a href="be32182710s.html#161">Praeneste Urbis
                  Latii Vestustissimae exactissima descriptio ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#164">V. De posteriorum
              temporum Praenestinae Urbis Statu</a>
            </li>
            <li>
              <a href="be32182710s.html#165">VI. De Episcopis
              Praenestinis, ex Suaresio et Hughello</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be32182710s.html#172">Pars IIII. Regio
          Labicorum, Hernicorum, aliorumque in meditullio Latii
          oppidorum situs et origo</a>
          <ul>
            <li>
              <a href="be32182710s.html#172">I. Pedum, Gabij,
              Labici, caeteraque oppida</a>
            </li>
            <li>
              <a href="be32182710s.html#173">II. Labicum, Pedum,
              Scapsia, Pupinia, et cetera oppida veteris Latii</a>
            </li>
            <li>
              <a href="be32182710s.html#177">III. De Agro, seu
              Regione Hernicorum</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="be32182710s.html#181">Civitatis Tyburis Delineatio
      ▣</a>
    </li>
    <li>
      <a href="be32182710s.html#184">Liber III. De Antiquitate
      Tiburtinae Urbis, et summa veteris magnificentiae, quae in
      villarum ruderibus adhuc in toto Territorio deteguntur,
      varietate, et multitudine</a>
      <ul>
        <li>
          <a href="be32182710s.html#184">Praefatio</a>
        </li>
        <li>
          <a href="be32182710s.html#185">Pars I.</a>
          <ul>
            <li>
              <a href="be32182710s.html#185">I. De Originis Urbis
              Tyburtinae</a>
            </li>
            <li>
              <a href="be32182710s.html#186">II. De Catilli Domus
              origine</a>
            </li>
            <li>
              <a href="be32182710s.html#188">III. De Evandri
              colonia in Aborigenum Terram, quod Latium dicebatur,
              deducta, et inde per Catillum in Tyburtinum
              Collem</a>
            </li>
            <li>
              <a href="be32182710s.html#191">IV. De Aeneae in
              Latium adventu</a>
            </li>
            <li>
              <a href="be32182710s.html#193">V. De variis
              Revolutionibus Tyburtinae urbis sub Regibus et
              Republica Romana</a>
            </li>
            <li>
              <a href="be32182710s.html#194">VI. De Tyburtinorum
              cum Romanis confoederatione</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be32182710s.html#197">Pars II. De Tyburtine
          Urbis Antiquatibus, quae etiamnum in eius Ruderibus
          splendescunt</a>
          <ul>
            <li>
              <a href="be32182710s.html#197">I. De Novo et Prisco
              Tyburtinae urbis situ</a>
              <ul>
                <li>
                  <a href="be32182710s.html#198">Prospectus ex
                  ponte in Catarractam quam vulgo la Cascata vocant
                  ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#199">Prospectus ex
                  ponte un subiectam aquarum voraginem quam la
                  bocca dell'inferno vocant ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#201">Territorii
                  Tiburtini Veteris et Novi Descriptio ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#204">II. De Villis
              Tyburtinis, quae olim à Caesaribus, Regibus,
              Consulibus, Dictatoribus, et Poetis in deliciis
              habitae fuerunt</a>
              <ul>
                <li>
                  <a href="be32182710s.html#206">Totius Plani
                  Villae Adrianae declaratio generalis</a>
                </li>
                <li>
                  <a href="be32182710s.html#213">Villae
                  celeberrimae ab Adriano Caesare in agro Tiburtino
                  extraxta vera et exactissima ichonographia ab
                  Pyrho Ligorio olim postea ab Francisco Contini
                  recognita et descripta, jussit eminentissimi
                  Francisci Cardinali Barberini ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#216">III. De Celeberrima
              Villa Adriani Imperatoris in Agro Tyburtino</a>
              <ul>
                <li>
                  <a href="be32182710s.html#217">De divisione
                  Villae Adrianeae</a>
                </li>
                <li>
                  <a href="be32182710s.html#217">Partium
                  singularum descriptio</a>
                </li>
                <li>
                  <a href="be32182710s.html#219">De partibus
                  Villae vesrus plagam meridionalem</a>
                </li>
                <li>
                  <a href="be32182710s.html#221">De reliquis
                  partitionibus Villae, quae sunt Infernus, Campi
                  Elysi, Prytaneum</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#225">IV. Villa Syphacis
              Regis Numidiae</a>
              <ul>
                <li>
                  <a href="be32182710s.html#226">Villa Zenobiae
                  Plamyrae Reginae</a>
                </li>
                <li>
                  <a href="be32182710s.html#227">Villa Mecaenatis
                  poesteà Cesaris Augusti</a>
                </li>
                <li>
                  <a href="be32182710s.html#229">Villa Mecaenatis
                  Tiburtina Caesaris Augusti &#160;▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#235">Villa Quintily
                  vari propinqui Caesaris Augusti ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#238">Villa Quintilii
                  Varii Consulis</a>
                </li>
                <li>
                  <a href="be32182710s.html#239">De Ponte Lucano,
                  Plautiorum sepulchro et villa</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#240">V. De Villa
              Rubelliorum</a>
              <ul>
                <li>
                  <a href="be32182710s.html#241">Villa Ventidii
                  Bassi, C. Turpilii, et Munatii Planci</a>
                </li>
                <li>
                  <a href="be32182710s.html#242">Villa Caji
                  Cassii</a>
                </li>
                <li>
                  <a href="be32182710s.html#243">Villa Q. Caecilii
                  pii Metelli Scipionis</a>
                </li>
                <li>
                  <a href="be32182710s.html#243">Villa
                  Lolliorum</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#243">VI. De Villis
              Poeatarum, Catulli, Horatii, Virgilii, Vopisci,
              etc.</a>
            </li>
            <li>
              <a href="be32182710s.html#245">VII. Villa Manlii
              Vopisci</a>
              <ul>
                <li>
                  <a href="be32182710s.html#248">[stampa] ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#250">[stampa] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#251">VIII. De caeteris
              Villis Tyburtinis</a>
            </li>
            <li>
              <a href="be32182710s.html#253">IX. Villa Hippolyti
              Cardinalis d'Este, quae Tybure hoiderna die
              superstes, summa Advenarum admiratione visitur</a>
              <ul>
                <li>
                  <a href="be32182710s.html#254">Rudera Villarum
                  passim inter Oliveta tendentibus ad Villam
                  Adrianam obvia ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#257">Palazzo e
                  Giardino e Villa in Tivoli fatto dalla Felice
                  Memoria dell'Emerito et Reverendissimo Hippolito
                  Estense ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="be32182710s.html#258">Liber IIII. De Veterum
      Urbium, Fanorumque; celebrium ruinis et situ, nec non de
      admirandis Naturae operibus, quae in Tyburtino Territorio
      etiamnum spectantur</a>
      <ul>
        <li>
          <a href="be32182710s.html#258">Pars I. De Urbium Veterum
          vestigis adhuc superstibus</a>
          <ul>
            <li>
              <a href="be32182710s.html#258">I. De Aniene flumine,
              quod hoide Teverone vocant</a>
            </li>
            <li>
              <a href="be32182710s.html#259">II. De Empulo, Urbe
              vetustissima</a>
              <ul>
                <li>
                  <a href="be32182710s.html#261">Empulitanae Urbis
                  destructae Situs ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#262">III. De Destructionis
              Empulitanae Urbis, sive Ampilionis causa et
              Origine</a>
            </li>
            <li>
              <a href="be32182710s.html#264">IV. De totali Urbis
              Empulitanae devastatione</a>
            </li>
            <li>
              <a href="be32182710s.html#266">V. De extructi ex
              ruderibus Empulitanis Castri Sancti Angeli
              origine</a>
            </li>
            <li>
              <a href="be32182710s.html#267">VI. De reliquis
              Urbibus Territorii Tyburtini</a>
            </li>
            <li>
              <a href="be32182710s.html#268">VII. De Scopulo
              conversioni Divi Eustachii sacro, et de Ecclesia à
              Costantino Magno in eo fundata</a>
              <ul>
                <li>
                  <a href="be32182710s.html#269">Locus
                  Conversionis Sancti Eusatchij in Monte Vulturello
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#270">VIII. Oppidum Sancti
              Viti</a>
              <ul>
                <li>
                  <a href="be32182710s.html#272">Numismata varia
                  Veterum Romanorum Argentea non ita pridem in Lacu
                  Fucino reperta quorum me participem fecit
                  Illustrissimus Marcus Antonius Colonna Paliani
                  Gubernator ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#273">Templum Herculis
                  Tiburtini juxta veterum scriptorum mentem ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="be32182710s.html#274">Pars II. De
          Antiquitatibus Tyburtinis</a>
          <ul>
            <li>
              <a href="be32182710s.html#274">I. De Antiquitatibus
              Tyburtinis, quo ad Fana et Delubra, Deorum et primò
              de Templo Herculis</a>
            </li>
            <li>
              <a href="be32182710s.html#277">II. De Sibyllae
              Albuneae, sive Tyburtinae Templo</a>
              <ul>
                <li>
                  <a href="be32182710s.html#279">Sibylla Tiburtina
                  detta Albunea ▣</a>
                </li>
                <li>
                  <a href="be32182710s.html#280">Prospectus
                  Catarractae ex ponte in Templum Sybillae ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="be32182710s.html#281">Pars III. De Aquarum
          varietate, quae in Agro Tiburtino reperiuntur</a>
          <ul>
            <li>
              <a href="be32182710s.html#281">I. De quadruplici
              aquarum genere</a>
            </li>
            <li>
              <a href="be32182710s.html#282">II. De bonitate
              Aquarum, quibus Tyburtina Urbs fruitur</a>
            </li>
            <li>
              <a href="be32182710s.html#283">III. Litis decisio,
              de perfecta bonitate Aquarum Tyburtinarum</a>
            </li>
            <li>
              <a href="be32182710s.html#286">IV. De Albula, sive
              Sulphureo Lacu, quam Sulphataram vocant, in Tyburtino
              Agro, commemoratione dignissimo</a>
            </li>
            <li>
              <a href="be32182710s.html#288">V. De petrifico hujus
              Lacus Albunei succo, quo totam illam circumsitam
              terrestrem planitiem in saxum convertit</a>
            </li>
            <li>
              <a href="be32182710s.html#289">VI. De Monte scisso,
              caeterisque rebus in Tyburtino Agro, consideratione
              dignissimis</a>
            </li>
            <li>
              <a href="be32182710s.html#291">VII. De Aquaeductibus
              Tyburtinis</a>
            </li>
            <li>
              <a href="be32182710s.html#294">VIII. De Sanctis
              Tyburtinis, et ecclesiis, quae hodiè in Tyburtina
              Urbe, spectantur</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be32182710s.html#299">Pars III (sic!). De
          Sabinorum Regione</a>
          <ul>
            <li>
              <a href="be32182710s.html#299">I. Divisio Sabinae
              Regionis singularumque ejus partium descriptio</a>
              <ul>
                <li>
                  <a href="be32182710s.html#301">Sabinae
                  Antiquo-Modernae ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#304">II. De Urbibus
              Veteribus Sabinorum, modernis parallelos infertis</a>
              <ul>
                <li>
                  <a href="be32182710s.html#305">Loca celebriora
                  circa Pontem Salarium ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#316">II (sic!). De Agri
              Reatini Memorabilibus</a>
            </li>
            <li>
              <a href="be32182710s.html#320">III (sic!). De
              Montibus, Fontibus, et Fluviis Sabinorum</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be32182710s.html#323">Pars IV. De Volscorum
          Regno, veterumque eo urbium situ</a>
          <ul>
            <li>
              <a href="be32182710s.html#323">I. De Volscorum situ,
              et finibus</a>
              <ul>
                <li>
                  <a href="be32182710s.html#325">Regionis
                  Volscorum exacta descriptio ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="be32182710s.html#330">II. De Volscorum
              oppidis Mediterraneis</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="be32182710s.html#335">Liber IV (sic!). Ager
      Pomptinus</a>
      <ul>
        <li>
          <a href="be32182710s.html#335">Praefatio</a>
        </li>
        <li>
          <a href="be32182710s.html#336">I. Qua Regionis Pomptinae
          tum veteris tum modernae situs describitur</a>
        </li>
        <li>
          <a href="be32182710s.html#337">II. Exsiccationem Paludum
          Pontinarum nullis non saeculis tentatam et absolutam</a>
        </li>
        <li>
          <a href="be32182710s.html#343">III. Utrum Paludes
          Pontinae hodiè siccari queant, et ad culturam pristinam
          revocari</a>
        </li>
        <li>
          <a href="be32182710s.html#347">IV. De modo, ratione,
          viribus &#160;atque industriis, quibus Pomptinae Paludes
          ad perpetuam culturam reduci queant</a>
          <ul>
            <li>
              <a href="be32182710s.html#350">[stampa] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be32182710s.html#353">V. Quantum negotium
          institutum importet, et quam ingentia commoda inde in
          universum Latium emanare possint, in quo et de expensis
          in campo conservando faciendis tractatur</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="be32182710s.html#358">Index</a>
    </li>
  </ul>
  <hr />
</body>
</html>
