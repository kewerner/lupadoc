<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghcit737538201s.html#6">Catalogo istorico de’
      pittori e scultori ferraresi e delle opere loro; Tomo I.</a>
      <ul>
        <li>
          <a href="ghcit737538201s.html#8">All'illustrissimo e
          reverendissimo Signore Monsignor Giammaria Riminaldi</a>
        </li>
        <li>
          <a href="ghcit737538201s.html#14">A chi leggera'</a>
        </li>
        <li>
          <a href="ghcit737538201s.html#47">Nomi de' professori
          di pittura e scultura</a>
        </li>
        <li>
          <a href="ghcit737538201s.html#50">Catalogo istorico
          de' pittori, e scultori ferraresi e delle sue opere</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
