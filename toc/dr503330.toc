<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dr503330s.html#12">Testamento politico d’un
      accademico fiorentino</a>
      <ul>
        <li>
          <a href="dr503330s.html#14">Lo stampatore al lettore</a>
        </li>
        <li>
          <a href="dr503330s.html#22">I. De' motivi che anno
          indotto l'autore a fare il presente testamento&#160;</a>
        </li>
        <li>
          <a href="dr503330s.html#27">II. Dell'elezione de'
          Ministri</a>
        </li>
        <li>
          <a href="dr503330s.html#28">III. Dell'elezione della
          famiglia</a>
        </li>
        <li>
          <a href="dr503330s.html#29">IV. Delle guardie del
          Corpo</a>
        </li>
        <li>
          <a href="dr503330s.html#30">V. Dell'Agricoltura</a>
        </li>
        <li>
          <a href="dr503330s.html#36">VI. Del commerzio interno
          delle grascie</a>
        </li>
        <li>
          <a href="dr503330s.html#38">VII. Del commerzio esterno
          delle grascie</a>
        </li>
        <li>
          <a href="dr503330s.html#43">VIII. Del commerzio generale
          interno</a>
        </li>
        <li>
          <a href="dr503330s.html#49">IX. Del commerzio generale
          esterno</a>
        </li>
        <li>
          <a href="dr503330s.html#57">X. Delle compre, e delle
          vendite occulte</a>
        </li>
        <li>
          <a href="dr503330s.html#57">XI. Della Zecca</a>
        </li>
        <li>
          <a href="dr503330s.html#63">XII. Dell' danajo che entra,
          e che esce della Stato</a>
        </li>
        <li>
          <a href="dr503330s.html#66">XIII. Dell'Aunona</a>
        </li>
        <li>
          <a href="dr503330s.html#69">XIV. Della grascia</a>
        </li>
        <li>
          <a href="dr503330s.html#71">XV. Delle gravezze</a>
        </li>
        <li>
          <a href="dr503330s.html#74">XVI. Delle Dogane</a>
        </li>
        <li>
          <a href="dr503330s.html#77">XVII. Degli appalti
          attivi</a>
        </li>
        <li>
          <a href="dr503330s.html#78">XVIII. Del tabacco</a>
        </li>
        <li>
          <a href="dr503330s.html#83">XIX. Della cioccolata, del
          caffè, e del te</a>
        </li>
        <li>
          <a href="dr503330s.html#84">XX. Della neve</a>
        </li>
        <li>
          <a href="dr503330s.html#84">XXI. Del fieno, e della
          paglia</a>
        </li>
        <li>
          <a href="dr503330s.html#85">XXII. De' cavalli</a>
        </li>
        <li>
          <a href="dr503330s.html#85">XXIII. Delle carozze, de'
          calessi, e delle carrette</a>
        </li>
        <li>
          <a href="dr503330s.html#87">XXIV. Delle parrucche</a>
        </li>
        <li>
          <a href="dr503330s.html#89">XXV. De' teatri</a>
        </li>
        <li>
          <a href="dr503330s.html#90">XXVI. Delle carte, e de'
          dadi</a>
        </li>
        <li>
          <a href="dr503330s.html#91">XXVII. Dei lotti</a>
        </li>
        <li>
          <a href="dr503330s.html#96">XXVIII. Della posta</a>
        </li>
        <li>
          <a href="dr503330s.html#97">XXIX. Della stamperia
          camerale</a>
        </li>
        <li>
          <a href="dr503330s.html#99">XXX. Del ferro</a>
        </li>
        <li>
          <a href="dr503330s.html#101">XXXI. Dell'allume&#160;</a>
        </li>
        <li>
          <a href="dr503330s.html#102">XXXII. Del Vitriviuolo</a>
        </li>
        <li>
          <a href="dr503330s.html#103">XXXIII. Della polvere, e
          dello zolso</a>
        </li>
        <li>
          <a href="dr503330s.html#105">XXXIV. Del zolso nel ducato
          d'Urbino</a>
        </li>
        <li>
          <a href="dr503330s.html#105">XXXV. Delle saline
          d'Ostia</a>
        </li>
        <li>
          <a href="dr503330s.html#106">XXXVI. Della tassa delle
          galce</a>
        </li>
        <li>
          <a href="dr503330s.html#107">XXXVII. Delle tesorerie</a>
        </li>
        <li>
          <a href="dr503330s.html#108">XXXVIII. Di Ferrara</a>
        </li>
        <li>
          <a href="dr503330s.html#112">XXXIX. Di Bologna</a>
        </li>
        <li>
          <a href="dr503330s.html#113">XL. DI Romagna</a>
        </li>
        <li>
          <a href="dr503330s.html#115">XLI. D'Urbino</a>
        </li>
        <li>
          <a href="dr503330s.html#116">XLII. Della Marca</a>
        </li>
        <li>
          <a href="dr503330s.html#118">XLIII. Di
          Camerino&#160;</a>
        </li>
        <li>
          <a href="dr503330s.html#119">XLIV. D'Ascoli</a>
        </li>
        <li>
          <a href="dr503330s.html#119">XLV. Dell'Umbria</a>
        </li>
        <li>
          <a href="dr503330s.html#122">XLVI. Del Patrimonio</a>
        </li>
        <li>
          <a href="dr503330s.html#124">XLVII. Di Roma</a>
        </li>
        <li>
          <a href="dr503330s.html#127">XLVIII. Di Maritima,
          Campagna, Lazio, Sabina, e Nettuno</a>
        </li>
        <li>
          <a href="dr503330s.html#128">XLIX. Di Terracina</a>
        </li>
        <li>
          <a href="dr503330s.html#129">L. Degl' affitti</a>
        </li>
        <li>
          <a href="dr503330s.html#131">LI. Del lago di
          Comacchio</a>
        </li>
        <li>
          <a href="dr503330s.html#133">LII. De' beni della
          Samoggia</a>
        </li>
        <li>
          <a href="dr503330s.html#133">LIII. Di Castiglon del
          Lago</a>
        </li>
        <li>
          <a href="dr503330s.html#137">LIV. Di Bassano, e
          d'Orte</a>
        </li>
        <li>
          <a href="dr503330s.html#137">LV. Di Castro, e di
          Ronciglione</a>
        </li>
        <li>
          <a href="dr503330s.html#140">LVI. Di Nepi</a>
        </li>
        <li>
          <a href="dr503330s.html#141">LVII. Di
          Castelnuovo&#160;</a>
        </li>
        <li>
          <a href="dr503330s.html#141">LVIII. Di Porto</a>
        </li>
        <li>
          <a href="dr503330s.html#142">LIX. Di Castelgandolfo, e
          di Rocapriora</a>
        </li>
        <li>
          <a href="dr503330s.html#142">LX. D' Albano</a>
        </li>
        <li>
          <a href="dr503330s.html#143">LXI. Di Petronella</a>
        </li>
        <li>
          <a href="dr503330s.html#143">LXII. Del taglio delle
          legne nelle selve di Nettuno</a>
        </li>
        <li>
          <a href="dr503330s.html#144">LXIII. Del lago di Santa
          Maria</a>
        </li>
        <li>
          <a href="dr503330s.html#145">LXIV. Della Terra di San
          Felice</a>
        </li>
        <li>
          <a href="dr503330s.html#145">LXV. Delle rendite
          ecclesiastiche</a>
        </li>
        <li>
          <a href="dr503330s.html#147">LXVI. Degli appalti
          passivi</a>
        </li>
        <li>
          <a href="dr503330s.html#148">LXVII. Del mantenimento
          delle galee</a>
        </li>
        <li>
          <a href="dr503330s.html#152">LXIIX. Della provisione de'
          panni per servigio delle Truppe</a>
        </li>
        <li>
          <a href="dr503330s.html#154">LXIX. Della provisione
          d'ogni altra cosa che si ricerca per servigio delle
          truppe</a>
        </li>
        <li>
          <a href="dr503330s.html#156">LXX. Della provisione di
          tutto ciò che si ricerca per servigio del palazzo, e
          della persona del principe</a>
        </li>
        <li>
          <a href="dr503330s.html#156">LXXI. Di tutto ciò che si
          ricerca per mantenimento, e rifarciemtno del palazzo, ed
          ogni altra fabbrica che appartenga al principe</a>
        </li>
        <li>
          <a href="dr503330s.html#158">LXXII. Della riforma di
          varie spese superflue, a cui soggiace il
          principe&#160;</a>
        </li>
        <li>
          <a href="dr503330s.html#159">LXXIII. Delle Regaglie</a>
        </li>
        <li>
          <a href="dr503330s.html#161">LXXIV. Degli Stati
          d'Avignone, di Masserano, e di Benevento</a>
        </li>
        <li>
          <a href="dr503330s.html#162">LXXV. De' debiti che ha il
          principe</a>
        </li>
        <li>
          <a href="dr503330s.html#166">LXXVI. De' debiti delle
          comunità col principe</a>
        </li>
        <li>
          <a href="dr503330s.html#170">LXXVII. Delle legazioni, e
          de' vescovati</a>
        </li>
        <li>
          <a href="dr503330s.html#172">LXXVIII. De' governi</a>
        </li>
        <li>
          <a href="dr503330s.html#177">LXXIX. De' giudici civili,
          e criminali</a>
        </li>
        <li>
          <a href="dr503330s.html#179">LXXX. De' notaj, e de'
          medici</a>
        </li>
        <li>
          <a href="dr503330s.html#180">LXXXI. Delle
          congregazioni</a>
        </li>
        <li>
          <a href="dr503330s.html#182">LXXXII. Degl' editti, e de'
          bandi</a>
        </li>
        <li>
          <a href="dr503330s.html#183">LXXXIII. Delle feste
          comandate</a>
        </li>
        <li>
          <a href="dr503330s.html#184">LXXXIV. Dell' Immunità</a>
        </li>
        <li>
          <a href="dr503330s.html#185">LXXXV. De' fallimenti</a>
        </li>
        <li>
          <a href="dr503330s.html#186">LXXXVI. Degli eziosi</a>
        </li>
        <li>
          <a href="dr503330s.html#188">LXXXVII. De' poveri</a>
        </li>
        <li>
          <a href="dr503330s.html#190">LXXXIIX. Delle Strade</a>
        </li>
        <li>
          <a href="dr503330s.html#193">LXXXIX. Della Milizia</a>
        </li>
        <li>
          <a href="dr503330s.html#196">XC. Delle lettere</a>
        </li>
        <li>
          <a href="dr503330s.html#198">CI. Del modo d'abbelir
          Roma</a>
        </li>
        <li>
          <a href="dr503330s.html#200">XCII. Dell' illuminazioni
          delle strade principali, e più batutte di Roma</a>
        </li>
        <li>
          <a href="dr503330s.html#200">XCIII. Della distribuzione
          de' siti a ciascuna professione</a>
        </li>
        <li>
          <a href="dr503330s.html#202">XCIV. Di Porta del popolo,
          e delle tre strade corrispondenti alla guglia</a>
        </li>
        <li>
          <a href="dr503330s.html#204">XCV. Di Porta San
          Giovanni</a>
        </li>
        <li>
          <a href="dr503330s.html#205">XCVI. Delle piazze
          principali</a>
        </li>
        <li>
          <a href="dr503330s.html#206">XCVII. Di Piazzanavona</a>
        </li>
        <li>
          <a href="dr503330s.html#207">XCVIII. Di Piazza a campo
          di fiori</a>
        </li>
        <li>
          <a href="dr503330s.html#208">XCIX. Di Piazza farnese</a>
        </li>
        <li>
          <a href="dr503330s.html#209">C. Della Piazza di
          Campidoglio</a>
        </li>
        <li>
          <a href="dr503330s.html#212">CI. Delle palazze
          inferiori</a>
        </li>
        <li>
          <a href="dr503330s.html#213">CII. Di Piazza giudea</a>
        </li>
        <li>
          <a href="dr503330s.html#214">CIII. Di Piazza alla
          madonna di loreto</a>
        </li>
        <li>
          <a href="dr503330s.html#215">CIV. Di Piazza alla madonna
          de' monti</a>
        </li>
        <li>
          <a href="dr503330s.html#216">CV. Di Piazza barberina</a>
        </li>
        <li>
          <a href="dr503330s.html#217">CVI. Di Piazza a fontana di
          Trevi</a>
        </li>
        <li>
          <a href="dr503330s.html#218">CVII. Del Palazzo
          quirinale</a>
        </li>
        <li>
          <a href="dr503330s.html#220">CVIII. De' ponti sul
          fiume</a>
        </li>
        <li>
          <a href="dr503330s.html#222">CIX. Della fabbrica alla
          Trinità de' monti</a>
        </li>
        <li>
          <a href="dr503330s.html#223">CX. Della conclusione dell'
          opera</a>
        </li>
        <li>
          <a href="dr503330s.html#226">Tavola</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
