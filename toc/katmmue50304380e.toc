<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katmmue50304380es.html#10">Verzeichniss der
      Gemälde in der königlichen Pinakothek zu München</a>
      <ul>
        <li>
          <a href="katmmue50304380es.html#12">Vorwort</a>
        </li>
        <li>
          <a href="katmmue50304380es.html#16">Erinnerungen</a>
        </li>
        <li>
          <a href="katmmue50304380es.html#18">Königliche
          Pinakothek</a>
          <ul>
            <li>
              <a href="katmmue50304380es.html#20">Saal der
              Stifter</a>
            </li>
            <li>
              <a href="katmmue50304380es.html#24">Erste
              Abtheilung. Beschreibung derjenigen Gemälde, welche
              in den neun grossen Sälen aufgestellt sind</a>
            </li>
            <li>
              <a href="katmmue50304380es.html#160">Zweite
              Abtheilung. Beschreibung derjenigen Gemälde, welche
              in den dreiundzwanzig Cabinetten nach Malerschulen
              geordnet, aufgestellt sind</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
