<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg804r661569s.html#4">Le Antichità della Città di
      Roma</a>
      <ul>
        <li>
          <a href="dg804r661569s.html#8">Al magnanimo et
          eccellente Signore il S. Ottavio Sammarco Thomaso
          Porcacchi</a>
        </li>
        <li>
          <a href="dg804r661569s.html#12">All'ilustrissimo et
          eccellentissimo S. il S. Don Francesco de' Medici</a>
        </li>
        <li>
          <a href="dg804r661569s.html#18">Giovanni Varisco a'
          lettori</a>
        </li>
        <li>
          <a href="dg804r661569s.html#20">Di M. Benedetto
          Varchi</a>
        </li>
        <li>
          <a href="dg804r661569s.html#22">I. Del luogo dove fu
          edificata Roma</a>
          <ul>
            <li>
              <a href="dg804r661569s.html#40">Del colle del
              Campidoglio, prima detto Capitolino</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg804r661569s.html#138">II. Del Foro Olitorio
          e Boario, e di tutto quello che è restato nella valle,
          che è tra il Campidoglio e il Palatino&#160;</a>
        </li>
        <li>
          <a href="dg804r661569s.html#215">III. Del Colle
          dell'Esquilie&#160;</a>
        </li>
        <li>
          <a href="dg804r661569s.html#354">IV. Del
          Trastevere&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
