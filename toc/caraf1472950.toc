<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caraf1472950s.html#8">Descrizzione Delle Imagini
      Dipinte Da Rafaelle D’Urbino Nelle Camere del Palazzo
      Apostolico Vaticano</a>
      <ul>
        <li>
          <a href="caraf1472950s.html#10">Beatissimo padre</a>
        </li>
        <li>
          <a href="caraf1472950s.html#14">A' gli studiosi di
          Rafaelle&#160;</a>
        </li>
        <li>
          <a href="caraf1472950s.html#16">Descrizione delle
          quattro Imagini dipinte da Rafaelle d'Urbino nella Camera
          di Signatura&#160;</a>
        </li>
        <li>
          <a href="caraf1472950s.html#42">Pitture di Rafaelle
          nelle seconda Camera contigua&#160;</a>
        </li>
        <li>
          <a href="caraf1472950s.html#56">Pitture di Rafaelle da
          Urbino colorite nella terza Camera&#160;</a>
        </li>
        <li>
          <a href="caraf1472950s.html#66">Pitture di Rafaelle da
          Urbino nella Sala del Palazzo Vaticano, con li fatti di
          Costantino</a>
        </li>
        <li>
          <a href="caraf1472950s.html#78">La favola di Amore, e
          Psiche dipinta da Rafaelle d'Urbino&#160;</a>
          <ul>
            <li>
              <a href="caraf1472950s.html#79">Introduzzione</a>
            </li>
            <li>
              <a href="caraf1472950s.html#83">Argomento della
              favola di Psiche</a>
            </li>
            <li>
              <a href="caraf1472950s.html#96">Della riparazione
              della galleria del Caracci nel Palazzo Farnese, e
              della loggia di Rafaelle alla Lungara&#160;</a>
            </li>
            <li>
              <a href="caraf1472950s.html#101">Se Rafaelle
              ingrandì, e megliorò la maniera, per aver veduto
              l'opere di Michel'Angelo&#160;</a>
            </li>
            <li>
              <a href="caraf1472950s.html#108">Dell'Ingegno,
              eccellenza, e grazia di Rafaelle comparato ad
              Apelle&#160;</a>
            </li>
            <li>
              <a href="caraf1472950s.html#115">Al Conte Baldassar
              Castiglione</a>
            </li>
            <li>
              <a href="caraf1472950s.html#116">Nascita, e
              Monumento di Rafaelle&#160;</a>
            </li>
            <li>
              <a href="caraf1472950s.html#120">Gli onori della
              pittura, e scoltura; Discorso di Giovan Pietro
              Bellori</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
