<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dr86544202s.html#8">Interpretatio obeliscorum
      urbis ad Gregorium XVI Pontificem Maximum</a>
      <ul>
        <li>
          <a href="dr86544202s.html#10">Series Tabularum</a>
        </li>
        <li>
          <a href="dr86544202s.html#12">[Tabulae]</a>
          <ul>
            <li>
              <a href="dr86544202s.html#12">I. Obeliscus
              Lateranensis ›Obelisco Lateranense‹</a>
            </li>
            <li>
              <a href="dr86544202s.html#13">II. Obeliscus
              Flaminius ›Obelisco Flaminio‹</a>
            </li>
            <li>
              <a href="dr86544202s.html#14">III. Obeliscus
              Campensis ›Obelisci: Iseum Campense‹&#160;</a>
            </li>
            <li>
              <a href="dr86544202s.html#15">IV. Obeliscus
              Pamphilius [›Obelisco agonale‹]</a>
            </li>
            <li>
              <a href="dr86544202s.html#16">V. Obelisci
              Beneventani</a>
            </li>
            <li>
              <a href="dr86544202s.html#17">VI. Obeliscus
              Barberinus [›Obelisco del Pincio‹]</a>
            </li>
            <li>
              <a href="dr86544202s.html#18">VII. Obeliscus
              Sallustianus ›Obelisco Sallustiano‹</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
