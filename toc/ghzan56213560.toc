<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghzan56213560s.html#6">Avvertimenti per lo
      incamminamento di un giovane alla pittura</a>
      <ul>
        <li>
          <a href="ghzan56213560s.html#8">Lettera
          All'Illustrissimo, e Reverendissimo Monsignore Marc
          Antonio Laurenti</a>
        </li>
        <li>
          <a href="ghzan56213560s.html#14">Proemio</a>
        </li>
        <li>
          <a href="ghzan56213560s.html#20">Capitoli&#160;</a>
        </li>
        <li>
          <a href="ghzan56213560s.html#158">All'Onorando Signor
          Petronio Dalla Volpe</a>
        </li>
        <li>
          <a href="ghzan56213560s.html#160">In laude della
          pittura, e della poesia</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
