<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501755s.html#6">Le cose maravigliose dell'Alma
      Città di Roma</a>
      <ul>
        <li>
          <a href="dg4501755s.html#8">Le sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg4501755s.html#8">La prima chiesa è ›San
              Giovanni in Laterano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501755s.html#13">Seconda chiesa di ›San
              Pietro in Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501755s.html#16">La terza chiesa è ›San
              Paolo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4501755s.html#17">La quarta chiesa è
              ›Santa Maria Maggiore‹ ▣</a>
            </li>
            <li>
              <a href="dg4501755s.html#19">La quinta chiesa è ›San
              Lorenzo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4501755s.html#20">La sesta chiesa è ›San
              Sebastiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501755s.html#21">La settima chiesa è
              ›Santa Croce in Gerusalemme‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501755s.html#22">Nell'›Isola Tiberina‹</a>
        </li>
        <li>
          <a href="dg4501755s.html#23">In ›Trastevere‹</a>
        </li>
        <li>
          <a href="dg4501755s.html#26">Nel ›Borgo‹</a>
        </li>
        <li>
          <a href="dg4501755s.html#28">Della Porta Flaminia fuori
          del Popolo ›Porta del popolo‹ fino alle radici del
          Campidoglio</a>
        </li>
        <li>
          <a href="dg4501755s.html#42">Del ›Campidoglio‹ a man
          sinistra verso li monti</a>
        </li>
        <li>
          <a href="dg4501755s.html#50">Dal Campidoglio a man
          dritta verso li monti</a>
        </li>
        <li>
          <a href="dg4501755s.html#57">Tavola delle Chiese di
          Roma</a>
        </li>
        <li>
          <a href="dg4501755s.html#62">Le Stationi, che sono nelle
          chiese di Roma, si per la Quadragesima, come per tutto
          l'anno. Con le solite Indulgenze.</a>
          <ul>
            <li>
              <a href="dg4501755s.html#62">Nel mese di Gennaro</a>
            </li>
            <li>
              <a href="dg4501755s.html#62">Nel mese di Febraro</a>
            </li>
            <li>
              <a href="dg4501755s.html#66">Nel mese di Aprile</a>
            </li>
            <li>
              <a href="dg4501755s.html#66">Nel mese di Maggio</a>
            </li>
            <li>
              <a href="dg4501755s.html#67">Nel mese di Giugno</a>
            </li>
            <li>
              <a href="dg4501755s.html#68">Nel mese di Luglio</a>
            </li>
            <li>
              <a href="dg4501755s.html#68">Nel mese di Agosto</a>
            </li>
            <li>
              <a href="dg4501755s.html#69">Nel mese di
              Settembre</a>
            </li>
            <li>
              <a href="dg4501755s.html#70">Nel mese di Ottobre</a>
            </li>
            <li>
              <a href="dg4501755s.html#70">Nel mese di
              Novembre</a>
            </li>
            <li>
              <a href="dg4501755s.html#71">Le Stationi
              dell'Advento</a>
              <ul>
                <li>
                  <a href="dg4501755s.html#71">Nel mese di
                  Dicembre</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501755s.html#72">Trattato over modo
          d'acquistar l'indulgentie alle Stationi</a>
        </li>
        <li>
          <a href="dg4501755s.html#78">La Guida Romana per li
          forastieri: che vengono per vedere le antichità di Roma,
          à una per una in bellissima forma et brevità</a>
          <ul>
            <li>
              <a href="dg4501755s.html#78">Del ›Borgo‹ la prima
              giornata</a>
              <ul>
                <li>
                  <a href="dg4501755s.html#79">Del
                  ›Trastevere‹</a>
                </li>
                <li>
                  <a href="dg4501755s.html#80">Dell'›Isola
                  Tiberina‹</a>
                </li>
                <li>
                  <a href="dg4501755s.html#80">Del Ponte Santa
                  Maria ›Ponte Rotto‹, del palazzo di Pilato ›Casa
                  dei Crescenzi‹, et d'altre cose.</a>
                </li>
                <li>
                  <a href="dg4501755s.html#82">Delle Therme
                  Antoniane ›Terme di Caracalla‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501755s.html#82">Di ›San Giovanni in
                  Laterano‹, ›Santa Croce in Gerusalemme‹, et
                  altri</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501755s.html#83">Giornata seconda</a>
              <ul>
                <li>
                  <a href="dg4501755s.html#83">Della ›Porta del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4501755s.html#84">Dei cavalli di
                  marmo, che stanno a Monte Cavallo ›Fontana di
                  Monte Cavallo‹, et delle Therme Dioceltiani
                  ›Terme di Diocleziano‹</a>
                </li>
                <li>
                  <a href="dg4501755s.html#85">Della Strada Pia
                  ›Via XX Settembre‹</a>
                </li>
                <li>
                  <a href="dg4501755s.html#86">Della ›Porta
                  Pia‹</a>
                </li>
                <li>
                  <a href="dg4501755s.html#86">Di ›Sant'Agnese
                  fuori le Mura‹ et altre anticaglie</a>
                </li>
                <li>
                  <a href="dg4501755s.html#87">Del tempio d'Iside
                  ›Isis Patricia‹ et altre cose</a>
                </li>
                <li>
                  <a href="dg4501755s.html#87">Delle ›Sette Sale‹,
                  et del ›Colosseo‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501755s.html#88">Del Tempio della
                  Pace ›Foro della Pace‹, et del monte ›Palatino‹,
                  hora detto Palazzo maggiore, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501755s.html#89">Del ›Campidoglio‹,
                  et altre cose</a>
                </li>
                <li>
                  <a href="dg4501755s.html#90">De i portichi
                  d'Ottavia di Settimio ›Portico d'Ottavia‹, et
                  ›Teatro di Pompeo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501755s.html#90">Giornata terza</a>
              <ul>
                <li>
                  <a href="dg4501755s.html#90">Delle due colonna,
                  una di Antonino Pio ›Colonna di Antonino Pio‹ e
                  l'altra di Traiano ›Colonna Traiana‹, et altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4501755s.html#91">Della Rotonda,
                  overo ›Pantheon‹</a>
                </li>
                <li>
                  <a href="dg4501755s.html#91">De i Bagni di
                  Agrippa ›Terme di Agrippa‹, e di Nerone ›Terme
                  Neroniano-Alessandrine‹</a>
                </li>
                <li>
                  <a href="dg4501755s.html#91">Della ›Piazza
                  Navona‹, et di mastro Pasquino ›Statua di
                  Pasquino‹</a>
                </li>
                <li>
                  <a href="dg4501755s.html#93">[Tavole]</a>
                  <ul>
                    <li>
                      <a href="dg4501755s.html#93">Summi
                      pontifices</a>
                    </li>
                    <li>
                      <a href="dg4501755s.html#122">Reges et
                      Imperatores Romani</a>
                    </li>
                    <li>
                      <a href="dg4501755s.html#126">Li Re di
                      Francia</a>
                    </li>
                    <li>
                      <a href="dg4501755s.html#128">Li Re del
                      Regno di Napoli et di Sicilia [...]</a>
                    </li>
                    <li>
                      <a href="dg4501755s.html#129">Li Dugi di
                      Venegia</a>
                    </li>
                    <li>
                      <a href="dg4501755s.html#132">Li Duchi di
                      Milano</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501755s.html#134">L'Antichità di Roma di M.
      Andrea Palladio racolta brevemente da gli Autori antichi, et
      moderni ▣</a>
      <ul>
        <li>
          <a href="dg4501755s.html#135">Alli lettori</a>
        </li>
        <li>
          <a href="dg4501755s.html#137">Libro I. Dell'edification
          di Roma</a>
          <ul>
            <li>
              <a href="dg4501755s.html#140">Del circuito di
              Roma</a>
            </li>
            <li>
              <a href="dg4501755s.html#140">Delle Porte</a>
            </li>
            <li>
              <a href="dg4501755s.html#142">Delle Vie</a>
            </li>
            <li>
              <a href="dg4501755s.html#143">Delli Ponti che sono
              sopra il Tevere et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501755s.html#144">Dell'Isola del Tevere
              ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#145">Delli Monti</a>
            </li>
            <li>
              <a href="dg4501755s.html#146">Del Monte Testaccio
              ›Testaccio (monte) ‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#146">Delle acque, et chi le
              condusse in Roma</a>
            </li>
            <li>
              <a href="dg4501755s.html#147">Della Cloaca ›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#148">Delli acquedotti</a>
            </li>
            <li>
              <a href="dg4501755s.html#148">Delle ›Sette Sale‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#149">Delle terme cioè
              bagni, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501755s.html#150">Delle Naumachie, dove
              si facevano le battaglie navali, et che cose
              erano</a>
            </li>
            <li>
              <a href="dg4501755s.html#150">De' Cerchi, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501755s.html#151">De' theatri, et che
              cosa erano, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501755s.html#152">Delli Anfiteatri et
              suoi edificatori, et che cosa erano</a>
            </li>
            <li>
              <a href="dg4501755s.html#152">De' Fori, cioè
              piazze</a>
            </li>
            <li>
              <a href="dg4501755s.html#153">Delli Archi Trionfali,
              et a chi si davano</a>
            </li>
            <li>
              <a href="dg4501755s.html#154">De' portichi</a>
            </li>
            <li>
              <a href="dg4501755s.html#155">De' trofei et colonne
              memorande</a>
            </li>
            <li>
              <a href="dg4501755s.html#156">De' colossi</a>
            </li>
            <li>
              <a href="dg4501755s.html#156">Delle piramidi</a>
            </li>
            <li>
              <a href="dg4501755s.html#156">Delle mete</a>
            </li>
            <li>
              <a href="dg4501755s.html#157">Delli obelischi, overo
              Aguglie</a>
            </li>
            <li>
              <a href="dg4501755s.html#157">Delle statue</a>
            </li>
            <li>
              <a href="dg4501755s.html#157">Di Marforio ›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#158">De cavalli</a>
            </li>
            <li>
              <a href="dg4501755s.html#158">Delle librarie</a>
            </li>
            <li>
              <a href="dg4501755s.html#158">Delli horiuoli</a>
            </li>
            <li>
              <a href="dg4501755s.html#159">Della Casa Aurea di
              Nerone ›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#160">Delle altre case de'
              ittadini</a>
            </li>
            <li>
              <a href="dg4501755s.html#161">Delle Curie et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501755s.html#161">De' senatuli, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501755s.html#161">De' magistrati et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501755s.html#163">Dei Comitii, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501755s.html#163">Delle tribù</a>
            </li>
            <li>
              <a href="dg4501755s.html#163">Delle Regioni, cioè
              Rioni et sue insegne ›Regiones Quattuordecim‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#163">Delle Basiliche et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501755s.html#164">Del ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#165">Dello Erario
              ›Aerarium‹ cioè camera del commune, et che moneta si
              spendeva in Roma a que' tempi</a>
            </li>
            <li>
              <a href="dg4501755s.html#166">Del ›Grecostasi‹, et
              che cosa era</a>
            </li>
            <li>
              <a href="dg4501755s.html#166">Dell'›Asilo di
              Romolo‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#167">Delle Rostre et che
              cosa erano ›Rostri‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#167">Della colonna detta
              miliario ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#167">Del Tempio di Carmenta
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#167">Della Colonna Bellica
              ›Columna Bellica‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#167">Della Colonna Lattaria
              ›Columna Lactaria‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#168">Dell'Equimelio
              ›Aequimaelium‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#168">Del ›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#168">Del Tigillo Sororio
              ›Tigillum Sororium‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#168">De' Campi Forasteri
              ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#168">Della ›Villa
              Publica‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#169">Della ›Taberna
              Meritoria‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#169">Del ›Vivarium‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#169">Degli Horti</a>
            </li>
            <li>
              <a href="dg4501755s.html#170">Del ›Velabro‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#170">Delle ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#171">Delli Clivi</a>
            </li>
            <li>
              <a href="dg4501755s.html#171">De' Prati</a>
            </li>
            <li>
              <a href="dg4501755s.html#172">De' granari publici et
              magazzini del sale</a>
            </li>
            <li>
              <a href="dg4501755s.html#172">Delle carceri
              publiche</a>
            </li>
            <li>
              <a href="dg4501755s.html#172">Di alcune feste, et
              giuochi che si solevano celebrare in Roma</a>
            </li>
            <li>
              <a href="dg4501755s.html#173">Del Sepolcro di
              Augusto, d'Adriano et di Settimio ›Mausoleo di
              Augusto‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#174">De Tempii</a>
            </li>
            <li>
              <a href="dg4501755s.html#175">De' Sacerdoti delle
              Vergini Vestali [...]</a>
            </li>
            <li>
              <a href="dg4501755s.html#178">Dell'Armamentario et
              che cosa era</a>
            </li>
            <li>
              <a href="dg4501755s.html#178">Dell'esercito romano
              di terra, et di mare, et loro insegne</a>
            </li>
            <li>
              <a href="dg4501755s.html#178">De' triomfi et a chi
              si concedevano et chi fu il primo triomfatore, et di
              quante maniere erano</a>
            </li>
            <li>
              <a href="dg4501755s.html#179">Delle Corone, et a chi
              si davano</a>
            </li>
            <li>
              <a href="dg4501755s.html#180">Del numero del Popolo
              Romano</a>
            </li>
            <li>
              <a href="dg4501755s.html#180">Delle ricchezze del
              popolo Romano</a>
            </li>
            <li>
              <a href="dg4501755s.html#180">Della liberalità de li
              antichi Romani</a>
            </li>
            <li>
              <a href="dg4501755s.html#181">Delli matrimoni
              antichi, et loro usanza</a>
            </li>
            <li>
              <a href="dg4501755s.html#182">Della buona creanza,
              che davano a i figliuoli</a>
            </li>
            <li>
              <a href="dg4501755s.html#182">Della separatione de'
              matrimonij</a>
            </li>
            <li>
              <a href="dg4501755s.html#183">Dell'esequie antiche
              et sue ceremonie</a>
            </li>
            <li>
              <a href="dg4501755s.html#184">Delle torri</a>
            </li>
            <li>
              <a href="dg4501755s.html#184">Del ›Tevere‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#185">Del Palazzo Papale
              ›Palazzo Apostolico Vaticano‹ et di Belvedere
              ›Cortile del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#186">Del ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4501755s.html#186">Recapitulatione
              dell'antichità</a>
            </li>
            <li>
              <a href="dg4501755s.html#187">De' tempii de gli
              antichi fuori di Roma</a>
            </li>
            <li>
              <a href="dg4501755s.html#189">Quante volte è stata
              presa Roma</a>
            </li>
            <li>
              <a href="dg4501755s.html#190">Dei fuochi de gli
              Antichi scritti da pochi autori, cavati da alcuni
              frammenti d'Historie</a>
            </li>
            <li>
              <a href="dg4501755s.html#194">Tavola delle Antichità
              della Città di Roma</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501755s.html#198">Lettera Pastorale di Monsignor
      Illustrissimo et reverendissimo Card. Borromeo [...]</a>
      <ul>
        <li>
          <a href="dg4501755s.html#199">Avvicinandosi, Figliuogli
          dilettissimi [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501755s.html#213">[Tavola] Poste de Italia</a>
      <ul>
        <li>
          <a href="dg4501755s.html#213">Poste da Roma à
          Bologna</a>
        </li>
        <li>
          <a href="dg4501755s.html#214">Poste da Bologna a Mantua,
          e da Mantua a Trento</a>
        </li>
        <li>
          <a href="dg4501755s.html#214">Poste da Trento a
          Bruscelles</a>
        </li>
        <li>
          <a href="dg4501755s.html#216">Poste da Roma à
          Venetia</a>
        </li>
        <li>
          <a href="dg4501755s.html#217">Poste da Genova a
          Milano</a>
        </li>
        <li>
          <a href="dg4501755s.html#218">Poste da Milano a
          Venetia</a>
        </li>
        <li>
          <a href="dg4501755s.html#218">Poste da Roma a Napoli</a>
        </li>
        <li>
          <a href="dg4501755s.html#219">Poste da Napoli à
          Messina</a>
        </li>
        <li>
          <a href="dg4501755s.html#220">Poste da Bologna in
          Ancona</a>
        </li>
        <li>
          <a href="dg4501755s.html#220">Poste da Bologna à
          Fiorenza</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
