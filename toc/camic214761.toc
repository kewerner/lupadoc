<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="camic214761s.html#8">Spigolatura Michelangiolesca
      fatta da P. Fanfani</a>
      <ul>
        <li>
          <a href="camic214761s.html#10">Ai lettori</a>
        </li>
        <li>
          <a href="camic214761s.html#22">Descrizione della
          galleria Buonarroti</a>
        </li>
        <li>
          <a href="camic214761s.html#66">Vindicale del Cav.
          Nicoloò Gabburri</a>
        </li>
        <li>
          <a href="camic214761s.html#98">Lettere e documenti</a>
        </li>
        <li>
          <a href="camic214761s.html#212">Storia della Accademia
          del Disegno</a>
        </li>
        <li>
          <a href="camic214761s.html#330">Appunti spicciolati dal
          codice Marucelliano</a>
        </li>
        <li>
          <a href="camic214761s.html#350">Poesie latine scritte
          in morte di Michelangelo</a>
        </li>
        <li>
          <a href="camic214761s.html#358">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
