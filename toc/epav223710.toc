<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="epav223710s.html#8">De’ plazzi reali che sono
      stati nella città, e territorio di Pavia</a>
      <ul>
        <li>
          <a href="epav223710s.html#10">A sua eccelenza il Signor
          Conte Carlo de Firmian</a>
        </li>
        <li>
          <a href="epav223710s.html#12">Eccellenza&#160;</a>
        </li>
        <li>
          <a href="epav223710s.html#18">[De' plazzi reali che
          sono stati nella città, e territorio di Pavia]</a>
          <ul>
            <li>
              <a href="epav223710s.html#192">[piante]</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
