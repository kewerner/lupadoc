<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502502s.html#6">Roma Ricercata nel suo sito, e
      nella scuola di tutti gli Antiquarij</a>
      <ul>
        <li>
          <a href="dg4502502s.html#8">Al lettore forastiero</a>
        </li>
        <li>
          <a href="dg4502502s.html#15">Indice delle giornate</a>
        </li>
        <li>
          <a href="dg4502502s.html#16">I. Per il Borgo
          Vaticano</a>
        </li>
        <li>
          <a href="dg4502502s.html#24">II. Per il Trastevere</a>
        </li>
        <li>
          <a href="dg4502502s.html#29">III. Da Strada Giulia
          all'Isola di S. Bartolomeo</a>
        </li>
        <li>
          <a href="dg4502502s.html#35">IV. Da S. Lorenzo in Damaso
          al Monte Aventino</a>
        </li>
        <li>
          <a href="dg4502502s.html#41">V. Dalla Piazza di Pasquino
          per li Monti Celio e Palatino&#160;</a>
        </li>
        <li>
          <a href="dg4502502s.html#51">VI. Da S. Salvatore del
          Lauro per Campo Vaccino e per le Carine&#160;</a>
        </li>
        <li>
          <a href="dg4502502s.html#63">VII. Dalla Piazza di S.
          Apollinare per il Monte Viminale, e Quirinale</a>
        </li>
        <li>
          <a href="dg4502502s.html#78">VIII. Da Piazza Nicosia
          alle Terme Diocletiane</a>
        </li>
        <li>
          <a href="dg4502502s.html#84">IX. Da Piazza Borghese à
          porta Pinciana</a>
        </li>
        <li>
          <a href="dg4502502s.html#91">X. Per le nove Chiese</a>
        </li>
        <li>
          <a href="dg4502502s.html#128">Notitia delle porte,
          monti, e rioni della Città</a>
        </li>
        <li>
          <a href="dg4502502s.html#139">Indice delle cose più
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
