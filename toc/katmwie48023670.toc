<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katmwie48023670s.html#6">Descrizzione (sic!)
      completa di tutto ciò che ritrovasi nella Galleria di pittura
      e scultura di sua altezza Giuseppe Wenceslao</a>
      <ul>
        <li>
          <a href="katmwie48023670s.html#8">Prefazione</a>
        </li>
        <li>
          <a href="katmwie48023670s.html#10">Protesta
          dell'autore</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="katmwie48023670s.html#12">[Text]</a>
      <ul>
        <li>
          <a href="katmwie48023670s.html#12">Della pittura in
          generale</a>
        </li>
        <li>
          <a href="katmwie48023670s.html#25">Descrizione della
          Fabbrica</a>
          <ul>
            <li>
              <a href="katmwie48023670s.html#27">Nella sala</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#28">Sei medaglie
              intorno il sottinsu</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#29">Camera prima</a>
              <ul>
                <li>
                  <a href="katmwie48023670s.html#31">Facciata
                  prima</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#33">Facciata
                  seconda</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#34">Faccita
                  (sic!) terza</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#36">Facciata
                  quarta</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#39">Statue</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katmwie48023670s.html#40">Camera
              seconda</a>
              <ul>
                <li>
                  <a href="katmwie48023670s.html#41">Facciata
                  prima</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#42">Facciata
                  seconda</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#44">Facciata
                  terza</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#45">Facciata
                  quarta</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#45">Statue</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katmwie48023670s.html#47">Camera terza</a>
              <ul>
                <li>
                  <a href="katmwie48023670s.html#48">Facciata
                  prima</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#52">Facciata
                  seconda</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#55">Facciata
                  terza</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#58">Facciata
                  quarta</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#59">Statue</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katmwie48023670s.html#60">Camera
              quarta</a>
              <ul>
                <li>
                  <a href="katmwie48023670s.html#61">Facciata
                  prima</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#65">Facciata
                  seconda</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#66">Facciata
                  terza</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#68">Facciata
                  quarta</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#69">Statue ed
                  altro</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katmwie48023670s.html#70">Camera
              quinta</a>
              <ul>
                <li>
                  <a href="katmwie48023670s.html#77">Facciata
                  prima</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#78">Facciata
                  seconda</a>
                  <ul>
                    <li>
                      <a href="katmwie48023670s.html#78">Tavola
                      prima</a>
                    </li>
                    <li>
                      <a href="katmwie48023670s.html#79">Tavola
                      seconda</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="katmwie48023670s.html#80">Facciata
                  terza</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#81">Facciata
                  quarta</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#82">Nel
                  mezzo</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katmwie48023670s.html#82">Camera sesta</a>
              <ul>
                <li>
                  <a href="katmwie48023670s.html#83">Facciata
                  prima</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#84">Facciata
                  seconda</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#86">Facciata
                  terza</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#88">Facciata
                  quarta</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#89">Statue, ed
                  altro</a>
                  <ul>
                    <li>
                      <a href="katmwie48023670s.html#89">Facciata
                      prima</a>
                    </li>
                    <li>
                      <a href="katmwie48023670s.html#89">Facciata
                      seconda</a>
                    </li>
                    <li>
                      <a href="katmwie48023670s.html#90">Facciata
                      terza</a>
                    </li>
                    <li>
                      <a href="katmwie48023670s.html#90">Facciata
                      quarta</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="katmwie48023670s.html#91">Camera
              settima</a>
              <ul>
                <li>
                  <a href="katmwie48023670s.html#92">Facciata
                  prima</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#93">Facciata
                  seconda</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#94">Facciata
                  terza</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#95">Facciata
                  quarta</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#95">Statue ed
                  altro</a>
                  <ul>
                    <li>
                      <a href="katmwie48023670s.html#95">Facciata
                      prima</a>
                    </li>
                    <li>
                      <a href="katmwie48023670s.html#95">Facciata
                      seconda</a>
                    </li>
                    <li>
                      <a href="katmwie48023670s.html#96">Facciata
                      terza</a>
                    </li>
                    <li>
                      <a href="katmwie48023670s.html#97">Facciata
                      quarta</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="katmwie48023670s.html#98">Camera
              ottava</a>
              <ul>
                <li>
                  <a href="katmwie48023670s.html#99">Facciata
                  prima</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#101">Facciata
                  seconda</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#102">Facciata
                  terza</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#104">Facciata
                  quarta</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#105">Statue ed
                  altro</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katmwie48023670s.html#105">Camera nona</a>
              <ul>
                <li>
                  <a href="katmwie48023670s.html#106">Facciata
                  prima</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#107">Facciata
                  seconda</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#108">Facciata
                  terza</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#109">Facciata
                  quarta</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#109">Statue ed
                  altro</a>
                  <ul>
                    <li>
                      <a href=
                      "katmwie48023670s.html#109">Facciata
                      prima</a>
                    </li>
                    <li>
                      <a href=
                      "katmwie48023670s.html#109">Facciata
                      seconda</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="katmwie48023670s.html#110">Camera
              decima</a>
              <ul>
                <li>
                  <a href="katmwie48023670s.html#111">Facciata
                  prima</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#112">Facciata
                  seconda</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#114">Facciata
                  terza</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#115">Facciata
                  quarta</a>
                </li>
                <li>
                  <a href="katmwie48023670s.html#116">Statue, ed
                  altro</a>
                  <ul>
                    <li>
                      <a href=
                      "katmwie48023670s.html#116">Facciata
                      prima</a>
                    </li>
                    <li>
                      <a href=
                      "katmwie48023670s.html#117">Facciata
                      seconda</a>
                    </li>
                    <li>
                      <a href=
                      "katmwie48023670s.html#118">Facciata
                      terza</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmwie48023670s.html#122">Compendio delle
          vite de pittori, scultori e d'altri artefici, le di cui
          famose opere formano la prescrita celebre galleria</a>
          <ul>
            <li>
              <a href="katmwie48023670s.html#124">Carlo
              Cignani</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#125">Francesco
              Bassano, il Seniore, avo del Iuniore</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#125">Giovanni
              Holbein, il Seniore</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#126">Giroramo
              Ferabosco</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#127">Paolo Caliari,
              detto il Veronese</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#128">Francesco
              Floris</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#128">Giacomo Palma,
              Seniore</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#129">Pellegrino
              Tibladi, da Bologna</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#129">Giovanni Ulrico
              Mayer</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#130">Pellegrino
              Monari, lo detto da Modana</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#131">Tiziano
              Veccelli</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#132">Giacinto
              Brandi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#132">Sebastiano
              Bombelli</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#133">Giulio
              Romano</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#133">Iacopo Palma
              Giuniore</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#134">Teodoro
              Van-Loon</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#134">Raffaello
              Sanzio d'Urbino</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#136">Giovanni
              Lanfranchi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#136">Lorenzo
              Pasinelli</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#137">Niccolò
              Poussin</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#138">Federico
              Barocci</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#139">Ercolino del
              Gessi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#139">Domenico
              Zampieri</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#140">Abate Francesco
              Primaticcio</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#140">Bartolomeo
              Manfredi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#141">Quintino
              Messius, o Metsis</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#142">Michelangelo da
              Caravaggio di Casa Amerighi, ovvero Merighi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#143">Giovanni Liss,
              detto volgarmente Par Oldemburhese</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#143">Guido Reni,
              Bolognese</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#144">Enrico Versüre,
              o Veshüring</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#146">Pietro
              Pollajolo</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#146">Monper
              Josse</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#147">Pietro
              Neefs</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#148">Teodoro
              Van-Tulden</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#148">Cavaliere
              Marcantonio Franceschini</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#151">Luca Uden</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#151">Antonio
              Bellucci</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#153">Alessandro
              Turco, detto l'orbetto di Verona</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#153">Melchiore Roos,
              detto il Roosa di Francfort</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#154">Bonaventura
              Preti, o Petri comunemente detti Peters</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#154">Carlo Emanuelle
              Biset</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#155">Agostino
              Caracci</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#156">Jacopo da
              Ponte, detto il Bassano</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#156">Rulando
              Saveri</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#157">Ossembek</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#158">Pietro Paolo
              Rubens</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#159">Melchiorre
              Hondekoeter</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#160">Giovanni David
              de Heem</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#161">Battista
              Zelotti</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#162">Filippo Roos,
              detto comunemente il Rosa di Tivoli</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#162">Rosalba
              Carriera</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#163">Antonio
              Goeboux</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#164">Giovanni
              Bokhorst, sovrannominato Langhen o Lagnian</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#164">Abate D.
              Domenico Martinelli Lucchese</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#165">Andrea
              Lanzano</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#165">Padre Daniel
              Seghers, o Zegers d'Anversa</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#166">Padre Andrea
              Pozzi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#167">Giuseppe
              Martino Geeraerts</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#171">Adriano Van der
              Venne</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#172">Carlo
              Rodart</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#172">Gasparo
              Poussein</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#173">Pietro
              Breughel</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#173">Rembrant Van
              Ryn, o Van Rein</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#175">Pietro
              Snayers</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#175">Simon
              Cantarini</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#176">Adriano
              Stalbent</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#177">Cavalier
              Goffred Kneller</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#178">Giovanni
              Battista Weeninx</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#179">Giovanni Fyt,
              overo Fayt</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#180">Antonio
              Caracci</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#180">Andrea
              Lanzano</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#181">Antonio del
              Sole, detto Antonio dei Paesi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#181">Pietro
              Vannucci, detto il Perugino</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#182">Francesco
              Mazzuoli, detto il Parmigianino</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#183">Guido Canlassi,
              detto il Cagnacci</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#183">Gian Giuseppe
              dal Sole</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#184">Pietro Laar, o
              Laer, detto il Bamboccio</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#185">Luca
              Kranach</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#185">Luca Hugens,
              detto Luca di Leidenm altrimenti detto d'Olanda</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#186">Paolo Brill</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#187">David
              Teniers</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#188">David Teniers
              Giuniore</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#188">Polidoro
              Caldara da Caravaggio</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#189">Adriano Van
              Ostade</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#190">Niccolò
              Bergheim</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#191">Dirk Van
              Bergen</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#192">Giovanni
              Guglielmo Bauer, ovvero Baur</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#193">Andrea
              Vannucchi, detto del Sarto</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#194">Iacopo Robusti,
              detto il Tintoretto</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#195">Giacomo
              Cavedone</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#196">Cavalier
              Giovanni Francesco Barbieri, pittore da Cento detto
              il Guercino</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#197">Francesco
              Pourbus</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#198">Filippo
              Wouwermans</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#199">Giovanni
              Wowermans</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#199">Adriano
              Honeman</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#200">Giosef. Van -
              Croesbeck</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#200">Luigi de
              Vadder</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#201">Paris
              Bourdon</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#202">Adriano
              Brouwer</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#203">Antonio Van
              Dyk</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#204">Giacomo
              Guglielmo Delft</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#204">Giovanni van
              Hoeck, ovvero Houck</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#205">Francesco
              Albani</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#206">Carlo
              Maratti</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#207">Luca Giordano,
              detto Luca Fapiresto</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#207">Giovanni
              Calcar</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#208">Leonardo
              Corona</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#209">Pietro
              Berrettini, detto Pietro da Cortona</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#209">Domenico
              Beccafumi, detto Mecherino</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#210">Andrea
              Sacchi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#211">Giacomo
              Cortesi, detto il Borgognone</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#212">Pietro
              Testa</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#212">Francesca
              Brizio</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#213">Allegri Antonio
              detto Coreggio</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#214">Gioseffo
              Ribera, detto lo Spagnoletto</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#215">Lorenzo
              Garbieri Bolognese</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#216">Daniello
              Crespi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#216">Giovanni Carlo
              Loth</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#217">Barone Pietro
              Strudel</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#217">Bredal il
              Vecchio</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#218">Giovanni
              Giorgio Hamilton</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#218">Anton
              Faisteberger</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#219">Francesco
              Verner-Tam</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#220">Erasmo
              Guellino</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#220">Giovanni Van
              Der Ulft</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#221">Cornelio
              Inasens</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#222">Giovanni Van
              Aken</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#223">Giovanni
              Batista Hagen</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#223">Francesco
              Sneyders, o Snyders</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#224">Cornelio
              Bruyn</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#226">Adamo
              Elsheimer</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#226">Samuele
              Hofman</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#227">Corrado
              Mayer</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#228">Eramo Fischer
              detto il Shwanefeld, o l'Eremita</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#228">Giovanni
              Burgmayer, o Birckmayer</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#229">Giorgio
              Dorfmeister</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#230">Breidel</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#230">Cristoforo
              Amberger</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#231">Giovanni
              Veenix</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#231">Pietro Boel</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#232">Pietro
              Van-Overschee</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#233">Accker</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#233">Aenvanik</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#233">Brodus, o
              Broers</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#234">Giovanni
              Spielberg, o Spilberg</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#235">Verendael</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#235">Bartolomeo
              Morillio</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#235">Cristiano
              Seibold</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#236">Francesco De'
              Neve</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#237">Francesco da
              Ponte, il Giuniore, detto il Bassano</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#238">Erardo
              Schoen</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#238">Horremnans</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#238">Pietro
              Gallis</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#239">Gillemans</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#239">Stnis</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#240">Baut, o
              Bout</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#240">Eyberdt van-der
              Poel</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#241">Giuseppe
              Mazza</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#241">Iacopo Antonio
              Ponzanelli</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#242">Domenico
              Cerasoli</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#242">Marcantonio
              Chiarini</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#247">Santino
              Bussi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#248">Don Errico
              Hugfort Vallombrosano</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#250">Lamberto
              Gori</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#251">Antonio
              Fumiani</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#251">Giovanni
              Bologna</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#252">Francesco
              Bonanni</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#252">Baccio
              Bandinelli</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#253">Massimiliano
              Soldani Benzi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#254">carlo Volgar, o
              Vogelauer, detto Carlo de' Fiori</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#254">Michelangelo
              Buonarroti</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#256">Maturino</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#257">Art Van der
              Neer</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#257">Liotard detto
              il Turco</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#261">Domenico
              Brandi</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#261">Vincenzo Del
              Fiore</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#262">Alessandro
              Cristiano</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#263">Gian
              Giuliano</a>
            </li>
            <li>
              <a href="katmwie48023670s.html#265">Imola</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katmwie48023670s.html#266">Tavola de' nomi
          degli autori citati, che hanno descritte le vite de'
          pittori, e scultori con particolar annotazioni delle loro
          opere</a>
        </li>
        <li>
          <a href="katmwie48023670s.html#268">Indice di tutti i
          nomi de' pittori delle di cui famose opere è formata la
          Galleria</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
