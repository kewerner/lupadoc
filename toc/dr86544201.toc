<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dr86544201s.html#6">Interpretatio obeliscorum
      urbis ad Gregorium XVI Pontificem Maximum</a>
      <ul>
        <li>
          <a href="dr86544201s.html#8">[Dedicatio]</a>
        </li>
        <li>
          <a href="dr86544201s.html#14">Praefatio</a>
        </li>
        <li>
          <a href="dr86544201s.html#26">Interpretatio Obelisci
          Lateranensis ›Obelisco Lateranense‹</a>
          <ul>
            <li>
              <a href="dr86544201s.html#28">Facies australis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#39">Facies orientalis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#43">Facies borealis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#46">Facies occidentalis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#51">Facies australis.
              Columna sinistrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#58">Facies australis.
              Columna dextrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#62">Facies orientalis.
              Columna sinistrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#67">Facies orientalis.
              Columna dextrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#70">Facies borealis.
              Columna sinistrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#73">Facies borealis.
              Columna dextrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#76">Facies occidentalis.
              Columna sinistrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#79">Facies occidentalis.
              Columna dextrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#84">Summa rerum quae in
              duodecim explanatis inscriptionibus continentur</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr86544201s.html#88">Interpretatio Obelisci
          Flamini ›Obelisco Flaminio‹</a>
          <ul>
            <li>
              <a href="dr86544201s.html#90">Facies occidentalis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#106">Facies occidentalis.
              Columna dextrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#107">Facies occidentalis.
              Columna sinistrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#109">Facies australis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#116">Facies australis.
              Columna dextrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#117">Facies australis.
              Columna sinistrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#117">Facies borealis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#123">Facies borealis.
              Columna dextrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#124">Facies borealis.
              Columna sinistrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#125">Facies orientalis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#130">Facies orientalis.
              Columna dextrorsum</a>
            </li>
            <li>
              <a href="dr86544201s.html#134">Summa rerum quae in
              duodecim explanatis inscriptionibus continentur</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr86544201s.html#138">Interpretatio Obelisci
          Matthaeiani ›Obelisco Mattei a Villa Celimontana‹</a>
          <ul>
            <li>
              <a href="dr86544201s.html#140">Facies occidentalis.
              Pyramidion</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr86544201s.html#142">Interpretatio Obelisci
          Mahutaei</a>
          <ul>
            <li>
              <a href="dr86544201s.html#144">Facies australis</a>
            </li>
            <li>
              <a href="dr86544201s.html#145">Facies
              orientalis</a>
            </li>
            <li>
              <a href="dr86544201s.html#145">Facies
              occidentalis</a>
            </li>
            <li>
              <a href="dr86544201s.html#146">Facies borealis</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr86544201s.html#148">Interpretatio Obelisci
          Campensis ›Obelisci: Iseum Campense‹&#160;</a>
          <ul>
            <li>
              <a href="dr86544201s.html#150">Facies occidentalis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#152">Facies australis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#154">Facies orientalis.
              Pyramidion</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr86544201s.html#156">Interpretatio Obelisci
          Minervei ›Elefante della Minerva‹</a>
          <ul>
            <li>
              <a href="dr86544201s.html#158">Facies
              occidentalis</a>
            </li>
            <li>
              <a href="dr86544201s.html#160">Facies australis</a>
            </li>
            <li>
              <a href="dr86544201s.html#161">Facies
              orientalis</a>
            </li>
            <li>
              <a href="dr86544201s.html#161">Facies borealis</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr86544201s.html#164">Interpretatio Obelisci
          Pamphilii [›Obelisco agonale‹]</a>
          <ul>
            <li>
              <a href="dr86544201s.html#166">Facies borealis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#169">Facies australis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#171">Facies orientalis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#173">Facies occidentalis.
              Pyramidion</a>
            </li>
            <li>
              <a href="dr86544201s.html#176">Summa rerum quae in
              duodecim explanatis inscriptionibus continentur</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr86544201s.html#178">Interpretatio
          Obeliscorum Beneventanorum</a>
          <ul>
            <li>
              <a href="dr86544201s.html#180">A Obeliscus
              Beneventanus</a>
              <ul>
                <li>
                  <a href="dr86544201s.html#180">I. Facies</a>
                </li>
                <li>
                  <a href="dr86544201s.html#181">II. Facies</a>
                </li>
                <li>
                  <a href="dr86544201s.html#183">III. Facies</a>
                </li>
                <li>
                  <a href="dr86544201s.html#183">IV. Facies</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dr86544201s.html#184">B Obeliscus
              Beneventanus</a>
              <ul>
                <li>
                  <a href="dr86544201s.html#184">I. Facies</a>
                </li>
                <li>
                  <a href="dr86544201s.html#185">II. Facies</a>
                </li>
                <li>
                  <a href="dr86544201s.html#185">III. Facies</a>
                </li>
                <li>
                  <a href="dr86544201s.html#186">IV. Facies</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dr86544201s.html#188">Summa rerum quae in
              quator inscriptionibus utroque obeslico repetitis
              continentur</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr86544201s.html#190">Interpretatio Obelisci
          Barberini [›Obelisco del Pincio‹]</a>
          <ul>
            <li>
              <a href="dr86544201s.html#192">Facies borealis</a>
            </li>
            <li>
              <a href="dr86544201s.html#198">Facies
              orientalis</a>
            </li>
            <li>
              <a href="dr86544201s.html#204">Facies australis</a>
            </li>
            <li>
              <a href="dr86544201s.html#208">Facies
              occidentalis</a>
            </li>
            <li>
              <a href="dr86544201s.html#214">Summa rerum quae in
              duodecim explanatis inscriptionibus continentur</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr86544201s.html#218">Indices</a>
        </li>
        <li>
          <a href="dr86544201s.html#243">[Signa] ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
