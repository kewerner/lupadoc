<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy3036203s.html#6">Nuova descrizione del Vaticano,
      o sia Della Sacrosanta Basilica di S. Pietro; Tomo III.</a>
      <ul>
        <li>
          <a href="dy3036203s.html#8">A Monsignore Giovanni
          Battista Rezzonico</a>
        </li>
        <li>
          <a href="dy3036203s.html#14">Prefazione</a>
        </li>
        <li>
          <a href="dy3036203s.html#18">Indice</a>
        </li>
        <li>
          <a href="dy3036203s.html#24">Introduzione</a>
        </li>
        <li>
          <a href="dy3036203s.html#58">Nuova descrizione del
          Vaticano</a>
        </li>
        <li>
          <a href="dy3036203s.html#58">I. Del gran Corridore
          della Cleopatra, e di alcune Abitazioni, alle quali in
          principio del medesimo si a l'ingresso</a>
        </li>
        <li>
          <a href="dy3036203s.html#72">II. Della Libreria
          Vaticana, e contigua Abitazione di Monsignore Primo
          Custode della medesima</a>
        </li>
        <li>
          <a href="dy3036203s.html#140">III. Dell'Archivio
          Segreti di Sua Santità</a>
        </li>
        <li>
          <a href="dy3036203s.html#162">IV. Continuazione del
          Gran Corridore della Cleopatra, e sue contigue
          Abitazioni</a>
        </li>
        <li>
          <a href="dy3036203s.html#171">V. Della famosa Scala
          grande a Lumaca di Bramante, e di alcune Abitazioni ad
          essa contigue</a>
        </li>
        <li>
          <a href="dy3036203s.html#178">VI. Cortile delle Statue,
          per cui si passa a diverse Abitazioni ivi contigue</a>
        </li>
        <li>
          <a href="dy3036203s.html#184">VII. Palazzetto
          d'Innocenzio VIII.</a>
        </li>
        <li>
          <a href="dy3036203s.html#203">VIII. Diverse Abitazioni,
          alle quali si ha l'ingresso dal Cortile della Statue</a>
        </li>
        <li>
          <a href="dy3036203s.html#209">IX. Appartamento
          Pontificio di Ritiro, detto di Belvedere, o sia di
          Tor-dè-Venti</a>
        </li>
        <li>
          <a href="dy3036203s.html#234">X. Cortile
          degli'Archivj</a>
        </li>
        <li>
          <a href="dy3036203s.html#254">XI. Del Giardino segreto
          Pontificio</a>
        </li>
        <li>
          <a href="dy3036203s.html#261">XXII. Del Gran Giardino
          Vaticano detto per vocabolo il Boscareccio</a>
        </li>
        <li>
          <a href="dy3036203s.html#313">XIII. Del grande
          Stradone, che vien rinchiuso dalla Facciata laterale del
          Palazzo, e dal muro, che serve di recinto al Giardino
          Boscareccio, con alcune Rimesse, ed altre abitazioni ivi
          esistenti</a>
        </li>
        <li>
          <a href="dy3036203s.html#331">XIV. Cortile della
          Piazzetta della Panetterìa</a>
        </li>
        <li>
          <a href="dy3036203s.html#352">XV. Del Palazzetto, o sia
          Casino della Zecca Pontificia</a>
        </li>
        <li>
          <a href="dy3036203s.html#364">XVI. Del Forno
          Apostolico, ed altre annesse Fabbriche</a>
        </li>
        <li>
          <a href="dy3036203s.html#380">XVII. Del Quartiere della
          Real Guardia Pontificia del Corpo, detta de'
          Cavalleggieri</a>
        </li>
        <li>
          <a href="dy3036203s.html#398">XIX. Quartiere per la
          Guardia Reale delle Corazze Pontificie</a>
        </li>
        <li>
          <a href="dy3036203s.html#399">XX. Delli Acquedotti, e
          Sorgenti d'Acque, che so possedono dal Palazzo
          Apostolico, e sue rispettive Chiaviche&#160;</a>
        </li>
        <li>
          <a href="dy3036203s.html#426">Indice delle Materie
          contenute nel Secondo, e Terzo Volume</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
