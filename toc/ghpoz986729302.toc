<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghpoz986729302s.html#8">Perspectiva Pictorum et
      architectorum&#160;</a>
      <ul>
        <li>
          <a href="ghpoz986729302s.html#16">[Dedica dell'autore
          a Giuseppe I] Alla sacra real maesta' Giuseppe primo re
          di' Romani, e d'Ongaria, arciduca d'Austria, et
          cetera&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#24">[Dedica dell'autore
          ai lettori] Al lettore</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#36">[Istruzione] Breve
          istruzione per dipingere à fresco</a>
          <ul>
            <li>
              <a href="ghpoz986729302s.html#36">Parte prima</a>
              <ul>
                <li>
                  <a href="ghpoz986729302s.html#36">I. Fabricare
                  palchi per dipingere</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#36">II.
                  Arrichiare</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#36">III.
                  Intonacare</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#37">IV.
                  Gravire</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#37">V.
                  Disegnare</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#37">VI.
                  Graticolare</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#37">VII.
                  Ricalcare&#160;</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#30">VIII.
                  Preparare</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ghpoz986729302s.html#30">Parte
              seconda</a>
              <ul>
                <li>
                  <a href="ghpoz986729302s.html#30">IX.
                  Dipingere</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#30">X.
                  Impastare, e caricare</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#30">XI.
                  Ritoccare</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#31">XII.
                  Sfumare, ed intenerire</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#31">XIII.
                  Rifare</a>
                </li>
                <li>
                  <a href="ghpoz986729302s.html#31">XIV.
                  Colorire</a>
                  <ul>
                    <li>
                      <a href="ghpoz986729302s.html#31">Bianco
                      di Calce</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#31">Bianco
                      di scorze d'Uovo</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#32">Bianco
                      di marmo di Carrara</a>
                    </li>
                    <li>
                      <a href=
                      "ghpoz986729302s.html#32">Cinabro</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#32">Vetriolo
                      brugiato</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#32">Rossetto
                      d'Inghilterra</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#32">Terra
                      rossa</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#32">Terra
                      brugiata</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#32">Terra
                      gialla chiara</a>
                    </li>
                    <li>
                      <a href=
                      "ghpoz986729302s.html#33">Giallolino di
                      Fornace</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#33">Pasta
                      verde</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#33">Terra
                      verde</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#33">Terra
                      d'ombra</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#33">Terra
                      d'ombra brugiata</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#33">Terra
                      nera di Venetia</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#33">Terra
                      nera di Roma</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#33">Nero di
                      Carbone</a>
                    </li>
                    <li>
                      <a href=
                      "ghpoz986729302s.html#33">Smaltino</a>
                    </li>
                    <li>
                      <a href=
                      "ghpoz986729302s.html#42">Oltremarino</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#42">Morel di
                      Sale</a>
                    </li>
                    <li>
                      <a href="ghpoz986729302s.html#42">Colori
                      contrarii alla Calce, e che non si possono
                      adoprare nelle pitture à fresco</a>
                    </li>
                    <li>
                      <a href=
                      "ghpoz986729302s.html#42">Dipingere à
                      secco</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghpoz986729302s.html#45">[Text]</a>
      <ul>
        <li>
          <a href="ghpoz986729302s.html#45">I. Come sia
          necessario imparar l'Architettura prima di metterla in
          prospettiva particolarmente che cosa sia pianta
          ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#47">II. Cosa sia
          Facciata ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#49">III. Cosa sia
          Profilo, e spaccato ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#51">IV. Si dimostra in
          un'Uomo che vede quattro pilastri che cosa sia
          prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#53">V. Come i pilastri
          passati si dispongono in pianta, ed in proffilo per
          disegnarli in Prospettiva ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#55">VI. Otto piedistalli
          senza cornici ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#57">VII. Otto
          piedistalli ornati di cornici ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#59">VIII. Sei Colonne in
          circolo ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#61">IX. Otto pilastrelli
          in circolo, con un documento ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#63">X. Quattro
          Piedistalli in mezzo circolo dove si dà un'altro
          documento ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#65">XI. Quadrato
          semplice in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#67">XII. Quadrato doppio
          ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#69">XIII. Circolo
          semplice ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#71">XIV. Tre semicircoli
          ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#73">XV. Tribuna
          d'Architettura ornata ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#75">XVI. Tre piedistalli
          rotondi, con un documento ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#77">XVII. Piedestallo
          quadrato, con un'altro documento ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#79">XVIII. Piedestallo
          rotondo sopra gradini ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#81">XIX. Tre pietre una
          sopra l'altra ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#83">XX. Vasca di
          fontana, dove si propone un dubbio ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#85">XXI. Arco trionfale,
          dove si scioglie il dubbio ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#87">XXII. L'istesso Arco
          veduto da un lato, con un documento ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#89">XXIII. Urna con
          ornamenti ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#91">XXIV. Piedestallo
          con ornamento ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#93">XXV. Sedie ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#95">XXVI. Piedestallo,
          per angolo ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#97">XXVII. Base di
          Colonna ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#99">XXVIII. Due basi
          alzate da una parte ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#101">XXIX. Capitello
          Corinthio per angolo ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#103">XXX. Capitello
          composito per angolo ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#105">XXXI. Capitello
          capriccioso ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#107">XXXII. Cornice
          Toscana ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#109">XXXIII. Cornice
          composita ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#111">XXXIV. Cornice
          Jonica ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#113">XXXV. Cornice
          composta secondo il Palladio ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#115">XXXVI. Fragmenti di
          Architettura ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#117">XXXVII.
          Instructione per i Teatri; come si fanno le piante, e
          profili: e come si trovi il punto ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#119">XXXVIII.
          Instruttione come si graticolano le Scene, e come si
          trova il punto quando sono storte ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#121">XXXIX. Teatro di
          Cortile ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#123">XL. Teatro di
          Arsenale ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#125">XLI. Teatro di
          Galleria ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#127">XLII. Teatro di
          Anticamera ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#129">XLIII. Teatro di
          Tempio ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#131">XLIV. Coliseo
          ›Colosseo‹ ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#133">XLV. Pianta di
          Teatro Sacro fatto in Roma, e come si dispone ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#135">XLVI. Come si fanno
          i pezzi; e le loro misure ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#137">XLVII. Teatro tutto
          intero, e ombreggiato ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#139">XLVIII. Altra
          inventione per l'istesso effetto ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#141">XLIX. Istruttione,
          per fare le Cupole di sotto in sù ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#143">L. Cupola in
          piccolo di sotto in sù ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#145">LI. Cupola del
          Collegio Romano ›Sant'Ignazio‹, con la regola del primo
          Tomo ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#147">LII. Cupola del
          Collegio Romano ›Sant'Ignazio‹ con la presente regola
          ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#149">LIII. Cupola del
          Collegio Romano ›Sant'Ignazio‹ ombreggiata ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#151">LIV. Cupola di
          diversa figura ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#153">LV. Un pezzo di
          Architettura di sotto in sù per linee rette ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#155">LVI. Soffitti
          bislunghi, e suo auvertimento ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#157">LVII. Pianta di
          Architettura per un soffito bislungo ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#159">LVIII. Elevatione
          della passata a pianta del soffitto bislungo ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#161">LIX. Prospettiva di
          sotto in sù delle passate figure ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#163">LX. Altare di
          Sant'Ignatio ›Sant'Ignazio‹ fabricato in Roma ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#165">LXI. Pianta, ed
          elevatione dell'Altare di Sant'Ignatio ›Sant'Ignazio‹
          ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#167">LXII. Altare del
          Beato Luigi, fabricato nelle Chiesa di Sant'Ignatio
          ›Sant'Ignazio‹ del Collegio Romano ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#169">LXIII. Pianta, e
          elevatione del passato Altare ›Sant'Ignazio‹ ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#171">LXIV. Altro Altare
          per il Beato Luigi, con due colonne ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#173">LXV. Un'altro
          Altare per l'istesso effetto alquanto mutato ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#175">LXVI. Pianta, ed
          elevatione del passato disegno ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#177">LXVII. Altare
          dipinto nella Chiesa de Collegio Romano ›Sant'Ignazio‹
          ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#179">LXVIII. Pianta, e
          elevatione del passato disegno ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#181">LXIX. Altare
          dipinto in Frascati ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#183">LXX. Pianta ed
          Elevatione del passato disegno ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#185">LXXI. Altare
          Maggiore per il Giesù di Roma ›Santissimo Nome di Gesù‹
          ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#187">LXXII. Pianta ed
          elevatione del passato disegno ›Santissimo Nome di Gesù‹
          ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#189">LXIII. Un'altro
          Altare maggiore, per l'istesso luogo ›Santissimo Nome di
          Gesù‹ ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#191">LXXIV. Pianta, e
          profilo del secondo disegno ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#193">LXXV. Altare
          capriccioso ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#195">LXXVI. Pianta, ed
          elevatione del passato disegno ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#197">LXXVII. Altare
          fatto à Verona ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#199">LXXVIII. Pianta, ed
          elevatione del passato ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#201">LXXIX. Altro Altare
          ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#203">LXXX. Prospettiva
          del passato ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#205">LXXXI. Altar
          maggiore della Chiesa di Sant'Ignatio ›Sant'Ignazio‹ nel
          Collegio Romano ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#207">LXXXII. Pianta ed
          elevatione in profilo del passato disegno ›Sant'Ignazio‹
          ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#209">LXXXIII. [LXXXIV.;
          LXXXV.; LXXXVI.; LXXXVII.] Facciate di San Giovanni
          Laterano ›San Giovanni in Laterano‹ ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#215">LXXXVIII. [LXXXIX.;
          XC.] Pianta d'una Chiesa rotonda ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#219">XCI. Facciata in
          prospettiva della passata Chiesa ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#221">XCII. [XCIII.;
          XCIV.; XCV.; XCVI.] Chiesa di figura lunga, cioè San
          Fedele di Milano, sua pianta, elevatione interiore
          ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#227">XCVII. [XCVIII.;
          XCIX.; C.; CI.; CII.; CIII.; CIV.; CV.] Porte, e Finestre
          ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#237">CVI. [CVII.;
          CVIII.] Mensole ornate ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#241">CIX. Pianta, ed
          elevatione esteriore della fabbrica di un Collegio ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#243">CX. Spaccato per
          lungo, e per largo dell'istesso Collegio ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#245">CXI. [CXII.;
          CXII.]Scale ▣</a>
        </li>
        <li>
          <a href="ghpoz986729302s.html#249">CXIV. [CXV.; CXVI.;
          CXVII.; CVIII.] Fortificationi ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghpoz986729302s.html#258">Indice</a>
    </li>
  </ul>
  <hr />
</body>
</html>
