<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="esas1423840s.html#6">Sposizione delle pitture in
      muro del ducale palazzo nella nobil terra di Sassuolo</a>
      <ul>
        <li>
          <a href="esas1423840s.html#8">Altezza Serenissima</a>
        </li>
        <li>
          <a href="esas1423840s.html#10">&#160;[Sposizione delle
          pitture in muro del ducale palazzo nella nobil terra di
          Sassuolo]</a>
        </li>
        <li>
          <a href="esas1423840s.html#166">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
