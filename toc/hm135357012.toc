<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="hm135357012s.html#8">La nuova galleria ovvero
      Cento racconti curiosi e piacevoli, tratti da cento pitture.
      Parte I&#160;</a>
      <ul>
        <li>
          <a href="hm135357012s.html#10">Illustrissimo Signor
          Conte</a>
        </li>
        <li>
          <a href="hm135357012s.html#14">Indice&#160;</a>
        </li>
        <li>
          <a href="hm135357012s.html#16">L'autore a chi
          legge</a>
        </li>
        <li>
          <a href="hm135357012s.html#18">[Testo]&#160;</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="hm135357012s.html#172">La nuova galleria overo
      Cento racconti curiosi e piacevoli, tratti da cento pitture.
      Parte II</a>
      <ul>
        <li>
          <a href="hm135357012s.html#174">Illustrissima Signora
          Contessa</a>
        </li>
        <li>
          <a href="hm135357012s.html#177">Indice</a>
        </li>
        <li>
          <a href="hm135357012s.html#180">[Testo]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
