<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502700as.html#6">Wegzeiger zu dem
      wunderbarlichen Sachen der heiligen Stat Rom, furnemlich zu
      den Siben in der gantzen Christenheit hochberùmbten, aus
      ihren dreihundert und funfzig, Kirchen</a>
    </li>
    <li>
      <a href="dg4502700as.html#8">Von der walfartgen Rom und
      ihrer Vorbereitung zu den siben Kirchen</a>
      <ul>
        <li>
          <a href="dg4502700as.html#11">›Santa Maria dell'Anima‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502700as.html#13">›Santa Maria della
          Pace‹</a>
        </li>
        <li>
          <a href="dg4502700as.html#15">Die Engelbug [sic]
          ›Castel Sant'Angelo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502700as.html#19">In Sancto Petro dat
          schiff ›San Pietro in Vaticano: Mosaico della Navicella‹
          ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502700as.html#19">Die Namen der siben
      Kirchen</a>
      <ul>
        <li>
          <a href="dg4502700as.html#20">I. Sanctus Petrus ›San
          Pietro in Vaticano‹</a>
          <ul>
            <li>
              <a href="dg4502700as.html#21">Porta Sancta ›Porta
              Santa‹</a>
            </li>
            <li>
              <a href="dg4502700as.html#25">Sanctae Veronicae
              Shcueistuch</a>
            </li>
            <li>
              <a href="dg4502700as.html#26">Das Heilige
              Creutz</a>
            </li>
            <li>
              <a href="dg4502700as.html#26">Die Heilige
              Lantze</a>
            </li>
            <li>
              <a href="dg4502700as.html#29">›San Pietro in
              Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502700as.html#31">Sancti Petri ›San
              Pietro in Vaticano: Baldacchino di San Pietro‹ ▣</a>
            </li>
            <li>
              <a href="dg4502700as.html#33">Sanctus Andreas
              Sancti Petri Brudeb [sic] ›San Pietro in Vaticano:
              Statua di Sant'Andrea‹</a>
            </li>
            <li>
              <a href="dg4502700as.html#45">Von dem Pabstlichen
              Pallast ›Palazzo Apostolico Vaticano‹</a>
            </li>
            <li>
              <a href="dg4502700as.html#46">Von Sancto Petro ›San
              Pietro in Vaticano‹ zu sancto Paulo ›San Paolo fuori
              le Mura‹</a>
              <ul>
                <li>
                  <a href="dg4502700as.html#49">Sancta Maria de
                  Scala ›Santa Maria della Scala‹</a>
                </li>
                <li>
                  <a href="dg4502700as.html#50">Sanctae Mariae de
                  Monte Carmelo ›Convento dei Carmelitani‹ und S.
                  Aegidio ›Sant'Egidio‹</a>
                </li>
                <li>
                  <a href="dg4502700as.html#54">Sanctae Caeciliae
                  ›Santa Cecilia in Trastevere‹</a>
                  <ul>
                    <li>
                      <a href="dg4502700as.html#55">Cubiculum et
                      Oratorium Sanctae Caeciliae ›Santa Cecilia in
                      Trastevere: Calidarium‹</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502700as.html#56">Ursach der
                  Tiberinischen inset [sic: per Insel] ›Isola
                  Tiberina‹</a>
                </li>
                <li>
                  <a href="dg4502700as.html#57">Sanctus
                  Bartholomaeus ›San Bartolomeo all'Isola‹</a>
                </li>
                <li>
                  <a href="dg4502700as.html#59">Sancta Maria in
                  Cosmedin ›Santa Maria in Cosmedin‹</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502700as.html#65">II. Sanct Paul ›San Paolo
          fuori le Mura‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502700as.html#72">Aus Sankt Paul ›San
              Paolo fuori le Mura‹ zu seinen heilige drei Brunnen
              ›San Paolo alle Tre Fontane‹ und daher zu Sankt
              Sebastian ›San Sebastiano‹</a>
            </li>
            <li>
              <a href="dg4502700as.html#73">Zu Sancti Pauli
              Brunnen ›San Paolo alle Tre Fontane‹</a>
            </li>
            <li>
              <a href="dg4502700as.html#76">Sancta Maria
              Annunciata ›Annunziatella‹, unser Lieben Frawen
              verkundigung Kirch</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502700as.html#77">III. Sanctus Sebastianus
          ›San Sebastiano‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502700as.html#85">Von Sancti Sebastiani
              leben, leiden, sterben, wunder werck und grosser ehr
              die ihm von den Romanern erzeigt wird, seinen
              andachtigen zuliebe</a>
            </li>
            <li>
              <a href="dg4502700as.html#87">Sancto Sebastiano
              martyri depulsori pestilitatis</a>
            </li>
            <li>
              <a href="dg4502700as.html#88">Der vueg von Sancto
              Sebastiano ›San Sebastiano‹ zu Sanctus Ioannes in
              Laterano ›San Giovanni in Laterano‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502700as.html#94">IV. Sanctus Ioannes in
          Laterano ›San Giovanni in Laterano‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502700as.html#104">Die Heilige Stiege
              ›Scala Santa‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502700as.html#110">V. Zum Heiligen Creutz
          in Ierusalem ›Santa Croce in Gerusalemme‹ ▣</a>
        </li>
        <li>
          <a href="dg4502700as.html#120">Auf Carfreitag [sic],
          vvan die Station in sanct Creutz [sic] in Ierusalem ist
          auch zu dem Crucifix sanctae Birgittae ›San Paolo fuori
          le Mura: Crocifisso‹ ▣, in sancti Pauli kirch [sic] die
          gròsseste andacht</a>
        </li>
        <li>
          <a href="dg4502700as.html#121">VI. Sanct Laurentz ›San
          Lorenzo fuori le Mura‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502700as.html#122">Sancti Laurentii
              acht Kirchen zu Rom</a>
              <ul>
                <li>
                  <a href="dg4502700as.html#122">Sancti Laurentij
                  in fonte ›San Lorenzo in Fonte‹</a>
                </li>
                <li>
                  <a href="dg4502700as.html#125">Sancti Laurentij
                  in Paneperna ›San Lorenzo in Panisperna‹</a>
                </li>
                <li>
                  <a href="dg4502700as.html#125">[›San Lorenzo
                  fuori le Mura‹]</a>
                </li>
                <li>
                  <a href="dg4502700as.html#125">Sancti Laurentij
                  in Damaso ›San Lorenzo in Damaso‹</a>
                </li>
                <li>
                  <a href="dg4502700as.html#125">Sancti Laurentij
                  in Lucina ›San Lorenzo in Lucina‹</a>
                </li>
                <li>
                  <a href="dg4502700as.html#126">Sancti Laurentij
                  Miranda ›San Lorenzo in Miranda‹</a>
                </li>
                <li>
                  <a href="dg4502700as.html#126">Sanctus
                  Laurentius in Burgo ›San Lorenzo in Piscibus‹</a>
                </li>
                <li>
                  <a href="dg4502700as.html#126">Laurentiolus
                  ›San Lorenzo de Ascesa ai Monti‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502700as.html#128">Coemeterio Sancte
              Cyriacae ›Catacombe di Ciriaca‹</a>
            </li>
            <li>
              <a href="dg4502700as.html#131">Nazareth</a>
            </li>
            <li>
              <a href="dg4502700as.html#136">Nazareth Lauretum
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502700as.html#139">VII. Sancta Maria Maior
          ›Santa Maria Maggiore‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502700as.html#151">Cappella
              Buaghesiorum [sic: per Burghesiorum] ›Cappella
              Paolina‹</a>
            </li>
            <li>
              <a href="dg4502700as.html#152">Grabschrift Pauli
              des Funften ›Sepolcro di Paolo V‹</a>
            </li>
            <li>
              <a href="dg4502700as.html#158">sanctae Praxedis
              ›Santa Prassede‹ der iungfrauen kirch [sic], da [...]
              in einer gar andachtigen schònen cappellen [sic] die
              heilige saul an vuelcher Christus unser Herr
              gegeislet vuorden &#160;›Cappella di San Zenone‹
              ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502700as.html#160">Wo nfn [sic: per nun] alle
      Aposteln</a>
    </li>
    <li>
      <a href="dg4502700as.html#163">Sancta Maria de Anima ›Santa
      Maria dell'Anima‹</a>
    </li>
    <li>
      <a href="dg4502700as.html#164">[Brevi biografie dei
      pontefici: Innocenzo X, Alessandro VII, Clemente IX]</a>
    </li>
    <li>
      <a href="dg4502700as.html#165">Ritratto della miracolosa
      chiave della santa casa di Loreto che è nel monasterio di
      Farfa ▣</a>
    </li>
  </ul>
  <hr />
</body>
</html>
