<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501010s.html#6">[In isto opusculo dicitur
      ...]</a>
      <ul>
        <li>
          <a href="dg4501010s.html#6">Roma civitas sancta caput
          mundi&#160;</a>
        </li>
        <li>
          <a href="dg4501010s.html#28">Oratio de sancta
          Veronica</a>
        </li>
        <li>
          <a href="dg4501010s.html#78">Stationes in
          quadragesima</a>
        </li>
        <li>
          <a href="dg4501010s.html#80">Stationes post pascha</a>
        </li>
        <li>
          <a href="dg4501010s.html#81">Item in quartum tempus
          mensis septembris [sic!]&#160;</a>
        </li>
        <li>
          <a href="dg4501010s.html#81">Stationes in adventum</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
