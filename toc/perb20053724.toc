<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="perb20053724s.html#6">Giulio Mancini : Viaggio per
      Roma</a>
      <ul>
        <li>
          <a href="perb20053724s.html#9">Inhalt</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="perb20053724s.html#10">Einleitung</a>
      <ul>
        <li>
          <a href="perb20053724s.html#10">I. Teil : Leben und
          Schriften Mancinis</a>
          <ul>
            <li>
              <a href="perb20053724s.html#12">I. Geschichte der
              Manciniforschung</a>
            </li>
            <li>
              <a href="perb20053724s.html#17">II. Das Leben
              Mancinis</a>
            </li>
            <li>
              <a href="perb20053724s.html#21">III. Die Schriften
              Mancinis</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="perb20053724s.html#26">II. Teil : Der «Viaggio
          di Roma»</a>
          <ul>
            <li>
              <a href="perb20053724s.html#28">I. Überlieferung
              und Handschriften</a>
            </li>
            <li>
              <a href="perb20053724s.html#30">II. Die Datierung
              des «Viaggio»</a>
            </li>
            <li>
              <a href="perb20053724s.html#32">III. Die Vorläufer
              Mancinis</a>
            </li>
            <li>
              <a href="perb20053724s.html#44">IV. Die Stellung
              Mancinis zu seinen Vorgängern</a>
            </li>
            <li>
              <a href="perb20053724s.html#47">V. Die Bedeutung
              Mancinis</a>
            </li>
            <li>
              <a href="perb20053724s.html#50">VI. Die Nachfolger
              Mancinis</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="perb20053724s.html#56">Giulio Mancini : Viaggio
      per Roma per vedere le pitture, che si ritrovano in essa</a>
    </li>
    <li>
      <a href="perb20053724s.html#116">Beilagen</a>
      <ul>
        <li>
          <a href="perb20053724s.html#118">Beilage I.
          Bibliographie der Codices Manciniani</a>
        </li>
        <li>
          <a href="perb20053724s.html#123">Beilage II.
          Bibliographie der römischen Guiden von 1541-1674</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="perb20053724s.html#140">Anhang</a>
      <ul>
        <li>
          <a href="perb20053724s.html#142">Verzeichnis der
          benutzten Literatur</a>
        </li>
        <li>
          <a href=
          "perb20053724s.html#148">Personenverzeichnis</a>
        </li>
        <li>
          <a href="perb20053724s.html#154">Ortsverzeichnis</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
