<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghmil36833711ds.html#6">Trattato Completo,
      Formale E Materiale Del Teatro</a>
      <ul>
        <li>
          <a href="ghmil36833711ds.html#8">Prefazione</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#10">Idea dell'opera</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#12">I. Del Teatro in
          generale</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#20">II. Della
          tragedia</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#30">III. Della
          commedia</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#41">IV. Della
          pastorale</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#43">V. Dell'opera</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#46">VI. Dell'argomento
          del dramma in musica</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#50">VII. Della
          musica</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#63">VIII. Degli
          attori</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#65">IX. De' balli</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#74">X. Della
          decorazione</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#77">XI. Del teatro
          materiale</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#103">XII. Cause de'
          difetti del teatro, e mezzi per ristabilirlo</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#106">Spiegazioni delle
          tavole</a>
        </li>
        <li>
          <a href="ghmil36833711ds.html#110">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
