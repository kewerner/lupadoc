<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghgra57411160s.html#8">Francisci Marii Grapaldi De
      partibus aedium</a>
      <ul>
        <li>
          <a href="ghgra57411160s.html#10">Tabula</a>
        </li>
        <li>
          <a href="ghgra57411160s.html#51">Liber I.</a>
          <ul>
            <li>
              <a href="ghgra57411160s.html#52">I. Parietes</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#62">II. Cavedium</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#68">III. Apotheca</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#76">IIII. [sic]
              Penarium</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#87">V. Hortus</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#112">VI. Piscina</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#123">VII.
              Leporarium</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#132">VIII.
              Stabulum</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#146">VIIII. [sic]
              Aviarium</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#161">X. Balnearia
              Cella</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghgra57411160s.html#168">Liber II.</a>
          <ul>
            <li>
              <a href="ghgra57411160s.html#169">I. Scalae
              contignatio pauimenta proce strium&#160;</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#172">II. Basilica</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#174">III.
              Coenaculum</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#216">IIII. [sic]
              Coquina</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#233">V. Gynoecium</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#240">VI. Cubiculum</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#263">VII.
              Valitudinarium</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#273">VIII.
              Sacellum</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#275">VIIII. [sic]
              Bibliotheca</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#285">V.
              Armamentarium</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#299">XI. Granarium</a>
            </li>
            <li>
              <a href="ghgra57411160s.html#309">XII. Turris et
              Tectum</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghgra57411160s.html#316">Francisci Marii
          Grapaldi poetae laureati de verborum explicatione quae in
          librode partibus aedium cum tinentur</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
