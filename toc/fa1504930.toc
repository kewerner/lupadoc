<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="fa1504930s.html#6">Diario di Monsig. Lorenzo
      Azzolini</a>
      <ul>
        <li>
          <a href="fa1504930s.html#12">[Text]</a>
        </li>
        <li>
          <a href="fa1504930s.html#32">Fonti</a>
        </li>
        <li>
          <a href="fa1504930s.html#34">Abozzo di relatione del
          viaggio del Card. Legato da Madrid a Roma</a>
        </li>
        <li>
          <a href="fa1504930s.html#62">Appendice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
