<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dr8653970s.html#6">De origine et usu obeliscorum ad
      Pium Sextum Pontificem Maximum</a>
      <ul>
        <li>
          <a href="dr8653970s.html#8">[Dedicatio]</a>
        </li>
        <li>
          <a href="dr8653970s.html#10">Praefatio</a>
        </li>
        <li>
          <a href="dr8653970s.html#13">Testimonium</a>
        </li>
        <li>
          <a href="dr8653970s.html#14">Synopsis Operis</a>
        </li>
        <li>
          <a href="dr8653970s.html#42">Index rerum memorabilium
          numeri paginas indicant</a>
        </li>
        <li>
          <a href="dr8653970s.html#53">Series peregrinatorum in
          Aegypto et Abessinia, quorum libri passim adducuntur</a>
        </li>
        <li>
          <a href="dr8653970s.html#54">1. Sectio Prima. Veterum de
          obeliscis et de stelis Aegyptiis testimonia</a>
          <ul>
            <li>
              <a href="dr8653970s.html#55">I. De Obeliscis ex
              auctoribus Graecis atque Latinis</a>
            </li>
            <li>
              <a href="dr8653970s.html#85">II. De stelis Aegyptiis
              ex auctoribus Graecis et Latinis</a>
            </li>
            <li>
              <a href="dr8653970s.html#104">III. Vetera
              Obeliscorum epigrammata</a>
            </li>
            <li>
              <a href="dr8653970s.html#109">IV. Monumenta in
              quibus expressi cernuntur obelisci</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr8653970s.html#118">2. Sectio Secunda.
          Enarratio obeliscorum Aegyptiorum qui hodie vel integri
          vel ailqua sui parte superstites offenduntur</a>
          <ul>
            <li>
              <a href="dr8653970s.html#119">I. Obelisci veteres
              Romae existentes</a>
            </li>
            <li>
              <a href="dr8653970s.html#136">II. Obelisci in
              Europae provinciis extra Urbem superstites</a>
            </li>
            <li>
              <a href="dr8653970s.html#145">III. Obelisci hodie
              exstantes in Aegypto et in Aethiopia</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr8653970s.html#180">3. Sectio Tertia. De usu
          Obeliscorum in Aegypto</a>
          <ul>
            <li>
              <a href="dr8653970s.html#180">I. De nomine
              obelisci</a>
            </li>
            <li>
              <a href="dr8653970s.html#185">II. De figura
              obeliscorum</a>
            </li>
            <li>
              <a href="dr8653970s.html#193">III. De materie e qua
              facti obelisci</a>
            </li>
            <li>
              <a href="dr8653970s.html#197">IV. De magnitudine
              obeliscorum</a>
            </li>
            <li>
              <a href="dr8653970s.html#204">V. De situ
              obeliscorum</a>
            </li>
            <li>
              <a href="dr8653970s.html#209">VI. Quo fine erecti
              fuerint obelisci</a>
            </li>
            <li>
              <a href="dr8653970s.html#228">VII. De argumento
              scalpturarum in obeliscis</a>
            </li>
            <li>
              <a href="dr8653970s.html#237">VIII. De mechanica
              obeliscorum</a>
            </li>
            <li>
              <a href="dr8653970s.html#245">Obeliscus Borgianus
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr8653970s.html#246">4. Sectio Quarta. De
          origine obeliscorum</a>
          <ul>
            <li>
              <a href="dr8653970s.html#246">I. De monumentorum
              instituto</a>
            </li>
            <li>
              <a href="dr8653970s.html#476">II. Litterarum apud
              Aegyptios usus et origo</a>
            </li>
            <li>
              <a href="dr8653970s.html#624">III. De stelis
              Aegyptiis atque de obeliscis originem trahentibus a
              stelis</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr8653970s.html#649">5. Sectio Quinta. De
          historia obeliscorum</a>
          <ul>
            <li>
              <a href="dr8653970s.html#649">I. Prima obeliscorum
              epocha</a>
            </li>
            <li>
              <a href="dr8653970s.html#659">II. Secunda epocha
              obeliscorum</a>
            </li>
            <li>
              <a href="dr8653970s.html#662">III. Tertia
              obeliscorum epocha</a>
            </li>
            <li>
              <a href="dr8653970s.html#676">IV. Quarta obeliscorum
              epocha</a>
            </li>
            <li>
              <a href="dr8653970s.html#692">V. Synopsis
              chronologica obeliscorum</a>
            </li>
            <li>
              <a href="dr8653970s.html#697">Obeliscus Beneventanus
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dr8653970s.html#698">Corrigenda et Addenda</a>
        </li>
        <li>
          <a href="dr8653970s.html#708">Fragmentum italianae ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
