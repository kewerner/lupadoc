<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katpmon135551002s.html#14">The Mond
      Collection</a>
      <ul>
        <li>
          <a href="katpmon135551002s.html#16">Contents</a>
        </li>
        <li>
          <a href="katpmon135551002s.html#22">List of
          illustrations</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="katpmon135551002s.html#28">[Text]</a>
      <ul>
        <li>
          <a href="katpmon135551002s.html#28">Leonardo Da Vinci
          and the Lombard School</a>
          <ul>
            <li>
              <a href="katpmon135551002s.html#30">Leonardo Da
              Vinci, Head of the Virgin</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#36">Leonardo
                  da Vinci, Virgin and Child with Saint Anne ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#45">Luini, Madonna
              and child with Saint John</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#51">Luini, Saint
              Catherine of Alexandria</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#56">Luini,
              Venus</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#60">Gaudenzio
              Ferrari, Saint Andrew</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#64">Sacchi, Saint
              Paul</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#70">Sodoma, Virgin
              and Child holding a red rose</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#76">Sodoma, Saint
              Jerome</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#83">Boltraffio,
              Portrait of a man</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#86">Plate P.
                  Portrait of Casio ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#92">Giampietrino,
              Salome</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#100">Marco
              d'Oggiono, Infants Christ and Saint John</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#98">Marco
                  d'Oggiono, Infants Christ and Saint John ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#102">Plate Q</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#102">Infant
                  Christ and Saint John ▣</a>
                </li>
                <li>
                  <a href="katpmon135551002s.html#102">Luini,
                  Holy Family with Saint John ▣</a>
                </li>
                <li>
                  <a href=
                  "katpmon135551002s.html#102">Bernardino de'
                  Conti, Virgin and Child with Saint John ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#106">Boccaccio
              Boccaccini, Portrait of a Lady</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#107">North Italian
              school, Portrait of a man</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551002s.html#108">Tuscan
          schools</a>
          <ul>
            <li>
              <a href="katpmon135551002s.html#110">Decapitation
              of a Saint ▣</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#112">Giovanni del
              Ponte, Decapitation of a Female Saint</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#118">Botticelli,
              Life and miracles of Saint Zenobius</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#128">Plate R.</a>
              <ul>
                <li>
                  <a href=
                  "katpmon135551002s.html#128">Botticelli,
                  Epiphany ▣</a>
                </li>
                <li>
                  <a href=
                  "katpmon135551002s.html#128">Botticelli,
                  Miracles and Death of Saint Zenobius ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#142">Florentine
              School, Virgin and Child</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#144">Florentine
              School, Virgin and Child</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#148">Plate S.</a>
              <ul>
                <li>
                  <a href=
                  "katpmon135551002s.html#148">Florentine
                  School, Virgin and Child with Angels ▣</a>
                </li>
                <li>
                  <a href=
                  "katpmon135551002s.html#148">Florentine
                  School, Virgin and Child ▣</a>
                </li>
                <li>
                  <a href=
                  "katpmon135551002s.html#148">Florentine
                  School, The Angel Raphael with Tobias ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#153">Tuscan
              school, Portrait of a Lady</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#156">Plate
                  T.</a>
                  <ul>
                    <li>
                      <a href=
                      "katpmon135551002s.html#156">Portrait of a
                      Lady ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#159">Fra
              Bartolomeo, Adoration of the infant Christ</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#160">Adoration
                  of &#160;the Infant Christ ▣</a>
                </li>
                <li>
                  <a href="katpmon135551002s.html#162">Plate U.
                  Adoration of the infant Christ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#166">Fra
              Bartolomeo, Adoration of the Child</a>
            </li>
            <li>
              <a href=
              "katpmon135551002s.html#174">Bacchiacca</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#176">Baptism
                  of Christ ▣</a>
                </li>
                <li>
                  <a href="katpmon135551002s.html#178">Plate UU.
                  Baptism of Christ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#184">Jacopo
              Carrucci da Pontormo</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#182">A
                  Dialogue ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#186">Italian
              school, Portrait of Alberto Pio, Prince of Capri</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#209">Daniele da
              Volterra, Portrait of the artist</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#210">Portrait
                  of the Artist ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#213">Florentine
              school, Portraits of Five Artists</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#216">Plate W.</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#216">Portrait of
              Alberto Pio ▣</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#216">Portrait of
              Raphael ▣</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#216">Portraits of
              Florentine Artists ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551002s.html#222">Umbrian
          School</a>
          <ul>
            <li>
              <a href="katpmon135551002s.html#226">Francesco di
              Gentile da Fabriano, Ecce Homo</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#224">Ecce Homo
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#231">Signorelli,
              Esther before Ahasuerus: scenes relating to the
              Apotheosis of Saint Jerome: A Predella</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#232">Esther
                  before Ahasuerus ▣</a>
                </li>
                <li>
                  <a href="katpmon135551002s.html#240">Plate
                  Y.</a>
                  <ul>
                    <li>
                      <a href=
                      "katpmon135551002s.html#240">Signorelli,
                      Altarpiece ▣</a>
                    </li>
                    <li>
                      <a href=
                      "katpmon135551002s.html#240">Virgin and
                      Child and the crowning of Esther ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="katpmon135551002s.html#242">Plate
                  YY.</a>
                  <ul>
                    <li>
                      <a href=
                      "katpmon135551002s.html#242">Signorelli,
                      The Immaculate Conception ▣</a>
                    </li>
                    <li>
                      <a href=
                      "katpmon135551002s.html#242">Cranach, The
                      effects of jealousy ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href=
                  "katpmon135551002s.html#252">Apparition of
                  Saint Jerome to Saint Augustin, and to Sulpicius
                  Serenus &#160;▣</a>
                </li>
                <li>
                  <a href=
                  "katpmon135551002s.html#258">Apparition of
                  Saint Jerome to Cyril ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#263">Raphael,
              Crucifixion; or the blood of the redeemer</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#270">Plate
                  X</a>
                  <ul>
                    <li>
                      <a href=
                      "katpmon135551002s.html#270">Adoration of
                      the crucifixus ▣</a>
                    </li>
                    <li>
                      <a href=
                      "katpmon135551002s.html#270">Crucifixus
                      with the Virgin and Saint Jerome ▣</a>
                    </li>
                    <li>
                      <a href=
                      "katpmon135551002s.html#270">Crucifixus
                      and the Magdalene ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="katpmon135551002s.html#272">Plate
                  XX</a>
                  <ul>
                    <li>
                      <a href="katpmon135551002s.html#272">The
                      Almighty, with Angels ▣</a>
                    </li>
                    <li>
                      <a href=
                      "katpmon135551002s.html#272">Portrait of
                      Daniele da Volterra ▣</a>
                    </li>
                    <li>
                      <a href=
                      "katpmon135551002s.html#272">Detail from
                      the entombment ▣</a>
                    </li>
                    <li>
                      <a href=
                      "katpmon135551002s.html#272">Detail from
                      the Resurrection of Christ ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#290">Spagna, Three
              Saints</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#288">Three
                  Saints ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#293">Genga,
              Coriolanus with Volumnia and Veturia</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551002s.html#304">Bolognese and
          Ferrarese Schools</a>
          <ul>
            <li>
              <a href="katpmon135551002s.html#306">Francesco
              Francia, Virgin and Child with Angel</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#312">Mazzolini,
              The tribute money</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#314">The
                  Tribute Money ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#317">Garofalo, A
              Sacrifice to Ceres</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#327">Dosso Dossi,
              Adoration of the Magi</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#333">Correggio,
              Head of an Angel</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#334">Head of
                  an Angel ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#339">Scasellino,
              The mystic marriage of Saint Catherine</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#340">Marriage
                  of Saint Catherine ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="katpmon135551002s.html#344">Cittadini,
              Portrait of a Gentleman and his son</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#346">Giuseppe
              Zola, Repentant Magdalen</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551002s.html#348">Spanish, German,
          and Flemish Schools</a>
          <ul>
            <li>
              <a href="katpmon135551002s.html#350">Murillo,
              Saint John the Baptist</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#352">Lucas
              Cranach, The effects of Jealousy</a>
            </li>
            <li>
              <a href="katpmon135551002s.html#356">Peter Paul
              Rubens, A Moonlit Landscape</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#354">A Moonlit
                  Landscape ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551002s.html#360">Hellenistic
          School</a>
          <ul>
            <li>
              <a href="katpmon135551002s.html#364">Four
              Portrait-Heads</a>
              <ul>
                <li>
                  <a href="katpmon135551002s.html#364">I.
                  Portrait of a man wearing a gold wreath</a>
                  <ul>
                    <li>
                      <a href=
                      "katpmon135551002s.html#362">Portrait of a
                      man ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="katpmon135551002s.html#364">II.
                  Portrait of a man</a>
                </li>
                <li>
                  <a href="katpmon135551002s.html#365">III.
                  Portrait of a young woman</a>
                  <ul>
                    <li>
                      <a href=
                      "katpmon135551002s.html#370">Portrait of a
                      Lady ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="katpmon135551002s.html#365">IV.
                  Portrait of a woman</a>
                  <ul>
                    <li>
                      <a href=
                      "katpmon135551002s.html#366">Portrait of a
                      Lady ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="katpmon135551002s.html#374">Index of
          Persons</a>
        </li>
        <li>
          <a href="katpmon135551002s.html#381">Index of
          Places</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
