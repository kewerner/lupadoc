<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efir83040403s.html#6">Memorie istoriche
      dell’Ambrosiana R. Basilica di S. Lorenzo di Firenze; Tomo
      III.</a>
      <ul>
        <li>
          <a href="efir83040403s.html#8">All'illustrissimo, e
          reverendissimo Sig. Angiolo Gilardoni Arciprete della
          Metropolitana Fiorentina</a>
        </li>
        <li>
          <a href="efir83040403s.html#12">Prefazione</a>
        </li>
        <li>
          <a href="efir83040403s.html#18">Memorie istoriche
          dell'Ambrosiana Imperial Basilica di San Lorenzo di
          Firenze</a>
        </li>
        <li>
          <a href="efir83040403s.html#150">Serie successiva dei
          Rettori dei moderni canonicati</a>
        </li>
        <li>
          <a href="efir83040403s.html#344">Prospetto dei
          personaggi più illustri del capitolo di S. Lorenzo</a>
        </li>
        <li>
          <a href="efir83040403s.html#358">Documenti</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
