<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="even11641810s.html#6">Venetia, città nobilissima
      et singolare&#160;</a>
      <ul>
        <li>
          <a href="even11641810s.html#8">Alla serenissima Signora
          Bianca Cappello de Medici</a>
        </li>
        <li>
          <a href="even11641810s.html#12">Autori citati nel
          presente volume</a>
        </li>
        <li>
          <a href="even11641810s.html#13">Materie che si trattano
          nell'opera presente</a>
        </li>
        <li>
          <a href="even11641810s.html#14">Libro primo</a>
        </li>
        <li>
          <a href="even11641810s.html#73">Libro secondo</a>
        </li>
        <li>
          <a href="even11641810s.html#120">Libro terzo</a>
        </li>
        <li>
          <a href="even11641810s.html#141">Libro quarto</a>
        </li>
        <li>
          <a href="even11641810s.html#160">Libro quinto</a>
        </li>
        <li>
          <a href="even11641810s.html#188">Libro sesto</a>
        </li>
        <li>
          <a href="even11641810s.html#212">Libro settimo</a>
        </li>
        <li>
          <a href="even11641810s.html#223">Libro ottavo</a>
        </li>
        <li>
          <a href="even11641810s.html#293">Libro nono</a>
        </li>
        <li>
          <a href="even11641810s.html#307">Libro decimo</a>
        </li>
        <li>
          <a href="even11641810s.html#362">Libro undecimo</a>
        </li>
        <li>
          <a href="even11641810s.html#401">Libro duodecimo</a>
        </li>
        <li>
          <a href="even11641810s.html#428">Libro terzodecimo</a>
        </li>
        <li>
          <a href="even11641810s.html#566">Prima tavola delle
          chiese e monisteri di Venetia</a>
        </li>
        <li>
          <a href="even11641810s.html#569">Seconda tavola dei
          Dogi di Venetia</a>
        </li>
        <li>
          <a href="even11641810s.html#571">Terza tavola degli
          huomini Letterati veneti che hanno scritto</a>
        </li>
        <li>
          <a href="even11641810s.html#576">Tavola quarta di tutte
          le marterie che si contengono nell'opera presente</a>
        </li>
        <li>
          <a href="even11641810s.html#626">Quinta tavola dei
          senatori et huomini illustri</a>
        </li>
        <li>
          <a href="even11641810s.html#634">Cronico particolare
          delle cose fatte dai veneti</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
