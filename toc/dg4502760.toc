<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502760s.html#6">Les Merveilles de la Ville de
      Rome</a>
      <ul>
        <li>
          <a href="dg4502760s.html#7">Figure de l'Echelle Sainte à
          coste de Saint Jean de Lateran ›Scala Santa‹ ▣</a>
        </li>
        <li>
          <a href="dg4502760s.html#8">Les septes Eglises
          principales</a>
          <ul>
            <li>
              <a href="dg4502760s.html#8">La premiere eglise Est
              Saint Jean de Lateran ›San Giovanni in Laterano‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#13">[›San Pietro in
              Vaticano‹ ▣]</a>
            </li>
            <li>
              <a href="dg4502760s.html#20">[›San Pietro in
              Vaticano: Baldacchino di San Pietro‹ ▣]</a>
            </li>
            <li>
              <a href="dg4502760s.html#21">[›San Pietro in
              Vaticano: Cattedra di San Pietro‹ ▣]</a>
            </li>
            <li>
              <a href="dg4502760s.html#22">La troisieme Eglise est
              Saint Paul ›San Paolo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#24">[›Piramide di Caio
              Cestio‹ ▣]</a>
            </li>
            <li>
              <a href="dg4502760s.html#25">La quatriesme Eglise
              est Sainte Marie Maieure ›Santa Maria Maggiore‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#28">La cinquiesme Eglise
              est Saint Laurens hors des Murs ›San Lorenzo fuori le
              Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#29">La sixieme Eglise est
              Saint Sebastien ›San Sebastiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#31">La septieme Eglise est
              celle de Sainte Croix in Hierusalem ›Santa Croce in
              Gerusalemme‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#32">Dans l'Isle ›Isola
              Tiberina‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#32">De la le Tybre dit
              Transtevere ›Trastevere‹</a>
              <ul>
                <li>
                  <a href="dg4502760s.html#33">[›Santa Cecilia in
                  Trastevere‹ ▣]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502760s.html#37">Au Bourg ›Borgo‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#39">Depuis la Porte
              Flaminia, autrement du Peuple ›Porta del Popolo‹
              iusques au pied du Capitol ›Campidoglio‹</a>
              <ul>
                <li>
                  <a href="dg4502760s.html#40">[›Santa Maria del
                  Popolo‹ ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#41">[›Trinità dei
                  Monti‹ ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#45">[›Colonna di
                  Antonino Pio‹ ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#46">[›Santa Maria sopra
                  Minerva‹ ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#47">[›Pantheon‹ ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#48">[›Colonna Traiana‹
                  ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#49">[›San Luigi dei
                  Francesi‹ ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#50">[›Santa Maria del
                  Popolo‹ ▣]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502760s.html#56">Depuis le Capitole à
              main gauche vers les Montes</a>
              <ul>
                <li>
                  <a href="dg4502760s.html#56">[›Santa Maria in
                  Aracoeli‹ ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#57">[›Foro Romano‹
                  ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#63">[›San Pietro in
                  Vincoli‹ ▣]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502760s.html#63">Du Capitole à main
              droite vers les Monts</a>
              <ul>
                <li>
                  <a href="dg4502760s.html#64">[›Santa Galla
                  (scomparsa)‹ ▣ ]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#66">[›Santo Stefano
                  Rotondo‹ ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#67">[›Santa Sabina‹
                  ▣]</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502760s.html#70">Les festes, et le stations
          qui sont ez Eglises de Rome [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502760s.html#78">La Guide Romaine por les
      estangers qui viennent voir les Andiquitez de Rome, l'une
      apres l'autre distinctement reduite en un tras bel ordre</a>
      <ul>
        <li>
          <a href="dg4502760s.html#78">La premiere journèe</a>
          <ul>
            <li>
              <a href="dg4502760s.html#78">Du Bourg ›Borgo‹</a>
              <ul>
                <li>
                  <a href="dg4502760s.html#78">[›Castel
                  Sant'Angelo‹ ▣]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502760s.html#79">De là de le Tybre
              ›Tevere‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#80">De l'Isle du Tybre
              ›Isola Tiberina‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#81">Du pont Sainte Marie
              ›Ponte Rotto‹, du palais de Pilate ›Casa dei
              Crescenzi‹ ▣, et d'autres choses</a>
            </li>
            <li>
              <a href="dg4502760s.html#82">Du Mont Testaceus
              ›Testaccio (Monte)‹, et d'autres choses</a>
            </li>
            <li>
              <a href="dg4502760s.html#82">[›Piramide di Caio
              Cestio‹ ▣]</a>
            </li>
            <li>
              <a href="dg4502760s.html#83">Des Thermes d'Antoninus
              Caracalla ›Terme di Caracalla‹ ▣ et autres choses</a>
            </li>
            <li>
              <a href="dg4502760s.html#84">De Sain Jean Lateran
              ›San Giovanni in Laterano‹, et autres choses</a>
              <ul>
                <li>
                  <a href="dg4502760s.html#84">[ ›Arco di Giano‹
                  ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#85">[ ›Porta Maggiore‹
                  ▣]</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502760s.html#85">La second journee</a>
          <ul>
            <li>
              <a href="dg4502760s.html#85">De la Porte de Populo
              ›Porta del Popolo‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#86">Des Chevaux de marbre
              qui sont à Monte Cavallo ›Fontana di Monte Cavallo‹,
              et des Bains, ou Thermes de Diocletian ›Terme di
              Diocleziano‹</a>
              <ul>
                <li>
                  <a href="dg4502760s.html#86">[›Palazzo del
                  Quirinale‹]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#87">[›Terme di
                  Diocleziano‹ ▣]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502760s.html#88">De la rue Pia ›Via XX
              Settembre‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#88">De la Vigna qui estoir
              au Cardinal de Ferrare</a>
            </li>
            <li>
              <a href="dg4502760s.html#88">De la Vigne du Cardinal
              de Carpi ›Palazzo Barberini‹ et autres</a>
            </li>
            <li>
              <a href="dg4502760s.html#88">De la ›Porta Pia‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#88">De Saint Agnes
              ›Sant'Agnese fuori le mura‹; et des Thermes de
              Dioceltian ›Terme di Diocleziano‹, et autres
              choses</a>
            </li>
            <li>
              <a href="dg4502760s.html#90">Du temple d'Isis ›Iseum
              Metellinum ‹, et d'autres choses</a>
            </li>
            <li>
              <a href="dg4502760s.html#90">Des sept Sales ›Sette
              Sale‹, du Colisèe ›Colosseo‹ ▣, et autres choses</a>
            </li>
            <li>
              <a href="dg4502760s.html#91">Tu Temple de la Paix
              ›Foro della Pace‹, du mont Palatinus ›Palatino‹ ▣
              maintenant dict Palais maieur, et d'autres choses
              notables</a>
            </li>
            <li>
              <a href="dg4502760s.html#92">Du For de Nerva ›Foro
              di Nerva‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#93">Du Capitole
              ›Campidoglio‹ ▣, et d'autres remarques</a>
            </li>
            <li>
              <a href="dg4502760s.html#94">Des Porches d'Octavia
              ›Portico d'Ottavia‹, de Septimius ›Porticus Severi‹,
              et de Theatre de Pompèe ›Teatro di Pompeo‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#95">Des deux Colomnes, une
              d'Antoninus Pius ›Colonna di Antonino Pio‹, l'autre
              de Traian ›Colonna Traiana‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#96">De la Rotonde, ou
              ›Pantheon‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#97">Des Bains d'Agrippa
              ›Terme di Agrippa‹, et de Neron ›Terme
              Neroniano-Alessandrine‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#98">De la Place Navone
              ›Piazza Navona‹ ▣, et du Pasquin ›Statua di
              Pasquino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502760s.html#99">Table des Papes romains</a>
        </li>
        <li>
          <a href="dg4502760s.html#115">Les Roys, et Empereurs
          romains</a>
        </li>
        <li>
          <a href="dg4502760s.html#132">Table des Eglises de
          Rome</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502760s.html#125">Le Sept Merveilles du
      Monde</a>
      <ul>
        <li>
          <a href="dg4502760s.html#125">I. Des Murailles de
          Babilone</a>
        </li>
        <li>
          <a href="dg4502760s.html#126">II. De la Tour de
          Pharos</a>
        </li>
        <li>
          <a href="dg4502760s.html#127">III. De le Statue de
          Iupiter</a>
        </li>
        <li>
          <a href="dg4502760s.html#128">IV. Du Colosse de
          Rhodes</a>
        </li>
        <li>
          <a href="dg4502760s.html#129">V. De Temple de Diane</a>
        </li>
        <li>
          <a href="dg4502760s.html#130">VI. Du Mausolèe
          d'Artemisia</a>
        </li>
        <li>
          <a href="dg4502760s.html#131">VII. Des Pyramids
          d'Egipte</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502760s.html#118">Les Antiquites de la Ville de
      Roma</a>
      <ul>
        <li>
          <a href="dg4502760s.html#119">Aux lecteurs</a>
        </li>
        <li>
          <a href="dg4502760s.html#134">[Text]</a>
          <ul>
            <li>
              <a href="dg4502760s.html#134">De l'Edification de
              Roma</a>
            </li>
            <li>
              <a href="dg4502760s.html#136">Des circuit, et
              enceinte de Rome</a>
            </li>
            <li>
              <a href="dg4502760s.html#137">Des Portes</a>
            </li>
            <li>
              <a href="dg4502760s.html#138">Porte Maieure ›Porta
              Maggiore‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#139">Des Rues</a>
            </li>
            <li>
              <a href="dg4502760s.html#139">Des Ponts qui sont sur
              le Tybre, et ceux qui les ont edifiz</a>
            </li>
            <li>
              <a href="dg4502760s.html#141">Del'isle du Tybre
              ›Isola Tiberina‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#141">Des Monts</a>
            </li>
            <li>
              <a href="dg4502760s.html#142">Palazzo maggiore
              ›Palatino‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#142">[›Palazzo del
              Quirinale‹] ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#143">Du Mont Testaceus
              ›Testaccio (monte)‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#143">[›Piramide di Caio
              Cestio‹ ▣]</a>
            </li>
            <li>
              <a href="dg4502760s.html#144">De la Cloaque ›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#145">Des Aqueducs</a>
            </li>
            <li>
              <a href="dg4502760s.html#146">Des sept Sales ›Sette
              Sale‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#147">Des Thermes, c'est à
              scavoir des Bains, et des caux qui les ont fait
              bastir</a>
            </li>
            <li>
              <a href="dg4502760s.html#148">Thermes Diocletianes
              ›Terme di Diocleziano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#149">Des Naumachies, où se
              faisoient les Batailles Navales</a>
            </li>
            <li>
              <a href="dg4502760s.html#149">Des Cirques</a>
            </li>
            <li>
              <a href="dg4502760s.html#150">Des Theatres, et de
              ceux qui les edifierent</a>
            </li>
            <li>
              <a href="dg4502760s.html#150">Du Theatre de
              Marcellus ›Teatro di Marcello‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#151">Des Amphitheatres, et
              de leurs edificateurs</a>
            </li>
            <li>
              <a href="dg4502760s.html#151">Le Colisèe ›Colosseo‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#152">Amphitheatre de
              Statilius ›Anfiteatro di Statilio Tauro‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#152">Des Places</a>
            </li>
            <li>
              <a href="dg4502760s.html#153">›Foro Boario‹ ▣
              appelle aujourd'huy Campo vaccino</a>
            </li>
            <li>
              <a href="dg4502760s.html#154">Des Arcs Tromphans, et
              à qui on les donnoit et eslevoit</a>
            </li>
            <li>
              <a href="dg4502760s.html#155">Des Porches</a>
              <ul>
                <li>
                  <a href="dg4502760s.html#155">[›Arco di
                  Costantino‹ ▣]</a>
                </li>
                <li>
                  <a href="dg4502760s.html#156">[›Pantheon‹ ▣]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502760s.html#157">Des Trophèes, et
              Colomnes remarquables ›Trofei di Mario‹ ▣</a>
            </li>
            <li>
              <a href="dg4502760s.html#158">Des Colosses</a>
            </li>
            <li>
              <a href="dg4502760s.html#158">Des Pyramides</a>
            </li>
            <li>
              <a href="dg4502760s.html#158">Des Metes</a>
            </li>
            <li>
              <a href="dg4502760s.html#159">Des Obelisques</a>
            </li>
            <li>
              <a href="dg4502760s.html#159">Des Statues</a>
            </li>
            <li>
              <a href="dg4502760s.html#159">De Marfore ›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#160">Des chevaux ›Fontana
              di Monte Cavallo‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#160">Des Libraries</a>
            </li>
            <li>
              <a href="dg4502760s.html#160">Des Horologes</a>
            </li>
            <li>
              <a href="dg4502760s.html#161">Des Palais</a>
            </li>
            <li>
              <a href="dg4502760s.html#161">De la maison dorèe de
              Neron ›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#162">Des autres maisons des
              Citoyens</a>
            </li>
            <li>
              <a href="dg4502760s.html#162">Des Cours</a>
            </li>
            <li>
              <a href="dg4502760s.html#163">Des petits Senats</a>
            </li>
            <li>
              <a href="dg4502760s.html#163">Des Magistrats</a>
            </li>
            <li>
              <a href="dg4502760s.html#164">Des Comices</a>
            </li>
            <li>
              <a href="dg4502760s.html#164">Des Tribus</a>
            </li>
            <li>
              <a href="dg4502760s.html#164">Des contrèes, ou
              quartiers, et de leurs enseignes</a>
            </li>
            <li>
              <a href="dg4502760s.html#164">Des Basiliques</a>
            </li>
            <li>
              <a href="dg4502760s.html#165">Des Capitole
              ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#166">De l'›Aerarium‹, ou
              chambre, et maison de Ville, et quelle mannoye
              couroit à Rome en ce temps</a>
            </li>
            <li>
              <a href="dg4502760s.html#167">Du ›Grecostasi‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#167">Du Gresse du peuple
              Romain</a>
            </li>
            <li>
              <a href="dg4502760s.html#168">De l'Asyle ›Asilo di
              Romolo‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#168">De ce qu'ils
              appelloient Rostra ›Rostri‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#168">De la Colomne discte
              Miliaria ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#168">Du Temple de Carmenta
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#168">De la Colomne de
              guerre ›Columna Bellica‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#169">De la Colomne Lattaria
              ›Columna Lactaria‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#169">De l'Equimelium
              ›Equimelio‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#169">Du Champ de Mars
              ›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#169">Du Sigillum Sororium
              ›Tigillum Sororium‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#169">Des champs nommez
              Forastieri ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#170">De la Ville publique
              ›Villa Publica‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#170">De la ›Taberna
              Meritoria‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#171">Du Parc</a>
            </li>
            <li>
              <a href="dg4502760s.html#171">Des Iardins</a>
            </li>
            <li>
              <a href="dg4502760s.html#171">Du Velabrum
              ›Velabro‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#172">Des Carines
              ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#172">Des Tretres</a>
            </li>
            <li>
              <a href="dg4502760s.html#172">Des Prez</a>
            </li>
            <li>
              <a href="dg4502760s.html#173">Des Greniers publies
              ›Horrea‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#173">Des ›Prisons
              publiques‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#173">De quelques jeux, et
              festes, qu'on souloit celebrer dans Rome</a>
            </li>
            <li>
              <a href="dg4502760s.html#174">Des Sepulchres
              d'Auguste, d'Adrian et de Septimius</a>
              <ul>
                <li>
                  <a href="dg4502760s.html#174">›Castel
                  Sant'Angelo‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502760s.html#175">Des Temples</a>
            </li>
            <li>
              <a href="dg4502760s.html#176">Des Prestres et des
              Viergers, Vestements, Vases, et autres instrumens
              faits à l'usage des sacrifices, et de leurs
              instituteurs</a>
            </li>
            <li>
              <a href="dg4502760s.html#177">De l'Arsenal
              ›Armilustrium‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#177">De l'armèe terrestre,
              et maritime des Romains, et de leurs insegnes</a>
            </li>
            <li>
              <a href="dg4502760s.html#178">Des Triomphes et a qui
              on les ostroyoit, et qui fut le premier qui triompha
              et de combien de sortes il y en avoit</a>
            </li>
            <li>
              <a href="dg4502760s.html#178">Des Couronees, et à
              qui se donnoient</a>
            </li>
            <li>
              <a href="dg4502760s.html#179">Du nombre du peuple
              Romain</a>
            </li>
            <li>
              <a href="dg4502760s.html#179">Des Richesses du
              Peuple Romain</a>
            </li>
            <li>
              <a href="dg4502760s.html#179">De la liberalitè des
              anciens Romains</a>
            </li>
            <li>
              <a href="dg4502760s.html#180">Des anciens Mariages,
              et de leurs ceremonies</a>
            </li>
            <li>
              <a href="dg4502760s.html#180">De la civiltè qu'ou
              enseignoit aux Enfans</a>
            </li>
            <li>
              <a href="dg4502760s.html#181">Du divorce des
              Martages</a>
            </li>
            <li>
              <a href="dg4502760s.html#181">Des obseques antiques
              et de leurs ceremonies</a>
            </li>
            <li>
              <a href="dg4502760s.html#182">Des Tours</a>
            </li>
            <li>
              <a href="dg4502760s.html#182">Du Tibre ›Tevere‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#183">Du Palais Papal
              ›Palazzo Apostolico Vaticano‹, et Belvedere ›Cortile
              del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4502760s.html#183">De delà le Tybre</a>
            </li>
            <li>
              <a href="dg4502760s.html#183">Recapitulation des
              Antiquites</a>
            </li>
            <li>
              <a href="dg4502760s.html#184">Des Temples des
              anciens hors de Rome</a>
            </li>
            <li>
              <a href="dg4502760s.html#186">Combien de fois Roma a
              essè prise</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502760s.html#187">Guide de chemins de Rome
          aux principales Villes d'Italie, et lieux circonvoisins,
          avec le voyage de Saint Jacques en Gallice</a>
        </li>
        <li>
          <a href="dg4502760s.html#195">Table de ce qui est plus
          remarquable en ce livre</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
