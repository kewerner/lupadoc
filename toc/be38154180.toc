<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="be38154180s.html#6">Lago Fucino ed emissario di
      Claudio nella regione de' Marsi.</a>
      <ul>
        <li>
          <a href="be38154180s.html#8">Indice.</a>
        </li>
        <li>
          <a href="be38154180s.html#10">Prefazione
          Apologetica.</a>
        </li>
        <li>
          <a href="be38154180s.html#36">Introduzione.</a>
        </li>
        <li>
          <a href="be38154180s.html#46">Problema.</a>
        </li>
        <li>
          <a href="be38154180s.html#52">§1 La storia viene
          smentita dalla critica</a>
        </li>
        <li>
          <a href="be38154180s.html#102">§2 Il fatto dimostra che
          Narciso fallò l’emissario di Claudio</a>
        </li>
        <li>
          <a href="be38154180s.html#138">§3 La scienza fa
          conoscere le menzogne della storia</a>
        </li>
        <li>
          <a href="be38154180s.html#214">Produzioni di C. Lippi,
          già stampate.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
