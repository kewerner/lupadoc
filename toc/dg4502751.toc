<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502751s.html#6">Studio di Pittura, Scoltura, ed
      Architettura, nelle Chiese di Roma</a>
      <ul>
        <li>
          <a href="dg4502751s.html#8">[Dedica al] Benigno
          lettore</a>
        </li>
        <li>
          <a href="dg4502751s.html#10">Tavola delle chiese di
          Roma</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502751s.html#18">[Text]</a>
      <ul>
        <li>
          <a href="dg4502751s.html#18">Di ›San Pietro in
          Vaticano‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#32">Di ›Santa Marta
          (Vaticano)‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#33">Di Santa Maria in Campo
          Santo ›Santa Maria della Pietà‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#34">Di ›Santo Spirito in
          Sassia‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#36">Di ›Sant'Onofrio al
          Gianicolo‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#38">Di ›San Leonardo in
          Settignano‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#38">Della Chiesa di Regina
          Coeli ›Santa Maria di Montesanto‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#39">Di Santa Croce della
          Penitenza ›Santa Croce delle Scalette‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#39">Di ›San Pietro in
          Montorio‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#42">Di ›Santa Maria della
          Scala‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#43">Di ›Santa Maria in
          Trastevere‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#46">Di ›San Callisto‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#46">Di ›San Francesco a
          Ripa‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#48">Di ›Santa Maria
          dell'Orto‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#49">Di ›Santa Cecilia in
          Trastevere‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#51">Di ›San Crisogono‹ in
          Trastevere</a>
        </li>
        <li>
          <a href="dg4502751s.html#52">Di ›San Bartolomeo
          all'Isola‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#53">Di San Giovanni Collavita
          ›San Giovanni Calibita‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#54">Di ›Santa Sabina‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#55">Di ›San Paolo fuori le
          Mura‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#58">Di San Vincenzo, et
          Anastasio alle tre Fontane ›Santi Vincenzo ed Anastasio
          alle Tre Fontane‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#58">Di ›Santa Maria Scala
          Coeli‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#59">Di ›San Paolo alle Tre
          Fontane‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#59">Di ›San Sebastiano‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#61">Dei ›Santi Nereo e
          Achilleo‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#62">Di ›Santa Balbina‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#62">Di ›Santa Prisca‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#63">Di San Gregorio nel Monte
          Celio ›San Gregorio Magno‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#64">Di Santa Silvia ›Oratorio
          di Santa Silvia‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#65">Di Sant'Andrea ›Oratorio di
          Sant'Andrea‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#65">Di Santa Barbara ›Oratorio
          di Santa Barbara‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#66">Di ›Santi Giovanni e
          Paolo‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#67">Di ›San Giovanni
          Decollato‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#68">›Oratorio di San Giovanni
          Decollato‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#69">Di ›Sant'Eligio dei
          Ferrari‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#70">Di Santa Maria in Portico,
          hoggi ›Santa Galla (scomparsa)‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#71">Di ›San Nicola in
          Carcere‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#72">Di ›Sant'Angelo in
          Pescheria‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#73">Di ›Sant'Ambrogio della
          Massima‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#73">Di ›Santa Caterina dei
          Funari‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#75">Di Sant'Anna alli Funari
          ›Sant'Anna dei Falegnami‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#76">Di ›San Carlo ai
          Catinari‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#78">Di ›Santa Maria del
          Pianto‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#79">Di ›San Tommaso dei
          Cenci‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#79">Di ›San Bartolomeo dei
          Vaccinari‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#80">Dell'›Oratorio della
          Santissima Trinità dei Pellegrini‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#81">Di ›San Francesco d'Assisi
          a Ponte Sisto‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#81">Di ›Santi Giovanni
          Evangelista e Petronio dei Bolognesi‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#81">Di ›Santa Caterina da
          Siena‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#82">Di ›Sant'Eligio degli
          Orefici‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#82">Della ›Chiesa dello Spirito
          Santo dei Napoletani‹, e di ›Santa Lucia del
          Gonfalone‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#83">Di ›Santa Maria in
          Monserrato‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#84">Della Santissima Trinità, ò
          di San Tomaso degl'Inglesi ›San Tommaso di
          Canterbury‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#84">Di ›Santa Caterina della
          Rota‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#85">Di ›San Girolamo della
          Carità‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#86">Della Santissima Trinità di
          Ponte Sisto ›Santissima Trinità dei Pellegrini‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#88">Di San Martino al Monte
          della Pietà ›San Martinello‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#88">Di ›San Lorenzo in
          Damaso‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#90">Di Santa Maria ›Chiesa
          Nuova‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#95">Di ›San Tommaso in
          Parione‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#95">Di Sant'Agnese in Piazza
          Navona ›Sant'Agnese in Agone‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#98">Di San Pantaleone alle
          Scuole Pie ›San Pantaleo‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#98">Di ›Sant'Elena dei
          Credenzieri‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#99">Di ›Sant'Andrea della
          Valle‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#104">Di San Giacomo degli
          Spagnoli ›Nostra Signora del Sacro Cuore‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#106">Di ›San Luigi dei
          Francesi‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#109">Di ›Sant'Eustachio‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#110">Di San Leone nella
          Sapienza ›Sant'Ivo‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#110">Delle Chiese di San
          Niccolò alle Carcere ›San Nicola ai Cesarini‹, e delle
          Stimmate ›Sante Stimmate di San Francesco‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#111">Di ›San Giovanni della
          Pigna‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#111">Di ›Santa Maria sopra
          Minerva‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#120">Di ›Sant'Ignazio‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#122">Di ›Santo Stefano del
          Cacco‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#123">Di Santa Marta incontro al
          Collegio Romano ›Santa Marta (Collegio Romano)‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#123">Del Gesù ›Santissimo Nome
          di Gesù‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#128">Di ›San Marco‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#131">Di ›Santo Stanislao dei
          Polacchi‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#132">Di ›Santa Maria in
          Campitelli‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#133">Di ›Santa Maria della
          Consolazione‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#135">Di ›Santa Maria in
          Aracoeli‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#139">Di ›San Giuseppe dei
          Falegnami‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#140">Di San Luca in Santa
          Martina ›Santi Luca e Martina‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#142">Di ›Sant'Adriano al Foro
          Romano‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#143">Di ›San Lorenzo in
          Miranda‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#143">Dei ›Santi Cosma e
          Damiano‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#144">Di ›Santa Francesca
          Romana‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#146">Di ›Santo Stefano
          Rotondo‹, e ›Santa Maria in Domnica‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#147">Di ›San Giovanni in
          Fonte‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#149">Di ›San Giovanni in
          Laterano‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#155">Di San Salvatore alla
          ›Scala Santa‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#157">Di ›Santa Croce in
          Gerusalemme‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#159">Di ›San Lorenzo fuori le
          Mura‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#160">Di ›Santa Bibiana‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#161">Di ›Sant'Eusebio‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#161">Di ›San Matteo in
          Merulana‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#161">Di ›Santi Quattro
          Coronati‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#162">Di ›San Clemente‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#163">Di ›Sant'Urbano‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#164">Di Santa Maria Annunziata
          in San Basilio ›San Basilio al Foro di Augusto‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#165">Di Santa Maria de' Monti
          ›Chiesa della Madonna dei Monti‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#167">Di ›San Francesco di
          Paola‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#168">Di ›San Pietro in
          Vincoli‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#169">Di ›Santa Lucia in
          Selci‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#170">Di ›San Martino ai
          Monti‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#171">Di ›Santa Prassede‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#174">Di ›Sant'Antonio
          Abate‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#174">Di ›Santa Maria
          Maggiore‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#176">Della Cappella di Sisto V
          ›Cappella Sistina (Santa Maria Maggiore)‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#183">Della Cappella di Paolo V
          ›Cappella Paolina (Santa Maria Maggiore)‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#189">Di ›Santa Pudenziana‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#191">Di ›San Lorenzo in Fonte‹,
          et ›San Lorenzo in Panisperna‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#191">Di San Bernardino alli
          Monti ›San Bernardino da Siena‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#192">Di ›Sant'Agata dei Goti‹,
          e ›Santi Domenico e Sisto‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#194">Di ›Santa Caterina da
          Siena‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#195">Di ›Santa Maria di
          Loreto‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#196">Di San Silvestro a Monte
          Cavallo ›San Silvestro al Quirinale‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#200">Di ›San Vitale‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#201">Di ›Santa Maria degli
          Angeli‹ alle Terme</a>
        </li>
        <li>
          <a href="dg4502751s.html#202">Di ›Sant'Agnese fuori le
          Mura‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#203">Di ›Santa Maria della
          Vittoria‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#205">Di ›Santa Susanna‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#206">Di ›San Bernardo alle
          Terme‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#207">Di ›San Caio‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#208">Di Santa Maria Nuntiata
          ›Santa Maria Annunziata (Monti)‹, e di ›San Carlo alle
          Quattro Fontane‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#209">Di Sant'Andrea delli
          Gesuiti ›Sant'Andrea al Quirinale‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#210">Di Santa Chiara al
          Santissimo Sagramento ›Santa Chiara al Quirinale‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#210">Di ›Santa Croce e San
          Bonaventura dei Lucchesi‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#211">Dei ›Santi Apostoli‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#213">Di ›San Romualdo‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#213">Di ›Santa Maria in Via
          Lata‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#214">Di ›San Marcello al
          Corso‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#217">Del Crocefisso di San
          Marcello Oratorio ›Oratorio del Crocifisso‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#218">Di Santa Maria
          dell'Humiltà, e delle Vergini ›Santa Maria
          dell'Umiltà‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#219">Dei ›Santi Vincenzo ed
          Anastasio‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#219">Di Santa Maria di
          Costantinopoli ›Santa Maria d'Itria‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#220">Di ›San Nicola da
          Tolentino‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#223">Di San Francesco (sic!) da
          Padova, detto della Concettione ›Santa Maria della
          Concezione‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#224">Di ›Sant'Isidoro‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#225">Di ›Santa Francesca Romana
          dei Padri del Riscatto‹, e ›Santi Ildefonso e Tommaso di
          Villanova‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#226">Di San Giuseppe alle
          Fratte ›San Giuseppe a Capo le Case‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#227">Di ›Sant'Andrea delle
          Fratte‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#228">Di Propaganda Fide
          ›Collegio Urbano di Propaganda Fide‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#229">Di Santa Maria in
          Giovannino ›San Giovanni in Capite‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#230">Di San Silvestro delle
          Monache ›San Silvestro in Capite‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#231">Di Santa Maria Maddalena
          al Corso ›Monastero di Santa Maria Maddalena‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#232">Di ›Santa Maria in
          Via‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#233">Di ›Santa Maria in Trivio‹
          de Crociferi</a>
        </li>
        <li>
          <a href="dg4502751s.html#234">Di ›Santa Maria della
          Pietà (Piazza Colonna)‹ de' Pazzarelli</a>
        </li>
        <li>
          <a href="dg4502751s.html#235">Di San Machuto, e
          Bartolomeo de Bergamaschi ›Santi Bartolomeo e Alessandro
          dei Bergamaschi‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#235">Di Santa Maria Equirio
          detta degli Orfanelli ›Santa Maria in Aquiro‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#236">Di Santa Maria della
          Rotonda ›Pantheon‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#238">Della Maddalena ›Santa
          Maria Maddalena‹, e San Salvatore delle Cuppelle ›San
          Salvatore alle Coppelle‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#239">Di Santa Croce ›Santa
          Croce a Montecitorio‹ , e San Biagio a Monte Citorio ›San
          Biagio de Monte (scomparsa)‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#240">Di ›Santa Maria della
          Concezione in Campo Marzio‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#241">Di ›San Lorenzo in
          Lucina‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#243">Di Sant'Ambrogio, e San
          Carlo nel Corso ›Santi Ambrogio e Carlo al Corso‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#245">Della Santissima Trinità
          del Monte ›Trinità dei Monti‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#249">Di ›Sant'Atanasio‹ de'
          Greci</a>
        </li>
        <li>
          <a href="dg4502751s.html#250">›Chiesa di Gesù e
          Maria‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#251">Di San Giacomo
          dell'Incurabili ›San Giacomo in Augusta‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#252">Di ›Santa Maria del
          Popolo‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#255">Di ›Santa Maria di
          Montesanto‹, e de Miracoli ›Santa Maria dei Miracoli‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#256">Di San Rocco, e Martino
          ›San Rocco‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#257">Di San Girolamo de'
          Schiavoni ›San Girolamo degli Illirici‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#258">Di Sant'Antonio da Padova
          de' Portoghesi ›Sant'Antonio dei Portoghesi‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#259">Di Santa Maria in
          Sant'Agostino ›Sant'Agostino‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#263">Di Santa maria
          dell'Apollinare ›Sant'Apollinare‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#264">Di ›San Salvatore in
          Lauro‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#266">Di ›Santa Maria
          dell'Anima‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#268">Di ›Santa Maria della
          Pace‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#272">Di ›San Biagio della
          Fossa‹, e Santi Pietro, e Paolo del Confalone ›Oratorio
          del Gonfalone‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#273">Di San Faustino ›Sant'Anna
          dei Bresciani‹, e ›Santa Maria del Suffragio‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#274">Di ›San Giovanni dei
          Fiorentini‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#277">Di San Celso, e Giuliano
          vicino a Ponte Sant'Angelo ›Santi Celso e Giuliano‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#278">Di Sant'Angelo
          ›Sant'Angelo al Corridoio‹ , e Sant'Anna in Borgo
          ›Sant'Anna dei Palafrenieri‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#279">Di Santa Maria della
          Transpontina ›Santa Maria in Traspontina‹</a>
        </li>
        <li>
          <a href="dg4502751s.html#281">Di ›San Giacomo
          Scossacavalli‹</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
