<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="epav224190s.html#8">Guida di Pavia</a>
      <ul>
        <li>
          <a href="epav224190s.html#10">Introduzione</a>
        </li>
        <li>
          <a href="epav224190s.html#14">I. Notizie statistiche, e
          generali</a>
        </li>
        <li>
          <a href="epav224190s.html#36">II. Cenno storico</a>
        </li>
        <li>
          <a href="epav224190s.html#56">III. Edifici
          ragguardevoli ed altri oggetti di curiosità</a>
        </li>
        <li>
          <a href="epav224190s.html#94">IV. Università ed
          annessi</a>
        </li>
        <li>
          <a href="epav224190s.html#115">V. Certosa</a>
        </li>
        <li>
          <a href="epav224190s.html#166">Appendice</a>
        </li>
        <li>
          <a href="epav224190s.html#186">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
