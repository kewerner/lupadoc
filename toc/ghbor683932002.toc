<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbor683932002s.html#12">Opus Architectonicum
      equitis Francisci Borromini ex ejusdem exemplaribus Petitum;
      Oratorium nempè, Aedque Romanae Reverendissimis Patris
      Congregationis Oratorii Sancti Philippi Nerii ›Oratorio dei
      Filippini‹ ▣</a>
      <ul>
        <li>
          <a href="ghbor683932002s.html#9">[Ritratto di
          Francesco Bernini] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#14">[Dedica di
          Sebastiano Giannini] Eminentissimi, e Reverendissimo
          Principe</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#14">[Dedica di
          Sebastiano Giannini a Giuseppe Renato Imperiali]
          Eminentissimo, e Reverendissimo Principi [...]</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#18">Indice di tutta
          l'opera</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#22">Relazione della
          presente Opera [...]</a>
          <ul>
            <li>
              <a href="ghbor683932002s.html#22">[Dedica di
              Borromini al Marchese di Castel Rodriguez]
              All'Illustrissimo, e Eccellentissimo [...]</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#22">[Dedica ai
              lettori] Alli benigni Lettori</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#22">I. Introduzione
              al racconto della nuova Fabrica, Capitolo I.</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#23">II. Sito della
              nuova Abitazione</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#24">III. Della
              Sagrestia nuova</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#25">IV. Divisione
              generale di tutta la Fabrica</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#25">V. Dell'Oratorio
              in generale</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#26">VI.
              Dell'Oratorio nuovo</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#28">VII. Della
              Facciata dell'Oratorio</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#29">VIII. Delle
              Cappelle nell'abitazione de' Padri</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#30">IX. Della
              Portaria</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#31">X. Del primo
              cortile</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#32">XI. Della Scala
              principale</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#32">XII. Dell'altre
              Scale, che sono nella Fabrica</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#33">XIII. Del
              secondo cortile, o Giardino</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#34">XIV. D'altre
              cose più neccessarie</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#35">XV. Dell'altezza
              della Fabrica, Logge, e Camere contigue</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#36">XVI. Delle Logge
              di detta Fabrica</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#37">XVII. Delle
              Logge chiuse</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#37">XVIII. Delle
              Logge scoperte</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#38">XIX. Dell'alzato
              della Loggia verso i Cortili</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#38">XX. Delli
              Lavamani</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#39">XXI. Del
              Resettorio, ed altre officine</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#40">XXII. Dell'altre
              commodità adjacenti al Resettorio</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#41">XXIII.
              Dell'altre officine sopra terra</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#43">XXIV. Delle
              Sottorranee</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#43">XXV. Della Sala
              della Ricreazione</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#44">XXVI. Della
              Forasteria</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#45">XXVII. Delle
              stanze del Predicatore</a>
            </li>
            <li>
              <a href="ghbor683932002s.html#46">XXVIII. Della
              Libraria</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghbor683932002s.html#50">[Tavole]</a>
      <ul>
        <li>
          <a href="ghbor683932002s.html#50">II. Pianta di tutta
          l'Isola continente l'intiera Fabrica dè Padri della
          Chiesa Nuova [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#52">III. Pianta generale
          di tutta la nuova Fabrica [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#54">IV. Facciata
          prospetica dell'Oratorio, come si trova al presente, che
          fa fa prospetto su la Piazza ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#56">V. Facciata
          prospetica del medesimo con l'aggiunta di varj ornamenti
          non seguiti ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#58">VI. Facciata
          geometrica del medesimo, come si trova al presente ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#60">VII. Porta della
          detta Facciata dell'Oratorio verso la Piazza come si
          trova al presente ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#62">VIII. 1. Modinatura
          dell'architrave, fregio, e cornice della porta ancedente,
          2. Sotto architrave delli pilastri della medesima, 3.
          Sotto architrave delle colonne delle medesima ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#64">IX. 4. Capitello
          composto di nuova invenzione [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#66">X. Altra idea del
          medesimo per la detta porta non eseguita ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#68">XI. Capitello
          composto della porta antecedente [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#70">XII. Finestra del
          primo ordine con sua Pianta [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#72">XIII. Porta laterale
          della facciata dell'Oratorio ingressiva alla Portaria de'
          Padri ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#74">XIV. 1. Modinatura
          della cornice [...] 2. Cornice [...] &#160;3. Stipite con
          sua pianta della medesima ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#76">XV. Finestra con sua
          pianta sopra la porta di mezzo della detta facciata [...]
          ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#78">XVI. 1. Modinatura
          del frontespizio del antecedente finestra [...], 2.
          Pianta [...], 3. Cornice sopra il zoccolo delle medesima
          ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#80">XVII. Finestra
          laterale del secondo ordine con sua pianta della detta
          facciata dell'Oratorio [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#82">XVIII. Finestra del
          secondo ordine con sua pianta della fabrica unita alla
          detta facciata dell'Oratorio [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#84">XIX. Finestra con
          sua pianta vicina all'antecedente [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#86">XX. Nicchia grande
          con finestra, balaustrata, e pilastri nel secondo ordine
          della detta facciata [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#88">XXI. 1. Modinitura
          [...], 2. Pianta ed elevazione dello stipite della
          medesima, 3. Pianta dell'intiera balaustrata, 4. Pianta
          coll'alzata d'un balaustro [...], 5. Cornice sopra, e
          sotto la detta balaustrata ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#90">XXII. Finestra
          dell'ultimo ordine con sua pianta [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#92">XXIII. Finestra del
          terzo ordine con sue piante [...] &#160;▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#94">XXIV. 1. Modinatura
          in grande della cornice del primo ordine [...], 2.
          Architrave sotto la detta Cornice [...], 3. Cornice
          [...], 4. Cornice [...], 5. Cornice [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#96">XXV. 6. Modinatura
          della cornice del secondo ordine [...], 7. Architrave
          [...], 8. Base delli pilastri [...], 9. Cornice [...],
          10. Cornice [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#98">XXVI. Capitello, e
          base dei pilastri del secondo ordine [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#100">XXVII. Facciata
          geometrica per longo [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#102">XXVIII. Facciata
          principale del dett'orologio verso la Piazza di Monte
          [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#104">XXIX. 1. Modinatura
          sopra, e sotto il parapetto [...], 2. Cornice, ed
          Architrave [...], 3. Capitello, e pianta dello stesso, 4.
          Cornice del piedestallo [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#106">XXX. Diverso
          pensiero per l'antecedente orologio non eseguito ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#108">XXXI. Altro diverso
          pensiero [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#110">XXXII. 1. Capitello
          con sua pianta delli detti due pensieri [...], 2. Pianta
          dell'orologio eseguito [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#112">XXXIII. Porta con
          sua pianta per cui si entra dalla Portaria nell'Oratorio
          in faccia all'altare ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#114">XXXIV. 1.
          Modinatura del frontespizio dell'antecedente porta, 2.
          Pianta dello stipite delle medesima, 3. Cornice, e
          capitello delli pilastri [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#116">XXXV. Porta con sua
          pianta per dove si cala all'Oratorio dalle logge terrene
          de i Padri ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#118">XXXVI. 1.
          Modinatura del frontespizio dell'antecedente porta, 2.
          Cornice [...], 3. Pianta con l'alzata dello stipite
          [...], 4. Capitello, e base delli pilastri laterali della
          medesima ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#120">XXXVII. Porta, che
          stà al piano nobile per dove s'entra l'appartamento [...]
          ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#122">XXXVIII 1.
          Modinatura della cornice [...], 2. Cornice sopra la
          finestra della medesima, 3. Cornice, e fregio [...], 4.
          Pianta con l'alzata [...], 5. Capitello e base delli
          pilastri [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#124">XXXIV. Spaccato in
          prospettiva dell'Oratorio verso l'altare, e della
          Libraria alzata sopra detto Oratorio ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#126">XL. Altro spaccato
          in prospettiva del detto Oratorio [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#128">XLI. Elevazioni
          geometriche delle antededenti facciate dell'altare, e
          della porta con suoi sotterranei, e Libraria ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#130">XLII. Spaccato
          geometrico per longo di detto Oratorio con l'atrio[...]
          ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#132">XLIII. Loggia di
          mezzo sopra l'altare dell'antecedento Oratorio ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#134">XLIV. Loggia
          laterale dell'antecedente del detto Oratorio ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#136">XLV. Loggia sotto
          le passate laterali all'altare con sua pianta del
          medesimo Oraatorio ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#138">XLVI. 1. Modinatura
          della cornice architravata [...], 2. Cornice sopra il
          frontespizio [...], 3. Cornice sopra la balaustreta
          [...], 4. Altra cornice [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#140">XLVII. Pianta, ed
          elevazione del Pulpito, che stà nel mezzo della facciata
          destra di detto Oratorio ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#142">XLVIII. Soffitto
          della Libraria ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#144">XLIX. Atrio
          ingressivo alla scale nobili nel primo delle loggie
          terrene ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#146">L. Spaccato
          prospetico del primo cortile doppo l'Oratorio con le
          loggie a tre ordini, che gli girano d'intorno ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#148">LI. Spaccato
          geometrico del medesimo cortile, e della scala nobile
          ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#150">LII. 1. Finestra di
          mezzo [...], 2. Cornice [...], 3. Pianta [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#152">LIII. Spaccato
          prospetico del secondo cortile doppo la Sagrestia con
          logge aperte a tre ordini, che lo circondano ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#154">LIV. Spaccato
          geometrico della facciata dellìantecedente cortile [...]
          ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#156">LV. Spaccato
          geometrico dell'abitazione, e scala [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#158">LVI. Spaccato
          geometrico, che morsta tutta l'Isola della Fabrica di
          dentro per longo [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#160">LVII. Modinatura
          dell'architrave, capitello, e base de' pilastri
          degl'antecedenti cortili ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#162">LVIII. Altro
          spaccato geometrico per longo di tutta la fabrica [...]
          ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#164">LIX. Pianta
          dell'atrio, e fontane de lavamani avanti l'ingresso del
          Resettorio ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#166">LX. Facciata
          dell'ingresso al Resettorio nel detto atrio con le due
          fontane alterali ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#168">LXI. Pianta del
          Resettorio ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#170">LXII. Elevazione
          per longo di detto Resettorio ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#172">LXIII. Pianta della
          Sala, dove fanno la ricreazione i Padri ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#174">LIV. Spaccato per
          longo della detta Sala di ricreazione [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#176">LV. 1. Modinatura
          del cornicione [...], 2. Cornice che corre sopra la luce
          [...], 3. Pianta con la base [...] ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#178">LVI. Proporzione
          maggiore del detto camino con sua pianta di nuova
          invenzione ▣</a>
        </li>
        <li>
          <a href="ghbor683932002s.html#180">LVII. Veduta in
          prospettiva delle facciata della Chiesa, Oratorio ed
          abitazione deì Padri [...] ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
