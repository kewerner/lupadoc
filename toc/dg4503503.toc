<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503503s.html#8">Les marveilles de la ville de
      Rome où est traité des églises, stations, &amp; reliques des
      corps saints qui y sont</a>
    </li>
    <li>
      <a href="dg4503503s.html#9">[Traicté des Églises, Stations,
      Reliques des Corps Saincts]</a>
      <ul>
        <li>
          <a href="dg4503503s.html#10">Les sept Eglises
          principales</a>
          <ul>
            <li>
              <a href="dg4503503s.html#10">La primiere Eglise qui
              est Saint Jean de Lateran ›San Giovanni in Laterano‹
              ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#16">La seconde Eglise
              sçavoir Sain Pirre au Mont Vatican ›San Pietro in
              Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#24">La troisieme Eglise qui
              est Saint Paul ›San Paolo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#26">La quatriesme Eglise,
              qui est Sainte Marie Majeurre ›Santa Maria Maggiore‹
              ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#30">La cinquieme Eglise est
              Saint Laurens hors des Murs ›San Lorenzo fuori le
              Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#31">La siexieme Eglise qui
              est Saint Sebastien ›San Sebastiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#33">La septieme Eglise, qui
              est colle de Sainte Croix en Hierusalem ›Santa Croce
              in Gerusalemme‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#35">De la Tybre</a>
          <ul>
            <li>
              <a href="dg4503503s.html#35">Dan l'Isle ›Isola
              Tiberina‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#36">Du Tybre dit
              ›Trastevere‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4503503s.html#36">Voicy la Ripe ▣</a>
                </li>
                <li>
                  <a href="dg4503503s.html#41">Voyci le modelle de
                  ladite Fontaine [›Fontana dell'Acqua Paola (via
                  Garibaldi)‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#42">Au Bourg ›Borgo‹ ▣</a>
          <ul>
            <li>
              <a href="dg4503503s.html#42">Eglise de Saint Esprit
              ›Santo Spirito in Sassia‹ au Bourg ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#43">Pont Saint-Ange ›Ponte
              Sant'Angelo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#45">Depuis la Porte Flaminia,
          aurement du Peuple jusques au pied du Capitole ▣</a>
          <ul>
            <li>
              <a href="dg4503503s.html#46">Voyci la figure de la
              Porte du Peuple ›Porta del Popolo‹ avec l'Eglise
              ›Santa Maria del Popolo‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#48">Santissima Trinità de
              Monti ›Trinità dei Monti‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#53">Voyci l'Eglis de Saint
              Marcel ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#55">L'Eglise de la Rotonda
              ›Pantheon‹, e la Place ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#57">Saint Louys ›San Luigi
              dei Francesi‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#59">Saint Andrè de la Valle
              ›Sant'Andrea della Valle‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#62">Voyci le Palais de la
              Cancellaria ›Palazzo della Cancelleria‹, e l'Eglise
              de Saint Laurens ›San Lorenzo in Damaso‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#65">Depuis le Capitole à main
          gauche vers les Monts ▣</a>
          <ul>
            <li>
              <a href="dg4503503s.html#70">Colonne ou fut flagellè
              Nostre Seigneur ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#73">Du Capitole à main droite
          vers le monts ▣</a>
          <ul>
            <li>
              <a href="dg4503503s.html#74">Eglise de Santa Maria
              in Portico ›Santa Maria in Campitelli‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#77">Saint Alexis
              ›Sant'Alessio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#79">Les Fetes, et Stations</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503503s.html#91">La Guide Romaine pour les
      Estrangers qui viennent voir les Antiquitez de Rome, l'un
      apres l'autre disctinctement reduite en un tres bel ordre</a>
      <ul>
        <li>
          <a href="dg4503503s.html#91">La premier journee</a>
          <ul>
            <li>
              <a href="dg4503503s.html#91">Du Rion du Bourg
              ›Borgo‹</a>
            </li>
            <li>
              <a href="dg4503503s.html#92">De la du Tybre
              ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4503503s.html#92">La Dovane
              Ripegrande</a>
            </li>
            <li>
              <a href="dg4503503s.html#93">Du Pont Sainte Marie
              ›Ponte Rotto‹, du Palais de Pilate ›Casa dei
              Crescenzi‹, et d'autres choses</a>
            </li>
            <li>
              <a href="dg4503503s.html#94">Du Mont Testaceus
              ›Testaccio (Monte)‹, et d'autres choses</a>
            </li>
            <li>
              <a href="dg4503503s.html#95">Des Thermes d'Antonius
              Caracalla ›Terme di Caracalla‹, et autres choses</a>
            </li>
            <li>
              <a href="dg4503503s.html#95">De Saint Jean Lateran
              ›San Giovanni in Laterano‹, et autres choses</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#96">La seconde journee ▣</a>
          <ul>
            <li>
              <a href="dg4503503s.html#96">De la Porte du peuple
              ›Porta del Popolo‹</a>
            </li>
            <li>
              <a href="dg4503503s.html#96">Des Chevaux de marbre
              qui sont a Monte Cavallo ›Fontana di Monte Cavallo‹,
              et des Bains, où Thermes de Diocletien ›Terme di
              Diocleziano‹</a>
            </li>
            <li>
              <a href="dg4503503s.html#97">Monte Cavallo
              ›Quirinale‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#98">De la Rue Pie</a>
              <ul>
                <li>
                  <a href="dg4503503s.html#98">Places des Thermes
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503503s.html#99">De Sainte Agnes
              ›Sant'Agnese‹, et des Thermes de Diocletien ›Terme di
              Diocleziano‹, et autre choses</a>
            </li>
            <li>
              <a href="dg4503503s.html#99">Du Temple d'Isis, et
              autres choses</a>
            </li>
            <li>
              <a href="dg4503503s.html#100">Des sept Sales ›Sette
              Sale‹ e du Collissee ›Colosseo‹, et autres choses
              ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#101">Du Temple de la Paix,
              du Mont Palatin maintenant dit Palais majeur
              ›Palatino‹, et d'autres choses remarquables</a>
            </li>
            <li>
              <a href="dg4503503s.html#102">Du For de Nerva ›Foro
              di Nerva‹</a>
            </li>
            <li>
              <a href="dg4503503s.html#102">Du Capitole
              ›Campidoglio‹, et d'autres remarques</a>
            </li>
            <li>
              <a href="dg4503503s.html#103">Des Vestibules
              d'Octavia, de Septimius, et du Theatre de Pompèe
              ›Teatro di Pompeo‹</a>
            </li>
            <li>
              <a href="dg4503503s.html#103">Le Palais Farnese
              ›Palazzo Farnese‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#104">La troisieme journee</a>
          <ul>
            <li>
              <a href="dg4503503s.html#104">Des Deux Colomnes, une
              d'Antoninus Pius ›Colonna di Antonino Pio‹, et
              l'autre de Trajan ›Colonna Traiana‹</a>
              <ul>
                <li>
                  <a href="dg4503503s.html#105">La Colomne
                  d'Antoninus Pius ›Colonna di Antonino Pio‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503503s.html#106">La Colomne Trajana
                  ›Colonna Traiana‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503503s.html#107">La Rotonde, ou
              ›Pantheon‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#108">De la Place Navone
              ›Piazza Navona‹, et du Pasquin ›Statua di Pasquino‹
              ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503503s.html#110">Table des eglises de Rome</a>
    </li>
    <li>
      <a href="dg4503503s.html#116">Les Antiquites de la ville de
      Rom</a>
      <ul>
        <li>
          <a href="dg4503503s.html#116">[Dedica ai lettori] Aux
          lecteurs</a>
        </li>
        <li>
          <a href="dg4503503s.html#117">De la Structure de
          Rome</a>
        </li>
        <li>
          <a href="dg4503503s.html#120">Du circuit, et enceinte de
          Rome</a>
        </li>
        <li>
          <a href="dg4503503s.html#120">Des Portes</a>
          <ul>
            <li>
              <a href="dg4503503s.html#122">Porte Majeure ›Porta
              Maggiore‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#123">Des Ponts qui sont sur le
          Tybre, et ceux qui les ont edifièz</a>
        </li>
        <li>
          <a href="dg4503503s.html#125">Isle du Tybre ›Isola
          Tiberina‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#125">Des Monts</a>
          <ul>
            <li>
              <a href="dg4503503s.html#127">Mont Testaccio
              ›Testaccio (Monte)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#129">De la Cloaque ›Cloaca
          Maxima‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#130">Des Thermes c'est a
          sçavoir Bains, et ceux qui les ont fait batir ▣</a>
        </li>
        <li>
          <a href="dg4503503s.html#131">Des sept Sales ›Sette
          Sale‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#131">Thermes Diocletianes
          ›Terme di Diocleziano‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#132">Les Noumachies, ou fe
          faisojent les batailles navales</a>
        </li>
        <li>
          <a href="dg4503503s.html#132">Du Cirque</a>
        </li>
        <li>
          <a href="dg4503503s.html#133">Des Theatres, et ceux qui
          les edifierent</a>
          <ul>
            <li>
              <a href="dg4503503s.html#133">Theatre de Marcellus
              ›Teatro di Marcello‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#134">Des Amphitheatres, et
          leurs edificateurs</a>
        </li>
        <li>
          <a href="dg4503503s.html#134">Le Colisèe ›Colosseo‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#134">Des Places</a>
          <ul>
            <li>
              <a href="dg4503503s.html#135">Forum Boarium appellè
              aujord'huy Campo Vaccino ›Foro Boario‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#136">Des Arcs Triomphaux, à qui
          on les donnoit, et eslevoit</a>
        </li>
        <li>
          <a href="dg4503503s.html#137">Des Vestibules</a>
        </li>
        <li>
          <a href="dg4503503s.html#137">Les Trophèes, et Colomnes
          remarquables</a>
        </li>
        <li>
          <a href="dg4503503s.html#138">Des Colosses</a>
        </li>
        <li>
          <a href="dg4503503s.html#139">Des Obelisques</a>
        </li>
        <li>
          <a href="dg4503503s.html#139">Des Statues</a>
          <ul>
            <li>
              <a href="dg4503503s.html#140">De Pasquin ›Statua di
              Pasquino‹ ▣</a>
            </li>
            <li>
              <a href="dg4503503s.html#140">De Marfore ›Statua di
              Marforio‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#141">Des Chevaux</a>
        </li>
        <li>
          <a href="dg4503503s.html#141">Des Librairies</a>
        </li>
        <li>
          <a href="dg4503503s.html#141">Des Horloges</a>
        </li>
        <li>
          <a href="dg4503503s.html#142">Des Palais</a>
        </li>
        <li>
          <a href="dg4503503s.html#142">De la Maison dorèe de
          Neron ›Domus Aurea‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#143">Des autres maisons des
          Citojens</a>
        </li>
        <li>
          <a href="dg4503503s.html#144">Des Couriers, où
          Curiers</a>
        </li>
        <li>
          <a href="dg4503503s.html#144">Des petits Senats</a>
        </li>
        <li>
          <a href="dg4503503s.html#144">Des Magistrats</a>
        </li>
        <li>
          <a href="dg4503503s.html#145">Des Cornices</a>
        </li>
        <li>
          <a href="dg4503503s.html#146">Des Tribus</a>
        </li>
        <li>
          <a href="dg4503503s.html#146">Des Coutrèes, où
          Quartiers, et de leurs enseignes</a>
        </li>
        <li>
          <a href="dg4503503s.html#146">Des Basiliques</a>
        </li>
        <li>
          <a href="dg4503503s.html#147">Le Capitole ›Campidoglio‹
          ▣</a>
        </li>
        <li>
          <a href="dg4503503s.html#149">La Grecostasis
          ›Grecostasi‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#149">De l'Aerarium ›Aerarium‹,
          où chambre, et maison de Ville, et quelle mennoye
          courroit a Rome en ce temps là</a>
        </li>
        <li>
          <a href="dg4503503s.html#150">DU Greffe du Peuple
          Romain</a>
        </li>
        <li>
          <a href="dg4503503s.html#150">De l'Asile</a>
        </li>
        <li>
          <a href="dg4503503s.html#150">De ce qu'ils appoient
          Rostra ›Rostri‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#150">De la Colomne dicta
          Miliaria ›Miliarium Aureum‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#150">Du Temple de Carmenta
          ›Tempio di Carmenta‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#151">De la Colomne de Guerre
          ›Columna Bellica‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#151">De la Colomne Lattaria
          ›Columna Lactaria‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#151">De l'Esquimelium
          ›Equimelio‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#151">Du Champ de Mars ›Campo
          Marzio‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#152">De Sigillum Sororium
          ›Tigillum Sororium‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#152">Des Camps nommez
          Forastieri ›Castra Peregrina‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#152">La Ville publique, en
          Palais de Plaisance ›Villa Publica‹ ▣</a>
        </li>
        <li>
          <a href="dg4503503s.html#153">De la ›Taberna
          Meritoria‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#153">Du Parc</a>
        </li>
        <li>
          <a href="dg4503503s.html#153">Des Jardins</a>
        </li>
        <li>
          <a href="dg4503503s.html#154">Du Velabrum ›Velabro‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#154">Des Carines ›Carinae‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#155">Des Theatres, où
          Collines</a>
        </li>
        <li>
          <a href="dg4503503s.html#155">Des Praz</a>
        </li>
        <li>
          <a href="dg4503503s.html#155">Des Greniers Publics</a>
        </li>
        <li>
          <a href="dg4503503s.html#156">es Prosons publiques</a>
        </li>
        <li>
          <a href="dg4503503s.html#156">De quelque jeux, et Fetes
          qu'on faisoit dans Rome</a>
        </li>
        <li>
          <a href="dg4503503s.html#157">Des Sepulcres d'Auguste,
          d'Adrien, et de Settimius, aujourd'huy le Chateau Saint
          Ange ›Castel Sant'Angelo‹ ▣</a>
        </li>
        <li>
          <a href="dg4503503s.html#158">Des Temples</a>
        </li>
        <li>
          <a href="dg4503503s.html#159">Des Pretres, et des
          Vierges, habillimens, Vases, et autres instruments faits
          à l'usage des sacrifices. et de leurs Instituteurs</a>
        </li>
        <li>
          <a href="dg4503503s.html#161">De l'Arcenal</a>
        </li>
        <li>
          <a href="dg4503503s.html#162">De l'armèe terrestre, et
          maritime des Romains, et de leurs enseignes</a>
        </li>
        <li>
          <a href="dg4503503s.html#162">Des Triomphes, et à qui on
          les octroyoit, et qui fut le primier qui triompha, et de
          combien de sortes il y en avoit</a>
        </li>
        <li>
          <a href="dg4503503s.html#163">Des Couronnes, et à qui
          elles se donnoient</a>
        </li>
        <li>
          <a href="dg4503503s.html#163">Du nombre du peuple
          Romain</a>
        </li>
        <li>
          <a href="dg4503503s.html#164">Des Richesses du peuple
          Romain</a>
        </li>
        <li>
          <a href="dg4503503s.html#164">De la liberalitè des
          anciens Romains</a>
        </li>
        <li>
          <a href="dg4503503s.html#165">Des anciens Mariages, et
          de leurs ceremonies</a>
        </li>
        <li>
          <a href="dg4503503s.html#165">De la civilitè qu'on
          enseignoit aux enfans</a>
        </li>
        <li>
          <a href="dg4503503s.html#166">Du divorce des
          Mariages</a>
        </li>
        <li>
          <a href="dg4503503s.html#166">Des obseques antiques, et
          leurs ceremonies</a>
        </li>
        <li>
          <a href="dg4503503s.html#168">Tours</a>
        </li>
        <li>
          <a href="dg4503503s.html#168">Du Tybre</a>
        </li>
        <li>
          <a href="dg4503503s.html#169">Du Palais Papal ›Palazzo
          Apostolico Vaticano‹, et Belvedere ›Cortile del
          Belvedere‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#169">De là du Tybre
          ›Trastevere‹</a>
        </li>
        <li>
          <a href="dg4503503s.html#170">Recapitulation des
          Antiquitès</a>
        </li>
        <li>
          <a href="dg4503503s.html#171">Des Temples des anciens
          hors de Rome</a>
        </li>
        <li>
          <a href="dg4503503s.html#172">Combien de fois Rome à etp
          prise</a>
        </li>
        <li>
          <a href="dg4503503s.html#173">Addition aux Antiuitès de
          Rome</a>
          <ul>
            <li>
              <a href="dg4503503s.html#174">Conti</a>
            </li>
            <li>
              <a href="dg4503503s.html#174">Borghese</a>
            </li>
            <li>
              <a href="dg4503503s.html#174">Mont Dràgon</a>
            </li>
            <li>
              <a href="dg4503503s.html#175">Du Jardin d'Este dans
              la Ville de Tivoli</a>
            </li>
            <li>
              <a href="dg4503503s.html#176">Du Jardin de
              Bagnaja</a>
            </li>
            <li>
              <a href="dg4503503s.html#176">Du Jardin de
              Caprarole</a>
            </li>
            <li>
              <a href="dg4503503s.html#176">Du Jardin, et du
              magnifique Palais du Marquis Ginetti</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503503s.html#177">[Ensemble les noms des Papes,
      Empereurs, &amp; autres Princes Chrestiens]</a>
      <ul>
        <li>
          <a href="dg4503503s.html#177">Table des Papes</a>
        </li>
        <li>
          <a href="dg4503503s.html#185">Les Roys et Empereurs
          romains</a>
          <ul>
            <li>
              <a href="dg4503503s.html#185">Les Roys</a>
            </li>
            <li>
              <a href="dg4503503s.html#185">Les empereurs</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503503s.html#187">Les Roys de France</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503503s.html#189">Mesures du Circuit de la Ville
      de Rome</a>
    </li>
    <li>
      <a href="dg4503503s.html#193">Table de ce qui est
      remarquable en ce Livre</a>
    </li>
    <li>
      <a href="dg4503503s.html#196">Guide de chemins de Rome, aux
      principales Villes d'Italie, et lieux circonvoisins, avec le
      voiage de Saint Jaques en Galice</a>
    </li>
  </ul>
  <hr />
</body>
</html>
