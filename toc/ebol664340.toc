<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebol664340s.html#6">Le Sculture delle Porte della
      Basilica di San Petronio in Bologna&#160;</a>
      <ul>
        <li>
          <a href="ebol664340s.html#8">[Dedica dell'autore al
          Cardinale Carlo Oppizzoni] All'eminentissomo e
          reverendissimo Signor Cardinale Carlo Oppizzoni
          Arcivescovo, ed Arcicancelliere della Pontificia
          Università di Bologna [...]&#160;</a>
        </li>
        <li>
          <a href="ebol664340s.html#10">[Dedica dell'autore delle
          illustrazioni ai lettori] L'autore delle illustrazioni a
          chi legge</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ebol664340s.html#12">[Text]</a>
      <ul>
        <li>
          <a href="ebol664340s.html#12">Parte prima</a>
          <ul>
            <li>
              <a href="ebol664340s.html#12">Intorno alle Sculture
              della Porta di mezzo</a>
            </li>
            <li>
              <a href="ebol664340s.html#14">Descrizione delle
              Sculture della Porta di mezzo</a>
            </li>
            <li>
              <a href="ebol664340s.html#18">Annotazioni alla
              Prima Parte&#160;</a>
            </li>
            <li>
              <a href="ebol664340s.html#21">Sommario della Tavole
              relative alla Prima Parte&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol664340s.html#22">Parte seconda</a>
          <ul>
            <li>
              <a href="ebol664340s.html#22">Intorno alle Sculture
              delle Porte piccole</a>
            </li>
            <li>
              <a href="ebol664340s.html#30">Annotazioni alla
              Seconda Parte&#160;</a>
            </li>
            <li>
              <a href="ebol664340s.html#34">Sommario delle Tavole
              relative alla Seconda Parte&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol664340s.html#36">Appendice de'
          documenti</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ebol664340s.html#46">[Tavole]</a>
      <ul>
        <li>
          <a href="ebol664340s.html#46">Prima Parte</a>
          <ul>
            <li>
              <a href="ebol664340s.html#46">I. Creazione di Adamo
              ▣&#160;</a>
            </li>
            <li>
              <a href="ebol664340s.html#48">II. Creazione di Eva
              ▣&#160;</a>
            </li>
            <li>
              <a href="ebol664340s.html#50">III. Disubbidienza
              de' primi Parenti ▣&#160;</a>
            </li>
            <li>
              <a href="ebol664340s.html#52">IV. Cacciata dal
              Terrestre Paradiso ▣&#160;</a>
            </li>
            <li>
              <a href="ebol664340s.html#54">V. Adamo ed Eva nello
              stato di condonanna ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#56">VI. Sacrificj di
              Caiano ed Abele ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#58">VII. Caino uccide
              Abele ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#60">VIII. Noè, cessato il
              Diluvio, esce dall'Area ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#62">VIIII. [sic] Noè
              deriso da Cam suo figlio ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#64">X. Sagrificio di
              Abramo ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#66">XI. Natività di
              Nostro Signore Gesù Cristo ▣&#160;</a>
            </li>
            <li>
              <a href="ebol664340s.html#68">XII. Adorazione de'
              Magi ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#70">XIII. Presentazione
              di Gesù al Tempio ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#72">XIIII. [sic] Fuga in
              Egitto ▣&#160;</a>
            </li>
            <li>
              <a href="ebol664340s.html#74">XV. Strage
              degl'innocenti fanciulli ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#76">XVI. - XXIV. Dio
              Padre, e trentadue Patriarchi e Profeti in mezze
              figure ▣&#160;</a>
            </li>
            <li>
              <a href="ebol664340s.html#94">XXV. Nostra Donna, e
              li Santi Petronio ed Ambrogio, statue di tutto tondo
              ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#96">XXVI. Prospetto della
              Porta di mezzo ▣</a>
            </li>
            <li>
              <a href="ebol664340s.html#98">XXVII., XXVIII, XXIX.
              Dettagli in grande de'fogliami ed ornamenti della
              suddetta Porta ▣&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol664340s.html#110">Seconda Parte</a>
          <ul>
            <li>
              <a href="ebol664340s.html#110">Porta
              destra&#160;</a>
              <ul>
                <li>
                  <a href="ebol664340s.html#110">I.
                  Trasfigurazione di Nostro Signore Gesù Cristo
                  ▣&#160;</a>
                </li>
                <li>
                  <a href="ebol664340s.html#112">II. Battesimo di
                  Cristo ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#114">III. Giuseppe
                  calato nella secca cisterna da' suoi Fratelli
                  ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#116">IIII. [sic]
                  Giuseppe venduto all'Ismaelita ▣&#160;</a>
                </li>
                <li>
                  <a href="ebol664340s.html#118">V. I Fratelli di
                  Giuseppe in atto di uccidere un capretto per
                  itingerne col sangue la veste di lui ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#120">VI. I medisimi,
                  che presentano a Giacobbe la veste insanguinata
                  di Giuseppe ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#122">VII. Coronazione
                  di Maria Vergine ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#124">VIII.
                  Flagellazione di Cristo ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#126">VIIII. [sic] I
                  sogni del Coppiere, e del Panatiere di Faraone,
                  interpretati da Giuseppe ▣&#160;</a>
                </li>
                <li>
                  <a href="ebol664340s.html#128">X. La tazza
                  nascosta per ordine di Giuseppe frammezzo al
                  grano comperato in Egitto da' suoi fratelli ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#130">XI. Soggetto
                  incerto, che sembra rappresentare il ratto di una
                  donna ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#132">XII. La tazza
                  nascosta si scuopre entro il sacco di Beniamino
                  ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#134">XIII. La lavanda
                  de' piedi ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#136">XIIII. [sic]
                  L'ultima cena Cena del Redentore ▣&#160;</a>
                </li>
                <li>
                  <a href="ebol664340s.html#138">XV. L'Orazione
                  nell'orto di Getsemani ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#140">XVI. Il bacio di
                  Giuda ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#142">XVII. Gesù
                  tratto davanti a' Tribunali ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#144">XVIII. - XXIX.
                  Sibille, es Angioletti della Porta destra ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ebol664340s.html#168">Porta sinistra</a>
              <ul>
                <li>
                  <a href="ebol664340s.html#168">I. Il Paralitico
                  alla Probatica Piscina sanato dal Redentore ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#170">II. Nascimento
                  di Nostro Signore Gesù Cristo ▣&#160;</a>
                </li>
                <li>
                  <a href="ebol664340s.html#172">III. Giuseppe
                  interpreta il sogno delle spiche, e giovenche
                  vedute da Faraone ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#174">IIII. [sic]
                  Lotta dell'Angiolo con Giacobbe ▣&#160;</a>
                </li>
                <li>
                  <a href="ebol664340s.html#176">V. Il cieco
                  Isacco benedice Giacobbe in vece di Esaù ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#178">VI. Storia della
                  Moglie del Levita descritta al Capo XIX del
                  Genesi ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#180">VII. Soggetto
                  d'incerta applicazione, che però rappresenta un
                  miracolo del Redentore ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#182">VIII. Visità di
                  Nostra Donna ad Elisabetta ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#184">IX. Lot fugge
                  dall'incendio di Soddoma colla sua Famiglia ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#186">X. Nascimento di
                  Esaù e di Giacobbe gemelli figli d'Isacco
                  ▣&#160;</a>
                </li>
                <li>
                  <a href="ebol664340s.html#188">XI.
                  Ritrovamento, e sponsali di Rebecca ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#190">XII. Il
                  fanciullo Mosè esposto dal Re Faraone
                  all'esperimento della scelta della gemma, o del
                  carbone ardente; storia apocrifa descritta alla
                  pagine 20 ▣&#160;</a>
                </li>
                <li>
                  <a href="ebol664340s.html#192">XIII. Gesù
                  risorto appare alla Maddalena in forma di
                  ortolano ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#194">XIIII. [sic] La
                  Maddalena addita festosa alle altre pietose donne
                  il risorto Redentore ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#196">XV. I due
                  Discepoli andando in Emaus s'accompagnano col
                  Divino Maestro senza riconoscerlo ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#198">XVI. I medesimi
                  riconoscono il Redentore nella frazione del pane
                  ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#200">XVII.
                  Incredulità di Tommaso ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#202">XVIII. - XXIX.
                  Sibille, ed Angioletti della Porta sinistra ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ebol664340s.html#226">Storie nella
              residenza degli Officiali della Fabbrica</a>
              <ul>
                <li>
                  <a href="ebol664340s.html#226">XXX. Soggeto
                  incerto, descritto alla pagina 19 ▣&#160;</a>
                </li>
                <li>
                  <a href="ebol664340s.html#228">XXXI. Giuseppe
                  tentato dalla Moglie di Putifarre ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#230">XXXII. Fabbrica
                  dell'Arca per ordine di Noè ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#232">XXXIII.
                  Sepoltura di Abramo ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ebol664340s.html#234">Statue nella Porta
              destra</a>
              <ul>
                <li>
                  <a href="ebol664340s.html#234">XXXIV. Gesù,
                  morto in braccio a Nicodemo, la Vergine, e San
                  Giovanni l'Evangelista ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ebol664340s.html#236">Statue nella Porta
              sinistra</a>
              <ul>
                <li>
                  <a href="ebol664340s.html#236">XXXV.
                  Risurrezione di Cristo, composizione di varie
                  figure ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#238">XXXVI.; XXXVII.
                  Prospetti nel loro intero di amendue le Porte
                  ▣</a>
                </li>
                <li>
                  <a href="ebol664340s.html#242">XXXVIII. -
                  XXXXIV. Saggio, e dettaglio in grande degl'intali
                  di amendue le prefate Porte ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
