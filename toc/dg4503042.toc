<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503042s.html#14">Romae Antiquae Notitia</a>
      <ul>
        <li>
          <a href="dg4503042s.html#16">[Dedica] To His Highness
          the Duke of Glocester</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503042s.html#20">[Text]</a>
      <ul>
        <li>
          <a href="dg4503042s.html#20">The preface</a>
        </li>
        <li>
          <a href="dg4503042s.html#24">Contents</a>
        </li>
        <li>
          <a href="dg4503042s.html#32">I. Of the Roman
          Learning</a>
        </li>
        <li>
          <a href="dg4503042s.html#46">II. Of the Roman
          Education</a>
        </li>
        <li>
          <a href="dg4503042s.html#62">Part I. The Original,
          Growth, and Decay of the Roman Common-wealth</a>
          <ul>
            <li>
              <a href="dg4503042s.html#62">I. Of the Building of
              the City</a>
            </li>
            <li>
              <a href="dg4503042s.html#65">II. Of the Roman
              Affairs undert the Kings</a>
            </li>
            <li>
              <a href="dg4503042s.html#67">III. Of the Roman
              Affairs, from the beginning of the Consular
              Governament, to the first Punic War</a>
            </li>
            <li>
              <a href="dg4503042s.html#71">IV. Of the Roman
              Affairs, from the beginning of the first Punic War,
              to the first Triumvirate</a>
            </li>
            <li>
              <a href="dg4503042s.html#75">V. Of the Roman
              Affairs, from the beginning of the first Triumvirate
              to the end of the Twelve Caesars</a>
            </li>
            <li>
              <a href="dg4503042s.html#82">VI. Of the Roman
              Affairs from Domitian to the end of Constantine the
              Great</a>
            </li>
            <li>
              <a href="dg4503042s.html#89">VII. Of the Roman
              Affairs from Costantine the Great, to the taking of
              Rome by Odoacer, and the Ruine of the Western
              Empire</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503042s.html#94">Part II. Of the City</a>
          <ul>
            <li>
              <a href="dg4503042s.html#93">Roma Antiqua ◉</a>
            </li>
            <li>
              <a href="dg4503042s.html#94">I. Of the City</a>
              <ul>
                <li>
                  <a href="dg4503042s.html#94">I. Of the
                  Pomoerium, and of the Form and Bigness of the
                  City, according to the Seven Hills</a>
                </li>
                <li>
                  <a href="dg4503042s.html#99">II. Of the Division
                  of the City into tribes and Regions: And of the
                  Gates and Bridges</a>
                </li>
                <li>
                  <a href="dg4503042s.html#103">III. Of the Places
                  of Worship; particularly of the Temples and
                  Luci</a>
                  <ul>
                    <li>
                      <a href="dg4503042s.html#105">Capitolium
                      ›Campidoglio‹ ▣</a>
                    </li>
                    <li>
                      <a href="dg4503042s.html#105">Templum
                      ›Pantheon‹ vulgo Rotunda ▣</a>
                    </li>
                    <li>
                      <a href="dg4503042s.html#111">Theatrum
                      Cornelii Balbi Gaditani ›Theatrum Balbi‹
                      ▣</a>
                    </li>
                    <li>
                      <a href="dg4503042s.html#111">Amphitheatrum
                      Claudii ›Colosseo‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4503042s.html#112">IV. Of the
                  Theatres, Amphitheatres, Circos, Naumachiae,
                  Odea, Stadia and Xysti, and of the Campus Martius
                  ›Campo Marzio‹</a>
                  <ul>
                    <li>
                      <a href="dg4503042s.html#116">Naumachiae Id
                      Est Navalis Pugnae ▣</a>
                    </li>
                    <li>
                      <a href="dg4503042s.html#119">Circi et
                      quinque Ludicrorum Circensium ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4503042s.html#120">V. Of the Curiae,
                  Senacula, Basilicae, Fora and ›Comitium‹</a>
                </li>
                <li>
                  <a href="dg4503042s.html#124">VI. Of the
                  Portico's, Arches, Columns, and Trophies</a>
                </li>
                <li>
                  <a href="dg4503042s.html#129">VII. Of the
                  Bagnio's, Aquaeducts, Cloacae, and Publick (sci!)
                  Ways</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503042s.html#134">II. Of the Religion of
              the Romans</a>
              <ul>
                <li>
                  <a href="dg4503042s.html#134">I. Of the Religion
                  and Morality of the Romans in general</a>
                </li>
                <li>
                  <a href="dg4503042s.html#137">II. Of the
                  Luperci, Lupercalia, et ceetera. Of the Potitii
                  and Pinarii; and of the Arval Borthers</a>
                </li>
                <li>
                  <a href="dg4503042s.html#140">III. Of the
                  Augurs, Auguries, et cetera.</a>
                </li>
                <li>
                  <a href="dg4503042s.html#142">IV. Of the
                  Aruspices and Pontifices</a>
                </li>
                <li>
                  <a href="dg4503042s.html#145">V. Of the
                  Flamines, Rex Sacrorum, Salii, Feciales and
                  Sodales</a>
                </li>
                <li>
                  <a href="dg4503042s.html#150">VI. Of the
                  Vestals</a>
                </li>
                <li>
                  <a href="dg4503042s.html#152">VII. Of the
                  Duumviri, Decemviri and Quindecemviri, Keepers of
                  the Sibillyine Writings, and of the Corybantes or
                  Priests of Cybele, and the Epulones</a>
                </li>
                <li>
                  <a href="dg4503042s.html#157">VIII. Of the Roman
                  Sacrifices</a>
                  <ul>
                    <li>
                      <a href="dg4503042s.html#158">Antiquorum
                      Sacrificandiritus ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4503042s.html#161">IX. Of the Roman
                  Year</a>
                </li>
                <li>
                  <a href="dg4503042s.html#164">X. The distinction
                  of the Roman Days</a>
                </li>
                <li>
                  <a href="dg4503042s.html#166">XI. Of the
                  Kalends, Nones, and Ides</a>
                </li>
                <li>
                  <a href="dg4503042s.html#167">XII. The most
                  Remarkable Festivals of the Romans as they stand
                  in the Kalendar</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503042s.html#172">III. Of the Civil
              Government of the Romans</a>
              <ul>
                <li>
                  <a href="dg4503042s.html#172">I. Of the general
                  Division of the People</a>
                </li>
                <li>
                  <a href="dg4503042s.html#176">II. Of the
                  Senate</a>
                </li>
                <li>
                  <a href="dg4503042s.html#180">III. Of the
                  general Divisions of the Magistrates; and of the
                  Candidates for Offices</a>
                </li>
                <li>
                  <a href="dg4503042s.html#182">IV. Of the
                  Consuls</a>
                </li>
                <li>
                  <a href="dg4503042s.html#184">V. Of the Dictator
                  and his Master of the Horse</a>
                </li>
                <li>
                  <a href="dg4503042s.html#186">VI. Of the
                  Praetors</a>
                </li>
                <li>
                  <a href="dg4503042s.html#187">VII. of the
                  Censors</a>
                </li>
                <li>
                  <a href="dg4503042s.html#189">VIII. Of the
                  Quaestors</a>
                </li>
                <li>
                  <a href="dg4503042s.html#190">IX. Of the
                  Tribunes of the People</a>
                </li>
                <li>
                  <a href="dg4503042s.html#191">X. Of the
                  Aediles</a>
                </li>
                <li>
                  <a href="dg4503042s.html#192">XI. Of the
                  Decemviri</a>
                </li>
                <li>
                  <a href="dg4503042s.html#194">XII. Tribuni
                  Militum Consulari potestate</a>
                </li>
                <li>
                  <a href="dg4503042s.html#195">XIII. Civil
                  Officers of less Note, or of less frequent
                  Occurrence in Authors; together with the Publick
                  Servants</a>
                </li>
                <li>
                  <a href="dg4503042s.html#199">XIV. Of the Civil
                  Government</a>
                </li>
                <li>
                  <a href="dg4503042s.html#202">XV. Of the
                  Provincial Praetors and Propraetors; of the
                  Legati, Quaestors, and Proquaestors</a>
                </li>
                <li>
                  <a href="dg4503042s.html#203">XVI. Of the
                  Comitia</a>
                </li>
                <li>
                  <a href="dg4503042s.html#209">XVII. Of the Roman
                  Judgements; and first of Private Judgements</a>
                </li>
                <li>
                  <a href="dg4503042s.html#212">XVIII. Of Publick
                  Judgements</a>
                </li>
                <li>
                  <a href="dg4503042s.html#216">XIX. Judgements of
                  the whole People</a>
                </li>
                <li>
                  <a href="dg4503042s.html#218">XX. Of the Roman
                  Punishments</a>
                </li>
                <li>
                  <a href="dg4503042s.html#223">XXI. Of the Roman
                  Laws in general</a>
                </li>
                <li>
                  <a href="dg4503042s.html#225">XXII. Of the Laws
                  in particular; and first of those relating to
                  Religion</a>
                </li>
                <li>
                  <a href="dg4503042s.html#227">XXIII. Laws
                  relating to the Rights and Privileges of the
                  Roman Citizens</a>
                </li>
                <li>
                  <a href="dg4503042s.html#229">XXIV. Laws
                  concerning Meetings and Assemblies</a>
                </li>
                <li>
                  <a href="dg4503042s.html#231">XXV. Laws relating
                  to the Senate</a>
                </li>
                <li>
                  <a href="dg4503042s.html#232">XXVI. Laws
                  relating to the Magistrates</a>
                </li>
                <li>
                  <a href="dg4503042s.html#235">XXVII. Laws
                  relating to Publick Constitutions, Laws, and
                  Privileges</a>
                </li>
                <li>
                  <a href="dg4503042s.html#236">XXVIII. Laws
                  relating to the Provinces, and the Governors of
                  them</a>
                </li>
                <li>
                  <a href="dg4503042s.html#238">XXIX. Leges
                  Agrariae, or Laws realting to the Division of
                  Lands among the People</a>
                </li>
                <li>
                  <a href="dg4503042s.html#240">XXX. Laws relating
                  to Corn</a>
                </li>
                <li>
                  <a href="dg4503042s.html#241">XXXI. Laws for the
                  regulating of Expences</a>
                </li>
                <li>
                  <a href="dg4503042s.html#243">XXXII. Laws
                  relating to Martial Affairs</a>
                </li>
                <li>
                  <a href="dg4503042s.html#244">XXXIII. De
                  Tutelis, or Laws concerning Wardships</a>
                </li>
                <li>
                  <a href="dg4503042s.html#245">XXXIV. Laws
                  concerning Wills, Heirs, and Legacies</a>
                </li>
                <li>
                  <a href="dg4503042s.html#245">XXXV. Laws
                  concerning Money, Usury, et cetera</a>
                </li>
                <li>
                  <a href="dg4503042s.html#246">XXXVI. Laws
                  concerning the Judges</a>
                </li>
                <li>
                  <a href="dg4503042s.html#248">XXXVII. Laws
                  relating to Judgements</a>
                </li>
                <li>
                  <a href="dg4503042s.html#248">XXXVIII. Laws
                  realting to Crimes</a>
                </li>
                <li>
                  <a href="dg4503042s.html#255">XXXIX. Miscellany
                  Laws not spoken of under the general Heads</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503042s.html#258">IV. The Roman Art of
              War</a>
              <ul>
                <li>
                  <a href="dg4503042s.html#258">I. The Levies of
                  the Roman Foot</a>
                </li>
                <li>
                  <a href="dg4503042s.html#260">II. The Levy, and
                  Review, of the Cavalry</a>
                </li>
                <li>
                  <a href="dg4503042s.html#263">III. The Military
                  Oath, and the Levies of the Confederates</a>
                </li>
                <li>
                  <a href="dg4503042s.html#264">IV. of the
                  Evocati</a>
                </li>
                <li>
                  <a href="dg4503042s.html#265">V. The several
                  kinds of the Roman Foot; and their Division into
                  Manipuli, Cohorts, and Legions</a>
                </li>
                <li>
                  <a href="dg4503042s.html#267">VI. The Division
                  of the Cavalry, and of the Allies</a>
                </li>
                <li>
                  <a href="dg4503042s.html#268">VII. The Officers
                  in the Roman Army; and first of the Centurions
                  and Tribunes; with the Commanders of the Horse,
                  and of the Confederate Forces</a>
                </li>
                <li>
                  <a href="dg4503042s.html#271">VIII. The Legati,
                  and the Imperator, or General</a>
                </li>
                <li>
                  <a href="dg4503042s.html#274">IX. Of the Roman
                  Arms and Weapons</a>
                </li>
                <li>
                  <a href="dg4503042s.html#278">X. The Order of
                  the Roman Army drawn up in Battalia</a>
                </li>
                <li>
                  <a href="dg4503042s.html#284">XI. The Ensigns
                  and Colours; the Musick; the Word in Engagements;
                  the Harangues of the General</a>
                </li>
                <li>
                  <a href="dg4503042s.html#287">XII. The Form and
                  Division of the Roman Camp</a>
                  <ul>
                    <li>
                      <a href="dg4503042s.html#288">Iconismus
                      Aciei Vulgate ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4503042s.html#291">XIII. Of the
                  Duties, Works, and Exercises of the Soldiers</a>
                </li>
                <li>
                  <a href="dg4503042s.html#296">XIV. Of the
                  Soldiers Pay</a>
                </li>
                <li>
                  <a href="dg4503042s.html#298">XV. Of the
                  Military Punishments</a>
                </li>
                <li>
                  <a href="dg4503042s.html#299">XVI. Of the
                  Military Rewards</a>
                  <ul>
                    <li>
                      <a href="dg4503042s.html#300">[Corone
                      trionfali] ▣</a>
                    </li>
                    <li>
                      <a href="dg4503042s.html#307">Iconismus
                      Triumphi ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4503042s.html#312">XVII. The Roman
                  Way of declaring War, and of making Leagues</a>
                </li>
                <li>
                  <a href="dg4503042s.html#314">XVIII. Th Roman
                  Method of treating the People they conquered;
                  with the Constitution of the Coloniae, Municipia,
                  Praefecturae, and Provinces</a>
                </li>
                <li>
                  <a href="dg4503042s.html#318">XIX. The Roman Way
                  of Taking Towns; with the most remarkable
                  Inventions and Engines made use of in their
                  Sieges</a>
                </li>
                <li>
                  <a href="dg4503042s.html#322">XX. The Naval
                  Affairs of the Romans</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503042s.html#330">V. Miscellany Customs
              of the Romans</a>
              <ul>
                <li>
                  <a href="dg4503042s.html#330">I. Of the Private
                  Sports and Games</a>
                </li>
                <li>
                  <a href="dg4503042s.html#335">II. Of the
                  Circensian Shows; and first od the Pentathlum,
                  the Chariot Races, the Ludus Trojae, and the
                  Pyrrhica Saltatio</a>
                </li>
                <li>
                  <a href="dg4503042s.html#348">III. Of the Shows
                  of Wild Beasts, and of the Naumachiae</a>
                </li>
                <li>
                  <a href="dg4503042s.html#353">IV. Of the
                  Gladiatores</a>
                </li>
                <li>
                  <a href="dg4503042s.html#365">V. Of the Ludi
                  Scenici, or Stage Plays [...]</a>
                </li>
                <li>
                  <a href="dg4503042s.html#369">VI. Of the Roman
                  Tragedy and Comedy</a>
                </li>
                <li>
                  <a href="dg4503042s.html#379">VII. of the
                  Sacred, Votive, and Funeral Games</a>
                </li>
                <li>
                  <a href="dg4503042s.html#389">VIII. Of the Roman
                  Habit</a>
                </li>
                <li>
                  <a href="dg4503042s.html#413">IX. Of the Roman
                  Marriages</a>
                </li>
                <li>
                  <a href="dg4503042s.html#421">X. Of the Roman
                  Funerals</a>
                  <ul>
                    <li>
                      <a href="dg4503042s.html#424">Consecratio
                      sive Indignatio Imperatoris post Obitum ▣</a>
                    </li>
                    <li>
                      <a href="dg4503042s.html#424">Ordo Funeris
                      ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4503042s.html#454">IX. (sic!) Of the
                  Roman Entertainments</a>
                </li>
                <li>
                  <a href="dg4503042s.html#459">XII. Of the Roman
                  Names</a>
                </li>
                <li>
                  <a href="dg4503042s.html#461">XIII. Of the Roman
                  Money</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503042s.html#466">Index</a>
        </li>
        <li>
          <a href="dg4503042s.html#484">Scriptores Qui in duodecim
          Tomis Thesauri [...]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
