<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502270as.html#12">Ritratto di Roma antica</a>
      <ul>
        <li>
          <a href="dg4502270as.html#10">[Antiporta] ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#14">All'illustrissimo et
          eccellentissimo Signore, il Signor Don Hercole
          Trivultio</a>
        </li>
        <li>
          <a href="dg4502270as.html#16">[Versi]
          All'illustrissimo, et eccellentissimo Signore il Signor
          Don Hercole Trivultio</a>
        </li>
        <li>
          <a href="dg4502270as.html#17">Pompilio Totti da Cerreto
          nell'Umbria. Alli signori lettori.</a>
        </li>
        <li>
          <a href="dg4502270as.html#18">[Imprimatur]</a>
        </li>
        <li>
          <a href="dg4502270as.html#19">[Immagine] ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502270as.html#20">Tavola delle cose che si
      trattano in quest'opera.</a>
    </li>
    <li>
      <a href="dg4502270as.html#28">[Testo dell'opera]</a>
      <ul>
        <li>
          <a href="dg4502270as.html#28">Geneologia [sic] di
          Romolo ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#32">Del ›Fico Ruminale‹ ▣, de
          la ›Casa di Faustulo‹ ▣, di quella di Catelina ›Domus
          Catilinae‹ ▣, e di Scauro ›Casa di Scauro‹ ▣, e de la
          ›Velia‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#34">De la forma, e circuito
          di Roma fatto da Romolo ›Muro di Romolo‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#35">De le porte di Roma
              al tempo di Romolo</a>
            </li>
            <li>
              <a href="dg4502270as.html#36">Del vario circuito di
              Roma nel tempo del Re, e de' Consoli</a>
            </li>
            <li>
              <a href="dg4502270as.html#38">Del vago circuito di
              Roma nel tempo de gli Imperadori</a>
            </li>
            <li>
              <a href="dg4502270as.html#43">De le porte
              generalmente</a>
            </li>
            <li>
              <a href="dg4502270as.html#46">Del sito di Roma</a>
            </li>
            <li>
              <a href="dg4502270as.html#48">Delle porte, che sono
              hoggi</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#51">Romolo primo Re ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#53">De le Tavole, o
              vogliamo dire libri publici</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#55">Del ›Campidoglio‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#57">[Immagine] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#58">Del'Asilo [sic] ›Asilo di
          Romolo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#60">Numa Pompilio secondo Re
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#61">Tullo Hostilio terzo
          Re</a>
        </li>
        <li>
          <a href="dg4502270as.html#62">Anco Martio quarto Re</a>
        </li>
        <li>
          <a href="dg4502270as.html#62">Tarquinio quinto Re</a>
        </li>
        <li>
          <a href="dg4502270as.html#63">Servio Tullo sesto Re</a>
        </li>
        <li>
          <a href="dg4502270as.html#63">Tarquinio Superbo
          settimo, et ultimo Re</a>
        </li>
        <li>
          <a href="dg4502270as.html#65">Sposalitio de' Gentili
          Romani ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#67">Sposa al Marito ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#69">Questo instrumento di
          Tripodi si trova in casa del Signor Cavaliero Gualdo,
          siccome se ne tratta nelli ›Bagni di Paolo Emilio‹ a suo
          luogo. ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#71">Del Gregostasi
          ›Grecostasi‹ ▣, del ›Tempio della Concordia‹ ▣, del
          Senatulo ›Senaculum‹ ▣, e de la Basilica d'Opimio
          ›Basilica Opimia‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#73">Del ›Tempio di Bellona‹
          ▣, e de la Colonna chiamata Bellica ›Columna Bellica‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#75">Ragionamento che faceva
          il Trionfante a i Soldati ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#78">Delle Insegne Militari
          del Popolo Romano ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#81">Del le Corone ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#85">Sacrificio Militare ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#87">Instrumenti de'
          Sacrificij ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#90">Colonna Miliaria
          ›Miliarium Aureum‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#92">Della ›Colonna Menia‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#94">Della ›Curia Hostilia‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#96">Della Colonna Rostrata
          ›Columna Rostrata C. Dulii (Forum)‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#99">Porti de Romani per Mare
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#102">Delli Colossi, e della
          forma loro ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#106">De la ›statua di
          Marforio‹ ▣, e de la segretaria del Popolo Romano
          ›Secretarium Senatus‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#108">De' Tempij della Fede
          ›Fides, Templum‹ ▣, et d'Apollo ›Tempio di Apollo Aziaco‹
          ▣, e della Libraria Palatina ›Biblioteca Palatina‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#111">Di ›Roma quadrata‹ ▣, e
          de' ›Bagni Palatini‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#113">Palazzo d'Augusto, o
          vero Maggiore ›Domus Augustana‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#115">Vestigi del Monte
          ›Palatino‹, e del Palazzo Maggiore ›Domus Augustana‹ ▣, e
          sua porta dalla parte di Cerchio ›Circo Massimo‹</a>
        </li>
        <li>
          <a href="dg4502270as.html#118">De la Casa di Pomponio
          Attico ›Domus Pomponii‹ ▣, e di quella di Flavio Sabino
          ›Domus Flavia (Quirinale)‹ ▣, e del ›Tempio di Quirino‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#121">Del tempio ›Tempio di
          Vesta (Foro Romano)‹ ▣, e selva della Dea Vesta ›Lucus
          Vestae‹ ▣, e del Palazzo di Numa Pompilio ›Regia‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#121">Sepolcri Vestali
              ›Atrium Vestae‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#123">Dell'Argileto
          ›Argiletum‹, della Casa di Spurio Melio ›Aequimaelium‹ ▣,
          di quella di Scipione Africano ›Casa di Scipione
          l'Africano‹ ▣, dell'Equimelio ›Aequimaelium‹, de la
          Basilica di Sempronio ›Basilica Sempronia‹, e dell'Asilo
          ›Asilo di Romolo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#125">De la ›Casa di Servio
          Tullio‹ ▣. De la Casa di Nerone ›Domus Aurea‹ ▣, et del
          Tempio de la Fortuna ›Fortuna Seiani, Aedes‹</a>
        </li>
        <li>
          <a href="dg4502270as.html#128">Della ›Torre delle
          Milizie‹ ▣, e della casa delli Cornelij ›Domus L.
          Cornelius Pusio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#130">De l'›Argine di
          Tarquinio‹ ▣, de la ›Casa di Pompeo‹ ▣, et di Virgilio
          ›Casa di Virgilio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#132">Ornitone, ò vero
          Uccelliera ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#134">Del monte ›Celio‹ ▣, e
          del monte Celiolo ›Caeliolus‹ ▣, e delle cose
          appartenenti à quelli</a>
          <ul>
            <li>
              <a href="dg4502270as.html#134">Alloggiamenti de
              Soldati forastieri ›Castra Peregrina‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#134">Alogiamenti [sic] di
              Albani ›Mansiones Albanae‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#139">Del ›Campo Marzio‹ ▣, e
          del Campo d'Agrippa ›Campus Agrippae‹ ▣, e del Tempio de
          i Lari, ò vogliamo dir Dei domestici ›Tempio dei Lari
          Permarini‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#142">Della Corte ›Curia
          Pompeia‹ ▣, de' Portici di Pompeo ›Portico di Pompeo‹ ▣,
          e del ›Portico di Ottavia‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#142">Tempio di Venere
              Vittrice [sic] ›Venus Victrix, Aedes‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#145">Del monte ›Vaticano‹, e
          de gli horti ›Prata Quinctia‹ di quello</a>
          <ul>
            <li>
              <a href="dg4502270as.html#145">Horti di Nerone
              ›Horti Neroniani‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#145">Tempio di Apollo
              ›Cappella di Santa Petronilla‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#145">Tempio di Marte
              ›Santa Maria della Febbre‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#145">Campo Vaticano ›Ager
              Vaticanus‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#148">Del Monte Ianicolo
          ›Gianicolo‹ ▣, et del li luoghi che gli sono al
          intorno</a>
        </li>
        <li>
          <a href="dg4502270as.html#152">De la sepoltura di Numa
          ›Sepulcrum Numae Pompilii‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#152">Horti di Martiale
              ›Horti di Giulio Marziale‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#152">Tribunale di Aurelio
              ›Tribunal Aurelium‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#154">Del Monte ›Aventino‹
          ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#154">Tempio della
              Vittoria ›Victoria, Aedes‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#154">Tempio della Luna
              ›Luna, Aedes‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#154">›Tempio di Giunone
              Regina‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#154">›Tempio di Libertas‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#156">Del Colle de gli Horti
          ›Pincio‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#156">Sepolcro di Nerone
              ›Sepulcrum Domitii‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#156">Casa di Pincio
              Senatore ›Domus Pinciana‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#158">Del ›Foro Romano‹ hor
          detto Campo Vaccino ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#158">A ›Arco di Settimio
              Severo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">B ›Tempio di Giove
              Statore‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">C. ›Sant'Adriano al
              Foro Romano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">D. ›San Lorenzo in
              Miranda‹, già Tempio di Faustina ›Tempio di Antonino
              e Faustina‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">E. ›Santi Cosma e
              Damiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">F. Tempio della Pace
              ›Foro della Pace‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">G. ›Arco di Tito‹
              Vespasiano ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">H. Horti de Farnesi
              ›Orti Farnesiani‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">I. Sancta Maria
              Libera nos à poenis Infernis ›Santa Maria Antiqua‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">K. Casa di Cicerone
              ›Casa di Cicerone sul Palatino‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">L. Lago di Curzio
              ›Lacus Curtius‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#158">M. ›Santa Francesca
              Romana‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#162">Del ›Foro Olitorio‹
          ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#162">›Tempio alla
              Speranza‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#162">Tempio di Giunone
              Matuta ›Tempio di Giunone Sospita (Foro Olitorio)‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#162">›Tempio della Pietà‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#162">Pregione della Plebe
              ›Carcere Mamertino‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#165">Del Foro Archemorio
          ›Forum Archimonii‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#165">›Pila Tiburtina‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#165">Tempio di Flora
              ›Tempio di Flora‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#165">Casa di Martiale
              ›Domus M.V. Martialis‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#167">Del Foro d'Augusto ›Foro
          di Augusto‹</a>
          <ul>
            <li>
              <a href="dg4502270as.html#167">›Foro di Nerva‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#167">›Foro di Cesare‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#167">›Tempio di Marte
              Ultore‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#171">Del Foro Traiano ›Foro
          di Traiano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#174">Del Foro ›Forum
          Sallustii‹ ▣ , et Horti di Salustio ›Horti Sallustiani‹ ▣
          e Campo Scelerato ›Campus Sceleratus‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#177">Vestigi Della Basilica
          di Antonio [sic] Pio ›Tempio di Adriano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#180">Dell'Anfiteatro chiamato
          ›Colosseo‹, e de gli ornamenti di quello ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#184">›Statua di Marforio‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#185">Spettacoli che si
          facevano nel Coliseo ›Colosseo‹</a>
          <ul>
            <li>
              <a href="dg4502270as.html#185">Caccie nel Coliseo
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#187">Della Meta Sudante ›Meta
          Sudans‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#189">›Anfiteatro di Statilio
          Tauro‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#191">Vestigi del ›Teatro di
          Marcello‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#193">Dell'›Arco di Settimio
          Severo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#196">Dell'Arco di Settimio
          ›Arco di Settimio Severo‹ ▣, e dell'Arco Boario à S.
          Giorgio ›Arco degli Argentari‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#199">Dell'›Arco di Tito‹
          Vespasiano ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#201">Dell'›Arco di
          Costantino‹ Magno ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#204">Dell'Arco di Domitiano
          hoggi detto di Portogallo ›Arco di Portogallo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#206">Dell'›Arco di Gallieno‹
          ▣, e de' ›Trofei di Mario‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#209">Dell'›Acqua Claudia‹ e
          del suo Condotto ›Acquedotto Claudio‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#209">›Porta Maggiore‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#214">Dell'›Acqua Vergine‹, e
          dell'Acqua Felice ›Acquedotto Felice‹</a>
          <ul>
            <li>
              <a href="dg4502270as.html#214">Acqua Felice
              ›Fontana del Mosè‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#214">Lago Ioturno ›Fonte
              di Giuturna‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#214">Acqua Vergine ›Arcus
              Claudii (Via del Nazareno)‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#216">Dell'Acqua Felice
              ›Fontana del Mosè‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#217">Del Ponte ›Pons
          Neronianus‹ ▣, et Arco Trionfale ›Arcus Arcadii, Honorii
          et Theodosii‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#219">Del Trionfo de' Romani
          ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#222">Dichiaratione det
              [sic] Trionfo per ordine della Figura</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#224">Re e Regine condotte in
          trionfo ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#227">Della Consacratione
          dell'Imperatore doppo morto ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#229">Dell'Isola del Tevere
          ›Isola Tiberina‹ ▣, del ›Tempio di Esculapio‹ ▣, e di
          quello di Giunone [sic: per il ›Tempio di Iuppiter
          Iurarius‹ ▣], e Fauno ›Tempio di Fauno‹ ▣, del Ponte
          Fabritio ›Ponte Fabricio‹ ▣, hoggi detto quattro Capi, e
          del ›Ponte Cestio‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#229">Tempio di Berecintia
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#234">Del ponte Sacro ›Ponte
          Rotto‹ ▣, et del Sullicio ›Ponte Sublicio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#237">Del ›Tevere‹ e del
          Navalio ›Emporium‹, ▣ hoggi detto Ripa</a>
        </li>
        <li>
          <a href="dg4502270as.html#240">Delle Saline ›Salinae‹ ▣
          de la contrada de Legnaiuoli, Vitrai et Fornaciai, e del
          ›Testaccio (Monte)‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#242">Del Ponte del Castello
          ›Ponte Sant'Angelo‹ ▣, e della Mole di Adriano ›Castel
          Sant'Angelo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#246">Del Mausoleo d'Augusto
          ›Mausoleo di Augusto‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#248">›Statua del Tevere‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#249">Vestigi del Mausoeo
          [sic] d'Augusto ›Mausoleo di Augusto‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#251">Settizonio di Severo
          ›Septizodium (1)‹</a>
          <ul>
            <li>
              <a href="dg4502270as.html#251">Settizonio di Severo
              ›Septizodium (2)‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#251">Settizonio vecchio
              ›Septizodium (1)‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#252">Settizonio di Severo
              il nuovo ›Septizodium (2)‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#253">De' Granai del Popolo
          Romano ›Horrea‹. Del Sepolcro di Cestio ›Piramide di Caio
          Cestio‹ ▣, et de la Selva Hilerna ›Lucus Helernus‹</a>
        </li>
        <li>
          <a href="dg4502270as.html#255">Della Sepoltura di
          Metella ›Tomba di Cecilia Metella‹ ▣, della Custodia de'
          Soldati ▣ e d'altri Sepolcri antichi</a>
        </li>
        <li>
          <a href="dg4502270as.html#258">Della Torre ›Turris
          Maecenatiana‹ ▣, e degl'Horti di Mecenate ›Horti
          Maecenatiani‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#260">Del Tempio di Giove
          Ottimo Massimo, ò vogliamo dire Capitolino ›Tempio di
          Giove Capitolino‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#264">Del Tempio della Pace
          ›Foro della Pace‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#266">Vestigij del Tempio
          della Pace ›Foro della Pace‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#268">Del Tempio di Vulcano
          ›Volcanal‹ ▣, del Sole e della Luna ›Tempio di Venere e
          Roma‹ ▣ e della ›Via Sacra‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#270">Del Tempio di Diana
          ›Tempio di Diana (Aventino)‹ &#160;▣, e della Spelonca di
          Cacco ›Atrium Caci‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#272">Del Tempio ›Aedes
          Herculis Invicti‹ ▣, Altare ›Ara massima di Ercole‹ ▣, e
          Statua di Hercole ›Statua di Ercole in bronzo dorato ▣‹,
          del Tempio della Pudicitia ›Tempio della Pudicitia
          Patricia‹ ▣, di quello di Matuta ›Tempio di Mater Matuta‹
          ▣, e della Fortuna ›Tempio della Fortuna‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#275">Del Panteon ›Pantheon‹,
          hoggi detto la Rotonda ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#278">Del ›Tempio della
          Concordia‹ ▣, del Senatulo ›Senaculum‹, della Curia
          ›Curia Hostilia‹, e delle Botteghe publiche ›Schola
          Xanthi‹</a>
        </li>
        <li>
          <a href="dg4502270as.html#282">Tempio della Fortuna
          Virile ›Tempio di Portunus‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#284">Della parte del Tempio
          del Sole ›Tempio di Serapide‹ ▣ in Quirinale, detto
          impropriamente il Frontespitio di Nerone</a>
        </li>
        <li>
          <a href="dg4502270as.html#286">Del ›Tempio di Saturno‹
          ▣ e dell'Erario ›Aerarium‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#289">Della Basilica di Paolo
          Emilio ›Basilica Aemilia‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#291">Vestigi del ›Tempio di
          Antonino e Faustina‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#293">Del ›Tempio di Giove
          Statore‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#295">Vestigi del Tempio di
          Giano ›Arco di Giano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#297">D'alcuni Tempij, che già
          erano presso al Cerchio Massimo ›Circo Massimo‹</a>
          <ul>
            <li>
              <a href="dg4502270as.html#297">Tempio di Proserpina
              ›Tempio di Cerere, Libero e Libera‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#297">Tempio di Cerere
              ›Tempio di Cerere, Libero e Libera‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#297">Tempio di Flora
              ›Flora, Aedes‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#297">Tempio di Bacco
              ›Tempio di Cerere, Libero e Libera‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#297">Tempio del Sole
              ›Tempio del Sole e della Luna (Circo Massimo)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#299">Del Tempio d'Apollo
          ›Tempio del Sole‹ ▣, di Giove, di Giunone, di Minerva
          ›Capitolium vetus‹ ▣, e della Fortuna ›Santuario di
          Fortuna (Vicus Longus)‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#301">Del sepolcro di Caio
          publicio ›Sepolcro di Gaio Poplicio Bibulo‹ ▣, De la casa
          de Corvini ▣, de la ›via Lata‹, Del Tempio d'Iside
          ›Tempio di Iside e Serapide‹ ▣, et di Minerva ›Tempietto
          di Minerva‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#303">Del Tempio di
              Minerva ›Tempietto di Minerva‹, hoggi la chiesa di
              ›Santa Maria sopra Minerva‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#305">De la contrada di
          ›Suburra‹, Del Tempio di Silvano ›Portico del dio
          Silvano‹ ▣ e del Testamento di Giocondo Soldato</a>
        </li>
        <li>
          <a href="dg4502270as.html#309">Del Tempio del Dio Conso
          ›Tempio di Consus‹ ▣, ò vero del Consiglio, e del suo
          Altare ›Ara di Consus‹, Del Tempio di Nettuno ›Ara di
          Consus‹ ▣, e di quello della Gioventù &#160;›Tempio di
          Iuventas‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#311">Tempio della Fortuna
          Muliebre ›Ara della Fortuna Redux‹ ▣, e del Tempio di
          Marte ›Aedes Martis extra Portam Capenam‹ ▣, e della
          Pietra Manale ›Lapis Manalis‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#313">Del Tempio d'Iside
          ›Tempio di Isis Athenodoria‹ ▣, di quel del Honore della
          Virtù ›Tempio di Honos et Virtus‹ ▣, di Quirino ›Aedes
          Martis extra Portam Capenam‹ ▣, e di Diana ›Tempio di
          Diana (Aventino)‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#315">Del Tempio, e selva
          delle Muse ›Fons et Lucus Camenarum‹ ▣, e del Dio
          Ridicolo ›Tempio di Rediculus‹ ▣, e delle Botteghe di
          Clditio [i.e. Ciditio] ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#317">Del Tempio della Fortuna
          primigena ›Tempio della Fortuna Primigenia "in Colle"‹ ▣,
          del Tempio della Salute ›Tempio dela Salus‹ ▣, e del
          Senatulo delle Donne ›Senaculum Mulierum‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#319">Delle ›Terme di Agrippa‹
          ▣ appresso la Rotonda ›Pantheon‹</a>
        </li>
        <li>
          <a href="dg4502270as.html#321">Della via Numentana ›Via
          Nomentana‹, del Tempio di Nenia ›Santuario di Naenia‹ ▣,
          del Tempio di Bacco ›Mausoleo di Santa Costanza‹ ▣, della
          ›Villa di Faonte‹ ▣, e del Campo della Custodia ›Castra
          nova equitum singularium‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#323">Del Tempio di Fauno
          ›Santo Stefano Rotondo‹ ▣, e di Venere, e Cupido ›Tempio
          di Venere e Cupido‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#325">Delle Terme di Nerone
          ›Terme Neroniano-Alessandrine‹ ▣, e dell'Altare di
          Plutone ›Tarentum‹ ▣, e Palude Caprea ›Caprae Palus‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#328">Delle ›Terme di
          Costantino‹ ▣, e delli ›Bagni di Paolo Emilio‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#330">Delli ›Bagni di
              Paolo Emilio‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#332">Del monte ›Esquilino‹,
          delle Carine ›Carinae‹, delle Terme ›Terme di Tito‹ ▣, e
          Casa di Tito Imperatore ›Domus Flavia (Quirinale)‹, e
          delle ›Sette Sale‹</a>
        </li>
        <li>
          <a href="dg4502270as.html#335">Delle Ruine delle ›Terme
          di Tito‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#336">Delle Terme Diocletiane
          ›Terme di Diocleziano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#339">Delle Terme, e casa di
          Gordiano Imperatore ›Parco dei Gordiani‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#341">Delle Terme, o vogliamo
          dire Bagni di Antonio Caracalla ›Terme di Caracalla‹,
          hoggi dette le Antoniane ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#343">Delle Terme di Decio
          Imperatore ›Terme Deciane‹ ▣, e delle Scale Germonìe
          ›Scalae Gemoniae‹ ▣, e Clivo publico ›Clivo dei Publicii‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#345">Terme di Aureliano
          Imperadore ›Thermae Aurelianae‹ ▣, e di Settimio Severo
          ›Thermae Septimianae‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#345">Tempio della Fortuna
              forte ›Tempio della Fors Fortuna‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#346">Delle Terme di
              Settimio Severo ›Thermae Septimianae‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#348">Del Cerchio Agonale
          ›Stadio di Domiziano‹ chiamato ›Piazza Navona‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#350">Del Cerchio Flaminio
          ›Circo Flaminio‹ ▣. Del ›Tempio di Nettuno‹ ▣, e di
          Vulcano ›Tempio di Vulcano (Circo Flaminio)‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#350">Tempio di Giove
              ›Tempio di Giove Statore in Circo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#350">Tempio di Ercole
              ›Hercules Musarum, aedes‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#353">Del Cerchio Massimo
          ›Circo Massimo‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#353">Monte ›Palatino‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502270as.html#353">Monte ›Aventino‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#356">Del Cerchio di Antonino
          Caracalla ›Circo di Massenzio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#358">Del Cerchio ›Circo di
          Caligola‹ ▣, et Naumachia di Nerone ›Naumachia di
          Traiano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#361">della Naumachia di
          Domttiano [sic!] ›Naumachia Domitiani‹ ▣, e del Tempio
          della famiglia de' Flavij ›Aedes gentis Flaviae‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#363">Della Naumachia
          ›Naumachia Augusti‹ ▣, et ›Horti di Cesare‹ ▣, et delli
          Prati di Mutio Scevola ›Prata Mucia‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#366">Il ›Porto di Claudio
          (Ostia Antica)‹ ▣, e di Traiano ›Porto di Traiano (Ostia
          Antica)‹ ▣ Imperatori</a>
        </li>
        <li>
          <a href="dg4502270as.html#368">Del Sistro Instromento
          degli Antichi ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#370">Colonna d'Antonio [sic]
          Pio Imperatore ›Colonna di Marco Aurelio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#373">Della ›Colonna Traiana‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#375">Degli Obelischi, o vero
          Guglie, di S. Pietro in Vaticano ›Obelisco Vaticano‹ ▣,
          de S. Giovanni Laterano ›Obelisco Lateranense‹ ▣, e ai
          [i.e.: di] S. Maria Maggiore ›Obelisco Esquilino‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502270as.html#377">Dell'Obelisco, overo
              Guglia di S. Giovanni in Laterano ›Obelisco
              Lateranense‹</a>
            </li>
            <li>
              <a href="dg4502270as.html#379">Della Guglia di
              Santa Maria Maggiore ›Obelisco Esquilino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502270as.html#381">Guglia della Madonna del
          Popolo ›Obelisco Flaminio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#383">Delle Guglie di S. Mauto
          ›Obelisco di Ramsses II (1)‹ ▣ de Medici ›Obelisco di
          Boboli‹ ▣, e de Matthei ›Obelisco di Ramsses II (2)‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502270as.html#386">D'alcune Statue, et
          altre cose che sono hoggi in Campidoglio</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502270as.html#388">Recapitulatione
      dell'Antichità</a>
    </li>
    <li>
      <a href="dg4502270as.html#392">›Statua del Tevere‹ ▣</a>
    </li>
  </ul>
  <hr />
</body>
</html>
