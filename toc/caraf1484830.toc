<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caraf1484830s.html#6">Memorie del ritrovamento
      delle ossa di Raffaello</a>
      <ul>
        <li>
          <a href="caraf1484830s.html#10">Il tabernacolo di
          Nostra Donna del Sasso nella chiesa di Santa Maria ad
          Martyres [...] ›Pantheon‹ ▣</a>
        </li>
        <li>
          <a href="caraf1484830s.html#12">Disegno del Camuccini,
          sottoscritto da lui, che rappresenta la sepoltura di
          Raffaello sotto la statua della Madonna del Sasso [...]
          ▣</a>
        </li>
        <li>
          <a href="caraf1484830s.html#14">Disegno del Cammuccini
          rappresentante la sepoltura di Raffaello [...] ▣</a>
        </li>
        <li>
          <a href="caraf1484830s.html#16">Il medesimo teschio di
          Raffaelle veduto di profilo ▣</a>
        </li>
        <li>
          <a href="caraf1484830s.html#18">Il teschio di Raffaello
          in gesso pietrificato [...] ▣</a>
        </li>
        <li>
          <a href="caraf1484830s.html#20">La mano di Raffaello in
          gesso pietrificato ▣</a>
        </li>
        <li>
          <a href="caraf1484830s.html#22">Il sarcofago antico di
          marmo, ornato di bucrani ed encarpi nella fronte, e di
          piante di lauro nelle testate, donato dalla s. m. Papa
          Gregorio XVI, perchè vi fossero riposte le ossa di
          Raffaelle [...] ▣</a>
        </li>
        <li>
          <a href="caraf1484830s.html#24">Veduta del fondo di una
          statua dei Virtuosi al Pantheon, contenente l'epitaffio
          dettato dal Bembo [...] ▣</a>
        </li>
        <li>
          <a href="caraf1484830s.html#26">La custodia di noce
          munita di cristalli, fatta eseguire dai Virtuosi, per
          tenervi riposte nelle loro sale le memorie della
          sepoltura di Raffelle [...] ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
