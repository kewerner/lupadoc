<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501720s.html#6">Le Cose Meravigliose Dell’Alma
      Città Di Roma</a>
      <ul>
        <li>
          <a href="dg4501720s.html#8">Delle edificatione di Roma
          et il successo in fino alla conversione di Constantino
          Magno Imperatore, e della donatione fatta alli sommi
          Pontefici della santa Romana Chiesa</a>
        </li>
        <li>
          <a href="dg4501720s.html#11">Le sette Chiese
          principali</a>
        </li>
        <li>
          <a href="dg4501720s.html#19">Nell'isola</a>
        </li>
        <li>
          <a href="dg4501720s.html#20">In Trastevere</a>
        </li>
        <li>
          <a href="dg4501720s.html#22">Nel Borgo</a>
        </li>
        <li>
          <a href="dg4501720s.html#23">Della Porta Flaminia fuori
          del Popolo fino alle redici del Campidoglio</a>
        </li>
        <li>
          <a href="dg4501720s.html#32">Del Campidoglio a man
          sinistra verso li monti</a>
        </li>
        <li>
          <a href="dg4501720s.html#37">Dal Campidoglio a man
          dritta verso li monti</a>
        </li>
        <li>
          <a href="dg4501720s.html#41">Le Stationi, indulgentie,
          et gratie spirituali, che sono nelle Chiese di Roma, si
          per la quadragesima, come per tutto l'anno&#160;</a>
        </li>
        <li>
          <a href="dg4501720s.html#54">Trattato over modo
          d'acquistar l'indulgentie alle Stationi</a>
        </li>
        <li>
          <a href="dg4501720s.html#58">La guida romana per tutti i
          forastieri che vengano per vedere le antichità di Roma, a
          una per una, in bellissima forma et brevità</a>
        </li>
        <li>
          <a href="dg4501720s.html#67">Tavola delle chiese</a>
        </li>
        <li>
          <a href="dg4501720s.html#69">Summi Pontifices</a>
        </li>
        <li>
          <a href="dg4501720s.html#85">Reges et Imperatores
          romani</a>
        </li>
        <li>
          <a href="dg4501720s.html#89">Li Re di Francia</a>
        </li>
        <li>
          <a href="dg4501720s.html#90">&#160;Li Re del Regno di
          Napoli et di Sicilia, liquali cominciorno [{sic!] a
          regnare l'anno di nostra salute</a>
        </li>
        <li>
          <a href="dg4501720s.html#90">Li Dogi di Venegia
          [sic!]</a>
        </li>
        <li>
          <a href="dg4501720s.html#93">Li Duchi di Milano</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
