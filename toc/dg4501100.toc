<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501100s.html#4">Francisci Albertini opusculum de
      mirabilibus novae urbis Romae</a>
      <ul>
        <li>
          <a href="dg4501100s.html#6">Einleitung</a>
        </li>
        <li>
          <a href="dg4501100s.html#22">Epistola</a>
        </li>
        <li>
          <a href="dg4501100s.html#24">De nonnullis
          triumphantibus</a>
        </li>
        <li>
          <a href="dg4501100s.html#28">De mirabilius novae urbis
          Romae</a>
          <ul>
            <li>
              <a href="dg4501100s.html#30">De nova Urbe</a>
            </li>
            <li>
              <a href="dg4501100s.html#33">De nonnullis ecclesiis
              et capellis</a>
            </li>
            <li>
              <a href="dg4501100s.html#47">De palatiis
              Pontificum</a>
            </li>
            <li>
              <a href="dg4501100s.html#52">De domibus
              Cardinalium</a>
            </li>
            <li>
              <a href="dg4501100s.html#61">De Hospitalibus</a>
            </li>
            <li>
              <a href="dg4501100s.html#62">De Bibliotecis novae
              Urbis</a>
            </li>
            <li>
              <a href="dg4501100s.html#66">De Castro sancti
              Angeli</a>
            </li>
            <li>
              <a href="dg4501100s.html#67">De Belvidere</a>
            </li>
            <li>
              <a href="dg4501100s.html#69">De porticibus</a>
            </li>
            <li>
              <a href="dg4501100s.html#71">De viis et plateis</a>
            </li>
            <li>
              <a href="dg4501100s.html#73">De sepulchris
              memorandis</a>
            </li>
            <li>
              <a href="dg4501100s.html#76">De valvis et columnis
              aeneis</a>
            </li>
            <li>
              <a href="dg4501100s.html#78">De officina cudendae
              pecuniae</a>
            </li>
            <li>
              <a href="dg4501100s.html#78">De fontibus et
              pontibus</a>
            </li>
            <li>
              <a href="dg4501100s.html#81">De cloacis et
              purgatione Anienis</a>
            </li>
            <li>
              <a href="dg4501100s.html#82">De aedificiis a Iulio
              secundo diversis in locis constructis</a>
            </li>
            <li>
              <a href="dg4501100s.html#85">De laudibus civitatum
              Florentiae et Saonensis</a>
            </li>
            <li>
              <a href="dg4501100s.html#99">Inhalt</a>
            </li>
            <li>
              <a href="dg4501100s.html#100">Verzeichnis der in
              Buch I und II erwähnten Kirchen</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
