<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="be38154230s.html#6">Considerazioni sul progetto di
      prosciugare il Lago Fucino e di congiugnere il Mar Tirreno
      all’Adriatico per mezzo di un canale di navigazione</a>
      <ul>
        <li>
          <a href="be38154230s.html#8">Il Duca di Noto</a>
        </li>
        <li>
          <a href="be38154230s.html#12">Indice delle materie</a>
        </li>
        <li>
          <a href="be38154230s.html#14">Discorso preliminare</a>
        </li>
        <li>
          <a href="be38154230s.html#56">[Considerazioni sul
          progetto di prosciugare il Lago Fucino e di congiugnere
          il Mar Tirreno all’Adriatico per mezzo di un canale di
          navigazione]</a>
          <ul>
            <li>
              <a href="be38154230s.html#56">I. Descrizione fisica
              del lago Fucino, ed osservazioni sulle sue
              alterazioni</a>
            </li>
            <li>
              <a href="be38154230s.html#73">II. Descrizione
              dell'emissario di Claudio ed osservazioni sulle
              controverse messe in campo intorno ad una tal
              opera</a>
            </li>
            <li>
              <a href="be38154230s.html#99">III. Delle operazioni
              che debbono precedere il progetto di prosciugar il
              lago e di congiugnere i due mari con un canale di
              navigazione</a>
            </li>
            <li>
              <a href="be38154230s.html#115">IV. Considerazioni
              relative agl'interrimenti dell'emissario ed al modo
              di sgombrarlo&#160;</a>
            </li>
            <li>
              <a href="be38154230s.html#135">V. Considerazioni
              intorno allo scolo del Fucino per l'emissario</a>
            </li>
            <li>
              <a href="be38154230s.html#166">VI. Delle difficoltà
              che s'incontrano per la derivazione delle acque del
              Fucino</a>
            </li>
            <li>
              <a href="be38154230s.html#186">VII. Delle opere
              opportune per regolare la derivazione delle acque del
              Fucino</a>
            </li>
            <li>
              <a href="be38154230s.html#212">VIII. Descrizione dei
              lavori e prospetto della spesa bisognevole per
              prosciugar il Fucino</a>
            </li>
            <li>
              <a href="be38154230s.html#235">IX. Dei mezzi onde
              impedire, dopo prosciugato il lago, il risorgimento
              del medesimo, l'oppilazione dell'emissario e le
              devastazioni dei terreni restituiti
              all'agricoltura</a>
            </li>
            <li>
              <a href="be38154230s.html#266">X. Considerazioni
              sull'utilità delle acque del Fucino per rendere
              navigabile il Liri fino alla foce, su i mezzi da
              adoperarsi per eseguire una tal intrapresa e sulla
              costruzione di un ponte di ferro fuso</a>
            </li>
            <li>
              <a href="be38154230s.html#288">XI. Sulla
              comunicazione per acqua da Solmona alla foce della
              Pescara, del profondamento della foce stessa e della
              costruzione di un ampio porto nelle sue vicinanze</a>
            </li>
            <li>
              <a href="be38154230s.html#304">XII. Del
              congiugnimento del Liri alla Pescara per mezzo di un
              canale di navigazione</a>
            </li>
            <li>
              <a href="be38154230s.html#319">XIII. Dei vantaggi
              che deriverebbero dalle intraprese di prosciugar il
              Fucino e di congiugnere il mar Tirreno all'Adriatico
              con un canale di navigazione</a>
            </li>
            <li>
              <a href="be38154230s.html#337">XIV. Dell'importanza
              del canale di comunicazione che congiugnesse i due
              mari per la difesa del Regno</a>
            </li>
            <li>
              <a href="be38154230s.html#374">[Tavole]</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
