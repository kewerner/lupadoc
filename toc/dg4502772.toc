<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502772s.html#6">Nuovo metodo per acquistare
      brievemente la lingua toscana romana per commodità delle
      nationi oltremontane</a>
      <ul>
        <li>
          <a href="dg4502772s.html#8">A b c d e e f g h i l m n o
          o p q r ſ s t u z</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502772s.html#42">Pitture più notabili nelle
      chiese più riguardevoli di Roma, per facilitare la curiosità
      de' forastieri</a>
      <ul>
        <li>
          <a href="dg4502772s.html#44">San Pietro in Vaticano</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502772s.html#102">Le cose più notabili tanto de'
      giardini, quanto de' palazzi, librerie, mosei, e galerie di
      Roma</a>
      <ul>
        <li>
          <a href="dg4502772s.html#104">Campidoglio</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
