<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghtir1673860s.html#6">Notizie de’ pittori,
      scultori, incisori, e architetti natii degli stati del
      Serenissimo Signor Duca di Modena con una appendice de’
      professori di musica</a>
      <ul>
        <li>
          <a href="ghtir1673860s.html#8">Prefazione</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#12">A</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#104">B</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#138">C</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#201">D</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#206">E</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#206">F</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#220">G</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#234">I</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#237">L</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#254">M</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#288">O</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#297">P</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#313">Q</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#313">R</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#321">S</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#339">T</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#354">V</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#364">Z</a>
        </li>
        <li>
          <a href="ghtir1673860s.html#369">Professori di
          musica</a>
          <ul>
            <li>
              <a href="ghtir1673860s.html#369">A</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#369">B</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#375">C</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#379">E</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#380">F</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#381">G</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#383">L</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#384">M</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#391">P</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#393">R</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#393">S</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#397">T</a>
            </li>
            <li>
              <a href="ghtir1673860s.html#399">V</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghtir1673860s.html#404">Aggiunte</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
