<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dt23002250as.html#10">De Lateranensibus parietinis
      dissertatio historica</a>
      <ul>
        <li>
          <a href="dt23002250as.html#14">Lectori erudito</a>
        </li>
        <li>
          <a href="dt23002250as.html#24">De Lateranensibus
          parientis restitutis historica Dissertatio</a>
        </li>
        <li>
          <a href="dt23002250as.html#156">De Triclinio quae
          appellabatur Basilica Major</a>
        </li>
        <li>
          <a href="dt23002250as.html#180">Josephi Simonii
          Assemani de Sacris Imaginibus</a>
        </li>
        <li>
          <a href="dt23002250as.html#249">Index Rerum</a>
        </li>
        <li>
          <a href="dt23002250as.html#268">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
