<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebas1714070s.html#8">De’ bassanesi illustri
      narrazione</a>
      <ul>
        <li>
          <a href="ebas1714070s.html#10">Alla egregia dama
          Paulina Trotti Taverna</a>
        </li>
        <li>
          <a href="ebas1714070s.html#12">Narrazione</a>
          <ul>
            <li>
              <a href="ebas1714070s.html#14">Al chiarissimo P. D.
              Gio. Antonio Moschini</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebas1714070s.html#74">Catalogo degli scrittori
          bassanesi del secolo XVIII.</a>
          <ul>
            <li>
              <a href="ebas1714070s.html#76">Al lettore</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebas1714070s.html#112">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
