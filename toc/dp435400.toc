<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dp435400s.html#6">Graffiti e chiaroscuri esistenti
      nell'esterno della case</a>
      <ul>
        <li>
          <a href="dp435400s.html#8">[Introduzione]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dp435400s.html#10">[Tavole]</a>
      <ul>
        <li>
          <a href="dp435400s.html#10">1. [Graffito in ›Via Tor di
          Nona‹ n. 39 e 40] ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#12">2. [Graffito in ›Via Tor di
          Nona‹ n. 39 e 40] ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#14">3. [Graffito in ›Via di San
          Salvatore in Campo‹ n. 43 e 43A] ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#16">4. [Graffito in ›Via
          Tomacelli‹ n. 103 e 104 a Roma] ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#18">5. Graffito esistente in
          Roma nel giardino di sua Eccelenza il Marchese del Bufalo
          ›Casino del Bufalo‹ ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#20">6. Dettagli del graffito
          esistente in Roma nel giardino di sua Eccelanza il
          Marchese del Bufalo ›Casino del Bufalo‹ ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#22">7. Parte inferiore del
          graffito esistente in Roma al Vicolo Calabraga ›Vicolo
          Cellini‹ n. 31 e 32 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#24">8. Parte superiore del
          graffito esistente in Roma al vicolo Calabraga ›Vicolo
          Cellini‹ n. 31 e 32 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#26">9. Parte inferiore del
          graffito esistente in Roma. Borgo al ›Vicolo del
          Campanile‹ nr. 4] ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#28">10. Parte superiore del
          graffito esistente in Roma. Borgo al ›Vicolo del
          Campanile‹ n. 4 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#30">11. Graffito esistente in
          Roma al ›Vicolo Sugarelli‹ n. 1 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#32">12. Fregio graffito
          esistente in Roma in ›Via dei Banchi Vecchi‹ n. 96 e
          fregio graffito che esiteva in Roma in Via Borgo Nuovo
          ›Strada di Borgo nuovo‹ n. 62] ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#34">13. Graffito esistente in
          Roma in ›Via dei Coronari‹ n. 148 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#36">14. Fregio che ha esistito
          in Roma al Vicolo dei Spagnoli ›Via degli Spagnoli‹ n. 39
          e 40 fino a tutto l'anno 1866 e Fregio chiaroscuro
          esistgente in Roma in ›Via dei Banchi Vecchi‹ n. 89 e 90
          ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#38">15. Avanzi di un graffito
          esistente in Roma in ›Via dei Coronari‹ n. 61 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#40">16. Fregio graffito
          esistente in Roma in ›Via Tomacelli‹ n. 21 e
          &#160;graffito esistente in Roma in una vigna ›Via di
          Santa Sabina‹ n. 14 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#42">17. Parte di un graffito
          esistente in Roma in Via di San Matteo in Merulana ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#44">18. Parte di un graffito
          esistente in Roma in Via di San Matteo in Merulana ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#46">19. Parte di un graffito
          esistente in Roma in Via di San Matteo in Merulana ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#48">20. Fregio di un graffito
          esistente in Roma in Via di San Matteo in Merulana ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#50">21. Parte superiore di un
          graffito del secolo XV esistente in Roma nel Cortile dei
          Reverendissimi Padri Penitenzieri alla ›Piazza
          Scossacavalli‹ ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#52">22. Parte media di un
          graffito del secolo XV esistente in Roma in un cortile
          Reverendissimi Padri Penitenzieri alla ›Piazza
          Scossacavalli‹ ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#54">23. Graffito di proprieta
          spagnuola esistente in Roma al Vicolo della Barchetta
          ›Via della Barchetta‹ n. 2, 3, 4 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#56">24. Fregi del 1°, 2° e 3°
          piano della fabrica esistente in Roma al Vicolo del
          Governo vecchio ›Via del Governo Vecchio‹ n. 52 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#58">25. Fregio esisitente in
          Roma in una casa di Vigna alla ›Via di Porta San
          Sebastiano‹ n. 27 e Fregi di un graffito che ha esistito
          in Roma al Vicolo de' Matricciani ›Vicolo degli
          Amatriciani‹ n. 5 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#60">26. Parte superiore di un
          graffito esistente in Firenze al Palazzo Montalvo in Via
          Borgo degli Albizi ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#62">27. Graffito esistente in
          Roma al ›Vicolo del Campanile‹ n. 4 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#64">28. Fregi esistenti in un
          Casino nella Villa di Papa Giulio II presso Roma e fregio
          esistente in Roma al 2° piano di una casa decorata da
          Pierin del Vaga in ›Via Tor Millina‹ n. 18 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#66">29. Fregio esistente in Roma
          al 1° piano di una casa decorata da Pierin del Vaga in
          ›Via di Tor Millina‹ n. 18 e fregi esistenti in Roma in
          una casa presso San Giovanni de Fiorentini in ›Via
          Giulia‹ n. 82 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#68">30. Fregi esistenti in Roma
          al ›Vicolo della Fossa‹ n. 14 e 15 e fregio esistente in
          Roma in un cortile al ›Vicolo delle Vacche‹ n. 8 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#70">31. Frammento di chiaroscuro
          in Roma al ›Vicolo del Campanile‹ n. 8, fregio graffito
          in Roma alla ›Via Capo di ferro‹ n. 12 e fregio graffito
          in Roma al Vicolo del Moro ›Via del Moro‹ n. 62 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#72">32. Chiaroscuro esistente in
          Roma alla ›Via Capo di ferro‹ n. 12 e graffito esistente
          presso Roma alla ›Via Flaminia‹ in un casino nell'orto di
          Papa Giulio volgarmente detto la casa del Curato ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#74">33. Chiaroscuro esistente in
          Roma in ›Via del Pellegrino‹ n. 66 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#76">34. Graffito esistente in
          Roma in ›Via Tomacelli‹ n. 103 e 104 e graffito esistente
          in Roma al ›Vicolo Cellini‹ giá Calabraga n. 31 e 32
          ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#78">35. Prospetto di un graffito
          esistente in Roma a Via San Matteo in Merulana ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#80">36. Graffito esistente in
          Roma in un cortile del ›Palazzo Altemps‹ presso la Piazza
          di Sant'Appolinare ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#82">37. Affresco esistente in
          Roma in un cortile al ›Vicolo di Savelli‹ i n. 24 ▣</a>
        </li>
        <li>
          <a href="dp435400s.html#84">38. Chiaroscuro celebre
          esistente in Roma in ›Via della Maschera d'oro‹ n. 7
          ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
