<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghrid550724802s.html#8">Le maraviglie dell’arte,
      overo Le vite de gl’ illustri pittori veneti, e dello Stato;
      parte II&#160;</a>
      <ul>
        <li>
          <a href="ghrid550724802s.html#10">Molto illustre
          Signor mio Colendissimo</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#14">Del Signor Pietro
          Michiele Gentil'Huomo Venetiano al Cavalier Ridolfi</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#18">Cortese lettore</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#20">Tavola delle Vite
          de' Pittori moderni veneti, e dello stato</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#22">Tavola delle cose
          notabili</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#71">Vita di Iacopo
          Robusti, detto il Tintoretto</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#141">Vita di Marietta
          Tintoretta</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#144">Vita di Paolo
          Franceschi</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#151">Vita di Dario
          Varotari Veronese</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#158">Vita di Ludovico
          Pozzosarato detto da Trevigi</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#163">Vita di Giovanni
          Contarino</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#173">Vita di Leonardo
          Corona da Murano</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#183">Vita di Domenico
          Riccio detto il Brusasorci Pittore&#160;</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#194">Vita di Battista
          del Moro e di Orlando Fiacco</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#198">Vita di Felice
          Riccio detto il Brusasorci</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#205">Vita di Paolo
          Farinato</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#213">Vita di Giovanni
          Mario Verdizzoti</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#215">Vita di Parrasio
          Michiele, e d'altri discepoli del Veronese</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#217">Vita di Francesco
          Monte Mezzano</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#219">Unbenannt</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#219">Vita di Luigi
          Benfatto</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#223">Vita di Marco
          Vecellio detto di Titiano</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#225">Vita di Andrea
          Vicentino</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#227">Vita di Antonio
          Foler</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#229">Vita di Maffeo
          Verona</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#235">Vita di Pietro
          Malombra</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#242">Vita di Fra Cosmo
          Piazza Capuccino da Castel Franco</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#249">Vita di Leandro da
          Ponte da Bassano</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#259">Vita di Iacopo
          Palma il Giovane</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#297">Vita di Antonio
          Vassilacchi detto Aliense</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#319">Vita di Battista
          Maganza detto Magagnò</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#322">Vita di Antonio
          Vicentino detto Tognone</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#325">Vita di Giovanni
          Antonio Fasolo</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#329">Vita di Alessandro
          Maganza</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#339">Vita di Marcantonio
          Bassetti Veronese</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#341">Vita di Pietro
          Damini da Castel Franco</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#345">Vita di Matteo
          Ingoli, detto Ravennato&#160;</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#349">Vita di Tomaso
          Sandrino Bresciano</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#351">Vita di Francesco
          Zugni Bresciano</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#353">Vita di Giovanni
          Battista Bissone</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#357">Vita di Domenico
          Tintoretto</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#367">Vita di Santo
          Peranda</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#379">Vita di Filippo
          Zanimberti</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#385">Vita di Tiberio
          Tinelli</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#401">Vita di Claudio
          Ridolfi</a>
        </li>
        <li>
          <a href="ghrid550724802s.html#405">L'autore a chi
          legge</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
