<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zsbru7713960s.html#6">Corona aurea correscantibus
      gemmis &amp; preciosissimis conserta margaritis i qua he
      perpulchre &amp; scientifice matie Parisiensi more
      pertractantur</a>
      <ul>
        <li>
          <a href="zsbru7713960s.html#8">Index
          argumentum&#160;</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#14">Praefatio</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#16">De laudibus
          litterarum&#160;</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#69">De quiditate
          anime&#160;</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#72">De unione animae ad
          humanum corpus&#160;</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#128">De convenientia
          unionis animae rationalis ad tale corpus&#160;</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#132">De simplicitate et
          compositione anime rationalis et angelorum</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#158">De differentiis
          animae et angeli</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#251">De immortalitate
          anime</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#326">De cognitione anime
          separate</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#342">De passione anime
          coniuncte</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#344">De passione anime ab
          igne corporeo&#160;</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#391">De pueris in
          originali decedentibus&#160;</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#393">De vermibus
          inferorum</a>
        </li>
        <li>
          <a href="zsbru7713960s.html#396">De corpore
          Christi&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
