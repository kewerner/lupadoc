<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="emil1039204s.html#6">Delle Antichità
      Longobardico-Milanesi Illustrate Con Dissertazioni Dai Monaci
      Della Congregazione Cisterciese Di Lombardia; Tomo IV.</a>
      <ul>
        <li>
          <a href="emil1039204s.html#8">Prefazione</a>
        </li>
        <li>
          <a href="emil1039204s.html#20">Indice delle
          dissertazioni</a>
        </li>
        <li>
          <a href="emil1039204s.html#22">XXXI. Ricerche intorno
          la Basilica, e il Monistero di S. Ambrogio, ove la
          risposta s'imprende agli allegati di Nicolò Sormani</a>
        </li>
        <li>
          <a href="emil1039204s.html#152">XXXII. Sulla venuta di
          S. Bernardo a Milano nel MCXXXIV. e su quanto na ivi
          operato</a>
        </li>
        <li>
          <a href="emil1039204s.html#250">XXXIII. Sopra l'abito
          e la tonsura degli antichi monaci dell'ordine di S.
          Benedetto</a>
        </li>
        <li>
          <a href="emil1039204s.html#281">XXXIV. Sopra un'antica
          preziosa croce, esistente nel Monistero di Chiaravalle
          presso Milano</a>
        </li>
        <li>
          <a href="emil1039204s.html#305">XXXV. Sopra il primo
          trasporto a Milano de' corpi dei santi Magi</a>
        </li>
        <li>
          <a href="emil1039204s.html#318">XXXVI. Sopra la chiesa
          di San Giorgio in Noceto, e sopra il supposto soggiorno
          presso la medesima de' vescovi suffraganei e del clero
          milanese&#160;</a>
        </li>
        <li>
          <a href="emil1039204s.html#328">XXXVII. Sopra la
          disposizione dell'arcivescovo Pietro si riguardo la
          fondazione del monistero che la cessione fatta ai monaci
          della Basilica Ambrosiana</a>
        </li>
        <li>
          <a href="emil1039204s.html#354">XXXVIII. Intorno il
          deposito di S. Ambrogio e il prezioso altare
          dall'arcivescovo Angelberto Erettovi e da lui consegnato
          all'abate Gaudenzio</a>
        </li>
        <li>
          <a href="emil1039204s.html#386">XXXIX. Sopra le
          innovazioni nei secoli bassi introdutte nella più antica
          penitenza canonica, e sopra il Monistero di S. Vito nella
          Diocesi di Lodi, fondato in adempimento d'una tal'
          innovazione</a>
        </li>
        <li>
          <a href="emil1039204s.html#410">XXXX. Sull' eccezione
          dalla regola generale, per cui i cisterciesi esser
          possono giudici nelle cause proprie e de' suoi</a>
        </li>
        <li>
          <a href="emil1039204s.html#424">Indice delle
          materie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
