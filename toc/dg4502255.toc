<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502255s.html#8">Le cose ma ravigliose dell'alma
      città di Roma</a>
      <ul>
        <li>
          <a href="dg4502255s.html#10">Le sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg4502255s.html#10">La prima Chiesa è ›San
              Giovanni in Laterano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#16">La seconda Chiesa è
              ›San Pietro in Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#23">La terza Chiesa &#160;è
              ›San Paolo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#25">La quarta Chiesa è
              ›Santa Maria Maggiore‹ ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#29">La quinta Chiesa è ›San
              Lorenzo fuori dalle Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#30">La Sesta Chiesa é ›San
              Sebastiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#32">La Settima Chiesa é
              ›Santa Croce in Gerusalemme‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502255s.html#33">Nell'›Isola Tiberina‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#34">In ›Trastevere‹</a>
          <ul>
            <li>
              <a href="dg4502255s.html#34">›Santa Maria in
              Trastevere‹ ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#36">›Santa Cecilia in
              Trastevere‹ ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#37">›San Crisogono‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502255s.html#40">In ›Borgo‹</a>
          <ul>
            <li>
              <a href="dg4502255s.html#42">›Santa Maria in
              Traspontina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502255s.html#43">Della Porta Flaminia overo
          dal Popolo ›Porta del Popolo‹ fino alle radici del
          ›Campidoglio‹</a>
          <ul>
            <li>
              <a href="dg4502255s.html#43">›Santa Maria del
              Popolo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#45">›Trinità dei Monti‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#50">›Santa Maria sopra
              Minerva‹ ▣</a>
            </li>
            <li>
              <a href="dg4502255s.html#57">›Santa Maria in
              Aracoeli‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502255s.html#58">Dal ›Campidoglio‹ a man
          sinistra verso i Monti</a>
        </li>
        <li>
          <a href="dg4502255s.html#64">Dal ›Campidoglio‹ a man
          dritta verso il Tevere</a>
        </li>
        <li>
          <a href="dg4502255s.html#68">Le stationi che sono nelle
          chiese di Roma [...]</a>
          <ul>
            <li>
              <a href="dg4502255s.html#75">Le Stationi
              dell'Advento</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502255s.html#76">Guida romana per il
          forestieri che vogliono veder l'Antichità di Roma, ad una
          per una</a>
          <ul>
            <li>
              <a href="dg4502255s.html#76">Del ›Borgo‹ prima
              giornata</a>
              <ul>
                <li>
                  <a href="dg4502255s.html#76">Del
                  ›Trastevere‹</a>
                </li>
                <li>
                  <a href="dg4502255s.html#77">Dell'Isola
                  Tiberina‹, e Licaonia</a>
                </li>
                <li>
                  <a href="dg4502255s.html#77">Del Ponte di Santa
                  Maria ›Ponte Rotto‹, del Palazzo di Pilato ›Casa
                  dei Crescenzi‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4502255s.html#78">Del Monte Testaccio
                  ›Testaccio (Monte)‹ e di molte altre cose</a>
                </li>
                <li>
                  <a href="dg4502255s.html#78">Delle Therme
                  Antoniane ›Terme di Caracalla‹ , ed altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4502255s.html#78">Di ›San Giovanni in
                  Laterano‹, ›Santa Croce in Gerusalemme‹, et
                  altri</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502255s.html#79">Gioranta seconda</a>
              <ul>
                <li>
                  <a href="dg4502255s.html#79">Della ›Porta del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4502255s.html#80">Della strada Pia
                  ›Via XX Settembre‹, e della Vigna che era già del
                  Cardinal di Ferrara</a>
                </li>
                <li>
                  <a href="dg4502255s.html#80">Della Vigna del
                  Cardinal di Carpi, et altre cose</a>
                </li>
                <li>
                  <a href="dg4502255s.html#80">Della ›Porta Pia‹,
                  di ›Sant'Agnese fuori le Mura‹, et altre
                  anticaglie</a>
                </li>
                <li>
                  <a href="dg4502255s.html#81">Delle Terme
                  Diocletiane ›Terme di Diocleziano‹</a>
                </li>
                <li>
                  <a href="dg4502255s.html#81">Del ›Colosseo‹,
                  delle ›Sette Sale‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4502255s.html#82">Del Monte
                  ›Palatino‹. hora detto Palazzo Maggiore, e del
                  Tempio della Pace ›Foro della Pace‹, et altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4502255s.html#82">Del ›Foro di
                  Nerva‹</a>
                </li>
                <li>
                  <a href="dg4502255s.html#83">Del ›Campidoglio‹,
                  et altre cose</a>
                </li>
                <li>
                  <a href="dg4502255s.html#83">De' portichi di
                  Ottavia ›Portico d'Ottavia‹, et di Settimio
                  ›Porticus Severi‹, e ›T eatro di Pompeo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502255s.html#83">Giornata terza</a>
              <ul>
                <li>
                  <a href="dg4502255s.html#83">Delle due Colonne
                  una di Antonio Pio, et l'altra di Traiano
                  ›Colonna di Antonino Pio‹ ›Colonna Traiana‹</a>
                </li>
                <li>
                  <a href="dg4502255s.html#84">Della Rotonda,
                  overo ›Pantheon‹</a>
                </li>
                <li>
                  <a href="dg4502255s.html#84">Dei Bagni di
                  Agrippa, et di Nerone ›Terme di Agrippa‹ ›Terme
                  Neroniano-Alessandrine‹</a>
                </li>
                <li>
                  <a href="dg4502255s.html#84">Della piazza
                  Navona, et di Mastro Pasquino ›Piazza Navona‹
                  ›Statua di Pasquino‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502255s.html#85">[Tavole]</a>
              <ul>
                <li>
                  <a href="dg4502255s.html#85">Indice brevissimo
                  de' Pontefici Romani</a>
                </li>
                <li>
                  <a href="dg4502255s.html#102">Reges, et
                  Imperatores Romani</a>
                </li>
                <li>
                  <a href="dg4502255s.html#105">Li Re di
                  Francia</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502255s.html#106">L'antichità di Roma</a>
      <ul>
        <li>
          <a href="dg4502255s.html#107">Dell'Edificatione di
          Roma</a>
        </li>
        <li>
          <a href="dg4502255s.html#109">Del Circuito di Roma</a>
        </li>
        <li>
          <a href="dg4502255s.html#109">Delle Porte</a>
        </li>
        <li>
          <a href="dg4502255s.html#110">Delle vie</a>
        </li>
        <li>
          <a href="dg4502255s.html#111">Delli Ponti che sono sopra
          il Tevere, et suoi edificatori</a>
        </li>
        <li>
          <a href="dg4502255s.html#112">Dell'Isola del Tevere
          ›Isola Tiberina‹ ▣</a>
        </li>
        <li>
          <a href="dg4502255s.html#113">Delli Monti</a>
        </li>
        <li>
          <a href="dg4502255s.html#113">Del Monte Testaccio
          ›Testaccio (Monte)‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#114">Delle acque, et chi le
          condusse in Roma</a>
        </li>
        <li>
          <a href="dg4502255s.html#115">Della ›Cloaca Maxima‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#115">Delli Acquedotti</a>
        </li>
        <li>
          <a href="dg4502255s.html#115">Delle ›Sette Sale‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#116">Delle Terme cioè Bagni et
          suoi edificatori</a>
        </li>
        <li>
          <a href="dg4502255s.html#116">›Foro Romano‹ ▣</a>
        </li>
        <li>
          <a href="dg4502255s.html#117">Delle Naumachie, dove si
          facevano le battaglie navali, et che cose erano</a>
        </li>
        <li>
          <a href="dg4502255s.html#117">De' Cerchi, et che cosa
          erano</a>
        </li>
        <li>
          <a href="dg4502255s.html#118">De' Theatri, et che cosa
          erano, et suoi edificatori</a>
        </li>
        <li>
          <a href="dg4502255s.html#119">Delli Anfiteatri, et suoi
          edificatori, et che cosa erano</a>
        </li>
        <li>
          <a href="dg4502255s.html#119">Il ›Colosseo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502255s.html#120">De i Fori, cioè Piazze</a>
        </li>
        <li>
          <a href="dg4502255s.html#120">›Terme di Diocleziano‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502255s.html#121">Delli Archi Trionfali, et
          a chi si davano</a>
        </li>
        <li>
          <a href="dg4502255s.html#121">Arcus Septimio Severo
          ›Arco degli Argentari‹ ▣</a>
        </li>
        <li>
          <a href="dg4502255s.html#122">De i Portichi</a>
        </li>
        <li>
          <a href="dg4502255s.html#122">Portico di Antonino, e
          Faustina ›Tempio di Antonino, e Faustina‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#123">De' Trofei, et Colonne
          memorande</a>
        </li>
        <li>
          <a href="dg4502255s.html#123">Li ›Trofei di Mario‹ ▣</a>
        </li>
        <li>
          <a href="dg4502255s.html#124">De i Colossi</a>
        </li>
        <li>
          <a href="dg4502255s.html#125">Delle Piramidi</a>
        </li>
        <li>
          <a href="dg4502255s.html#125">Sepoltura di Cestio
          ›Piramide di Caio Cestio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502255s.html#125">Delle Mete</a>
        </li>
        <li>
          <a href="dg4502255s.html#126">Delli Obelischi, overo
          Aguglie</a>
        </li>
        <li>
          <a href="dg4502255s.html#126">Delle Statue</a>
        </li>
        <li>
          <a href="dg4502255s.html#126">›Statua di Marforio‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#126">De Cavalli ›Fontana di
          Monte Cavallo‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#127">Delle Librarie</a>
        </li>
        <li>
          <a href="dg4502255s.html#127">Delli Horiuoli</a>
        </li>
        <li>
          <a href="dg4502255s.html#127">De i Palazzi</a>
        </li>
        <li>
          <a href="dg4502255s.html#127">Della Casa Aurea di Nerone
          ›Domus Aurea‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#128">Dell'altre case de'
          Cittadini</a>
        </li>
        <li>
          <a href="dg4502255s.html#128">Delle Curie, et che cosa
          erano</a>
        </li>
        <li>
          <a href="dg4502255s.html#129">De' Senatuli, et che cosa
          erano</a>
        </li>
        <li>
          <a href="dg4502255s.html#129">De' Magistrati</a>
        </li>
        <li>
          <a href="dg4502255s.html#130">De i Comitij, et che cosa
          erano</a>
        </li>
        <li>
          <a href="dg4502255s.html#130">Delle Tribù</a>
        </li>
        <li>
          <a href="dg4502255s.html#130">Delle Regioni, cioè Rioni,
          et sue Insegne</a>
        </li>
        <li>
          <a href="dg4502255s.html#130">Delle Basiliche, et che
          cosa erano</a>
        </li>
        <li>
          <a href="dg4502255s.html#131">Del ›Campidoglio‹ ▣</a>
        </li>
        <li>
          <a href="dg4502255s.html#132">Dello Erario ›Aerarium‹,
          cioè Camera del commune, et che moneta si spendeva in
          Roma in que' tempi</a>
        </li>
        <li>
          <a href="dg4502255s.html#133">Del Gregostasi, et che
          cosa era ›Grecostasi‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#133">Della Secretaria del
          Popolo Romano ›Secretarium Senatus‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#133">Dell'›Asilo di Romolo‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#133">Delli ›Rostri‹, et che
          cosa erano</a>
        </li>
        <li>
          <a href="dg4502255s.html#133">Della Colonna detta
          Miliario ›Miliarium Aureum‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#133">Del Tempio di Carmenta
          ›Carmentis, Carmenta‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#134">Della colonna Bellica
          ›Columna Bellica‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#134">Della Colonna Lattaria
          ›Columna Lactaria‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#134">Dell'›Aequimaelium‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#134">Del ›Campo Marzio‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#134">Del Tigillo Sororio
          ›Tigillum Sororium‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#134">De' Campi Forastieri
          ›Castra Peregrina‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#134">Della ›Villa Publica‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#135">Della ›Taberna
          Meritoria‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#135">Del Vivario ›Vivarium‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#135">Delli Horti</a>
        </li>
        <li>
          <a href="dg4502255s.html#136">Del ›Velabro‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#136">Delle ›Carinae‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#136">Delli Clivi</a>
        </li>
        <li>
          <a href="dg4502255s.html#136">De' Prati</a>
        </li>
        <li>
          <a href="dg4502255s.html#137">De i Granari publici</a>
        </li>
        <li>
          <a href="dg4502255s.html#137">Delle Carceri publiche</a>
        </li>
        <li>
          <a href="dg4502255s.html#137">Di alcune Feste, et
          Giochi, che si solevano celebrare in Roma</a>
        </li>
        <li>
          <a href="dg4502255s.html#138">Del Sepolcro di Augusto,
          et d'Adriano, et di Settimio ›Castel Sant'Angelo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502255s.html#139">De i Tempij</a>
        </li>
        <li>
          <a href="dg4502255s.html#141">De Sacerdoti, delle
          Vergini Vestali, vestimenti, vasi, et altri instrumenti,
          fatti per uso delli sacrificij, et suoi institutori</a>
        </li>
        <li>
          <a href="dg4502255s.html#142">Dell'Armamentario
          ›Armamentaria‹, et che cosa era</a>
        </li>
        <li>
          <a href="dg4502255s.html#143">De Trionfi, et à chi si
          concedevano, et chi fu il primo Trionfatore, et di quante
          maniere erano</a>
        </li>
        <li>
          <a href="dg4502255s.html#143">Delle Corone, et à chi si
          davano</a>
        </li>
        <li>
          <a href="dg4502255s.html#144">Del numero del Popolo
          Romano</a>
        </li>
        <li>
          <a href="dg4502255s.html#144">Delle ricchezze del Popolo
          Romano</a>
        </li>
        <li>
          <a href="dg4502255s.html#144">Della liberalità delli
          antichi Romani</a>
        </li>
        <li>
          <a href="dg4502255s.html#145">Delli matrimonij antichi,
          et loro usanza</a>
        </li>
        <li>
          <a href="dg4502255s.html#145">Della buona creanza, che
          davano à i figliuoli</a>
        </li>
        <li>
          <a href="dg4502255s.html#145">Della separatione de'
          Matrimonij</a>
        </li>
        <li>
          <a href="dg4502255s.html#146">Dell'Essequie antiche, et
          sue Cerimonie</a>
        </li>
        <li>
          <a href="dg4502255s.html#147">Delle Torre</a>
        </li>
        <li>
          <a href="dg4502255s.html#147">Del ›Tevere‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#147">Del Palazzo Papale
          ›Palazzo Apostolico Vaticano‹, e di ›Cortile del
          Belvedere‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#148">Del ›Trastevere‹</a>
        </li>
        <li>
          <a href="dg4502255s.html#148">Recapitulatione
          dell'Antichità</a>
        </li>
        <li>
          <a href="dg4502255s.html#149">De i Tempij de gli
          antichi, fuori di Roma</a>
        </li>
        <li>
          <a href="dg4502255s.html#150">De' fuochi de gli antichi,
          scritti da pochi autori, cavati da alcuni fragmenti
          d'Historie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
