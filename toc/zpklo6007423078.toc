<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zpklo6007423078s.html#6">Klopstocks Sämtliche
      Werke. Siebenter Band. Oden. Geistliche Lieder. Epigramme</a>
      <ul>
        <li>
          <a href="zpklo6007423078s.html#8">Verzeichnis der
          Oden</a>
        </li>
        <li>
          <a href="zpklo6007423078s.html#10">Oden</a>
          <ul>
            <li>
              <a href="zpklo6007423078s.html#12">1796</a>
              <ul>
                <li>
                  <a href="zpklo6007423078s.html#12">Unsere
                  Sprache an uns</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zpklo6007423078s.html#14">1798</a>
              <ul>
                <li>
                  <a href="zpklo6007423078s.html#14">Die
                  öffentliche Meinung</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#17">Freude und
                  Leid</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#19">Die
                  Erscheinende</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zpklo6007423078s.html#21">1799</a>
              <ul>
                <li>
                  <a href="zpklo6007423078s.html#21">Auch die
                  Nachwelt</a>
                </li>
                <li>
                  <a href=
                  "zpklo6007423078s.html#23">Wissbegierde</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zpklo6007423078s.html#25">1800</a>
              <ul>
                <li>
                  <a href="zpklo6007423078s.html#25">An die
                  Dichter meiner Zeit</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#27">Der
                  Segen</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#29">Der
                  Bund</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#30">Die
                  unbekannten Seelen</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#33">Der neue
                  Python</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#35">Die
                  Aufschriften</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#37">Die
                  Wage</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#38">Die
                  Unvergessliche</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#39">Die
                  Sieger, und die Besiegten</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#42">Die
                  Nachkommen der Angelsachsen</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#43">Die
                  Wahl</a>
                </li>
                <li>
                  <a href=
                  "zpklo6007423078s.html#45">Losreißung</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#48">Die
                  Unschuldigen</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zpklo6007423078s.html#50">[1801]</a>
              <ul>
                <li>
                  <a href="zpklo6007423078s.html#50">Zwei
                  Johanneswürmchen</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#51">Die
                  Bildhauerkunst, die Malerei, und die
                  Dichtkunst</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#52">Das
                  Schweigen</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#54">Kaiser
                  Alexander</a>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#55">Die
                  höheren Stufen</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="zpklo6007423078s.html#58">Geistliche
          Lieder</a>
          <ul>
            <li>
              <a href="zpklo6007423078s.html#58">Erster
              Teil</a>
              <ul>
                <li>
                  <a href=
                  "zpklo6007423078s.html#60">Einleitung</a>
                </li>
                <li>
                  <a href=
                  "zpklo6007423078s.html#74">[Lieder]</a>
                  <ul>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#74">Fürbitte für
                      Sterbende</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#76">Danklied</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#78">Dieses
                      und jenes Leben</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#82">Vergebung der
                      Sünde</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#83">Vorbereitung
                      zum Tode</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#87">Die
                      Feinde des Kreuzes Christi</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#90">Gott
                      dem Vater</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#92">Dem
                      Dreieinigen</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#97">Der
                      Erbamer</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#98">Die
                      geistliche Auferstehung</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#101">Gott
                      dem Sohne am Weihnachtsfeste</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#103">Um
                      Gnade</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#104">Auferstehung</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#107">Gott
                      dem Sohne am Karfreitage</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#110">Der
                      Taufbund</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#117">Der
                      nahe Tod</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#119">Die
                      Gottheit Jesu</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#120">Gott
                      dem Sohne zum Osterfeste</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#123">Fürbitte für
                      Sterbende</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#125">Um
                      Versöhnlichkeit</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#127">Die
                      Auferstehung</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#129">Gott
                      dem Sohne am Himmelfahrtstage</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#131">Die
                      sieben Gemeinen</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#144">Die
                      Feinde des Kreuzes Christi</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#146">Gott
                      dem Sohne</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#149">Loblied</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#151">Der
                      Tod</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#154">Dem
                      Dreieinigen</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#155">Bußlied</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#156">Gott
                      dem heiligen Geiste zum Pfingstfeste</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#159">Allgemeines
                      Gebet um geistliche Gaben</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#164">Beim
                      Abendmale</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#171">Loblied eines
                      Sterbenden</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#174">Danklied</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#177">Nach
                      dem Segen</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#178">Inhalt
                  der Lieder</a>
                </li>
                <li>
                  <a href=
                  "zpklo6007423078s.html#180">Alphabetisches
                  Register</a>
                </li>
                <li>
                  <a href=
                  "zpklo6007423078s.html#182">Veränderte
                  Lieder</a>
                  <ul>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#184">Vorbericht</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#188">Allein Gott
                      in der Höh sei Ehr</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#189">Der
                      am Kreuz ist meine Liebe</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#191">Es
                      woll uns Gott genädig sein</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#193">Gelobet seist
                      du, Jesu Christ</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#194">Gott
                      der Vater wohn uns bei</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#195">Herr
                      Gott, dich loben wir</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#198">Herr
                      Jesu Christ dich zu uns wend</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#199">Herzlich lieb
                      hab ich dich oh Herr</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#201">Herzliebster
                      Jesu was hast du verbrochen</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#205">Jesaia dem
                      Propheten</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#206">Jesu
                      seine tiefe Wunden</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#208">Jesus
                      Christus unser Heiland, der den Tod
                      überwand</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#209">Jesus
                      Christus unser Heiland der von uns Gottes
                      Zorn wand</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#211">Jesus
                      meine Zuversicht</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#213">Komm
                      heiliger Geist herre Gott</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#215">Liebster Jesu
                      wir sind hier</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#216">Mitten wir im
                      Leben sind</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#218">Nun
                      bitten wir den heiligen Geist</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#219">Nun
                      lasset uns den Leib begraben</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#221">Nun
                      lob mein Seel den Herren</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#224">O
                      großer Gott von Macht</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#226">Schmücke dich
                      o liebe Seele</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#229">Sei
                      Lob und Ehr dem höchsten Gut</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#232">Sollt
                      ich meinem Gott nicht singen</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#236">Mach
                      auf mein Herz und singe</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#238">Machet auf!
                      ruft uns die Stimme</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#241">Wenn
                      meine Sünd mich tränten</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#244">Wie
                      schön leucht't uns der Morgenstern</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href=
                  "zpklo6007423078s.html#248">Alphabetisches
                  Register</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zpklo6007423078s.html#250">Zweiter
              Teil</a>
              <ul>
                <li>
                  <a href=
                  "zpklo6007423078s.html#252">Vorrede</a>
                </li>
                <li>
                  <a href=
                  "zpklo6007423078s.html#254">[Lieder]</a>
                  <ul>
                    <li>
                      <a href="zpklo6007423078s.html#254">Die
                      tägliche Buße</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#256">Die
                      Auferstehung Jesu</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#261">Die
                      Größe der Christen</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#264">Der
                      Sieg der Glaubenden</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#266">Das
                      Gebet des Herrn</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#268">Die
                      Nachfolge</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#270">Die
                      Hoffnung der Auferstehung</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#272">Die
                      Erneuerung</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#275">Die
                      Erlösung</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#278">Dem
                      Vater und dem Sohne</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#281">Der
                      Kampf der Glaubenden</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#284">Einsegnung
                      eines Sterbenden</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#287">Morgenlied</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#289">Die
                      große Verheißung</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#290">Das
                      Bekenntnis</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#293">Der
                      alte und der neue Bund</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#294">Abendlied</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#296">Dem
                      Erlöser</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#298">Die
                      Wege zum Heile</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#299">Die
                      Wenigen</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#303">Die
                      Hoffnung der Seligkeit</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#304">Nach
                      dem Abendmahle</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#307">Vorbereitung
                      zum Gottesdienst</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#308">Die
                      zukünftige Welt</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#310">Vorbereitung
                      zum Gottesdienste</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#311">Das
                      Beispiel</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#314">Weihnachtslied</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#315">Das
                      Abendmahl</a>
                    </li>
                    <li>
                      <a href="zpklo6007423078s.html#316">Sinai
                      und Golgatha</a>
                    </li>
                    <li>
                      <a href=
                      "zpklo6007423078s.html#318">Stärkung</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="zpklo6007423078s.html#322">Inhalt
                  der Lieder</a>
                </li>
                <li>
                  <a href=
                  "zpklo6007423078s.html#324">Alphabetisches
                  Register</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="zpklo6007423078s.html#326">Epigramme</a>
          <ul>
            <li>
              <a href="zpklo6007423078s.html#328">1.
              Gegenseitige Wirkung</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#328">2. An einige
              Beurtheiler des deutschen Hexameters</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#329">3.
              Überlebung</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#329">4. Nach
              Horaz</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#330">5. Der
              doppelte Mitausdruck</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#330">6.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#331">7./8.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#332">9. An
              Boileau's Schatten</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#333">10.
              Wunderkur</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#334">11. Lob der
              Bescheidenheit</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#334">12. Popens
              vermutliche Reue</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#335">13. Die
              Antwort der Sängerin</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#335">14.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#336">15. Die
              beiden Gesetze</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#336">16. An die
              Ausleger der Alten</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#337">17./18.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#338">19./20.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#339">21. Was man
              fo[r]dert</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#339">22.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#340">23. Der
              Ausbruch der Leidenschaften</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#340">24.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#341">25. Sic se
              servavit Apollo</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#341">26. Das
              Lächeln und die Lache</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#342">27. Der
              Scheideweg</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#342">28.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#343">29./30.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#344">31.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#345">32. An einige
              der heurigen Philosophen</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#345">33. Der
              Unschuldige</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#346">34.
              Erweiterung des Thierreichs</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#347">35. Der nicht
              kleine Unterschied</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#347">36.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#348">37. Der
              Unterscheidende</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#348">38. Frage</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#349">39. Die
              gewissenhafte Deklamazion</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#350">40. Die
              orthodoxen Republikaner</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#350">41.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#351">42. Der
              Befürchtende</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#351">43. Der
              eingeschränkte Geschmack</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#352">44.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#353">45. Die
              goldenen Zeiten</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#353">46. Der
              epicurische Leser</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#354">47. Die
              Schriftstelleren</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#355">48. Der
              schwere Sieg</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#355">49.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#356">50./51.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#357">52. Streit
              unter zwei Franzosen</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#357">53.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#358">54. Eine gute
              Regel</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#359">55.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#359">56. Die
              philosophische Karrikatur</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#360">57./58.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#361">59. Frommer
              Wunsch</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#361">60.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#362">61. Vorlesung
              der Henriade</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#362">62.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#363">63.</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#364">64. Die
              äthiopische, französische und deutsche
              Orthographie</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#364">65. An einen
              ausländischen Wortleser</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#365">66. Die bösen
              Nachbarn</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#365">67.</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="zpklo6007423078s.html#366">Klopstocks Sämtliche
      Werke. Achter Band. Der Tod Adams. Hermanns Schlacht</a>
      <ul>
        <li>
          <a href="zpklo6007423078s.html#368">Vorrede</a>
        </li>
        <li>
          <a href="zpklo6007423078s.html#370">Der Tod Adams.
          Ein Trauerspiel</a>
          <ul>
            <li>
              <a href="zpklo6007423078s.html#371">Personen</a>
            </li>
            <li>
              <a href=
              "zpklo6007423078s.html#372">Vorbericht</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#376">I. Erste
              Handlung</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#390">II. Zweite
              Handlung</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#410">III. Dritte
              Handlung</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zpklo6007423078s.html#426">Hermanns
          Schlacht. Ein Bardiet für die Schaubühne</a>
          <ul>
            <li>
              <a href="zpklo6007423078s.html#428">An den
              Kaiser</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#434">Tacitus</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#436">Personen</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#438">I. Erste
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#448">II. Zweite
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#469">III. Dritte
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#485">IV. Vierte
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#494">V. Fünfte
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#496">VI. Sechste
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#513">VII. Siebente
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#525">VIII. Achte
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#533">IX. Neunte
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#539">X. Zehnte
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#543">XI. Elfte
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#583">XII. Zwölfte
              Scene</a>
            </li>
            <li>
              <a href="zpklo6007423078s.html#600">XIV.
              Vierzehnte Scene</a>
            </li>
            <li>
              <a href=
              "zpklo6007423078s.html#608">Anmerkungen</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
