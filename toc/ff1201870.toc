<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ff1201870s.html#6">Il devotissimo viaggio di
      Gerusalemme</a>
      <ul>
        <li>
          <a href="ff1201870s.html#8">All'illustrissimo et
          eccellentissimo Signor Don Duarte farnese</a>
        </li>
        <li>
          <a href="ff1201870s.html#8">[Dediche in versi]</a>
          <ul>
            <li>
              <a href="ff1201870s.html#15">Epigramma Iacobi Demii
              Nobilis Batavi</a>
            </li>
            <li>
              <a href="ff1201870s.html#17">Epigramma ad Ioannes
              Zuallardum Militem</a>
            </li>
            <li>
              <a href="ff1201870s.html#18">Ad Dominum Ioannem
              Zuallardum</a>
            </li>
            <li>
              <a href="ff1201870s.html#19">Sonetto do Segnor
              Gaspar Pacheco gentilhuomen portuges</a>
            </li>
            <li>
              <a href="ff1201870s.html#20">Al Signor Giovanni
              Zuallardo Cavaliero del Santissimo Sepolcro</a>
            </li>
            <li>
              <a href="ff1201870s.html#21">Iulii Roscii Hortini
              Epigramma</a>
            </li>
            <li>
              <a href="ff1201870s.html#21">Ad Natalem
              Bonifacium</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ff1201870s.html#22">Proemio</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ff1201870s.html#26">Libro I. Che contiene molti
      avvertimenti necessarij à tutti coloro che vogliono fare
      detto viaggio</a>
      <ul>
        <li>
          <a href="ff1201870s.html#50">Instruttione delle cose
          necessarie al pelegrino</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ff1201870s.html#89">Libro II. Nel quale è contenuto
      la descrittione de i Paesi, Golfi, Isole, Città, terre, et
      luoghi dove si passa [...]</a>
      <ul>
        <li>
          <a href="ff1201870s.html#89">Venetia ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#91">Colpho (sic!) coll'Italia
          ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#97">Tremiti ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#106">Zante ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#112">Colfo (sic!) de Settelia
          ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#113">Candia ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#116">Cipro ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ff1201870s.html#132">Libro III. Che contiene la
      descrittione de tutti i luoghi Santi, che si veggono, ò
      visitano in Gierusalemme [...]</a>
      <ul>
        <li>
          <a href="ff1201870s.html#139">Ramma ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#141">Domus Boni Latronis ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#143">Seritz ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#145">Shieremia ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#146">Vallis Therebinthi ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#148">Hierusalem ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#156">Hierusalem ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#164">Hierusalem ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#170">Hierusalem ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#171">Seconda giornata</a>
          <ul>
            <li>
              <a href="ff1201870s.html#174">Sepulchrum Virginis
              Mariae ▣</a>
            </li>
            <li>
              <a href="ff1201870s.html#177">Mons Olivarum ▣</a>
            </li>
            <li>
              <a href="ff1201870s.html#184">Hierusalem ▣</a>
            </li>
            <li>
              <a href="ff1201870s.html#189">Hierusalem ▣</a>
            </li>
            <li>
              <a href="ff1201870s.html#191">Ecce Homo ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ff1201870s.html#195">Terza giornata</a>
          <ul>
            <li>
              <a href="ff1201870s.html#196">Mons Olivarium ▣</a>
            </li>
            <li>
              <a href="ff1201870s.html#202">Betania ▣</a>
            </li>
            <li>
              <a href="ff1201870s.html#208">Della Chiesa della
              Resurrettione overo del Santo Sepolcro, del Nostro
              Salvatore</a>
              <ul>
                <li>
                  <a href="ff1201870s.html#209">Pianta della
                  Chiesa del Santissimo Sepolcro e del Monta
                  Calvario del Nostro Signore Gesù Cristo ▣</a>
                </li>
                <li>
                  <a href="ff1201870s.html#211">Ecclesia Sancti
                  Sepulchri ▣</a>
                </li>
                <li>
                  <a href="ff1201870s.html#228">Mons Calvarius
                  ▣</a>
                </li>
                <li>
                  <a href="ff1201870s.html#232">Sepulchrum Christi
                  ▣</a>
                </li>
                <li>
                  <a href="ff1201870s.html#250">Monasterum Sancti
                  Heliae ▣</a>
                </li>
                <li>
                  <a href="ff1201870s.html#252">Sepulchrum Rachel
                  ▣</a>
                </li>
                <li>
                  <a href="ff1201870s.html#274">Montana Iudeae
                  ▣</a>
                </li>
                <li>
                  <a href="ff1201870s.html#277">Desertum Sancti
                  Ioannis Baptistae ▣</a>
                </li>
                <li>
                  <a href="ff1201870s.html#278">Fons Sancti
                  Philippi</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ff1201870s.html#284">Libro IV. Nel quale sono
      menzonate tutte le Città, Terre et castelli antici e Moderni
      [...]</a>
      <ul>
        <li>
          <a href="ff1201870s.html#289">Pellegrinatione di Gierico
          et Quarantana: et del fiume Giordano</a>
        </li>
        <li>
          <a href="ff1201870s.html#294">Quarantane Mons</a>
        </li>
        <li>
          <a href="ff1201870s.html#299">La Samaria et Galilea</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ff1201870s.html#330">Libro V. Che contiene tutto il
      successo havuto del ritorno di quel santissimo viaggio
      [...]</a>
      <ul>
        <li>
          <a href="ff1201870s.html#354">Tripoli di Soria ▣</a>
        </li>
        <li>
          <a href="ff1201870s.html#370">[Dediche in versi]</a>
          <ul>
            <li>
              <a href="ff1201870s.html#370">Iacobi Demij Nobilis
              Batavi</a>
            </li>
            <li>
              <a href="ff1201870s.html#371">Iulii Roscii
              Hortini</a>
            </li>
            <li>
              <a href="ff1201870s.html#374">Aurelii Ursii
              Romani</a>
            </li>
            <li>
              <a href="ff1201870s.html#375">Iulii Roscii
              Hortini</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ff1201870s.html#378">Libro VI. Che contiene le
      Orationi, che si dicano ne luoghi Santi di Gierusalemme, con
      altre convenevoli à dire da pellegrini</a>
      <ul>
        <li>
          <a href="ff1201870s.html#387">Cominciano le Orationi,
          che si dicono nè luoghi Santi di Gierusalemme, con altre
          convenevoli a dire, per i Pellegrini</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ff1201870s.html#428">Tavola delle cose più notabili
      contenute in questo libro</a>
    </li>
  </ul>
  <hr />
</body>
</html>
