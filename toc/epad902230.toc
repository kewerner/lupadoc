<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="epad902230s.html#6">Della felicità di Padova di
      Angelo Portenari</a>
      <ul>
        <li>
          <a href="epad902230s.html#8">Lettera dedicatoria di
          Angelo Portenari alla sua Patria Padova</a>
        </li>
        <li>
          <a href="epad902230s.html#14">Tavola prima delli libri,
          e delli capitoli di questa opera</a>
        </li>
        <li>
          <a href="epad902230s.html#20">Tavola seconda degli
          autori latini, volgari,greci, [...]</a>
        </li>
        <li>
          <a href="epad902230s.html#26">Discorso della felicità,
          il quale serve per argomento dell'opera</a>
        </li>
        <li>
          <a href="epad902230s.html#30">Libro primo</a>
        </li>
        <li>
          <a href="epad902230s.html#67">Libro secondo</a>
        </li>
        <li>
          <a href="epad902230s.html#107">Libro terzo</a>
        </li>
        <li>
          <a href="epad902230s.html#144">Libro quarto</a>
        </li>
        <li>
          <a href="epad902230s.html#178">Libro quinto</a>
        </li>
        <li>
          <a href="epad902230s.html#223">Libro sesto</a>
        </li>
        <li>
          <a href="epad902230s.html#248">Libro settimo</a>
        </li>
        <li>
          <a href="epad902230s.html#322">Libro ottavo</a>
        </li>
        <li>
          <a href="epad902230s.html#399">Libro Nono</a>
        </li>
        <li>
          <a href="epad902230s.html#554">Tavola terza delle cose
          notabili</a>
        </li>
        <li>
          <a href="epad902230s.html#580">Tavola quarta delli
          cittadini Padovani [...]</a>
        </li>
        <li>
          <a href="epad902230s.html#587">Tavola quinta delli
          cittadini Padovani Religiosi claustrali [...]</a>
        </li>
        <li>
          <a href="epad902230s.html#588">Epigramma</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
