<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503441as.html#6">Le vestigia e rarità di Roma
      antica</a>
      <ul>
        <li>
          <a href="dg4503441as.html#8">Beatissimo Padre</a>
        </li>
        <li>
          <a href="dg4503441as.html#10">Al lettore</a>
        </li>
        <li>
          <a href="dg4503441as.html#12">Indice de' capitoli</a>
        </li>
        <li>
          <a href="dg4503441as.html#15">Indice delle chiese del
          primo libro</a>
        </li>
        <li>
          <a href="dg4503441as.html#17">Indice de' Soggetti
          nominati</a>
        </li>
        <li>
          <a href="dg4503441as.html#18">I. Le vestigia di Roma
          antica&#160;</a>
          <ul>
            <li>
              <a href="dg4503441as.html#276">Appendice</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503441as.html#288">II. Le singolarità di
          Roma moderna&#160;</a>
          <ul>
            <li>
              <a href="dg4503441as.html#290">Indice de'
              capitoli</a>
            </li>
            <li>
              <a href="dg4503441as.html#292">Indice de' Palazzi
              ripieni di rarità</a>
            </li>
            <li>
              <a href="dg4503441as.html#293">Indice delle chiese,
              e loro singolarità</a>
            </li>
            <li>
              <a href="dg4503441as.html#295">Indice de' soggetti
              nominati</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
