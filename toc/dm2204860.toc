<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dm2204860s.html#8">Artisti svizzeri in Roma nei
      secoli XV, XVI e XVII</a>
      <ul>
        <li>
          <a href="dm2204860s.html#10">Indice delle cose più
          notevoli</a>
        </li>
        <li>
          <a href="dm2204860s.html#13">Indice degli artisti e di
          altri nominati in questo lavoro</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dm2204860s.html#22">[Text]</a>
      <ul>
        <li>
          <a href="dm2204860s.html#22">Introduzione</a>
        </li>
        <li>
          <a href="dm2204860s.html#24">II. Secolo XV.</a>
        </li>
        <li>
          <a href="dm2204860s.html#29">III. Secolo XVI.
          Architetti, Ingegneri, Intraprenditori di lavori
          edilizi.</a>
        </li>
        <li>
          <a href="dm2204860s.html#39">IV. Scultori, Fonditori,
          Stuccatori.</a>
        </li>
        <li>
          <a href="dm2204860s.html#47">V. Intagliatori in oro,
          ottone, ferro e legno.</a>
        </li>
        <li>
          <a href="dm2204860s.html#49">VI. Pittore- Stampatore-
          Ricamatori.</a>
        </li>
        <li>
          <a href="dm2204860s.html#50">VII. Artisti svizzeri nel
          secolo XVI.</a>
        </li>
        <li>
          <a href="dm2204860s.html#53">VIII. Secolo XVII.
          Architetti- Ingegneri.</a>
        </li>
        <li>
          <a href="dm2204860s.html#67">IX. Scultori-
          Stuccatori.</a>
        </li>
        <li>
          <a href="dm2204860s.html#74">X. Orefici- Orologiari-
          Coniatori.</a>
        </li>
        <li>
          <a href="dm2204860s.html#76">XI. Intagliatori in ottone,
          ferro, legno.</a>
        </li>
        <li>
          <a href="dm2204860s.html#78">XII. Pittori.</a>
        </li>
        <li>
          <a href="dm2204860s.html#83">XIII. Ricamatori.</a>
        </li>
        <li>
          <a href="dm2204860s.html#84">XIV. Le Belle Arti svizzere
          nel secolo XVII.</a>
        </li>
        <li>
          <a href="dm2204860s.html#89">Appendice</a>
        </li>
        <li>
          <a href="dm2204860s.html#91">Epilogo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
