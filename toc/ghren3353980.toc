<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghren3353980s.html#6">Della pittura friulana</a>
      <ul>
        <li>
          <a href="ghren3353980s.html#8">A S. E. Reverendissimo
          Monsig. Pierantonio Zorzi</a>
        </li>
        <li>
          <a href="ghren3353980s.html#10">[Della pittura
          friulana]</a>
        </li>
        <li>
          <a href="ghren3353980s.html#104">Indice de' pittori
          friulani</a>
        </li>
        <li>
          <a href="ghren3353980s.html#108">Rapporto fatto per
          ordine dell'Accademia di Scienze, Lettere, ed
          Arti&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
