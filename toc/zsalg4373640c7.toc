<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zsalg4373640c7s.html#6">Opere; Tomo VII.</a>
      <ul>
        <li>
          <a href="zsalg4373640c7s.html#8">Pensieri diversi
          sopra materie filosofiche e filologiche</a>
        </li>
        <li>
          <a href="zsalg4373640c7s.html#262">Lettere di
          Polianzio ad Ermogene Intorno alla Tradizione dell'Eneide
          dal Caro</a>
        </li>
        <li>
          <a href="zsalg4373640c7s.html#402">Indice delle
          materie contenute nel Tomo Settimo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
