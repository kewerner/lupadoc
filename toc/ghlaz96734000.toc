<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlaz96734000s.html#1">Memorie d'alcuni più
      celebri pittori di Urbin &#160;&#160;</a>
      <ul>
        <li>
          <a href="ghlaz96734000s.html#6">Memorie di
          Raffaello</a>
          <ul>
            <li>
              <a href="ghlaz96734000s.html#10">Al nobil uomo il
              Signor Filippo Viviani&#160;</a>
            </li>
            <li>
              <a href="ghlaz96734000s.html#36">Albero Genealogico
              della famiglia Sanzia di Urbino</a>
            </li>
            <li>
              <a href="ghlaz96734000s.html#37">Appendice alle
              memorie di Raffaello</a>
            </li>
            <li>
              <a href="ghlaz96734000s.html#47">Opere pittoriche
              di Raffaello</a>
            </li>
            <li>
              <a href="ghlaz96734000s.html#49">Opere
              archittettoniche di Raffaello</a>
            </li>
            <li>
              <a href="ghlaz96734000s.html#49">Catalogo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghlaz96734000s.html#52">Memorie di Federico
          Barocci di Urbino&#160;</a>
          <ul>
            <li>
              <a href="ghlaz96734000s.html#54">A sua Eccellenza
              il Signor Cavaliere Girolamo Staccoli</a>
            </li>
            <li>
              <a href="ghlaz96734000s.html#85">Appendice alle
              memorie di Federico Barocci</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghlaz96734000s.html#92">Memorie di Girolamo
          Ebartolommeo Genga di Urbino&#160;</a>
          <ul>
            <li>
              <a href="ghlaz96734000s.html#94">All' illustrissimo
              Signor canonico Alessandro Liera</a>
            </li>
            <li>
              <a href="ghlaz96734000s.html#116">Di Bartolommeo
              Genga da Urbino</a>
            </li>
            <li>
              <a href="ghlaz96734000s.html#123">Giustificazione
              dell'Albero Genga</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghlaz96734000s.html#130">Memorie di Timoteo
          Viti di Urbino</a>
          <ul>
            <li>
              <a href="ghlaz96734000s.html#132">A sua Eccellenza
              il Signor marchese Antaldo Antaldi</a>
            </li>
            <li>
              <a href="ghlaz96734000s.html#160">Albero della
              nobile famiglia Viti d'Urbino</a>
            </li>
            <li>
              <a href="ghlaz96734000s.html#162">Giustificazione
              dell'albero suddetto</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
