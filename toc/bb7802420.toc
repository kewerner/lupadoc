<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="bb7802420s.html#6">Heroico splendore delle città
      del mondo</a>
      <ul>
        <li>
          <a href="bb7802420s.html#8">[Piante]</a>
          <ul>
            <li>
              <a href="bb7802420s.html#8">Novum Zamoscium</a>
            </li>
            <li>
              <a href="bb7802420s.html#10">Toleto</a>
            </li>
            <li>
              <a href="bb7802420s.html#12">Città vecchia di
              Malta</a>
            </li>
            <li>
              <a href="bb7802420s.html#14">Valletta città nova di
              Malta</a>
            </li>
            <li>
              <a href="bb7802420s.html#16">Palermo</a>
            </li>
            <li>
              <a href="bb7802420s.html#18">Messina</a>
            </li>
            <li>
              <a href="bb7802420s.html#20">Napoli</a>
            </li>
            <li>
              <a href="bb7802420s.html#22">Aquila</a>
            </li>
            <li>
              <a href="bb7802420s.html#24">Cortona città
              antichissima in Toscana Metropoli</a>
            </li>
            <li>
              <a href="bb7802420s.html#26">Reggio</a>
            </li>
            <li>
              <a href="bb7802420s.html#28">Venetia</a>
            </li>
            <li>
              <a href="bb7802420s.html#30">Padoa</a>
            </li>
            <li>
              <a href="bb7802420s.html#32">Bononia</a>
            </li>
            <li>
              <a href="bb7802420s.html#34">Ancona</a>
            </li>
            <li>
              <a href="bb7802420s.html#36">Auximum&#160;</a>
            </li>
            <li>
              <a href="bb7802420s.html#38">Civitas nova in
              Piceno</a>
            </li>
            <li>
              <a href="bb7802420s.html#40">Orvieto</a>
            </li>
            <li>
              <a href="bb7802420s.html#42">Antica città di
              Norsia</a>
            </li>
            <li>
              <a href="bb7802420s.html#44">Recanati</a>
            </li>
            <li>
              <a href="bb7802420s.html#46">Regia Pici Macerata</a>
            </li>
            <li>
              <a href="bb7802420s.html#48">L'antichissima città di
              Todi</a>
            </li>
            <li>
              <a href="bb7802420s.html#50">Perugia Augusta</a>
            </li>
            <li>
              <a href="bb7802420s.html#52">Velletri</a>
            </li>
            <li>
              <a href="bb7802420s.html#54">Terni</a>
            </li>
            <li>
              <a href="bb7802420s.html#56">Fossombrone</a>
            </li>
            <li>
              <a href="bb7802420s.html#58">Viterbo</a>
            </li>
            <li>
              <a href="bb7802420s.html#60">Parma</a>
            </li>
            <li>
              <a href="bb7802420s.html#62">Piacenza</a>
            </li>
            <li>
              <a href="bb7802420s.html#64">Fiorenza</a>
            </li>
            <li>
              <a href="bb7802420s.html#66">Siena</a>
            </li>
            <li>
              <a href="bb7802420s.html#68">Mediolanum</a>
            </li>
            <li>
              <a href="bb7802420s.html#70">La magnifica città di
              Brescia in Lombardia</a>
            </li>
            <li>
              <a href="bb7802420s.html#72">Genua</a>
            </li>
            <li>
              <a href="bb7802420s.html#74">Marsilia</a>
            </li>
            <li>
              <a href="bb7802420s.html#76">Nizza</a>
            </li>
            <li>
              <a href="bb7802420s.html#78">Momigliano</a>
            </li>
            <li>
              <a href="bb7802420s.html#80">Digion</a>
            </li>
            <li>
              <a href="bb7802420s.html#82">Sedunum primaria et
              Metropo litica valesiae germanorum et gallorum</a>
            </li>
            <li>
              <a href="bb7802420s.html#84">Parigi</a>
            </li>
            <li>
              <a href="bb7802420s.html#86">Nansi&#160;</a>
            </li>
            <li>
              <a href="bb7802420s.html#88">Augusta</a>
            </li>
            <li>
              <a href="bb7802420s.html#90">Vienna Austriae</a>
            </li>
            <li>
              <a href="bb7802420s.html#92">Lublinum</a>
            </li>
            <li>
              <a href="bb7802420s.html#94">Leopolis</a>
            </li>
            <li>
              <a href="bb7802420s.html#96">Vilna lithuaniae
              Metropolis</a>
            </li>
            <li>
              <a href="bb7802420s.html#98">Moscovia urbs
              metropolis totius Russiae Albae&#160;</a>
            </li>
            <li>
              <a href="bb7802420s.html#100">Constantinopoli</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
