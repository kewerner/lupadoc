<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="gi1769146s.html#8">Serie Degli Uomini I Più
      Illustri Nella Pittura, Scultura, E Architettura; Tomo
      IV.</a>
      <ul>
        <li>
          <a href="gi1769146s.html#10">Eccellenza</a>
        </li>
        <li>
          <a href="gi1769146s.html#19">Alberto Durero</a>
        </li>
        <li>
          <a href="gi1769146s.html#31">Luca Kranach</a>
        </li>
        <li>
          <a href="gi1769146s.html#37">Quintino Messis</a>
        </li>
        <li>
          <a href="gi1769146s.html#43">Guglielmo Marcilla</a>
        </li>
        <li>
          <a href="gi1769146s.html#51">Michelangioli
          Buonarroti</a>
        </li>
        <li>
          <a href="gi1769146s.html#93">Domenico Puligo</a>
        </li>
        <li>
          <a href="gi1769146s.html#97">Galeazzo Campi, pittore
          cremonese</a>
        </li>
        <li>
          <a href="gi1769146s.html#103">Mariotti Albertinelli,
          pittore fiorentino</a>
        </li>
        <li>
          <a href="gi1769146s.html#111">Raffaellino Del Garbo,
          pittore fiorentino</a>
        </li>
        <li>
          <a href="gi1769146s.html#119">Girolamo Genga, pittore,
          e architetto di Urbino&#160;</a>
        </li>
        <li>
          <a href="gi1769146s.html#125">Giorgione da
          Castelfranco</a>
        </li>
        <li>
          <a href="gi1769146s.html#141">Francesco
          Granacci&#160;</a>
        </li>
        <li>
          <a href="gi1769146s.html#147">Andrea del Sarto</a>
        </li>
        <li>
          <a href="gi1769146s.html#165">Lorenzo Costa</a>
        </li>
        <li>
          <a href="gi1769146s.html#171">Jacopo Sansovino,
          scultore fiorentino</a>
        </li>
        <li>
          <a href="gi1769146s.html#181">Jacopo Razzi</a>
        </li>
        <li>
          <a href="gi1769146s.html#187">Antonio da Sangallo</a>
        </li>
        <li>
          <a href="gi1769146s.html#197">Tiziano Vecelli</a>
        </li>
        <li>
          <a href="gi1769146s.html#211">Giuliano Bugiardini</a>
        </li>
        <li>
          <a href="gi1769146s.html#219">Baldassarre Peruzzi</a>
        </li>
        <li>
          <a href="gi1769146s.html#227">Giovan Antonio
          Sogliani</a>
        </li>
        <li>
          <a href="gi1769146s.html#233">Marcantonio Raimondi,
          detto de' Franci, intagliatore bolognese</a>
        </li>
        <li>
          <a href="gi1769146s.html#243">Elogio di Jacopo Palma
          il Vecchio</a>
        </li>
        <li>
          <a href="gi1769146s.html#249">Marcantio Francia
          Bigio&#160;</a>
        </li>
        <li>
          <a href="gi1769146s.html#255">Raffaello da Urbino</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="gi1769146s.html#276">Serie Degli Uomini I Più
      Illustri Nella Pittura, Scultura, E Architettura; Tomo V.</a>
      <ul>
        <li>
          <a href="gi1769146s.html#278">Illustrissimo
          Signore</a>
        </li>
        <li>
          <a href="gi1769146s.html#287">Gio. Antonio Licinio,
          detto il Pordenone</a>
        </li>
        <li>
          <a href="gi1769146s.html#299">Domenico Beccafumi
          senese</a>
        </li>
        <li>
          <a href="gi1769146s.html#311">Valerio Vicentino</a>
        </li>
        <li>
          <a href="gi1769146s.html#321">Michele Sanmichele da
          Verona</a>
        </li>
        <li>
          <a href="gi1769146s.html#333">Fra Sebastiano veneziano
          detto del Piombo&#160;</a>
        </li>
        <li>
          <a href="gi1769146s.html#341">Morto da Feltre</a>
        </li>
        <li>
          <a href="gi1769146s.html#349">Benvenuto Garofalo da
          Ferrara</a>
        </li>
        <li>
          <a href="gi1769146s.html#359">Ridolfo Ghirlandajo,
          pittore fiorentino</a>
        </li>
        <li>
          <a href="gi1769146s.html#371">Baccio Bandinelli</a>
        </li>
        <li>
          <a href="gi1769146s.html#391">Alfonso Lombardo,
          scultore ferrarese</a>
        </li>
        <li>
          <a href="gi1769146s.html#401">Gio. Francesco
          Penni,detto il fattore pittore fiorentino</a>
        </li>
        <li>
          <a href="gi1769146s.html#407">Polidoro da Caravaggio,
          pittore</a>
        </li>
        <li>
          <a href="gi1769146s.html#417">Antonio Allegri da
          Coreggio</a>
        </li>
        <li>
          <a href="gi1769146s.html#435">Giulio Romano</a>
        </li>
        <li>
          <a href="gi1769146s.html#453">Bartolommeo da
          Bagnacavallo, pittore romagnuolo</a>
        </li>
        <li>
          <a href="gi1769146s.html#461">Giacomo da Pontormo</a>
        </li>
        <li>
          <a href="gi1769146s.html#477">Luca di Leida, detto
          Luca d'Olanda</a>
        </li>
        <li>
          <a href="gi1769146s.html#487">Lorenzetto, scultore, e
          architetto fiorentino</a>
        </li>
        <li>
          <a href="gi1769146s.html#493">Lorenzo Lotto</a>
        </li>
        <li>
          <a href="gi1769146s.html#501">Domenico Riccio detto il
          Brusasorci</a>
        </li>
        <li>
          <a href="gi1769146s.html#507">Properzia de Rossi,
          scultrice bolognese</a>
        </li>
        <li>
          <a href="gi1769146s.html#513">Giovanni da Udine,
          pittore</a>
        </li>
        <li>
          <a href="gi1769146s.html#525">Rosso del Rosso, pittore
          fiorentino</a>
        </li>
        <li>
          <a href="gi1769146s.html#539">Simone Mosca, scultore,
          ed architetto&#160;</a>
        </li>
        <li>
          <a href="gi1769146s.html#547">Giulio Clovio</a>
        </li>
        <li>
          <a href="gi1769146s.html#554">Indice</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="gi1769146s.html#556">Serie Degli Uomini I Più
      Illustri Nella Pittura, Scultura, E Architettura; Tomo
      VI.</a>
      <ul>
        <li>
          <a href="gi1769146s.html#558">Illustrissimo
          Signore</a>
        </li>
        <li>
          <a href="gi1769146s.html#566">Indice</a>
        </li>
        <li>
          <a href="gi1769146s.html#569">Niccolò detto il
          Tribolo</a>
        </li>
        <li>
          <a href="gi1769146s.html#585">Giovanni
          Holbein&#160;</a>
        </li>
        <li>
          <a href="gi1769146s.html#595">Cristoforo Gherardi</a>
        </li>
        <li>
          <a href="gi1769146s.html#605">Perino del Vaga</a>
        </li>
        <li>
          <a href="gi1769146s.html#619">Francesco
          Primaticcio</a>
        </li>
        <li>
          <a href="gi1769146s.html#627">Francesco Mazzuoli</a>
        </li>
        <li>
          <a href="gi1769146s.html#637">Fra Agnolo
          Montorsoli</a>
        </li>
        <li>
          <a href="gi1769146s.html#651">Francesco Rustici</a>
        </li>
        <li>
          <a href="gi1769146s.html#661">Girolamo Carpi</a>
        </li>
        <li>
          <a href="gi1769146s.html#669">Giacomo Barocci da
          Vignola&#160;</a>
        </li>
        <li>
          <a href="gi1769146s.html#683">Girolamo da Trevigi</a>
        </li>
        <li>
          <a href="gi1769146s.html#689">Paris Bordone</a>
        </li>
        <li>
          <a href="gi1769146s.html#697">Daniello Ricciarelli</a>
        </li>
        <li>
          <a href="gi1769146s.html#709">Agnolo Bronzino</a>
        </li>
        <li>
          <a href="gi1769146s.html#719">Francesco Salviati</a>
        </li>
        <li>
          <a href="gi1769146s.html#731">Francesco da S. Gallo e
          di Sebastiano suo fratello detto Aristotile</a>
        </li>
        <li>
          <a href="gi1769146s.html#739">Giacomo da Bassano</a>
        </li>
        <li>
          <a href="gi1769146s.html#759">Bartolommeo
          Ammannati</a>
        </li>
        <li>
          <a href="gi1769146s.html#771">Camillo Boccaccino</a>
        </li>
        <li>
          <a href="gi1769146s.html#777">Giorgio Vasari</a>
        </li>
        <li>
          <a href="gi1769146s.html#795">Jacopo Robusti</a>
        </li>
        <li>
          <a href="gi1769146s.html#811">Alessandro Buonvicino
          detto il Moretto</a>
        </li>
        <li>
          <a href="gi1769146s.html#819">Batista Franco</a>
        </li>
        <li>
          <a href="gi1769146s.html#827">Andrea Palladio</a>
        </li>
        <li>
          <a href="gi1769146s.html#843">Antonio Moro</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
