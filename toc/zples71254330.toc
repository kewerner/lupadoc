<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zples71254330s.html#6">Del Laocoonte o sia Dei
      limiti della pittura e della poesia discorso di G. E.
      Lessing. Recato dal tedesco in italiano dal Cavaliere C. G.
      Londonio</a>
      <ul>
        <li>
          <a href="zples71254330s.html#8">Il Traduttore ai
          Lettori</a>
        </li>
        <li>
          <a href="zples71254330s.html#14">Prefazione
          dell'Autore</a>
        </li>
        <li>
          <a href="zples71254330s.html#20">Del Laocoonte</a>
          <ul>
            <li>
              <a href="zples71254330s.html#20">I.</a>
            </li>
            <li>
              <a href="zples71254330s.html#26">II.</a>
            </li>
            <li>
              <a href="zples71254330s.html#35">III.</a>
            </li>
            <li>
              <a href="zples71254330s.html#39">IV.</a>
            </li>
            <li>
              <a href="zples71254330s.html#52">V.</a>
            </li>
            <li>
              <a href="zples71254330s.html#63">VI.</a>
            </li>
            <li>
              <a href="zples71254330s.html#69">VII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#74">VIII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#80">IX.</a>
            </li>
            <li>
              <a href="zples71254330s.html#85">X.</a>
            </li>
            <li>
              <a href="zples71254330s.html#89">XI.</a>
            </li>
            <li>
              <a href="zples71254330s.html#94">XII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#100">XIII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#104">XIV.</a>
            </li>
            <li>
              <a href="zples71254330s.html#107">XV.</a>
            </li>
            <li>
              <a href="zples71254330s.html#109">XVI.</a>
            </li>
            <li>
              <a href="zples71254330s.html#120">XVII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#128">XVIII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#137">XIX.</a>
            </li>
            <li>
              <a href="zples71254330s.html#145">XX.</a>
            </li>
            <li>
              <a href="zples71254330s.html#154">XXI.</a>
            </li>
            <li>
              <a href="zples71254330s.html#159">XXII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#167">XXIII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#173">XXIV.</a>
            </li>
            <li>
              <a href="zples71254330s.html#177">XXV.</a>
            </li>
            <li>
              <a href="zples71254330s.html#188">XXVI.</a>
            </li>
            <li>
              <a href="zples71254330s.html#197">XXVII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#202">XXVIII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#206">XXIX.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zples71254330s.html#214">Note</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="zples71254330s.html#258">Frammenti della seconda
      Parte del Lacoonte di Lessing Traduzione dall'Originale
      tedesco coll'Aggiunta di alcune Note e d'un Appendice del
      Cavaliere C. G. Londonio</a>
      <ul>
        <li>
          <a href="zples71254330s.html#260">Il Traduttore ai
          Lettori</a>
        </li>
        <li>
          <a href="zples71254330s.html#262">Frammenti della
          seconda Parte del Lacoonte di Lessing</a>
          <ul>
            <li>
              <a href="zples71254330s.html#262">XXX.</a>
            </li>
            <li>
              <a href="zples71254330s.html#262">XXXI.</a>
            </li>
            <li>
              <a href="zples71254330s.html#265">XXXII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#265">XXXIII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#265">XXXIV.</a>
            </li>
            <li>
              <a href="zples71254330s.html#266">XXXV.</a>
            </li>
            <li>
              <a href="zples71254330s.html#267">XXXVI.</a>
            </li>
            <li>
              <a href="zples71254330s.html#267">XXXVII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#267">XXXVIII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#269">XXXIX.</a>
            </li>
            <li>
              <a href="zples71254330s.html#271">XL.</a>
            </li>
            <li>
              <a href="zples71254330s.html#273">XLI.</a>
            </li>
            <li>
              <a href="zples71254330s.html#277">XLII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#277">XLIII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#279">XLIV.</a>
            </li>
            <li>
              <a href="zples71254330s.html#287">XLV.</a>
            </li>
            <li>
              <a href="zples71254330s.html#296">XLVI.</a>
            </li>
            <li>
              <a href="zples71254330s.html#302">XLVII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#306">XLVIII.</a>
            </li>
            <li>
              <a href="zples71254330s.html#307">XLIX.</a>
            </li>
            <li>
              <a href="zples71254330s.html#309">L.</a>
            </li>
            <li>
              <a href="zples71254330s.html#311">LI.</a>
            </li>
            <li>
              <a href="zples71254330s.html#313">LII.</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="zples71254330s.html#316">Dei Quadri storici
      Appendice del Traduttore</a>
      <ul>
        <li>
          <a href="zples71254330s.html#318">I.</a>
        </li>
        <li>
          <a href="zples71254330s.html#321">II.</a>
        </li>
        <li>
          <a href="zples71254330s.html#327">III.</a>
        </li>
        <li>
          <a href="zples71254330s.html#330">IV.</a>
        </li>
        <li>
          <a href="zples71254330s.html#333">V.</a>
        </li>
        <li>
          <a href="zples71254330s.html#338">VI.</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="zples71254330s.html#340">Indice del Lacoonte</a>
      <ul>
        <li>
          <a href="zples71254330s.html#340">Parte prima</a>
        </li>
        <li>
          <a href="zples71254330s.html#341">Frammenti della
          seconda Parte</a>
        </li>
        <li>
          <a href="zples71254330s.html#342">Appendice del
          Traduttore</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
