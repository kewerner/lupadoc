<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efir2653890s.html#6">Ornati Presi da Graffiti, e
      Pitture antiche Esistenti in Firenze disegnati ed incise in
      40 Rami da Carlo Lasinio Trevigiano</a>
      <ul>
        <li>
          <a href="efir2653890s.html#8">No. 1 [Ornati ▣]</a>
        </li>
        <li>
          <a href="efir2653890s.html#10">No. 2 [Ornati ▣]</a>
        </li>
        <li>
          <a href="efir2653890s.html#12">No. 3 Graffito di Andrea
          &#160;di Cosimo Feltrini esisente in una facciata di Casa
          in Via Maggio ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#14">No. 4 Pitture del
          Poccetti com binate da vari Luoghi ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#16">No. 5 Graffitto di
          Andrea di Cosimo Fel trini esistente nella Sud.a
          &#160;Casa ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#18">No. 6 Grottesco Scolpito
          da Benedetto da Rovezzano in un Cammino del Signore Cave
          del Turco/Borgo Santi Apostoli ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#20">No. 7 Altro Pezzo del
          Grottesco esistente nel Cammino del Signore Cav. e del
          Turco ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#22">No. 8 Frammenti di
          Ornati antichi in basso rilievo combinati da vari luoghi
          ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#24">No. 9 Frammenti di
          Ornati antichi in basso rilievo combinati da vari luoghi
          ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#26">No. 10 Rabeschi di
          Michel Angelo Buonarroti dipinti in una finestra alla
          Libreria Laurenziana ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#28">No. 11 Seconda finestra
          della Libreria Laurenziana dipinta da Giovanni &#160;da
          Udine ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#30">No. 12 Terza finestra
          della Libreria Laurenziana dipinta da Giovanni da Udine
          ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#32">No. 13 Frammenti di
          Ornati antichi in basso rilievo combinati da vari luoghi
          ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#34">No. 14 Graffito di
          Andrea di Cosimo Feltrini esistente in una Casa di Via
          Maggio ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#36">No. 15 Graffito di
          Bennardino Poccetti esistente in Via Maggio nella
          facciata di una Casa de Signori &#160;Marchesi Riccardi
          ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#38">No. 16 Graffito di
          Bennardino Poccetti esistente in Via Maggio nella
          facciata di una Casa de Signori &#160;Marchesi Riccardi
          ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#40">No. 17 Graffito di
          Bennardino Poccetti esistente in Via Maggio nella
          facciata di una Casa de Signori &#160;Marchesi Riccardi
          ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#42">No. 18 Graffito di
          Bennardino Poccetti esistente in Via Maggio nella
          facciata di una Casa de Signori &#160;Marchesi Riccardi
          ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#44">No. 19 Graffiti diversi
          esistenti nella Sud.a Casa ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#46">No. 20 Ornato antico
          esistente in una facciata di Casa in Parione ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#48">No. 21 Graffiti di
          Bennardino Poccetti esistenti nella facciata del Palazzo
          de Signori Corboli Lungarno ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#50">No. 22 Graffiti di
          Bennardino Poccetti esistenti nella facciata del Palazzo
          de Signori Corboli Lungarno ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#52">No. 23 Finestra dipinta
          da Giovanni da Udine nella Certosa ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#54">No. 24 &#160;Finestra
          dipinta da Giovanni da Udine nella Certosa ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#56">No. 25 Seconda finestra
          dipinta da Giovanni da Udine nella Certosa ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#58">No. 26 Terza
          &#160;finestra dipinta da &#160;Giovanni da Udine nella
          Certosa ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#60">No. 27 Ornati di
          Bernardino Poccetti combinati da vari luoghi ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#62">No. 28 Ornati di
          Bernardino Poccetti in uno sfondo in Casa il Signore Cave
          Uguccioni ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#64">No. 29 Sfondo da
          Bernardino Poccetti dipinto nel ingresso del Palazzo del
          Signore Cave Pitti Gaddi ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#66">No. 30 Parte dello
          sfondo della Cappella del Capitolo di Santa Maria Novella
          dipinto da Bernardino Poccetti ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#68">No. 31 Graffiti antichi
          combinati da vari luoghi ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#70">No. 32 Sfondo dipinto da
          Bernardino Poccetti in una Casa di Borgo Santa Croce
          ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#72">No. 33 Graffiti di
          Bernardino Poccetti nella facciata del palazzo Ramirez di
          Moltavo ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#74">No. 34 Graffiti di
          Bernardino Graffiti di Bernardino Poccetti nella facciata
          del palazzo Ramirez di Moltavo ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#76">No. 35 Ornati antichi
          che si vedono in una facciata di Casa su la piazza di San
          Stefano ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#78">No 36 Ornati antichi che
          si vedono in una facciata di Casa su la piazza di San
          Stefano ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#80">No. 37 Ornati antichi
          combinati da vari luoghi ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#82">No. 38 Graffito di
          Bernardino Poccetti esistente nel Cortile del Palazzo
          abitato dal Ministro di Francia, di proprietà de Signori
          Marchesi Riccardi l'ungo l'arno ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#84">No. 39 Ornato antico
          levato da un Graffito dal Palazzo Montalvi ▣</a>
        </li>
        <li>
          <a href="efir2653890s.html#86">No. 40 Ornati antichi
          che si vedono in una facciata di casa su la piazza di San
          Stefano ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
