<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ecre1524180s.html#6">Guida storica sacra della R.
      città e sobborghi di Cremona per gli amatori delle belle
      arti</a>
      <ul>
        <li>
          <a href="ecre1524180s.html#8">Al nobilissimo e
          reverendissimo Monsignore D. Cesare Ghisi</a>
        </li>
        <li>
          <a href="ecre1524180s.html#12">Al lettore</a>
        </li>
        <li>
          <a href="ecre1524180s.html#14">[Guida storica sacra
          della R. città e sobborghi di Cremona per gli amatori
          delle belle arti]</a>
        </li>
        <li>
          <a href="ecre1524180s.html#144">Chiese nei sobborghi di
          Cremona</a>
        </li>
        <li>
          <a href="ecre1524180s.html#164">Direzioni, ifficj ed
          altri stabilimenti in ordine alfabetico</a>
        </li>
        <li>
          <a href="ecre1524180s.html#176">Dicasteri pubblici</a>
        </li>
        <li>
          <a href="ecre1524180s.html#190">Gallerie particolari in
          ordine alfabetico</a>
        </li>
        <li>
          <a href="ecre1524180s.html#198">Conclusione</a>
        </li>
        <li>
          <a href="ecre1524180s.html#200">Ordine</a>
        </li>
        <li>
          <a href="ecre1524180s.html#202">Indice d'altre cose
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
