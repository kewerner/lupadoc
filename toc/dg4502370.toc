<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502370s.html#8">Ristretto delle Grandezze di
      Roma</a>
      <ul>
        <li>
          <a href="dg4502370s.html#10">All'illustrissimi et
          eccellentissimi Signori e Padroni miei colendissimi li
          Signori Carlo, e Maffeo Barberini</a>
        </li>
        <li>
          <a href="dg4502370s.html#14">Pompilio Totti a'
          lettori</a>
        </li>
        <li>
          <a href="dg4502370s.html#16">Tavola della cose più
          notabili</a>
        </li>
        <li>
          <a href="dg4502370s.html#22">Numero di tutte le
          Chiese,Parocchie Baptismali, Parocchie semplici,
          Monasterij, Collegij, Spedali, Compagnie, et altri
          luoghi&#160;</a>
        </li>
        <li>
          <a href="dg4502370s.html#24">Li Titoli delli Cardinali
          di S. Chiesa</a>
        </li>
        <li>
          <a href="dg4502370s.html#32">Delle Chiese di Roma e suoi
          Rioni</a>
        </li>
        <li>
          <a href="dg4502370s.html#117">La Guida Romana per li
          forastieri, che desiderano vedere non solo le Antichità,
          ma le fabriche principali di tutta Roma</a>
        </li>
        <li>
          <a href="dg4502370s.html#139">Raccolta di alcune cose
          traslaciate ne' Rioni</a>
        </li>
        <li>
          <a href="dg4502370s.html#145">Catalago delle Reliquie
          de' Santi, che si trovano in tutte le Chiese di Roma</a>
        </li>
        <li>
          <a href="dg4502370s.html#202">Catalogo delle feste, et
          indulgentie plenarie e perpetue</a>
        </li>
        <li>
          <a href="dg4502370s.html#232">Grandezze dell'Impero
          Romano cavate da Giulio Lipsio, e da altri autori</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
