<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="erav554080s.html#6">Compendio istorico dell’arte
      di comporre i musaici, con la descrizione de’ musaici
      antichi, che trovansi nelle basiliche di Ravenna, e con due
      brevi ragionamenti l’uno su la Ravennate pignata, l’altro su
      la Repubblica delle Api</a>
      <ul>
        <li>
          <a href="erav554080s.html#8">All'ornatissimo Signore
          Giuliano Monaldini</a>
        </li>
        <li>
          <a href="erav554080s.html#12">A chi legge</a>
        </li>
        <li>
          <a href="erav554080s.html#20">Origine, formazione, e
          progressi della musicaria, ossia dell'arte di comporre, e
          lavorare i musajci</a>
        </li>
        <li>
          <a href="erav554080s.html#92">Descrizione delle antiche
          opere di musajco nelle chiese di Ravenna&#160;</a>
        </li>
        <li>
          <a href="erav554080s.html#204">Ragionamento due su la
          Pigneta Ravennate e su la Repubblica delle Alpi&#160;</a>
        </li>
        <li>
          <a href="erav554080s.html#262">Indice de' capitolo
          dell'opera sui musici</a>
        </li>
        <li>
          <a href="erav554080s.html#263">Indice delle chiese</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
