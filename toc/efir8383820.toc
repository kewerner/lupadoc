<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efir8383820s.html#6">Vago, e curioso ristretto
      profano, e sagro dell'historia bresciana</a>
      <ul>
        <li>
          <a href="efir8383820s.html#8">Al glorioso capo delli
          antichi</a>
        </li>
        <li>
          <a href="efir8383820s.html#10">A chi vuol leggere.
          L'autore</a>
        </li>
        <li>
          <a href="efir8383820s.html#14">Parte prima</a>
          <ul>
            <li>
              <a href="efir8383820s.html#14">I. Origine della
              città di Brescia</a>
            </li>
            <li>
              <a href="efir8383820s.html#18">II. Sito della Città
              di Brescia, e sua breve descrittione</a>
            </li>
            <li>
              <a href="efir8383820s.html#20">III. Grandezza del
              Bresciano: Sua figura, e suoi confini&#160;</a>
            </li>
            <li>
              <a href="efir8383820s.html#22">IV. Quanto Popolo
              faccia la Città di Brescia, e suo Territorio</a>
            </li>
            <li>
              <a href="efir8383820s.html#25">V. Brescia Capo
              degli Antichi, e Valorosi Cenomani, e sua
              confederatione co' Popoli Veneti</a>
            </li>
            <li>
              <a href="efir8383820s.html#27">VI. Grande stima, e
              conto, che fecero i Romani della Città di Brescia</a>
            </li>
            <li>
              <a href="efir8383820s.html#29">VII. Confederatione
              de' Romani co' Cenomani, contro Annibale</a>
            </li>
            <li>
              <a href="efir8383820s.html#31">VIII. Gallia
              Cisalpina lodata da Cicerone, e perche [...]</a>
            </li>
            <li>
              <a href="efir8383820s.html#33">IX. Brescia Colonia,
              e Municipio della Republica Romana</a>
            </li>
            <li>
              <a href="efir8383820s.html#35">X. Bresciani
              ch'hebbero in Roma la dignità del Consolato</a>
            </li>
            <li>
              <a href="efir8383820s.html#37">XI. Bresciani,
              Proconsoli dell'Asia, e Vicepretori dell'Ungheria
              inferiore, e superiore [...]</a>
            </li>
            <li>
              <a href="efir8383820s.html#38">XII. Protettori di
              Republiche</a>
            </li>
            <li>
              <a href="efir8383820s.html#39">XIII. Prencipi
              dell'Ordine de Cavalieri in Roma</a>
            </li>
            <li>
              <a href="efir8383820s.html#39">XIV. Bresciani, che
              hanno havuto Podestarie in diverse Città d'Italia</a>
            </li>
            <li>
              <a href="efir8383820s.html#43">XV. Bresciani, che
              sono stati Capitani del popolo in Milano, Bologna, e
              Orvieto</a>
            </li>
            <li>
              <a href="efir8383820s.html#45">XVI. Bresciani, che
              sono stati Generali, e conduttori d'Esserciti</a>
            </li>
            <li>
              <a href="efir8383820s.html#50">XVII. Luogotenenti,
              e Governatori Generali</a>
            </li>
            <li>
              <a href="efir8383820s.html#53">XVIII. Maestri di
              Campo</a>
            </li>
            <li>
              <a href="efir8383820s.html#55">XIX. Condottieri di
              Gente d'Armi</a>
            </li>
            <li>
              <a href="efir8383820s.html#63">XX. Colonelli</a>
            </li>
            <li>
              <a href="efir8383820s.html#67">XXI. Capitani</a>
            </li>
            <li>
              <a href="efir8383820s.html#67">XXII. Consiglieri
              d'Imperatori, Regi, e gran Prencipi</a>
            </li>
            <li>
              <a href="efir8383820s.html#70">XXIII.
              Ambasciatori</a>
            </li>
            <li>
              <a href="efir8383820s.html#77">XXIV. Promotori in
              Lombardia di Leghe</a>
            </li>
            <li>
              <a href="efir8383820s.html#77">XXV. Arbitri di
              differenze&#160;</a>
            </li>
            <li>
              <a href="efir8383820s.html#78">XXVI. Commissari</a>
            </li>
            <li>
              <a href="efir8383820s.html#79">XXVII. Bresciani,
              che sono stati Senatori in Roma, e Milano</a>
            </li>
            <li>
              <a href="efir8383820s.html#79">XXVIII. Bresciani,
              che sono stati Capitani di Giustitia in Milano</a>
            </li>
            <li>
              <a href="efir8383820s.html#80">XXIX. Vicegerenti
              d'Imperatori, e gran Prencipi</a>
            </li>
            <li>
              <a href="efir8383820s.html#80">XXX. Prodi ne
              duelli</a>
            </li>
            <li>
              <a href="efir8383820s.html#82">XXXI. Bresciani
              habilitati da Imperatori à duellar à nome loro con
              arme, e insegne Reali</a>
            </li>
            <li>
              <a href="efir8383820s.html#83">XXXII. Bresciani,
              che hanno havuto Privilegio presso il Serenissimo
              Prencipe di Venetia</a>
            </li>
            <li>
              <a href="efir8383820s.html#83">XXXIII. Bresciani,
              che hanno portato lo Stendardo d'Imperatori, e della
              Serenissima Republica di Venetia&#160;</a>
            </li>
            <li>
              <a href="efir8383820s.html#85">XXXIV. Bresciani
              insigniti di diversi Ordini di Cavaleria</a>
            </li>
            <li>
              <a href="efir8383820s.html#86">XXXV. Bresciani
              fabbricatori della Città d'Alessandria [...]</a>
            </li>
            <li>
              <a href="efir8383820s.html#88">XXXVI. Soccorsi
              prestati dalla Città di Brescia, e suoi Cittadini ad
              Imperatori, Rè, e gran' Prencipi</a>
            </li>
            <li>
              <a href="efir8383820s.html#93">XXXVII. Bresciani
              gran persecutori d'Eretici</a>
            </li>
            <li>
              <a href="efir8383820s.html#94">XXXVIII. Gran' conto
              sempre fatto da Bresciani della Religione Cattolica,
              e loro gran' divotione [...]</a>
            </li>
            <li>
              <a href="efir8383820s.html#96">XXXIX. Bresciani,
              che hanno fatto prigionieri Rè, e gran' Prencipi</a>
            </li>
            <li>
              <a href="efir8383820s.html#97">XXXX. Cancellieri, e
              Segretari d'Imperatori, Rè, e gran Prencipi</a>
            </li>
            <li>
              <a href="efir8383820s.html#98">XLI. Camerieri di
              gran' Prencipi, e Imperatori</a>
            </li>
            <li>
              <a href="efir8383820s.html#99">XLII. Coppieri
              d'Imperatori</a>
            </li>
            <li>
              <a href="efir8383820s.html#99">XLIII. Paggi di
              Prencipi, Rè, e Imperatori</a>
            </li>
            <li>
              <a href="efir8383820s.html#100">XLIV. Scudieri di
              gran Prencipi</a>
            </li>
            <li>
              <a href="efir8383820s.html#100">XLV. Capitani della
              Guardia</a>
            </li>
            <li>
              <a href="efir8383820s.html#101">XLVI. Residenti de
              Prencipi</a>
            </li>
            <li>
              <a href="efir8383820s.html#101">XLII.
              Giostratori</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
