<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg450830s.html#4">Reliquie rhomane urbis atque
      indulgentie</a>
      <ul>
        <li>
          <a href="dg450830s.html#7">De oratoriis et eius
          sanctuariis que sunt ad fontes dicte ecclesie sancti
          Johannis ›San Giovanni in Laterano‹</a>
        </li>
        <li>
          <a href="dg450830s.html#7">De oratorio sancti Laurentii
          in palacio quod dicitur sancta lanctorum [sic] ›San
          Lorenzo in Palatio ad Sancta Sanctorum‹</a>
        </li>
        <li>
          <a href="dg450830s.html#9">De ecclesia que vocatur ad
          sanctam crucem in hierusalem ›Santa Croce in
          Gerusalemme‹</a>
        </li>
        <li>
          <a href="dg450830s.html#10">De ecclesia sancti laurentii
          extra muros ›San Lorenzo fuori le Mura‹</a>
        </li>
        <li>
          <a href="dg450830s.html#11">De ecclesia sancte marie
          maioris ›Santa Maria Maggiore‹</a>
        </li>
        <li>
          <a href="dg450830s.html#13">De ecclesia sancti Petri
          ›San Pietro in Vaticano‹</a>
        </li>
        <li>
          <a href="dg450830s.html#14">De ecclesia sancti Pauli
          ›San Paolo fuori le Mura‹</a>
        </li>
        <li>
          <a href="dg450830s.html#14">De ecclesia sancti
          Sebastiani ›San Sebastiano‹</a>
        </li>
        <li>
          <a href="dg450830s.html#15">De ecclesia sancti petri in
          vincula ›San Pietro in Vincoli‹</a>
        </li>
        <li>
          <a href="dg450830s.html#15">De ecclesiis diversis beate
          marie virginis</a>
        </li>
        <li>
          <a href="dg450830s.html#15">De ecclesia sancte marie
          nove ›Santa Francesca Romana‹</a>
        </li>
        <li>
          <a href="dg450830s.html#16">In ecclesia sancte marie
          transpodiana ›Santa Maria in Traspontina‹</a>
        </li>
        <li>
          <a href="dg450830s.html#16">De ecclesia sancte Marie de
          populo ›Santa Maria del Popolo‹</a>
        </li>
        <li>
          <a href="dg450830s.html#17">De ecclesia beate Marie que
          dicitur araceli ›Santa Maria in Aracoeli‹</a>
        </li>
        <li>
          <a href="dg450830s.html#17">De ecclesia beate Marie in
          transtiberim ›Santa Maria in Trastevere‹</a>
        </li>
        <li>
          <a href="dg450830s.html#18">De alia capella beate marie
          virginis imperatrix ›Santa Maria Imperatrice‹</a>
        </li>
        <li>
          <a href="dg450830s.html#18">De ecclesia sancte marie
          rotunde ›Pantheon‹</a>
        </li>
        <li>
          <a href="dg450830s.html#18">De ecclesia sancte Hraxedis
          [sic] ›Santa Prassede‹</a>
        </li>
        <li>
          <a href="dg450830s.html#18">De ecclesia sancti georii
          [sic] ›San Giorgio in Velabro‹</a>
        </li>
        <li>
          <a href="dg450830s.html#18">De ecclesiis diversis primo
          de ecclesia sanctorum Viti et modesti ›Santi Vito e
          Modesto‹</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
