<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlec55212990a2s.html#8">Cabinet des
      singularitez d’architecture, peinture, sculpture et graveure,
      ou Introduction à la connoissance des plus beaux arts,
      figurés sous les tableaux, les statues &amp; les estampes;
      Tome II.</a>
      <ul>
        <li>
          <a href="ghlec55212990a2s.html#10">A Monseigneur
          Jules Hardouin Mansart</a>
        </li>
        <li>
          <a href="ghlec55212990a2s.html#12">Preface</a>
        </li>
        <li>
          <a href="ghlec55212990a2s.html#14">Reflexions</a>
        </li>
        <li>
          <a href="ghlec55212990a2s.html#18">Le Cabinet des
          singularietez d'architecture, peinture, sculpture, et
          graveure</a>
          <ul>
            <li>
              <a href="ghlec55212990a2s.html#50">Ecole
              romaine</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#104">Ecole de
              Lombardie et Venitienne</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#161">Ecole de
              Bologne, ou des Carraches</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#209">Peintres
              flamands, allemans, et hollandois&#160;</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#307">[Planche]</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#308">Explication
              de la planche qui suit&#160;</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#311">Marques
              d'autres Peintres et Graveurs italiens&#160;</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#315">Explication
              de la planche qui suit</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#316">[Planche]</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#319">Marques
              d'autres Peintres et Graveurs Ultramontains, dont
              voici l'explication</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#322">Oeuvre de
              Claude Mellan</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#356">Oeuvre de
              Tempete</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#372">Oeuvre de
              Guillelme Baurn&#160;</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#375">Catalogue de
              l'Oeuvre d'Abraham Bloemhaert</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#397">Oeuvre de
              Jaques Callot</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#414">Catalogue
              d'Etienne De La Belle&#160;</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#428">Catalogue des
              Sadelers</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#434">Catalogue de
              ce qui a été gravé d'aprés Monsieur Poussin fameux
              Peintre de ce siécle</a>
            </li>
            <li>
              <a href="ghlec55212990a2s.html#444">Table des
              principeaux Sujets et des noms des Peintres,
              Sculpteurs, et Graveurs</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
