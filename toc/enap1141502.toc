<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="enap1141502s.html#5">Napoli antica e moderna;
      parte seconda</a>
      <ul>
        <li>
          <a href="enap1141502s.html#9">Napoli moderna</a>
          <ul>
            <li>
              <a href="enap1141502s.html#9">I. Quadro generale
              di Napoli</a>
            </li>
            <li>
              <a href="enap1141502s.html#47">II. Palazzo
              Reale</a>
            </li>
            <li>
              <a href="enap1141502s.html#59">III. Chiese
              celebri</a>
            </li>
            <li>
              <a href="enap1141502s.html#164">IV. Università
              degli Studi</a>
            </li>
            <li>
              <a href="enap1141502s.html#173">V. Gabinetti di
              Machine Fisiche&#160;</a>
            </li>
            <li>
              <a href="enap1141502s.html#180">VI.
              Biblioteche</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
