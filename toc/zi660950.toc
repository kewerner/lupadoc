<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zi660950s.html#8">[Epitoma De Regno Apvlie Et
      Sicilie Redvcens Svmmatim In Vnvm Qvecvnqvae De Eo Tangvnt
      Historici Incidenter Et Sparsim Dvm Vniversaliter Rervm
      Omnivm Gesta Describvnt]</a>
      <ul>
        <li>
          <a href="zi660950s.html#8">Michael Fernus Mediolanensis
          Pomponio Laeto Litterato Principi</a>
        </li>
        <li>
          <a href="zi660950s.html#13">Alexandro VI. Pontifici
          Maximo</a>
        </li>
        <li>
          <a href="zi660950s.html#14">Epitoma De Regno Apvlie Et
          Sicilie Redvcens Svmmatim In Vnvm Qvecvnqvae De Eo
          Tangvnt Historici Incidenter Et Sparsim Dvm Vniversaliter
          Rervm Omnivm Gesta Describvnt</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
