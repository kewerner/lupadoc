<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dt13203470s.html#6">Istoria dell’antichissimo
      oratorio, o cappella di San Lorenzo nel Patriarchio
      Lateranense, comunemente appellato Sancta Sanctorum ›San
      Lorenzo in Palatio ad Sancta Sanctorum‹ e della celebre
      Immagine del SS. Salvatore detta Acheropita, che ivi
      conservasi ...</a>
      <ul>
        <li>
          <a href="dt13203470s.html#8">Beatissimo Padre [dedica a
          Benedetto XIV]</a>
        </li>
        <li>
          <a href="dt13203470s.html#16">L'autore a' chi legge</a>
        </li>
        <li>
          <a href="dt13203470s.html#18">Indice delle Tavole, e
          Figure che sono nell'Opera</a>
        </li>
        <li>
          <a href="dt13203470s.html#19">Indice de' capi</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dt13203470s.html#30">[Text]</a>
      <ul>
        <li>
          <a href="dt13203470s.html#29">Vera Effigies Sanctissimi
          Salvatoris ad Sancta Sanctorum de Urbe ›San Lorenzo in
          Palatio ad Sancta Sanctorum‹ ▣</a>
        </li>
        <li>
          <a href="dt13203470s.html#30">I. Del Sito dell'Antico
          Palagio Lateranense, di cui fù, ed è parte l'Oratorio di
          San Lorenzo, appellto ad Sancta Sanctorum</a>
        </li>
        <li>
          <a href="dt13203470s.html#35">II. Oppinioni diverse
          circa la prima Origine di questo Santuario, e quale
          sembri la più probabile: e delle più antiche memorie, che
          di esso à noi sieno rimaste</a>
        </li>
        <li>
          <a href="dt13203470s.html#39">III. Delle varie Sagre
          Funzioni, che i Sommi Pontefici celebravano nell'antico
          Oratorio di San Lorenzo: E di una Celeste Apparizione in
          esso seguita</a>
        </li>
        <li>
          <a href="dt13203470s.html#42">IV. Della forma antica di
          quell'Oratorio di San Lorenzo; e delle Reliquie
          collocatevi da San Leone P. P. III e da altri Sommi
          Pontefici</a>
        </li>
        <li>
          <a href="dt13203470s.html#48">V. Del Titolo di Sancta
          Sanctorum; quando, e per quale cagione derivato sia à
          questa Sagra Cappella</a>
        </li>
        <li>
          <a href="dt13203470s.html#52">VI. Memorie, e Adornamenti
          lasciati da Papa Innocenzo III. in questo antico
          Santuario; del Ristoramento fattovi da Onorio III. e del
          totale rinovamento di Papa Nicolò III.</a>
        </li>
        <li>
          <a href="dt13203470s.html#56">VII. Si descrivono le
          parti Interiori della Sagra Cappella di Sancta Sanctorum,
          ed i Mosaici, ed altre pitture fattevi effigiare da P. P.
          Niccolò III</a>
        </li>
        <li>
          <a href="dt13203470s.html#62">VIII. Si descrive la parte
          interiore del Santuario, cosistente nel Portico, ed
          Altare colla Tribuna, ove serbasi la Sagra Tavola
          coll'Immagine del Salvatore</a>
        </li>
        <li>
          <a href="dt13203470s.html#67">IX. Delle Reliquie
          collocate da Papa Niccolò III, in questo Altare, e sopra
          di esso: e di alcune, che al presente più non vi sono</a>
        </li>
        <li>
          <a href="dt13203470s.html#72">X. Del Titolo di Basilica
          dato all'Oratorio di San Lorenzo, e del suo Clero, e
          Canonici, che l'ufficiarono sino all'anno 1423</a>
        </li>
        <li>
          <a href="dt13203470s.html#76">XI. Degli XII. Ostiarj
          Nobili Romani, deputati alla Custodia dell'Immagine del
          Santissimo Salvatore: loro incombenze, et abiti: e come à
          questi surrogati furono i Guardiani della Compagnia de
          Raccomandati del Santissimo Salvatore ad Sancta
          Sanctorum</a>
        </li>
        <li>
          <a href="dt13203470s.html#82">XII. Soppressione del
          Priorato, e de' Canonicati della Basilica ad Sancta
          Sanctorum, ed unione de medesimi al Capitolo di San
          Giovanni Laterano, fatta da Papa Martino V. l'anno
          1423</a>
        </li>
        <li>
          <a href="dt13203470s.html#85">XIII. Differenze note frà
          gli Ostiarj rimasti nell'ufficio, ed i Fratelli della
          Compagnia surrogati à Defonti, intorno la Custodia, e le
          Oblazioni di Sancta Sanctorum, tolte da P.P. Martino V
          con altra sua Bolla, data l'anno 1424. colla quale
          conferma la surrogazione della Compagnia agli Ostiarj
          mancanti, confermata poscia da altri Pontefici.</a>
        </li>
        <li>
          <a href="dt13203470s.html#89">XIV. Della Communicazione,
          e corrispondenza dell'Eccellentissimo Senato, e Popolo
          Romano, co' Signori Guardiani della Compagnia di Sancta
          Sanctorum, in ordine alla Custodia dell'Immagine del
          Santissimo Salvatore</a>
        </li>
        <li>
          <a href="dt13203470s.html#93">XV. Alcuni Provvedimenti
          ed Ordini del Pontefice Leone X. à fine di togliere
          alcune occasioni di contese Giurisdizionali tra
          l'Illustrissimo Capitolo Lateranense, ed i Signori
          Guardiani e Fratelli della Compagnia del Santissimo
          Salvatore ad Sancta Sanctorum</a>
        </li>
        <li>
          <a href="dt13203470s.html#98">XVI. Della Sagra Tavola
          coll'Immagine dipinta del Santissimo Salvatore, appellata
          Acheropita, che in questo Santuario si venera: e delle
          varie opinioni di alcuni Scrittori intorno alla sua
          origine, e suo trasporto fatto in Roma</a>
        </li>
        <li>
          <a href="dt13203470s.html#102">XVII. Si tratta della
          Relazione di questa Sagra Immagine, scritta dal
          Maniacutio nel Secolo XII. e delle opposizioni fattegli
          dal Millino. Delle Immagini di Cristo Nostro Signore,
          formate, essendo egli ancora vivente, ed in que' primi
          secoli: e come alcune copie di esse ottennero
          equivocamente il titolo di Acheropite</a>
        </li>
        <li>
          <a href="dt13203470s.html#107">XVIII. Altra Istoria
          della venuta in Roma di questa Sagra Immagine del
          Santissimo Salvatore: ed opposizioni, che si fanno alla
          medesima</a>
        </li>
        <li>
          <a href="dt13203470s.html#116">XIX. Stato della Sagra
          Tavola ed Immagine del Santissimo Salvatore, come al
          presente ritrovasi</a>
        </li>
        <li>
          <a href="dt13203470s.html#121">XX. Delle Lastre
          d'Argento figurante, colle quali P. P. Innocenzo III.
          ricoprì la Sagra Tavola del Santissimo Salvatore: e di
          altri preziosi adornamenti della medesima</a>
          <ul>
            <li>
              <a href="dt13203470s.html#122">Degli adornamenti, e
              lastre d'argento figurate fatte da Pontefici
              Innocenzo e Niccolo III alla Sagra Immagine del
              Santissimo Salvatore detta Acheropita che si venera
              nell'Oratorio di San Lorenzo detto Ad Sancta
              Sanctorum di Roma ›San Lorenzo in Palatio ad Sancta
              Sanctorum‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dt13203470s.html#129">XXI. Della Cicatrice, che
          sotto l'occhio destro dell'Immagine del Salvatore
          apparisce: e si tratta della Compagnia degli Stizzi</a>
        </li>
        <li>
          <a href="dt13203470s.html#134">XXII. Del Costume antico
          di tenersi chiusa, ordinariamente, la Sagra Immagine del
          Salvatore, e di aprirsi alla vista, e divozione de Fedeli
          alcune volte frà l'anno: e della maestosa Funzione, che
          in tali congiunture si prattica.</a>
        </li>
        <li>
          <a href="dt13203470s.html#143">XXIII. Delle Processioni,
          e trasportamenti della Sagra Immagine del Salvatore à
          varie Basiliche, e Chiese di Roma, fatte da diversi
          antichi Sommi Pontefici, nè maggiori bisogni del
          Cristianesimo.</a>
        </li>
        <li>
          <a href="dt13203470s.html#151">XXIV. Ordine dell'annua
          Processione coll'Immagine del Santissimo Salvatore, per
          la Festa dell'Assunta della Beata Vergine, ne' secoli
          XIV. e XV. di nostra salute.</a>
        </li>
        <li>
          <a href="dt13203470s.html#159">XXV. Altre Processioni, e
          trasporti della Sagra Immagine Acherotipa ad altre
          Chiese, in questi ultimi secoli: e particolarmente nel
          Pontificato di Papa Clemente XI.</a>
        </li>
        <li>
          <a href="dt13203470s.html#170">XXVI. Come la Processione
          annua coll'Immagine del Santissimo Salvatore, nella
          vigilia dell'Assunta della Beata Vergine, fù proibita dal
          Pontefice S. Pio V. E come di essa in alcune Città, e
          Terre ve ne sia rimasta la memoria, col farsi somiglianti
          Processioni, con altre simili Immagini del Salvatore</a>
          <ul>
            <li>
              <a href="dt13203470s.html#172">Parte anteriore della
              Sagra Tavola Anagnina ▣</a>
            </li>
            <li>
              <a href="dt13203470s.html#172">Parte Posteriore
              della medesima ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dt13203470s.html#181">XXVII. Del Titolo di
          Salvatore , ed à quali Immagini di Cristo Nostro Signore
          propriamente si adatti: si descrivono quali fossero le
          sue fattezze corporali mentre conversava sopra la Terra:
          si dichiara, come contro di queste sue Immagini,
          principalmente, (e per qualche tempo,) indirizzata fosse
          la Persecuzione de gl' Iconoclasti: E perchè tali
          Immagini, né primi tempi, si usarono più frequentemente
          che quelle del Crocifisso.</a>
        </li>
        <li>
          <a href="dt13203470s.html#191">XXVIII. Si espongono
          diversi Misteriosi adornamenti delle Sagre Immagini del
          Salvatore usati sino da primi tempi, co' quali si rendono
          sommamente Venerabili, e Maestose</a>
          <ul>
            <li>
              <a href="dt13203470s.html#192">Musivi operis Tabulam
              vetustate pretiosam post plura Saecula Basilicam
              Liberianam magnificentium instaurante Benedicto XIV
              Pontifice vere Maximo ex intercolumnio Praesepi [...]
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dt13203470s.html#199">XXIX. Delle antiche
          Immagini del Santissimo Salvatore, che si venerano in
          Roma, sino da primi secoli della Chiesa: ed in specie di
          quelle, ch'effigiate, e dipinte si ritrovano ne Sagri
          Cimiterj de Santi Martiri</a>
          <ul>
            <li>
              <a href="dt13203470s.html#202">In Coem. Cyriacae Via
              Tiburtina ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dt13203470s.html#205">XXX. Del costume de'
          primi tempi di dedicarsi principalmente le Chiese al
          Salvatore, bench'erette in memoria, ed onore di qualche
          Santo: continuato poscia in Roma da Sommi Pontefici,
          coll'effig arvi (sic!) nel mezzo delle Tribune, ed Archi
          trionfali le Immagini del medesimo Salvatore. E di molte
          Chiese di Roma unicamente erette con questa sola
          denominazione del Salvatore.</a>
        </li>
        <li>
          <a href="dt13203470s.html#221">XXXI. Del culto verso il
          Santissimo Salvatore, e sue Immagini, assunto, e
          propagato, nel fondarsi Abbazie, e Monasteri sotto tale
          denominazione: o col prendersene le sue Immagini per
          Arma, o Stemma, o il Titolo da alcun Ordine Religioso</a>
        </li>
        <li>
          <a href="dt13203470s.html#228">XXXII. Di un'altra sorta
          d'Immagini del Santissimo Salvatore appellate comunemente
          LA PIETÀ: loro uso, e come quelle sono state assunte, per
          loro Stemma, Insegna, e sigillo da varie Confraternite, e
          Luoghi Pii</a>
        </li>
        <li>
          <a href="dt13203470s.html#231">XXXIII. Di alcune altre
          Immagini Maestose del Santissimo Salvatore, le quali si
          venerano, o pure si conservano in varj luoghi di Roma, e
          nel celebre Museo Kircheriano</a>
          <ul>
            <li>
              <a href="dt13203470s.html#238">Imago aerea Sancti
              Salvatoris reperta sub ruderibus prope Templum a
              Sancto Calisto ›San Callisto‹ aedificatum circa anno
              224 in Regione Transtiberina ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dt13203470s.html#243">XXXIV. Costume di
          effigiarsi le Immagini del Salvatore ne' Cimiterj de
          Fedeli dopo le Persecuzioni, e sopra le Urne, Sarcofagi,
          e Cenostasi de medesimi</a>
        </li>
        <li>
          <a href="dt13203470s.html#254">XXXV. Delle Immagini del
          Santissimo Salvatore effigiate anticamente negli
          Anelli</a>
        </li>
        <li>
          <a href="dt13203470s.html#258">XXXVI. Del costume di
          effigiarsi l'Immagine Maestosa del Salvatore nelle
          Medaglie, ò Monete antiche, continuato poscia fino à
          nostri tempi</a>
        </li>
        <li>
          <a href="dt13203470s.html#270">XXXVII. Di due altre
          Immagini Acheropite del Santissimo Salvatore, che in Roma
          si venerano</a>
        </li>
        <li>
          <a href="dt13203470s.html#275">XXXVIII. Come nella
          Cappella di Sancta Sanctorum serbansi l'Immagine della
          Beatissima Vergine, che al presente si venera nella
          Chiesa di ›Santa Maria del Popolo‹; sue Tradizioni: e si
          tratta di varie altre Immagini della medesima, credute
          essere opere di San Luca, esposte in diverse Chiese di
          Roma</a>
        </li>
        <li>
          <a href="dt13203470s.html#285">XXXIX. Come in questa
          Sagra Cappella di Sancta Sanctorum serbavansi altre Sagre
          Reliquie, che più non vi sono: ed in primo luogo del
          Santissimo Prepuzio di Nostro Signore Gesù Cristo.</a>
        </li>
        <li>
          <a href="dt13203470s.html#295">XL. Delle teste de Santi
          Apostoli Pietro, e Paolo, che si conservavano in questa
          Sagra Cappella di Sancta Sanctorum</a>
        </li>
        <li>
          <a href="dt13203470s.html#301">XLI. Il Pontefice Sisto
          V. per accrescere il Culto, e Venerazione della Sagra
          Cappella di Sancta Sanctorum, e dell'Immagine del
          Santissimo Salvatore, stabilisce di trasportarvi la
          ›Scala Santa‹, di cui si narrano le più antiche
          memorie</a>
        </li>
        <li>
          <a href="dt13203470s.html#307">XLII. Del trasporto della
          ›Scala Santa‹, e delle Tre Porte di marmo del Palagio di
          Pilato ›Palatium Pilati‹ avanti il Sancta Sanctorum fatto
          fare da Papa Sisto V.</a>
        </li>
        <li>
          <a href="dt13203470s.html#313">XLIII. Delle indulgenze,
          che si acquistano nella visita della Sagra Cappella,
          dell'Immagine del Santissimo Salvatore, e della Scala
          Santa: e del modo di conseguirle.</a>
        </li>
        <li>
          <a href="dt13203470s.html#317">XLIV. Della nobilissima
          Compagna de Raccomandati del Santissimo Salvatore ad
          Sancta Sanctorum: Sua Origine, stabilimento, primi
          Statuti, e suo fervore nella pratica della Santa
          Ospitalità.</a>
        </li>
        <li>
          <a href="dt13203470s.html#328">XLV. Come fu derogato
          allo Statuto del numero degli cento Fratelli, per la
          copia di coloro, che bramavano d'essere ascritti alla
          Compagnia, per godere del frutto delle grandi Opere Pie
          praticate dalla medesima: alla di cui somma vigilanza, e
          governo si appoggia l'amministrazione dei varj
          CollegJ.</a>
        </li>
        <li>
          <a href="dt13203470s.html#334">XLVI. Delle Aggregazioni
          à se fatte, dalla Compagnia, di altre Adunanze,
          Compagnie, Confraternite, e Luoghi Pii, e Religiosi
          comunicando loro le Indulgenze, Indulti, e Privilegj,
          ch'ella hà goduti, e gode fin ora, tanto per ragione
          dello Spedale, quanto per la Custodia della Cappella di
          Sancta Sanctorum, e della Sagra Immagine del
          Salvatore</a>
        </li>
        <li>
          <a href="dt13203470s.html#339">XLVII. Dell'Abito de
          Confratelli della Compagnia del Santissimo Salvatore ad
          Sancta Sanctorum nelle pubbliche Funzioni</a>
        </li>
        <li>
          <a href="dt13203470s.html#341">XLVIII. Come la Nobile
          Compagnia del Santissimo Salvatore ad Sancta Sanctorum,
          ne suoi Guardiani e Custodi, è un Immagine dell'Inclito
          Senato, e di Popolo Romano de Secoli Cristiani: Siccome
          questi è un maestoso Ritratto dell'antico Senato di Roma
          Gentile</a>
        </li>
        <li>
          <a href="dt13203470s.html#349">Catalogo de Signori
          Guardiani della Compagnia del Santissimo Salvatore ad
          Sancta Sanctorum</a>
        </li>
        <li>
          <a href="dt13203470s.html#366">Giunte</a>
        </li>
        <li>
          <a href="dt13203470s.html#369">Indice delle cose più
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
