<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg804r661715s.html#6">Il Mercurio Errante delle
      grandezze di Roma, tanto antiche che moderne</a>
      <ul>
        <li>
          <a href="dg804r661715s.html#8">Eminentissimo, e
          Reverendissimo Principe</a>
        </li>
        <li>
          <a href="dg804r661715s.html#12">Indice de' Palazzi,
          Ville, e Giardini di Roma</a>
        </li>
        <li>
          <a href="dg804r661715s.html#15">Indice delle cose più
          notabili</a>
        </li>
        <li>
          <a href="dg804r661715s.html#30">Libro I. Delle
          Grandezze di Roma</a>
        </li>
        <li>
          <a href="dg804r661715s.html#119">Libro II. Delle
          Ville, e Giardini, che sono dentro, e fuori del circuito
          di Roma</a>
        </li>
        <li>
          <a href="dg804r661715s.html#176">Libro III. Delle
          Antichità di Roma, che di presente si vedono&#160;</a>
        </li>
        <li>
          <a href="dg804r661715s.html#342">Catalogho d'alcune
          chiese più belle di Roma</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
