<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502280s.html#6">Grandezze della città di
      Roma</a>
      <ul>
        <li>
          <a href="dg4502280s.html#8">All'Illlustre sig. Tomasso
          Rosi</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502280s.html#10">[Text]</a>
      <ul>
        <li>
          <a href="dg4502280s.html#10">›Foro Romano‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#11">Marche Romain</a>
            </li>
            <li>
              <a href="dg4502280s.html#12">›Foro Romano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#13">Il ›Foro di Traiano‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#14">Le Marche de Traian</a>
            </li>
            <li>
              <a href="dg4502280s.html#15">Foro di Traiano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#16">Il ›Foro di Nerva‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#17">Le Marche de Nerva</a>
            </li>
            <li>
              <a href="dg4502280s.html#18">›Foro di Nerva‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#19">Le Terme Diocletiane ›Terme
          di Diocleziano‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#20">Les Thermes de
              Diocletian</a>
            </li>
            <li>
              <a href="dg4502280s.html#21">›Terme di Diocleziano‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#22">Le Terme Antoniane ›Terme
          di Caracalla‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#23">Les Thermes
              Antoniens</a>
            </li>
            <li>
              <a href="dg4502280s.html#24">›Terme di Caracalla‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#25">L'›Arco di Settimio
          Severo‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#26">L'Arc de Septimus
              Severus</a>
            </li>
            <li>
              <a href="dg4502280s.html#27">›Arco di Settimio
              Severo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#28">L'›Arco di Costantino‹
          Magno</a>
          <ul>
            <li>
              <a href="dg4502280s.html#29">L'Arc de Constantin le
              Grand</a>
            </li>
            <li>
              <a href="dg4502280s.html#30">›Arco di Costantino‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#31">›Porta Maggiore‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#32">La Porte Maieure</a>
            </li>
            <li>
              <a href="dg4502280s.html#33">›Porta Maggiore‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#34">L'›Arco di Tito‹, e
          Vespasiano</a>
          <ul>
            <li>
              <a href="dg4502280s.html#35">L'Arc de Titus et
              Vespasien</a>
            </li>
            <li>
              <a href="dg4502280s.html#36">›Arco di Tito‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#37">L'Arco di San Giorgio ›Arco
          degli Argentari‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#38">L'Arc de Saint
              George</a>
            </li>
            <li>
              <a href="dg4502280s.html#39">Arcus Septimii Severi
              ›Arco degli Argentari‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#40">L'›Arco di Portogallo‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#41">L'Arc de Portugal</a>
            </li>
            <li>
              <a href="dg4502280s.html#42">Arcus Domitiani
              Imperatoris ›Arco di Portogallo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#43">L'Anfiteatro detto
          ›Colosseo‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#44">L'Anphitheatre dir
              Colisee</a>
            </li>
            <li>
              <a href="dg4502280s.html#45">›Colosseo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#46">L'›Anfiteatro di Statilio
          Tauro‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#47">L'Amphitheatre de
              Statilius Taurus</a>
            </li>
            <li>
              <a href="dg4502280s.html#48">›Anfiteatro di Statilio
              Tauro‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#49">Il ›Teatro di Marcello‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#50">Le Theatre de
              Marcellus</a>
            </li>
            <li>
              <a href="dg4502280s.html#51">Il ›Teatro di Marcello‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#52">Il Tempio della Rotonda
          ›Pantheon‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#53">Le Temple diet la
              Rotonde</a>
            </li>
            <li>
              <a href="dg4502280s.html#54">Templum Marci Agrippae
              ›Pantheon‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#55">Il Tempio in Pace ›Foro
          della Pace‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#56">Le temple de la
              Paix</a>
            </li>
            <li>
              <a href="dg4502280s.html#57">Templum Pacis ›Foro
              della Pace‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#58">Il ›Tempio della
          Concordia‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#59">Le Temple de la
              Concorde</a>
            </li>
            <li>
              <a href="dg4502280s.html#59">›Tempio della
              Concordia‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#61">Del Tempio di Giano ›Arco
          di Giano‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#62">Du Temple de Ianus</a>
            </li>
            <li>
              <a href="dg4502280s.html#63">Templum Iani ›Arco di
              Giano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#64">La Basilica d'Antonino Pio
          ›Tempio di Antonino e Faustina‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#65">L'Hostel ou Maison
              Publique d'Antonin Pie</a>
            </li>
            <li>
              <a href="dg4502280s.html#66">Porticus Antonini Pii
              Imperatoris ›Tempio di Antonino e Faustina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#67">Il Tempio della Fortuna
          ›Tempio di Portunus‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#68">Le Temple de la
              Fortune</a>
            </li>
            <li>
              <a href="dg4502280s.html#69">Templum Fortunae
              Virilis ›Tempio di Portunus‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#70">Il ›Tempio di Antonino e
          Faustina‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#71">Le Temple d'Antonin, et
              Faustine</a>
            </li>
            <li>
              <a href="dg4502280s.html#72">›Tempio di Antonino e
              Faustina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#73">Il ›Tempio di Giove
          Statore‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#74">Le Temple de Iupiter
              surnomme Stator</a>
            </li>
            <li>
              <a href="dg4502280s.html#75">›Tempio di Giove
              Statore‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#76">Il Tempio del Sole ›Tempio
          di Serapide‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#77">Le Temple du Soleil</a>
            </li>
            <li>
              <a href="dg4502280s.html#78">Templum Solis › ›Tempio
              di Serapide‹▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#79">›Castel Sant'Angelo‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#80">Le Chasteau Sant
              Ange</a>
            </li>
            <li>
              <a href="dg4502280s.html#81">Moles Hadriani
              Imperatoris ›Castel Sant'Angelo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#82">Il sepolchro di Metella
          ›Tomba di Cecilia Metella‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#83">Le Sepulchre de
              Metella</a>
            </li>
            <li>
              <a href="dg4502280s.html#84">Sepulchrum Metellorum
              ›Tomba di Cecilia Metella‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#85">La sepoltura di Cestio
          ›Piramide di Caio Cestio‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#86">Le Sepulchre de
              Cestin</a>
            </li>
            <li>
              <a href="dg4502280s.html#87">Sepulchrum Caii Cestii
              ›Piramide di Caio Cestio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#88">›Campidoglio‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#89">Le Capitole</a>
            </li>
            <li>
              <a href="dg4502280s.html#90">›Campidoglio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#91">Il Cerchio di Caracalla
          ›Circo di Massenzio‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#92">Le Cirque de
              Caracalla</a>
            </li>
            <li>
              <a href="dg4502280s.html#93">Circus Antonini
              Caracallae ›Circo di Massenzio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#94">Li ›Trofei di Mario‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#95">Les Trophees de
              Marius</a>
            </li>
            <li>
              <a href="dg4502280s.html#96">Marii Trophaea ›Trofei
              di Mario‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#97">Palazzo Maggiore
          ›Palatino‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#98">Le Palais Maieur</a>
            </li>
            <li>
              <a href="dg4502280s.html#99">Palatium Maius
              ›Palatino‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#100">L'›Isola Tiberina‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#101">L'Isle du Tybre</a>
            </li>
            <li>
              <a href="dg4502280s.html#102">›Isola Tiberina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#103">La ›Colonna Traiana‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#104">La Colonne de
              Traian</a>
            </li>
            <li>
              <a href="dg4502280s.html#105">›Colonna Traiana‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#106">La Colonna d'Antonino
          ›Colonna Antonina‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#107">La Colonne
              d'Antonin</a>
            </li>
            <li>
              <a href="dg4502280s.html#108">›Colonna Antonina‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#109">Guglia di San Pietro
          ›Obelisco Vaticano‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#110">Esguille de Sainct
              Pierre</a>
            </li>
            <li>
              <a href="dg4502280s.html#111">Obeliscus Sancti Petri
              ›Obelisco Vaticano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#112">Guglia di San Giovanni
          Laterano ›Obelisco Lateranense‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#113">Esguille, ou Obelisque
              de Sainct Iehan Lateran</a>
            </li>
            <li>
              <a href="dg4502280s.html#114">Obeliscus Sancti
              Ioannis Lateranensis ›Obelisco Lateranense‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#115">Guglia di Santa Maria
          Maggiore ›Obelisco Esquilino‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#116">Esguille de Saincte
              Marie Maieure</a>
            </li>
            <li>
              <a href="dg4502280s.html#117">Obeliscum Sanctae
              Mariae Maioris ›Obelisco Esquilino‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#118">Guglia della Madonna del
          Popolo ›Obelisco Flaminio‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#119">Esguilla de Nostre
              Dame du Peuple</a>
            </li>
            <li>
              <a href="dg4502280s.html#120">Obeliscus Sanctae
              Mariae de Populo ›Obelisco Flaminio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#121">Guglia di San Mauto
          ›Obelisco di Ramsses II (1)‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#122">Esguille de Sainct
              Maute</a>
            </li>
            <li>
              <a href="dg4502280s.html#123">Obeliscus Sancti Mauti
              ›Obelisco di Ramsses II (1)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#124">Guglia nel Giardino de
          Medici ›Obelisco di Boboli‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#125">Esguille, ou
              Obelisque, qui est dans le Iardin de Medicis</a>
            </li>
            <li>
              <a href="dg4502280s.html#126">Obeliscus in Horto
              Mediceo ›Obelisco di Boboli‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#127">Guglia nel Giardino de'
          Mattei ›Obelisco di Ramsses II (2)‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#128">Esguilla du Iardin des
              Matthei</a>
            </li>
            <li>
              <a href="dg4502280s.html#129">Obeliscus in Viridario
              Mattheiorum ›Obelisco di Ramsses II (2)‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#130">Colonna di Santa Maggiore
          ›Fontana di Piazza Santa Maria Maggiore‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#131">Colonne de Sancte
              Marie Maieure</a>
            </li>
            <li>
              <a href="dg4502280s.html#132">Columnae Sanctae
              Mariae Maioris ›Fontana di Piazza Santa Maria
              Maggiore‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#133">La facciata di ›San Pietro
          in Vaticano: Facciata‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#134">Le Frontispice de
              Sainct Pierre</a>
            </li>
            <li>
              <a href="dg4502280s.html#135">›San Pietro in
              Vaticano: Facciata‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#136">›Scala Santa‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#137">L'Eschelle Saincte</a>
            </li>
            <li>
              <a href="dg4502280s.html#138">›Scala Santa‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#139">Palazzo Pontificio di San
          Pietro ›Palazzo Apostolico Vaticano‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#140">Le Palais du Pape a
              Sainct Pierre</a>
            </li>
            <li>
              <a href="dg4502280s.html#141">Aedes Vaticana
              Romanorum Pontificum ›Palazzo Apostolico Vaticano‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#142">Palazzo Pontificio di
          Monte Cavallo ›Palazzo del Quirinale‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#143">Le Palais du Pape sur
              le Mont Quirinal dict Monte Cavallo ›Palazzo del
              Quirinale‹</a>
            </li>
            <li>
              <a href="dg4502280s.html#144">Aedes Pontificia in
              Quirinale Monte ›Palazzo del Quirinale‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#145">›Collegio Romano‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#146">Le College Romain</a>
            </li>
            <li>
              <a href="dg4502280s.html#147">Collegium Romanum
              ›Collegio Romano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#148">La Sapientia ›Palazzo
          della Sapienza‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#149">La Sapience ou
              l'Universite</a>
            </li>
            <li>
              <a href="dg4502280s.html#150">Gymnasium Sapientiae
              ›Palazzo della Sapienza‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#151">›Palazzo della
          Cancelleria‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#152">Le Palais dela
              Chancellerie</a>
            </li>
            <li>
              <a href="dg4502280s.html#153">Aedes Cancellariae
              Apostolicae ›Palazzo della Cancelleria‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#154">›Palazzo Farnese‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#155">Le Palais de
              Farnese</a>
            </li>
            <li>
              <a href="dg4502280s.html#156">Facies aedis
              Farnesianae ›Palazzo Farnese‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#157">›Palazzo Borghese‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#158">Palais de Borghese</a>
            </li>
            <li>
              <a href="dg4502280s.html#159">Aedes Burghesiana
              ›Palazzo Borghese‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#160">Giardino di Borghese
          ›Villa Borghese‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#161">Le Jardin de
              Borghese</a>
            </li>
            <li>
              <a href="dg4502280s.html#162">›Villa Borghese‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#163">Giardino di Fiorenza
          ›Villa Medici‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#164">Le Jardin de
              Florence</a>
            </li>
            <li>
              <a href="dg4502280s.html#165">Viridarium Mediceorum
              ›Villa Medici‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#166">Giardino de Mattei ›Villa
          Celimontana‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#167">Jardin des Matthei</a>
            </li>
            <li>
              <a href="dg4502280s.html#168">Viridarium Mattheiorum
              ›Villa Celimontana‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#169">›Fontana di San Pietro in
          Montorio (scomparsa)‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#170">Fontaine de Saint
              Pierre Montorio</a>
            </li>
            <li>
              <a href="dg4502280s.html#171">Fons Sancti Petri
              Montisaurei ›Fontana di San Pietro in Montorio
              (scomparsa)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#172">Fontana di Terme ›Fontana
          del Mosè‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#173">Fontaine de Terme</a>
            </li>
            <li>
              <a href="dg4502280s.html#174">Fons Thermarum
              ›Fontana del Mosè‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#175">Fontana di San Pietro in
          Vaticano ›Piazza San Pietro: Fontana (N)‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#176">Fontaine de Saint
              Pierre au Vatican</a>
            </li>
            <li>
              <a href="dg4502280s.html#177">Fons Sancti Petri in
              Vaticano ›Piazza San Pietro: Fontana (N)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502280s.html#178">›Piazza Navona‹</a>
          <ul>
            <li>
              <a href="dg4502280s.html#179">Le Place Navone</a>
            </li>
            <li>
              <a href="dg4502280s.html#180">Platea Agonalis
              ›Piazza Navona‹ ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
