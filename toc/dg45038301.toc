<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg45038301s.html#6">La ville de Rome ou
      description abregée de cette superbe villa</a>
      <ul>
        <li>
          <a href="dg45038301s.html#8">Table des titres</a>
        </li>
        <li>
          <a href="dg45038301s.html#10">Roma qualis erat anno
          1777 ◉</a>
          <ul>
            <li>
              <a href="dg45038301s.html#10">Mons Vaticanus
              ›Vaticano‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Basilica Sancti Petri
              ›San Pietro in Vaticano‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">›Porta Angelica‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">›Porta Castello‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Castrum Sancti Angeli
              ›Castel Sant'Angelo‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Porta Fabricae ›Porta
              Fabbrica‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">›Porta Cavalleggeri‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Porta Sancti
              Pancratii ›Porta San Pancrazio‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Porta Portese ›Porta
              Portuensis‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Mons Testaceus
              ›Testaccio (Monte)‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Porta Sancti Pauli
              ›Porta San Paolo‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Porta Sancti
              Sebastiani ›Porta San Sebastiano‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">›Porta Latina‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Porta Sancti Ioannis
              ›Porta San Giovanni‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Porta Major ›Porta
              Maggiore‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Porta Sancti
              Laurentii ›Porta Tiburtina‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">›Porta Pia‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">›Porta Salaria‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">›Porta Pinciana‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Porta Populi ›Porta
              del Popolo‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Trinitas Montis
              ›Trinità dei Monti‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Termini ›Piazza dei
              Cinquecento‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Monte Cavallo
              ›Quirinale‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Piazza Narona ›Piazza
              Navona‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Basilica Sanctae
              Mariae Majoris ›Santa Maria Maggiore‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Basilica Sancti
              Ioanis ›San Giovanni in Laterano‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Sancta Crux ›Santa
              Croce in Gerusalemme‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Therame Caracallae
              ›Terme di Caracalla‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">›Colosseo‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">Campo Vaccino ›Foro
              Romano‹ ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">›Villa Borghese‹
              ◉</a>
            </li>
            <li>
              <a href="dg45038301s.html#10">›Villa Albani‹ ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038301s.html#12">Roma in regiones XIV
          distributa anno 1777 ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Villa Albani‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Villa Borghese‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Campo Vaccino ›Foro
          Romano‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Colosseo‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Therame Caracallae ›Terme
          di Caracalla‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Sancta Crux ›Santa Croce
          in Gerusalemme‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Basilica Sancti Ioanis
          ›San Giovanni in Laterano‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Basilica Sanctae Mariae
          Majoris ›Santa Maria Maggiore‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Piazza Navona‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Monte Cavallo ›Quirinale‹
          ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Mons Vaticanus ›Vaticano‹
          ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Basilica Sancti Petri
          ›San Pietro in Vaticano‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Porta Angelica‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Porta Castelli ›Porta
          Castello‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Castrum Sancti Angeli
          ›Castel Sant'Angelo‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Porta Fabbrica‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Porta Cavalleggeri‹
          ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Porta Sancti Pancratii
          ›Porta San Pancrazio‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Porta Portese ›Porta
          Portuensis‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Ripa major ›Porto di Ripa
          Grande‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Mons Testaceus ›Testaccio
          (monte)‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Porta Sancti Pauli ›Porta
          San Paolo‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Porta Sancti Sebastiani
          ›Porta San Sebastiano‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Porta Latina‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Porta San Giovanni‹
          ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Porta Major ›Porta
          Maggiore‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Porta Sancti Laurentii
          ›Porta Tiburtina‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Porta Pia‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Porta Salaria‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Porta Pinciana‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Porta Populi ›Porta del
          Popolo‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Trinitas Monti ›Trinità
          dei Monti‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Sanctus Esusebius
          ›Sant'Eusebio‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Sanctus Petrus in
          Vinculis ›San Pietro in Vincoli‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Sanctus Gregorius ›San
          Gregorio Magno‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Santo Stefano Rotondo‹
          ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Sanctus Sabas ›San Saba‹
          ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Santa Sabina‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Santa Maria in
          Trastevere‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Palazzo Corsini‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Rotunda ›Pantheon‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">›Santi Domenico e Sisto‹
          ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Termini ›Piazza dei
          Cinquecento‹ ◉</a>
        </li>
        <li>
          <a href="dg45038301s.html#12">Fons Trivii ›Fontana di
          Trevi‹ ◉</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg45038301s.html#14">La ville de Rome [text]</a>
      <ul>
        <li>
          <a href="dg45038301s.html#17">I. Quartier des Monts. Où
          sont la Basilique de Saint Jaun de Latran ›San Giovanni
          in Laterano‹, celle de Sainte Marie Majeure ›Santa Maria
          Maggiore‹, et la place de Termini ›Piazza dei
          Cinquecento‹.</a>
          <ul>
            <li>
              <a href="dg45038301s.html#18">Montes. Regio I.
              Romana qualis erat anni 1777 ◉</a>
              <ul>
                <li>
                  <a href="dg45038301s.html#18">Monte Cavallo
                  ›Quirinale‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">Le quattro
                  fontane ›Quadrivio delle Quattro Fontane‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">Via Porate Piae
                  ›Via XX Settembre‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Porta Pia‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">Piazza di Termini
                  ›Piazza dei Cinquecento‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">Certosa ›Terme di
                  Diocleziano‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Castra
                  Praetoria‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Palazzo Albani
                  Del Drago‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Villa Negroni‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Sant'Antonio
                  Abate‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Sant'Eusebio‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">Porta San Lorenzo
                  ›Porta Tiburtina‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">Thermae Titi
                  ›Terme di Tito‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Santa Bibiana‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Porta Maggiore‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Santa Croce in
                  Gerusalemme‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Anfiteatro
                  Castrense‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Porta San
                  Giovanni‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›San Giovanni in
                  Laterano‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Santo Stefano
                  Rotondo‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›San Clemente‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Colosseo‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#18">›Villa Altieri‹
                  ◉</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45038301s.html#20">I. Partie Orientale
              du I. Quartier, où sont la Basilique de Saint Jean de
              Latran ›San Giovanni in Laterano‹, celle de Sainte
              Croix en Jerusalem ›Santa Croce in Gerusalemme‹, et
              l'Eglise de Saint Eusebe ›Sant'Eusebio‹.</a>
            </li>
            <li>
              <a href="dg45038301s.html#42">II. Partie
              Septentrionale du I Quartier, où sont la Basilique de
              Sainte Marie Majeure ›Santa Maria Maggiore‹, la villa
              Negroni ›Villa Negroni‹, et la place de Termini
              ›Piazza dei Cinquecento‹.</a>
            </li>
            <li>
              <a href="dg45038301s.html#59">III. Partie
              Occidentale du I Quartier, où sont l'Eglise de Saint
              Andrè ›Sant'Andrea al Quirinale‹, le palais de la
              Consulte ›Palazzo della Consulta‹ , et celui de
              Rospigliosi ›Palazzo Pallavicini Rospigliosi‹.</a>
            </li>
            <li>
              <a href="dg45038301s.html#69">IV. Partie
              Meridionale du I Quartier, où sont la colonne Trajane
              ›Colonna Traiana‹, l'Eglise de Saint Pierre aus liens
              ›San Pietro in Vincoli‹, et le ruines du temple de la
              Paix ›Foro della Pace‹.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038301s.html#91">II. Quartier de Trevi. Où
          sont le Palais du Pape è Monte Cavallo ›Palazzo del
          Quirinale‹, la Place des Apostoli ›Piazza dei Santi
          Apostoli‹, et le Palais du Prince Barberin ›Palazzo
          Barberini‹.</a>
          <ul>
            <li>
              <a href="dg45038301s.html#91">I. Partie Occidentale
              du II Quartier, où sont le Palais du Pape à Monte
              Cavalllo ›Palazzo del Quirinale‹, la Place des
              Apostoli ›Piazza dei Santi Apostoli‹, et la Fontaine
              de trevi ›Fontana di Trevi‹</a>
            </li>
            <li>
              <a href="dg45038301s.html#92">Trivii Regio II
              Romana qualis erat anno 1775 ◉</a>
              <ul>
                <li>
                  <a href="dg45038301s.html#92">Palazzo
                  Pontificio ›Palazzo del Quirinale‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">Monte Cavallo
                  ›Quirinale‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">›Quadrivio delle
                  Quattro Fontane‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">›Santa Susanna‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">›Porta Pia‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">›San Silvestro al
                  Quirinale‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">›Colonna Traiana‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">›Fontana di
                  Trevi‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">›Piazza
                  Barberini‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">›Porta Salaria‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">San Claudio
                  ›Santi Andrea e Claudio dei Borgognoni‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">Strada del Corso
                  ›Via del Corso‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">Strada Felice
                  ›Via Felice‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#92">›Villa Mandosi‹
                  ◉</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45038301s.html#123">II. Partie Orientale
              du II Quartier, où sont le Palais Barberin ›palazzo
              Barberini‹, l'Eglise de Sainte Susanne ›Santa
              Susanna‹, et celle de Saint Nicolas de Tolentin ›San
              Nicola da Tolentino‹.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038301s.html#148">III. Quartier de
          Colonne. Où sont la Place Colonne ›Piazza Colonna‹, le
          palais de Monte Citorio ›Palazzo Montecitorio‹, et la
          ›Villa Ludovisi‹.</a>
          <ul>
            <li>
              <a href="dg45038301s.html#147">Columnae Regio III
              Romana qualis erat anno 1777 ◉</a>
              <ul>
                <li>
                  <a href="dg45038301s.html#147">›Sant'Andrea
                  delle Fratte‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#147">Strada Fratina
                  ›Via Frattina‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#147">›San Lorenzo in
                  Lucina‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#147">›Piazza di
                  Montecitorio‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#147">›Piazza Colonna‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#147">›Sant'Isidoro‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#147">›Villa Ludovisi‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#147">›Villa Altieri‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#147">›Villa Verospi
                  Vitelleschi‹ ◉</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45038301s.html#148">I. Partie orientale
              du III Quartier, où sont la ›Villa Ludovisi‹, le
              convent des Capucins ›Santa Maria della Concezione‹,
              et l'Eglise de Saint Isidore ›Sant'Isidoro‹.</a>
            </li>
            <li>
              <a href="dg45038301s.html#153">II. Parte
              occidentale du III Quartier, où sont la place Colonne
              ›Piazza Colonna‹, le Palais de Monte-Citorio ›Palazzo
              Montecitorio‹, et le College de la Propagande
              ›Collegio Urbano di Propaganda Fide‹.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038301s.html#171">IV. Quartier du Champ de
          Mars ›Campo Marzio‹, où sont la place d'Espagne ›Piazza
          di Spagna‹, celle du Peuple ›Piazza del Popolo‹, et la
          Palais Borghese ›Palazzo Borghese‹ .</a>
          <ul>
            <li>
              <a href="dg45038301s.html#172">Campi Martii Regio
              IV Romana qualis erat in anno 1777 ◉</a>
              <ul>
                <li>
                  <a href="dg45038301s.html#172">›Trinità dei
                  Monti‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">›Piazza di
                  Spagna‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">›Villa Medici‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">›Villa Ludovisi‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">›Porta Pinciana‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">›Santa Maria del
                  Popolo‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">Porta Populi
                  ›Porta del Popolo‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">›Piazza del
                  Popolo‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">›Porto di
                  Ripetta‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">Strada Fratina
                  ›Via Frattina‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">›Palazzo
                  Ruspoli‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">›Muro Torto‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#172">›Sant'Antonio
                  dei Portoghesi‹ ◉</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg45038301s.html#174">I. Partie orientale
              du IV Quartier, où sont la place d'Espagne ›Piazza di
              Spagna‹, celle du Peuple ›Piazza del Popolo‹, et la
              Trinité du Mont ›Trinità dei Monti‹.</a>
            </li>
            <li>
              <a href="dg45038301s.html#197">II. Parte
              Occidentale du Iv Quartier, où sont l'Eglise de Saint
              Charles ›Santi Ambrogio e Carlo al Corso‹, le palais
              Borghese, et le port de Ripette ›Porto di
              Ripetta‹.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038301s.html#207">V. Quartier du Pont. Où
          sont l'Eglise de Notre Dame de la Paix ›Santa Maria della
          Pace‹, le College des Allemams ›Palazzo di
          Sant'Apollinare / Collegio Germanico Ungarico‹, et
          l'Eglise de Saint Jean des Florentins ›San Giovanni dei
          Fiorentini‹.</a>
          <ul>
            <li>
              <a href="dg45038301s.html#208">Pontis Regio V
              Romana qualis erat anno 1777 ◉</a>
              <ul>
                <li>
                  <a href="dg45038301s.html#208">›Ponte
                  Sant'Angelo‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#208">›San Giovanni
                  dei Fiorentini‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#208">Palazzo
                  Gabrielli ›Palazzo Taverna‹ ◉</a>
                </li>
                <li>
                  <a href=
                  "dg45038301s.html#208">›Sant'Apollinare‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#208">›San Salvatore
                  in Lauro‹ ◉</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038301s.html#219">VI. Quartier de Parione.
          Où sont la place Navone ›Piazza Navona‹, le palais de la
          Chancelerie ›Palazzo della Cancelleria‹, et la ›Chiesa
          Nuova‹.</a>
          <ul>
            <li>
              <a href="dg45038301s.html#220">Parionis Regio VI
              Romana qualis erat anno 1777 ◉</a>
              <ul>
                <li>
                  <a href="dg45038301s.html#220">›Piazza Navone‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#220">›Campo de'
                  Fiori‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#220">Cancellaria
                  ›Palazzo della Cancelleria‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#220">›Chiesa Nuova‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#220">›Sant'Agnese in
                  Agone‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#220">San Giacomo
                  ›Nostra Signora del Sacro Cuore‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#220">San Nicolaus
                  ›San Nicola dei Lorenesi‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#220">›Santo Stefano
                  de Piscina‹ ◉</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038301s.html#236">VII. Quartier de la
          Regola, Où sont le Palais Farnese ›Palazzo Farnese‹, le
          Mont de la Pietè ›Palazzo del Monte di Pietà‹, et le
          Palais Spada ›Palazzo Spada‹.</a>
          <ul>
            <li>
              <a href="dg45038301s.html#235">Arenula seu Regola
              Regio VII Romana qualis erat anno 1777 ◉</a>
              <ul>
                <li>
                  <a href="dg45038301s.html#235">›Santa Lucia del
                  Gonfalone‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#235">Area Farnesia
                  ›Piazza Farnese‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#235">Pons Sixtu
                  ›Ponte Sisto‹ ◉</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg45038301s.html#258">VIII. Quartier de
          Sainte-Esutache, Où sont le College de la Sapience
          ›Palazzo della Sapienza‹, l'Eglise de Saint Andrè de la
          Vallé ›Sant'Andrea della Valle‹, et celle de Saint
          Charles aux Catinari ›San Carlo ai Catinari‹.</a>
          <ul>
            <li>
              <a href="dg45038301s.html#257">Sancti Eustachii
              Regio VIII Romana qualis erat anno 1777 ◉</a>
              <ul>
                <li>
                  <a href="dg45038301s.html#257">›Sant'Andrea
                  della Valle‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#257">Sapienza
                  ›Sant'Ivo‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#257">Sanctus
                  Ludovicus ›San Luigi dei Francesi‹ ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#257">›Sant'Agostino‹
                  ◉</a>
                </li>
                <li>
                  <a href="dg45038301s.html#257">›San Carlo ai
                  Catinari‹ ◉</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
