<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghmal828344102s.html#8">Felsina pittrice; parta
      quarta</a>
      <ul>
        <li>
          <a href="ghmal828344102s.html#10">Alcune etichette
          della Felsina Pittrice</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#12">Al Signore Ulisse
          Guidi, uno dei Tipografi-Editori della nuova impressione
          della Felsina Pittrice</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#16">Varianze che
          trovansi ne' diversi esemplari della prima edizione</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#21">Guido Reni&#160;</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#87">Giovanni Andrea
          Donducci detto il Mastelletta e di Agostino
          Tassi&#160;</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#95">Leonello Spada e di
          Pietro Desani e Giovannino da Capugnano&#160;</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#113">Giovanni Battista
          Viola e di Baldassar Galanino&#160;</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#120">Giovanluigi Valesio
          e di Giovanni Battista Coriolano, Giovanni Petrelli e
          Oliviero Gatti ed altri suoi discepoli&#160;</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#131">Girolamo Curti,
          detto il Dentone e di Giovanni Paderna, Andrea Schizzi ed
          altri suoi discepoli&#160;</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#147">Alessandro
          Tiarini&#160;</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#173">Giacomo
          Cavedone</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#181">Francesco
          Albani</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#231">Carlo Cignani</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#245">Lorenzo
          Garbieri</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#255">Domenico Zampieri
          detto il Domenichino</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#282">Francesco Gessi e
          Giovanni Giacomo Sementi, e di Giovanni Battista
          Ruggieri, Ercole de' Maria ed altri del detto Gessi
          desicepoli</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#293">Francesco Barbieri
          detto il Guercin da Cento e di Paolo Antonio Fratello,
          Ercole Cognato, Benedetto e Cesare nipoti dello stesso ed
          altri suoi discepoli</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#385">Antonio Michele
          Colonna e Agostino Metelli e di Giacomo Alborsi,
          Fulgenzio Mondina ed altri del sudetto Agostino
          discepoli</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#416">Simone Cantarini
          detto il Pesarese e di Flaminio Torre ed altri del detto
          Cantarini discepoli</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#429">Giovanni Andrea
          Sirani e di Elisabetta sua figliuola</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#464">Pittori de' quali
          si tratta in questo secondo tomo</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#466">Osservazioni sopra
          il libro della Felsina Pittrice</a>
          <ul>
            <li>
              <a href="ghmal828344102s.html#468">Al cortese
              lettore</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#472">Lettera
              prima</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#474">Lettere
              seconda</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#479">Lettera
              terza</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#484">Lettera
              quarta</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#486">Lettera
              quinta</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#492">Lettera
              sesta</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#495">Lettere
              settima</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghmal828344102s.html#498">Lettere familiari
          scritte ad un amico in difesa del Conte Carlo Cesare
          Malvasia</a>
          <ul>
            <li>
              <a href="ghmal828344102s.html#500">Illustrissimo
              Signore</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#501">L'autore a chi
              legge</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#502">Lettera
              prima</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#505">Lettera
              seconda</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#510">Lettera
              terza</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#520">Lettera
              quarta</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#523">Lettera
              quinta</a>
            </li>
            <li>
              <a href="ghmal828344102s.html#528">Lettere
              sesta</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghmal828344102s.html#532">Dialogo di Giovanni
          Pietro Cavazzoni Zanotti in deifesa di Guido Reni</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#540">Indice delle
          chiese, entro quali si trovano Pitture nominate
          nell'opera</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#549">Indice di tutti li
          Nomi, Cognomi, o Famiglie, delle quali, per posseder
          Pitture, o per altro, si fa menzione nell'Opera</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#565">Indice di tutti li
          Pittori, de'quali o espressamente si tratta, o
          incidentemente si fa menzione nell'Opera</a>
        </li>
        <li>
          <a href="ghmal828344102s.html#579">Indice della
          maggior parte della Pittura, si private, che pubbliche, e
          delle cose più notabili, che si sontengono nell'Opera</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
