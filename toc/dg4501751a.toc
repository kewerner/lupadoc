<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501751as.html#3">Le cose maravigliose dell'alma
      citta di Roma</a>
      <ul>
        <li>
          <a href="dg4501751as.html#6">Le sette chiese
          principali</a>
        </li>
        <li>
          <a href="dg4501751as.html#48">Tavola delle chiese di
          Roma</a>
        </li>
        <li>
          <a href="dg4501751as.html#50">Le stationi, che sono ne
          le Chiese di Roma, si per la Quadragesima, come per tutto
          l'anno</a>
        </li>
        <li>
          <a href="dg4501751as.html#58">Trattato over modo
          d'acqvistar l'indulgenze a le Stationi</a>
        </li>
        <li>
          <a href="dg4501751as.html#66">La guida romana per li
          forastieri che uengono per uedere le antichità di Roma, a
          una per una in bellissima forma &amp; breuitá</a>
        </li>
        <li>
          <a href="dg4501751as.html#76">Summi pontifices</a>
        </li>
        <li>
          <a href="dg4501751as.html#100">Reges et imperatores
          romani</a>
        </li>
        <li>
          <a href="dg4501751as.html#106">Li re del regno di
          Napoli, et di Sicilia, li quali cominciorno a regnare
          l'anno di nostra salute 1425</a>
        </li>
        <li>
          <a href="dg4501751as.html#110">L'antichità di Roma</a>
          <ul>
            <li>
              <a href="dg4501751as.html#112">A li lettori</a>
            </li>
            <li>
              <a href="dg4501751as.html#116">Delle antichià della
              cittá di Roma. Libro primo</a>
            </li>
            <li>
              <a href="dg4501751as.html#164">Tavola de le
              antichità de la città di Roma</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501751as.html#164">Lettera pastorale di
          Monsignor illvstrissimo, et reverendissimo Card.
          Borromeo</a>
          <ul>
            <li>
              <a href="dg4501751as.html#176">Poste di Italia.
              Poste da Roma a Bologna.</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
