<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="camic211642s.html#6">Esequie del divino
      Michelagnolo Buonarroti celebrate in Firenze dall’Accademia
      de Pittori, Scultori, &amp; Architettori nella Chiesa di S.
      Lorenzo il di 28. giugno MDLXIIII</a>
      <ul>
        <li>
          <a href="camic211642s.html#8">Al molto magnifico M.
          Franc. Buonaventura</a>
        </li>
        <li>
          <a href="camic211642s.html#9">[Esequie del divino
          Michelagnolo Buonarroti celebrate in Firenze
          dall’Accademia de Pittori, Scultori, &amp; Architettori
          nella Chiesa di S. Lorenzo il di 28. giugno MDLXIIII
          ]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
