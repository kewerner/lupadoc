<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="wasto42602440s.html#9">Racconto delle sontuose
      esequie fatte alla serenissima Isabella Reina di Spagna</a>
      <ul>
        <li>
          <a href="wasto42602440s.html#11">All'illustrissimo et
          eccelentissimo signore il signor Don Antonio Sancio</a>
        </li>
        <li>
          <a href="wasto42602440s.html#14">Racconto</a>
        </li>
        <li>
          <a href="wasto42602440s.html#25">Descrittione
          dell'apparato</a>
        </li>
        <li>
          <a href="wasto42602440s.html#142">In funere
          augustissimae Hispaniarum Reginae Isabellae Borboniae
          oratio</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
