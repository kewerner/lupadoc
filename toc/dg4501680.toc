<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501680s.html#6">Le Cose Maravigliose Dell’alma
      Città di Roma</a>
      <ul>
        <li>
          <a href="dg4501680s.html#8">Della edificazione di Roma
          [...]</a>
        </li>
        <li>
          <a href="dg4501680s.html#12">Le sette chiese</a>
          <ul>
            <li>
              <a href="dg4501680s.html#12">San Giovanni in
              Laterano</a>
            </li>
            <li>
              <a href="dg4501680s.html#16">San Pietro in
              Vaticano</a>
            </li>
            <li>
              <a href="dg4501680s.html#18">San Paolo</a>
            </li>
            <li>
              <a href="dg4501680s.html#19">Santa Maria
              Maggiore</a>
            </li>
            <li>
              <a href="dg4501680s.html#21">San Lorenzo fuora delle
              mura</a>
            </li>
            <li>
              <a href="dg4501680s.html#22">San Sebastiano</a>
            </li>
            <li>
              <a href="dg4501680s.html#22">Santa Croce in
              Gierusalem</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501680s.html#23">Nell'Isola</a>
        </li>
        <li>
          <a href="dg4501680s.html#24">In Trastevere</a>
        </li>
        <li>
          <a href="dg4501680s.html#26">Nel Borgo</a>
        </li>
        <li>
          <a href="dg4501680s.html#28">Dalla Porta Flaminia duori
          del Popolo fino alle radici del Campidoglio&#160;</a>
        </li>
        <li>
          <a href="dg4501680s.html#38">Del Campidoglio a man
          sinistra verso li monti</a>
        </li>
        <li>
          <a href="dg4501680s.html#45">Dal Campidoglio a man
          dritta verso li monti</a>
        </li>
        <li>
          <a href="dg4501680s.html#50">Le Stationi. Indulgnetie,
          et gratie spirituali, che sono nelle chiese di Roma, si
          per la quadrigesima,come per tutto l'anno&#160;</a>
          <ul>
            <li>
              <a href="dg4501680s.html#50">Nel mese di Gennaro</a>
            </li>
            <li>
              <a href="dg4501680s.html#51">Nel mese di
              Febbraio</a>
            </li>
            <li>
              <a href="dg4501680s.html#56">Nel mese d'Aprile</a>
            </li>
            <li>
              <a href="dg4501680s.html#56">Nel mese di Maggio</a>
            </li>
            <li>
              <a href="dg4501680s.html#58">Nel mese di Giugno</a>
            </li>
            <li>
              <a href="dg4501680s.html#59">Nel mese di Luglio</a>
            </li>
            <li>
              <a href="dg4501680s.html#60">Nel mese di Agosto</a>
            </li>
            <li>
              <a href="dg4501680s.html#61">Nel mese di
              Settembre</a>
            </li>
            <li>
              <a href="dg4501680s.html#62">Nel mese di
              Novembre</a>
            </li>
            <li>
              <a href="dg4501680s.html#63">Le Stationi
              dell'Advento</a>
            </li>
            <li>
              <a href="dg4501680s.html#66">Queste sono speciali
              Indulgentie, et Stationi in diverse chiese di Roma,
              concesse per diversi sommi Pontefici, oltra le
              soprascritte</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501680s.html#68">Trattato over modo
          d'acquistar l'induglentie alle stationi</a>
        </li>
        <li>
          <a href="dg4501680s.html#73">La guida romana per tutti i
          Forastieri che vengono per vedere le antichità di Roma, a
          una per una, in bellissima forma, et brevita</a>
        </li>
        <li>
          <a href="dg4501680s.html#73">Del Borgo la prima
          giornata</a>
        </li>
        <li>
          <a href="dg4501680s.html#76">Giornata seconda</a>
        </li>
        <li>
          <a href="dg4501680s.html#82">Giornata terza</a>
        </li>
        <li>
          <a href="dg4501680s.html#84">[Tavole]</a>
          <ul>
            <li>
              <a href="dg4501680s.html#84">Tavola delle chiese</a>
            </li>
            <li>
              <a href="dg4501680s.html#86">Summi Pontifices</a>
            </li>
            <li>
              <a href="dg4501680s.html#108">Reges, et Imperatores
              Romanos</a>
            </li>
            <li>
              <a href="dg4501680s.html#112">Li Re di Francia</a>
            </li>
            <li>
              <a href="dg4501680s.html#113">Li Re deö Regno di
              Napoli, et di Sicilia, liquali [sic!] cominciono a
              regnare l'anno di nostra salute</a>
            </li>
            <li>
              <a href="dg4501680s.html#114">Li Dogi di
              Vinegia&#160;</a>
            </li>
            <li>
              <a href="dg4501680s.html#116">Li Duchi di Milano</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501680s.html#118">L’Antichità Di Roma</a>
      <ul>
        <li>
          <a href="dg4501680s.html#120">Alli lettori</a>
        </li>
        <li>
          <a href="dg4501680s.html#121">Tavola</a>
        </li>
        <li>
          <a href="dg4501680s.html#123">Delle antichità della
          città di Roma&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
