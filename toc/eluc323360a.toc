<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="eluc323360as.html#6">Guida sacra alle chiese di
      Lucca per tutti gli anni del Signore, nella quale si
      contengono le feste stabili e mobili di tutto l’anno ...</a>
      <ul>
        <li>
          <a href="eluc323360as.html#8">Santissimo Pastore
          Paolino</a>
        </li>
        <li>
          <a href="eluc323360as.html#14">Gli Stampatori a chi
          legge</a>
        </li>
        <li>
          <a href="eluc323360as.html#16">Regola per trovare la
          lettera domenicale</a>
        </li>
        <li>
          <a href="eluc323360as.html#24">Esposizioni</a>
        </li>
        <li>
          <a href="eluc323360as.html#26">Gennajo</a>
        </li>
        <li>
          <a href="eluc323360as.html#56">Febbrajo</a>
        </li>
        <li>
          <a href="eluc323360as.html#82">Marzo</a>
        </li>
        <li>
          <a href="eluc323360as.html#116">Aprile</a>
        </li>
        <li>
          <a href="eluc323360as.html#145">Maggio</a>
        </li>
        <li>
          <a href="eluc323360as.html#178">Giugno</a>
        </li>
        <li>
          <a href="eluc323360as.html#208">Luglio</a>
        </li>
        <li>
          <a href="eluc323360as.html#246">Agosto</a>
        </li>
        <li>
          <a href="eluc323360as.html#280">Settembre</a>
        </li>
        <li>
          <a href="eluc323360as.html#317">Ottobre</a>
        </li>
        <li>
          <a href="eluc323360as.html#347">Novembre</a>
        </li>
        <li>
          <a href="eluc323360as.html#375">Dicembre</a>
        </li>
        <li>
          <a href="eluc323360as.html#408">Catalogo delle Chiese,
          delle quali si fa menzione nella Guida Sacra</a>
        </li>
        <li>
          <a href="eluc323360as.html#413">Catalogo De' Corpi
          Santi, Reliquie [...]</a>
        </li>
        <li>
          <a href="eluc323360as.html#423">Cronologia De' Vescovi
          ed Arcivescovi di Lucca</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
