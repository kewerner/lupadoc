<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghmal82844020s.html#6">Lettere Del Consigliere
      Gian Lodovico Bianconi scritte in nome Del Segretario
      Dell’Accademia Di S. Luca Di Roma Al Segretario
      Dell’Accademia Clementina Di Bologna Sopra il libro del
      Canonico Luigi Crespi Bolognese, intitolato Felsina Pittrice,
      Vite de’ Pittori Bolognesi, Tomo terzo, impresso in Roma nel
      1769.</a>
      <ul>
        <li>
          <a href="ghmal82844020s.html#8">Alla bolognese illustre
          Accademia Clementina delle belle arti</a>
        </li>
        <li>
          <a href="ghmal82844020s.html#12">Prefazione</a>
        </li>
        <li>
          <a href="ghmal82844020s.html#36">Lettera I.</a>
        </li>
        <li>
          <a href="ghmal82844020s.html#41">Lettera II.</a>
        </li>
        <li>
          <a href="ghmal82844020s.html#64">Lettera III.</a>
        </li>
        <li>
          <a href="ghmal82844020s.html#71">Lettera IV.</a>
        </li>
        <li>
          <a href="ghmal82844020s.html#76">Lettera V.</a>
        </li>
        <li>
          <a href="ghmal82844020s.html#82">Lettera VI.</a>
        </li>
        <li>
          <a href="ghmal82844020s.html#89">Lettera VII.</a>
        </li>
        <li>
          <a href="ghmal82844020s.html#95">Lettera VIII.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
