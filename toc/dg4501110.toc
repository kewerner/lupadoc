<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501110s.html#6">Indulgentie Ecclesiarum vrbis
      Rome</a>
      <ul>
        <li>
          <a href="dg4501110s.html#8">Roma civitas sancta caput
          mundi&#160;</a>
        </li>
        <li>
          <a href="dg4501110s.html#36">oratio devotissima de
          Sudarlo sacratissimi Vultus usii nostri Jesu rpi del
          Veronica</a>
        </li>
        <li>
          <a href="dg4501110s.html#38">Incipiunt indulgentie
          septem ecclesiarum principalium urbis Rome</a>
        </li>
        <li>
          <a href="dg4501110s.html#112">Stationes in
          quadragesima</a>
        </li>
        <li>
          <a href="dg4501110s.html#114">Stationes post pascha</a>
        </li>
        <li>
          <a href="dg4501110s.html#115">Stationes in Adventu</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
