<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dm6504600a1s.html#6">Le scienze e le arti sotto
      il pontificato di Pio IX</a>
      <ul>
        <li>
          <a href="dm6504600a1s.html#8">Ai Lettori</a>
        </li>
        <li>
          <a href="dm6504600a1s.html#10">Scala Papale</a>
        </li>
        <li>
          <a href="dm6504600a1s.html#14">Cortile di San
          Damaso</a>
        </li>
        <li>
          <a href="dm6504600a1s.html#18">Biblioteca Vaticana</a>
        </li>
        <li>
          <a href="dm6504600a1s.html#28">Nuova fabbrica pei
          beneficiati al Vaticano</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
