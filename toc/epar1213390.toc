<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="epar1213390s.html#6">Guida, ed esatta notizia a’
      forastieri delle più eccellenti pitture, che sono in molte
      chiese della città di Parma</a>
      <ul>
        <li>
          <a href="epar1213390s.html#8">Illustrissimo, e
          Reverendissimo Signore</a>
        </li>
        <li>
          <a href="epar1213390s.html#16">Indice delle chiese,
          nelle quali si ammirano le più insigne Pitture</a>
        </li>
        <li>
          <a href="epar1213390s.html#18">Sant'Antonio
          Abate&#160;</a>
        </li>
        <li>
          <a href=
          "epar1213390s.html#23">Sant'Alessandro&#160;</a>
        </li>
        <li>
          <a href="epar1213390s.html#27">Sant'Andrea</a>
        </li>
        <li>
          <a href="epar1213390s.html#28">Sant'Anna</a>
        </li>
        <li>
          <a href="epar1213390s.html#29">P. P.
          dell'Annunziata&#160;</a>
        </li>
        <li>
          <a href="epar1213390s.html#30">Il Battistero</a>
        </li>
        <li>
          <a href="epar1213390s.html#31">P. P. del Carmine</a>
        </li>
        <li>
          <a href="epar1213390s.html#33">San Cosimo</a>
        </li>
        <li>
          <a href="epar1213390s.html#34">M. M. Cappuccine</a>
        </li>
        <li>
          <a href="epar1213390s.html#35">P. P. Cappuccini</a>
        </li>
        <li>
          <a href="epar1213390s.html#40">Duomo</a>
        </li>
        <li>
          <a href="epar1213390s.html#52">P. P. Eremitani</a>
        </li>
        <li>
          <a href="epar1213390s.html#53">P. P. di San
          Francesco</a>
        </li>
        <li>
          <a href="epar1213390s.html#56">San Giovanni
          Evangelista</a>
        </li>
        <li>
          <a href="epar1213390s.html#66">San Michele</a>
        </li>
        <li>
          <a href="epar1213390s.html#68">Madonna della
          Steccata</a>
        </li>
        <li>
          <a href="epar1213390s.html#75">San Marcellino</a>
        </li>
        <li>
          <a href="epar1213390s.html#76">Madonna degl'Angioli</a>
        </li>
        <li>
          <a href="epar1213390s.html#76">Madonna della Scala</a>
        </li>
        <li>
          <a href="epar1213390s.html#78">Santa Maria Maddalena,
          chiesa parocchiale</a>
        </li>
        <li>
          <a href="epar1213390s.html#80">Madonna delle Grazie al
          Ponte</a>
        </li>
        <li>
          <a href="epar1213390s.html#80">P. P. del Quartiere</a>
        </li>
        <li>
          <a href="epar1213390s.html#81">Oratorio della
          Santissima Trinità</a>
        </li>
        <li>
          <a href="epar1213390s.html#82">San Paolo</a>
        </li>
        <li>
          <a href="epar1213390s.html#84">San Pietro Martire</a>
        </li>
        <li>
          <a href="epar1213390s.html#85">San Quintino</a>
        </li>
        <li>
          <a href="epar1213390s.html#86">San Rocco</a>
        </li>
        <li>
          <a href="epar1213390s.html#87">San Sepolcro</a>
        </li>
        <li>
          <a href="epar1213390s.html#91">Santo Stefano</a>
        </li>
        <li>
          <a href="epar1213390s.html#93">Santa Teresa</a>
        </li>
        <li>
          <a href="epar1213390s.html#93">San Tommaso</a>
        </li>
        <li>
          <a href="epar1213390s.html#94">Sant'Ulderico</a>
        </li>
        <li>
          <a href="epar1213390s.html#95">San Vidale</a>
        </li>
        <li>
          <a href="epar1213390s.html#95">Oratorio della Morte</a>
        </li>
        <li>
          <a href="epar1213390s.html#96">Tutti i Santi</a>
        </li>
        <li>
          <a href="epar1213390s.html#99">Indice de' Pittori</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
