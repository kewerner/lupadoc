<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503752s.html#8">Roma antica, e moderna</a>
      <ul>
        <li>
          <a href="dg4503752s.html#10">Giornata prima</a>
          <ul>
            <li>
              <a href="dg4503752s.html#10">Da ›Ponte Sant'Angelo‹
              a ›San Pietro in Vaticano‹</a>
              <ul>
                <li>
                  <a href="dg4503752s.html#18">[›San Pietro in
                  Vaticano: Mosaico della Navicella‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503752s.html#33">Giornata seconda</a>
          <ul>
            <li>
              <a href="dg4503752s.html#33">Dalla ›Porta Santo
              Spirito‹ al ›Trastevere‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503752s.html#45">Giornata Terza</a>
          <ul>
            <li>
              <a href="dg4503752s.html#45">Da Strada Giulia ›Via
              Giulia‹ all'Isola di San Bartolomeo ›Isola
              Tiberina‹</a>
              <ul>
                <li>
                  <a href="dg4503752s.html#57">[›Isola Tiberina‹]
                  ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503752s.html#58">Giornata Quarta</a>
          <ul>
            <li>
              <a href="dg4503752s.html#58">Da ›San Lorenzo in
              Damaso‹ al Monte Aventino</a>
              <ul>
                <li>
                  <a href="dg4503752s.html#68">[›San Paolo fuori
                  le Mura‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#72">[›Anfiteatro
                  Castrense‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503752s.html#74">Giornata Quinta</a>
          <ul>
            <li>
              <a href="dg4503752s.html#74">Dalla Piazza di ›Monte
              Giordano‹, per li Monti ›Celio‹, e ›Palatino‹</a>
              <ul>
                <li>
                  <a href="dg4503752s.html#81">›San Sebastiano‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#85">[›San Giovanni in
                  Laterano‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#89">[›Scala Santa‹]
                  ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503752s.html#96">Giornata Sesta</a>
          <ul>
            <li>
              <a href="dg4503752s.html#96">Da ›San Salvatore in
              Lauro‹ per ›Campidoglio‹, e per le ›Carinae‹</a>
              <ul>
                <li>
                  <a href="dg4503752s.html#98">[›Fontana dei
                  Fiumi‹]</a>
                </li>
                <li>
                  <a href="dg4503752s.html#103">Capitolium
                  ›Campidoglio‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#106">[›Foro Romano‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#111">[›Arco di
                  Costantino‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#112">[›Colosseo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#117">[›Foro di Nerva‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#118">›Colonna Traiana‹
                  ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503752s.html#121">Giornata Settima</a>
          <ul>
            <li>
              <a href="dg4503752s.html#121">Dalla ›Piazza di
              Sant'Agostino‹ per il Monte ›Viminale‹, e
              ›Quirinale‹</a>
              <ul>
                <li>
                  <a href="dg4503752s.html#130">[›San Paolo fuori
                  le Mura‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#134">[›Santa Maria
                  Maggiore‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#144">[›Pantheon‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503752s.html#146">Giornata Ottava</a>
          <ul>
            <li>
              <a href="dg4503752s.html#146">Dalla strada dell'Orso
              ›Via dell'Orso‹ a Monte Cavallo ›Quirinale‹, e alle
              Terme Diocleziane ›Terme di Diocleziano‹</a>
              <ul>
                <li>
                  <a href="dg4503752s.html#149">›Fontana di Trevi‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#150">[›Palazzo del
                  Quirinale‹ ▣ ›Fontana di Monte Cavallo‹ ▣]</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503752s.html#158">Giornata Nona</a>
          <ul>
            <li>
              <a href="dg4503752s.html#158">Dal ›Palazzo Borghese‹
              a ›Porta del Popolo‹, a ›Piazza di Spagna‹</a>
              <ul>
                <li>
                  <a href="dg4503752s.html#160">[›Fontana di
                  Piazza del Popolo / Obelisco Flaminio‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503752s.html#169">Giornata Decima</a>
          <ul>
            <li>
              <a href="dg4503752s.html#169">Dal Monte Citorio
              ›Piazza di Montecitorio‹ alla ›Porta Pia‹, e al Monte
              ›Pincio‹</a>
              <ul>
                <li>
                  <a href="dg4503752s.html#170">Columna Antonini
                  ›Colonna di Antonino Pio‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503752s.html#176">›Trinità dei
                  Monti‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503752s.html#178">Cronologia di tutti li
          sommi Pontefici</a>
        </li>
        <li>
          <a href="dg4503752s.html#186">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
