<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cabar2730310s.html#6">Sereniss.mo Principi
      Leopoldo Medices Leonis X admirandae virtutis imagines, ab
      Hetruriae legatione ad pontificatum, à Raphaele Urbinate ad
      vivum, et ad miraculum expressas, in Aulaeis Vaticanis,
      textili monocromate elaboratas, tibi dico... meis typis
      adumbratas, ut quae veteres gentis tuae sunt imagines, et
      monumenta gloriosissimi Pontificis, divinitus ad rerum culmen
      euecti</a>
      <ul>
        <li>
          <a href="cabar2730310s.html#8">Iannes Medices
          Cardinalis missus ab Innocentio VIII Hetruriae Legatus in
          patria redit mortuo Laurentio Patre</a>
        </li>
        <li>
          <a href="cabar2730310s.html#8">Civium salutantium
          occursu Florentiam advenit magna familiae, ac dignitatis
          suae existimatione</a>
        </li>
        <li>
          <a href="cabar2730310s.html#10">Cum Petrus, ac Iulianus
          Medices Florentia aufugissent, Ioannes Hetruriae Legatus
          exutam purpuram, cineream cucullam, ad Serafici
          Sacerdotis effigiem, sibi indutam inter tumultum
          discurrentis turbe elabitur incolumis</a>
        </li>
        <li>
          <a href="cabar2730310s.html#10">Eiectis Mediceis
          Florentini armati ipsorum aedes invadunt, aulaeorum
          supellectilem antiqui operis diripiunt</a>
        </li>
        <li>
          <a href="cabar2730310s.html#12">Statuas siogna aerea,
          tabulas pcitas, bibliothecam graecam, et latinam, quae
          annis 60. Cosmus ac Laurentius congessere, praedantur</a>
        </li>
        <li>
          <a href="cabar2730310s.html#12">Ingenti clade ad Roncum
          Ravennae amnem in Pontificios, Hispanosque a Gallis
          inlatam Medici Legatus ab Equitum manibus adventu
          Federici Gonzagae Bozoli eripitur, cui accepta fides
          prontius est deditus</a>
        </li>
        <li>
          <a href="cabar2730310s.html#14">Captivus inter
          Obstrepentium Hostium insultationes, securitatem,
          libertatemque sibi comparat</a>
        </li>
        <li>
          <a href="cabar2730310s.html#14">Conspiratores in
          Medices suppliciis muletandi</a>
        </li>
        <li>
          <a href="cabar2730310s.html#16">Pratensium multitudo
          cum effugere obseratis portis nequiret interficitur supra
          inermium hominum millia caesa sunt&#160;</a>
        </li>
        <li>
          <a href="cabar2730310s.html#16">Prato opido capito,
          refugientes matronas, virgines, Senesque Medices Legatus
          Iulianusque frater servant incolumes</a>
        </li>
        <li>
          <a href="cabar2730310s.html#18">Effusi sunt cunti cives
          ad salutandum Ioannem Legatum, et ab his in paternas
          aedes est deductus &#160;XVIII post annum, ex quo fuerat
          expulsis</a>
        </li>
        <li>
          <a href="cabar2730310s.html#18">Vexillifer Ridolfus in
          vestibulum curiae descendit assensu, et clamore populi ad
          ordinandum R. P. Statum</a>
        </li>
        <li>
          <a href="cabar2730310s.html#20">Iulio II vita suncto
          Ioannes Card. Medices patriae compas Romam ad Comitia
          contendit post aversam prosperamque eius peregrinationem
          tanto populi studio exceptus, ut salutantium omine mox
          futuris Pontifex habentur</a>
        </li>
        <li>
          <a href="cabar2730310s.html#20">Ioannes Medices
          Pontifex Maximus creatus Leo X appellatus se
          complectendum, et adorandum exibet Cardinalibus</a>
        </li>
        <li>
          <a href="cabar2730310s.html#20">Tybris flubius.
          Basilica Vaticana.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
