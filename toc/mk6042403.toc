<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="mk6042403s.html#8">Storia dell'arte col mezzo dei
      monumenti dalla sua decadenza nel IV secolo fino al suo
      risorgimento nel XVI. Volume III. Scultura - Testo</a>
    </li>
    <li>
      <a href="mk6042403s.html#10">Scultura</a>
      <ul>
        <li>
          <a href="mk6042403s.html#10">Introduzione</a>
          <ul>
            <li>
              <a href="mk6042403s.html#12">Della Scultura presso
              gli Egiziani</a>
            </li>
            <li>
              <a href="mk6042403s.html#18">Della Scultura presso
              gli Etruschi</a>
            </li>
            <li>
              <a href="mk6042403s.html#23">Della Scultura presso
              i Greci</a>
            </li>
            <li>
              <a href="mk6042403s.html#40">Della Scultura presso
              i Romani</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042403s.html#50">I. Decadenza della
          Scultura dal IV fino al XIII secolo&#160;</a>
          <ul>
            <li>
              <a href="mk6042403s.html#50">[Tavola I.] Scelta di
              alcuni dei più bei monumenti della Scultura
              antica</a>
            </li>
            <li>
              <a href="mk6042403s.html#64">Tavola II. Confronto
              de' bassirilievi degli archi di trionfo di Tito, di
              Settimio Severo e di Costantino. I, II e IV secolo
              ›Arco di Tito‹ ›Arco di Settimio Severo‹ ›Arco di
              Costantino‹</a>
            </li>
            <li>
              <a href="mk6042403s.html#66">Tavola III. Statue di
              Costantino e de' suoi figli, bassirilievi, busti ed
              altre opere del medesimo tempo. IV secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#70">Tavola IV. Urne
              sepolcrali e sarcofagi, trovati a Roma, nelle
              catacombe di sant'Urbano ›Catacombe di Pretestato‹ e
              di Torre-Pignattara ›Catacombe dei Santi Marcellino e
              Pietro‹. IV secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#72">Tavola V. Bassirilievi
              ed ornamenti di diverse urne sepolcrali trovate nelle
              catacombe. Primi secoli del cristianesimo</a>
            </li>
            <li>
              <a href="mk6042403s.html#74">Tavola VI. Altre opere
              di bassorlievo eseguite sulle urne delle catacombe.
              Primi secoli del cristianesimo</a>
            </li>
            <li>
              <a href="mk6042403s.html#77">Tavola VII. Figure ed
              iscrizioni incise in incavo sulle pietre sepolcrali
              delle catacombe</a>
            </li>
            <li>
              <a href="mk6042403s.html#79">Tavola VIII. Unione di
              differenti soggetti scolpiti nelle catacombe:
              iscrizioni sepolcrali</a>
            </li>
            <li>
              <a href="mk6042403s.html#82">Tavola IX. Forziere di
              argento, vaso dei profumi ed altri utensili della
              toletta di una dama romana. IV o V secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#86">Tavola X.
              Bassilirilievi del piedistallo dell'obelisco rialzato
              da Teodosio nell'Ippodromo di Costantinopoli:
              medaglie della medesima epoca. IV secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#89">Tavola XI. Piedistallo
              e parte dei bassirilievi della colonna di Teodosio a
              Costantinopoli. IV e V secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#94">Tavola XII.
              Bassirilievi copiati da dittici greci e latini, ed
              altri lavori in avorio. Dal IV secolo all'XI</a>
            </li>
            <li>
              <a href="mk6042403s.html#102">Tavole XIII-XX. Porta
              principale di ›San Paolo fuori le Mura‹ di Roma, con
              figure incise in incavo nel bronzo e damaschinate in
              argento: lavori eseguiti a Costantinopoli. XI
              secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#104">Tavola XXI.
              Bassirilievi e sculture in marmo: cesellature in
              bronzo ed in argento. XII secolo&#160;</a>
            </li>
            <li>
              <a href="mk6042403s.html#107">Tavola XXII.
              Bassirilievi eseguiti in legno sulla porta della
              chiesa di ›Santa Sabina‹ in Roma. XIII secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#108">Tavola XXIII.
              Tabernacolo di San Pietro [sic!] fuori delle mura
              ›San Paolo fuori le Mura: Ciborio‹ di Roma. XIII
              secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#109">Tavola XXIV. Mausoleo
              del cardinal Gonsalvi ›Tomba del cardinale Consalvo
              Rodriguez‹ in ›Santa Maria Maggiore‹, in Roma. XIII
              secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#110">Tavola XXV. Globo
              celeste cufico-arabo, del museo Borgia, a Velletri.
              XIII secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#111">Tavola XXVI. Riunione
              di diverse opere di Scultura eseguite in Italia. Dal
              V al XIII secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#120">Tavola XXVI A, B, e
              C. Palliotto dell'altar maggiore della basilica di
              sant'Ambrogio in Milano. IX secolo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042403s.html#124">II. Principio del
          risorgimento della Scultura nel XIII secolo&#160;</a>
          <ul>
            <li>
              <a href="mk6042403s.html#124">Tavola XXVII. Statue,
              bassirilievi e medaglie dei secoli XII, XIII e
              XIV</a>
            </li>
            <li>
              <a href="mk6042403s.html#126">Tavola XXVIII.
              Mausoleo dei Savelli ›Sepolcro di Luca Savelli‹ nella
              chiesa d'Ara-Coeli ›Santa Maria in Aracoeli‹ a Roma.
              XIII e XIV secolo</a>
            </li>
            <li>
              <a href="mk6042403s.html#127">Tavola XXIX. Opere di
              Scultura, eseguite fuori d'Italia, dal principio
              della decadenza fino al secolo XIV</a>
            </li>
            <li>
              <a href="mk6042403s.html#130">Tavola XXX. Mausoleo
              del re Roberto ed altri monumenti della Casa d'Anjou.
              Secolo XIII e XIV</a>
            </li>
            <li>
              <a href="mk6042403s.html#135">Tavola XXXI.
              Bassorilievo della tomba della regina Sancia
              d'Aragona, nella chiesa di santa Maria della Croce a
              Napoli. XIV secolo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042403s.html#138">III. Risorgimento totale
          della Scultura</a>
          <ul>
            <li>
              <a href="mk6042403s.html#138">Epoca prima. Dalla
              metà del XIII secolo al principio del XIV</a>
              <ul>
                <li>
                  <a href="mk6042403s.html#142">Tavola XXXIV.
                  Mausoleo o arca di san Pietro martire, opera di
                  Giovanni Balduccio, nella chiesa di
                  Sant'Eustorgio a Milano. XIV secolo</a>
                </li>
                <li>
                  <a href="mk6042403s.html#145">Tavola XXXV.
                  Statue, bassirilievi ed altre sculture delle
                  diverse scuole d'Italia. XIV secolo</a>
                </li>
                <li>
                  <a href="mk6042403s.html#148">Tavola XXXVI.
                  Tabernacolo dell'altar maggiore di ›San Giovanni
                  in Laterano‹ a Roma. XIV secolo</a>
                </li>
                <li>
                  <a href="mk6042403s.html#148">Tavola XXXVII.
                  Cesellature e busti di san Pietro e san Paolo
                  nella chiesa di ›San Giovanni in Laterano‹ a
                  Roma. XIV secolo</a>
                </li>
                <li>
                  <a href="mk6042403s.html#149">Tavola XXXVIII.
                  Statue, bassilrilievi ed altre sculture di
                  diverse scuole, in Italia e fuori. XV secolo</a>
                </li>
                <li>
                  <a href="mk6042403s.html#152">Tavola XXXIX.
                  Mausoleo del cardinal Filippo d'Alençon, nella
                  chiesa di ›Santa Maria in Trastevere‹, a Roma</a>
                </li>
                <li>
                  <a href="mk6042403s.html#154">Tavola XL.
                  Mappamondo inciso sul rame: specie di lavoro alla
                  damaschina. XV secolo</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042403s.html#155">Epoca seconda.
              Progressi del Risorgimento della Scultura alla metà
              del XV secolo</a>
              <ul>
                <li>
                  <a href="mk6042403s.html#155">Tavola XLI. Porta
                  principale del battistero di Firenze, lavoro in
                  bronzo di Lorenzo Ghiberti. XV secolo</a>
                </li>
                <li>
                  <a href="mk6042403s.html#164">Tavola XLII.
                  Dettagli dei bassirilievi della porta del
                  battistero di Firenze: miracolo di san Zanobi
                  altro bassorilievo di Lorenzo Ghiberti. XV
                  secolo</a>
                </li>
                <li>
                  <a href="mk6042403s.html#172">Tavola XLIII.
                  Incisioni in incavo eseguite sopra una cassettina
                  di cristallo da Valerio Belli. XVI secolo</a>
                </li>
                <li>
                  <a href="mk6042403s.html#175">Tavola XLIV.
                  Medaglioni diversi scolpiti in legno od in
                  bronzo. XV e XVI secolo</a>
                </li>
                <li>
                  <a href="mk6042403s.html#177">Tavola XLV.
                  Mausoleo della famiglia Bonsi, a san Gregorio sul
                  monte Celio ›San Gregorio Magno‹ a Roma. XVI
                  secolo</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042403s.html#179">Epoca terza. Totale
              risorgimento della Scultura nel XVI secolo</a>
              <ul>
                <li>
                  <a href="mk6042403s.html#179">Tavola XLVI.
                  Schizzo del mausoleo progettato da Michelangelo
                  Bonarotti per la sepoltura del pontefice Giulio
                  II, nella chiesa di ›San Pietro in Vincoli‹, a
                  Roma. XVI secolo</a>
                </li>
                <li>
                  <a href="mk6042403s.html#183">Tavola XLVII.
                  Altre opere di Scultura di Michelangelo
                  Buonarotti. XVI secolo</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042403s.html#186">Riassunto generale della
          storia della Scultura per mezzo delle medaglie e delle
          pietre incise</a>
          <ul>
            <li>
              <a href="mk6042403s.html#186">Tavola XLVIII
              &#160;</a>
            </li>
            <li>
              <a href="mk6042403s.html#187">Medaglie</a>
            </li>
            <li>
              <a href="mk6042403s.html#193">Pietre incise</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="mk6042403s.html#200">Tavola delle principali
          divisioni della storia della Scultura</a>
        </li>
        <li>
          <a href="mk6042403s.html#202">Tavola delle materie
          della Storia dell'arte per mezzo dei monumenti - Sezione
          di Scultura</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
