<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghmal4313910s.html#6">Delle leggi del bello
      applicate alla pittura ed architettura&#160;</a>
      <ul>
        <li>
          <a href="ghmal4313910s.html#8">Indice</a>
        </li>
        <li>
          <a href="ghmal4313910s.html#10">Introduzione</a>
        </li>
        <li>
          <a href="ghmal4313910s.html#20">Parte prima</a>
          <ul>
            <li>
              <a href="ghmal4313910s.html#20">I. Idea generale
              del bello</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#34">II. Del bello
              intelettuale</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#36">III. Del bello
              morale</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#43">IV. Del bello
              sensibile</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#51">V. Parallelo de'
              varj generi di bello</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#58">VI. De' varj nomi o
              delle differenti modificazioni del bello</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#69">VII. Del bello
              nelle arti</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghmal4313910s.html#77">Parte seconda</a>
          <ul>
            <li>
              <a href="ghmal4313910s.html#77">I. Idea generale
              del bello nella pittura</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#79">II.
              Dell'invenzione</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#100">III.
              Dell'ordinanza o disposizione</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#110">IV.
              Dell'espressione</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#124">V. Del disegno</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#142">VI. Del
              chiaroscuro</a>
            </li>
            <li>
              <a href="ghmal4313910s.html#152">VII. Del
              colorito</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghmal4313910s.html#169">Parte terza</a>
        </li>
        <li>
          <a href="ghmal4313910s.html#169">I. Idea generale del
          bello nell'architettura</a>
        </li>
        <li>
          <a href="ghmal4313910s.html#178">II.
          Dell'invenzione</a>
        </li>
        <li>
          <a href="ghmal4313910s.html#220">III. Della
          disposizione</a>
        </li>
        <li>
          <a href="ghmal4313910s.html#237">VI. [sic]
          Dell'espressione</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
