<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="elor8245301s.html#6">La Santa Casa di Nazareth e
      la Città di Loreto descritte storicamente e disegnate da
      Gaetano Ferri</a>
      <ul>
        <li>
          <a href="elor8245301s.html#8">Breve Relazione storica
          della Santa Casa di Nazareth e della Città di Loreto</a>
        </li>
        <li>
          <a href="elor8245301s.html#28">Indice delle Materie
          contenute in questa Relazione storica</a>
        </li>
        <li>
          <a href="elor8245301s.html#30">Indice delle incise
          Località scenografiche di Loreto e Pagina in cui vengono
          citate</a>
        </li>
        <li>
          <a href="elor8245301s.html#32">[Tavole]</a>
          <ul>
            <li>
              <a href="elor8245301s.html#32">II. Parte della
              Città di Loreto e Monte di Ancona veduta dalla Cima
              di Monte-Reale ▣</a>
            </li>
            <li>
              <a href="elor8245301s.html#34">VIII. Piazza della
              Madonna e Prospetto della Basilica con Veduta delle
              Loggie di Bramante ▣</a>
            </li>
            <li>
              <a href="elor8245301s.html#36">IX. Interno della
              Loggia di Bramante al Pianterreno ▣</a>
            </li>
            <li>
              <a href="elor8245301s.html#38">X. Interno del
              secondo Portico di Bramante ▣</a>
            </li>
            <li>
              <a href="elor8245301s.html#40">XI. Navata maggiore
              della Basilica con Veduta del Lato anteriore ad
              occidentale di Santa Casa ▣</a>
            </li>
            <li>
              <a href="elor8245301s.html#42">XII. Arco
              principale sotto la Cupola con Veduta del Fianco a
              Mezzogiorno di Santa Casa ▣</a>
            </li>
            <li>
              <a href="elor8245301s.html#44">XIII. Arco minore
              sotto la Cupola col Monumento marmoreo di Santa Casa
              veduto in Angolo ▣</a>
            </li>
            <li>
              <a href="elor8245301s.html#46">XIV. Interno della
              Santa Casa con Prospetto della Santa Immagine di
              Maria Santissima ▣</a>
            </li>
            <li>
              <a href="elor8245301s.html#48">XV. Gran Sala e
              Cappella del Tesoro ▣</a>
            </li>
            <li>
              <a href="elor8245301s.html#50">XVI. Pianta del
              Santuario della Santa Casa di Loreto e dell'Edificio
              marmoreo che la riveste all'Esterno ◉</a>
            </li>
            <li>
              <a href="elor8245301s.html#52">XVII. Sezione nel
              Lungo dell'Interno della Santa Casa di Loreto ▣</a>
            </li>
            <li>
              <a href="elor8245301s.html#54">XVIII. Virgo
              Lauretana ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
