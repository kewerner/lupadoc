<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502771s.html#8">Roma Ricercata nel suo sito</a>
      <ul>
        <li>
          <a href="dg4502771s.html#10">Lettore forestiero</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502771s.html#14">[Text]</a>
      <ul>
        <li>
          <a href="dg4502771s.html#14">Giornata prima</a>
          <ul>
            <li>
              <a href="dg4502771s.html#14">Per il ›Borgo‹
              Vaticano</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502771s.html#31">Giornata seconda</a>
          <ul>
            <li>
              <a href="dg4502771s.html#31">Per il ›Trastevere‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502771s.html#37">Giornata terza</a>
          <ul>
            <li>
              <a href="dg4502771s.html#37">Da Strada Giulia ›Via
              Giulia‹ all'Isola di San Bartolomeo ›Isola
              Tiberina‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502771s.html#48">Giornata quarta</a>
          <ul>
            <li>
              <a href="dg4502771s.html#48">Da ›San Lorenzo in
              Damaso‹ al Monte ›Aventino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502771s.html#61">Giornata quinta</a>
          <ul>
            <li>
              <a href="dg4502771s.html#61">Dalla ›Piazza di
              Pasquino‹ per li monti ›Celio‹, e ›Palatino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502771s.html#76">Giornata sesta</a>
          <ul>
            <li>
              <a href="dg4502771s.html#76">Da ›San Salvatore in
              Lauro‹ per Campo Vaccino ›Foro Romano‹, e per le
              ›Carinae‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502771s.html#103">Giornata settima</a>
          <ul>
            <li>
              <a href="dg4502771s.html#103">Dalla ›Piazza di
              Sant'Apollinare‹ per il Monte ›Viminale‹, e
              ›Quirinale‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502771s.html#118">Giornata ottava</a>
          <ul>
            <li>
              <a href="dg4502771s.html#118">Da ›Piazza Nicosia‹
              alle Terme Diocletiane ›Terme di Diocleziano‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502771s.html#126">Giornata nona</a>
          <ul>
            <li>
              <a href="dg4502771s.html#126">›Piazza Nicosia‹ alle
              Porte del Popolo ›Porta del Popolo‹ e ›Porta
              Pinciana‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502771s.html#149">Giornata decima</a>
          <ul>
            <li>
              <a href="dg4502771s.html#149">Per le Nove Chiese</a>
              <ul>
                <li>
                  <a href="dg4502771s.html#160">Alle tre Fontane
                  ›Abbazia delle Tre Fontane‹</a>
                </li>
                <li>
                  <a href="dg4502771s.html#161">All'Annunziata
                  ›Annunziatella‹</a>
                </li>
                <li>
                  <a href="dg4502771s.html#161">A San Bastiano
                  ›San Sebastiano‹</a>
                </li>
                <li>
                  <a href="dg4502771s.html#163">Alla Basilica di
                  ›San Giovanni in Laterano‹</a>
                </li>
                <li>
                  <a href="dg4502771s.html#177">A ›Santa Croce in
                  Gerusalemme‹</a>
                </li>
                <li>
                  <a href="dg4502771s.html#178">A ›San Lorenzo
                  fuori le Mura‹</a>
                </li>
                <li>
                  <a href="dg4502771s.html#178">A ›Santa Maria
                  Maggiore‹</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502771s.html#182">Notitia delle Porte,
          Monti, e Rioni della Città</a>
          <ul>
            <li>
              <a href="dg4502771s.html#182">Porte della Città</a>
            </li>
            <li>
              <a href="dg4502771s.html#183">In ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502771s.html#183">In ›Borgo‹</a>
            </li>
            <li>
              <a href="dg4502771s.html#183">Monti dentro la
              Città</a>
            </li>
            <li>
              <a href="dg4502771s.html#185">Piazze nelle quali si
              vendono vettovaglie dette anticamente Macelli</a>
            </li>
            <li>
              <a href="dg4502771s.html#186">Piazze, e Contrade,
              dove risiedono diverse arti, e si fanno Fiere, e
              Mercati</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
