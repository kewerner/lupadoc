<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghvig50552820s.html#6">Le due regole della
      prospettiva prattica</a>
      <ul>
        <li>
          <a href="ghvig50552820s.html#12">Vita di Iacomo Barozzi
          da Vignola, Archtietto, e Prospettivo Eccellentissimo</a>
        </li>
        <li>
          <a href="ghvig50552820s.html#16">[Prefazione]
          Prefatione</a>
        </li>
        <li>
          <a href="ghvig50552820s.html#10">[Dedica di Natale
          Doregucci] Illustrissimo Signore&#160;</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvig50552820s.html#18">[Text]</a>
      <ul>
        <li>
          <a href="ghvig50552820s.html#18">La prima regola della
          prospettiva prattica ▣</a>
          <ul>
            <li>
              <a href="ghvig50552820s.html#26">Suppositione della
              prospettiva prattica ▣</a>
            </li>
            <li>
              <a href="ghvig50552820s.html#34">Teorema Primo
              ▣</a>
              <ul>
                <li>
                  <a href="ghvig50552820s.html#34">Propositione
                  prima ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghvig50552820s.html#115">La seconda regola
          della prospettiva prattica ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvig50552820s.html#163">[Registro] Tavola della
      cose più notabili&#160;</a>
    </li>
  </ul>
  <hr />
</body>
</html>
