<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ever302410s.html#6">Pitture scolture ed
      architetture, nelle tre chiese matrici della città di Verona,
      situate oltre l'adigetto fuori dalla parte meridionale</a>
      <ul>
        <li>
          <a href="ever302410s.html#8">Parrocchia di San
          Zeno&#160;</a>
        </li>
        <li>
          <a href="ever302410s.html#33">San Luca Parrocchia</a>
        </li>
        <li>
          <a href="ever302410s.html#37">La Santissima Trinità</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ever302410s.html#42">Nuovo diario
      veronese&#160;</a>
      <ul>
        <li>
          <a href="ever302410s.html#46">Pitture, scolture, ed
          architetture, le quali si riscontrano nelle chiese
          matrici&#160;</a>
          <ul>
            <li>
              <a href="ever302410s.html#46">Parrocchia della
              Cattedrale</a>
            </li>
            <li>
              <a href="ever302410s.html#52">Parrocchia di Sant'
              Eufemia</a>
            </li>
            <li>
              <a href="ever302410s.html#57">Parrocchia di Sant'
              Anastasia</a>
            </li>
            <li>
              <a href="ever302410s.html#69">Parrocchia de' Santi
              Appostoli</a>
            </li>
            <li>
              <a href="ever302410s.html#71">Parrocchia di San
              Nicolo</a>
            </li>
            <li>
              <a href="ever302410s.html#75">Parrocchia de'
              Filippini</a>
            </li>
            <li>
              <a href="ever302410s.html#78">Parrocchia di San
              Fermo Maggiore</a>
            </li>
            <li>
              <a href="ever302410s.html#86">Parrocchia di San
              Luca</a>
            </li>
            <li>
              <a href="ever302410s.html#86">Parrocchia della
              Santissima Trinità</a>
            </li>
            <li>
              <a href="ever302410s.html#87">Parrocchia di San
              Zeno</a>
            </li>
            <li>
              <a href="ever302410s.html#87">Parrocchia di San
              Stefano</a>
            </li>
            <li>
              <a href="ever302410s.html#88">Parrocchia de' Santi
              Nazaro e Celso&#160;</a>
            </li>
            <li>
              <a href="ever302410s.html#89">Divozioni fra
              l'anno</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="ever302410s.html#90">Pitture scolture ed
      architetture nelle quattro chiese matrici, situate alla
      sinistra dell'Adige.</a>
      <ul>
        <li>
          <a href="ever302410s.html#92">San Stefano</a>
        </li>
        <li>
          <a href="ever302410s.html#97">Santa Maria in
          Organis&#160;</a>
        </li>
        <li>
          <a href="ever302410s.html#109">Santi Nazario e
          Celso</a>
        </li>
        <li>
          <a href="ever302410s.html#121">San Paolo di Campo
          Marzo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
