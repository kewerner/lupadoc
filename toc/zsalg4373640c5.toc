<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zsalg4373640c5s.html#6">Opere del conte
      Algarotti, Tom. V.</a>
      <ul>
        <li>
          <a href="zsalg4373640c5s.html#8">Opere militari</a>
          <ul>
            <li>
              <a href="zsalg4373640c5s.html#10">Scienza
              militare del segretario fiorentino</a>
              <ul>
                <li>
                  <a href="zsalg4373640c5s.html#12">A Sua
                  Altezza reale Monsignor il principe Enrico di
                  Prussia&#160;</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#14">Lettera
                  I.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#22">Lettera
                  II.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#33">Lettera
                  III.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#38">Lettera
                  IV.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#43">Lettera
                  V.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#50">Lettera
                  VI.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#55">Lettera
                  VII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#66">Lettera
                  VIII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#73">Lettera
                  IX.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#77">Lettera
                  X.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#84">Lettera
                  XI.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#95">Lettera
                  XII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#101">Lettera
                  XIII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#107">Lettera
                  XIV.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#129">Lettera
                  XV.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#155">Lettera
                  XVI.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#165">Lettera
                  XVII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#173">Lettera
                  XVIII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#178">Lettera
                  XIX.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#182">Lettera
                  XX.</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c5s.html#186">Discorsi
              militari</a>
              <ul>
                <li>
                  <a href="zsalg4373640c5s.html#188">Discorso
                  I.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#202">Discorso
                  II.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#210">Discorso
                  III.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#217">Discorso
                  IV.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#225">Discorso
                  V.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#241">Discorso
                  VI.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#265">Discorso
                  VII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#282">Discorso
                  VIII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#291">Discorso
                  IX.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#307">Discorso
                  X.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#312">Discorso
                  XI.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#317">Discorso
                  XII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#326">Discorso
                  XIII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#347">Discorso
                  XIV.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#358">Discorso
                  XV.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#375">Discorso
                  XVI.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#385">Discorso
                  XVII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#399">Discorso
                  XVIII.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#408">Discorso
                  XIX.</a>
                </li>
                <li>
                  <a href="zsalg4373640c5s.html#418">Discorso
                  XX.</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c5s.html#438">Saggio sopra
              la giornata di Zama</a>
              <ul>
                <li>
                  <a href="zsalg4373640c5s.html#440">A Sua
                  Eccellenza il Signor Maresciallo di Keith</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c5s.html#460">Indice delle
              materie contenute nel Tomo Quinto</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
