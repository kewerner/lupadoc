<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ecre1523620s.html#6">Distinto rapporto delle
      dipinture, che trovansi nelle chiese della città, e sobborghi
      di Cremona</a>
      <ul>
        <li>
          <a href="ecre1523620s.html#8">Eccelenza</a>
        </li>
        <li>
          <a href="ecre1523620s.html#14">Correse Leggitore</a>
        </li>
        <li>
          <a href="ecre1523620s.html#26">Giornata prima</a>
        </li>
        <li>
          <a href="ecre1523620s.html#90">Giornata seconda</a>
        </li>
        <li>
          <a href="ecre1523620s.html#149">Giornata terza</a>
        </li>
        <li>
          <a href="ecre1523620s.html#166">Giornata quarta</a>
        </li>
        <li>
          <a href="ecre1523620s.html#201">Giornata quinta</a>
        </li>
        <li>
          <a href="ecre1523620s.html#229">Indice delle chiese di
          cui si descrivono le pitture</a>
        </li>
        <li>
          <a href="ecre1523620s.html#233">Indice degli Artefici
          nominati nell'Opera</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
