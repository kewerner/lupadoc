<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501191s.html#10">Primordia Regum &amp;
      Imperatorum Romanæ gentis, a Romulo &amp; Remo usque ad
      Constantinum Imperatorem &#160;</a>
      <ul>
        <li>
          <a href="dg4501191s.html#10">Primordia Regum &amp;
          Imperatorum Romanæ gentis, a Romulo &amp; Remo usque ad
          Constantinum Imperatorem &#160;</a>
          <ul>
            <li>
              <a href="dg4501191s.html#12">De Regibus Romanae
              Urbis&#160;</a>
            </li>
            <li>
              <a href="dg4501191s.html#16">De Imperatoribus
              Romanae Urbis&#160;</a>
            </li>
            <li>
              <a href="dg4501191s.html#25">De Constatino
              Imperatore Romanae Urbis</a>
            </li>
            <li>
              <a href="dg4501191s.html#31">Oratio devotissima ad
              Veronicam</a>
            </li>
            <li>
              <a href="dg4501191s.html#33">Indulgentie Ecclesiarum
              Romanae Urbis</a>
            </li>
            <li>
              <a href="dg4501191s.html#88">Stationes Romanae
              Urbis</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501191s.html#98">Mirabilia Romanae Urbis</a>
        </li>
        <li>
          <a href="dg4501191s.html#110">Divisio decem nationum
          totius Christianitatis</a>
        </li>
        <li>
          <a href="dg4501191s.html#116">Translatio miraculosa
          ecclesiae beatae Mariae de Loretae</a>
        </li>
        <li>
          <a href="dg4501191s.html#122">Indulgentiæ Ecclesiarum
          Romanæ Urbis</a>
        </li>
        <li>
          <a href="dg4501191s.html#122">Modus coronandi
          Imperatorem&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
