<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghalb18211850s.html#6">Trattato della nobiltà
      della pittura. Composto ad. instantia della venerabil'
      Compagnia di San Luca, et nobil'Academia delli pittori di
      Roma</a>
    </li>
    <li>
      <a href="ghalb18211850s.html#8">Tavola delle cose più
      notabili che si contengono nella presente opera</a>
    </li>
    <li>
      <a href="ghalb18211850s.html#11">[Due sonetti dedicati
      all'autore da Girolamo Magagnati]</a>
    </li>
    <li>
      <a href="ghalb18211850s.html#14">Al'Illustrissimo et
      Reverendissimo [...] Monsignor Alfonso Gesualdo Cardinale di
      Santa Chiesa Prottetore [sic] del Regno di Napoli [Dedica
      dell'autore]</a>
    </li>
    <li>
      <a href="ghalb18211850s.html#17">[Trattato]</a>
      <ul>
        <li>
          <a href="ghalb18211850s.html#17">Capitolo I.</a>
        </li>
        <li>
          <a href="ghalb18211850s.html#51">Capitolo II.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
