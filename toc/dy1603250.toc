<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy1603250s.html#8">Relazione della statua equestre
      di Carlo Magno, eretta nel portico del Tempio Vaticano colla
      raccolta d’alcuni componimenti poetici</a>
      <ul>
        <li>
          <a href="dy1603250s.html#10">Altezza Reale</a>
        </li>
        <li>
          <a href="dy1603250s.html#12">Relazione della statua
          equestre di Carlo Magno</a>
        </li>
        <li>
          <a href="dy1603250s.html#18">[Sonetti]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
