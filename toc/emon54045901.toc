<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="emon54045901s.html#8">Il Duomo di Monreale</a>
      <ul>
        <li>
          <a href="emon54045901s.html#12">Ragione dell'opera</a>
        </li>
        <li>
          <a href="emon54045901s.html#20">I. Descrizione del
          tempio</a>
        </li>
        <li>
          <a href="emon54045901s.html#24">II. Storia del tempio
          dal secolo XII all'anno 1858</a>
        </li>
        <li>
          <a href="emon54045901s.html#40">III. Architettura del
          tempio</a>
        </li>
        <li>
          <a href="emon54045901s.html#102">IV. Sacra iconografia
          del duomo di Monreale</a>
        </li>
        <li>
          <a href="emon54045901s.html#158">V. Usi, e costumi
          sacri e profani tratti dai mosaici del duomo di
          Monreale</a>
        </li>
        <li>
          <a href="emon54045901s.html#208">VI. La simbolica del
          duomo di Monreale</a>
        </li>
        <li>
          <a href="emon54045901s.html#230">Conclusione.
          Aggiunzioni e correzioni</a>
        </li>
        <li>
          <a href="emon54045901s.html#234">Indice delle cose
          notevoli</a>
        </li>
        <li>
          <a href="emon54045901s.html#242">Indice delle
          materie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
