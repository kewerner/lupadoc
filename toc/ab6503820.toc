<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ab6503820s.html#8">A catalogue of engravers, who
      haven been born, or resided in England digested by Horace
      Walpole from the mss. of George Vertue; to which is added an
      account of the life and works of the latter</a>
      <ul>
        <li>
          <a href="ab6503820s.html#10">A Catalogue of
          Engravers</a>
          <ul>
            <li>
              <a href="ab6503820s.html#19">Thomas Geminus, or
              Geminie</a>
            </li>
            <li>
              <a href="ab6503820s.html#22">Remigius Hogenbergh</a>
            </li>
            <li>
              <a href="ab6503820s.html#22">Francis Hogenbergh</a>
            </li>
            <li>
              <a href="ab6503820s.html#24">Dr. William
              Cunygham</a>
            </li>
            <li>
              <a href="ab6503820s.html#25">Ralph Aggas</a>
            </li>
            <li>
              <a href="ab6503820s.html#28">Humphry Cole</a>
            </li>
            <li>
              <a href="ab6503820s.html#29">John Bettes</a>
            </li>
            <li>
              <a href="ab6503820s.html#30">William Rogers</a>
            </li>
            <li>
              <a href="ab6503820s.html#31">Christopher Saxton</a>
            </li>
            <li>
              <a href="ab6503820s.html#33">George Hoefnagle</a>
            </li>
            <li>
              <a href="ab6503820s.html#34">Theodore de Brie</a>
            </li>
            <li>
              <a href="ab6503820s.html#36">Robert Adams</a>
            </li>
            <li>
              <a href="ab6503820s.html#37">Reginald Elstracke</a>
            </li>
            <li>
              <a href="ab6503820s.html#41">Francis Delaram</a>
            </li>
            <li>
              <a href="ab6503820s.html#45">Crispin Pass</a>
            </li>
            <li>
              <a href="ab6503820s.html#51">William Pass</a>
            </li>
            <li>
              <a href="ab6503820s.html#56">Magdalen Pass</a>
            </li>
            <li>
              <a href="ab6503820s.html#56">Simon Pass</a>
            </li>
            <li>
              <a href="ab6503820s.html#62">John Payne</a>
            </li>
            <li>
              <a href="ab6503820s.html#65">Joannes Barra</a>
            </li>
            <li>
              <a href="ab6503820s.html#66">John Norden</a>
            </li>
            <li>
              <a href="ab6503820s.html#68">William Hole or
              Holle</a>
            </li>
            <li>
              <a href="ab6503820s.html#68">Jo Docus Hondius</a>
            </li>
            <li>
              <a href="ab6503820s.html#70">Henry Hondius</a>
            </li>
            <li>
              <a href="ab6503820s.html#70">A. Bloom</a>
            </li>
            <li>
              <a href="ab6503820s.html#71">Thomas Cockson</a>
            </li>
            <li>
              <a href="ab6503820s.html#72">Peter Stent</a>
            </li>
            <li>
              <a href="ab6503820s.html#74">William Dolle</a>
            </li>
            <li>
              <a href="ab6503820s.html#74">Deodate</a>
            </li>
            <li>
              <a href="ab6503820s.html#75">R. Meighan</a>
            </li>
            <li>
              <a href="ab6503820s.html#75">Thomas Cecill</a>
            </li>
            <li>
              <a href="ab6503820s.html#76">Robert Vaughan</a>
            </li>
            <li>
              <a href="ab6503820s.html#79">William Marshal</a>
            </li>
            <li>
              <a href="ab6503820s.html#82">G. Glover</a>
            </li>
            <li>
              <a href="ab6503820s.html#82">Henry Peacham</a>
            </li>
            <li>
              <a href="ab6503820s.html#84">Robert de Voerst</a>
            </li>
            <li>
              <a href="ab6503820s.html#87">Luke Vosterman</a>
            </li>
            <li>
              <a href="ab6503820s.html#91">Martin Droeshout</a>
            </li>
            <li>
              <a href="ab6503820s.html#92">H. Stock</a>
            </li>
            <li>
              <a href="ab6503820s.html#92">H. Vanderborcht</a>
            </li>
            <li>
              <a href="ab6503820s.html#92">T. Slater</a>
            </li>
            <li>
              <a href="ab6503820s.html#93">Thomas Cross</a>
            </li>
            <li>
              <a href="ab6503820s.html#94">S. Savery</a>
            </li>
            <li>
              <a href="ab6503820s.html#95">J. Goddard</a>
            </li>
            <li>
              <a href="ab6503820s.html#96">J. Dickson</a>
            </li>
            <li>
              <a href="ab6503820s.html#96">A. Hertocks</a>
            </li>
            <li>
              <a href="ab6503820s.html#97">J. Chantry</a>
            </li>
            <li>
              <a href="ab6503820s.html#97">F. H. van Hove</a>
            </li>
            <li>
              <a href="ab6503820s.html#98">Rotermans</a>
            </li>
            <li>
              <a href="ab6503820s.html#98">Francis Barlow</a>
            </li>
            <li>
              <a href="ab6503820s.html#99">R. Gaywood</a>
            </li>
            <li>
              <a href="ab6503820s.html#101">Dudley and Carter</a>
            </li>
            <li>
              <a href="ab6503820s.html#102">Mr. Francis Place</a>
            </li>
            <li>
              <a href="ab6503820s.html#106">J. Savage</a>
            </li>
            <li>
              <a href="ab6503820s.html#107">Mr. William Lodge</a>
            </li>
            <li>
              <a href="ab6503820s.html#112">William Sherwin</a>
            </li>
            <li>
              <a href="ab6503820s.html#113">Joseph Nutting</a>
            </li>
            <li>
              <a href="ab6503820s.html#115">William Faithorne</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ab6503820s.html#120">[Classes]</a>
          <ul>
            <li>
              <a href="ab6503820s.html#120">I.</a>
            </li>
            <li>
              <a href="ab6503820s.html#125">II.</a>
            </li>
            <li>
              <a href="ab6503820s.html#129">III.</a>
            </li>
            <li>
              <a href="ab6503820s.html#130">IV. and V.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ab6503820s.html#133">Heads</a>
        </li>
        <li>
          <a href="ab6503820s.html#135">[A Catalogue of
          Engravers]</a>
          <ul>
            <li>
              <a href="ab6503820s.html#135">William Faithorne,
              junior</a>
            </li>
            <li>
              <a href="ab6503820s.html#137">John Fillian</a>
            </li>
            <li>
              <a href="ab6503820s.html#137">Peter Lombart</a>
            </li>
            <li>
              <a href="ab6503820s.html#140">James Gammon</a>
            </li>
            <li>
              <a href="ab6503820s.html#141">Robert Thacker</a>
            </li>
            <li>
              <a href="ab6503820s.html#141">William Skillman</a>
            </li>
            <li>
              <a href="ab6503820s.html#142">John Dunstall</a>
            </li>
            <li>
              <a href="ab6503820s.html#142">J. Brown</a>
            </li>
            <li>
              <a href="ab6503820s.html#142">Prince Rupert</a>
            </li>
            <li>
              <a href="ab6503820s.html#152">Wallerant Vaillant</a>
            </li>
            <li>
              <a href="ab6503820s.html#154">Mr. John Evelyn</a>
            </li>
            <li>
              <a href="ab6503820s.html#161">David Loggan</a>
            </li>
            <li>
              <a href="ab6503820s.html#168">Abraham Blooteling</a>
            </li>
            <li>
              <a href="ab6503820s.html#170">Gerard Valck</a>
            </li>
            <li>
              <a href="ab6503820s.html#171">Edward le Davis</a>
            </li>
            <li>
              <a href="ab6503820s.html#172">Lightfoot</a>
            </li>
            <li>
              <a href="ab6503820s.html#173">Michael Burghers</a>
            </li>
            <li>
              <a href="ab6503820s.html#175">Peter Vanderbank</a>
            </li>
            <li>
              <a href="ab6503820s.html#179">Nicholas Yeates and
              John Collins</a>
            </li>
            <li>
              <a href="ab6503820s.html#180">William Clarke</a>
            </li>
            <li>
              <a href="ab6503820s.html#180">John Clarke</a>
            </li>
            <li>
              <a href="ab6503820s.html#181">R. Tompson</a>
            </li>
            <li>
              <a href="ab6503820s.html#182">Paul Vansomer</a>
            </li>
            <li>
              <a href="ab6503820s.html#184">Robert White</a>
            </li>
            <li>
              <a href="ab6503820s.html#200">George White</a>
            </li>
            <li>
              <a href="ab6503820s.html#200">Arthur Soly</a>
            </li>
            <li>
              <a href="ab6503820s.html#201">Hamlet Winstanley</a>
            </li>
            <li>
              <a href="ab6503820s.html#203">Burnford</a>
            </li>
            <li>
              <a href="ab6503820s.html#203">Isaac Oliver</a>
            </li>
            <li>
              <a href="ab6503820s.html#204">John Drapentiere</a>
            </li>
            <li>
              <a href="ab6503820s.html#204">William Elder</a>
            </li>
            <li>
              <a href="ab6503820s.html#205">John Sturt</a>
            </li>
            <li>
              <a href="ab6503820s.html#207">Mr. Lutterel</a>
            </li>
            <li>
              <a href="ab6503820s.html#208">Isaac Becket</a>
            </li>
            <li>
              <a href="ab6503820s.html#211">Mr. John Smith</a>
            </li>
            <li>
              <a href="ab6503820s.html#214">Simon Gribelin</a>
            </li>
            <li>
              <a href="ab6503820s.html#216">Sir Nicholas
              Dorigny</a>
            </li>
            <li>
              <a href="ab6503820s.html#220">Charles Dupuis</a>
            </li>
            <li>
              <a href="ab6503820s.html#221">Claude Dubosc</a>
            </li>
            <li>
              <a href="ab6503820s.html#222">Lewis du Guernier</a>
            </li>
            <li>
              <a href="ab6503820s.html#223">George Bickham</a>
            </li>
            <li>
              <a href="ab6503820s.html#224">S. Coignard</a>
            </li>
            <li>
              <a href="ab6503820s.html#224">T. Johnson</a>
            </li>
            <li>
              <a href="ab6503820s.html#224">John Kip</a>
            </li>
            <li>
              <a href="ab6503820s.html#225">George King</a>
            </li>
            <li>
              <a href="ab6503820s.html#226">S. Nichols</a>
            </li>
            <li>
              <a href="ab6503820s.html#227">Joseph Simpson</a>
            </li>
            <li>
              <a href="ab6503820s.html#227">Peter van Gunst</a>
            </li>
            <li>
              <a href="ab6503820s.html#228">Robert, or Roger
              Williams</a>
            </li>
            <li>
              <a href="ab6503820s.html#229">W. Wilson</a>
            </li>
            <li>
              <a href="ab6503820s.html#229">Michael
              Vandergutch</a>
            </li>
            <li>
              <a href="ab6503820s.html#230">John Vandergutch</a>
            </li>
            <li>
              <a href="ab6503820s.html#231">Claud David</a>
            </li>
            <li>
              <a href="ab6503820s.html#231">Chereau, junior</a>
            </li>
            <li>
              <a href="ab6503820s.html#232">Bernard Lens</a>
            </li>
            <li>
              <a href="ab6503820s.html#233">Samuel Moore</a>
            </li>
            <li>
              <a href="ab6503820s.html#234">Scotin</a>
            </li>
            <li>
              <a href="ab6503820s.html#235">Mr. English</a>
            </li>
            <li>
              <a href="ab6503820s.html#235">Henry Hulsberg</a>
            </li>
            <li>
              <a href="ab6503820s.html#236">John Faber</a>
            </li>
            <li>
              <a href="ab6503820s.html#236">John Faber, junior</a>
            </li>
            <li>
              <a href="ab6503820s.html#237">Edward Kirkall</a>
            </li>
            <li>
              <a href="ab6503820s.html#239">James Christopher Le
              Blon</a>
            </li>
            <li>
              <a href="ab6503820s.html#241">John Simon</a>
            </li>
            <li>
              <a href="ab6503820s.html#242">L. Boitard</a>
            </li>
            <li>
              <a href="ab6503820s.html#243">B. Baron</a>
            </li>
            <li>
              <a href="ab6503820s.html#245">Henry Gravelot</a>
            </li>
            <li>
              <a href="ab6503820s.html#246">John Pine</a>
            </li>
            <li>
              <a href="ab6503820s.html#247">Arthur Pond</a>
            </li>
            <li>
              <a href="ab6503820s.html#248">Henry Fletcher</a>
            </li>
            <li>
              <a href="ab6503820s.html#248">Carey Creed</a>
            </li>
            <li>
              <a href="ab6503820s.html#249">Joseph Wagner</a>
            </li>
            <li>
              <a href="ab6503820s.html#249">Thomas Preston</a>
            </li>
            <li>
              <a href="ab6503820s.html#250">John Laguerre</a>
            </li>
            <li>
              <a href="ab6503820s.html#251">Peter Fourdriniere</a>
            </li>
            <li>
              <a href="ab6503820s.html#251">John Green</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ab6503820s.html#256">The Life of Mr. George
          Verture</a>
        </li>
        <li>
          <a href="ab6503820s.html#280">List of Vertue's Works</a>
        </li>
        <li>
          <a href="ab6503820s.html#281">[Classes]</a>
          <ul>
            <li>
              <a href="ab6503820s.html#281">I. Royal Portraits</a>
            </li>
            <li>
              <a href="ab6503820s.html#283">II. Noblemen</a>
            </li>
            <li>
              <a href="ab6503820s.html#285">III. Ladies</a>
            </li>
            <li>
              <a href="ab6503820s.html#285">IV. Bishops</a>
            </li>
            <li>
              <a href="ab6503820s.html#288">V. Clergymen</a>
            </li>
            <li>
              <a href="ab6503820s.html#291">VI. Chancellors,
              Judges, Lawyers</a>
            </li>
            <li>
              <a href="ab6503820s.html#293">VII. Ministers, and
              Gentlemen</a>
            </li>
            <li>
              <a href="ab6503820s.html#295">VIII. Physicians,
              etc.</a>
            </li>
            <li>
              <a href="ab6503820s.html#295">IX. Founders,
              Benefactors, etc.</a>
            </li>
            <li>
              <a href="ab6503820s.html#296">X. Antiquaries,
              Authors, Mathematicians</a>
            </li>
            <li>
              <a href="ab6503820s.html#297">XI. Poets and
              Musicians</a>
            </li>
            <li>
              <a href="ab6503820s.html#300">XII. Foreigners</a>
            </li>
            <li>
              <a href="ab6503820s.html#302">XIII. Historic Prints,
              and Prints with two or more Portraits</a>
            </li>
            <li>
              <a href="ab6503820s.html#304">XIV. Tombs</a>
            </li>
            <li>
              <a href="ab6503820s.html#304">XV. Plans, Views,
              Churches, Buildings, etc.</a>
            </li>
            <li>
              <a href="ab6503820s.html#307">XVI. Coins, Medals,
              Busts, Seals, Charters, Gems, and Shells</a>
            </li>
            <li>
              <a href="ab6503820s.html#310">XVII. Frontispieces,
              Head and Tail-Pieces</a>
            </li>
            <li>
              <a href="ab6503820s.html#312">XVIII.
              Miscellaneous</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ab6503820s.html#314">Index of Names of
          Engravers, ranged according to the Times in which they
          lived</a>
        </li>
        <li>
          <a href="ab6503820s.html#317">Index of Names of
          Engravers, ranged alphabetically</a>
        </li>
        <li>
          <a href="ab6503820s.html#320">Carmina quaedam
          elegantissima</a>
          <ul>
            <li>
              <a href="ab6503820s.html#352">Ex mar. Antonii
              Flaminii epistolis.</a>
              <ul>
                <li>
                  <a href="ab6503820s.html#352">Ad al. Farnesium
                  card.</a>
                </li>
                <li>
                  <a href="ab6503820s.html#353">Ad hier.
                  Turrianum</a>
                </li>
                <li>
                  <a href="ab6503820s.html#354">Ad ulyss.
                  Bassianum</a>
                </li>
                <li>
                  <a href="ab6503820s.html#355">Ad galat.
                  Florimon. Philalethem</a>
                </li>
                <li>
                  <a href="ab6503820s.html#358">Ad Petrum
                  Carnescum</a>
                </li>
                <li>
                  <a href="ab6503820s.html#358">Ad Ubertum
                  Folietam</a>
                </li>
                <li>
                  <a href="ab6503820s.html#359">Ad Ludov.
                  Strozzam</a>
                </li>
                <li>
                  <a href="ab6503820s.html#360">Ad Petrum
                  Victorium</a>
                </li>
                <li>
                  <a href="ab6503820s.html#360">Ad Carol.
                  Valterutium</a>
                </li>
                <li>
                  <a href="ab6503820s.html#361">Ad Christophorum
                  Longolium</a>
                </li>
                <li>
                  <a href="ab6503820s.html#362">Ad Honoratum
                  Fascitellum</a>
                </li>
                <li>
                  <a href="ab6503820s.html#363">Ad Hieronymum
                  Fracastorium</a>
                </li>
                <li>
                  <a href="ab6503820s.html#364">Ad Petrum
                  Viperam</a>
                </li>
                <li>
                  <a href="ab6503820s.html#365">Ad Hieronymum
                  Pontanum</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
