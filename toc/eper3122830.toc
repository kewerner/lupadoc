<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="eper3122830s.html#8">Brevi notizie delle pitture,
      e sculture che adornano l’augusta città di Perugia</a>
      <ul>
        <li>
          <a href="eper3122830s.html#10">Illustrissimo
          Signore</a>
        </li>
        <li>
          <a href="eper3122830s.html#15">Ai cortesi lettori</a>
        </li>
        <li>
          <a href="eper3122830s.html#24">[Brevi notizie delle
          pitture, e sculture che adornano l’augusta città di
          Perugia]</a>
        </li>
        <li>
          <a href="eper3122830s.html#161">Indice di quelli, che
          anno, dipinto, scolpito, ò intagliato</a>
        </li>
        <li>
          <a href="eper3122830s.html#189">Indice delle Chiese, e
          altri Luoghi nomanati nella presente opera</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
