<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlan982540901s.html#8">Storia pittorica della
      Italia; tomo I. Ove si descrive la scuola fiorentina e la
      senese&#160;</a>
      <ul>
        <li>
          <a href="ghlan982540901s.html#10">Al nobil uomo il
          Signor Cavalier Giovanni Alessandri</a>
        </li>
        <li>
          <a href="ghlan982540901s.html#14">Compartimento di
          questo tomo primo</a>
        </li>
        <li>
          <a href="ghlan982540901s.html#16">Prefazione</a>
        </li>
        <li>
          <a href="ghlan982540901s.html#56">Della storia della
          Italia inferiore</a>
          <ul>
            <li>
              <a href="ghlan982540901s.html#56">I. Scuola
              fiorentina</a>
              <ul>
                <li>
                  <a href="ghlan982540901s.html#56">I. Origini
                  della pittura risoirta. Società e metodi degli
                  antichi pittori. Serie de' Toscani fino a Cimabue
                  e a Giotto&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540901s.html#172">II. Il
                  Vinci, il Buonarruoti, ed altri artefici
                  eccelenti formano la più florida epoca a questa
                  scuola&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540901s.html#239">III.
                  Gl'imitatori di Michelangiolo&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540901s.html#281">IV. I
                  Cigoli e i suoi compagni tornan la pittura in
                  miglior grado&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540901s.html#326">V. I
                  Cortoneschi&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ghlan982540901s.html#356">II. Scuola
              senese</a>
              <ul>
                <li>
                  <a href="ghlan982540901s.html#356">I. Gli
                  antichi&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540901s.html#384">II. Pittori
                  esteri a Siena, Principi in qulla città, e
                  progressi nello stile moderno&#160;</a>
                </li>
                <li>
                  <a href="ghlan982540901s.html#407">III. L'arte
                  decaduta in Siena fra le pubbliche traverse, per
                  opera del Salimbeni e de' figli torna in buon
                  grado&#160;</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
