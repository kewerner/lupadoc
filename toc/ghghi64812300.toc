<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghghi64812300s.html#6">Cefalogia fisonomica divisa
      in dieci Deche</a>
      <ul>
        <li>
          <a href="ghghi64812300s.html#8">All'illustrissimo e
          Generosissimo Signore, il signor Giorgio Sigefirdo
          Brevner [...]</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#16">L'auttore à chi
          legge</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#18">Tavola delle Deche, e
          de' Discorsi</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#22">Modo di porre in
          pratica le regole del Fisonomizare [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghghi64812300s.html#26">Discorsi fisonomici
      dell'Huomo</a>
      <ul>
        <li>
          <a href="ghghi64812300s.html#26">Deca prima nella quale
          si tratta la materia de' Capegli, et sue parti</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#98">Deca seconda nella
          quale si tratta della Fronte, che è la più secreta, e
          nobil parte della Fisonomia</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#164">Deca terza nella
          quale si ragiona delle Sovraciglia, ornamento della
          faccia</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#219">Deca quarta nella
          quale si tratta de gli occhi, che sono il maggiore, e più
          importante negotio di tutta la Fisonomia</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#332">Deca quinta nella
          quale si discorre del Naso</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#417">Deca sesta nella
          quale si discorre della Bocca, e sue parti</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#465">Deca settima nella
          quale si discorre del Mento, e sue qualità</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#506">Deca ottava nella
          quale si discorre delle Orecchie, e qualità sue</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#552">Deca nona nella quale
          si ragiona della Faccia, e sue qualità</a>
        </li>
        <li>
          <a href="ghghi64812300s.html#606">Deca decima nella
          quale si discorre del Capo, e sue parti</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghghi64812300s.html#650">Universal cognitione di
      varie Genti, e Provincie</a>
    </li>
    <li>
      <a href="ghghi64812300s.html#654">Tavola delle cose più
      notabili, che nella presente Fisonomia si contengono</a>
    </li>
  </ul>
  <hr />
</body>
</html>
