<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caraf1474520s.html#6">The Caryatides from the
      "Stanza dell’Eliodoro" in the Vatican, designed by Raffaelle
      d’Urbino</a>
      <ul>
        <li>
          <a href="caraf1474520s.html#8">To the Rev. Henry
          Wellesby D. D. Principal of New Inn Hall</a>
        </li>
        <li>
          <a href="caraf1474520s.html#10">Tge Caryatides, int the
          stanza dell'Eliodoro, or Hall of Heliodorus, in the
          Vatican</a>
        </li>
        <li>
          <a href="caraf1474520s.html#14">[Plates]&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
