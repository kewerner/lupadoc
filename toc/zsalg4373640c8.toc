<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zsalg4373640c8s.html#6">Opere; Tomo VIII.</a>
      <ul>
        <li>
          <a href="zsalg4373640c8s.html#8">Lettere sopra la
          pittura</a>
        </li>
        <li>
          <a href="zsalg4373640c8s.html#216">Lettere sopra
          l'architettura</a>
        </li>
        <li>
          <a href="zsalg4373640c8s.html#358">Progetto per
          ridurre a compimento il Regio Museo di Dresda</a>
          <ul>
            <li>
              <a href="zsalg4373640c8s.html#382">Argomento di
              quadri</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zsalg4373640c8s.html#396">Indice de' pittori
          architetti ec. ec. nominati in questo tomo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
