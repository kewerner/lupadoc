<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg1403481s.html#6">Nuova pianta di Roma data in
      luce da Giambattista Nolli l'anno MDCCXLVIII</a>
      <ul>
        <li>
          <a href="dg1403481s.html#8">Avviso al lettore</a>
        </li>
        <li>
          <a href="dg1403481s.html#10">Indice de' numeri della
          pianta&#160;</a>
          <ul>
            <li>
              <a href="dg1403481s.html#10">Rione I.
              Monti&#160;</a>
            </li>
            <li>
              <a href="dg1403481s.html#10">Rione II.
              Trevi&#160;</a>
            </li>
            <li>
              <a href="dg1403481s.html#10">Rione III.
              Colonna&#160;</a>
            </li>
            <li>
              <a href="dg1403481s.html#12">Rione IV.
              Campomarzo&#160;</a>
            </li>
            <li>
              <a href="dg1403481s.html#12">Rione V.
              Ponte&#160;</a>
            </li>
            <li>
              <a href="dg1403481s.html#12">Rione VI. Parione</a>
            </li>
            <li>
              <a href="dg1403481s.html#12">Rione VII. Regola</a>
            </li>
            <li>
              <a href="dg1403481s.html#14">Rione VIII.
              Sant'Eustachio</a>
            </li>
            <li>
              <a href="dg1403481s.html#14">Rione IX. Pigna</a>
            </li>
            <li>
              <a href="dg1403481s.html#14">Rione X. Campitelli</a>
            </li>
            <li>
              <a href="dg1403481s.html#14">Rione XI.
              Sant'Angelo</a>
            </li>
            <li>
              <a href="dg1403481s.html#16">Rione XII. Ripa</a>
            </li>
            <li>
              <a href="dg1403481s.html#16">Rione XIII.
              Trastevere</a>
            </li>
            <li>
              <a href="dg1403481s.html#16">Rione XIV. Borgo</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg1403481s.html#19">La Nuova Topografia di Roma</a>
      <ul>
        <li>
          <a href="dg1403481s.html#19">[fol.] 5 Rione Ripa</a>
        </li>
        <li>
          <a href="dg1403481s.html#20">[fol.] 6 Rioni Ripa,
          Campitelli</a>
        </li>
        <li>
          <a href="dg1403481s.html#23">[fol.] 7
          [Rom-Allegorie]</a>
        </li>
        <li>
          <a href="dg1403481s.html#24">[fol.] 8 Rione Ripa</a>
        </li>
        <li>
          <a href="dg1403481s.html#27">[fol.] 9 Rione Monti</a>
        </li>
        <li>
          <a href="dg1403481s.html#28">[fol.] 10 [Allegori der
          Santa Chiesa Romana]</a>
        </li>
        <li>
          <a href="dg1403481s.html#31">[fol.] 11 Rione
          Trastevere</a>
        </li>
        <li>
          <a href="dg1403481s.html#32">[fol.] 12 Rioni Trastevere,
          Ripa</a>
        </li>
        <li>
          <a href="dg1403481s.html#35">[fol.] 13 Rioni Trastevere,
          Ripa</a>
        </li>
        <li>
          <a href="dg1403481s.html#36">[fol.] 14 Rioni Campitelli,
          Monti</a>
        </li>
        <li>
          <a href="dg1403481s.html#39">[fol.] 15 Rioni Monti,
          Campitelli</a>
        </li>
        <li>
          <a href="dg1403481s.html#40">[fol.] 16 Rione Monti</a>
        </li>
        <li>
          <a href="dg1403481s.html#43">[fol.] 17 Rione
          Trastevere</a>
        </li>
        <li>
          <a href="dg1403481s.html#44">[fol.] 18 Rioni Trastevere,
          Ponte, Regola, Parione</a>
        </li>
        <li>
          <a href="dg1403481s.html#47">[fol.] 19 Rioni Parione,
          Sant' Eustachio, Pigna, Sant' Angelo, Trastevere, Ponte,
          Regola, Campo Marzo, Campitelli</a>
        </li>
        <li>
          <a href="dg1403481s.html#48">[fol.] 20 Rioni Campitelli,
          Pigna, Colonna, Trevi, Monti</a>
        </li>
        <li>
          <a href="dg1403481s.html#51">[fol.] 21 Rioni Monti,
          Trevi</a>
        </li>
        <li>
          <a href="dg1403481s.html#52">[fol.] 22 Rione Monti</a>
        </li>
        <li>
          <a href="dg1403481s.html#55">[fol.] 23 Rione Borgo</a>
        </li>
        <li>
          <a href="dg1403481s.html#56">[fol.] 24 Rioni Trastevere,
          Ponte</a>
        </li>
        <li>
          <a href="dg1403481s.html#59">[fol.] 25 Rioni Campo
          Marzo, Ponte, Colonna</a>
        </li>
        <li>
          <a href="dg1403481s.html#60">[fol.] 26 Rioni Campo
          Marzo, Ponte, Colonna, Trevi</a>
        </li>
        <li>
          <a href="dg1403481s.html#63">[fol.] 27 Rione Trevi</a>
        </li>
        <li>
          <a href="dg1403481s.html#64">[fol.] 28 Rione Monti</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg1403481s.html#66">Indice alfabetico della
      pianta</a>
      <ul>
        <li>
          <a href="dg1403481s.html#66">Antichità ch' esistono</a>
        </li>
        <li>
          <a href="dg1403481s.html#66">Acquedotti e archi
          moderni</a>
        </li>
        <li>
          <a href="dg1403481s.html#66">Capelle</a>
        </li>
        <li>
          <a href="dg1403481s.html#66">Chiese de' secolari o con
          monasteri conventi ospizj etc.</a>
        </li>
        <li>
          <a href="dg1403481s.html#68">Collegi e seminari</a>
        </li>
        <li>
          <a href="dg1403481s.html#68">Conservatori e monasteri di
          donne senza chiesa</a>
        </li>
        <li>
          <a href="dg1403481s.html#68">Fontane rinominate</a>
        </li>
        <li>
          <a href="dg1403481s.html#68">Luoghi pubblici</a>
        </li>
        <li>
          <a href="dg1403481s.html#68">Oratori</a>
        </li>
        <li>
          <a href="dg1403481s.html#68">Ospizj regolari senza
          chiesa</a>
        </li>
        <li>
          <a href="dg1403481s.html#68">Ospizj secolari</a>
        </li>
        <li>
          <a href="dg1403481s.html#70">Ospizj secolari
          nazionali</a>
        </li>
        <li>
          <a href="dg1403481s.html#70">Palazzi</a>
        </li>
        <li>
          <a href="dg1403481s.html#70">Piazze</a>
        </li>
        <li>
          <a href="dg1403481s.html#72">Spedali</a>
        </li>
        <li>
          <a href="dg1403481s.html#72">Spedali nazionali</a>
        </li>
        <li>
          <a href="dg1403481s.html#72">Strade</a>
        </li>
        <li>
          <a href="dg1403481s.html#72">Torri</a>
        </li>
        <li>
          <a href="dg1403481s.html#72">Vicoli</a>
        </li>
        <li>
          <a href="dg1403481s.html#72">Avvertimento</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
