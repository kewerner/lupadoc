<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zsalg4373640c4s.html#6">Opere del conte
      Algarotti, Tom. IV.</a>
      <ul>
        <li>
          <a href="zsalg4373640c4s.html#8">Saggi sopra
          differenti soggetti</a>
          <ul>
            <li>
              <a href="zsalg4373640c4s.html#10">Saggio sopra la
              necessita' di scrivere nella propria lingua</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#12">Al molto
                  Rev. Padre Saverio Bettinelli</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#36">Saggio sopra la
              lingua francese</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#38">Al Sig.
                  Marchese Scipione Maffei</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#82">Saggio sopra la
              rima</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#84">Al Signor
                  Tomaso Villiers</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#134">Saggio sopra
              la durata de'regni de're di Roma</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#136">Al Signor
                  Francesco Maria Zanotti</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#178">Saggio sopra
              l'imperio degl'incas</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#180">Al
                  Reverendiss. Padre Jacopo Stellini C.R.S.</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#210">Saggio sopra
              quella quistione, perche' i grandi ingegni a certi
              tempi sorgano tutti ad un tratto e fioriscano
              insieme</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#212">Al Signore
                  di Maupertuis</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#250">Saggio sopra
              la quistione se la qualita' varie de'popoli originate
              siano dallo influsso del clima, ovveramente dalla
              virtu' delle legislazione</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#252">Al Signor
                  Guglielmo Tailor How</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#284">Saggio sopra
              il gentilesimo</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#286">A Sua
                  Eccellenza il Sig. Giovanni Emo</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#322">Saggio sopra
              il commercio</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#324">Al SIgnor
                  Cavaliere Lorenzo Guazzesi</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#342">Saggio sopra
              il cartesio</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#344">Al Signor
                  Eustachio Zanotti</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#412">Saggio sopra
              Orazio</a>
              <ul>
                <li>
                  <a href="zsalg4373640c4s.html#414">A Federico
                  il grande</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="zsalg4373640c4s.html#564">Indice delle
              materie contenute nel Tomo Quarto</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
