<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efer1013730s.html#8">Memorie istoriche delle
      chiese di Ferrara e de’ suoi borghi ; munite, ed illustrate
      con antichi inediti monumenti, che ponno servire all’istoria
      sacra della suddetta città &#160;</a>
      <ul>
        <li>
          <a href="efer1013730s.html#10">Illustrissimo
          Signore</a>
        </li>
        <li>
          <a href="efer1013730s.html#16">Al benigno lettore</a>
        </li>
        <li>
          <a href="efer1013730s.html#20">[Memorie istoriche delle
          chiese di Ferrara e de’ suoi borghi ; munite, ed
          illustrate con antichi inediti monumenti, che ponno
          servire all’istoria sacra della suddetta città]
          &#160;</a>
        </li>
        <li>
          <a href="efer1013730s.html#444">Borghi di Ferrara</a>
        </li>
        <li>
          <a href="efer1013730s.html#574">Tavole delle chiese, e
          luoghi pij, che si contengono in questo libro</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
