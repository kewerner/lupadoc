<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbor683932001s.html#6">Opera Del Caval.
      Francesco Boromino Cavata da suoi Originali cioè La Chiesa, e
      Fabrica della Sapienza di Roma con le Vedute in Prospettiua
      &amp; con lo Studio delle Proporz.ni Geometriche, Piante,
      Alzate, Profili e Spaccati</a>
      <ul>
        <li>
          <a href="ghbor683932001s.html#8">Santissimo padre</a>
        </li>
        <li>
          <a href="ghbor683932001s.html#10">Al lettore</a>
        </li>
        <li>
          <a href="ghbor683932001s.html#12">Indice di tutta
          l'opera</a>
        </li>
        <li>
          <a href="ghbor683932001s.html#14">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
