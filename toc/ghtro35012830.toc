<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghtro35012830s.html#6">Paradossi per pratticare la
      prospettiva senza saperla, fiori, per facilitare
      l’intelligenza ...</a>
      <ul>
        <li>
          <a href="ghtro35012830s.html#8">Eccellentissimi
          padroni</a>
        </li>
        <li>
          <a href="ghtro35012830s.html#10">A benigni, e virtuosi
          studenti</a>
        </li>
        <li>
          <a href="ghtro35012830s.html#12">Tavola</a>
        </li>
        <li>
          <a href="ghtro35012830s.html#14">I. Paradossi rispetti
          a' fiori della prospettiva prattica&#160;</a>
        </li>
        <li>
          <a href="ghtro35012830s.html#62">II. Frutti della
          prospettiva prattica</a>
        </li>
        <li>
          <a href="ghtro35012830s.html#134">III. Paradossi overo
          fiori, e frutti di prospettiva prattica</a>
        </li>
        <li>
          <a href="ghtro35012830s.html#195">Tavola prima</a>
        </li>
        <li>
          <a href="ghtro35012830s.html#196">Tavola seconda</a>
        </li>
        <li>
          <a href="ghtro35012830s.html#197">Tavola terza</a>
        </li>
        <li>
          <a href="ghtro35012830s.html#200">Tavola quarta</a>
        </li>
        <li>
          <a href="ghtro35012830s.html#201">Tavola quinta</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
