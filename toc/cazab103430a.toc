<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cazab103430as.html#8">Castelli E Ponti Di Maestro
      Niccola Zabaglia &#160;</a>
      <ul>
        <li>
          <a href="cazab103430as.html#10">[Castelli e Ponti di
          Maestro Niccola Zabaglia]</a>
          <ul>
            <li>
              <a href="cazab103430as.html#10">Alla Santità di
              Nostro Signore Leone XII. Pontefice Massimo</a>
            </li>
            <li>
              <a href="cazab103430as.html#12">Notizie storiche
              della vita e delle opere di Maestro Niccola
              Zabaglia&#160;</a>
            </li>
            <li>
              <a href="cazab103430as.html#42">Discorso per la
              presente edizione&#160;</a>
            </li>
            <li>
              <a href="cazab103430as.html#44">Prefazione della
              prima edizione&#160;</a>
            </li>
            <li>
              <a href="cazab103430as.html#46">Spiegazione delle
              tavole</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="cazab103430as.html#92">Contignationes ac
          pontes Nicolai Zabaglia</a>
          <ul>
            <li>
              <a href="cazab103430as.html#94">Praefatio</a>
            </li>
            <li>
              <a href="cazab103430as.html#96">Tabularum
              explanatio</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="cazab103430as.html#144">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
