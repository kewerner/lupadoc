<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg15549304s.html#3">Forma Urbis Romae</a>
    </li>
    <li>
      <a href="dg15549304s.html#7">XIX.</a>
      <ul>
        <li>
          <a href="dg15549304s.html#8">›Horti: Geta‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#8">›Villa Lante‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#8">›Sant'Onofrio al
          Gianicolo‹ ◉</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg15549304s.html#11">XX.</a>
      <ul>
        <li>
          <a href="dg15549304s.html#11">›Sant'Onofrio al
          Gianicolo‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#11">›Horti: Geta‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#11">›San Giacomo in
          Settignano‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#11">›Santa Maria Regina
          Coeli‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#11">›San Giuseppe alla
          Lungara‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#11">Santa Croce ›Santa Croce
          delle Scalette‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#11">Chiesa della Visitazione
          ›Santa Maria della Visitazione e San Francesco di Sales‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#11">›Sant'Anna dei Bresciani‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#11">›Oratorio del Gonfalone‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Villa Farnesina‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Palazzo Corsini‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Santa Lucia del
          Gonfalone‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Santo Stefano de
          Piscina‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Piazza Sora‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›San Filippo Neri‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Carceri Nuove‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›San Nicolò degli
          Incoronati‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">Collegio Ghisleri ›Liceo
          Ginnasio Virgilio‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Chiesa dello Spirito
          Santo dei Napoletani‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Palazzo Ricci‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Palazzo Capponi‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›San Giovanni Evangelista
          in Ayno‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Sant'Eligio degli
          Orefici‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Santa Maria in
          Monserrato‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Santa Caterina da Siena‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Collegio Inglese‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">San Tommaso Gantuar ›San
          Tommaso di Canterbury‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Santa Caterina della
          Rota‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›San Girolamo della
          Carità‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Santa Brigida‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">Santa Maria del Morte
          ›Santa Maria dell'Orazione e Morte‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Palazzo Falconieri‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Palazzo Farnese‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Piazza Farnese‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Fontane di Piazza
          Farnese‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">Palazzo Pighini ›Palazzo
          del Gallo di Roccagiovine‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Santi Giovanni
          Evangelista e Petronio dei Bolognesi‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Fontana del Mascherone‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›San Salvatore in Onda‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Ponte Sisto‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Via de' Pettinari‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Palazzo della
          Cancelleria‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›San Lorenzo in Damaso‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#12">›Palazzo Spada‹ ◉</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg15549304s.html#15">XXI.</a>
      <ul>
        <li>
          <a href="dg15549304s.html#15">›Palazzo Spada‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›San Lorenzo in Damaso‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›San Pantaleo‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Palazzo Massimo alle
          Colonne‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Campo de' Fiori‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Santa Maria in
          Grottapinta (ex)‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Palazzo Lancellotti
          (piazza Navona)‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Sant'Andrea della Valle‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Teatro di Pompeo‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Portico di Pompeo‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›San Carlo ai Catinari‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Teatro Argentina‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›San Giuliano
          Ospitaliere‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Teatro di Balbo‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›San Salvatore in Campo‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Santa Maria in
          Monticelli‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›San Paolo alla Regola‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Santissima Trinità dei
          Pellegrini‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Palazzo Alberini‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Santi Benedetto e
          Scolastica all'Argentina‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Terme di Agrippa‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Santi Cosma e Damiano
          de' Barbieri‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Santissimo Sudario di
          Nostro Signore Gesù Cristo‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">San Nicolao de Calcarario
          ›San Nicola ai Cesarini‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Sant'Elena dei
          Credenzieri‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Sant'Anna dei Falegnami‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›San Sebastiano de'
          Mercanti‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Palazzo Costaguti‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Santa Maria in
          Publicolis‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">Santa Maria in
          Cacabariis‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Santa Maria del Pianto‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Fontana di Piazza delle
          Cinque Scole‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Piazza Giudea‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›San Lorenzo della
          Craticola‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">Piazza Branca già
          dell'Antella ›Piazza di Branca‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Palazzo Santacroce‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Piazza del Monte di
          Pietà‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Palazzo del Monte di
          Pietà‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Via de' Pettinari‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›San Martinello‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">Chiesa di Santa Teresa e
          ospizio dei Carmelitani ›Santi Teresa e Giovanni della
          Croce dei Carmelitani‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Santa Barbara dei
          Librari‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">Palazzo Orsini Pio
          ›Palazzo Pio Righetti‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Piazza del Pollarola‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Palazzo Nari‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Palazzo Origo‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Palazzo Besso‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#15">›Santa Chiara‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Sante Stimmate di San
          Francesco‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Santa Lucia alle
          Botteghe Oscure‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›San Giovanni della
          Pigna‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Santo Stefano del Cacco‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Santa Marta (Collegio
          Romano)‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Santissimo Nome di Gesù‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Palazzo Altieri‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">Palazzo Petroni ›Palazzo
          Cenci Bolognetti Petroni‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Sant'Andrea in
          Pallacine‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Palazzo Muti Bussi‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">San Salvatore in Pensili
          ›Santo Stanislao dei Polacchi‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Portico di Filippo‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Sant'Ambrogio della
          Massima‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Santa Maria in
          Campitelli‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Palazzo di Venezia‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›San Marco‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›San Giovanni in
          Mercatello‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Fontana di Piazza
          d'Aracoeli‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Santa Maria de Curte‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">Sante Orsola e Catarina
          ›San Niccolò de Funariis‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Palazzo Doria Pamphilj‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">Palazzo
          Gottifredi-Grazioli ›Palazzo Grazioli‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#16">›Santa Caterina dei
          Funari‹ ◉</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg15549304s.html#19">XXII.</a>
      <ul>
        <li>
          <a href="dg15549304s.html#19">›Santa Maria in Aracoeli‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Monumento a Vittorio
          Emanuele II‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Capitolino (Monte)‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Palazzo dei
          Conservatori‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Musei Capitolini‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Palazzo Senatorio‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">Foro di Augusto‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Foro di Traiano‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Colonna Traiana‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Santa Maria di Loreto‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›San Lorenzo de Ascesa ai
          Monti‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Chiesa del Santo Spirito
          alla Colonna Traiana‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Sant'Eufemia al Foro
          Traiano‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›San Niccolò de Columna‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Sant'Urbano ai Pantani‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Santa Maria in Campo‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Sant'Abbaciro de
          Militiis‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">Palazzo Bolognetti
          Torlonia ›Palazzo Bigazzini-Bolognetti poi Torlonia‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Ospedale de' Fornari‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Foro di Cesare‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">San Silvestro de' Caballo
          de' Archioni ›San Silvestro al Quirinale‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">Santa Caterina da Siena
          ›Santa Caterina a Magnanapoli‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Hortus Mirabilis‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Arco dei Pantani‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Palazzo del Grillo‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#19">›Santi Quirico e
          Giulitta‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#20">Villa
          Pamphili-Miollis-Aldobrandini ›Villa Aldobrandini‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#20">›Santi Domenico e Sisto‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#20">›Santa Maria Bagnanapoli‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#20">›Chiesa della Madonna dei
          Monti‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#20">›Santa Maria della
          Concezione delle Cappuccine‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#20">›San Francesco di Paola‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#20">Salvatoris trium Ymaginum
          ›San Salvatore delle Tre Immagini‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#20">›Santi Sergio e Bacco‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#20">›San Bernardino da Siena‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#20">›Santa Maria in
          Monasterio‹ ◉</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg15549304s.html#23">XXIII.</a>
      <ul>
        <li>
          <a href="dg15549304s.html#23">›San Lorenzo in
          Panisperna‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#23">›San Pietro in Vincoli‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#23">›Suburra‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#23">›Santa Maria della
          Purificazione‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#23">›Terme di Traiano‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#23">›Portico di Livia‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#23">Santa Lucia in Orphea
          ›Santa Lucia in Selci‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#23">›Santa Maria Annunziata
          delle Turchine‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#23">Scae Eufemiae
          ›Sant'Eufemia al Vico Patrizio‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#23">Sancti Martini ›San
          Martino ai Monti‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#23">›Santa Maria Maggiore‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#24">›Piazza di Santa Maria
          Maggiore‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#24">›Fontana di Piazza Santa
          Maria Maggiore‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#24">Scae Praxedis ›Santa
          Prassede‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#24">›Sant'Antonio Abate‹
          ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#24">›Ospedale di Sant'Antonio
          Abate‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#24">›Sette Sale‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#24">›Horti Maecenatiani‹
          ◉</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg15549304s.html#27">XXIV.</a>
      <ul>
        <li>
          <a href="dg15549304s.html#27">›Sant'Eusebio‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#27">Villa Peretti Negroni
          Massimi ›Villa Negroni‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#27">Horti Lamiani et Maiani
          ›Horti Lamiani (2)‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#28">›Horti Liciniani‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#28">›Porta Tiburtina‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#28">Ecclesia santae Vibianae
          quae Olympiana iuxta palatium Licinianum ›Palatium
          Liciniani/Licinianum‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#28">Ecclesia santae Vibianae
          ›Santa Bibiana‹ ◉</a>
        </li>
        <li>
          <a href="dg15549304s.html#28">Le Galuzze ›Tempio di
          Minerva Medica‹ ◉</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
