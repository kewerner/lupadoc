<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caber19212820s.html#8">Vita del Cavaliere Gio.
      Lorenzo Bernino scultore, architetto, e pittore</a>
      <ul>
        <li>
          <a href="caber19212820s.html#10">Tavola delle cose più
          notabili</a>
        </li>
        <li>
          <a href="caber19212820s.html#14">Sacra Reale Maestà</a>
        </li>
        <li>
          <a href="caber19212820s.html#18">Vita del Cavalier Gio.
          Lorenzo Bernini</a>
        </li>
        <li>
          <a href="caber19212820s.html#126">Protesta
          dell'autore</a>
        </li>
        <li>
          <a href="caber19212820s.html#130">[Piante]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
