<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501950s.html#6">Stationi delle Chiese di Roma
      per tutta la Quaresima ▣</a>
      <ul>
        <li>
          <a href="dg4501950s.html#8">Girolamo Francini alli
          Lettori</a>
        </li>
        <li>
          <a href="dg4501950s.html#10">Encomiub Beatae
          Virginis</a>
        </li>
        <li>
          <a href="dg4501950s.html#11">Sixtus Papa Quintus</a>
        </li>
        <li>
          <a href="dg4501950s.html#16">Tavola delle stationi et
          reliquie delle Chiese di Roma, che si contengono in
          questo Libro</a>
        </li>
        <li>
          <a href="dg4501950s.html#20">Tavola della Vita et
          martirio de' Santi che sono in questo Libro</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501950s.html#22">[Stationi]</a>
      <ul>
        <li>
          <a href="dg4501950s.html#22">I. ›Santa Sabina‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#27">II. ›San Giorgio in
          Velabro‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#32">III. ›Santi Giovanni e
          Paolo‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#38">IV. ›San Trifone in
          Posterula‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#42">V. ›San Giovanni in
          Laterano‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#49">V. ›San Pietro in Vaticano‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#50">VI. ›San Pietro in Vincoli‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#54">VII. ›Sant'Anastasia‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#57">VIII. ›Santa Maria
          Maggiore‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#65">IX. ›San Lorenzo in
          Panisperna‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#67">X. ›Santi Apostoli‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#70">XI. ›San Pietro in
          Vaticano‹ [2] ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#80">XII. ›Santa Maria in
          Domnica‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#92">XII. ›Santa Maria Maggiore‹
          [2] ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#93">XII. ›San Clemente‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#99">XIV. ›Santa Balbina‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#101">XV. ›Santa Cecilia‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#107">XVI. ›Santa Maria in
          Trastevere‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#113">XVII. ›San Vitale‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#116">[XVIII?] ›Santi Marcellino
          e Pietro‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#119">[XIX?] ›San Lorenzo fuori
          le Mura‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#126">XX. ›San Marco‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#129">XXI. ›Santa Pudenziana‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#132">XXII. ›San Sisto Vecchio‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#135">XXIII. ›Santi Cosma e
          Damiano‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#138">XXIV . ›San Lorenzo in
          Lucina‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#140">XXV. ›Santa Susanna‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#144">XXV. ›Santa Maria degli
          Angeli‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#152">XXVI. ›Santa Croce in
          Gerusalemme‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#163">XXVII. ›Santi Quattro
          Coronati‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#166">XXVIII. ›San Lorenzo in
          Damaso‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#168">XXIX. ›San Paolo fuori le
          Mura‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#176">XXX. ›San Martino ai
          Monti‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#180">XXX. ›San Silvestro in
          Capite‹</a>
        </li>
        <li>
          <a href="dg4501950s.html#185">XXXI. ›Sant'Eusebio‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#187">XXXII. ›San Nicola in
          Carcere‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#191">XXXIII. ›San Pietro in
          Vaticano‹ [3] ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#192">XXXIV. ›San Crisogono‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#196">XXXV. ›Santi Quirico e
          Giulitta‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#198">XXXVI. ›San Marcello al
          Corso‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#201">XXXVII. ›Sant'Apollinare‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#204">XXXVII. ›Monastero di
          Santa Maria Maddalena‹</a>
        </li>
        <li>
          <a href="dg4501950s.html#208">XXXVIII. ›Santo Stefano
          Rotondo‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#213">XXXIX. ›San Giovanni a
          Porta Latina‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#215">XL. ›San Giovanni in
          Laterano‹ [2]</a>
        </li>
        <li>
          <a href="dg4501950s.html#218">XLI. ›Santa Prassede‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#221">XLII. ›Santa Prisca‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#224">XLIII. ›Santa Maria
          Maggiore‹ [2]</a>
        </li>
        <li>
          <a href="dg4501950s.html#224">XLIV. ›San Giovanni in
          Laterano‹ [3]</a>
        </li>
        <li>
          <a href="dg4501950s.html#224">XLV. ›Santa Croce in
          Gerusalemme‹ [2]</a>
        </li>
        <li>
          <a href="dg4501950s.html#225">XLV. ›Santa Maria degli
          Angeli‹ [2] ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#228">XLVI. ›San Giovanni in
          Laterano‹ [4]</a>
        </li>
        <li>
          <a href="dg4501950s.html#228">XLVII. ›Santa Maria
          Maggiore‹ [3]</a>
        </li>
        <li>
          <a href="dg4501950s.html#228">XLVII. ›Santa Maria degli
          Angeli‹ [3]</a>
        </li>
        <li>
          <a href="dg4501950s.html#229">XLVIII ›San Pietro in
          Vaticano‹ [4]</a>
        </li>
        <li>
          <a href="dg4501950s.html#229">XLIX. ›San Paolo fuori le
          Mura‹ [2]</a>
        </li>
        <li>
          <a href="dg4501950s.html#229">L. ›San Lorenzo fuori le
          Mura‹ [2]</a>
        </li>
        <li>
          <a href="dg4501950s.html#230">LI. ›Santi Apostoli‹
          [2]</a>
        </li>
        <li>
          <a href="dg4501950s.html#234">LII. ›Pantheon‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#236">LII. ›Santa Maria sopra
          Minerva‹</a>
        </li>
        <li>
          <a href="dg4501950s.html#239">LIII. ›San Giovanni in
          Laterano‹ [5]</a>
        </li>
        <li>
          <a href="dg4501950s.html#240">LIV. ›San Pancrazio‹ ▣</a>
        </li>
        <li>
          <a href="dg4501950s.html#243">La chiesa di Santa Maria e
          San Gregorio in Vallicella ›Chiesa Nuova‹ ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
