<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501883s.html#6">Stationi delle Chiese di Roma,
      per tutta la Quaresima [...]</a>
      <ul>
        <li>
          <a href="dg4501883s.html#8">Gieronimo Francini alli
          lettori</a>
        </li>
        <li>
          <a href="dg4501883s.html#10">Encomium Beatae
          Virginis</a>
        </li>
        <li>
          <a href="dg4501883s.html#11">Sixtus Papa quintus. Ad
          futuram rei memoriam</a>
        </li>
        <li>
          <a href="dg4501883s.html#16">Tavola delle Stationi, et
          reliquie delle Chiese di Roma, che si contengono in
          questo Libro</a>
        </li>
        <li>
          <a href="dg4501883s.html#20">Tavola della vita, et
          martirio de' Santi, che sono in questo Libro</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501883s.html#22">[Stazioni e corrispondenti
      giornate]</a>
      <ul>
        <li>
          <a href="dg4501883s.html#22">I. ›Santa Sabina‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#27">II. ›San Giorgio in
          Velabro‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#32">III. ›Santi Giovanni e
          Paolo‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#38">IV. ›San Trifone in
          Posterula‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#42">V. ›San Giovanni in
          Laterano‹ [1] ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#48">V. ›San Pietro in Vaticano‹
          [1]</a>
        </li>
        <li>
          <a href="dg4501883s.html#49">VI. ›San Pietro in
          Vincoli‹</a>
        </li>
        <li>
          <a href="dg4501883s.html#53">VII. ›Sant'Anastasia‹</a>
        </li>
        <li>
          <a href="dg4501883s.html#56">VIII. ›Santa Maria
          Maggiore‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#64">IX. ›San Lorenzo in
          Panisperna‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#66">X. ›Santi Apostoli‹ [1]
          ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#69">XI. ›San Pietro in
          Vaticano‹ [2] ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#79">XII. ›Santa Maria in
          Domnica‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#91">XII. ›Santa Maria Maggiore‹
          [2] ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#92">XIII. ›San Clemente‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#98">XIV. ›Santa Balbina‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#100">XV. ›Santa Cecilia in
          Trastevere‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#106">XVI. ›Santa Maria in
          Trastevere‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#112">XVII. ›San Vitale‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#115">XVIII. ›Santi Marcellino e
          Pietro‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#118">XIX. ›San Lorenzo fuori le
          Mura‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#125">XX. ›San Marco‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#128">XXI. ›Santa Pudenziana‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#131">XXII. ›San Sisto Vecchio‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#134">XXIII. ›Santi Cosma e
          Damiano‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#137">XXIV. ›San Lorenzo in
          Lucina‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#139">XXV. ›Santa Susanna‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#143">XXV. ›Santa Maria degli
          Angeli‹ [1] ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#151">XXVI. ›Santa Croce in
          Gerusalemme‹ [1] ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#162">XXVII. ›Santi Quattro
          Coronati‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#165">XXVIII. ›San Lorenzo in
          Damaso‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#167">XXIX. ›San Paolo fuori le
          Mura‹ [1] ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#175">XXX. Santi Silvestro e
          Martino ai Monti ›San Martino ai Monti‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#184">XXXI. ›Sant'Eusebio‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#186">XXXII. ›San Nicola in
          Carcere‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#190">XXXIII. ›San Pietro in
          Vaticano‹ [3] ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#191">XXXIV. ›San Crisogono‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#195">XXXV. ›Santi Quirico e
          Giulitta‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#197">XXXVI. ›San Marcello al
          Corso‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#200">XXXVII. ›Sant'Apollinare‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#203">XXXVII. Santa Maria
          Maddalena delle Convertite ›Monastero di Santa Maria
          Maddalena‹</a>
        </li>
        <li>
          <a href="dg4501883s.html#207">XXXVIII. ›Santo Stefano
          Rotondo‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#212">XXXIX. ›San Giovanni a
          Porta Latina‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#214">XL. ›San Giovanni in
          Laterano‹ [2]</a>
        </li>
        <li>
          <a href="dg4501883s.html#217">XLI. ›Santa Prassede‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#220">XLII. ›Santa Prisca‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#223">XLIII. ›Santa Maria
          Maggiore‹ [3]</a>
        </li>
        <li>
          <a href="dg4501883s.html#223">XLIV. ›San Giovanni in
          Laterano‹ [3]</a>
        </li>
        <li>
          <a href="dg4501883s.html#223">XLV. ›Santa Croce in
          Gerusalemme‹ [2]</a>
        </li>
        <li>
          <a href="dg4501883s.html#224">XLV. ›Santa Maria degli
          Angeli‹ [2] ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#227">XLVI. ›San Giovanni in
          Laterano‹ [4]</a>
        </li>
        <li>
          <a href="dg4501883s.html#227">XLVII. ›Santa Maria
          Maggiore‹ [4]</a>
        </li>
        <li>
          <a href="dg4501883s.html#227">XLVII. ›Santa Maria degli
          Angeli‹ [3]</a>
        </li>
        <li>
          <a href="dg4501883s.html#228">XLVIII. ›San Pietro in
          Vaticano‹ [4]</a>
        </li>
        <li>
          <a href="dg4501883s.html#228">XLIX. ›San Paolo fuori le
          Mura‹ [2]</a>
        </li>
        <li>
          <a href="dg4501883s.html#228">L. ›San Lorenzo fuori le
          Mura‹ [2]</a>
        </li>
        <li>
          <a href="dg4501883s.html#229">LI. ›Santi Apostoli‹
          [2]</a>
        </li>
        <li>
          <a href="dg4501883s.html#233">LII. Santa Maria Rotonda
          ›Pantheon‹ ▣</a>
        </li>
        <li>
          <a href="dg4501883s.html#235">LII. ›Santa Maria sopra
          Minerva‹</a>
        </li>
        <li>
          <a href="dg4501883s.html#238">LIII. ›San Giovanni in
          Laterano‹ [5]</a>
        </li>
        <li>
          <a href="dg4501883s.html#239">LIV. ›San Pancrazio‹ ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
