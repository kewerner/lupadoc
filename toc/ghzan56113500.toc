<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghzan56113500s.html#6">Orazione del Signor
      Francesco Maria Zanotti in lode della pittura, della
      scoltura, e dell'architettura</a>
      <ul>
        <li>
          <a href="ghzan56113500s.html#8">Francesco Tibaldi A i
          Lettori</a>
        </li>
        <li>
          <a href="ghzan56113500s.html#16">Orazione prima</a>
        </li>
        <li>
          <a href="ghzan56113500s.html#36">Orazione II.</a>
        </li>
        <li>
          <a href="ghzan56113500s.html#60">Orazione III.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
