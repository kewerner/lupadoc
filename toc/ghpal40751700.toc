<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghpal40751700s.html#6">I quattro libri
      dell'architettura</a>
      <ul>
        <li>
          <a href="ghpal40751700s.html#8">[Dedica dell'autore al
          Conte Giacomo Angaranno] Al molto magnifico mio Signor
          osservandissimo, il Signor Conte Giacomo Angaranno</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghpal40751700s.html#10">[Text]</a>
      <ul>
        <li>
          <a href="ghpal40751700s.html#10">Il primo Libro</a>
          <ul>
            <li>
              <a href="ghpal40751700s.html#10">Proemio à i
              Lettori</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#11">I. Quali cose
              deono considerarsi, e prepararsi avanti che al
              fabricar si pervenga&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#12">II. Dei
              legnami&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#12">III. Delle
              pietre</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#13">IIII. [sic]
              Dell'arena&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#13">V. Della calce, e
              modo d'impastarla&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#14">VI. Dei
              metalli&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#15">VII. Delle qualità
              del terreno, ove s'hanno da poner le
              fondamenta&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#16">VIII. Delle
              fondamenta&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#16">IX. Delle maniere
              de' muri ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#19">X. Del modo che
              tenevano gli antichi nel far gli edificij di
              pietra&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#19">XI. Delle
              diminutioni de' muri, et delle parti loro</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#20">XII. De' cinque
              ordini, che usarono gli antichi</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#20">XIII. Della
              gonfiezza, e diminutione delle colonne, de gli
              intercolumnij, e de' pilastri ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#21">XIIII. [sic]
              Dell'ordine toscano ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#27">XV. Dell'ordine
              dorico ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#33">XVI. Dell'ordine
              ionico ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#42">XVII. Dell'ordine
              corinthio ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#49">XVIII. Dell'ordine
              composto ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#56">XIX. Dei
              piedestili</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#56">XX. De gli
              abusi</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#57">XXI. Delle logge,
              dell'entrate, delle sale, e delle stanze: et della
              forma loro</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#58">XXII. De'
              pavimenti, e de' soffittati</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#58">XXIII.
              Dell'altezza delle stanze ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#59">XXIIII. [sic]
              Delle maniere de' volti ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#60">XXV. Delle misure
              e delle porte, e delle finestre</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#60">XXVI. De gli
              ornamenti delle porte, e delle finestre ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#65">XXVII. De'
              camini</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#65">XXVIII. Delle
              scale, e varie maniere di quelle, e del numero, e
              grandezza de' gradi ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#72">XXIX. De i
              coperti</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghpal40751700s.html#74">Il secondo Libro</a>
          <ul>
            <li>
              <a href="ghpal40751700s.html#76">I. Del decoro, o'
              convenienza, che si deve osservar nelle fabriche
              private</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#76">II. Del
              compartimento delle stanze, e d'altri luoghi</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#77">III. De i disegni
              delle case della città ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#97">IIII. [sic]
              Dell'atrio toscano ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#100">V. Dell'atrio di
              quattro colonne ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#102">VI. Dell'atrio
              corinthio ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#106">VII. Dell'atrio
              testugginato, e della casa privata de gli antichi
              Romani ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#109">VIII. Delle sale
              di quattro colonne ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#111">IX. Delle sale
              corinthie ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#114">X. Delle sale
              egittie ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#116">XI. Delle case
              private de' greci ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#118">XII. Del sito da
              eleggersi per le fabriche di villa&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#119">XIII. Del
              compartimento delle case di villa&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#120">XIIII. [sic] De i
              disegni delle case di Villa di alcuni nobili ventiani
              ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#129">XV. De i disegni
              delle case di Villa di alcuni gentil'huomini di Terra
              Ferma ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#142">XVI. Della casa
              di villa de gli antichi ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#144">XVII. Di alcune
              inventioni secondo diversi siti ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghpal40751700s.html#154">Il terzo libro</a>
          <ul>
            <li>
              <a href="ghpal40751700s.html#156">[Dedica
              dell'autore a al principe Emanuel Filiberto] Al
              serenissimo e magnanimo Pricipe Emanuel Filiberto
              Duca di Savoia, etc.</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#158">Proemio a i
              lettori</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#160">I. Delle vie</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#160">II. Del
              compartimento delle vie, dentro della città</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#161">III. Delle vie
              fuori della città ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#164">IIII. [sic] Di
              quello, che nel fabricare i ponti si deve osservare,
              e del sito che si deve eleggere</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#164">V. De i ponti di
              legno, et di quelle avermenti, che nell'edificarli si
              devono havere&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#165">VI. Del ponte
              ordinato da Ceare sopra il Rheno ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#168">VII. Del ponte
              del Cismone ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#169">VIII. Di tre
              altre inventioni secondo le quali si ponno fare i
              ponti di legno senza porre altrimenti pali nel fiume
              ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#172">IX. Del ponte di
              Bassano ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#173">X. De i ponti di
              pietra, e di quello che nell'edificarli si deve
              osservare&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#174">XI. Di alcuni
              ponti celebri edificati dagli antichi, e de' disegni
              del ponte di Rimino ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#177">XII. Del ponte di
              Vicenza, ch'è sopra il Bacchiglione ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#178">XIII. Di un ponte
              di pietra di mia inventione ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#181">XIIII. [sic] Di
              un'altro ponte di mia inventione ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#183">XV. Del ponte di
              Vicenza, ch'è sopra il Rerone ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#184">XVI. Delle
              piazze, e de gli edifichi che intorno à quelle si
              sanno</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#185">XVII. Delle
              piazze de i Greci ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#188">XVIII. Delle
              piazze de' Latini ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#191">XIX. Delle
              basiliche antiche ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#194">XX. Delle
              basiliche de' nostri tempi ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#197">XXI. Delle
              palestre, e de i xisti de' Greci ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghpal40751700s.html#202">Il quattro libro</a>
          <ul>
            <li>
              <a href="ghpal40751700s.html#204">Proemio a i
              lettori</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#206">I. Del sito, che
              si deve eleggere per edificarvi i tempii</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#207">II. Delle forme
              de' tempii et del decoro, che in quelli si deve
              osservare</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#208">III. De gli
              aspetti de i tempii</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#209">IIII. [sic] Di
              cinque specie di tempii&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#210">V. Del
              compartimento dei tempii</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#212">VI. De i disegni
              di alcuni tempii antichi che sono in Roma, e prima di
              quello della Pace ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#216">VII. Del Tempio
              di Marte Vendicatore ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#224">VIII. Del Tempio
              di Nerva Traiano ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#231">IX. Del Tempio
              d'Antonino e di Faustina ›Tempio di Antonino e
              Faustina‹ ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#237">X. De i tempi del
              sole, e della luna ›Tempio del Sole e della Luna
              (Palatino)‹ ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#240">XI. Del tempio
              vulgarmente detto Le Galluce Capitolo ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#242">XII. Del Tempio
              di Giove ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#249">XIII. Del Tempio
              della Fortuna Virile ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#253">XIIII. [sic] Del
              Tempio di Vesta ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#256">XV. Del Tempio di
              Marte ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#262">XVI. Del
              Battesimo di Costantino ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#265">XVII. Del Tempio
              di Bramante ›Tempietto di Bramante‹ ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#268">XVIII. Del Tempio
              di Giove Statore ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#271">XIX. Del Tempio
              di Giove Tonante ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#274">XX. Del Pantheon
              hoggi detto La Ritonda ›Pantheon‹ ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#286">XXI. De i disegni
              di alcuni tempii, che sono fuori di Roma, e per
              Italia, e prima del Tempio di Bacco ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#289">XXII. Del tempio
              i cui vestigi si veggono vicino alla Chiesa di Santo
              Sebastiano sopra la Via Appia ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#291">XXIII. Del Tempio
              di Vesta ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#296">XIIII. [sic] Del
              Tempio di Castore, e di Polluce ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#299">XXV. Del Tempio
              ch'è sotto Trevi ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#304">XXVI. Del Tempio
              di Scisi ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#308">XXVII. De i
              disegni di alcuni tempii, che sono fuori d'Italia, e
              prima de' due Tempij di Pola ▣&#160;</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#312">XXVIII. Di due
              tempii di Nimes, e prima di quello, ch'è detto la
              Mazon Quaree ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#319">XIX. Dell'altro
              Tempio di Nimes ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#325">XXX. Di due
              tempii di Roma, e prima di quello della Concordia
              ▣</a>
            </li>
            <li>
              <a href="ghpal40751700s.html#329">XXXI. Del Tempio
              di Nettuno ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
