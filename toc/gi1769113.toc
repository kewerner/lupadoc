<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="gi1769113s.html#10">Serie degli Uomini i più
      illustri nella pittura, scultura, e architettura con i loro
      elogi, e ritratti […] tomo primo</a>
      <ul>
        <li>
          <a href="gi1769113s.html#12">Illustrissimo e
          clarissimo signore</a>
        </li>
        <li>
          <a href="gi1769113s.html#18">Avviso al lettore</a>
        </li>
        <li>
          <a href="gi1769113s.html#20">Prefazione</a>
        </li>
        <li>
          <a href="gi1769113s.html#42">Elogi contenuti nel
          presente volume, e sono degli appresso autori</a>
        </li>
        <li>
          <a href="gi1769113s.html#46">Elogio di Arnolfo di
          Lapo</a>
        </li>
        <li>
          <a href="gi1769113s.html#52">Elogio di Giovann detto
          Cimabue</a>
        </li>
        <li>
          <a href="gi1769113s.html#58">Elogio di Buonamico
          Buffalmacco</a>
        </li>
        <li>
          <a href="gi1769113s.html#64">Elogio di Giotto di
          Bondone</a>
        </li>
        <li>
          <a href="gi1769113s.html#72">Elogio di Pietro
          Cavallini</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="gi1769113s.html#210">Serie degli Uomini i più
      illustri nella pittura, scultura, e architettura con i loro
      elogi, e ritratti […] tomo secondo</a>
      <ul>
        <li>
          <a href="gi1769113s.html#212">Eccellenza</a>
        </li>
        <li>
          <a href="gi1769113s.html#222">Gli autori dell'opera a
          chi legge</a>
        </li>
        <li>
          <a href="gi1769113s.html#228">Elogio di Filippo
          Brunelleschi scultore, ed architetto Fiorentino</a>
        </li>
        <li>
          <a href="gi1769113s.html#240">Elogio di Dello</a>
        </li>
        <li>
          <a href="gi1769113s.html#244">Elogio di Paolo
          Uccello</a>
        </li>
        <li>
          <a href="gi1769113s.html#250">Elogio d'Iacopo della
          Querci</a>
        </li>
        <li>
          <a href="gi1769113s.html#256">Elogio di Pietro della
          Francesca</a>
        </li>
        <li>
          <a href="gi1769113s.html#262">Elogio di Gentila da
          Fabriano</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="gi1769113s.html#386">Serie degli Uomini i più
      illustri nella pittura, scultura, e architettura con i loro
      elogi, e ritratti […] tomo terzo</a>
      <ul>
        <li>
          <a href="gi1769113s.html#388">Illustrissimo
          signore</a>
        </li>
        <li>
          <a href="gi1769113s.html#398">Elogio di Luca
          Signorelli</a>
        </li>
        <li>
          <a href="gi1769113s.html#406">Elogio di Antonio
          Rossellino</a>
        </li>
        <li>
          <a href="gi1769113s.html#412">Elogio di Gherardo
          Miniatore</a>
        </li>
        <li>
          <a href="gi1769113s.html#418">Elogio di Benedetto da
          Maiano</a>
        </li>
        <li>
          <a href="gi1769113s.html#426">Elogio di Bramante
          d'Urbino</a>
        </li>
        <li>
          <a href="gi1769113s.html#436">Elogio di Leonardo da
          Vinci</a>
        </li>
        <li>
          <a href="gi1769113s.html#452">Elogio di Domenico
          Ghirlandajo</a>
        </li>
        <li>
          <a href="gi1769113s.html#462">Elogio di Andrea da
          Fiesole&#160;</a>
        </li>
        <li>
          <a href="gi1769113s.html#470">Elogio di Francesco
          Francia</a>
        </li>
        <li>
          <a href="gi1769113s.html#480">Elogio di Liberale
          Veronese</a>
        </li>
        <li>
          <a href="gi1769113s.html#492">Elogio di Ercole da
          Ferrara</a>
        </li>
        <li>
          <a href="gi1769113s.html#496">Elogio di Andrea
          Mantegna</a>
        </li>
        <li>
          <a href="gi1769113s.html#504">Elogio di Bernardino
          Pinturicchio</a>
        </li>
        <li>
          <a href="gi1769113s.html#514">Elogio di Giuliano Das
          Gallo</a>
        </li>
        <li>
          <a href="gi1769113s.html#522">Elogio di Baccio da
          Montelupo</a>
        </li>
        <li>
          <a href="gi1769113s.html#528">Elogio di Desiderio da
          Settignano</a>
        </li>
        <li>
          <a href="gi1769113s.html#534">Elogio di Lorenzo di
          Credi</a>
        </li>
        <li>
          <a href="gi1769113s.html#540">Elogio di Simone detto
          il Cronaca</a>
        </li>
        <li>
          <a href="gi1769113s.html#552">Elogio di Mino da
          Fiesole</a>
        </li>
        <li>
          <a href="gi1769113s.html#558">Elogio di Filippo
          Lippi</a>
        </li>
        <li>
          <a href="gi1769113s.html#564">Elogio di Baccio
          d'Agnolo</a>
        </li>
        <li>
          <a href="gi1769113s.html#572">Elogio di Andrea
          Contucci</a>
        </li>
        <li>
          <a href="gi1769113s.html#582">Elogio di Baccio della
          Porta</a>
        </li>
        <li>
          <a href="gi1769113s.html#588">Elogio di Vincenzio da
          San Gimignano</a>
        </li>
        <li>
          <a href="gi1769113s.html#594">Elogio di Vittore
          Carpaccio</a>
        </li>
        <li>
          <a href="gi1769113s.html#600">Elogi Autori</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
