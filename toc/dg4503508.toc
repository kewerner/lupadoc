<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503508s.html#8">Roma Ampliata, E Rinovata, O Sia
      Nuova Descrizione Dell’Antica, E Moderna Città Di Roma, E Di
      Tutti Gli Edifizj Notabili Che Sono In Essa</a>
      <ul>
        <li>
          <a href="dg4503508s.html#10">Giornata prima</a>
        </li>
        <li>
          <a href="dg4503508s.html#31">Giornata seconda</a>
        </li>
        <li>
          <a href="dg4503508s.html#41">Giornata terza</a>
        </li>
        <li>
          <a href="dg4503508s.html#54">Giornata quarta</a>
        </li>
        <li>
          <a href="dg4503508s.html#68">Giornata quinta</a>
        </li>
        <li>
          <a href="dg4503508s.html#93">Giornata sesta</a>
        </li>
        <li>
          <a href="dg4503508s.html#118">Giornata settima</a>
        </li>
        <li>
          <a href="dg4503508s.html#149">Giornata ottava</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
