<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="de8504200s.html#8">Il carnevale di Roma</a>
      <ul>
        <li>
          <a href="de8504200s.html#10">[Descrizione delle
          Tavole]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="de8504200s.html#12">[Tavole]</a>
      <ul>
        <li>
          <a href="de8504200s.html#12">I. ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#14">II. Ballo nella strada
          ▣&#160;</a>
        </li>
        <li>
          <a href="de8504200s.html#16">III. ›Piazza del Popolo‹
          ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#18">IV. ›Piazza del Popolo‹
          ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#20">V. ›Piazza del Popolo‹
          ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#22">VI. ›Piazza del Popolo‹
          ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#24">VII. Il Corso ›Via del
          Corso‹ ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#26">VIII. Il Corso ›Via del
          Corso‹ ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#28">IX. Il Corso ›Via del
          Corso‹ ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#30">X. Il Corso ›Via del Corso‹
          ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#32">XI. Corsa dei Cavalli
          ▣&#160;</a>
        </li>
        <li>
          <a href="de8504200s.html#34">XII. Corsa dei Cavalli
          ▣&#160;</a>
        </li>
        <li>
          <a href="de8504200s.html#36">XIII. Corsa dei Cavalli
          ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#38">XIV. I Moccoletti ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#40">XV. I Moccoletti ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#42">XVI. I Moccoletti ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#44">XVII. I Moccoletti ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#46">XVIII. Il Festino ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#48">XIX. Il Festino ▣</a>
        </li>
        <li>
          <a href="de8504200s.html#50">XX. Fine del Carnevale
          ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
