<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efae523190s.html#6">Il fonte pubblico di Faenza e
      la descrizione d’ogni sua parte</a>
      <ul>
        <li>
          <a href="efae523190s.html#8">Agl'illustrissimi
          Signori</a>
        </li>
        <li>
          <a href="efae523190s.html#10">Illustrissimi Sigg. Sigg.
          e Proni Colmi</a>
        </li>
        <li>
          <a href="efae523190s.html#14">A chi legge</a>
        </li>
        <li>
          <a href="efae523190s.html#16">Dell'acqua e sue
          prerogative</a>
        </li>
        <li>
          <a href="efae523190s.html#32">I. Del principio e
          origine di detto fonte</a>
        </li>
        <li>
          <a href="efae523190s.html#35">II. Dell'ordine tenuto
          dall'Architetto in detta Fabbrica</a>
        </li>
        <li>
          <a href="efae523190s.html#38">III. Delle parti
          neccessarie, che compongono e mantengono detto Fonte</a>
        </li>
        <li>
          <a href="efae523190s.html#44">IV. Dichiarazione
          particolare di detta Fabbrica, con l'uso d'ogni sua
          parte</a>
        </li>
        <li>
          <a href="efae523190s.html#102">V. Dell'acqua del
          ritorno</a>
        </li>
        <li>
          <a href="efae523190s.html#108">VI. Avvertimenti intorno
          all'assistenza di detto Fonte</a>
          <ul>
            <li>
              <a href="efae523190s.html#112">Tavola</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="efae523190s.html#118">VII. Memorie, Regole, e
          Diligenze da usarsi da' Signori Deputati del Fonte
          Pubblico</a>
        </li>
        <li>
          <a href="efae523190s.html#121">VIII. Capitoli da
          osservarsi dal Soprastante del Fonte Pubblico&#160;</a>
        </li>
        <li>
          <a href="efae523190s.html#125">IX. Altri comodi,
          servizi, e utili, che potrebbe aver la Città da questo
          Fonte, otlre a quelli, che presentemente vi sono</a>
        </li>
        <li>
          <a href="efae523190s.html#128">Appendice che contiene
          le principali cognizioni, che debbe avere
          l'architetto</a>
          <ul>
            <li>
              <a href="efae523190s.html#130">I. Delli Fonti, e
              loro origine, e del modo, ond'escono dalla Terra</a>
            </li>
            <li>II. Delle Acquilegie, overo segni, che indicano
            l'acque sotterranee</li>
            <li>
              <a href="efae523190s.html#146">III. Come si
              fabbrichi la Sorgente, che debbe raccogliere l'acque
              per il Fonte da farsi</a>
            </li>
            <li>
              <a href="efae523190s.html#148">IV. Che cose sia
              livellazione, e come si faccia</a>
            </li>
            <li>
              <a href="efae523190s.html#178">V. Delli stromenti
              atti alla livellazione, e della loro fabbrica ed
              uso</a>
            </li>
            <li>
              <a href="efae523190s.html#181">VI. Come si debba
              praticare la livellazione</a>
            </li>
            <li>
              <a href="efae523190s.html#191">VII. Della
              necessaria pendenza, che si debbe dare agli alvei, o
              acquedotti, acciocchè l'acqua possa liberamente
              correre</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="efae523190s.html#196">Indice delle cose
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
