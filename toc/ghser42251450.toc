<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghser42251450s.html#10">Il primo libro
      d’Architettura, di Sabastiano Serlio, Bolognese</a>
      <ul>
        <li>
          <a href="ghser42251450s.html#10">Il primo libro
          d'Architettura [di Geometria]</a>
        </li>
        <li>
          <a href="ghser42251450s.html#66">Il secondo libro di
          Perspettiva</a>
        </li>
        <li>
          <a href="ghser42251450s.html#168">Il terzo libro, nel
          qual si figurano, e descrivano le antiquità di Roma, e le
          altre che sono in Italia, e fuori de Italia</a>
          <ul>
            <li>
              <a href="ghser42251450s.html#171">Tavola de le cose
              aggiunte in questa seconda edizione</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghser42251450s.html#324">Regole generali di
          architettura sopra le cinque de gli edifici, cioe,
          thoscano, dorico, ionico, corinthio, e composito, con gli
          essempi dell’antiquita, che, par la maggior parte
          concordano con la dottrina di Vitruvio [il quarto
          libro]</a>
        </li>
        <li>
          <a href="ghser42251450s.html#476">Il quinto libro, nel
          quale se tratta de diverse forme de Tempij sacri secondo
          il costume christiano, et al modo antico&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
