<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="epis164100s.html#8">Lettere pittoriche sul Campo
      Santo di Pisa</a>
      <ul>
        <li>
          <a href="epis164100s.html#10">Avvertimento
          dell'Editore</a>
        </li>
        <li>
          <a href="epis164100s.html#16">Al Signore Cavalier
          Ippolito Pindemonte</a>
        </li>
        <li>
          <a href="epis164100s.html#22">Al Signore Cavalier Gio.
          Gherardo de' Rossi</a>
        </li>
        <li>
          <a href="epis164100s.html#34">Risposta del Signore
          Cavalier de' Rossi</a>
        </li>
        <li>
          <a href="epis164100s.html#42">Allo stesso Signore
          Cavalier de' Rossi</a>
        </li>
        <li>
          <a href="epis164100s.html#66">Risposta del medesimo
          Signore Cavalier de' Rossi</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
