<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="fa1703810c2s.html#8">Voyage Pittoresque ou
      Description des Royaumes de Naples et de Sicile seconde
      Partie du premier Volume</a>
      <ul>
        <li>
          <a href="fa1703810c2s.html#10">Avant-Propos</a>
        </li>
        <li>
          <a href="fa1703810c2s.html#14">Ordre des Fleurons et
          Vignettes</a>
        </li>
        <li>
          <a href="fa1703810c2s.html#16">Table des Chapitres et
          Articles différens contenus dans ce second Volume</a>
        </li>
        <li>
          <a href="fa1703810c2s.html#20">Explication des
          Fleurons, Vignettes et Ornemens répandus dans ce
          Volume</a>
        </li>
        <li>
          <a href="fa1703810c2s.html#38">VII. Tableaux et
          Peintures antiques d'Herculanum. Choix fait dans la
          Collection de ces Peintures, qui est conservée au Palais
          du Roi de Naples a Portici</a>
          <ul>
            <li>
              <a href="fa1703810c2s.html#40">De la Découverte
              d'Herculanum, avec un Détail sommaire de ses
              différentes Antiquités</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#47">Peintures Antiques
              d'Herculanum, Conservées dans le Museum de Portici
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#48">Les Centaures,
              peinture antique d'Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#51">1. La Marchande
              d'Amours d'Herculanum 2. Peinture Antique trouvée à
              Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#52">La Marchande
              d'Amours d'Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#55">Peintures Antiques
              d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#56">Le Repas
              antique</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#59">Arabesques et
              Peintures antiques d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#60">Arabesques
              d'Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#61">Vases et Corbeilles
              de Fruits</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#62">Peintures antiques
              d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#65">Peintures antiques
              d'Herculanum conservées dans le Museum de Portici
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#66">Concert et Musique
              antique</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#67">Danseurs de
              Corde</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#68">Peintures antiques
              d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#71">Peintures antiques
              découvertes à Herculanum et conservées dans le Museum
              de Portici ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#72">Scènes comiques</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#75">Arabesques et
              Peintures antiques d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#77">Peintures antiques
              découvertes à Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#78">Arabesques
              Égyptiens</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#79">Danseuses
              d'Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#80">Peintures antiques
              découvertes à Herculanum et conservées dans le Museum
              de Portici ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#83">Peintures antiques
              d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#84">Frises ou
              bas-reliefs peints à Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#85">Sacrifices et
              Cérémonies Égyptiennes</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#86">Peintures antiques
              d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#89">Le Silène</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#90">Peintures antiques
              d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#93">Peintures antiques
              d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#94">La Bacchante</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c2s.html#96">VIII. Antiquités
          d'Heraculanum. Statues, Vases, Autels, Trépieds, Lampes
          antiques, Meubles et Fragmens divers</a>
          <ul>
            <li>
              <a href="fa1703810c2s.html#98">Statues, Manuscrits
              Grecs, et Fragmens antiques d'Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#103">Statues Équestres
              des Consuls Nonius Balbus</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#104">Statues Équestres
              des Consuls Marcus-Nonius Balbus ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#109">Statues de Bronze
              tirées d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#110">Bronzes et Statues
              antiques d'Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#113">Bronzes et Statues
              d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#114">Statues antiques
              d'Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#117">Vases, Trépieds,
              Candélabres de Bronze, et autres Meubles antiques</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#118">Differents Vases,
              Meubles, Autels et Trépieds ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#120">Tables, Vases ou
              Coupes de differentes formes decouvertes à Herculanum
              et Conservés dans le Muesum de Portici ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#123">Lampes antiques,
              Trépieds et Instruments de Musique trouvés à
              Herculanum et Conservés dans le Museum de Portici
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#124">Instrumens de
              Musique, Autels et Trépieds antiques</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#127">Lampes antiques,
              Vases et Meubles d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#128">Lampes antiques,
              Vases et Meubles d'Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#129">Patères, Vases et
              Lampes antiques</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#130">Trépieds, Vases et
              Lampes antiques ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#133">Lampes antiques,
              Meubles et Fragments divers ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#135">Differentes
              Statues et Fragments de Bronze tirés d'Herculanum
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#136">Bronzes et
              Fragemens antiques</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#139">Phalles ou
              Phallums trouvés à Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#140">Phalles ou
              Phallums trouvés à Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#143">Transport des
              Antiquités d'Herculanum au Palais des Ètudes ou
              Museum de Naples</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#144">Transport des
              Antiquités d'Herculanum, du Museum de Portici au
              Palais des Études à Naples ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c2s.html#150">IX. Théâtre
          d'Herculanum. Élevation et Plan géométral de ce Théâtre,
          avec quelques Détails sur les différens Spectacles des
          anciens Romains</a>
          <ul>
            <li>
              <a href="fa1703810c2s.html#152">Du Théâtre
              d'Herculanum</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#154">Coupes et
              Elevations du Théâtre d'Herculanum ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#157">Plan du Théâtre
              d'Herculanum près de Naples decouvert en l'Année 1783
              ◉</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#160">Des Cirques des
              Romains</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#166">Plan géométral du
              Cirque de Caracalla</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#168">Plan géométral du
              Cirque de l'Empereur Caracalla ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#175">Elevations, Coupes
              et Plans du Cirque de Caracalla ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#176">Coupe et Élévation
              du Cirque de Caracalla</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#177">Bas-Reliefs
              antiques représentants les Courses des Chars dans le
              Cirque</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#178">Bas-Reliefs de
              Marbre, antiques, où sont representées des Courses
              des Chars dans le Cirques des Anciens Romains ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#183">Obélisques
              Egyptiens ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#184">Sur les Obélisques
              des Cirques</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#189">Peinture antique,
              Pierres gravées et autres Monumens qui ont Rapport
              aux Cirques</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#190">Peinture antique
              trouvée dans le Cirque de Caracalla à Rome ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#193">Des Théâtres des
              Romains</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#201">Des Masques
              scèniques</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#205">Des Acteurs, des
              Pantomimes et des Représentations théâtrales des
              Anciens</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#210">Des
              Amphithéâtres</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#212">Des Naumachies</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#214">Vue et Plan d'une
              Naumachie ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#219">Fragments
              antiques, Bronzes et Pierres gravées, representants
              différents Caracteres de Masques Scèniques ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#220">Supplément.
              Fragmens antiques, Bronzes et Pierres gravées
              représentants différens Caractères de Masques
              Scèniques</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#223">Monumens antiques
              ayant Rapport aux Représentations théâtrales des
              Anciens</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#224">Monumens antiques
              ayant Rapport aux Représentations théâtrales des
              Anciens ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c2s.html#228">X. Monumens et
          Antiquités de l'ancienne Ville de Pompeii, détruite et
          ensévelie sous les Cendres du Vésuve, à la même Époque,
          et par la même Éruption qu'Herculanum, en l'Année 79</a>
          <ul>
            <li>
              <a href="fa1703810c2s.html#230">Antiquités de
              Pompeii</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#231">Vue de l'Entrée et
              de la Rue principale de Pompeii</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#232">Vue de l'Entrée de
              Pompeii et de la Rue principale de cette ancienne
              Ville située près du Vésuve ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#237">Vue du Temple
              d'Isis à Pompeii, dans l'état où il est actuellement
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#238">Temple d'Isis dans
              son État actuel</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#241">Vue du Temple
              d'Isis à Pompeii prise sur la partie latérale et
              reitabli tel qu'il devoit être avant l'Éruption de 79
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#242">Temple d'Isis
              rétabli et vu sur la Partie latérale</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#243">Seconde Vue du
              Temple d'Isis à Pompeii, rétabli tel qu'il devoit
              être en l'Année 79</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#244">Temple d'Isis à
              Pompeii tel qu'il devoit être en l'Année 79 lorsqu'il
              a été détruit par l'Éruption du Vésuve ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#247">Plan géométral du
              Temple d'Isis</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#248">1. Plan géométral
              du Temple d'Isis à Pompeii 2. Plan géométral du
              Tombeau de Mammia avec la Coupe de ce Tombeau à
              Pompeii ◉</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#253">Fragmens du Temple
              d'Isis</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#254">Divers Fragments
              et Détails du Temple d'Isis à Pompeii ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#257">1. Vue de la
              petite Cour supérieure de la Maison de Campagne de
              Pompeii 2. Vue de la Parte intérieure ou du Rez de
              Chaussée d'une Maison de Campagne découverte près de
              Pompeii ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#258">Maison de Campagne
              de Pompeii</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#261">Plan géométral de
              la Maison de Campagne de Pompeii</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#262">1. Plan du Rez de
              Chaussée et du premier Etage de la Maison de Campagne
              à Pompeii 2. Dévelopement de la Chambre des Bains
              ◉</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#267">Rétablissement de
              la Maison de Campagne de Pompeii</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#268">Vue d'une Maison
              de Campagne située près de l'ancienne Ville de
              Pompeii ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#271">1. Vue du Tombeau
              de la Prêtresse Mammia, situé à Pompeii 2. Vue prise
              dans l'enceinte du Tombeau de Mammia ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#272">Vue du Tombeau de
              Mammia, Grande Prêtresse, à Pompeii</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#273">Vue d'un Temple
              Grec, à Pompeii</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#274">1. Plan et
              Élévation d'un Ancien Temple Grec situé à Pompeii ◉
              2. Vue Perspective et Rétablissemens du même Temple
              Grec à Pompeii ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#277">Vue générale du
              Camp, ou Quartier des Soldats à Pompeii</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#278">Vue générale de
              l'État actuel des fouilles ordonnées par le Roi de
              Naples dans le lieu que l'ou pense avoir été le Camp
              ou Quartier des Soldats à Pompeii ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#281">Plan géométral du
              Camp des Soldats à Pompeii</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#282">1. Plan géométral
              de l'État actuel de la fouille du Quartier des
              Soldats à Pompeii 2. Plan général des Ruines de
              Pompeii où sont compris l'Amphithéâtre, le Camp des
              Soldats, le Temple d'Isis et l'ancien Temple Grec
              ◉</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#287">1. Vue Perspective
              de la Colonnade du Camp des Soldats à Pompeii 2. Vue
              prise dans l'intérieur des fouilles du Camp des
              Soldats à Pompeii ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#288">Vues prises dans
              l'Intérieur de la Fouille du Camp des Soldats à
              Pompeii</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#290">1. Vue d'une
              petite Maison bâtie sur les murs de Pompeii 2. Vue
              &#160;Perspective de la Colonnade du Quartier des
              Soldats à Pompeii ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#293">1. Plan et
              Élévation d'un Angle de la Colonnade du Quartier des
              Soldats à Pompeii 2. Divers Fragments et Détails
              d'Architecture du Quartier des Soldats à Pompeii
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#294">Plan et
              Élévation</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#297">Rétablissemens du
              Quartier des Soldates à Pompeii ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#298">Rétablissement</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c2s.html#300">XI. Description des
          Champs Phlégréens, précédée d'une Notice succinte sur
          l'Origine et les Causes des Volcans, avec les Vues des
          Monumens antiques de Pouzzole, Bayes, Cumes, Bauli et
          Misene</a>
          <ul>
            <li>
              <a href="fa1703810c2s.html#302">Essai ou Notice
              succinte sur les Causes et l'Origine des Volcans</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#313">Carte du Golfe de
              Pouzzoles avec une Partie des Champs Phlégréens dans
              la Terre de Labour ◉</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#316">Carte du Golfe de
              Pouzzole et des Champs Phlégréens</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#321">Vue du Golfe et du
              Village de Mare Piano près de Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#322">Vue du Golfe de
              Mare Piano</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#323">Vue des Écoles de
              Virgile [et] Vue d'une des principales Carrières de
              Pozzolane</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#324">Vues des Ruines et
              Constructions antiques appellées vulgairement Les
              Écoles de Virgile ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#327">1. Vue d'un Chemin
              qui conduit aux Carrières de Pouzzolane 2. Vue d'une
              des principales Carrières de Pouzzolane ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#331">1. Première Vue du
              Temple de Jupiter Serapis 2. Seconde Vue du Temple de
              Serapis à Pouzzols ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#332">Vues du Temple de
              Jupiter Sérapis, à Pouzzole</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#335">Plan géométral du
              Temple de Sérapis</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#336">Plan de l'Edifice
              Antique connu sous le nom de Temple de Serapis à
              Pouzole ◉</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#339">Rétablissement</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#340">Élevation d'un
              Temple que l'on pense avoir été dédié à Jupiter
              Serapis à Pouzzols près de Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#343">Vue de la Place de
              Pouzzole, et du Jardin des Camaldules</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#344">1. Vue prise dans
              les Jardins des Camaldules de Pouzzoles 2. Vue de la
              Place de Pouzzoles, où l'on voit encore le Pied
              d'Estal d'une Statue élevée à Tibere ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#349">Bas-Reliefs du
              Pied d'Estal d'une Statue élevée à Tibere que l'on
              voit dans la Place Pouzzoles ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#350">Piédestal d'une
              Statue élevée à Tibere sur la Place publique de
              Pouzzole</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#353">1. Vue des Restes
              d'un Théâtre Antique improprement appellée le Tombeau
              d'Agrippine, situé à Bauli près de Bayes 2. Vue de
              l'Amphithéâtre de Pouzzoles ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#354">Vues de
              l'Amphithéâtre de Pouzzole et des Ruines Antiques</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#357">Vue de la
              Solfaterra près de Pouzzole ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#358">Vue de la
              Solfaterra</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#361">1. Vue des Sources
              d'Eaux Chaudes appellées le Pisciarelli près de la
              Solfaterra 2. Vue d'une des Portes de l'ancienne
              Ville de Cumes appellé aujourd'hui l'Arco felice près
              Bayes et Pouzzoles ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#362">Vue des
              Pisciarelli et de l'Arco felice</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#367">Vue du Lac
              d'Agnano et des Étuves de San Germano près de Naples
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#368">Vue du Lac
              Agnano</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#371">1. Vue de la
              Grotte du Chien près le Lac Agnano 2. Vue de l'Église
              de San Vitale en sortant de la Grotte de Pausilippe
              près de Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#372">Vue de la Grotte
              du Chien près du Lac d'Agnano</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#379">Vue de
              l'Astruni</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#380">Vue de l'Astruni
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#383">Vue du Lac Averne,
              des Restes du Temple d'Appolon et de l'Entrée de la
              Grotte de la Sibille de Cumes ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#384">Vue du Lac
              Averne</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#395">Vue prise de
              dessus le Crater de Monte Nuovo ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#396">Vue de Monte
              Nuovo</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#401">Vue des Bains de
              Néron, ou Étuves de Tritoli à Bayes, et du Temple de
              Mercure</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#402">1. Bains ou Étuves
              de Tritoli, connus sous le nom de Bains de Néron 2.
              Vue du Temple de Mercure sur le bord de la Mer dans
              le Golphe de Bayes ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#407">1. Temple de Diane
              sur le bord de la Mer dans le Golphe de Bayes 2. Vue
              prise et dessinée d'après Nature sur le Chemin de
              Naples à Pestum à peu de distance de Salernes ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#408">Vue du Temple de
              Diane, à Bayes</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#409">Vue des Champs
              Élisées</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#410">Vue d'une Partie
              des Champs Élisées prise sur les bords du Lac Acheron
              et dans l'Eloignement ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#413">Plan du Théâtre de
              Misène, et d'une Conserve d'Eau, antique, appellée
              dans le Pays Piscina Admirabile</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#414">Plan de Fabriques
              et Ruine antiques dans les Environs de Bayes et de
              Pouzzoles ◉</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c2s.html#420">XII. Des Environs de
          Naples, ou Description de la Terre de Labour, autrefois
          la Campania Felice</a>
          <ul>
            <li>
              <a href="fa1703810c2s.html#422">Description de la
              Terre de Labour, autrefois Campania Felice</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#424">Carte de
              l'ancienne Campania Felice ◉</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#429">Cumes</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#430">Capoue</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#435">Formies [et]
              Minturne</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#436">Sinuessa</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#438">Cales ou Caleno,
              aujourd'hui Calvi</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#439">Suessa</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#440">Theano [et]
              Atella</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#442">Nola</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#443">Vue de
              l'Amphithéâtre de Capoue</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#444">Ruines de
              l'antique Amphithéâtre de Capoue ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#447">Plans des divers
              Etages, Élevation, Coupe et Détails de l'Amphithéâtre
              de Capoue ◉</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#448">Plan géométral de
              l'Amphithéâtre de Capoue [et] Ruines et Fragmens
              divers de Capoue</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#450">1. Vue de Ruines
              et de Fragments de Constructions antiques, que l'on
              croit être les Portes de l'ancienne Capoue 2. Vue des
              Ruines d'un des Escaliers de l'Amphithéâtre de Capoue
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#453">Tombeaux antiques
              entre Caserte et Capoue ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#454">Tombeaux antiques
              près de Capoue</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#457">Vue de l'Abbaye du
              Mont Cassin</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#458">Vue dessinée
              d'après Nature, de la Cour supérieure et de la
              principale Entrée de l'Abbaye du Mont Cassin près de
              Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#463">1. Première vue de
              l'Isle de Sora, Lieu appellé l'Isola, appartenant au
              Prince de Piombino dans les Environs de Naples 2.
              Deuxième vue de l'Isola et des Cascades formées par
              le fibrene et le Lirys, aujourd'hui appellée le
              Garigliano, à peu de distance du Mont Cassin ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#464">Vue de l'Isle de
              Sora</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#467">1. Vue de Gaette
              prise du bord de la mer a Môle de Gaette 2. Vue du
              Rocher de Terracina sur le Chemin de Naples ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#468">Vues du Rocher de
              Terracina, et de Môle de Gaete</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#475">1. Vue du Palais
              de Casertes à cinq lieues de Naples 2. Vue de la
              Vallée des Fourches Caudines près de Casertes ▣</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#476">Vue du Château
              royal de Caserte</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#478">Vue de la Vallée
              des Fourches Caudines</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#483">Stabie, Sorrente,
              Amalfi, et Salerne</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#486">Des Isles situées
              dans les Environs de Naples</a>
            </li>
            <li>
              <a href="fa1703810c2s.html#491">Des Vases
              Campaniens, vulgairement nommés Vases Étrusques</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
