<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cacia304870s.html#8">Operette istoriche edite ed
      inedite di Antonio Manetti matematico ed architetto
      fiorentino del secolo XV &#160;</a>
      <ul>
        <li>
          <a href="cacia304870s.html#10">Prefazione</a>
        </li>
        <li>
          <a href="cacia304870s.html#40">I. Novella del grasso
          legnajuolo</a>
        </li>
        <li>
          <a href="cacia304870s.html#108">II. Vita di Filippo Ser
          Brunellesco</a>
        </li>
        <li>
          <a href="cacia304870s.html#198">III. Uomini singolari
          in Firenze</a>
        </li>
        <li>
          <a href="cacia304870s.html#208">IV. Notizia di Guido
          Cavalcanti</a>
        </li>
        <li>
          <a href="cacia304870s.html#222">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
