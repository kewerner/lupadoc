<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghrig29813680s.html#4">Le pitture di Cento e le
      vite in compendio di vari incisori e pittori della stessa
      città</a>
      <ul>
        <li>
          <a href="ghrig29813680s.html#6">All'illustrissimo, e
          Reverendissimo Monsignore D. Sante Comarini</a>
        </li>
        <li>
          <a href="ghrig29813680s.html#8">A chi vorrà leggere</a>
        </li>
        <li>
          <a href="ghrig29813680s.html#10">Le chiese</a>
        </li>
        <li>
          <a href="ghrig29813680s.html#25">Le abitazioni</a>
        </li>
        <li>
          <a href="ghrig29813680s.html#34">Le vite in compendio
          degl'incisori, e de' pittori della stessa città</a>
        </li>
        <li>
          <a href="ghrig29813680s.html#76">Indice de' pittori</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
