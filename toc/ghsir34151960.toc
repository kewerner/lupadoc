<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghsir34151960s.html#6">La pratica di
      prospettiva</a>
      <ul>
        <li>
          <a href="ghsir34151960s.html#8">Al serenissimo
          Ferdinando Medici</a>
        </li>
        <li>
          <a href="ghsir34151960s.html#9">A benigni ed amorevoli
          lettori</a>
        </li>
        <li>
          <a href="ghsir34151960s.html#11">Tavola de' capitoli
          nella presente opera contenuti</a>
        </li>
        <li>
          <a href="ghsir34151960s.html#14">Libro primo</a>
        </li>
        <li>
          <a href="ghsir34151960s.html#102">Libro secondo</a>
          <ul>
            <li>
              <a href="ghsir34151960s.html#104">[Tavole]</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
