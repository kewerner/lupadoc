<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="fa1703810c42s.html#8">Voyage Pittoresque Ou
      Description Des Royaumes De Naples Et De Sicile. Quatrième
      Volume, contenant la Description de la Sicile. Seconde
      Partie</a>
      <ul>
        <li>
          <a href="fa1703810c42s.html#10">Avant-Propos</a>
        </li>
        <li>
          <a href="fa1703810c42s.html#14">Table des Chapitres,
          avec les Noms des Planches et des Vues</a>
        </li>
        <li>
          <a href="fa1703810c42s.html#16">XII. Retour de Malte
          en Sicile, Arrivée à Syracuse. Description d'une Partie
          de ses Monumens et de ses Antiquités</a>
          <ul>
            <li>
              <a href="fa1703810c42s.html#23">Vue générale de
              la Ville de Syracuse, prise de dessus Mer. Planche
              cent septième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#24">107. Vue générale
              du Port et de la Ville de Syracuse 108. Vue de
              l'intérieur du Port de Syracuse ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#26">Vue du Lazareth
              et de l'intérieur du Port de Syracuse. Planche cent
              huitième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#27">Plan géométral du
              Sol ou Terrein qu'occupoit l'ancienne Ville de
              Syracuse. Planche cent neuvième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#28">109. Carte ou
              Plan géométral fait à Vol d'Oiseau de l'antique Ville
              de Syracuse et de ses Environs ◉</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#32">Vue de la
              Fontaine d'Arethuse. Planche cent dixième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#34">110. Vue de la
              Fontaine d'Arethuse à Syracuse 111. Restes du Temple
              de Minerve à Syracuse ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#36">Vue des Restes du
              Temple de Minerve à Syracuse. Planche cent
              onzième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#41">112. Vue du Site
              pittoresque et des Débris de l'antique Théâtre de
              Syracuse ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#42">Vue des Restes du
              Théâtre de Syracuse. Planche cent douzième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#45">113. Vue
              extérieure des Lathomies ou Carrières antiques de
              Syracuse 114. Première Vue intérieure des Lathomies
              de Syracuse ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#46">Vue des Latomies
              de Syracuse, avec une Vue intérieure de ces antiques
              Carrières. Planches cent treizième et cent
              quatorzième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#49">115. Seconde Vue
              de l'intérieure des Lathomies de Syracuse 116. Vue de
              l'Entrée d'une des Lathomies de Syracuse appellée
              vulgairement l'Oreille de Denys ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#50">Seconde Vue de
              l'intérieur des Latomies de Syracuse, avec l'Entrée
              de celle qui est particulièrement connue sous le Nom
              de l'Oreille de Denys. Planches cent quizième et cent
              seizième</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c42s.html#54">XIII. Suite de la
          Description de Syracuse, ses Tombeaux, ses Catacombes,
          Grottes et Vallée d'Ispica. Retour à Messine par Augusta,
          Lentini, Catana etc.</a>
          <ul>
            <li>
              <a href="fa1703810c42s.html#57">Vue d'une Latomie
              ou Carrière de Syracuse, servant aujourd'hui de
              Jardin aux Capucins de cette Ville. Planche cent
              dix-septième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#58">117. Vue d'une
              des anciennes Lathomies ou Carrières de Syracuse
              formant aujourd'hui le Jardin des Capucins ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#61">Plan géométral
              des Catacombes antiques de Syracuse, avec la Vue
              intérieure d'une des Chambres sépulcrales. Planches
              cent dix-huitième et cent dix-neuvième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#62">118. Plan des
              Catacombes de Syracuse ◉ 119. Vue intérieure d'une
              des Chambres sépulcrales faisant Partie des
              Catacombes de Syracuse ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#67">120. Vue des
              Restes de quelques Tombeaux antiques à Syracuse 121.
              Petite Vue de l'antinque Théâtre de Syracuse prise
              sur les Gradins même du Théâtre ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#68">Vue d'antiques
              Tombeaux à Syracuse. Planche cent vingtième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#70">Vue d'une Partie
              des Gradins de l'ancien Théâtre de Syracuse. Planche
              cent vingt-unième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#72">Vue des Ruines du
              Temple de Jupiter Olympien à Syracuse. Planche cent
              vingt-deuxième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#75">122. Vue des
              Restes du Temple de Jupiter Olympien à Syracuse 123.
              Vue prise sur l'Anapus petite Rivière qui se jette
              dans le Port de Syracuse et près de la Fontaine ou
              croit le Papyrus ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#76">Vue prise sur
              l'Anapus, près du Port de Syracuse, avec quelques
              Détails sur la Plante du Papyrus. Planche cent
              vingt-troisième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#81">Vue des Grottes
              d'Ispica, et d'une Partie de la Vallée appellée dans
              le Pays Castello d'Ispica. Planche cent
              vingt-quatrième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#82">124. Vue d'une
              Partie des Grottes de la Vallée d'Ispica située dans
              le Val di Noto près de Syracuse ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#88">Vue des Restes
              d'un Monument élevé autrefois par les Syracusains en
              Mémoire de la Victoire mémorable qu'ils remportèrent
              sur les Athéniens. Planche cent vingt-cinquième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#90">125. Restes d'un
              antique Monument élevé par les Syracusains après la
              Défaite des Athéniens commandés par Nicias, dans la
              grande Olympiade 400 ans environ avant Jésus-Christ
              126. Vue d'une Rampe ou Vaste Escalier taillé dans
              les Laves de l'Etna près d'Yaci ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#96">Vue d'antiques
              Laves de l'Etna qui ont coulé jusques dans la Mer
              près d'Iaci Reale. Planche cent vingt-sixième</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c42s.html#100">XIV. Description
          d'une Partie du Val di Noto et des Isles de Lipari</a>
          <ul>
            <li>
              <a href="fa1703810c42s.html#100">Vue des Environs
              des Villes de Piazza et de Pietra-Percia, situées
              dans la Partie de la Sicile appellée Val di Noto.
              Planches cent vingt-septième et cent
              vingt-huitième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#104">127. Vue prise
              dans les Environs de Piazza, jolie Ville située au
              centre de la Sicile, et dans la Partie la plus
              fertile du Val di Noto 128. Vue dessinée dans les
              Environs de Pietra Perica, petite Ville située près
              de Piazza, dans le Val di Noto ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#106">Vue des Grottes
              de San Pantarica près de Sortino, avec une autre
              petite Vue prise dans les Environs. Planches cent
              vingt-neuvième et cent trentième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#108">129. Vues des
              Grottes de San Pantarica dans le Val di Noto, près du
              Lieu où était autrefois l'antique Ville d'Erbessus
              130. Site pittoresque dessiné près des Grottes de San
              Pantarica en Sicile ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#113">Mémoire sur les
              Volcans éteints du Val di Noto en Sicile, par M. le
              Commandeur de Dolomieu</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#120">131. Vue prise
              dans les Campagnes du Val di Noto en Sicile, près de
              Sciortino 132. Vue d'un Pic volcanique composè par
              Couches alternatives de Laves et de Pierres Calcaires
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#126">Description des
              Isles de Lipari</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#127">Vues de l'Isle
              Vulcano. Planches cent trente-troisième et cent
              trente-quatrième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#128">133. Première
              Vue de l'Isle de Vulcano, une des Isles de Lipari, à
              30 mille Nord-Est, des Côtes de la Sicile 134.
              Seconde Vue de l'Isle Vulcanom prise du côté ou est
              situé le Vulcanello ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#141">135. Première
              Vue du Stromboli prise en y arrivant du côté di Midy
              136. Seconde Vue du Stromboli dans la Partie opposée
              au Nord-Est ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#142">Vues du
              Stromboli. Planches cent trente-cinquième et cent
              trente-sixième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#146">Notice ou
              Description sommaire des Médailles de la Sicile</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#148">137. Médailles
              de Gelon, et d'Hyeron première Tyrans de Syracuse
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#152">138. Médailles
              de Denys, de Pyrrhus, et d'Agathocles, Tyrans de
              Syracuse ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#156">139. Médailles
              d'Hyeron, de la Reine Philistides, de Théron, et
              Phinytas ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#159">Médailles de la
              Sicile depuis la Domination des Romaines. Planche
              cent quarantième</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#160">140. Médailles
              relatives à l'Histoire de la Sicile depuis la prise
              de Syracuse par les Romains ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c42s.html#163">Médailles des
          anciennes Villes de la Sicile</a>
          <ul>
            <li>
              <a href="fa1703810c42s.html#163">I. Premier
              Chapitre. Messine</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#164">[Médailles de
              Messine] ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#167">[Médailles de
              Naxos e Taorminum] ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#168">II. Seconde
              Chapitre. Taorminum et Naxos</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#169">III. Troisième
              Chapitre. Catane</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#170">[Médailles de
              Catania] ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#173">[Médailles]
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#174">IV. Quatrième
              Chapitre. Hybla, Aetnaion, Adranum et Morgantia</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#175">V. Cinquième
              Chapitre. Agyrium, Assoro, Centorbi et Enna</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#176">[Médailles]
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#179">VI. Sixième
              Chapitre. Hymera, Termini, Solunte et Palerme</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#180">[Médailles]
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#183">[Médailles]
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#184">VII. Septième
              Chapitre. Iccara, Iaetas, Entella et Segeste</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#185">VIII. Huitième
              Chapitre. Erix, Drepanum, Motya, Lilibée et
              Selinunte</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#186">[Médailles]
              ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#189">IX./X. Neuvième
              et dixième Chapitres. Agrigente</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#190">[Médailles de
              Agrigentum] ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#192">[Médailles de
              Agrigentum] ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#195">[Médailles de
              Cela et Melita] ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#196">XI. Onzième
              Chapitre. Gela et les Isles de Malte et de Gozzo,
              anciennement Melita et Gaulos</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#196">XII./XIII.
              Douzième et treizième Chapitres. Syracuse</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#198">[Médailles de
              Syracusa] ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#201">[Médailles de
              Syracusa] ▣</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#202">XIV.
              Quatorizième Chapitre. Camarina, Megara et
              Leontium</a>
            </li>
            <li>
              <a href="fa1703810c42s.html#204">[Médailles de
              Leontium, Megara et Camarina] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="fa1703810c42s.html#206">Mémoire sur les
          Tremblemens de Terre de la Calabre ultérieure pendant
          l'année 1783</a>
        </li>
        <li>
          <a href="fa1703810c42s.html#231">Table des
          Matières</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
