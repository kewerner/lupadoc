<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghagl35152860s.html#8">Painting illustrated in
      three diallogues, containing some choise observations upon
      the art</a>
      <ul>
        <li>
          <a href="ghagl35152860s.html#12">The Epistle</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#16">The Preface</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#36">The contents of this
          work</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#37">An explanation of some
          terms of the art of painting</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#44">Dialogues about
          painting: between a traveller and his friend</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#76">The history of the art
          of painting, dialogue II.</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#140">Dialogue III.</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#172">The life of
          Cimabue</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#184">The life of
          Giotto</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#210">The life of Leonardo
          da Vinci</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#242">The life of Andrea
          del Sarto</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#266">The life of Raphael
          del Urbin</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#314">The life of Giorgione
          da Caselfranco</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#322">The life of Michael
          Angelo Buonarotti</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#365">The life of Giulio
          Romano</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#383">The life of Perino
          del Vaga</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#403">The life of Titiano
          da Ladore</a>
        </li>
        <li>
          <a href="ghagl35152860s.html#414">The life of
          Donato</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
