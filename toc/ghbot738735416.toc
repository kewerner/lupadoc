<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbot738735416s.html#6">Raccolta Di Lettere Sulla
      Pittura Scultura Ed Architettura; Tomo VI.</a>
      <ul>
        <li>
          <a href="ghbot738735416s.html#8">All'illustrissimo, e
          reverendissimo Signore Monsignor Gio. Diaz Guerra</a>
        </li>
        <li>
          <a href="ghbot738735416s.html#16">Al cortese
          lettore</a>
        </li>
        <li>
          <a href="ghbot738735416s.html#22">[I.] Lettere su la
          pittura scultura ed architettura&#160;</a>
        </li>
        <li>
          <a href="ghbot738735416s.html#121">II. Dell'idea dei
          pittori, scultori, e architetti</a>
        </li>
        <li>
          <a href="ghbot738735416s.html#377">Indice de' nomi
          degli autori</a>
        </li>
        <li>
          <a href="ghbot738735416s.html#378">Indice de' nomi di
          quelli, a cui sono dirette le Lettere</a>
        </li>
        <li>
          <a href="ghbot738735416s.html#379">Indice delle cose
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
