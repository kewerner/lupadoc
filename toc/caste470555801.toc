<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caste470555801s.html#8">Raffaele Stern. Ein
      Beitrag zur Architekturgeschichte in Rom zwischen 1790 und
      1830</a>
      <ul>
        <li>
          <a href=
          "caste470555801s.html#10">Inhaltsverzeichnis</a>
        </li>
        <li>
          <a href="caste470555801s.html#14">Vorwort</a>
        </li>
        <li>
          <a href="caste470555801s.html#16">Einleitung</a>
        </li>
        <li>
          <a href="caste470555801s.html#20">§1 Das Leben des
          Baumeisters Raffaele Stern</a>
        </li>
        <li>
          <a href="caste470555801s.html#44">§2 Das Bauen in Rom
          zwischen 1790 und 1817 (Braccio Nuovo)</a>
        </li>
        <li>
          <a href="caste470555801s.html#82">§3 Der Braccio
          Nuovo</a>
        </li>
        <li>
          <a href="caste470555801s.html#222">§4 Die
          Neuausstattung des Quirinalspalastes und andere, kleinere
          Arbeiten</a>
        </li>
        <li>
          <a href="caste470555801s.html#264">§5 Restaurationen
          an antiken Monumenten</a>
        </li>
        <li>
          <a href="caste470555801s.html#320">§6 Sterns
          Architektur-Traktat</a>
        </li>
        <li>
          <a href="caste470555801s.html#366">Zusammenfassung</a>
        </li>
        <li>
          <a href="caste470555801s.html#376">Anmerkungen</a>
        </li>
        <li>
          <a href="caste470555801s.html#470">Anhang</a>
          <ul>
            <li>
              <a href="caste470555801s.html#470">Urkunden und
              Quellen</a>
            </li>
            <li>
              <a href=
              "caste470555801s.html#510">Literaturverzeichnis</a>
            </li>
            <li>
              <a href=
              "caste470555801s.html#532">Abbildungsverzeichnis</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="caste470555801s.html#548">Lebenslauf</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
