<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502390s.html#8">Le Nove Chiese Di Roma</a>
      <ul>
        <li>
          <a href="dg4502390s.html#10">All'eminentissimo e
          reverendissimo Principe Francesco Cardinale Barberino</a>
        </li>
        <li>
          <a href="dg4502390s.html#12">Dichiaratione della
          Basilica di San Pietro Principe degli Apostoli&#160;</a>
        </li>
        <li>
          <a href="dg4502390s.html#55">Dichiaratione della
          Basilica di San Paolo fuori di Roma</a>
        </li>
        <li>
          <a href="dg4502390s.html#75">Dichiaratione delle tre
          fontane fuori di Roma</a>
        </li>
        <li>
          <a href="dg4502390s.html#87">Dichiaratione della Chiesa
          dell'Annunziata fuori di Roma&#160;</a>
        </li>
        <li>
          <a href="dg4502390s.html#92">Dichiaratione della Chiesa
          di San Sebastiano fuori di Roma</a>
        </li>
        <li>
          <a href="dg4502390s.html#103">Dichiaratione della
          sacrosante Basilica Lateranense</a>
        </li>
        <li>
          <a href="dg4502390s.html#140">Dichiaratione della Chiesa
          di Santa Croce in Gierusalemme</a>
        </li>
        <li>
          <a href="dg4502390s.html#151">Dichiaratione della Chiesa
          di San Lorenzo fuori delle mura</a>
        </li>
        <li>
          <a href="dg4502390s.html#164">Dichiaratione della Chiesa
          di Santa Maria Maggiore</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
