<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502670s.html#6">Roma antica figurata</a>
      <ul>
        <li>
          <a href="dg4502670s.html#8">I. Dell'origine et progresso
          dell'alma città di Roma</a>
        </li>
        <li>
          <a href="dg4502670s.html#11">II. Del circuito di
          Roma</a>
        </li>
        <li>
          <a href="dg4502670s.html#11">III. Di tutte le Porte di
          Roma secondo hora si trovano, con le dichiarationi de'
          loro nomi antichi</a>
        </li>
        <li>
          <a href="dg4502670s.html#15">IV. Delle Vie</a>
        </li>
        <li>
          <a href="dg4502670s.html#16">V. Del ›Tevere‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#17">VI. Delle Inondationi del
          Tevere</a>
        </li>
        <li>
          <a href="dg4502670s.html#18">VII. Delli Ponti, e che
          furono, et hoggi sono sopra il Tevere, et suoi
          edificatori</a>
          <ul>
            <li>
              <a href="dg4502670s.html#18">›Ponte Sublicio‹ ▣ ,
              come era anticamente</a>
            </li>
            <li>
              <a href="dg4502670s.html#20">Ponte Trionfale ›Pons
              Neronianus‹, come già era ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#22">Del Ponte, del Castello
              e della Mole d'Adriano ›Castel Sant'Angelo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#26">VIII. Dell'Isola Tiberina
          come hoggi si vede ▣</a>
          <ul>
            <li>
              <a href="dg4502670s.html#27">Dell'Isola del Tevere,
              del ›Tempio di Esculapio‹ ▣, e di quello di Giunone
              ›Tempio di Iuppiter Iurarius‹, e Fausto ›Tempio di
              Fauno‹, del Ponte Fabritio, hoggi detto ›Ponte (dei)
              Quattro Capi‹, e del ›Ponte Cestio‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#30">IX. Del ›Trastevere‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#30">X. Di tutti gli Monti di
          Roma</a>
        </li>
        <li>
          <a href="dg4502670s.html#32">XI. Di ›Testaccio
          (monte)‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#33">XII. Dell'Acque, et chi le
          condusse in Roma</a>
          <ul>
            <li>
              <a href="dg4502670s.html#35">Prospettiva della Fonte
              Felice ›Fontana del Mosè‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#36">XIII. De gli Acquedotti</a>
          <ul>
            <li>
              <a href="dg4502670s.html#38">›Porta Maggiore‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#40">XIV. Delle Cloache, ovvero
          Chiaviche</a>
        </li>
        <li>
          <a href="dg4502670s.html#41">XV. Delle ›Sette Sale‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#42">XVI. Delle Therme, e suoi
          Edificatori</a>
          <ul>
            <li>
              <a href="dg4502670s.html#43">Delle Therme Agrippine
              ›Terme di Agrippa‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#44">Therme di Nerone, et di
              Alessandro Severo ›Terme Neroniano-Alessandrine‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#46">Delle Therme, ò Bagni
              di Antonino Caracalla hoggi dette le Antoniane ›Terme
              di Caracalla‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#47">Delle Therme
              Diocletiane ›Terme di Diocleziano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#49">Delle ›Terme di
              Costantino‹ ▣, come hoggi si trovano</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#51">XVII. Delli ›Bagni di Paolo
          Emilio‹ ▣</a>
          <ul>
            <li>
              <a href="dg4502670s.html#54">Della Naumachia
              ›Naumachia Caesaris‹ ▣, ed ›Horti di Cesare‹, e delli
              Prati di Mutio Scevola ›Prata Mucia‹</a>
            </li>
            <li>
              <a href="dg4502670s.html#56">Della Naumachia di
              Domitiano ›Naumachia Domitiani‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#57">XIX. Delli Cerchi, et che
          cosa erano</a>
          <ul>
            <li>
              <a href="dg4502670s.html#58">Del Cerchio Massimo
              ›Circo Massimo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#60">Del Cerchio, et
              Naumachia di Nerone ›Circo di Caligola‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#62">Del Cerchio Agonale,
              chiamato ›Piazza Navona‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#64">Del Cerchio di Antonino
              Caracalla ›Circo di Massenzio‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#65">Del Cerchio di Flora
              ›Circo di Flora‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#66">XX. Delli Theatri, che cosa
          erano, et suoi Edificatori</a>
          <ul>
            <li>
              <a href="dg4502670s.html#66">›Teatro di Pompeo‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#67">›Teatro di Marcello‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#69">XXI. Degli Anfiteatri, et
          suoi Edficatori, et che cosa erano</a>
          <ul>
            <li>
              <a href="dg4502670s.html#69">Anfiteatro di
              Vespasiano, detto ›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#72">›Anfiteatro di Statilio
              Tauro‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#73">XXII. Delli Fori, cioè
          Piazze, overo Mercati</a>
          <ul>
            <li>
              <a href="dg4502670s.html#76">Disegno secondo che
              hoggi è il ›Foro Romano‹ detto Campo Vaccino, perchè
              hora vi si fà il Mercato de' Bovi, et d'altri animali
              ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#78">Del ›Foro di
              Cesare‹</a>
            </li>
            <li>
              <a href="dg4502670s.html#78">Del ›Foro di
              Augusto‹</a>
            </li>
            <li>
              <a href="dg4502670s.html#80">Del ›Foro di Nerva‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#82">Del ›Foro di Traiano‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#84">Del ›Foro Boario‹</a>
            </li>
            <li>
              <a href="dg4502670s.html#84">Del ›Foro Olitorio‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#86">XXIII. Delli Archi
          Trionfali, et a chi si davano, et del modo di
          trionfare</a>
          <ul>
            <li>
              <a href="dg4502670s.html#86">›Arco di Settimio
              Severo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#88">Dell'›Arco di Tito‹
              Vespasiano ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#90">Dell'›Arco di
              Costantino‹ Magno ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#92">Dell'Arco Boario ›Arco
              degli Argentari‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#94">Dell'›Arco di Gallieno‹
              ▣, hoggi detto di Santo Vito</a>
            </li>
            <li>
              <a href="dg4502670s.html#96">Delle ›Terme di
              Domiziano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#96">XXIV. Delli Portichi</a>
        </li>
        <li>
          <a href="dg4502670s.html#100">XXV. Delli Trofei di Gaio
          Mario, e fabrica di Gaio, e Lucio ›Trofei di Mario‹</a>
          <ul>
            <li>
              <a href="dg4502670s.html#100">Disegno delli Trofei,
              come erano anticamente ›Trofei di Mario
              (Campidoglio)‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#102">Disegno delli ›Trofei
              di Mario‹ ▣, come erano avanti fossero posti in
              Campidoglio ›Trofei di Mario‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#103">XXVI. Delle Colonne più
          memorande</a>
          <ul>
            <li>
              <a href="dg4502670s.html#103">Della ›Colonna
              Traiana‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#105">Della Colonna
              d'Antonino Imperatore ›Colonna di Antonino Pio‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#108">Della Colonna Bellica
              ›Columna Bellica‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#109">Della Colonna
              Milliaria ›Miliarium Aureum‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#111">Della Colonna Lattaria
              ›Columna Lactaria‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#111">Della ›Colonna
              Menia‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#112">XVII. Delli Colossi</a>
          <ul>
            <li>
              <a href="dg4502670s.html#113">Capo del Colosso di
              Commodo ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#116">Capo del Colosso di
              Scipione Africano, nel palazzo dell'Illustrissimo, et
              Eccellentissimo Duca Cesis ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#117">Capo del Colosso di
              Commodo Imperatore di bronzo come si vede in
              Campidoglio nel Palazzo de' Signori Conservateri
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#118">XXVIII. Delle Piramidi</a>
          <ul>
            <li>
              <a href="dg4502670s.html#118">Della ›Piramide di
              Caio Cestio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#120">XXIX. Delle Mete</a>
          <ul>
            <li>
              <a href="dg4502670s.html#121">Della ›Meta Sudante‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#122">Del Sepolcro di
              Metella, detto Capo di Bove, della custodia de'
              Soldati, e d'altri Sepolcri antichi ›Tomba di Cecilia
              Metella‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#124">XXX. Delli Obelischi,
          overo Aguglie</a>
          <ul>
            <li>
              <a href="dg4502670s.html#125">Disegno del Castello,
              con il quale fù eretta la Aguglia del Vaticano, et
              altre ›Obelisco Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#127">L'Aguglia, overo
              ›Obelisco Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#129">L'Aguglia Lateranense
              ›Obelisco Lateranense‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#132">L'Aguglia Flaminia,
              alla Madonna del Popolo ›Obelisco Flaminio‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#134">L'Aguglia di Santa
              Maria Maggiore ›Obelisco Esquilino‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#136">L'Aguglia di San
              Mahuto ›Obelisco di Ramsses II (1)‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#137">L'Aguglia del Giardino
              de' Medici ›Obelisco di Boboli‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#138">L'Aguglia del Giardino
              de' Mattei ›Obelisco di Ramsses II (2)‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#140">L'Aguglia, overo
              Obelisco, posto nuovamente sopra la piazza della
              Minerva ›Pulcin della Minerva‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#141">XXXI. Delle Statue</a>
          <ul>
            <li>
              <a href="dg4502670s.html#142">Statua dell'Imperatore
              Marco Aurelio Antonino Pio, di pronzo, sù la piazza
              di Campidoglio ›Statua equestre di Marco Aurelio‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#143">Statua d'Hercole di
              Metallo ›Statua di Ercole in bronzo dorato‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#144">Pastore di Metallo,
              che è in Campidoglio ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#145">Esculapio nel Palazzo
              de Savelli ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#146">Statua di Sisto V di
              bronzo in Campidoglio ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#148">›Laocoonte‹ di
              Belvedere ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#149">Figura di Cleopatra in
              Belvedere ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#150">L'Hercole, detto
              Tronco di Belvedere ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#151">Figura del Toro, nel
              Palazzo Farnese ›Toro Farnese‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#152">Figura di Roma
              trionfante ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#153">Figura della Dacia
              soggiogata da Roma, come si vede in casa
              dell'Eminentissimo Cardinal Cesis ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#154">Figura di un Re
              Barbaro schiavo in casa del Cardinal Cesis ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#155">Un'altro Re Barbaro
              schiavo, ambi nella casa del Cardinal Cesis ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#156">Lottatori nel Giardino
              de' Medici ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#157">Fugura di Bacco,
              nell'istesso Giardino ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#158">Figura di Cacciatore,
              nell'istesso Giardino de' Medici ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#159">›Statua di Mosè‹ ▣ in
              San Pietro in Vincoli</a>
            </li>
            <li>
              <a href="dg4502670s.html#160">Tavola di marmo di
              basso rilievo, che rappresenta l'Agricoltura ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#163">Della ›Statua di
              Marforio‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#165">Della ›Statua di
              Pasquino‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#167">XXXII. Delli Cavalli</a>
          <ul>
            <li>
              <a href="dg4502670s.html#167">Figura delli Cavalli,
              e Statue, che sono su l'monte Quirinale, oggi detto
              monte Cavallo, secondo erano avanti che fossero
              ristorare da Sisto V ›Fontana di Monte Cavallo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#169">XXXIII. Delle Librarie</a>
          <ul>
            <li>
              <a href="dg4502670s.html#171">Della Libraria Augusta
              ›Biblioteca Palatina‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#172">Della Libraria
              Vaticana ›Biblioteca Apostolica Vaticana‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#174">XXXIV. Della Nobilissima
          Arte della Stampa, quando fu trovata</a>
        </li>
        <li>
          <a href="dg4502670s.html#174">XXXV. De gli Horiuoli
          diversi</a>
        </li>
        <li>
          <a href="dg4502670s.html#175">XXXVI. Delli Palazzi in
          generale</a>
        </li>
        <li>
          <a href="dg4502670s.html#176">XXXVII. Della Casa Aurea
          di Nerone ›Domus Aurea‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#178">XXXVIII. Della parte del
          Tempio del Sole nel Quirinale, edificato da Aureliano
          Imperatore, detto impropriamente Frontespicio di Nerone
          ›Torre Mesa‹ ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#180">XXXIX. Delle Case de'
          Cittadini</a>
          <ul>
            <li>
              <a href="dg4502670s.html#181">Della Casa de' Flavij
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#181">XL. Delle Curie, e che
          cosa erano</a>
        </li>
        <li>
          <a href="dg4502670s.html#182">XLI. De' Senaculi, overo
          Senatuli, et che cosa erano</a>
          <ul>
            <li>
              <a href="dg4502670s.html#183">Del Senaculo delle
              Donne ›Senaculum Mulierum‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#184">XLII. Delli Mastrati, cioè
          offitij con li quali Roma, et il Romano Imperio fu
          governato</a>
        </li>
        <li>
          <a href="dg4502670s.html#186">XLIII. Delli Comitij, e
          che cosa erano ›Comitium‹ ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#187">XLIV. Delle Tribù</a>
        </li>
        <li>
          <a href="dg4502670s.html#187">XLV. Della Genologia di
          Romolo</a>
        </li>
        <li>
          <a href="dg4502670s.html#190">XLVI. Del ›Fico Ruminale‹,
          della Casa di faustolo ›Casa di Faustulo‹, di quella di
          Catilina ›Domus Catilinae‹, e della ›Velia‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#191">XLVII. Della forma, e
          circuito di Roma, fatto da Romolo</a>
        </li>
        <li>
          <a href="dg4502670s.html#192">XLVIII. Delle Porte di
          Roma, al tempo di Romolo</a>
        </li>
        <li>
          <a href="dg4502670s.html#193">XLIX. Del vario circuito
          di Roma nel tempo de' Re, e de' Consoli</a>
        </li>
        <li>
          <a href="dg4502670s.html#195">L. Del vago circuito di
          Roma, nel tempo de gli Imperatori</a>
        </li>
        <li>
          <a href="dg4502670s.html#200">LI. Delle porte
          generalmente</a>
        </li>
        <li>
          <a href="dg4502670s.html#202">LII. Del sito di Roma</a>
        </li>
        <li>
          <a href="dg4502670s.html#204">LIII. Di Romolo primo Re
          de' Romani</a>
        </li>
        <li>
          <a href="dg4502670s.html#206">LIV. Delle Tavole, ò
          vogliamo dire libri publici</a>
        </li>
        <li>
          <a href="dg4502670s.html#208">LV. Del ›Campidoglio‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#210">LVI. Dell'›Asilo di Romo‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#211">LVII. Di Numa Pompilio
          secondo Re</a>
        </li>
        <li>
          <a href="dg4502670s.html#211">LVIII. Di Tullio Hostilio
          terzo Re</a>
        </li>
        <li>
          <a href="dg4502670s.html#212">LIX. Di Anco Martio,
          quarto Re</a>
        </li>
        <li>
          <a href="dg4502670s.html#213">LX. Di Tarquinio Prisco,
          quinto Re</a>
        </li>
        <li>
          <a href="dg4502670s.html#213">LXI. Di Servio Tullio,
          sesto Re</a>
        </li>
        <li>
          <a href="dg4502670s.html#214">LXII. Di Tarquinio
          Superbo, settimo, et ultimo Re</a>
        </li>
        <li>
          <a href="dg4502670s.html#215">LXIII. Dello Sposalitio
          de' Gentili Romani</a>
        </li>
        <li>
          <a href="dg4502670s.html#216">LXIV. Della Sposa al
          Marito</a>
        </li>
        <li>
          <a href="dg4502670s.html#217">LXV. Delle Basiliche, e
          che cosa erano</a>
          <ul>
            <li>
              <a href="dg4502670s.html#219">Della Basilica di
              Antonino Pio ›Tempio di Antonino e Faustina‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#221">LXVI. Dell'Erario, cioè
          camera del commune, e che moneta si spendeva in Roma in
          quei tempi ›Aerarium‹ ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#223">LXVII. Del Gregostaso, che
          cosa era, et a chi serviva ›Grecostasi‹ ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#224">LXXIII (sic!). Della
          Secretaria del Popolo Romano ›Secretarium Senatus‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#225">LXIX. Delli Rostri, et che
          cosa erano</a>
        </li>
        <li>
          <a href="dg4502670s.html#225">LXX. Dell'Equimelio
          ›Aequimaelium‹, ›Campo Marzio‹, et Tigillo Sororio
          ›Tigillum Sororium‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#226">LXXI. De i Campi
          forestieri ›Castra Peregrina‹, et della ›Villa
          Publica‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#227">LXXII. Della ›Taberna
          Meritoria‹, e che cosa era</a>
        </li>
        <li>
          <a href="dg4502670s.html#228">LXXIII. Del Vivario
          ›Vivarium‹, e che cosa era ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#229">LXXIV. Della Torre ›Turris
          Maecenatiana‹, et Horti di mecenate ›Horti Maecenatiani‹
          ▣</a>
          <ul>
            <li>
              <a href="dg4502670s.html#230">Degli Horti, et foro
              di Sallustio ›Horti Sallustiani‹</a>
            </li>
            <li>
              <a href="dg4502670s.html#233">Delli Horti Palatini
              detti Farnesiani ›Orti Farnesiani‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#234">LXXV. Del Campo Esquilino
          ›Campus Esquilinus‹ ▣, nel quale erano in Puticuli
          ›Puticuli‹ ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#235">LXXVI. Del ›Velabro‹ ove
          era, e d'onde vien detto ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#236">LXXVII. Delle ›Carinae‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#236">LXXVIII. Delli Clivi, e
          che cosa erano</a>
        </li>
        <li>
          <a href="dg4502670s.html#237">LXXIX. Delli Prati</a>
        </li>
        <li>
          <a href="dg4502670s.html#237">LXXX. Delli Granari
          publici, e Magazzini del Sale</a>
        </li>
        <li>
          <a href="dg4502670s.html#239">LXXXI. Delle Carcere
          publiche</a>
        </li>
        <li>
          <a href="dg4502670s.html#240">LXXXII. Di alcune feste, e
          giuochi, che solevano celebrarsi in Roma</a>
        </li>
        <li>
          <a href="dg4502670s.html#242">LXXXIII. Del Mausoleo
          d'Augusto, come era anticamente ›Castel Sant'Angelo‹
          ▣</a>
          <ul>
            <li>
              <a href="dg4502670s.html#244">Delli vestigij del
              Mausoleo d'Augusto ›Castel Sant'Angelo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#246">LXXXIV. Del Settizzonio di
          Severo ›Septizodium (2)‹ ›Septizodium (1)‹ ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#248">LXXXV. Della Pigna di
          metallo d'Adriano ›Pigna di bronzo‹ ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#249">LXXXVI. Delli Tempij</a>
          <ul>
            <li>
              <a href="dg4502670s.html#250">Del ›Tempio della
              Concordia‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#252">Del ›Tempio di Giove
              Feretrio‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#253">Del Tempio di Giove
              Ottimo Massimo, ò vogliamo dire Capitolino ›Tempio di
              Giove Capitolino‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#256">Del Tempio della Pace
              ›Foro della Pace‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#258">Delli Vestigij del
              Tempio della Pace ›Foro della Pace‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#260">Del ›Pantheon‹, cioè
              luogo consecrato à tutti li Dei ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#263">Del Tempio di Giove
              Tonante ›Iuppiter Tonans, Aedes‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#264">Del ›Tempio di Giove
              Statore‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#266">Del ›Tempio
              d'Esculapio‹ nell'Isola del Tevere ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#268">Del Tempio del Sole
              nel Monte Quirinale ›Tempio di Serapide‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#269">Del Tempio di Carmenta
              ›Carmentis, Carmenta‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#270">Del Tempio della
              Pudicitia ›Tempio della Pudicitia Patricia‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#271">Del Tempio della
              Fortuna Virile ›Tempio di Portunus‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#273">Del Tempio di
              Serapide, e d'Iside, essendo accanto uno all'altro, e
              d'una stessa forma ›Tempio di Venere e Roma‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#275">Del ›Tempio di
              Antonino e Faustina‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#277">Del Tempio di Giano
              Quadrifronte ›Arco di Giano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#279">Del Tempio di Giove
              Licaonio nell'Isola del Tevere ›Tempio di Iuppiter
              Iurarius‹ ▣</a>
            </li>
            <li>
              <a href="dg4502670s.html#280">D'alcuni altri
              Templij, de' quali non vi è il disegno, ne si sa il
              luogo proprio ove fossero eretti</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#282">LXXXVII. Degli Tempij,
          Horti, et luoghi ameni fuora di Roma</a>
        </li>
        <li>
          <a href="dg4502670s.html#284">LXXXVIII. Dell'Hipodromo,
          che cosa era, et ove fù anticamente ›Circo di Caligola‹
          ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#286">LXXXIX. Del Castro
          Pretoriano, à che serviva, et ove era anticamente ›Castra
          Praetoria‹ ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#287">XCI (sic!). Delle Vergini
          Vestali, Campo Scelerato ›Campus Sceleratus‹ ▣,
          Sacerdoti, vestimenti, Vasi, et altri istromenti, fatti
          per uso de' Sacrificij</a>
          <ul>
            <li>
              <a href="dg4502670s.html#291">Vasi, et altri
              instrumenti che anticamente servivano per l'uso de'
              sacrificij ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#292">XCI. Del Sepolcro detto di
          Bacco, nella Chiesta di Santa Costanza fuori dalla Porta
          Pia ›Mausoleo di Santa Costanza‹ ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#294">XCII. Dell'Armamentario,
          et a che serviva ›Armamentaria‹ ▣</a>
        </li>
        <li>
          <a href="dg4502670s.html#295">XCIII. Delle Torri de'
          Conti, e delle Militie ›Torre dei Conti‹ ›Torre delle
          Milizie‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#295">XCIV. Degli eserciti
          Romani di Terra, e di Mare, e loro insegne</a>
        </li>
        <li>
          <a href="dg4502670s.html#296">XCV. De' Trionfi, à che si
          concedevanom chi fu il primo, ultimo trionfatore, e di
          quante maniere erano</a>
        </li>
        <li>
          <a href="dg4502670s.html#299">XCVI. Delle Corone, à chi
          si davano, e loro materia</a>
        </li>
        <li>
          <a href="dg4502670s.html#300">XCVII. Del numero del
          Popolo Romano</a>
        </li>
        <li>
          <a href="dg4502670s.html#301">XCVIII. Delle ricchezze
          del Popolo Romano</a>
        </li>
        <li>
          <a href="dg4502670s.html#301">XCIX. Della liberalità
          degli antichi Romani</a>
        </li>
        <li>
          <a href="dg4502670s.html#302">C. Del modo che usavano
          gli antichi Romani in allevare i loro figliuoli, et in
          che anno pigliavano le toghe</a>
        </li>
        <li>
          <a href="dg4502670s.html#303">CI. Della separatione de'
          Matrimonij, che si faceva dalli antichi Romani</a>
        </li>
        <li>
          <a href="dg4502670s.html#303">CII. Dell'essequie antiche
          de' Romani, e sue cerimonie</a>
        </li>
        <li>
          <a href="dg4502670s.html#305">CIII. Roma quante volte fu
          presa, e come sia stata sempre trionfante</a>
        </li>
        <li>
          <a href="dg4502670s.html#306">CIV. Del Palazzo Papale
          ›Palazzo Apostolico Vaticano‹, et ›Cortile del
          Belvedere‹</a>
        </li>
        <li>
          <a href="dg4502670s.html#308">CV. Delle Regioni, cioè
          Rioni, et sue insegne</a>
          <ul>
            <li>
              <a href="dg4502670s.html#309">Del Rione di
              ›Borgo‹</a>
            </li>
            <li>
              <a href="dg4502670s.html#311">Del Rione di
              ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502670s.html#312">Del Rione di Ripa</a>
            </li>
            <li>
              <a href="dg4502670s.html#313">Del Rione di
              Sant'Angelo in Pescaria</a>
            </li>
            <li>
              <a href="dg4502670s.html#314">Del Rione della
              Regola</a>
            </li>
            <li>
              <a href="dg4502670s.html#315">Del Rione di
              Colonna</a>
            </li>
            <li>
              <a href="dg4502670s.html#316">Del Rione di Trevi</a>
            </li>
            <li>
              <a href="dg4502670s.html#317">Del Rione di Campo
              Marzo</a>
            </li>
            <li>
              <a href="dg4502670s.html#318">Del Rione di Ponte</a>
            </li>
            <li>
              <a href="dg4502670s.html#319">Del Rione della
              Pigna</a>
            </li>
            <li>
              <a href="dg4502670s.html#320">Del Rione di
              Parione</a>
            </li>
            <li>
              <a href="dg4502670s.html#321">Del Rione di
              Campitelli</a>
            </li>
            <li>
              <a href="dg4502670s.html#322">Del Rione di
              Sant'Esutachio</a>
            </li>
            <li>
              <a href="dg4502670s.html#323">Del Rione de'
              Monti</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502670s.html#324">CVI. Trattato delli fuochi
          antichi, cavato da diversi scritti, e dall'istesse rovine
          di edifitij antichi</a>
        </li>
        <li>
          <a href="dg4502670s.html#327">Catalogo delli Re, et
          imperatori Romani, e di molti altri Prencipi [...]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
