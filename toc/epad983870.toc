<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="epad983870s.html#8">Alticchiero</a>
      <ul>
        <li>
          <a href="epad983870s.html#10">A Mylord William
          Petty</a>
        </li>
        <li>
          <a href="epad983870s.html#13">Avant-propos de
          l'editeur</a>
        </li>
        <li>
          <a href="epad983870s.html#15">Index</a>
        </li>
        <li>
          <a href="epad983870s.html#20">Alticchiero</a>
        </li>
        <li>
          <a href="epad983870s.html#84">Notes de l'editeur</a>
        </li>
        <li>
          <a href="epad983870s.html#100">[Planches]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
