<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ecre1523940s.html#6">Le pitture e le sculture
      della città di Cremona</a>
      <ul>
        <li>
          <a href="ecre1523940s.html#8">Ornatissimo padre</a>
        </li>
        <li>
          <a href="ecre1523940s.html#10">Al leggitore</a>
        </li>
        <li>
          <a href="ecre1523940s.html#193">Particolari
          Gallerie</a>
        </li>
        <li>
          <a href="ecre1523940s.html#196">Celebri pittori
          cremonesi</a>
        </li>
        <li>
          <a href="ecre1523940s.html#197">Notizie dei tempi de'
          gentili, e delle antiche chiese cristiane</a>
        </li>
        <li>
          <a href="ecre1523940s.html#199">Ordine delle chiese
          esistenti nella Città di Cremona, nei Sobborghi, ed in
          altri Luoghi, ne' quali sono descritte le Pitture</a>
        </li>
        <li>
          <a href="ecre1523940s.html#201">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
