<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg450395012s.html#10">Roma antica</a>
      <ul>
        <li>
          <a href="dg450395012s.html#8">[Antiporta] ▣</a>
        </li>
        <li>
          <a href="dg450395012s.html#12">[Dedica dell'autore al
          Principe Giovanni Lambertini]</a>
        </li>
        <li>
          <a href="dg450395012s.html#14">Indice de' capi del
          volume I</a>
        </li>
        <li>
          <a href="dg450395012s.html#16">Al cortese lettore</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg450395012s.html#10">Volume I.</a>
      <ul>
        <li>
          <a href="dg450395012s.html#18">I. Notizie
          preliminari</a>
          <ul>
            <li>
              <a href="dg450395012s.html#19">›Colonna
              Traiana‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#21">Piano Antico</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#23">Settimonzio</a>
                  <ul>
                    <li>
                      <a href=
                      "dg450395012s.html#23">›Palatino‹</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#23">›Capitolino
                      (Monte)‹</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Pianta
                      dimostrativa di Roma Antica ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Capena‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta San
                      Paolo‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Testaccio
                      (monte)‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Portuensis‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Settimiana‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Porta
                      Ianiculensis ›Porta San Pancrazio‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Isola
                      Tiberina‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Porta
                      Aurelia ›Porta San Pietro‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Porta
                      Flaminia ›Porta del Popolo‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Pinciana‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Salaria‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Nomentana‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Viminalis‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Porta
                      Gabiusa ›Porta Metronia‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Porta Inter
                      Aggeres ›Porta Chiusa‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Porta
                      Praenestina ›Porta Maggiore‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Porta
                      Celimontana ›Arco di Dolabella‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Naevia‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Latina‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Romanula‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Trigonia‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Mugonia‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Porta
                      Ianuale ›Ianus Geminus, aedes‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Carmentalis‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Trionfale‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Collina‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Porta
                      Esquilina ›Arco di Gallieno‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Porta
                      Asinaria‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Ponte
                      Sublicio‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Ponte
                      Palatino‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Ponte
                      Fabricio‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">›Ponte
                      Cestio‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Ponte
                      Gianicolense ›Ponte Sisto‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Ponte
                      Trionfale ›Pons Neronianus‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#24">Ponte Elio
                      ›Ponte Sant'Angelo‹ ◉</a>
                    </li>
                    <li>
                      <a href="dg450395012s.html#26">›Celio‹</a>
                    </li>
                    <li>
                      <a href=
                      "dg450395012s.html#27">›Aventino‹</a>
                    </li>
                    <li>
                      <a href=
                      "dg450395012s.html#27">›Quirinale‹</a>
                    </li>
                    <li>
                      <a href=
                      "dg450395012s.html#28">›Viminale‹</a>
                    </li>
                    <li>
                      <a href=
                      "dg450395012s.html#28">›Esquilino‹</a>
                    </li>
                    <li>
                      <a href=
                      "dg450395012s.html#29">›Pincio‹</a>
                    </li>
                    <li>
                      <a href=
                      "dg450395012s.html#30">›Gianicolo‹</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#30">›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#31">›Tevere‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#32">Mura</a>
            </li>
            <li>
              <a href="dg450395012s.html#36">Porte</a>
            </li>
            <li>
              <a href="dg450395012s.html#37">Vie</a>
            </li>
            <li>
              <a href="dg450395012s.html#39">Regioni</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450395012s.html#42">II. Adiacenze del Monte
          Palatino</a>
          <ul>
            <li>
              <a href="dg450395012s.html#42">Colonne dette del
              ›Tempio di Giove Statore ‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#45">›Velabro‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#45">›Tempio del Divo
              Romolo‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#46">›Arco di Giano‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#48">Arco piccolo di
              Serttimio Severo ›Arco degli Argentari‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#50">Tav. I. Arco di
                  Settimio Severo al Velabro ›Arco degli Argentari‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#52">Tav. II. Arco
                  di Settimio Severo al Velabro ›Arco degli
                  Argentari‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#54">›Foro Boario‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#55">Acqua di Giuturna
              ›Fonte di Giuturna‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#56">›Cloaca Maxima‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#58">›Circo Massimo‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#61">›Acqua Crabra‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#63">›Acquedotto
              Claudio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#63">›Arco di
              Costantino‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#64">Tav. I. ›Arco
                  di Costantino‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#66">Tav. II. ›Arco
                  di Costantino‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#68">Tav. III. ›Arco
                  di Costantino‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#73">›Arco di Tito‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#74">Tav. ›Arco di
                  Tito‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450395012s.html#78">III. Monte Palatino</a>
          <ul>
            <li>
              <a href="dg450395012s.html#78">Riflessioni</a>
            </li>
            <li>
              <a href="dg450395012s.html#82">Rovine negli Orti
              Spada ›Villa Spada (Palatino)‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#84">Tav. I. ›Villa
                  Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#86">Tav. II. ›Villa
                  Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#89">Tav. III. F. 1
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#92">Tav. IV. F. 1
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#95">Tav. V. ›Villa
                  Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#96">Tav. VI. ›Villa
                  Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#101">Tav. VII.
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#103">Tav. VIII
                  ›Villa Spada (Palatino)‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#104">Rovine negli ›Orti
              Farnesiani‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450395012s.html#108">IV. Antichità del
          moderno Campo Vaccino ›Foro Romano‹</a>
          <ul>
            <li>
              <a href="dg450395012s.html#108">›Tempio di Venere
              e Roma‹ oggi Chiesa di ›Santa Francesca Romana‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#110">›Via Sacra‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#111">Tempio della Pace
              ›Foro della Pace‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#112">Limita post Pacis
              Palladiumque Forum ›Foro di Nerva‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#114">Tempio di Venere
              Cloacina ›Cloacina, Sacrum‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#115">Tempio di Romolo e
              Remo ›Tempio del Divo Romolo‹ oggi Chiesa dei ›Santi
              Cosma e Damiano‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#117">›Tempio di
              Antonino e Faustina‹ oggi ›San Lorenzo in
              Miranda‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#118">Tav. I.
                  ›Tempio di Antonino e Faustina‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#120">Tav. II.
                  ›Tempio di Antonino e Faustina‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#125">Tav. III.
                  ›Tempio di Antonino e Faustina‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#126">›Foro Romano‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#128">Basilica di Paolo
              Emilio ›Basilica Aemilia‹ oggi Chiesa di
              ›Sant'Adriano al Foro Romano‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#130">›Arco di Settimio
              Severo‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#132">Colonna detta del
              ›Tempio di Giove Custode‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450395012s.html#133">V. Monte Capitolino, e
          sue adiacenze</a>
          <ul>
            <li>
              <a href="dg450395012s.html#133">Carcere Mamertino,
              e Tulliano ›Carcere Mamertino‹ oggi ›San Pietro in
              Carcere‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#135">›Sepolcro di Gaio
              Poplicio Bibulo‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#137">Sepolcro de'
              Claudii ›Sepulcrum: Claudii‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#137">›Teatro di
              Marcello‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#143">Tav. I. Pianta
                  del ›Teatro di Marcello‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#145">›Tempio della
                  Concordia‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#149">Tav. II.
                  Aspetto del ›Teatro di Marcello‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#153">Tav. III
                  ›Teatro di Marcello‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#155">Casa detta di Cola
              di Rienzo ›Casa dei Crescenzi‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#156">›Ponte
              Palatino‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#157">›Tevere‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#159">Tempio della
              Fortuna Virile ›Tempio di Portunus‹ oggi ›Santa Maria
              Egiziaca‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#160">[Tavola]
                  Tempio detto della Fortuna Virile ›Tempio di
                  Portunus‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#163">Tempio di Vesta
              ›Tempio di Vesta (Foro Romano)‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#166">[Tavola]
                  ›Tempio di Vesta (Foro Romano)‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#168">›Tempio della
              Pietà‹ oggi ›San Nicola in Carcere‹ ▣</a>
            </li>
            <li>
              <a href="dg450395012s.html#168">Carcere de'
              Decemviri ›San Nicola in Carcere‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#169">Salite del
              Campidoglio ›Centum Gradus‹ ›Clivus Capitolinus‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#174">›Tempio della
              Concordia‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#176">Tempio di Giove
              Tonante ›Iuppiter Tonans, Aedes‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#177">›Tabularium‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#177">Sostruzioni, o
              siano mura Capitoline</a>
            </li>
            <li>
              <a href="dg450395012s.html#178">Fabbriche di sito
              incerto</a>
            </li>
            <li>
              <a href="dg450395012s.html#180">Internmonzio</a>
            </li>
            <li>
              <a href="dg450395012s.html#180">›Rupe Tarpea‹</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg450395012s.html#170">Volume II.</a>
      <ul>
        <li>
          <a href="dg450395012s.html#172">Indice de' capi del
          volume II</a>
        </li>
        <li>
          <a href="dg450395012s.html#182">VI. Monte ›Celio‹ e
          sue adiacenze</a>
          <ul>
            <li>
              <a href="dg450395012s.html#182">›Clivo di
              Scauro‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#183">›Curia
              Hostilia‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#184">›Arco di
              Dolabella‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#185">Castro de'
              pellegrini ›Castra Peregrina‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#187">›Castra
                  Peregrina‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#189">›Acquedotto
              Claudio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#191">›Battistero di
              Costantino‹ ▣, Tempio detto di Caludio ›Tempio del
              Divo Caludio‹ ▣</a>
            </li>
            <li>
              <a href="dg450395012s.html#192">›Tempio del Divo
              Claudio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#193">Battistero di
              Costantino ›Battistero Lateranense‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#195">›Anfiteatro
              Castrense‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#196">›Tempio di Venere
              e Cupido‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#198">›Obelisco
              Lateranense‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#199">›Suburra‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#199">Anfiteatro Flavio
              ›Colosseo‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#204">Tav. I
                  &#160;›Colosseo‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#210">[Tavola] ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#214">Tav. II
                  &#160;›Colosseo‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#216">Tav. III
                  &#160;›Colosseo‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#226">›Colosseo‹
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#225">›Meta sudans‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#228">[stampa] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450395012s.html#231">VII. Monumenti lungo
          la Via Appia</a>
          <ul>
            <li>
              <a href="dg450395012s.html#231">›Via Appia‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#232">›Via Appia‹
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#234">›Sepolcro degli
              Scipioni‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#236">›Arco di
              Druso‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#238">Fiumicello
              ›Almone‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#239">Sepolcri d'incerta
              denominazione</a>
            </li>
            <li>
              <a href="dg450395012s.html#239">›Catacombe di San
              Sebastiano‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#241">Mutatorio
              ›Mutatorium Caesaris‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#242">Sepolcro di
              Cecilia Metella ›Tomba di Cecilia Metella‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#244">Sepolcro de'
              Servilj ›Sepulcrum: Servilii‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#245">Circo di Caracalla
              ›Circo di Massenzio‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#246">Pianta del
                  Circo detto di Caracalla ›Circo di Massenzio‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#252">[Tavola] ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#258">[Tavola] ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#260">[Tavola] ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#262">[Tavola] ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#266">[Tavola] ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#268">[Tavola] ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#270">[Tavola] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#275">Fontana Egeria
              ›Ninfeo di Egeria‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#276">Tav. I. Tempio
              detto dell'Onore e della Virtù ›Tempio di Honos et
              Virtus‹ ▣</a>
            </li>
            <li>
              <a href="dg450395012s.html#278">Tav. II. Tempio
              detto dell'Onore e della Virtù &#160;›Tempio di Honos
              et Virtus‹ ▣</a>
            </li>
            <li>
              <a href="dg450395012s.html#280">Tempio del Dio
              Ridicolo ›Tempio di Rediculus‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450395012s.html#281">VIII. Monte Aventino e
          sue adiacenze</a>
          <ul>
            <li>
              <a href="dg450395012s.html#281">›Terme di
              Caracalla‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#284">[Tavola]
                  Pianta delle ›Terme di Caracalla‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#289">›Tempio della
              Pudicitia Patricia‹ oggi ›Santa Maria in
              Cosmedin‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#291">›Clivo dei
              Publicii‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#291">›Acqua Appia‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#292">›Navalia‹ oggi
              ›Marmorata‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#293">›Ponte
              Sublicio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#295">›Testaccio
              (Monte)‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#297">›Piramide di Caio
              Cestio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#300">›San Paolo fuori
              le Mura‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450395012s.html#302">IX. Campo Marzo ›Campo
          Marzio‹ e sue adiacenze</a>
          <ul>
            <li>
              <a href="dg450395012s.html#302">›Portico di
              Ottavia‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#303">›Ponte
              Fabricio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#304">›Isola
              Tiberina‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#306">›Ponte Cestio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#306">›Trastevere‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#309">Ponte e Mole
              Adriana ›Ponte Sant'Angelo‹ ›Castel Sant'Aneglo‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#310">Ponte Trionfale
              ›Pons Neronianus‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#311">›Obelisco
              Vaticano‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#312">Sepoltura di
              Nerone ›Sepulcrum Domitii‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#313">Ponte Molle ›Ponte
              Milvio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#314">›Muro Torto‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#315">Obelisco del
              Popolo ›Obelisco Flaminio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#315">›Mausoleo di
              Augusto‹ e Ustrino de' Cesari ›Ustrinum Augusti‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#317">›Colonna di Marco
              Aurelio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#318">Obelisco Orario
              ›Horologium Augusti‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#318">Tempio di Antonino
              Pio ›Tempio di Antonino e Faustina‹ in oggi Dogana di
              Terra ›Dogana nuova di Terra‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#320">›Pantheon‹ oggi
              Chiesa di Santa Maria della Rotonda</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#322">Tav. I
                  ›Pantheon‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#325">Tav. II
                  ›Pantheon‹ ▣</a>
                </li>
                <li>
                  <a href="dg450395012s.html#329">Tav. III
                  ›Pantheon‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#332">Circo agonale o
              Alessandrino oggi ›Piazza Navona‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#332">Tempio d'Apollo.
              Secondo altri di Ercole Musagete, o Ercole Custode
              ›Tempio di Ercole Custode‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#333">Condotto
              dell'Acqua Vergine ›Acqua Vergine/Acquedotto
              Vergine‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#335">›Bagni di Paolo
              Emilio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450395012s.html#336">X e XI. Monte
          ›Quirinale‹ e ›Viminale‹</a>
          <ul>
            <li>
              <a href="dg450395012s.html#336">›Bagni di Paolo
              Emilio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#337">Obelisco e Cavalli
              antichi ›Fontana di Monte Cavallo‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#337">Obelisco della
              Trinità de' Monti ›Obelisco Sallustiano (Trinità de'
              Monti)‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#338">Circo di Sallustio
              ›Horti Sallustiani‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#338">›Ponte
              Salario‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#339">›Mausoleo di Santa
              Costanza‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#340">Basilica di Santa
              Agnese fuori dalle mura ›Sant'Agnese fuori le
              Mura‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#340">›Ponte
              Nomentano‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#340">›Monte Sacro‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#341">›Castra
              Praetoria‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#342">Porta inter
              aggeres ›Porta Chiusa‹ oggi San Lorenzo ›Porta
              Tiburtina‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#343">Aggere di Servio
              Tullio ›Argine di Tarquinio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#343">›Terme di
              Diocleziano‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#344">›Thermae
              Olympiadis‹ oggi ›San Lorenzo in Panisperna‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#344">›Casa di
              Pompeo‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#345">Casa di Pudente
              ›Domus Pudentis‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#345">Vico Patrizio
              ›Vicus Patricius‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg450395012s.html#346">XII. ›Monte Esquilino‹
          e sue adiacenze</a>
          <ul>
            <li>
              <a href="dg450395012s.html#346">Obelisco di Santa
              Maria Maggiore ›Obelisco Esquilino‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#347">Colonna antica
              ›Colonna della Basilica di Massenzio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#347">Tempio di Diana,
              oggi Chiesa di ›Sant'Antonio Abbate‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#347">›Trofei di
              Mario‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#347">›Tempio di Minerva
              Medica‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#348">Tempio detto
                  Le Gallucce, e Minerva Medica ›Tempio di Minerva
                  Medica‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#351">Colombario della
              Famiglia Arrunzia ›Sepulcrum: Arruntii‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#351">Monumento
              dell'Acqua Claudia ›Castello dell'Acqua Claudia‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#352">›Vivarium‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#353">Aggere di
              Tarquinio ›Argine di Tarquinio‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#353">›Arco di
              Gallieno‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#354">›Terme di
              Traiano‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#354">›Sette Sale‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#355">Palazzo e ›Terme
              di Tito‹</a>
              <ul>
                <li>
                  <a href="dg450395012s.html#356">Pianta delle
                  ›Terme di Tito‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg450395012s.html#359">Tempio di Pallade
              ›Tempio di Minerva (Foro di Nerva)‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#360">›Tempio di Minerva
              (Foro di Nerva)‹ ▣</a>
            </li>
            <li>
              <a href="dg450395012s.html#363">Tempio detto di
              Nerva ›Foro di Nerva‹ ▣</a>
            </li>
            <li>
              <a href="dg450395012s.html#364">Foro e Tempio di
              Nerva ›Foro di Nerva‹</a>
            </li>
            <li>
              <a href="dg450395012s.html#364">Conclusione</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg450395012s.html#365">Indice delle cose
      notabili</a>
    </li>
  </ul>
  <hr />
</body>
</html>
