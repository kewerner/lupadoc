<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.6.0" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="h13848s.html#2">Opticae thesaurus Alhazeni Arabis
      libri septem</a>
      <ul>
        <li>
          <a href="h13848s.html#4">Federici Risneri in Alhazeni
          Arabis opticam praefatio</a>
        </li>
        <li>
          <a href="h13848s.html#8">Alhazen filii Alhayzen opticae
          liber primus</a>
        </li>
        <li>
          <a href="h13848s.html#31">Alhazen filii Alhayzen opticae
          liber secundus</a>
        </li>
        <li>
          <a href="h13848s.html#82">Alhazen filii Alhayzen opticae
          liber tertius</a>
        </li>
        <li>
          <a href="h13848s.html#109">Alhazen filii Alhayzen
          opticae liber quartus</a>
        </li>
        <li>
          <a href="h13848s.html#132">Alhazen filii Alhayzen
          opticae liber quintus</a>
        </li>
        <li>
          <a href="h13848s.html#195">Alhazen filii Alhayzen
          opticae liber sextus</a>
        </li>
        <li>
          <a href="h13848s.html#238">Alhazen filii Alhayzen
          opticae liber septimus</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="h13848s.html#290">Alhazen filii Alhayzen de
      crepusculis et nubium ascensionibus liber unus</a>
    </li>
    <li>
      <a href="h13848s.html#296">Vitellonis Thuringopoloni opticae
      libri decem</a>
      <ul>
        <li>
          <a href="h13848s.html#298">Federici Risneri in
          Vitellonis opticam praefatio</a>
        </li>
        <li>
          <a href="h13848s.html#304">Veritatis amatori fratri
          Guilielmo de Morbeta, Vitello filius Thuringorum et
          Polonorum</a>
        </li>
        <li>
          <a href="h13848s.html#307">Vitellonis filii Thuringorum
          et Polonorum opticae liber primus</a>
        </li>
        <li>
          <a href="h13848s.html#364">Vitellonis filii Thuringorum
          et Polonorum opticae liber secundus</a>
        </li>
        <li>
          <a href="h13848s.html#387">Vitellonis filii Thuringorum
          et Polonorum opticae liber tertius</a>
        </li>
        <li>
          <a href="h13848s.html#420">Vitellonis filii Thuringorum
          et Polonorum opticae liber quartus</a>
        </li>
        <li>
          <a href="h13848s.html#492">Vitellonis filii Thuringorum
          et Polonorum opticae liber quintus</a>
        </li>
        <li>
          <a href="h13848s.html#526">Vitellonis filii Thuringorum
          et Polonorum opticae liber sextus</a>
        </li>
        <li>
          <a href="h13848s.html#571">Vitellonis filii Thuringorum
          et Polonorum opticae liber septimus</a>
        </li>
        <li>
          <a href="h13848s.html#612">Vitellonis filii Thuringorum
          et Polonorum opticae liber octavus</a>
        </li>
        <li>
          <a href="h13848s.html#670">Vitellonis filii Thuringorum
          et Polonorum opticae liber nonus</a>
        </li>
        <li>
          <a href="h13848s.html#706">Vitellonis filii Thuringorum
          et Polonorum opticae liber decimus</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
