<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebol683710s.html#6">Pianta e Spaccato del Nuovo
      Teatro di Bologna</a>
      <ul>
        <li>
          <a href="ebol683710s.html#8">[Dedica dell'autore a
          Girolamo Legnani] Nobile, ed eccelso Signore&#160;</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ebol683710s.html#10">[Tavole]</a>
      <ul>
        <li>
          <a href="ebol683710s.html#10">I. Veduta del Proscenio
          del Nuovo Teatro della Città di Bologna ▣</a>
        </li>
        <li>
          <a href="ebol683710s.html#12">II. Spaccato per il lungo
          del Nuovo Teattro [sic] della Città di Bologna
          ▣&#160;</a>
        </li>
        <li>
          <a href="ebol683710s.html#14">III. Pianta del Nuovo
          Teatro della Città di Bologna ▣</a>
        </li>
        <li>
          <a href="ebol683710s.html#16">IV. Pianta per Metà del
          Portico, e Atrio ▣</a>
        </li>
        <li>
          <a href="ebol683710s.html#18">V. Pianta per Metà del
          Palco Scenario ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
