<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebre2923600s.html#10">Le pitture e sculture di
      Brescia che sono esposte al pubblico</a>
      <ul>
        <li>
          <a href="ebre2923600s.html#12">Agl'illustrissimi
          signori deputati pubblici della città di Brescia</a>
        </li>
        <li>
          <a href="ebre2923600s.html#16">L'autore a chi legge</a>
        </li>
        <li>
          <a href="ebre2923600s.html#32">Indice delle chiese e di
          altri luoghi</a>
        </li>
        <li>
          <a href="ebre2923600s.html#34">Le pitture e sculture di
          Brescia</a>
          <ul>
            <li>
              <a href="ebre2923600s.html#34">Parte prima</a>
            </li>
            <li>
              <a href="ebre2923600s.html#85">Parte seconda</a>
            </li>
            <li>
              <a href="ebre2923600s.html#108">Parte terza</a>
            </li>
            <li>
              <a href="ebre2923600s.html#131">Parte quarta</a>
            </li>
            <li>
              <a href="ebre2923600s.html#156">Parte quinta</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebre2923600s.html#176">Appendice</a>
          <ul>
            <li>
              <a href="ebre2923600s.html#176">[Gallerie
              private]</a>
            </li>
            <li>
              <a href="ebre2923600s.html#219">Tavola de' pittori
              e degli scultori</a>
            </li>
            <li>
              <a href="ebre2923600s.html#224">Pittori delle
              Gallerie</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
