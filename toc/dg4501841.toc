<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501841s.html#6">Onuphrii Panvinii De praecipuis
      urbis Romae, sanctioribusque basilicis, quas septem ecclesias
      vulgo vocant, liber ...</a>
      <ul>
        <li>
          <a href="dg4501841s.html#8">Reverendo nobilitate,
          pietate, atque doctrina prestanti Domino [...]</a>
        </li>
        <li>
          <a href="dg4501841s.html#11">Index ecclesiarum et piorum
          locorum</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501841s.html#13">[Text]</a>
      <ul>
        <li>
          <a href="dg4501841s.html#13">Praefatio</a>
        </li>
        <li>
          <a href="dg4501841s.html#17">I. De quinque urbis
          praecipuis et Patriarchalibus Baslicis, ad de duabus
          aliis Ecclesiis eisdem additis, quae septenarium numerum
          explerunt</a>
        </li>
        <li>
          <a href="dg4501841s.html#25">II. De Presbyterorum
          Cardinalium origine, et XXIIX.ipsorum titulis antiquis,
          et XXI novis</a>
        </li>
        <li>
          <a href="dg4501841s.html#42">III. De diaconorum
          Cardinalium origine, et ipsorum duo decim antiquis
          Diaconiis</a>
        </li>
        <li>
          <a href="dg4501841s.html#50">IV. De Basilica Sancti
          Petri Apostolorum Pincipis in Vaticano ›San Pietro in
          Vaticano‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#87">De Ecclesia Sancta Maria
          trans Tiberim ›Santa Maria in Trastevere‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#92">De secunda basilica Sancti
          Pauli Apostoli via Ostiensi ›San Paolo fuori le Mura‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#106">Gregorius Episcopus servus
          servorum Dei Felici subdiacono rectori patrimonii
          Appiae</a>
        </li>
        <li>
          <a href="dg4501841s.html#113">De Ecclesia sanctorum
          Vincentij et Anastasij martyrum ad tres fontes, sive ad
          aquas Salvias ›Santi Vincenzo e Anastasio alle Tre
          Fontane‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#117">De Oratorio Sanctae Mariae
          Scala coeli ›Santa Maria in scala Coeli‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#119">De Oratorio Sancti Pauli
          ad Tres fontes ›San Paolo alle Tre Fontane‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#120">De Ecclesia Sanctae Mariae
          Annunciatae ›Annunziatella‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#123">De III. Ecclesia Sancti
          Sebastiani ›San Sebastiano‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#136">De sacello, Domine quo
          vadis, vocato ›Chiesa del "Domine quo vadis"‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#140">De IIII. sacrosancto
          basilica Sancti Salvatoris Lateranensi ›Scala Santa‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#164">De basilicae Lateranensis
          redditibus et ornamentis ›San Giovanni in Laterano‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#193">De baptisterio
          Lateranensi, et eius ornamentis ›Battistero
          Lateranense‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#200">De porticu Sancti Venantij
          ante baptisterium ›Cappella di San Venanzio‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#203">De duobus oratoriis in
          baptisterio Lateranensi, Sanctorum Ioannis Baptista, et
          Evangelista ›Cappella del Battista‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#206">De Oratorio Sancti Ioannis
          Evangelista ›Cappella di San Giovanni Evangelista‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#208">De Oratorio Sanctae Crucis
          ›Cappella della Santa Croce‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#211">De Ecclesia Sancti
          Venantii ›Cappella di San Venanzio‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#213">De monasterio Honorij Papa
          iuxta Lateranum ›Santi Andrea e Bartolomeo‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#213">De Patriarchio Lateranensi
          ›Palazzo Lateranense‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#227">De Basilica Leoniana
          minori ›Triclinio Leoniano‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#228">De porticu Lateranensi</a>
        </li>
        <li>
          <a href="dg4501841s.html#230">De pulpitu Bonifacii VIII.
          Papa</a>
        </li>
        <li>
          <a href="dg4501841s.html#231">De aula quam nunc Sala
          concilij vocant ›Sala del Concilio‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#233">De oratorio Sancti
          Sylvestri ›Cappella di San Silvestro‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#234">De Oratorio Sancti
          Laurentij, quod nunc dicitur Sancta Sanctorum ›San
          Lorenzo in Palatio ad Sancta Sanctorum‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#238">[Immagine] ▣</a>
        </li>
        <li>
          <a href="dg4501841s.html#247">De hospitalibus domibus
          Lateranensibus ›Ospedale di San Giovanni‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#249">V. De quinta Ecclesia, et
          titulo Sanctae Crucis in Hierusalem ›Santa Croce in
          Gerusalemme‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#269">De Sancto Sacello
          Hierusalem dicto ›Cappella di Sant'Elena‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#272">De inventione tituli
          Crucis D. N. Iesu Christi Alexandri VI. Papa Bulla</a>
        </li>
        <li>
          <a href="dg4501841s.html#281">VI. De VI. basilica Sancti
          Laurentii extra muros ›San Lorenzo fuori le Mura‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#292">De VII. Basilica Sanctae
          Mariae maioris ›Santa Maria Maggiore‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#319">De Ecclesia et titulo
          Sancti Praxedis ›Santa Prassede‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#328">De Ecclesia et titulo
          Sanctae Pudentianae ›Santa Pudenziana‹</a>
        </li>
        <li>
          <a href="dg4501841s.html#333">De Ecclesia et titulo
          Sanctae Mariae Angelorum in Thermis Diocletiani ›Santa
          Maria degli Angeli‹</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
