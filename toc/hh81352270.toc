<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="hh81352270s.html#6">La Vita di Santa Bibiana
      vergine, e martire romana alla Santità di Nostro Signore Papa
      Urbano Ottavo</a>
      <ul>
        <li>
          <a href="hh81352270s.html#8">›Santa Bibiana: Statua
          della Santa‹ ▣</a>
        </li>
        <li>
          <a href="hh81352270s.html#10">[Dedica dell'autore al
          pontefice Urbano VIII]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="hh81352270s.html#12">Vita di Santa Bibiana vergine,
      e martire romana</a>
      <ul>
        <li>
          <a href="hh81352270s.html#62">Edificazione della Chiesa
          di ›Santa Bibiana‹</a>
        </li>
        <li>
          <a href="hh81352270s.html#62">Basilica Olimpiana ›Santa
          Bibiana‹</a>
        </li>
        <li>
          <a href="hh81352270s.html#63">Fondazione della Chiesa di
          ›Santa Bibiana‹</a>
        </li>
        <li>
          <a href="hh81352270s.html#64">San Simplicio Papa
          restaurò la Chiesa di ›Santa Bibiana‹</a>
        </li>
        <li>
          <a href="hh81352270s.html#64">Honorio III. consacrò la
          chiesa di ›Santa Bibiana‹</a>
        </li>
        <li>
          <a href="hh81352270s.html#64">Cimiterio di
          Sant'Anastasio Papa all'Orso Pileato ›Ursus Pileatus‹</a>
        </li>
        <li>
          <a href="hh81352270s.html#67">›Colonna di Santa
          Bibiana‹</a>
        </li>
        <li>
          <a href="hh81352270s.html#70">Chiesa di ›Santa Bibiana‹
          unita al Capitolo di ›Santa Maria Maggiore‹</a>
        </li>
        <li>
          <a href="hh81352270s.html#74">Urbano VIII. fa restaurare
          la Chiesa di ›Santa Bibiana‹</a>
        </li>
        <li>
          <a href="hh81352270s.html#79">Stato presente della
          Chiesa di ›Santa Bibiana‹</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="hh81352270s.html#88">Tavola delle cose notabili</a>
    </li>
  </ul>
  <hr />
</body>
</html>
