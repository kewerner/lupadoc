<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katpstr536951201s.html#12">Pièces de choix de la
      collection Comte Grégoire Stroganoff à Rome I</a>
      <ul>
        <li>
          <a href="katpstr536951201s.html#30">Premiére partie:
          Les antiques</a>
          <ul>
            <li>
              <a href="katpstr536951201s.html#32">Les
              marbres</a>
            </li>
            <li>
              <a href="katpstr536951201s.html#48">Les
              bronzes</a>
            </li>
            <li>
              <a href=
              "katpstr536951201s.html#64">L'orfèvrerie</a>
            </li>
            <li>
              <a href=
              "katpstr536951201s.html#78">L'argenterie</a>
            </li>
            <li>
              <a href="katpstr536951201s.html#84">Les vases</a>
            </li>
            <li>
              <a href="katpstr536951201s.html#94">Les terres
              cuites</a>
            </li>
            <li>
              <a href="katpstr536951201s.html#104">Les ivoires
              ambres, peintures et verres</a>
            </li>
            <li>
              <a href="katpstr536951201s.html#114">Tables</a>
            </li>
            <li>
              <a href="katpstr536951201s.html#126">Planches</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
