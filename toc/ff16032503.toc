<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ff16032503s.html#8">Voyage au Levant</a>
      <ul>
        <li>
          <a href="ff16032503s.html#10">[Text]</a>
          <ul>
            <li>
              <a href="ff16032503s.html#10">XXXVII. Dans le
              Principaux endroits de l'Asie Mineure, dans les Isles
              de l'Archipel; en Egypte, Syrie, Palestine, et c,</a>
            </li>
            <li>
              <a href="ff16032503s.html#31">XXXVIII. Description
              circonstanciée de la fameuse Ville du Caire, Capitale
              d'Egypte</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#34">Vil[l]e du Caire
                  ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#34">Vista di Cairo
                  ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#34">Puteus Josephi
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#52">XXXIX. Description du
              Chateau du Caire</a>
            </li>
            <li>
              <a href="ff16032503s.html#63">XL. Quantité
              d'Aveugles au Caire [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#67">Obeliscus ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#88">XLI. Du Nil; de la
              source, et de son accroissement</a>
            </li>
            <li>
              <a href="ff16032503s.html#120">XLII. Départ du
              Caire pour aller à Alexandrie [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#131">XLIII. Description
              de la Ville d'Alexandrie, et de ce qui est aux
              environs</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#132">Alexandria ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#132">Palatium
                  Cleopatrae ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#132">Turrium interior
                  aspectus ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#132">Turrium exterior
                  aspectus ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#143">Chiesa del
                  Santissimo Sepolcro ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#151">XLIV. Départ
              d'Alexandrie [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#157">XLV. Second départ
              du Caire. Arrivée à Damiette [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#160">Ioppe ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#167">XLVI. Départ de
              Jaffa pour aller à Rama [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#186">XLVII. L'Auteur
              monte de Rama à Jerusalem</a>
            </li>
            <li>
              <a href="ff16032503s.html#190">XLVIII. Enumération,
              et courte description des lieux Saints, qui sont tant
              dans la ville de Jerusalem qu'aux environs</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#205">Sepolchro di
                  Zacharia ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#205">Sepolchro
                  d'Absalon ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#205">Grotta di
                  Gieremia ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#205">Prigione di
                  Gieremia ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#219">XLIX. Voyage à
              Bethanie, et à quelques autres Lieux Saints [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#224">L. Voyage à Bethléem
              [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#228">Grotta di
                  Giovanni ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#228">Fonte di Palippo
                  ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#228">Fonte Signato
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#236">LI. Vojage à la
              Fontaine Seellée [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#242">Grotta di Davide
                  ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#242">Rouine d'Encaddi
                  ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#242">Bethlehem ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#242">Sepolcro di
                  Rachele ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#242">Rouine del Torre
                  di Giacobbe ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#242">Rouine del
                  propheta Habakuko ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#242">Torre di Simeone
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#254">LII. Retour de
              Bethléem à Jerusalem [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#261">Rovine del
                  palazzo di Davide ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#261">Rose di
                  Gierichonte ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#261">Cedri di Caza
                  ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#261">Monte Libano
                  ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#261">Vista presso di
                  Tripoli ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#264">LIII. L'Auteur va à
              la Montagne des Oliviers pour dessiner la ville de
              Jerusalem, sa forme extérieure [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#266">Ierusalem ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#266">Tripolis ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#266">Buon Ladron
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#270">LIV. Descriptione de
              l'Eglise di Saint Sepulchre [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#272">Chiesa del
                  Santissimo Sepolcro ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#272">Santissimo
                  Sepolcro ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#276">Choro della
                  chiesa del Sepolchro ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#276">Il Santissimo
                  Sepolchro die dientro ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#296">LV. Description de
              la ville de Jerusalem [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#312">LVI. Attestations
              qu'on donne aux Pelerins [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#318">LVII. Voyage à la
              Montagne du Liban</a>
            </li>
            <li>
              <a href="ff16032503s.html#333">LVIII. Pierres dans
              lesquelles il paroit des resemblances de Poissons
              [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#335">Chiesa di S.t
                  Juan d'Akari ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#341">LIX. Départ de
              Tripoli. Kaifa. Montagne de Carmel [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#347">LX. Voyage de Saint
              Jean d'Acre à Nazareth [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#349">Nazareth ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#349">Grotta della
                  Annonciata ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#349">Grotta del
                  presepio ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#349">Chiesa di Cana
                  ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#349">Monte delle
                  Beatitudini &#160;▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#356">LXI. Voyage de
              Nazareth à la Mer de Galilée [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#366">Tabor ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#366">Grotta del Monte
                  Tabor ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#366">Vista nel Monte
                  Tabor ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#366">Rovine della
                  chiesa di Tiro ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#366">Aleppo ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#371">LXII. Départ de
              Nazareth. Sefora. Vallée de Zabulon, et montagnes de
              Damas [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#374">LXIII. Départ d'Acre
              [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#389">LXIV. L'Auteur part
              de Tripoli pour aller à Allep [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#395">LXV. Description de
              la ville d'Alep [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#405">LXVI. Départ d'une
              Caravane pour la Mecque [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#409">LXVII. Histoire de
              Milheym Prince Arabe</a>
            </li>
            <li>
              <a href="ff16032503s.html#418">LXVIII. Description
              de la ville de Tadmor, ou Palmyre</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#420">Palmyra alias
                  Tadmor ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#498">LXIX. Description de
              quelques Mèdailles, et de queleques Pierres qu'on
              trouve à Alep [...]</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#509">Saint
                  Chrisostomo ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#509">Monastero de la
                  Pays ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#511">LXX. Départ d'Alep.
              Arrivée à Alexandrette ou Scanderone [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#518">LXX. (sic!) L'Auteur
              va voir l'Isle de Chypre [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#536">LXXII. Voyage à
              Chiti. Mosquée où est le Sepulchre de la Mere de
              Mahomett [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#553">LXXIII. Départ de
              l'Isle de Chypre [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#564">LXXIV. Description
              de la Ville de Sattalia</a>
              <ul>
                <li>
                  <a href="ff16032503s.html#569">Sattalia ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#569">Hasellaar ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#569">Grandine ▣</a>
                </li>
                <li>
                  <a href="ff16032503s.html#569">Gerbo ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ff16032503s.html#571">LXXV. Départ de
              Sattalia, et retour à Smyrne</a>
            </li>
            <li>
              <a href="ff16032503s.html#581">LXXXVI. Etrange
              avanture qui arrive à l'Auteur, qui à cause de la
              ressemblance des noms, fut pris pour un de ceux qui
              assassinérent le Pensionnaire de Wit</a>
            </li>
            <li>
              <a href="ff16032503s.html#588">LXXVII. L'Auteur
              part de Smyrne pour aller à Venise [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#601">LXXVIII. Arrivée à
              Venise [...]</a>
            </li>
            <li>
              <a href="ff16032503s.html#606">LXXIX. Départ de
              Venise, et retour à la Haye</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ff16032503s.html#614">[Tables]</a>
          <ul>
            <li>
              <a href="ff16032503s.html#614">Tables des
              chapitres</a>
            </li>
            <li>
              <a href="ff16032503s.html#618">Tables des
              matieres</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
