<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501560s.html#6">Le Antichità De La Città Di
      Roma</a>
      <ul>
        <li>
          <a href="dg4501560s.html#7">Tavola de l'ordine, e de'
          capitoli de le antichità de la città di Roma&#160;</a>
        </li>
        <li>
          <a href="dg4501560s.html#8">All'ilustrissimo et
          honoratissimo Signore, il Signor Giulio Martinengo dalla
          Palada</a>
        </li>
        <li>
          <a href="dg4501560s.html#15">Tavola per alphabeto de'
          luoghi di queste Antichità di Roma</a>
        </li>
        <li>
          <a href="dg4501560s.html#23">Tavola de' luoghi, dove le
          statue sono, secondo l'ordine del libro</a>
        </li>
        <li>
          <a href="dg4501560s.html#28">Tavola de' nomi de le
          statue, che si dechiarano chi fussero, per alphabeto</a>
        </li>
        <li>
          <a href="dg4501560s.html#30">Le antichità della città di
          Roma</a>
          <ul>
            <li>
              <a href="dg4501560s.html#34">I.Del colle Capitolino,
              che vi furono, ò che hora vi sono&#160;</a>
            </li>
            <li>
              <a href="dg4501560s.html#41">II. Del Colle Palatino
              con tutte le cose sue antiche</a>
            </li>
            <li>
              <a href="dg4501560s.html#47">III. Del Foro Romano,
              del Comitio, de l'arco di Costantino, del Coliseo, e
              de la casa di Nerone</a>
            </li>
            <li>
              <a href="dg4501560s.html#62">IIII. Di quattro altri
              Fori, di Cesare, di Augusto, di Nerva, et di
              Traiano</a>
            </li>
            <li>
              <a href="dg4501560s.html#65">V. De la valle, che è
              tra il Campidoglio, e'l Palatino; del Foro Olitorio,
              e del Buario, et del Circo Maßimo</a>
            </li>
            <li>
              <a href="dg4501560s.html#76">VI. Del Settizonio di
              Severo, de la strada Appia, e porta Capena, con ciò
              che vi era</a>
            </li>
            <li>
              <a href="dg4501560s.html#81">VII. Del piano di
              Testaccio con ciò che vi era</a>
            </li>
            <li>
              <a href="dg4501560s.html#84">VIII. Del Colle
              Aventino con tutti i suoi luoghi antichi, e
              moderni</a>
            </li>
            <li>
              <a href="dg4501560s.html#87">IX. Del Celiolo, e del
              Celio co' luoghi loro antichi, ò moderni</a>
            </li>
            <li>
              <a href="dg4501560s.html#96">X. Del Colle
              dell'Esquilie, co' luoghi, che vi furono, ò vi
              sono</a>
            </li>
            <li>
              <a href="dg4501560s.html#105">XI. Del Colle Viminale
              con tutti i suoi luoghi</a>
            </li>
            <li>
              <a href="dg4501560s.html#108">XII. Del Colle
              Quirinale. e del monte de gli Hortoli co' luoghi
              loro&#160;</a>
            </li>
            <li>
              <a href="dg4501560s.html#118">XIII. De' luoghi della
              città piana, e fra gli altri, del Circo Flaminio, e
              del Theatro di Pompeio&#160;</a>
            </li>
            <li>
              <a href="dg4501560s.html#122">XIIII. De' luoghi del
              Campo Martio, e del Pantheone, del Circo chiamato in
              Agone, e della palude Caprea&#160;</a>
            </li>
            <li>
              <a href="dg4501560s.html#131">XV. Di Trastevere, e
              de' luoghi suoi; e dell'Isola co' ponti, che sono sul
              Tevere da quetsa parte</a>
            </li>
            <li>
              <a href="dg4501560s.html#138">XVI. Di tutti i luoghi
              di Vaticano, che chiamano hoggi in Borgo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501560s.html#144">Delle statue antiche, che
          per tutta Roma, in diversi luoghi, et case si veggono</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
