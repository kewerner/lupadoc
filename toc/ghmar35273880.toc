<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghmar35273880s.html#8">Lettere pittoriche perugine
      o sia Ragguaglio di alcune memorie istoriche risguardanti le
      arti del disegno in Perugia al Signor Baldassarre Orsini
      ...</a>
      <ul>
        <li>
          <a href="ghmar35273880s.html#10">Indice delle
          Lettere</a>
        </li>
        <li>
          <a href="ghmar35273880s.html#12">Lettera prima</a>
        </li>
        <li>
          <a href="ghmar35273880s.html#48">Lettera II.</a>
        </li>
        <li>
          <a href="ghmar35273880s.html#71">Lettera III.</a>
        </li>
        <li>
          <a href="ghmar35273880s.html#94">Lettera IV.</a>
        </li>
        <li>
          <a href="ghmar35273880s.html#127">Lettera V.</a>
        </li>
        <li>
          <a href="ghmar35273880s.html#150">Lettera VI.</a>
        </li>
        <li>
          <a href="ghmar35273880s.html#186">Lettera VII.</a>
        </li>
        <li>
          <a href="ghmar35273880s.html#201">Lettera VIII.</a>
        </li>
        <li>
          <a href="ghmar35273880s.html#222">Lettera VIIII.
          [sic]</a>
        </li>
        <li>
          <a href="ghmar35273880s.html#284">Indice</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
