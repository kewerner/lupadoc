<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501891s.html#6">Las cosas Maravillosas dela
      Sancta Ciudad de Roma</a>
    </li>
    <li>
      <a href="dg4501891s.html#8">Las siete yglesia
      principales</a>
      <ul>
        <li>
          <a href="dg4501891s.html#8">La primera Yglesia es de San
          Juan de Letran ›San Giovanni in Laterano‹ ▣</a>
        </li>
        <li>
          <a href="dg4501891s.html#11">La segunda Yglesia es San
          Pedro en Vaticano ›San Pietro in Vaticano‹ ▣</a>
        </li>
        <li>
          <a href="dg4501891s.html#14">La tercera Yglesia es San
          Pablo ›San Paolo fuori le Mura‹ ▣</a>
        </li>
        <li>
          <a href="dg4501891s.html#15">La quarta Yglesia, es
          Sancta Maria Maior ›Santa Maria Maggiore‹ ▣</a>
        </li>
        <li>
          <a href="dg4501891s.html#17">La quinta Yglesia es San
          Lorenzo fuera de los muros ›San Lorenzo fuori le Mura‹
          ▣</a>
        </li>
        <li>
          <a href="dg4501891s.html#18">La sesta Yglesia es de San
          Sebastiano ›San Sebastiano‹ ▣</a>
        </li>
        <li>
          <a href="dg4501891s.html#18">La setima Yglesia ed Sancta
          Cruz en Ierusalem ›Santa Croce in Gerusalemme‹ ▣</a>
        </li>
        <li>
          <a href="dg4501891s.html#19">La Ygelsia de Sancta Maria
          del Pueblo, en lugar de San Sebastian ›Santa Maria del
          Popolo‹ ▣</a>
        </li>
        <li>
          <a href="dg4501891s.html#24">En la Isla</a>
        </li>
        <li>
          <a href="dg4501891s.html#25">En Transtiber</a>
        </li>
        <li>
          <a href="dg4501891s.html#29">En el Burgo</a>
        </li>
        <li>
          <a href="dg4501891s.html#32">De la Puerta Flaminia del
          Populo hasta la raiz del Campidolio</a>
        </li>
        <li>
          <a href="dg4501891s.html#47">Del Campidolio a mano
          izquierda hazia los Montes</a>
        </li>
        <li>
          <a href="dg4501891s.html#54">Del Campidolio a mano
          derecha hazia los Montes</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501891s.html#59">Las estaciones, y indulgencias
      que ay en las yglesias de Roma</a>
      <ul>
        <li>
          <a href="dg4501891s.html#60">Las estaciones dela
          Quaresima</a>
        </li>
        <li>
          <a href="dg4501891s.html#65">Las estationes del
          adviento</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501891s.html#66">Tratado o modo para ganar las
      indulgencias en las estaciones</a>
    </li>
    <li>
      <a href="dg4501891s.html#70">La guia romana para todos los
      foresteros que vienen a Roma</a>
      <ul>
        <li>
          <a href="dg4501891s.html#70">Iornada primera</a>
          <ul>
            <li>
              <a href="dg4501891s.html#70">Del Trastiber
              ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4501891s.html#71">Dela Isla Tiberina
              ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501891s.html#71">De la Puente Sancta
              Maria ›Ponte Rotto‹, del palacio de Pilato ›Casa dei
              Crescenzi‹ y otras cosas</a>
            </li>
            <li>
              <a href="dg4501891s.html#72">Del monte Testacio
              ›Testaccio (monte)‹, de muchas otras cosas</a>
            </li>
            <li>
              <a href="dg4501891s.html#72">De los Vagnos
              Antonianos ›Terme di Caracalla‹, y de otras cosas</a>
            </li>
            <li>
              <a href="dg4501891s.html#72">De Sancto Iuan de
              Latran ›San Giovanni in Laterano‹, y de Sancta Cruz
              ›Santa Croce in Gerusalemme‹, y otros lugares</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501891s.html#73">Iornada segunda</a>
          <ul>
            <li>
              <a href="dg4501891s.html#73">De la Puerta del Pueblo
              ›Porta del Popolo‹</a>
            </li>
            <li>
              <a href="dg4501891s.html#73">De los cavallos de
              marmor, los quales estan a monte cavallo ›Fontana di
              Monte Cavallo‹, y de las Thermas Diocletianas ›Terme
              di Diocleziano‹</a>
            </li>
            <li>
              <a href="dg4501891s.html#74">Della calle Pia ›Via XX
              Settembre‹, y dela vigna que era del Cardenal de
              Ferrara, y dela de Carpi, y otras cosas</a>
            </li>
            <li>
              <a href="dg4501891s.html#74">Dela Puerta Pia ›Porta
              Pia‹</a>
            </li>
            <li>
              <a href="dg4501891s.html#74">De Sancta Ines
              ›Sant'Agnese fuori le Mura‹, y de otras
              antiguedades</a>
            </li>
            <li>
              <a href="dg4501891s.html#75">De Templo de Isisde, y
              otras cosas ›Tempio di Isis Patricia‹</a>
            </li>
            <li>
              <a href="dg4501891s.html#75">Delas Siete salas
              ›Sette Sale‹, y del Coliseo ›Colosseo‹</a>
            </li>
            <li>
              <a href="dg4501891s.html#76">Del Templo dela paz
              ›Foro della Pace‹, y del Monte palatino aora dicho
              palatio mayor ›Palatino‹</a>
            </li>
            <li>
              <a href="dg4501891s.html#76">Del Campidolio
              ›Campidoglio‹, y otras cosas</a>
            </li>
            <li>
              <a href="dg4501891s.html#77">De los Portales de
              Octavio, y de Septimio ›Portico di Ottavia‹, y del
              Theatro de Pompeo ›Teatro di Pompeo‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501891s.html#77">Iornada tercera</a>
          <ul>
            <li>
              <a href="dg4501891s.html#77">Delas dos Collunnas de
              Antonino Pio ›Colonna di Antonino Pio‹, y la otra de
              Traiano ›Colonna Traiana‹, y otras cosas</a>
            </li>
            <li>
              <a href="dg4501891s.html#78">De la Ridonda, o
              ›Pantheon‹</a>
            </li>
            <li>
              <a href="dg4501891s.html#78">Del los Bagnos de
              Agrippa ›Terme di Agrippa‹, y de Neron ›Terme
              Neroniano-Alessandrine‹</a>
            </li>
            <li>
              <a href="dg4501891s.html#78">Dela plaza Maestra
              ›Piazza Navona‹, y de Maestro Pasquino ›Statua di
              Pasquino‹</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501891s.html#79">Los nombres de todos los
          pontifices romanos, empereadores, reyes, y duques</a>
        </li>
        <li>
          <a href="dg4501891s.html#103">Los reyes del reyno de
          Napoles, y de Sicilia</a>
        </li>
        <li>
          <a href="dg4501891s.html#104">Los doques de Venecia</a>
        </li>
        <li>
          <a href="dg4501891s.html#107">Tabla de las Yglesias de
          Roma, que son en este libro</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501891s.html#110">Las antiguidades de Roma</a>
      <ul>
        <li>
          <a href="dg4501891s.html#111">De la edificacion de
          Roma</a>
        </li>
        <li>
          <a href="dg4501891s.html#113">Del circuito de Roma</a>
        </li>
        <li>
          <a href="dg4501891s.html#113">De las Puertas</a>
        </li>
        <li>
          <a href="dg4501891s.html#114">De las calles de Roma</a>
        </li>
        <li>
          <a href="dg4501891s.html#115">De las puentes que estan
          sobre el rio Tiber, y sus edificadores</a>
        </li>
        <li>
          <a href="dg4501891s.html#116">De la Isla del Tiber
          ›Isola Tiberina‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#116">De los Montes</a>
        </li>
        <li>
          <a href="dg4501891s.html#117">Del monte Testacio
          ›Testaccio (monte)‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#117">Delas aguas, y quien las
          trayo a Roma</a>
        </li>
        <li>
          <a href="dg4501891s.html#118">Dela Cloacha ›Cloaca
          Maxima‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#119">Delos Conductos de
          Agua</a>
        </li>
        <li>
          <a href="dg4501891s.html#119">De las siete Salas ›Sette
          Sale‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#119">De las Thermas, o Banos y
          de sus edificadores</a>
        </li>
        <li>
          <a href="dg4501891s.html#121">De los Naumachias, donde
          se hazian las batallas Navales, y que cosa eran</a>
        </li>
        <li>
          <a href="dg4501891s.html#121">Delos Circos, y que cosa
          eran</a>
        </li>
        <li>
          <a href="dg4501891s.html#121">De los Theatros, y que
          cosa eran, y de sus edificadores</a>
        </li>
        <li>
          <a href="dg4501891s.html#122">Delos Amphitheatros, y
          edificadores dellos, y que cosa eran</a>
        </li>
        <li>
          <a href="dg4501891s.html#122">De los Foros, o Plazas</a>
        </li>
        <li>
          <a href="dg4501891s.html#123">Delos Arcos Triumphales, y
          a quien se davan</a>
        </li>
        <li>
          <a href="dg4501891s.html#124">Delos Porticos</a>
        </li>
        <li>
          <a href="dg4501891s.html#124">Delos Tropheos y Colunnas
          memorables</a>
        </li>
        <li>
          <a href="dg4501891s.html#125">De los Colosos</a>
        </li>
        <li>
          <a href="dg4501891s.html#125">De la Piramide ›Piramide
          di Caio Cestio‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#125">Delas Metas</a>
        </li>
        <li>
          <a href="dg4501891s.html#125">Delos Obeliscos o bien
          Agujas</a>
        </li>
        <li>
          <a href="dg4501891s.html#126">De las Statuas</a>
        </li>
        <li>
          <a href="dg4501891s.html#126">De Marforio ›Statua di
          Marforio‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#126">De los Cavallos</a>
        </li>
        <li>
          <a href="dg4501891s.html#127">De las Librarias</a>
        </li>
        <li>
          <a href="dg4501891s.html#127">De los Reloxes</a>
        </li>
        <li>
          <a href="dg4501891s.html#127">De los Palacios</a>
        </li>
        <li>
          <a href="dg4501891s.html#127">De la casa aurea de Neron
          ›Domus Aurea‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#128">De la Casas de
          Ciudadanos</a>
        </li>
        <li>
          <a href="dg4501891s.html#129">De las Curias, y que cosa
          eran</a>
        </li>
        <li>
          <a href="dg4501891s.html#129">De los Senados, y que cosa
          eran</a>
        </li>
        <li>
          <a href="dg4501891s.html#129">De los Magistrados</a>
        </li>
        <li>
          <a href="dg4501891s.html#130">De los Comicios, y que
          cosa eran</a>
        </li>
        <li>
          <a href="dg4501891s.html#130">Delos Tribus</a>
        </li>
        <li>
          <a href="dg4501891s.html#130">Delas Regiones, dichas
          Riones, y de sus Insignias ›Regiones Quattuordecim‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#131">Delas Basilicas, y que
          cosa eran</a>
        </li>
        <li>
          <a href="dg4501891s.html#131">Del Campidolio
          ›Campidoglio‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#132">Del Erario ›Erario
          militare, o del Campidoglio‹ obien camera del commun, y
          que moneda corria en Roma en a quel tiempo</a>
        </li>
        <li>
          <a href="dg4501891s.html#132">Dela secretaria del oueblo
          Romano</a>
        </li>
        <li>
          <a href="dg4501891s.html#132">Delo Asilo ›Asilo di
          Romolo‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#133">Dela Columna dicha
          milliaria ›Miliarium Aureum‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#133">Dela ›Columna Bellica‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#133">Del Campo Marcio ›Campo
          Marzio‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#133">Del Tigillo sororio
          ›Tigillum Sororium‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#133">Del Vivario ›Vivarium‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#133">Delos Huertos</a>
        </li>
        <li>
          <a href="dg4501891s.html#134">Del ›Velabro‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#134">Delas Carinas
          ›Carinae‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#134">Delos Clivios</a>
        </li>
        <li>
          <a href="dg4501891s.html#135">De las Carceres
          publicas</a>
        </li>
        <li>
          <a href="dg4501891s.html#135">De algunas Fiestas y
          juegos que se acostumbravan celebrar en Roma</a>
        </li>
        <li>
          <a href="dg4501891s.html#135">De los Sepulcros de
          Augusto, Adriano y de Septimio ›Mausoleo di Augusto‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#136">De los Templos</a>
        </li>
        <li>
          <a href="dg4501891s.html#137">Del Armamentario, y que
          cosa era</a>
        </li>
        <li>
          <a href="dg4501891s.html#137">Del exercitu Romano de
          tierra, y de mar, y de sus ensignias</a>
        </li>
        <li>
          <a href="dg4501891s.html#137">Del Triumphos, y a quein
          se concedian</a>
        </li>
        <li>
          <a href="dg4501891s.html#138">De la Coronas, y a quein
          se davan</a>
        </li>
        <li>
          <a href="dg4501891s.html#138">Del numero del Pueblo
          Romano</a>
        </li>
        <li>
          <a href="dg4501891s.html#139">Delas riquezas del Pueblo
          Romano</a>
        </li>
        <li>
          <a href="dg4501891s.html#139">De los Matrimonios
          antiguos, y de como se usavan</a>
        </li>
        <li>
          <a href="dg4501891s.html#139">De la buona crianza, que
          davan alos hijos</a>
        </li>
        <li>
          <a href="dg4501891s.html#140">Dela separacion de los
          Matrimonios</a>
        </li>
        <li>
          <a href="dg4501891s.html#140">Dellas Exequias antiguas,
          y sus eremonias</a>
        </li>
        <li>
          <a href="dg4501891s.html#141">De las Torres</a>
        </li>
        <li>
          <a href="dg4501891s.html#141">Del Tyber ›Tevere‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#142">Del Palacio Papal ›Palazzo
          Apostolico Vaticano‹, y Belveder ›Cortile del
          Belvedere‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#142">Del Transtiber
          ›Trastevere‹</a>
        </li>
        <li>
          <a href="dg4501891s.html#143">Quantas vezes a sido
          tomada Roma</a>
        </li>
        <li>
          <a href="dg4501891s.html#143">Delos fuegos, delos
          Antiguos</a>
        </li>
        <li>
          <a href="dg4501891s.html#144">Significacion delos
          Agnusdei</a>
        </li>
        <li>
          <a href="dg4501891s.html#145">Las virtudes delos
          Agnusdei</a>
        </li>
        <li>
          <a href="dg4501891s.html#147">Tabla de las antiguedades
          dela Sancta Ciudad de Roma</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
