<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zsbio48691310as.html#6">De Roma Triumphante</a>
      <ul>
        <li>
          <a href="zsbio48691310as.html#8">Rerum ac vocum
          memorabilium index</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#42">Ad Sanctiss. Patrem
          ad Dominum Pium II Pont. Max. epistola dedicata</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#42">Prooemium</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#44">Liber primus</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#71">Liber secundus</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#95">Liber tertius</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#122">Liber quartus</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#147">Liber quintus</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#166">Liber sextus</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#186">Liber septimus</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#201">Liber octavus</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#220">Liber nonus</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#243">Liber decimus</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="zsbio48691310as.html#259">De Roma Instaurata</a>
      <ul>
        <li>
          <a href="zsbio48691310as.html#259">Index, quemadmodum
          isthic in margine annotatum est</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#263">Praefatio ad
          Eugenium IIII pontificem maximum</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#264">Liber primus</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#282">Liber secundus</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#302">Liber tertius</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="zsbio48691310as.html#314">De Origine et Gestis
      Venetorum</a>
    </li>
    <li>
      <a href="zsbio48691310as.html#334">De Italia
      Illustrata</a>
      <ul>
        <li>
          <a href="zsbio48691310as.html#334">Praefatio</a>
        </li>
        <li>
          <a href="zsbio48691310as.html#335">Liber primus</a>
          <ul>
            <li>
              <a href="zsbio48691310as.html#336">Regio prima,
              Liguria</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#340">Regio secunda,
              Etruria</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#354">Regio tertia,
              Latina</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#369">Regio quarta,
              Umbria sive ducatus Spoletanus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#376">Regio quinta,
              Picenum sive Marchia Anconitana</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#383">Regio sexta,
              Romandiola sive Flaminia</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#397">Regio septima,
              Lombardia</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#409">Regio octava,
              Venetiae</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#415">Regio nona,
              Italia Transpadana, sive Marchia Tarvisina</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#425">Regio decima,
              Forumiulium</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#427">Regio undecima,
              Istria</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#430">Regio xii,
              Aprutium, sive Samnium, Campania, Apulia, Lucania,
              Salentini, Calabria &amp; Brutij</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#447">Regio
              tertiadecima, Campania</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#462">Regio
              quartadecima, Apulia</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="zsbio48691310as.html#466">De historia ab
      inclinatione Romanorum</a>
      <ul>
        <li>
          <a href="zsbio48691310as.html#468">Decades prima</a>
          <ul>
            <li>
              <a href="zsbio48691310as.html#468">Liber
              primus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#481">Liber
              secundus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#495">Liber
              tertius</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#508">Liber
              quartus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#522">Liber
              quintus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#536">Liber
              sextus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#550">Liber
              septimus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#566">Liber
              octavus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#582">Liber nonus</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zsbio48691310as.html#615">Decades secunda</a>
          <ul>
            <li>
              <a href="zsbio48691310as.html#615">Liber
              primus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#632">Liber
              secundus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#649">Liber
              tertius</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#680">Liber
              quartus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#703">Liber
              quintus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#720">Liber
              sextus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#741">Liber
              septimus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#769">Liber
              octavus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#798">Liber nonus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#823">Liber
              decimus</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zsbio48691310as.html#858">Decades tertia</a>
          <ul>
            <li>
              <a href="zsbio48691310as.html#858">Liber
              primus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#878">Liber
              secundus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#898">Liber
              tertius</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#915">Liber
              quartus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#931">Liber
              quintus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#946">Liber
              sextus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#962">Liber
              septimus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#979">Liber
              octavus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#993">Liber nonus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#1010">Liber
              decimus</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zsbio48691310as.html#1026">Decades quarta</a>
          <ul>
            <li>
              <a href="zsbio48691310as.html#1026">Liber
              primus</a>
            </li>
            <li>
              <a href="zsbio48691310as.html#1031">Liber
              undecimus [?]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zsbio48691310as.html#1046">Rerum ac vocum
          memorabilium index</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
