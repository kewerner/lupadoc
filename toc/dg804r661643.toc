<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg804r661643s.html#8">Descrittione di Roma antica
      e moderna</a>
      <ul>
        <li>
          <a href="dg804r661643s.html#10">Tavola de tutte le
          chiese dell'alma citta di Roma&#160;</a>
        </li>
        <li>
          <a href="dg804r661643s.html#17">Delli Titoli de
          Cardinali di Santa Chiesa&#160;</a>
        </li>
        <li>
          <a href="dg804r661643s.html#22">Le sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg804r661643s.html#22">La prima chiesa è
              San Giovanni in Laterano&#160;</a>
            </li>
            <li>
              <a href="dg804r661643s.html#29">La seconda chiesa
              è San Pietro in Vaticano</a>
            </li>
            <li>
              <a href="dg804r661643s.html#36">La quarta chiesa è
              Santa Maria Maggiore</a>
            </li>
            <li>
              <a href="dg804r661643s.html#41">La quinta chiesa è
              San Lorenzo fuori delle Mura</a>
            </li>
            <li>
              <a href="dg804r661643s.html#43">La sesta chiesa è
              San Sebastiano fuori delle Mura</a>
            </li>
            <li>
              <a href="dg804r661643s.html#45">La settima chiesa
              è Santa Croce in Gerusalemme</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg804r661643s.html#47">La chiesa di Santa
          Maria del Popolo è posta in questo luogo dietro le sette
          chiese</a>
        </li>
        <li>
          <a href="dg804r661643s.html#50">&#160;[chiese]</a>
        </li>
        <li>
          <a href="dg804r661643s.html#58">In Trastevere</a>
        </li>
        <li>
          <a href="dg804r661643s.html#79">Nel Borgo</a>
        </li>
        <li>
          <a href="dg804r661643s.html#96">Dalla Porta Flaminia,
          overo del Popolo à mano destra, e sinistra, fino alla
          Madonna delli Monti</a>
        </li>
        <li>
          <a href="dg804r661643s.html#162">Dal Giesù, Parione,
          strada Giulia, Regola, e restante infino Araceli</a>
        </li>
        <li>
          <a href="dg804r661643s.html#309">Dal Campidoglio da
          ogni parte, finendo a Sant'Agnese di Porta Pia&#160;</a>
        </li>
        <li>
          <a href="dg804r661643s.html#392">Le Stationi che sono
          ne le Chiese dentro, e fuori di Roma, sì per la
          Quaresima, e Avvento, come per tutto l'Anno</a>
        </li>
        <li>
          <a href="dg804r661643s.html#403">La Guida romana per
          li forastieri</a>
          <ul>
            <li>
              <a href="dg804r661643s.html#403">Giornata
              prima</a>
            </li>
            <li>
              <a href="dg804r661643s.html#409">Giornata
              seconda</a>
            </li>
            <li>
              <a href="dg804r661643s.html#415">Giornata
              terza</a>
            </li>
            <li>
              <a href="dg804r661643s.html#421">Indice brevissimo
              de Pontefici romani</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg804r661643s.html#442">L'Antichità figurate
          dell'alma città di Roma</a>
          <ul>
            <li>
              <a href="dg804r661643s.html#444">Dell'origine et
              progresso dell'alma città di Roma, et sue
              antichità</a>
            </li>
            <li>
              <a href="dg804r661643s.html#785">Catalogo delli Re
              et Imperatori Romani</a>
            </li>
            <li>
              <a href="dg804r661643s.html#789">Li Re di
              Francia</a>
            </li>
            <li>
              <a href="dg804r661643s.html#790">Li Dogi di
              Venetia</a>
            </li>
            <li>
              <a href="dg804r661643s.html#792">Duchi di
              Savoia</a>
            </li>
            <li>
              <a href="dg804r661643s.html#792">Duchi di
              Mantoa</a>
            </li>
            <li>
              <a href="dg804r661643s.html#793">Duchi di
              Fiorenza</a>
            </li>
            <li>
              <a href="dg804r661643s.html#793">Duchi di
              Modena</a>
            </li>
            <li>
              <a href="dg804r661643s.html#794">Appendice</a>
            </li>
            <li>
              <a href="dg804r661643s.html#801">Tavola delle cose
              più notabili</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
