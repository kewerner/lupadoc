<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbil42254920s.html#6">Il libro di Antonio Billi
      esistente in due copie nella Biblioteca Nazionale di
      Firenze</a>
      <ul>
        <li>
          <a href="ghbil42254920s.html#8">Avvertimento</a>
        </li>
        <li>
          <a href="ghbil42254920s.html#27">Indice della
          materia</a>
        </li>
        <li>
          <a href="ghbil42254920s.html#28">Pittorj</a>
        </li>
        <li>
          <a href="ghbil42254920s.html#29">Architettj Fiorentinj
          et Scultorj</a>
        </li>
        <li>
          <a href="ghbil42254920s.html#81">Aggiunte
          dell'editore</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
