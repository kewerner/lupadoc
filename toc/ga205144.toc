<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ga205144s.html#8">Die Kunstliteratur</a>
      <ul>
        <li>
          <a href="ga205144s.html#12">Werter Freund!</a>
        </li>
        <li>
          <a href="ga205144s.html#16">Inhaltsübersicht</a>
        </li>
        <li>
          <a href="ga205144s.html#22">Vorerinnerung</a>
        </li>
        <li>
          <a href="ga205144s.html#28">1. Das Mittelalter&#160;</a>
          <ul>
            <li>
              <a href="ga205144s.html#30">Einleitung</a>
            </li>
            <li>
              <a href="ga205144s.html#34">I. Die mittelalterliche
              Kunstliteratur (Überblick)</a>
              <ul>
                <li>
                  <a href="ga205144s.html#34">1. Im griechischen
                  Osten&#160;</a>
                </li>
                <li>
                  <a href="ga205144s.html#41">2. Im lateinischen
                  Westen&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ga205144s.html#66">II. Zur Kunsttheorie des
              Mittelalters</a>
              <ul>
                <li>
                  <a href="ga205144s.html#66">1. Kunsttheoretische
                  Ergebnisse des Altertums&#160;</a>
                </li>
                <li>
                  <a href="ga205144s.html#80">2. Das Erbe des
                  Altertums im Mittelalter&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ga205144s.html#88">III. Theorie und Praxis
              im toskanischen Trecento</a>
              <ul>
                <li>
                  <a href="ga205144s.html#88">1. Zu Dantes
                  Kunstlehre&#160;</a>
                </li>
                <li>
                  <a href="ga205144s.html#98">2. Die Werkstatt des
                  Trecento&#160;</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="ga205144s.html#106">2. Die Frührenaissance.
          Leonardos Vermächtnis&#160;</a>
          <ul>
            <li>
              <a href="ga205144s.html#108">I. Die historische
              Literatur</a>
              <ul>
                <li>
                  <a href="ga205144s.html#108">1. Lorenzo
                  Ghiberti&#160;</a>
                </li>
                <li>
                  <a href="ga205144s.html#113">2. Die übrigen
                  historischen Schriften des Quattrocento</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ga205144s.html#126">II. Die Theoretiker der
              Frührenaissance</a>
              <ul>
                <li>
                  <a href="ga205144s.html#126">(1. L. B.
                  Alberti)</a>
                </li>
                <li>
                  <a href="ga205144s.html#133">2. Die Romantiker
                  der Frührenaissance</a>
                </li>
                <li>
                  <a href="ga205144s.html#141">3. Die strengen
                  Theoretiker der Frührenaissance</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ga205144s.html#151">III. Die historischen
              Thesen der Früuhrenaissance</a>
            </li>
            <li>
              <a href="ga205144s.html#154">IV. Zu den
              kunsttheoretischen Thesen der Frührenaissance</a>
            </li>
            <li>
              <a href="ga205144s.html#161">V. Leonardos
              Vermächtnis</a>
              <ul>
                <li>
                  <a href="ga205144s.html#161">1. Einleitung</a>
                </li>
                <li>
                  <a href="ga205144s.html#164">2.
                  Bibliographie</a>
                </li>
                <li>
                  <a href="ga205144s.html#171">3. Zu Leonardos
                  Kunstlehre</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="ga205144s.html#186">3. Die
          Kunstgeschichtschreibung vor Vasari&#160;</a>
          <ul>
            <li>
              <a href="ga205144s.html#188">I. Die Vorläufer
              Vasaris</a>
              <ul>
                <li>
                  <a href="ga205144s.html#188">1. Das Buch des
                  Antonio Billi</a>
                </li>
                <li>
                  <a href="ga205144s.html#189">2. Der Anonymus der
                  Magliabecchiana. Gelli. Giovio. Wirkliche und
                  angebliche Quellen Vasaris</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ga205144s.html#199">II. Erste Ansätze zur
              Kunstgeschichtschreibung außerhalb Italiens</a>
            </li>
            <li>
              <a href="ga205144s.html#204">III. Die
              Kunsttopographie. Beginn der
              Guidenliteratur&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ga205144s.html#218">4. Die Kunsttheorie der
          ersten Hälfte des sechzehnten Jahrhunderts</a>
          <ul>
            <li>
              <a href="ga205144s.html#220">I. Die Kunstteheorie
              Mittelitaliens vor Vasari</a>
            </li>
            <li>
              <a href="ga205144s.html#226">II. Oberitalienische
              Theoretiker</a>
            </li>
            <li>
              <a href="ga205144s.html#240">III. Fortsetzung der
              vitruvianischen Studien</a>
            </li>
            <li>
              <a href="ga205144s.html#247">IV. Erste Fernwirkung
              der italienischen Theorie auf das Ausland</a>
              <ul>
                <li>
                  <a href="ga205144s.html#247">1. Viator</a>
                </li>
                <li>
                  <a href="ga205144s.html#252">2. Dürer</a>
                </li>
                <li>
                  <a href="ga205144s.html#263">3. Deutsche
                  Kunstbücher</a>
                </li>
                <li>
                  <a href="ga205144s.html#267">4. Francisco de
                  Hollanda</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="ga205144s.html#272">5. Vasari</a>
          <ul>
            <li>
              <a href="ga205144s.html#274">Einleitung</a>
            </li>
            <li>
              <a href="ga205144s.html#276">I.
              Entstehungsgeschichte der Viten. Verhältnis der
              ersten zur zweiten Auflage</a>
            </li>
            <li>
              <a href="ga205144s.html#279">II. Die Quellen
              Vasaris</a>
              <ul>
                <li>
                  <a href="ga205144s.html#279">1. Eigentlich
                  kunsthistorische Quellen</a>
                </li>
                <li>
                  <a href="ga205144s.html#283">2. Historische
                  Literatur</a>
                </li>
                <li>
                  <a href="ga205144s.html#284">3. Sonstige
                  Literatur</a>
                </li>
                <li>
                  <a href="ga205144s.html#284">4. Mündliche
                  Überlieferung. Vasaris Denkmälerkenntnis und
                  Autopsie</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ga205144s.html#286">III. Vasaris
              geschichtliche Orientierung und Arbeitstechnik</a>
              <ul>
                <li>
                  <a href="ga205144s.html#286">1. Der
                  Geschichtsbegriff der Renaissance</a>
                </li>
                <li>
                  <a href="ga205144s.html#289">2. Vasaris
                  historische Absichten</a>
                </li>
                <li>
                  <a href="ga205144s.html#292">3. Vasaris
                  historische &#160;Arbeitstechnik und Stilkritik
                  im einzelnen</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ga205144s.html#298">IV. Vasaris historische
              Gesamtansicht</a>
            </li>
            <li>
              <a href="ga205144s.html#306">V. Vasaris ästhetischer
              und kunstkritischer Standpunkt&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ga205144s.html#326">6. Die Kunstliteratur der
          Manieristenzeit</a>
          <ul>
            <li>
              <a href="ga205144s.html#328">I. Historik und
              Periegese</a>
            </li>
            <li>
              <a href="ga205144s.html#359">II. Die
              kunsttheoretischen Schriften des Manierismus</a>
              <ul>
                <li>
                  <a href="ga205144s.html#359">(Überblick)</a>
                </li>
                <li>
                  <a href="ga205144s.html#360">1. Der
                  Toskanisch-Römische Umkreis</a>
                </li>
                <li>
                  <a href="ga205144s.html#370">2. Oberitalien</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ga205144s.html#381">III. Die Lehrer der
              Baukunst</a>
            </li>
            <li>
              <a href="ga205144s.html#399">IV. Die Moralisten</a>
            </li>
            <li>
              <a href="ga205144s.html#406">V. Die Kunsttheorie des
              Manierismus in ihren Grundzügen</a>
              <ul>
                <li>
                  <a href="ga205144s.html#406">1. Ansichten vom
                  Wesen der Kunst</a>
                </li>
                <li>
                  <a href="ga205144s.html#410">2. Vorherrschen des
                  Intellektualismus</a>
                </li>
                <li>
                  <a href="ga205144s.html#414">3. Die Lehre von
                  der "künstlerischen Idee"</a>
                </li>
                <li>
                  <a href="ga205144s.html#416">4. Verhätlnis der
                  Kunst zur "Schönheit"</a>
                </li>
                <li>
                  <a href="ga205144s.html#420">5. Grundsätze der
                  Kunstkritik</a>
                </li>
                <li>
                  <a href="ga205144s.html#423">6. Die Lehre von
                  den Genres und Stilgesetzen</a>
                </li>
                <li>
                  <a href="ga205144s.html#425">7. Der Gedanke des
                  "Klassischen"</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="ga205144s.html#428">7. Die Geschichtschreibung
          des Barock und des Klassizismus</a>
          <ul>
            <li>
              <a href="ga205144s.html#430">Einleitung</a>
            </li>
            <li>
              <a href="ga205144s.html#431">I. Die
              römisch-florentinische Universaltheorie</a>
            </li>
            <li>
              <a href="ga205144s.html#446">II. Die
              Kunsthistoriographie im übrigen Europa. Die
              Geschichtschreibung des italienischen
              Klassizismus</a>
            </li>
            <li>
              <a href="ga205144s.html#470">III. Einige Bemerkungen
              zum Gesamtcharakter der Kunstgeschichtschreibung des
              Barock und Klassizismus</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ga205144s.html#484">8. Die italienische
          Ortsliteratur</a>
          <ul>
            <li>
              <a href="ga205144s.html#486">I. Die örtliche
              Kunstgeschichtschreibung Italiens</a>
            </li>
            <li>
              <a href="ga205144s.html#493">II. Die Literatur der
              Ciceroni</a>
            </li>
            <li>
              <a href="ga205144s.html#513">III. Bibliographie der
              Ortsliteratur Italiens</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ga205144s.html#552">9. Die Kunstlehre des
          siebzehnten und achtzehnten Jahrhunderts</a>
          <ul>
            <li>
              <a href="ga205144s.html#554">I. Die italienische
              Kunsttheorie des 17. Jahrhunderts</a>
            </li>
            <li>
              <a href="ga205144s.html#568">II. Die Kunsttheorie
              des 17. Jahrhunderts in Frankreich</a>
            </li>
            <li>
              <a href="ga205144s.html#578">III. Die Kunsttheorie
              des Barock in den übrigen Ländern</a>
            </li>
            <li>
              <a href="ga205144s.html#583">IV. Die Kunsttheorie
              des 18. Jahrunderts außerhalb Italiens</a>
            </li>
            <li>
              <a href="ga205144s.html#596">V. Die italienische
              Kunstlehre des 18. Jahrhunderts</a>
            </li>
            <li>
              <a href="ga205144s.html#612">VI. Einige Bemerkungen
              zur Kunsttheorie des Barock&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ga205144s.html#632">Register</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
