<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502790s.html#4">Roma Avanti, e doppo Romolo</a>
      <ul>
        <li>
          <a href="dg4502790s.html#6">[Dedica dell'autore] Signore
          [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502790s.html#8">[Text]</a>
      <ul>
        <li>
          <a href="dg4502790s.html#8">Origine d'Italia. Parte
          prima.</a>
        </li>
        <li>
          <a href="dg4502790s.html#38">Fondatione di Roma di
          Romolo. Parte seconda.</a>
          <ul>
            <li>
              <a href="dg4502790s.html#131">Dell'Isola
              Trasteverina ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4502790s.html#143">Delli Ponti</a>
            </li>
            <li>
              <a href="dg4502790s.html#145">Delli Colli di
              Roma</a>
            </li>
            <li>
              <a href="dg4502790s.html#145">I Sacchi di Roma</a>
            </li>
            <li>
              <a href="dg4502790s.html#147">Della Separatione
              degli Antichi Matrimonij</a>
            </li>
            <li>
              <a href="dg4502790s.html#148">Delle Esequie de'
              Romani antichi</a>
            </li>
            <li>
              <a href="dg4502790s.html#150">Delle ricchezze degli
              antichi Romani</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
