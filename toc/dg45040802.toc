<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg45040802s.html#6">L’osservatore delle belle arti
      in Roma ossia Esame analitico de’ monumenti antichi, e
      moderni spettanti alla pittura, scultura e architettura
      tuttora esistenti nelle chiese, gallerie, ville ed altri
      luoghi dell’alma città di Roma; Tomo II. Della parte
      occidentale di Roma &#160;</a>
      <ul>
        <li>
          <a href="dg45040802s.html#8">Indice de' Monumenti
          Moderni esistenti in questo Secono Tomo&#160;</a>
        </li>
        <li>
          <a href="dg45040802s.html#12">Indice de' Monumenti
          Antichi, esistenti in questo secondo Tomo</a>
        </li>
        <li>
          <a href="dg45040802s.html#14">Risposta dell'autore
          diretta all'artista leggitore</a>
        </li>
        <li>
          <a href="dg45040802s.html#20">Esame analitico della
          parte occidentale di Roma</a>
        </li>
        <li>
          <a href="dg45040802s.html#306">Indice generale</a>
        </li>
        <li>
          <a href="dg45040802s.html#334">Elenco I. de'
          pittori</a>
        </li>
        <li>
          <a href="dg45040802s.html#342">Elenco II. dei
          scultori</a>
        </li>
        <li>
          <a href="dg45040802s.html#345">Elenco III. degli
          architetti</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
