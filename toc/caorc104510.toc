<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caorc104510s.html#6">Il tabernacolo della Madonna
      d’Orsanmichele lavoro insigne di Andrea Orcagna e altre
      sculture di eccellenti maestri le quali adornano la loggia e
      la chiesa predetta</a>
      <ul>
        <li>
          <a href="caorc104510s.html#8">Al molto onorevole
          Giorgio Giovanni Warren</a>
        </li>
        <li>
          <a href="caorc104510s.html#12">Dichiarazione delle
          tavole rappresentanti il tabernacolo della Madonna
          d'Orsanmichele ed altre sculture della Loggia e della
          Chiesa medesima&#160;</a>
          <ul>
            <li>
              <a href="caorc104510s.html#12">Brevi notizie
              intorno alla fabbrica d'Orsanmichele</a>
            </li>
            <li>
              <a href="caorc104510s.html#13">Tavola I. Prospetto
              del tabernacolo della Madonna</a>
            </li>
            <li>
              <a href="caorc104510s.html#14">Tavola II. Quattro
              storie della vita della Madonna</a>
              <ul>
                <li>
                  <a href="caorc104510s.html#14">I. La natività
                  di Maria</a>
                </li>
                <li>
                  <a href="caorc104510s.html#14">II. La
                  Presentazione al Tempio</a>
                </li>
                <li>
                  <a href="caorc104510s.html#15">III. Lo
                  sposalizio</a>
                </li>
                <li>
                  <a href="caorc104510s.html#15">IV.
                  L'annunciazione</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="caorc104510s.html#15">Tavola III. Quattro
              storie della vita della Madonna</a>
              <ul>
                <li>
                  <a href="caorc104510s.html#15">V. La Nascita di
                  Gesù&#160;</a>
                </li>
                <li>
                  <a href="caorc104510s.html#16">VI. La Visita
                  dei Magi&#160;</a>
                </li>
                <li>
                  <a href="caorc104510s.html#16">VII. La
                  Purificazione della Madonna</a>
                </li>
                <li>
                  <a href="caorc104510s.html#16">VIII. L'Annunzio
                  della morte</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="caorc104510s.html#17">Tavola IV. Una
              storia della vita della Madonna</a>
              <ul>
                <li>
                  <a href="caorc104510s.html#17">IX. La Sepoltura
                  e l'Assunzione</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="caorc104510s.html#18">Tavola V. I dodici
              Apostoli, e una virtù Teologale</a>
            </li>
            <li>
              <a href="caorc104510s.html#19">Tavola VI. Due Virtù
              Teologali, e otto Angeli&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#19">Tavola VII. Venti
              mezze di figure di Virtù e di Santi&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#20">Tavola VIII. San
              Matteo, Santo Stefano e San Lo (sic!). Statue nelle
              nicchie esterne</a>
            </li>
            <li>
              <a href="caorc104510s.html#21">Tavola IX. San
              Iacopo Apostolo, la Madonna e San Giovanni
              Evangelista. Statue nelle nicchie esterne</a>
            </li>
            <li>
              <a href="caorc104510s.html#22">Tavola X. San
              Giovanni Battista, San Tommaso Apostolo e San Luca
              Evangelista. Statue nelle nicchie esterne.</a>
            </li>
            <li>
              <a href="caorc104510s.html#23">XI. San Filippo
              Apostolo, i Quattro Santi e San Giorgio. Statue nelle
              nicchie esterne.</a>
            </li>
            <li>
              <a href="caorc104510s.html#25">XII. San Marco
              Evangelista, Sant'Anna e San Pietro Apostolo. Statue
              nelle nicchie esterne.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="caorc104510s.html#26">[Tavole]</a>
          <ul>
            <li>
              <a href="caorc104510s.html#26">Prospetto geometrico
              del Tabernacolo della Madonna d'Orsanmichele ▣</a>
            </li>
            <li>
              <a href="caorc104510s.html#28">Tavola II. [Quattro
              storie della vita della Madonna] ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#30">Tavola III. [Quattro
              storie della vita della Madonna] ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#32">Tavola IV. [Una
              storia della vita della Madonna] ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#34">Tavola V. [I dodici
              Apostoli, e una virtù Teologale] ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#36">Tavola VI. [Due
              Virtù Teologali, e otto Angeli] ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#38">Tavola VII. [Venti
              mezze di figure di Virtù e di Santi] ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#40">Tavola VIII. [San
              Matteo, Santo Stefano e San Lorenzo. Statue nelle
              nicchie esterne] ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#42">Tavola IX. [San
              Iacopo Apostolo, la Madonna e San Giovanni
              Evangelista. Statue nelle nicchie esterne] ▣
              &#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#44">Tavola X. [San
              Giovanni Battista, San Tommaso Apostolo e San Luca
              Evangelista. Statue nelle nicchie esterne]
              ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#46">Tavola XI. [San
              Filippo Apostolo, i Quattro Santi e San Giorgio.
              Statue nelle nicchie esterne] ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#48">Tavola XII. [San
              Marco Evangelista, Sant'Anna e San Pietro Apostolo.
              Statue nelle nicchie esterne] ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#50">Tavola XIII. Pianta
              Alzato e sviluppo Geometrico dell'Edifizio di
              Orsanmichele di Firenze ▣&#160;</a>
            </li>
            <li>
              <a href="caorc104510s.html#52">[Immagine] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="caorc104510s.html#53">Annotazioni</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
