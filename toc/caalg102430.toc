<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caalg102430s.html#6">Poesie dedicate alle glorie
      del Signor Alessandro Algardi ottimo degli scultori&#160;</a>
      <ul>
        <li>
          <a href="caalg102430s.html#8">Al molto illustre Signor
          mio osservandissimo il Signor Alessandro Algardi</a>
        </li>
        <li>
          <a href="caalg102430s.html#10">[Poesie]</a>
          <ul>
            <li>
              <a href="caalg102430s.html#10">De Alexandro
              Algardo</a>
            </li>
            <li>
              <a href="caalg102430s.html#10">Ad statuam Somni
              lapidei</a>
            </li>
            <li>
              <a href="caalg102430s.html#12">Per la statua di San
              Filippo Neri</a>
            </li>
            <li>
              <a href="caalg102430s.html#13">Per le Statue di San
              Filippo Neri e d'altri Santi</a>
            </li>
            <li>
              <a href="caalg102430s.html#14">Venere con Adone in
              braccio, à cui un Amorino asciuga i sudori del
              volto</a>
            </li>
            <li>
              <a href="caalg102430s.html#15">Per l'istessa statua
              l'Autore parla à Venere</a>
            </li>
            <li>
              <a href="caalg102430s.html#17">Prometeo nel
              tormento</a>
            </li>
            <li>
              <a href="caalg102430s.html#18">Per la statua di San
              Filippo Neri</a>
            </li>
            <li>
              <a href="caalg102430s.html#21">San Paolo col
              manigoldo in atto di ferire</a>
            </li>
            <li>
              <a href="caalg102430s.html#22">Per un Adone ferito
              in braccio à Venere</a>
            </li>
            <li>
              <a href="caalg102430s.html#23">Prometeo statua del
              medesimo</a>
            </li>
            <li>
              <a href="caalg102430s.html#25">Venere, con Adone
              ferito in grembo, à cui Amore asciuga le piaghe</a>
            </li>
            <li>
              <a href="caalg102430s.html#26">Crocifisso opera del
              medesimo</a>
            </li>
            <li>
              <a href="caalg102430s.html#27">Per il medesimo
              Crocifisso</a>
            </li>
            <li>
              <a href="caalg102430s.html#30">Venere con Adone in
              braccio, à cui Amore asciuga le piaghe</a>
            </li>
            <li>
              <a href="caalg102430s.html#34">Bacco in figura d'un
              fanciullo</a>
            </li>
            <li>
              <a href="caalg102430s.html#35">Venere con Adone in
              braccio</a>
            </li>
            <li>
              <a href="caalg102430s.html#36">Per la medesima
              statua</a>
            </li>
            <li>
              <a href="caalg102430s.html#37">Per la statua del
              Dio del sonno, fatto in pietra di paragone</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
