<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="zomar35052140as.html#6">Dicerie sacre</a>
      <ul>
        <li>
          <a href="zomar35052140as.html#8">Alla Immortalità di
          Paolo Quinto</a>
        </li>
        <li>
          <a href="zomar35052140as.html#12">Al serenissimo D.
          Carlo Emanuello Duca di Savoia</a>
        </li>
        <li>
          <a href="zomar35052140as.html#15">Del Signor Conte
          Lodovico d'Aglie&#160;</a>
        </li>
        <li>
          <a href="zomar35052140as.html#16">Del Signor Marchese
          D. Andrea di Ceva</a>
        </li>
        <li>
          <a href="zomar35052140as.html#18">Lettore</a>
        </li>
        <li>
          <a href="zomar35052140as.html#20">[La Pittura, Diceria
          I.]&#160;</a>
          <ul>
            <li>
              <a href="zomar35052140as.html#20">Parte prima</a>
            </li>
            <li>
              <a href="zomar35052140as.html#123">Parte
              seconda&#160;</a>
            </li>
            <li>
              <a href="zomar35052140as.html#168">Parte terza</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zomar35052140as.html#212">La Musica, Diceria
          Seconda, sopra le Sette Parole Dette da Christo in
          Croce</a>
          <ul>
            <li>
              <a href="zomar35052140as.html#214">Al serenissimo
              Prencipe Mauritio Cardinale di Savioa</a>
            </li>
            <li>
              <a href="zomar35052140as.html#219">Del S. Conte
              Lodovico Tesauro</a>
            </li>
            <li>
              <a href="zomar35052140as.html#220">Del Signor D.
              Lorenzo Scoto</a>
            </li>
            <li>
              <a href="zomar35052140as.html#221">Parte prima</a>
            </li>
            <li>
              <a href="zomar35052140as.html#283">Parte
              seconda</a>
            </li>
            <li>
              <a href="zomar35052140as.html#346">Parte terza</a>
            </li>
            <li>
              <a href="zomar35052140as.html#417">Parte
              quarta</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="zomar35052140as.html#500">Il Cielo, Diceria
          Terza, sopra la Religione de' Santi Mauritio, et
          Lazaro</a>
          <ul>
            <li>
              <a href="zomar35052140as.html#502">Al serenissimo
              Prencipe di Piamonte</a>
            </li>
            <li>
              <a href="zomar35052140as.html#505">Del Signor
              Conte di Rovigliasco</a>
            </li>
            <li>
              <a href="zomar35052140as.html#506">Del Signore
              Marchese Carlo Pallavicino</a>
            </li>
            <li>
              <a href="zomar35052140as.html#508">Il Cielo,
              Diceria terza</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
