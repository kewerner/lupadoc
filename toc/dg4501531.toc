<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501531s.html#6">Delle Antichità Della Città Di
      Roma</a>
      <ul>
        <li>
          <a href="dg4501531s.html#8">Iulius Papa III.</a>
        </li>
        <li>
          <a href="dg4501531s.html#12">Al Signor Messer Giacopo de
          Meleghini Michel Tramezzino</a>
        </li>
        <li>
          <a href="dg4501531s.html#14">De l'ordine che si tiene in
          questo libro dell'Antichità di Roma</a>
        </li>
        <li>
          <a href="dg4501531s.html#17">Tavola delle cose, che in
          questo libro si contengono per Capitoli</a>
        </li>
        <li>
          <a href="dg4501531s.html#30">Delle Antichità della città
          di Roma</a>
          <ul>
            <li>
              <a href="dg4501531s.html#30">Libro I.</a>
              <ul>
                <li>
                  <a href="dg4501531s.html#30">I. Che Romolo
                  edificio la città di Roma, e gli diede il
                  nome</a>
                </li>
                <li>
                  <a href="dg4501531s.html#33">II. Delle nationi,
                  che habitano in luoco, dove fu poi edificata
                  Roma</a>
                </li>
                <li>
                  <a href="dg4501531s.html#35">III. Della città di
                  Romolo, e delle sue porte</a>
                </li>
                <li>
                  <a href="dg4501531s.html#39">IIII. Delle mure, e
                  vario circuito di Roma antica</a>
                </li>
                <li>
                  <a href="dg4501531s.html#45">V. Del Pomerio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#46">VI. Delle porte
                  della città, e delle strade nel generale</a>
                </li>
                <li>
                  <a href="dg4501531s.html#49">VII. Della porta
                  del Popolo, e della strada Flaminia</a>
                </li>
                <li>
                  <a href="dg4501531s.html#51">VIII. Della porta
                  Principale, e della strada Collatina</a>
                </li>
                <li>
                  <a href="dg4501531s.html#52">IX. Della porta, e
                  strada Salaria</a>
                </li>
                <li>
                  <a href="dg4501531s.html#56">X. Della porta di
                  S. Agnesa, e de la strada Numentana&#160;</a>
                </li>
                <li>
                  <a href="dg4501531s.html#59">XI. Della porta tra
                  gli Argini, et della Querque tulana&#160;</a>
                </li>
                <li>
                  <a href="dg4501531s.html#61">XII. De la porta di
                  S. Lorenzo. e de la strada Tiburtina, Labicana, e
                  prenestina</a>
                </li>
                <li>
                  <a href="dg4501531s.html#63">XIII. Della porta
                  Nevia, e de la strada Labicana e Tiburtina</a>
                </li>
                <li>
                  <a href="dg4501531s.html#64">XIIII. De la porta
                  di San Giovanni, e de la strada Campana</a>
                </li>
                <li>
                  <a href="dg4501531s.html#65">XV. De la porta
                  Gabiusa, e de la strada Gabina&#160;</a>
                </li>
                <li>
                  <a href="dg4501531s.html#66">XVI. Della porta, e
                  strada Latina</a>
                </li>
                <li>
                  <a href="dg4501531s.html#67">XVII. De la porta
                  Capena, e de la strada Appia</a>
                </li>
                <li>
                  <a href="dg4501531s.html#72">XVIII. De la porta
                  Trigemina, e della strada Ostiense&#160;</a>
                </li>
                <li>
                  <a href="dg4501531s.html#75">XIX. De le porte di
                  Trastevere, e prima della porta di Ripa, e della
                  strada Portuense</a>
                </li>
                <li>
                  <a href="dg4501531s.html#78">XX. De la porta e
                  strada Aurelia</a>
                </li>
                <li>
                  <a href="dg4501531s.html#79">XXI. De la porta
                  Settimiana, e della strada Iulia</a>
                </li>
                <li>
                  <a href="dg4501531s.html#80">XXII. Della porta,
                  e via Trionfale</a>
                </li>
                <li>
                  <a href="dg4501531s.html#80">XXIII. De le sei
                  porte di Vaticano&#160;</a>
                </li>
                <li>
                  <a href="dg4501531s.html#82">XXIIII. De le altre
                  porta antiche di Roma</a>
                </li>
                <li>
                  <a href="dg4501531s.html#83">XXV. De le altre
                  strade che gia vi furono</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501531s.html#85">Libro II.</a>
              <ul>
                <li>
                  <a href="dg4501531s.html#85">I. De' sette Colli
                  de la città di Roma, e prima del Campidoglio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#88">II. De la Rocca del
                  Campidoglio, del Tempio di Giunone Moneta, de la
                  Casa di Manilio, e di T. Tatio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#92">III. De i clivi, ò
                  salite che diciamo, del Campidoglio, del tempio
                  di Giove Tpnante, e de &#160;la Fortuna: de la
                  Rupe Tarpeia, del tempio di Saturno, del sasso di
                  Carmenta</a>
                </li>
                <li>
                  <a href="dg4501531s.html#95">IIII. Del tempio di
                  Giove Ottimo Maßimo, del chiodo Annale: del
                  tempio del Termine, e de la Fede</a>
                </li>
                <li>
                  <a href="dg4501531s.html#99">V. Del tempio di
                  Giove Feretrio, di Giove Custode, di Veiove:
                  della Curia Calabria, della Casa di Romolo, del
                  Senatulo, de l'Asilo</a>
                </li>
                <li>
                  <a href="dg4501531s.html#102">VI. Di alcuni
                  tempij et altri luoghi, che erano nel
                  Campidoglio, et hora non si fa il luogo certo,
                  ove fussero</a>
                </li>
                <li>
                  <a href="dg4501531s.html#104">VII. Di molte
                  statue et altri ornamenti, che sono hoggi, è
                  furono già del Campidoglio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#110">VIII. Del Vico
                  Giugario, e del Vico Toscano</a>
                </li>
                <li>
                  <a href="dg4501531s.html#113">IX. Della via
                  nova, della casa di Tarquino Prisco, e del tempio
                  e boschetto di Vesta, della regia di Numa, de
                  gl'archi di Romolo, del tempio di Quirino, e del
                  Lupercale</a>
                </li>
                <li>
                  <a href="dg4501531s.html#120">X. Del Foro
                  Romano, del Carcere, di Marforio, del secretario
                  del popolo Romano, del tempio di Concordia</a>
                </li>
                <li>
                  <a href="dg4501531s.html#128">XI. De l'arco di
                  Settimio, del Milario aureo, del tempio da
                  Saturno, e de l'Erario</a>
                </li>
                <li>
                  <a href="dg4501531s.html#133">XII. Del tempio di
                  Giove Statore, della casa di Tarquino Superbo, e
                  de' Rostri</a>
                </li>
                <li>
                  <a href="dg4501531s.html#136">XIII. Del cavallo
                  di Diomitiano, del Lago Curtio, della Cloaca
                  Maßima</a>
                </li>
                <li>
                  <a href="dg4501531s.html#139">XIIII. De la
                  Basilica di Paolo Emilio, del tempio di Iulio
                  Cesare, di Castore, e Polluce, e di Augusto, e
                  del ponte di Caligula</a>
                </li>
                <li>
                  <a href="dg4501531s.html#142">XV. Del tempio di
                  Faustina, dell'arco Afbiano, del tribunale di
                  Libone, del tempio di Iano, de i Dolioli, e delle
                  altre cose, che erano nel Foro Romano</a>
                </li>
                <li>
                  <a href="dg4501531s.html#146">XVI. De i luoghi
                  del Comitio, e prima del tempio di Romolo, della
                  Basilica Portia, della casa di Menio, e della
                  Curia Hostilia</a>
                </li>
                <li>
                  <a href="dg4501531s.html#149">XVII. Della casa
                  di C. Cesare, del portico di Livia, del tempio
                  della pace, e della via sacra</a>
                </li>
                <li>
                  <a href="dg4501531s.html#152">XVIII. Del Fico
                  Ruminale, del luogo proprio del Comitio, del
                  Gregostasi, del tempio di Concordia, del Senato,
                  della casa di Faustolo, di Catilina, e di Scauro,
                  e del tempio di Vulcano</a>
                </li>
                <li>
                  <a href="dg4501531s.html#157">XIX. De l'Arco di
                  Tito Vespasiano</a>
                </li>
                <li>
                  <a href="dg4501531s.html#159">XXI. Del colle
                  Palatino, del Palazzo, che vi era, della casa di
                  Romolo, di M. Tullio, e di Flacco&#160;</a>
                </li>
                <li>
                  <a href="dg4501531s.html#162">XXI. [sic!] Della
                  somma Velia, della casa di Valerio Publicola, del
                  tempio di Vittoria, de li dei Penati, de l'Orco,
                  della casa di Tullo Hostilio, del luogo del
                  Palladio, del tempio di Cibele</a>
                </li>
                <li>
                  <a href="dg4501531s.html#165">XXII. Della casa
                  di Augusto, del tempio di Apolline, delle
                  librarie antiche, del tempio della Fede, de i
                  Bagni Palatini, del tempio di Vittoria, e di
                  molti altri luoghi, che furono sul Palatino</a>
                </li>
                <li>
                  <a href="dg4501531s.html#169">XXIII. Del Foro di
                  Cesare, di Augusto, e di Nerva, con ciò che vi
                  era</a>
                </li>
                <li>
                  <a href="dg4501531s.html#173">XXIIII. Del Foro
                  di Traiano con ciò che vi era, del sepolcro di
                  Publicio, e della casa de Corvini</a>
                </li>
                <li>
                  <a href="dg4501531s.html#177">XXV. De busti
                  Gallici, del Vico scelerato, del Tigillo sororio,
                  del tempio di Tellure, e della casa di M.
                  Antonio</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501531s.html#180">Libro III.</a>
              <ul>
                <li>
                  <a href="dg4501531s.html#180">I. Del colle
                  Aventino, del Clivo che v'era, del tempio di
                  Giunone Regina, delle scale Gemonie, del tempio
                  di Diana, della buona Dea, di Hercole,
                  dell'Armilustro, de le Terme di Decio, di
                  Traiano, e di Vario, e del fonte, di Fauno, e di
                  Pico</a>
                </li>
                <li>
                  <a href="dg4501531s.html#186">II. Delle Therme
                  di Antonio Caracalla, e delle altre cose che
                  erano nell'Aventino, e dell'Acquedotto del
                  l'acqua Appia</a>
                </li>
                <li>
                  <a href="dg4501531s.html#188">III. Del campo,
                  dove è Testaccio, dietro l'Aventino, con ciò che
                  gia vi fu</a>
                </li>
                <li>
                  <a href="dg4501531s.html#191">IIII. Del tempio
                  di Murcia, de gli archi di Oratio, de la Saline,
                  del tempio di Vesta, de la Fortuna virile, e del
                  Foro Piscario</a>
                </li>
                <li>
                  <a href="dg4501531s.html#196">V. Del Teatro di
                  Marcello, del tempio della Pietà, del Carcere
                  della Plebe, della Curia, e Portico di
                  Ottavia</a>
                </li>
                <li>
                  <a href="dg4501531s.html#199">VI. Del Foro
                  Olitorio, del tempio di Iano, del Sacrario di
                  Numa, del tempio di Matuta, di Carmenta, della
                  Speranza, della colonna Lattaria, de l'Argileto,
                  de l'Esquimelio, dell'Asilo</a>
                </li>
                <li>
                  <a href="dg4501531s.html#202">VII. Del Velabro,
                  della Basilica Sempronia, del Foro Boario,
                  dell'arco di Settimio, del tempio di Iano
                  Quadrifronte, del tempio di Hercole Vincitore, e
                  dell'Ara Maßima</a>
                </li>
                <li>
                  <a href="dg4501531s.html#207">VIII. Del tempio
                  della Fortuna prospera e della Madre Matuta, de
                  gli Archi di Stertino, e del tempio della
                  Pudicitia Patritia</a>
                </li>
                <li>
                  <a href="dg4501531s.html#209">IX. Del Circo
                  Maßimo, e del tempio di Conso, e di Nettuno, che
                  vi erano&#160;</a>
                </li>
                <li>
                  <a href="dg4501531s.html#213">X. Di molti
                  tempij, che erano nel Circo Maßimo, ò appreßo, e
                  dell'obelisco rotto, che vi è</a>
                </li>
                <li>
                  <a href="dg4501531s.html#215">XI. Del
                  Settizonio, dell'Arco di Costantino, e della Meta
                  sudante</a>
                </li>
                <li>
                  <a href="dg4501531s.html#217">XII.
                  Dell'Anfiteatro di Tito, della casa aurea di
                  Nerone, e del tempio della Fortuna Seia</a>
                </li>
                <li>
                  <a href="dg4501531s.html#222">XIII. Del tempio
                  d'Iside, di Quirino, dell'Honore, della Virtù,
                  del Celiolo, del tempio di Diana, e del Rio
                  d'Appio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#225">XIIII. Fauno
                  alloggiamenti pellegrini, la scala de Laterani,
                  e'l Palagio di Costantino</a>
                </li>
                <li>
                  <a href="dg4501531s.html#228">XV. Del tempio di
                  Venere, di Cupidine, dell'Anfiteatro di Statilio
                  Tauro, del palagio Sesoriano, e de gli Acquedotti
                  dell'acqua Claudia e de l'Aniene nuovo</a>
                </li>
                <li>
                  <a href="dg4501531s.html#235">XVI. Di molti
                  luoghi del monte Celio, de' quali non si sa hoggi
                  il luogo, ove fussero</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501531s.html#237">Libro IIII.</a>
              <ul>
                <li>
                  <a href="dg4501531s.html#237">I. Dell Esquilie,
                  et in particolare delle Carine, e de' luoghi
                  suoi, come furonole Terme, e'l palagio di Tito,
                  la Curia vecchia, et alcuni altri antichi luoghi,
                  che vi furono co' moderni che vi sono</a>
                </li>
                <li>
                  <a href="dg4501531s.html#245">II. Del Clivo
                  Suburrano, del Clivio Urbico, della casa di Ser
                  Tullo, della Basilica di Sesimino, de l'arco di
                  Galieno, del Macello Liviano</a>
                </li>
                <li>
                  <a href="dg4501531s.html#248">III. De li trofei
                  di Mario, della casa de gli Elij, della
                  Tabernola, del palagio e Terme di Gordiani, della
                  Basilica di Gaio e Lucio, e del palagio
                  Liciano</a>
                </li>
                <li>
                  <a href="dg4501531s.html#251">IIII. De gli
                  Argini di Tarquino, della Torre, et horti di
                  Mecenate, del campo Esquilino, delle Puticole, e
                  di altri luoghi di questo colle</a>
                </li>
                <li>
                  <a href="dg4501531s.html#253">V. De gli
                  Acquedotti dell'Acqua martia, della Tepula, della
                  Iulia, e dell'Aniene Vecchio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#257">VI. Del Colle
                  Viminale, del bagno di Agrippina, delle Terme di
                  Novatio, di Olimpiade, e di Dioclitiano, della
                  casa di C. Aquilio, e del campo Viminale</a>
                </li>
                <li>
                  <a href="dg4501531s.html#262">VII. Di Suburra,
                  del Vico Patritio, della valle Quirinale con
                  luoghi, che quivi erano&#160;</a>
                </li>
                <li>
                  <a href="dg4501531s.html#264">VIII. Del
                  Quirinale, delle cose che vi erano, come furono i
                  bagni di Paolo Emilio, le Terme di Costantino, il
                  tempio del Sole, la casa, e'l Vico de' Cornelij,
                  e de' tempij di Saturno</a>
                </li>
                <li>
                  <a href="dg4501531s.html#268">IX. De l'alta
                  Semita, della casa di Attico, del tempio di
                  Quirino, della casa de Flavij, del monte di
                  Apolline, e di Clatra, del Campidoglio vecchio,
                  del Circo e tempio di Flora, del Clivio Publicio,
                  di un'altro tempio di Quirino, del Vico di
                  Mamurra</a>
                </li>
                <li>
                  <a href="dg4501531s.html#271">X. Del Foro, casa,
                  et horti di Salustio, del Campo scelerato, della
                  pila Tibrutina, della casa di Martiale, e
                  d'alcuni altri luoghi del Quirinale</a>
                </li>
                <li>
                  <a href="dg4501531s.html#275">XI. Del colle de
                  gli Horti, con quello, che gia vi fu sopra, ò che
                  hora vi è</a>
                </li>
                <li>
                  <a href="dg4501531s.html#277">XII. Del Campo
                  Martio, della Valle Martia, della Naumachia di
                  Domitiano, e del Mausoleo di Augusto, con lu due
                  Obelisci del Campo Martio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#281">XIII. De l'arco di
                  Domitiano, o di Claudio, del tempio di Giunone
                  Lucina, dell'Obelisco, e dello Horologio del
                  campo Martio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#283">XIIII. Della
                  colonna à chiocciole di Antonio Pio, e del suo
                  portico, e del monte Acitorio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#285">XV. Della villa
                  publica, de i Septi, del tempio di Nettuno, e
                  dell'Anfiteatro di Claudio Imperatore</a>
                </li>
                <li>
                  <a href="dg4501531s.html#287">XVI. Dell'acqua
                  vergine, del tempio, e del lago di Iuturna, e del
                  tempio della Pietà&#160;</a>
                </li>
                <li>
                  <a href="dg4501531s.html#291">XVII. Della via
                  Lata, del tempio d'Iside, del Foro Suario, del
                  l'arco di Camillo, e del tempio di Minerva</a>
                </li>
                <li>
                  <a href="dg4501531s.html#294">XVIII. Del
                  Panteone, chiamato hoggi S. Maria Rotonda</a>
                </li>
                <li>
                  <a href="dg4501531s.html#298">XIX. Delle Terme
                  di Agrippa, del tempio del buono Evento, delle
                  Terme di Nerone, di Alessandro, e di Adriano</a>
                </li>
                <li>
                  <a href="dg4501531s.html#300">XX. Del Circo, che
                  chiamano Agone, e del tempio di Netunno</a>
                </li>
                <li>
                  <a href="dg4501531s.html#301">XXI. De l'altare
                  di Plutone, di Terento, della palude Caprea, e di
                  altri luoghi del Campo Martio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#304">XXII. Del tempio
                  di Bellona, della Colonna bellica, del tempio
                  d'Apolline, e di Giunone</a>
                </li>
                <li>
                  <a href="dg4501531s.html#307">XXIII. Del Circo
                  Flaminio, del tempio di Vulcano, di Netunno, di
                  Hercole Custode, e di Hercole, e delle Muse, con
                  altre cose, che in questo Circo erano</a>
                </li>
                <li>
                  <a href="dg4501531s.html#309">XXIIII. Del Teatro
                  di M. Scauro, di Curione, di Gn. Pompeio, e di
                  Balbo</a>
                </li>
                <li>
                  <a href="dg4501531s.html#313">XXV. Della Curia e
                  portico di Pompeio, del portico di Ottavio, e del
                  campo di Fiora</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501531s.html#315">Libro V.</a>
              <ul>
                <li>
                  <a href="dg4501531s.html#315">I. Del Tevere</a>
                </li>
                <li>
                  <a href="dg4501531s.html#318">II. De i ponti,
                  che sono sopra al Tevere, e prima del
                  Sublicio</a>
                </li>
                <li>
                  <a href="dg4501531s.html#320">III. Del ponte di
                  S. Maria</a>
                </li>
                <li>
                  <a href="dg4501531s.html#321">IIII. De l'Isola
                  dal Tevere con ciò che gia vi fu, e de li due
                  ponti, che la congiungono con la città, e con
                  Trastevere</a>
                </li>
                <li>
                  <a href="dg4501531s.html#324">V. Di ponte Sisto,
                  del Trionfale, del ponte di Castello, e di ponte
                  Molle</a>
                </li>
                <li>
                  <a href="dg4501531s.html#327">VI. Di Trastevere,
                  del tempio de Ravennati, e del sepolcro di
                  Numa</a>
                </li>
                <li>
                  <a href="dg4501531s.html#329">VII. De gli Horti,
                  e Naumachia di C. Cesare, e dell'acqua
                  Alfietina</a>
                </li>
                <li>
                  <a href="dg4501531s.html#330">VIII. Delle Terme
                  di Severo, e di Aureliano, de' prati Mutij, e di
                  altri luoghi antichi, ò moderni, che gia furono,
                  ò sono hoggi in Trastevere</a>
                </li>
                <li>
                  <a href="dg4501531s.html#332">IX. Del colle
                  Vaticano, del Circo, e Naumachia di Nerone, e de
                  l'Obelisco di Vaticano</a>
                </li>
                <li>
                  <a href="dg4501531s.html#334">X. Del Tempio
                  d'Apolline, e di Marte, e della chiesa di S.
                  Pietro, e delle opere antiche, che si veggono in
                  Belvedere</a>
                </li>
                <li>
                  <a href="dg4501531s.html#340">XI. Del Sepolcro
                  di Adriano, e dello Spedale di S. Spirito in
                  Saßia&#160;</a>
                </li>
                <li>
                  <a href="dg4501531s.html#342">XII. De l'acqua
                  Sabatina, del sepolcro di Scipione, e de' Prati
                  Quntij</a>
                </li>
                <li>
                  <a href="dg4501531s.html#343">XIII. Delle XIIII.
                  regioni della città, et à quali di loro le IX.
                  acque, che venivano in Roma, servißero&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501531s.html#348">Sommaria dechiaratione
              de li Titoli, et Epistasi antichi, che in questa
              opera sono</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501531s.html#362">Tavola de luoghi, che in
          questo libro si descrivono, per alfabeto</a>
        </li>
        <li>
          <a href="dg4501531s.html#383">Alli lettori Lucio
          Fauno</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
