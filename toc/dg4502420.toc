<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502420s.html#8">Le cose meravigliose di Roma</a>
      <ul>
        <li>
          <a href="dg4502420s.html#9">Le sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg4502420s.html#9">La prima Chiesa è ›San
              Giovanni in Laterano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#14">La seconda Chiesa é
              ›San Pietro in Vaticano‹</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#15">[›San Pietro in
                  Vaticano‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#19">[›San Pietro in
                  Vaticano: Baldacchino di San Pietro‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#20">[›San Pietro in
                  Vaticano: Mosaico della Navicella‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#22">La terza Chiesa è ›San
              Paolo fuori le Mura‹ ▣</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#24">[›Piramide di Caio
                  Cestio‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#25">La quarta Chiesa é
              ›Santa Maria Maggiore‹ ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#28">La quinta Chiesa è ›San
              Lorenzo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#29">La sesta Chiesa é ›San
              Sebastiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#31">La settima Chiesa è
              ›Santa Croce in Gerusalemme‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502420s.html#32">Delle altre Chiese di Roma,
          e fuora</a>
          <ul>
            <li>
              <a href="dg4502420s.html#32">Nell'Isola di San
              Bartolomeo ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#32">In ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#36">In ›Borgo‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#38">Dalla Porta Flaminia,
              ovvero del Popolo ›Porta del Popolo‹, fino alle
              radici del Campidoglio‹</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#42">[›Colonna Traiana‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#45">[›Colonna di
                  Antonino Pio‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#50">›San Carlo ai
                  Catinari‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#53">Da ›Campidoglio‹, à man
              sinistra verso i Monti</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#54">[›Foro Romano‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#57">[›Trofei di Mario‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#61">[›Palatino‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502420s.html#65">Le stationi delle Chiese di
          Roma</a>
          <ul>
            <li>
              <a href="dg4502420s.html#71">Le stationi
              dell'Avvento</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502420s.html#72">Modo di visitare li sette
          altari di ›San Pietro in Vaticano‹ e loro indulgenze</a>
        </li>
        <li>
          <a href="dg4502420s.html#74">Modo di visitare la ›Scala
          Santa‹</a>
        </li>
        <li>
          <a href="dg4502420s.html#76">Guida romana per li
          forestieri</a>
          <ul>
            <li>
              <a href="dg4502420s.html#76">Del ›Borgo‹ prima
              giornata</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#76">›Castel
                  Sant'Angelo‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#77">Del
                  ›Trastevere‹</a>
                </li>
                <li>
                  <a href="dg4502420s.html#78">Dell'›Isola
                  Tiberina‹, e Licaonia ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#78">Del Ponte di Santa
                  Maria ›Ponte Rotto‹, del Palazzo di Pilato ›Casa
                  dei Crescenzi‹, et altre cose</a>
                  <ul>
                    <li>
                      <a href="dg4502420s.html#79">[›Tempio di
                      Portunus‹] ▣</a>
                    </li>
                    <li>
                      <a href="dg4502420s.html#79">[›Tempio di
                      Hercules Olivarius‹] ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502420s.html#80">Del Monte Testaccio
                  ›Testaccio (Monte)‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4502420s.html#81">Arcus Ianu ›Arco di
                  Giano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#81">Delle Terme
                  Antoniane, et altre cose ›Terme di Caracalla‹</a>
                </li>
                <li>
                  <a href="dg4502420s.html#81">Di ›San Giovanni in
                  Laterano‹, ›Santa Croce in Gerusalemme‹, et altri
                  luoghi</a>
                </li>
                <li>
                  <a href="dg4502420s.html#82">[›Anfiteatro
                  Castrense‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#83">Giornata seconda</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#83">[›Porta Maggiore‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#83">Della ›Porta del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4502420s.html#84">[›Piazza del
                  Popolo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#84">Di Monte Cavallo,
                  già detto ›Quirinale‹, e de' Cavalli di Marmo
                  ›Fontana di Monte Cavallo‹</a>
                </li>
                <li>
                  <a href="dg4502420s.html#85">[›Quirinale‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#86">Della Strada Pia, e
                  della vigna che era già del Cardinal di
                  Ferrara</a>
                </li>
                <li>
                  <a href="dg4502420s.html#86">[›Torre Mesa‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#86">Della ›Porta Pia‹,
                  di ›Sant'Agnese‹, et altre anticaglie</a>
                </li>
                <li>
                  <a href="dg4502420s.html#87">[Foro di Nerva‹]
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#87">Delle ›Terme di
                  Diocleziano‹</a>
                  <ul>
                    <li>
                      <a href="dg4502420s.html#88">[›Terme di
                      Diocleziano‹] ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502420s.html#89">[›Colosseo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#90">[›Arco di
                  Costantino‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#92">Del Monte
                  ›Palatino‹, hoggi detto Palazzo Maggiore, et
                  altre cose</a>
                  <ul>
                    <li>
                      <a href="dg4502420s.html#91">[›Palatino‹]
                      ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502420s.html#93">Del ›Foro di
                  Nerva‹</a>
                  <ul>
                    <li>
                      <a href="dg4502420s.html#92">[›Foro di
                      Nerva‹] ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502420s.html#93">Dell'Arco trionfale
                  di Settimio Severo ›Arco di Settimio Severo‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#94">Del ›Campidoglio‹,
                  et altre cose ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#95">De i Portici
                  d'Ottavio, di Settimio, e Teatro di Pompeo
                  ›Portico di Ottavio‹ ›Porticus Severi‹ ›Teatro di
                  Pompeo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#96">Giornata terza</a>
            </li>
            <li>
              <a href="dg4502420s.html#96">Delle due Colonne, una
              d'Antonino Pio, e l'altra di Traiano ›Colonna di
              Antonino Pio‹ › Colonna Traiana‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#96">Della Rotonda, ovvero
              ›Pantheon‹ ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#97">Dei Bagni d'Agrippa
              ›Terme di Agrippa‹, e di Nerone ›Terme
              Neroniano-Alessandrine‹ ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#98">Della ›Piazza Navona‹
              ▣, e di Pasquino ›Statua di Pasquino‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#99">Indice de' sommi
              Pontefici Romani</a>
            </li>
            <li>
              <a href="dg4502420s.html#111">Reges, et Imperatores
              Romani</a>
            </li>
            <li>
              <a href="dg4502420s.html#114">Li Re di Francia</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502420s.html#116">Le sette maraviglie del
      mondo</a>
      <ul>
        <li>
          <a href="dg4502420s.html#116">I. Delle Mura di
          Babilionia ▣</a>
        </li>
        <li>
          <a href="dg4502420s.html#117">II. Della Torre di Faros
          ▣</a>
        </li>
        <li>
          <a href="dg4502420s.html#118">III. Della Statua di Giove
          ▣</a>
        </li>
        <li>
          <a href="dg4502420s.html#119">IV. Del Colosso di Rodi
          ▣</a>
        </li>
        <li>
          <a href="dg4502420s.html#120">V. Del Tempio di Diana
          ▣</a>
        </li>
        <li>
          <a href="dg4502420s.html#121">VI. Del Mausoleo di
          Artemisia ▣</a>
        </li>
        <li>
          <a href="dg4502420s.html#122">VII. Delle Piramidi
          d'Egitto ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502420s.html#123">[Tavole]</a>
      <ul>
        <li>
          <a href="dg4502420s.html#123">Le Principali poste
          d'Italia</a>
        </li>
        <li>
          <a href="dg4502420s.html#126">Indice delle Chiese, et de
          lochi principali di Roma, che sono nel presente libro</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502420s.html#128">Le antichità dell'alma città
      di Roma</a>
      <ul>
        <li>
          <a href="dg4502420s.html#129">Dell'Edificatione di
          Roma</a>
        </li>
        <li>
          <a href="dg4502420s.html#130">Del circuito di Roma</a>
        </li>
        <li>
          <a href="dg4502420s.html#131">Delle Porte</a>
          <ul>
            <li>
              <a href="dg4502420s.html#132">Porta Maggiore ›Arco
              di Costantino‹ ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#133">Delle Vie</a>
            </li>
            <li>
              <a href="dg4502420s.html#133">Delli Ponti, che sono
              sopra il Tevere, e suoi edificatori</a>
            </li>
            <li>
              <a href="dg4502420s.html#134">Dell'Isola del
              Tevere</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#135">[›Isola Tiberina‹]
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#135">Delli Monti</a>
            </li>
            <li>
              <a href="dg4502420s.html#135">Palazzo Maggiore
              ›Palatino‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#136">Monte ›Quirinale‹,
              hoggi detto Monte Cavallo</a>
            </li>
            <li>
              <a href="dg4502420s.html#136">Del Monte Testaccio
              ›Testaccio (monte)‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#136">Delle Acque, e chi le
              condusse a Roma</a>
            </li>
            <li>
              <a href="dg4502420s.html#137">Della ›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#137">Delli Acquedotti</a>
            </li>
            <li>
              <a href="dg4502420s.html#138">[›Palatino‹] ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#138">Delle ›Sette Sale‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#139">Delle Terme cioè
              bagni, e suoi edificatori</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#139">Termae Alessandri
                  Severi ›Terme Neroniano-Alessandrine‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#140">[›Terme
                  Diocleziane‹]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#141">Delle Naumachie, dove
              si facevano le battaglie Navali, e che cosa erano</a>
            </li>
            <li>
              <a href="dg4502420s.html#141">De' Cerchi, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4502420s.html#142">De Theatri, e che cosa
              erano, e suoi edificatori</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#142">Del Theatro di
                  Marcello ›Anfiteatro Castrense‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#143">Delli Anfiteatri, e
              suoi edificatori, e che cosa erano</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#143">[›Colosseo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502420s.html#144">Enfiteatro (sic!)
                  di Statilio Tauro ›Teatro di Marcello‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#144">De' Fori, cioè
              Piazze</a>
            </li>
            <li>
              <a href="dg4502420s.html#145">›Foro Boario‹, hoggi
              detto Campo Vaccino ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#146">De gli Archi
              trionfali, et a chi si davano</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#146">[›Arco di
                  Costantino‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#147">De' Portichi</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#147">[›Pantheon‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#148">De' Trofei, et Colonne
              memorande</a>
            </li>
            <li>
              <a href="dg4502420s.html#148">De' Colossi</a>
            </li>
            <li>
              <a href="dg4502420s.html#149">Delle Piramidi</a>
            </li>
            <li>
              <a href="dg4502420s.html#149">Delle Mete</a>
            </li>
            <li>
              <a href="dg4502420s.html#149">Delli Obedischi
              (sic!)</a>
            </li>
            <li>
              <a href="dg4502420s.html#149">Delle Statue</a>
            </li>
            <li>
              <a href="dg4502420s.html#149">Di Marforio ›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#150">De' Cavalli ›Fontana
              di Monte Cavallo‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#150">De' palazzi</a>
            </li>
            <li>
              <a href="dg4502420s.html#150">Della casa Aurea di
              Nerone, e di Antonino ›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#151">Delle altre case de'
              Cittadini</a>
            </li>
            <li>
              <a href="dg4502420s.html#151">Delle Librarie</a>
            </li>
            <li>
              <a href="dg4502420s.html#151">Delli Horioli</a>
            </li>
            <li>
              <a href="dg4502420s.html#152">Delle Curie, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4502420s.html#152">De' Senatuli, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4502420s.html#152">De' Magistrati</a>
            </li>
            <li>
              <a href="dg4502420s.html#153">De' Comitij, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4502420s.html#153">Delle Tribù</a>
            </li>
            <li>
              <a href="dg4502420s.html#153">Delle Regioni, o
              Rioni, sue insegne precedenti</a>
            </li>
            <li>
              <a href="dg4502420s.html#153">Delle Basiliche, et
              che cosa erano</a>
            </li>
            <li>
              <a href="dg4502420s.html#153">Della Segretaria del
              Popolo Romano</a>
            </li>
            <li>
              <a href="dg4502420s.html#154">Dell'›Asilo di
              Romolo‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#154">Del ›Campidoglio‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#155">Dell'Erario
              ›Aerarium‹, cioè Camera del commune; et che moneta si
              spendeva in Roma in quei tempi</a>
            </li>
            <li>
              <a href="dg4502420s.html#156">Delle ›Grecostasi‹, et
              che cosa era</a>
            </li>
            <li>
              <a href="dg4502420s.html#156">Delli ›Rostri‹, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4502420s.html#157">Della Colonna detta
              Miliario ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#157">Della Colonna Bellica
              ›Columna Bellica‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#157">Della Colonna Lattaria
              ›Columna Lactaria‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#157">Del Tempio di Carmenta
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#157">Dell'Equimelio
              ›Aequimaelium‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#157">Del ›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#157">Del Sigillo Sororio
              ›Tigillum Sororium‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#158">De' campi forastieri
              ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#158">Del Vivario
              ›Vivarium‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#158">Della ›Villa Pubblica‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#159">Della ›Taberna
              Meritoria‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#159">De gli Horti</a>
            </li>
            <li>
              <a href="dg4502420s.html#159">Delle ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#160">Del ›Velabro‹</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#160">[›Teatro di
                  Marcello‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#160">Delli Clivi</a>
            </li>
            <li>
              <a href="dg4502420s.html#161">De' Prati</a>
            </li>
            <li>
              <a href="dg4502420s.html#161">De' Granari
              pubblichi</a>
            </li>
            <li>
              <a href="dg4502420s.html#161">Delle Carcere
              publiche</a>
            </li>
            <li>
              <a href="dg4502420s.html#161">Di alcune feste, e
              giuochi, che si solevano celebrare in Roma</a>
            </li>
            <li>
              <a href="dg4502420s.html#162">Del Sepolcro
              d'Adriano, d'Augusto, e di Settimio ›Castel
              Sant'Angelo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502420s.html#163">De' Tempij</a>
            </li>
            <li>
              <a href="dg4502420s.html#164">De' Sacerdoti, delle
              Vergini, vestimenti, vasi, et altri instrumenti fatti
              per uso delli Sacrificij, e suoi institutori</a>
            </li>
            <li>
              <a href="dg4502420s.html#165">Dell'Armentario, e che
              cosa era ›Armamentaria‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#165">Dell'Essercito di
              Terra, e di Mare, e loro insegne</a>
            </li>
            <li>
              <a href="dg4502420s.html#166">De' Trionfi, et a chi
              si concedevano, chi fu il primo trionfatore, e di
              quante maniere erano</a>
            </li>
            <li>
              <a href="dg4502420s.html#166">Delle Corone, et a chi
              si davano</a>
            </li>
            <li>
              <a href="dg4502420s.html#166">Del numero del Popolo
              Romano</a>
            </li>
            <li>
              <a href="dg4502420s.html#167">Delle ricchezze del
              Popolo Romano</a>
            </li>
            <li>
              <a href="dg4502420s.html#167">Della liberalità degli
              antichi Romani</a>
            </li>
            <li>
              <a href="dg4502420s.html#167">Delli Matrimonij
              antichi, e loro usanza</a>
            </li>
            <li>
              <a href="dg4502420s.html#168">Della buona creanza,
              che davano a' figliuoli</a>
            </li>
            <li>
              <a href="dg4502420s.html#168">Della sepratione de'
              Matrimonij</a>
            </li>
            <li>
              <a href="dg4502420s.html#168">Dell'Essequie antiche,
              e sue cerimonie</a>
            </li>
            <li>
              <a href="dg4502420s.html#169">Delle Torri</a>
            </li>
            <li>
              <a href="dg4502420s.html#169">Del ›Tevere‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#170">Del Palazzo Papale
              ›Palazzo Apostolico Vaticano‹. e Belvedere ›Cortile
              del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#170">Del ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502420s.html#171">Epilogo
              dell'Antichità</a>
            </li>
            <li>
              <a href="dg4502420s.html#171">De' Templij de gli
              antichi fuori di Roma</a>
              <ul>
                <li>
                  <a href="dg4502420s.html#172">[›Arco di Settimio
                  Severo‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502420s.html#173">Quante volte Roma è
              stata presa</a>
            </li>
            <li>
              <a href="dg4502420s.html#173">De fuochi de gli
              antichi, scritti da pochi autori, cavati d'alcuni
              fragmenti, et historie</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
