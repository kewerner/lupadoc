<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlec552129903s.html#10">Cabinet Des Singularitez
      D’Architecture, Peinture, Sculpture, Et Graveure Ou
      Introduction A La Connoissance des plus beaux Arts, figurés
      sous les Tableaux les Statuës, &amp; les Estampes; Tome
      III.</a>
      <ul>
        <li>
          <a href="ghlec552129903s.html#12">A Monseigneur Jules
          Hardouin Mansart</a>
        </li>
        <li>
          <a href="ghlec552129903s.html#14">Preface</a>
        </li>
        <li>
          <a href="ghlec552129903s.html#18">Avertissement</a>
        </li>
        <li>
          <a href="ghlec552129903s.html#22">Table des
          principeaux sujets et des noms des Peintres, Sculpteurs,
          et Graveurs</a>
        </li>
        <li>
          <a href="ghlec552129903s.html#48">Cabinet des
          singularitez d'architecture, peintre, sculpture, et
          graveure</a>
          <ul>
            <li>
              <a href="ghlec552129903s.html#198">Parlons
              maintenant de Monsieur Le Brun et de Monsieur
              Mignard</a>
            </li>
            <li>
              <a href="ghlec552129903s.html#226">Venons
              maintenant au denombrement de quelches Peintres
              étrangers qui ont eu beaucoup de succez dans leurs
              ouvrages</a>
            </li>
            <li>
              <a href="ghlec552129903s.html#288">Description des
              peintures, sculptures, et estames exposez dans la
              grande Gallerie du Louvre dans le mois de Septembre
              1699&#160;</a>
            </li>
            <li>
              <a href="ghlec552129903s.html#321">Portraits des
              Sadelers</a>
            </li>
            <li>
              <a href="ghlec552129903s.html#430">Entrons
              presentement dans le détail des Gravieurs, et
              parcourons leurs Ouvrages avec la même application
              que ceux des autres</a>
            </li>
            <li>
              <a href="ghlec552129903s.html#544">Recapitulation
              sur cet ouvrage</a>
            </li>
            <li>
              <a href="ghlec552129903s.html#548">Catalogue de
              l'oeuvre de Monsieur Le Brun&#160;</a>
            </li>
            <li>
              <a href="ghlec552129903s.html#580">Oeuvre de
              Raphael&#160;</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
