<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghtem3313781s.html#8">Vite dei più celebri
      architetti, e scultori veneziani che fiorirono nel secolo
      decimosesto</a>
      <ul>
        <li>
          <a href="ghtem3313781s.html#8">Libro primo</a>
          <ul>
            <li>
              <a href="ghtem3313781s.html#10">A sua eccelenza il
              Signor Cavaliere Girolamo Ascano Giustiniani</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#14">Prefazione</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#18">Ai professori delle
              tre nobilissime arti del disegno</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#21">Catalogo de'
              professori</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#22">Vita di Fra
              Francesco Colonna. sopranominato Polifilo</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#75">Vita di Fra
              Giovanni Giocondo</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#100">Vita di Pietro
              Lombardo</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#114">Vita di Martino
              Lombardo</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#119">Vita di Mastro
              Bartolommeo Buono</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#127">Vita di Antonio
              Scarpagnino</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#131">Vita di Alessandro
              Leopardo</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#138">Vita di Tullio, e
              di Antonio fratteli Lombardo, e di Sante Lombardo,
              loro nipote</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#147">Vita di Guglielmo
              Bergamasco</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#152">Vita di Giovanni
              Maria Falconetto</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghtem3313781s.html#168">Libro secondo</a>
          <ul>
            <li>
              <a href="ghtem3313781s.html#171">Catalogo de'
              professori</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#172">Vita di Michele
              Sanmicheli veronese, e di Giovanni Girolamo suo
              nipote</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#219">Vita di Jacopo
              Sansovino</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#290">Vita di Danese
              Cataneo</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#305">Vita di Andrea
              Palladio</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#430">Vita di Vincenzio
              Scamozzi</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#496">Vita di Alessandro
              Vittoria</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#520">Vita di Antonio da
              Ponte</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#540">Vita di Girolamo
              Campagna</a>
            </li>
            <li>
              <a href="ghtem3313781s.html#550">Indice</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
