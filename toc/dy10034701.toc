<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy10034701s.html#8">Collectionis bullarum
      Sacrosanctae Basilicae Vaticanae tomus primus a Sancto Leone
      Magno ad Innocentium VI. productus notis auctus &amp;
      illustratus</a>
      <ul>
        <li>
          <a href="dy10034701s.html#10">[Dedicatio]</a>
        </li>
        <li>
          <a href="dy10034701s.html#14">Operis Ratio</a>
        </li>
        <li>
          <a href="dy10034701s.html#19">Approbationes</a>
        </li>
        <li>
          <a href="dy10034701s.html#22">Index chronologicus
          Diplomatum primi Voluminis</a>
        </li>
        <li>
          <a href="dy10034701s.html#45">Statua Sanctus Petrus
          ›San Pietro in Vaticano‹ ▣</a>
        </li>
        <li>
          <a href="dy10034701s.html#46">[Papae]</a>
          <ul>
            <li>
              <a href="dy10034701s.html#46">Sanctus Leo
              Primus</a>
            </li>
            <li>
              <a href="dy10034701s.html#48">Sanctus Gregorius
              I.</a>
            </li>
            <li>
              <a href="dy10034701s.html#52">Honorius I.</a>
            </li>
            <li>
              <a href="dy10034701s.html#52">Sanctus Gregorius
              II.</a>
            </li>
            <li>
              <a href="dy10034701s.html#55">Sanctus Gregorius
              III.</a>
            </li>
            <li>
              <a href="dy10034701s.html#56">Sanctus Zacharias</a>
            </li>
            <li>
              <a href="dy10034701s.html#57">Stephanus II.</a>
            </li>
            <li>
              <a href="dy10034701s.html#57">Sanctus Paulus I.</a>
            </li>
            <li>
              <a href="dy10034701s.html#58">Hadrianus I.</a>
            </li>
            <li>
              <a href="dy10034701s.html#60">Sancuts Leo IV.</a>
            </li>
            <li>
              <a href="dy10034701s.html#62">Joannes XIX.</a>
            </li>
            <li>
              <a href="dy10034701s.html#65">Benedictus IX.</a>
            </li>
            <li>
              <a href="dy10034701s.html#67">Sanctus Leo IX.</a>
            </li>
            <li>
              <a href="dy10034701s.html#82">Victor II.</a>
            </li>
            <li>
              <a href="dy10034701s.html#84">Alexander II.</a>
            </li>
            <li>
              <a href="dy10034701s.html#86">Sanctus Gregorius
              VII.</a>
            </li>
            <li>
              <a href="dy10034701s.html#89">Paschalis II.</a>
            </li>
            <li>
              <a href="dy10034701s.html#91">Innocentius II.</a>
            </li>
            <li>
              <a href="dy10034701s.html#93">Eugenius III.</a>
            </li>
            <li>
              <a href="dy10034701s.html#98">Hadrianus IV.</a>
            </li>
            <li>
              <a href="dy10034701s.html#106">Alexander III.</a>
            </li>
            <li>
              <a href="dy10034701s.html#113">Urbanus III.</a>
            </li>
            <li>
              <a href="dy10034701s.html#117">Clemens III.</a>
            </li>
            <li>
              <a href="dy10034701s.html#119">Caelestinus III.</a>
            </li>
            <li>
              <a href="dy10034701s.html#122">Innocentius III.</a>
            </li>
            <li>
              <a href="dy10034701s.html#144">Honorius III.</a>
            </li>
            <li>
              <a href="dy10034701s.html#156">Gregorius IX.</a>
            </li>
            <li>
              <a href="dy10034701s.html#171">Innocentius IV.</a>
            </li>
            <li>
              <a href="dy10034701s.html#177">Alexander IV.</a>
            </li>
            <li>
              <a href="dy10034701s.html#188">Urbanus IV.</a>
            </li>
            <li>
              <a href="dy10034701s.html#189">Clemens IV.</a>
            </li>
            <li>
              <a href="dy10034701s.html#198">B. Gregorius X.</a>
            </li>
            <li>
              <a href="dy10034701s.html#199">Joannes XX., dictus
              XXI.</a>
            </li>
            <li>
              <a href="dy10034701s.html#220">Nicolaus III.</a>
            </li>
            <li>
              <a href="dy10034701s.html#248">Martinus IV.</a>
            </li>
            <li>
              <a href="dy10034701s.html#250">Honorius IV.</a>
            </li>
            <li>
              <a href="dy10034701s.html#256">Nicolaus IV.</a>
            </li>
            <li>
              <a href="dy10034701s.html#267">Sanctus Caelestinus
              V.</a>
            </li>
            <li>
              <a href="dy10034701s.html#269">Bonifacius VIII.</a>
            </li>
            <li>
              <a href="dy10034701s.html#285">Clemens V.</a>
            </li>
            <li>
              <a href="dy10034701s.html#297">Joannes XXI., dictus
              XXII.</a>
            </li>
            <li>
              <a href="dy10034701s.html#332">Benedictus XII.</a>
            </li>
            <li>
              <a href="dy10034701s.html#370">Clemens VI.</a>
            </li>
            <li>
              <a href="dy10034701s.html#391">Innocentius VI.</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy10034701s.html#414">Dissertatio de
          Antiquitate, Ditione, Juribus, variaque fortuna Abbatiae
          Sancti Salvatoris ad Montem Magellae</a>
        </li>
        <li>
          <a href="dy10034701s.html#467">Regestum</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
