<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghrid550724801s.html#10">Le Maraviglie dell'arte
      ovvero Le Vite de gl'illustri pittori veneti, e dello
      stato</a>
      <ul>
        <li>
          <a href="ghrid550724801s.html#12">Illustrissimi
          Signori miei Colendissimi</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#16">Cortese Lettore</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#21">Gio. Francesco
          Loredano al Cavalier Ridolfi</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#22">M. illustre Signor
          mio colendissimo</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#23">Dell'Eccellentissimo
          Signore Nicolo Crasso al Cavalier Ridolfi sopra Le Vite
          de'pittori</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#24">Dell'Eccellentissimo
          Signor Giulio Strozzi al Cavalier Ridolfi</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#25">Del Signor
          Alessandro Berardelli all'istesso</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#27">Del Clarissimo
          Signor Marc'Antonio Tirabosco al medesimo</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#28">Del Signor M.
          Antonio Mainenti</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#28">Di Don Francesco
          Maddalena</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#29">Ad Carolum
          Rodulphium Equitem</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#30">Lettore</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#32">Tavola de'pittori
          antichi, greci, e romani</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#34">Tavola delle vite
          de'pittori moderni &#160;veneti, e dello stato</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#36">Autori citati
          nell'opera</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#38">Tavola delle cose
          notabili, e de' luoghi dove sono le opere</a>
        </li>
        <li>
          <a href="ghrid550724801s.html#72">Parte Prima</a>
          <ul>
            <li>
              <a href="ghrid550724801s.html#88">Vita di
              Guariento padovano, e d'altri pittori veneti</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#98">Vita di Vittore
              Carpaccio pittore cittadino venetiano</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#103">Vita die Lazaro
              Sebastiani, e di Giovanni Mansueti pittori
              venetiani</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#105">Vita di Iacopo
              Bellino pittore cittadino venetiano</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#110">Vita di Gentile
              Bellino pittore, e Cavaliere</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#118">Vita di
              Giovanni Bellino pittore e cittadino venetiano</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#130">Vita di Gio.
              Battista Cima da Conegliano pittore et altri
              Discepoli di Gio. Bellino</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#133">Vita di
              Girolamo Santa Croce</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#135">Vita di Vicenzo
              Catena pittore cittadino venetiano</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#140">Vita di Andrea
              Mantegna cavaliere</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#150">Vita di
              Giorgione da Castel Franco pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#164">Vita di
              Bartolomeo e di Benedetto Montagna pittori
              vicentini</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#168">VIta di Gio.
              Antonio Regillo detto Licinio da Pordenone, ed altri
              Pittori del Friuli</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#192">Vita di Iacopo
              Palma il Vecchio, e d'altri Pittori Bergamaschi</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#200">Vita di Lorenzo
              Lotto pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#208">Vita di Titiano
              Vecellio da Cadore, pittore, e cavaliere</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#264">Esequie di
              Titiano</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#272">Vita di
              Francesco Vecellio pittore, Fratello di Titiano</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#282">Vita di Paris
              Bordone, e d'altri Pittori Trivigiani</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#294">Vita di
              Gioseppe Porta detto Salviati pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#300">Vita di Andrea
              Schiavone pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#318">Vita di
              Alessandro Bonvicino detto il Moretto e d'altri
              Pittori Bresciani</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#325">Vita di
              Girolamo Romanino pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#332">Vita di
              Lattantio Gambera pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#338">Vita di
              Girolamo Mutiano pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#342">Vita di
              Bonifacio venetiano pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#356">Vita di Paolo
              Caliari Veronese pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#412">Vita di Carlo e
              Gabrielle figliuoli di Paolo e di Benedetto il
              Fratello Caliari Pittori</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#422">Vita di
              Battista Zelotti Veronese pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#446">Vita di Iacopo
              da Ponte da Bassano pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#466">Vita di
              Francesco da Ponte da Bassano pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#474">Vita di Vicenzo
              Civerchio, e d'altri Pittori Cremaschi</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#475">Vita di Carlo
              Urbino pittore</a>
            </li>
            <li>
              <a href="ghrid550724801s.html#479">Vita di Aurelio
              Buso pittore</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
