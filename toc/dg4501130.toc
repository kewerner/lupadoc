<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501130s.html#10">Mirabilia Rome</a>
      <ul>
        <li>
          <a href="dg4501130s.html#11">De Portis infra urbem</a>
        </li>
        <li>
          <a href="dg4501130s.html#11">De Portis transtyberim</a>
        </li>
        <li>
          <a href="dg4501130s.html#12">De montibus infra urbem</a>
        </li>
        <li>
          <a href="dg4501130s.html#12">De Pontibus Urbis</a>
        </li>
        <li>
          <a href="dg4501130s.html#12">Palatia Imperatorum sunt
          hec</a>
        </li>
        <li>
          <a href="dg4501130s.html#13">De Arcubus
          triumphalibus</a>
        </li>
        <li>
          <a href="dg4501130s.html#14">De Arcubus non
          triumphalibus</a>
        </li>
        <li>
          <a href="dg4501130s.html#14">De termis</a>
        </li>
        <li>
          <a href="dg4501130s.html#15">De Theatris</a>
        </li>
        <li>
          <a href="dg4501130s.html#15">De Angulea sancti Petri</a>
        </li>
        <li>
          <a href="dg4501130s.html#15">De Cimiteriis</a>
        </li>
        <li>
          <a href="dg4501130s.html#16">Loca ubi Sancti passi
          sunt</a>
        </li>
        <li>
          <a href="dg4501130s.html#17">De Templis</a>
        </li>
        <li>
          <a href="dg4501130s.html#17">De Capitolio</a>
        </li>
        <li>
          <a href="dg4501130s.html#18">De Equis marmoreis</a>
        </li>
        <li>
          <a href="dg4501130s.html#19">De Rustico sedente super
          equum</a>
        </li>
        <li>
          <a href="dg4501130s.html#21">De Coliseo</a>
        </li>
        <li>
          <a href="dg4501130s.html#22">De sancta Maria rotunda</a>
        </li>
        <li>
          <a href="dg4501130s.html#24">De Octaviano Imperatore</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501130s.html#26">Modus confidenti</a>
      <ul>
        <li>
          <a href="dg4501130s.html#27">Modus confitendi compositus
          per Reverendum Episcopum Andream Hispanum sancte Romane
          Ecclesie Penitentiarium&#160;</a>
        </li>
        <li>
          <a href="dg4501130s.html#28">De Cogitatione</a>
        </li>
        <li>
          <a href="dg4501130s.html#29">De Locutione</a>
        </li>
        <li>
          <a href="dg4501130s.html#30">De septem peccatis
          mortalibus et primo de Superbia</a>
        </li>
        <li>
          <a href="dg4501130s.html#31">De Avaritia</a>
        </li>
        <li>
          <a href="dg4501130s.html#32">De Luxuria&#160;</a>
        </li>
        <li>
          <a href="dg4501130s.html#32">De Invidia</a>
        </li>
        <li>
          <a href="dg4501130s.html#32">De Gula</a>
        </li>
        <li>
          <a href="dg4501130s.html#33">De Ira</a>
        </li>
        <li>
          <a href="dg4501130s.html#33">De Accidia</a>
        </li>
        <li>
          <a href="dg4501130s.html#34">Dedecem preceptis legis</a>
        </li>
        <li>
          <a href="dg4501130s.html#39">De duodecim articulis
          fidei</a>
        </li>
        <li>
          <a href="dg4501130s.html#40">De septem Sacramentis
          Ecclesie</a>
        </li>
        <li>
          <a href="dg4501130s.html#40">De septem virtutibus
          theologicalibus</a>
        </li>
        <li>
          <a href="dg4501130s.html#41">Septem dona Spiritus
          sancti</a>
        </li>
        <li>
          <a href="dg4501130s.html#42">Duodecim fructus Spiritus
          sancti</a>
        </li>
        <li>
          <a href="dg4501130s.html#42">Octo beatitudines</a>
        </li>
        <li>
          <a href="dg4501130s.html#46">Sequitur oratio post
          Confessionem</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501130s.html#50">Interrogationes sive doctrine:
      quibus quilibet Sacerdos debet interrogare suum
      consitente&#160;</a>
      <ul>
        <li>
          <a href="dg4501130s.html#50">Qualis debet esse
          Confessor</a>
        </li>
        <li>
          <a href="dg4501130s.html#50">Quomodo confessor docebit
          confitentem</a>
        </li>
        <li>
          <a href="dg4501130s.html#51">Quomodo confessor
          confortabit confitentem</a>
        </li>
        <li>
          <a href="dg4501130s.html#51">Quomodo confessor ordinet
          confessionem</a>
        </li>
        <li>
          <a href="dg4501130s.html#52">Que sunt cavenda per
          confessorem</a>
        </li>
        <li>
          <a href="dg4501130s.html#52">Inquisitio confessoris erga
          confitentem</a>
        </li>
        <li>
          <a href="dg4501130s.html#52">Querenda circa quodlibet
          peccatum</a>
        </li>
        <li>
          <a href="dg4501130s.html#53">De querendis circa decem
          praecepta legis</a>
        </li>
        <li>
          <a href="dg4501130s.html#54">De querendis circa mortalia
          peccata</a>
        </li>
        <li>
          <a href="dg4501130s.html#54">Que vota sunt tenenda: et
          que non</a>
        </li>
        <li>
          <a href="dg4501130s.html#55">Que Iuramenta debent
          servari: et que non</a>
        </li>
        <li>
          <a href="dg4501130s.html#55">Ad Iuramenta requiruntur
          Tria</a>
        </li>
        <li>
          <a href="dg4501130s.html#56">Quibus Confessor prohibet
          recipere Corpus christi</a>
        </li>
        <li>
          <a href="dg4501130s.html#56">Qualiter confessio est
          facienda</a>
        </li>
        <li>
          <a href="dg4501130s.html#57">De confessione in
          generali</a>
        </li>
        <li>
          <a href="dg4501130s.html#57">De confessione in
          speciali</a>
        </li>
        <li>
          <a href="dg4501130s.html#58">De septem peccatis
          mortalibus</a>
        </li>
        <li>
          <a href="dg4501130s.html#59">De Avaritia seu
          cupiditate</a>
        </li>
        <li>
          <a href="dg4501130s.html#59">De Luxuria et
          pollutione</a>
        </li>
        <li>
          <a href="dg4501130s.html#60">De Ira seu rancore</a>
        </li>
        <li>
          <a href="dg4501130s.html#60">De Gulositate</a>
        </li>
        <li>
          <a href="dg4501130s.html#60">De Invidia seu
          malivolentia</a>
        </li>
        <li>
          <a href="dg4501130s.html#60">De accidia seu pigritia</a>
        </li>
        <li>
          <a href="dg4501130s.html#61">De decem preceptis
          legis</a>
        </li>
        <li>
          <a href="dg4501130s.html#61">De duodecim articulis
          fidei</a>
        </li>
        <li>
          <a href="dg4501130s.html#62">De vii operibus
          misericordie temporalibus</a>
        </li>
        <li>
          <a href="dg4501130s.html#63">De vii operibus
          spiritualibus</a>
        </li>
        <li>
          <a href="dg4501130s.html#63">De septem sacramentis
          Ecclesie</a>
        </li>
        <li>
          <a href="dg4501130s.html#63">Tractatus de
          absolutionibus</a>
        </li>
        <li>
          <a href="dg4501130s.html#64">Qui non debent absolui: et
          cuius absolutio non valet</a>
        </li>
        <li>
          <a href="dg4501130s.html#65">Casus multum utiles circa
          Confessionem</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501130s.html#66">Coniuratio malignorum spirituum
      in corporibus hominum existentium prout fit in sancto
      Petro</a>
      <ul>
        <li>
          <a href="dg4501130s.html#66">Secundum Matheum</a>
        </li>
        <li>
          <a href="dg4501130s.html#66">Secundum Lucam</a>
        </li>
        <li>
          <a href="dg4501130s.html#67">Oratio</a>
        </li>
        <li>
          <a href="dg4501130s.html#67">Alia oratio</a>
        </li>
        <li>
          <a href="dg4501130s.html#68">Oratio</a>
        </li>
        <li>
          <a href="dg4501130s.html#69">Officium super hiis: qui a
          demonibus vexantur</a>
        </li>
        <li>
          <a href="dg4501130s.html#70">Sequitur Oratio</a>
        </li>
        <li>
          <a href="dg4501130s.html#70">Oratio</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501130s.html#82">Orationes sancte Brigitte</a>
      <ul>
        <li>
          <a href="dg4501130s.html#83">Hec sunt Quindecim Collecte
          sive Orationes [...]</a>
        </li>
        <li>
          <a href="dg4501130s.html#92">Sequitur Oratio eximii
          doctoris s.cti Augustini</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501130s.html#98">Divisiones Decem Nationum
      totius Christianitatis</a>
    </li>
    <li>
      <a href="dg4501130s.html#106">Tabula Christiane religionis
      valde utilis et necessaria cuilibet Christiano quam omnes
      scire tenentur</a>
      <ul>
        <li>
          <a href="dg4501130s.html#106">Ultima enim sunt hec
          [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501130s.html#136">Roma Civitas sancta Caput
      mundi</a>
      <ul>
        <li>
          <a href="dg4501130s.html#136">[Indulgentiae]</a>
        </li>
        <li>
          <a href="dg4501130s.html#162">Oratio devotissima de
          Sudario sacratissimi Vultus domini nostri Jesu Christi
          vel Veronica&#160;</a>
        </li>
        <li>
          <a href="dg4501130s.html#164">Incipiunt indulgentie
          septem ecclesiarum principalium urbis Rome&#160;</a>
        </li>
        <li>
          <a href="dg4501130s.html#238">Stationes in
          Quadragesima</a>
        </li>
        <li>
          <a href="dg4501130s.html#240">Stationes post Pascha</a>
        </li>
        <li>
          <a href="dg4501130s.html#241">Stationes in
          Adventum&#160;</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
