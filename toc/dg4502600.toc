<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502600s.html#8">Roma antica e moderna</a>
      <ul>
        <li>
          <a href="dg4502600s.html#10">All'Eminentissimo e
          Reverendissimo Signore il Signor Cardinale Ottobono</a>
        </li>
        <li>
          <a href="dg4502600s.html#12">Tavola di quanto si
          contiene nella Roma Antica, e Moderna</a>
        </li>
        <li>
          <a href="dg4502600s.html#30">Delli Titoli de' Cardinali
          di Santa Chiesa</a>
        </li>
        <li>
          <a href="dg4502600s.html#33">Numero di tutte le Chiese,
          Parochie [...]</a>
        </li>
        <li>
          <a href="dg4502600s.html#38">Le sette chiese
          principali&#160;</a>
        </li>
        <li>
          <a href="dg4502600s.html#68">[Altre chiese]</a>
        </li>
        <li>
          <a href="dg4502600s.html#76">Nell'Isola</a>
        </li>
        <li>
          <a href="dg4502600s.html#79">In Trastevere</a>
        </li>
        <li>
          <a href="dg4502600s.html#101">Nel Borgo</a>
        </li>
        <li>
          <a href="dg4502600s.html#118">Dalla Porta Flaminia,
          overo del Popolo á mano destra, e sinistra, fino alla
          Madonna delli Monti</a>
        </li>
        <li>
          <a href="dg4502600s.html#184">Das Giesu, in Parione,
          Strada Giulia, alla Regola, e restante, infino ad
          Araceli</a>
        </li>
        <li>
          <a href="dg4502600s.html#340">Dal Campidoglio da ogni
          parte finendo a Sant'Agnese di Porta Pia</a>
        </li>
        <li>
          <a href="dg4502600s.html#426">Le Stationi che sono nella
          Chiese dentro, e fuori di Roma</a>
        </li>
        <li>
          <a href="dg4502600s.html#438">La Guida Romana per li
          forastieri</a>
          <ul>
            <li>
              <a href="dg4502600s.html#438">Giornata prima</a>
            </li>
            <li>
              <a href="dg4502600s.html#444">Giornata seconda</a>
            </li>
            <li>
              <a href="dg4502600s.html#450">Giornata terza</a>
            </li>
            <li>
              <a href="dg4502600s.html#457">Indice brevissimo de'
              Pontefici Romani</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502600s.html#486">Roma antica figurata</a>
          <ul>
            <li>
              <a href="dg4502600s.html#835">Catalogo delli Re, et
              Imperatori Romani, e di molti altri [...]&#160;</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
