<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="eerc2235701s.html#10">Le Pitture antiche
      d'Ercolano e Corntorni incise con qualche spiegazioni</a>
      <ul>
        <li>
          <a href="eerc2235701s.html#12">[Dedica]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="eerc2235701s.html#14">[Text]</a>
      <ul>
        <li>
          <a href="eerc2235701s.html#14">I. [Cinque Eroine che
          giocano con gli astragali] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#18">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#20">II. [Eurito Centauro,
          Ippodamia e Teseo] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#22">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#24">III. [Vecchio con
          bambino, donna e Pastora/ Ninfa] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#28">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#30">IV. [Scena di Teatro]
          ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#32">[Tavola]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#34">V. [Teseo in Creta con
          Minotauro] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#38">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#40">VI. [Cerva, Telefo e
          Ercole] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#44">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#46">VII. [la prima fatica
          di Ercole] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#50">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#52">VIII. [Achille e
          Chirone] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#56">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#58">IX. [il Sartiro Marsia
          e il giovane Olimpo] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#60">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#62">X. [il Ciclope Polifemo
          e Galatea] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#66">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#68">XI. [Ifigenia riconosce
          Oreste] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#74">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#76">XII. [Altra scena da
          Ifigenia in Tauride] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#80">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#82">XIII. [Didone?] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#86">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#88">XIV. [Cena domestica]
          ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#92">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#94">XV. [Fauno che bacia
          una Baccante] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#98">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#100">XVI. [Fauno che tenta
          baciare una Ninfa] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#104">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#106">XVII. [Due ballatrici]
          ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#108">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#110">XVIII. [Baccante
          danzante] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#112">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#114">XIX. [Baccante] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#116">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#118">XX. [Baccante] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#122">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#124">XXI. [Baccante
          danzante e suonante] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#128">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#130">XXII. [Baccante] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#132">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#134">XXIII. [Baccante]
          ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#136">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#138">XXIV. [Baccante] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#142">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#144">XXV. [Centauro e
          Baccante] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#148">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#150">XXVI. [Centauressa che
          porta una donna] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#154">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#156">XXVII. [Centauro con
          Baccante] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#158">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#160">XXVIII. [Centauressa
          con giovanetto] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#162">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#164">XXIX. [Quattro Cupidi
          che giocano con due troni che rappresentano Venere e
          Marte: (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#168">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#170">XXX. [Putti alati (due
          immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#174">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#176">XXXI. [Quattro puttini
          (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#180">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#182">XXXII. [Quattro
          puttini (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#184">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#186">XXXIII. [Sei puttini
          (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#188">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#190">XXXIV. [Cinque puttini
          (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#194">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#196">XXXV. [Cinque puttini
          (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#200">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#202">XXXVI. [Cinque puttini
          (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#204">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#206">XXXVII. [Tre puttini
          (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#210">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#212">XXXIII. [Due puttini,
          serpente con giovanetto (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#220">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#222">XXXIX. [Finta
          architettura] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#227">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#229">XL. [Finta
          architettura] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#231">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#233">XLI. [Finta
          architettura] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#235">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#237">XLII. [Finta
          architettura (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#239">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#241">XLIII. [Finta
          architettura] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#243">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#245">XLIV. [Finta
          architettura (cinque immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#247">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#249">XLV. [Due navi a
          guerra, pesci di sorti diverse (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#254">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#256">XLVI. [Edificio sul
          lido e navi, uccelli e natura morte (quattro immagini)]
          ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#258">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#260">XLVII. [Papagallo che
          tira un cocchio guidato da un grillo, pesci die varie
          forti (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#262">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#264">XLVIII. [Ninfa, testa
          di Medusa e vista su un Tempio (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#268">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#270">XLIX. [Quercia, testa
          di Medusa e alcuni edifici (due immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#272">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#274">L. [Coccodrillo e
          Ippopotamo sul Nilo, e quattro deità egiziane (tre
          immagini)] ▣</a>
          <ul>
            <li>
              <a href="eerc2235701s.html#278">[Tavola] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="eerc2235701s.html#280">Alcune
          Osservazioni</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="eerc2235701s.html#296">Indice delle cose
      notabili</a>
    </li>
  </ul>
  <hr />
</body>
</html>
