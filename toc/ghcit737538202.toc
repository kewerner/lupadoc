<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghcit737538202s.html#6">Catalogo istorico de’
      pittori e scultori ferraresi e delle opere loro; Tomo II.</a>
      <ul>
        <li>
          <a href="ghcit737538202s.html#12">Nomi de' professori
          di pittura, e scultura&#160;</a>
        </li>
        <li>
          <a href="ghcit737538202s.html#229">Memorie aggiunte al
          tomo primo</a>
        </li>
        <li>
          <a href="ghcit737538202s.html#249">Memorie aggiunte al
          tomo secondo</a>
        </li>
        <li>
          <a href="ghcit737538202s.html#269">Registro de'
          ritratti</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
