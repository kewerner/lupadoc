<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="perb10038404s.html#6">Giornale delle belle arti e
      della incisione antiquaria, musica e poesia</a>
      <ul>
        <li>
          <a href="perb10038404s.html#8">Eminentissimo
          Principe</a>
        </li>
        <li>
          <a href="perb10038404s.html#10">Num. 1. li 6. Gennaro
          1878&#160;</a>
        </li>
        <li>
          <a href="perb10038404s.html#18">Num. 2. li 13. Gennaro
          1787&#160;</a>
        </li>
        <li>
          <a href="perb10038404s.html#26">Num. 3. li 20. Gennaro
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#34">Num. 4. li 27. Gennaro
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#42">Num. 5. li 3. Febrajo
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#50">Num. 6. li 10. Febrajo
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#58">Num. 7. li 17. Febrajo
          1787&#160;</a>
        </li>
        <li>
          <a href="perb10038404s.html#66">Num. 8. li 24. Febrajo
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#74">Num. 9. li 31. Febrajo
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#82">Num. 10. li 10. Marzo
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#90">Num. 11. li 17. Marzo
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#98">Num. 12. li 24. Marzo
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#106">Num. 13. li 31. Marzo
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#114">Num. 14. li 7. Aprile
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#122">Num. 15. li 14. Aprile
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#130">Num. 16. li 21. Aprile
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#138">Num. 17. li 28. Aprile
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#146">Num. 18. li 5. Maggio
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#154">Num. 19. li 12. Maggio
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#162">Num. 20. li 19. Maggio
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#170">Num. 21. li 26. Maggio
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#178">Num. 22. li 2. Guigno
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#186">Num. 23. li 9. Giugno
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#194">Num. 24. li 16. Giugno
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#202">Num. 25. li 23. Giugno
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#210">Num. 26. li 30. Giugno
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#218">Num. 27. li 7. Luglio
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#226">Num. 28. li 14. Luglio
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#234">Num. 29. li 21. Luglio
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#242">Num. 30. li 28. Luglio
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#250">Num. 31. li 4. Agosto
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#258">Num. 32. li 11. Agosto
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#266">Num. 33. li 18. Agosto
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#274">Num. 34. li 25. Agosto
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#282">Num. 35. li 1.
          Settembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#290">Num. 36. li 8.
          Settembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#298">Num. 37. li 15.
          Settembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#306">Num. 38. li 22.
          Settembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#314">Num. 39. li 29.
          Settembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#322">Num. 40. li 6. Ottobre
          1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#330">Num. 41. li 13.
          Ottobre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#338">Num. 42. li 20.
          Ottobre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#346">Num. 43. li 27.
          Ottobre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#354">Num. 44. li 3.
          Novembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#362">Num. 45. li 10.
          Novembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#370">Num. 46. li 17.
          Novembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#378">Num. 47. li 24.
          Novembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#386">Num. 48. il di 1.
          Dicembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#394">Num. 49. il di 8.
          Decembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#402">Num. 50. il di 15.
          Decembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#410">Num. 51. il di 22.
          Decembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#418">Num. 52. il di 29.
          Decembre 1787</a>
        </li>
        <li>
          <a href="perb10038404s.html#426">Indice delle cose più
          notabili in questo Quarto Tomo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
