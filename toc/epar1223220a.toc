<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="epar1223220as.html#6">Parma</a>
      <ul>
        <li>
          <a href="epar1223220as.html#10">Al lettore benevolo, e
          Concittadino Divoto</a>
        </li>
        <li>
          <a href="epar1223220as.html#12">I. Delle Feste
          Solenni, Sacre Funzioni, e divote Processioni per tutto
          il corso dell'Anno&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#57">II. Delle Vite de'
          Santi Protettori della Città di Parma&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#79">III. Delle Vite
          d'alcuni pochi Santi, e Beati Parmigiani, fra i molti
          altri Nazionali&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#101">IV. Delle Feste
          Mobili di tutto l'Anno&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#111">V. Delle Benedizioni
          del Venerabile&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#114">VI. Dell'Officio de'
          Defonti&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#116">VII. Delle Chiese
          Parrocchiali di Parma, e del numero d'esse&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#118">VIII. Delle Chiese
          de' Regolari, e loro numero&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#120">IX.De' Monisteri
          delle Monache&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#122">X. Delle
          Confraternite Secolari&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#124">Protesta
          dell'Autore</a>
        </li>
        <li>
          <a href="epar1223220as.html#128">Nuova Aggiunta al
          libro intitolato: Parma Città d'Oro</a>
        </li>
        <li>
          <a href="epar1223220as.html#174">XI. Delle altre
          Chiese di Parma, oltre le Chiese Parrochiali, Chiese de'
          Regolari, di Monache, ed Oratorj di Confraternite</a>
        </li>
        <li>
          <a href="epar1223220as.html#178">XII. De' Collegj,
          Seminarj, ed Ospizj</a>
        </li>
        <li>
          <a href="epar1223220as.html#182">XIII. De' Spedali,
          Conservatorj, e Luoghi Pii</a>
        </li>
        <li>
          <a href="epar1223220as.html#184">XIV. Delle Scuole
          della Dottrina Cristiana</a>
        </li>
        <li>
          <a href="epar1223220as.html#187">XV. De' Santi
          Protettori delle Arti</a>
        </li>
        <li>
          <a href="epar1223220as.html#190">XVI. D'altre cose
          utili, e degne da sapersi da ogni Crisitano&#160;</a>
        </li>
        <li>
          <a href="epar1223220as.html#196">XVII. Dell'origine,
          ed uso delle Campane delle Chiese</a>
        </li>
        <li>
          <a href="epar1223220as.html#199">XVIII. Delle
          Rogazioni, o Litanie suseguenti alla Domenica Quinta dopo
          la Pasqua di Resurrezione</a>
        </li>
        <li>
          <a href="epar1223220as.html#205">XIX. Delle Immagini,
          e Reliquie de' Santi nelle Chiese, e dell'uso, e virtù
          dell'Acqua benedetta</a>
        </li>
        <li>
          <a href="epar1223220as.html#208">XX. Degli Agnus Dei
          di cera bianca, benedetti da' Sommi Pontefici</a>
        </li>
        <li>
          <a href="epar1223220as.html#212">XXI. Dell'effetto
          dell'Indulgenza Plenaria, ed in quanta stima devono
          tenersi le Medaglie, o Corone, o Crocefissi</a>
        </li>
        <li>
          <a href="epar1223220as.html#216">Sommario delle
          Indulgenze</a>
        </li>
        <li>
          <a href="epar1223220as.html#218">Decreto</a>
        </li>
        <li>
          <a href="epar1223220as.html#219">Indulgenze, ed utili,
          che si acquistano, nel frequentare questa breve
          Orazione</a>
        </li>
        <li>
          <a href="epar1223220as.html#222">Indulgenze, concesse
          dalla Sanità di N.S. Benedetto XIII. a chiunque de'
          Fedeli dell'uno, e dell'altro sesso, che reciterà in
          ginocchio l'Ave Maria ogni volta che suona</a>
        </li>
        <li>
          <a href="epar1223220as.html#224">Indice delle
          notizie</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
