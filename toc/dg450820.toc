<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg450820s.html#12">Blondi Flavii Forliviensis in
      Romam instauratam Praefatio ad Eugenium quartum pontificem
      maximum Romanorum</a>
    </li>
    <li>
      <a href="dg450820s.html#90">Ad Franciscum Foscari
      serenissimum ducem inclytumque Senatum et patritios Venetae
      rei publicae Blondus Flavius Forliviensis de origine et
      gestis Venetorum</a>
    </li>
    <li>
      <a href="dg450820s.html#120">Blondi Flavii Forliviensis in
      Italiam illustratam Praefatio</a>
    </li>
  </ul>
  <hr />
</body>
</html>
