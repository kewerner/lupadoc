<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="mk6042404s.html#8">Storia dell'arte col mezzo dei
      monumenti dalla sua decadenza nel IV secolo fino al suo
      risorgimento nel XVI. Volume IV. Contenente le tavole di
      Scultura</a>
    </li>
    <li>
      <a href="mk6042404s.html#12">Scultura</a>
      <ul>
        <li>
          <a href="mk6042404s.html#12">Indice delle tavole
          Contenente un sommario dei diversi monumenti che esse
          rappresentano, ed alcune notizie che non potevano essere
          inserite nel corpo dell'Opera</a>
          <ul>
            <li>
              <a href="mk6042404s.html#12">I. Decadenza della
              Scultura dal II secolo fino al XIII</a>
              <ul>
                <li>
                  <a href="mk6042404s.html#11">Tavola I. Scelta
                  dei monumenti più belli della Scultura
                  antica&#160;</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#12">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#14">Tavola II.
                  Parallelo di Bassirilevi [sic] degli Archi
                  trionfali di Tito, di Settimio Severo, e di
                  Costantino. I. II. IV. Secolo ›Arco di Tito‹
                  ›Arco di Settimio Severo‹ ›Arco di Costantino‹
                  ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#16">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#18">Tavola III. Statue
                  rappresentanti Costantino e suoi figli.
                  Bassirilevi [sic] e altre opere del medesimo
                  tempo. IV Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#17">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#20">Tavola IV. Urne
                  sepolcrali ritrovate nelle Catacombe di
                  Sant'Urbano ›Catacombe di Pretestato‹ e di
                  Torre-pignattara ›Catacombe dei Santi Marcellino
                  e Pietro‹ in Roma. IV. Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#24">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#22">Tavola V.
                  Bassirilievi, e ornamenti di varie urne
                  sepolcrali tratti dalle Catacombe. I. Secolo del
                  Cristianesimo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#24">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#26">Tavola VI. Altri
                  bassirilievi scolpiti sulle urne delle Catacombe.
                  IV. Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#25">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#28">Tavola VII. Figure
                  e iscrizioni intagliate in cavo sulle pietre
                  Sepolcrali delle Catacome [sic]</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#30">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#32">Tavola VIII.
                  Collezzione [sic] di differenti Soggetti Scolpiti
                  nelle Catacombe: Iscrizioni Sepolcrali</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#31">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#38">Tavola IX.
                  Cofanetto d'argento, Scatola da profumi, e altri
                  utensili di una dama Romana. IV. e V. Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#37">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#42">Tavola X.
                  Bassilirilievi del piedistallo dell'Obelisco
                  rialzato da Teodosio nell'Ippodromo di
                  Costantinopoli. Medaglie di què [sic] tempi. IV.
                  Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#44">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#46">Tavola XI.
                  Piedistallo e parte dei bassirilievi della
                  Colonna di Teodosio in Costantinopoli. IV. e V.
                  Secolo &#160;</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#45">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#48">Tavola XII.
                  Bassirilievi ricavati dai dittici greci e Latini,
                  e altre opere in avorio: dal IV. fino all'XI.
                  Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#50">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#54">Tavola XIII. Porta
                  principale di ›San Paolo fuori le Mura‹ di Roma,
                  ornata di figure a contorni incavate nel bronzo e
                  damaschinate in argento; lavoro eseguito a
                  Costantinopoli. XI Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#53">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#56">Tavola XIV. Porta
                  di ›San Paolo fuori le Mura‹: disegno in grande
                  di una porzione dei soggetti incisi ne' suoi
                  compartimenti. XI Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#60">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#58">Tavola XV. Porta
                  di ›San Paolo fuori le Mura‹: continuazione dei
                  soggetti e delle iscrizioni incise ne' suoi
                  compartimenti. XI Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#60">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#62">Tavola XVI. Porta
                  di ›San Paolo fuori le Mura‹: continuazione dei
                  soggetti e delle figure incise ne' suoi
                  compartimenti. XI Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#61">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#64">Tavola XVII. Porta
                  di ›San Paolo fuori le Mura‹: continuazione delle
                  figure e dei soggetti incisi ne' suoi
                  compartimenti. XI Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#68">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#66">Tavola XVIII.
                  Porta di ›San Paolo fuori le Mura‹: continuazione
                  delle figure e delle iscrizioni incise ne' suoi
                  compartimenti. XI Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#68">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#70">Tavola XIX.
                  Continuazione delle figure incise nei
                  compartimenti della porta di ›San Paolo fuori le
                  Mura‹. XI Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#69">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#72">Tavola XX. Porta
                  di ›San Paolo fuori le Mura‹, disegni in grande
                  di alcune Figure, iscrizioni e forma dei loro
                  caratteri. XI Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#74">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#76">Tavola XXI.
                  Bassirilievi e Sculture in marmo, Cesellature in
                  bronzo e in argento. XII. Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#75">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#80">Tavola XXII.
                  Bassirilievi eseguiti in Legno sulla porta della
                  Chiesa di ›Santa Sabina‹ in Roma. XIII. Secolo
                  ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#79">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#82">Tavola XXIII.
                  Tabernacolo di San Paolo Fuori delle mura ›San
                  Paolo fuori le Mura: Ciborio‹ di Roma. XIII.
                  Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#79">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#84">Tavola XXIV.
                  Mausoleo del Cardinale Consalvo ›Tomba del
                  cardinale Consalvo Rodriguez‹ in ›Santa Maria
                  Maggiore‹ a Roma. XIII. Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#88">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#86">Tavola XXV. Globo
                  celeste Cufico-Arabo del Museo Borgia a Velletri.
                  XIII. Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#88">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#90">Tavola XXVI.
                  Riunione di diverse opere di Scultura eseguite in
                  Italia dal V al XIII Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#92">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#96">Tavola XXVI A.
                  Parte anteriore del Palliotto [sic] dell'Altare
                  maggiore della Basilica di Sant'Ambrogio in
                  Milano. IX. Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#98">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#100">Tavola XXVI B.
                  Parti laterali del Palliotto [sic] dell'Altar
                  maggiore della Basilica di Sant'Ambrogio in
                  Milano. IX. Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#99">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#102">Tavola XXVI C.
                  Parte posteriore del Palliotto [sic] dell'Altar
                  maggiore della Basilica di Sant'Ambrogio in
                  Milano. IX. Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#99">[Text]</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042404s.html#105">II. Risorgimento
              della Scultura nel XIII secolo &#160;</a>
              <ul>
                <li>
                  <a href="mk6042404s.html#106">Tavola XXVII.
                  Statue, Bassirilievi e Medaglie. XII. XIII. e
                  XIV. Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#105">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#108">Tavola XXVIII.
                  Mausoleo dei Savelli ›Sepolcro di Luca Savelli‹
                  nella chiesa di ›Santa Maria in Aracoeli‹ a Roma.
                  XIII e XIV Secolo ▣</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#110">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#112">Tavola XXIX.
                  Sculture eseguite in [sic: ma è 'fuori'] Italia
                  dal principio della decadenza dell'Arte fino al
                  XIV Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#111">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#118">Tavola XXX.
                  Mausoleo del re Roberto a Napoli ed altri
                  monumenti della Casa d'Anjou. XIII e XIV
                  Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#120">[Text]</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#122">Tavola XXXI.
                  Tomba della regina Sancia d'Aragona nella chiesa
                  di Santa Maria della Croce a Napoli. XIV
                  Secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#121">[Text]</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
            <li>
              <a href="mk6042404s.html#126">III. Risorgimento
              della Scultura, sul finire del XIII secolo al
              principiare del XIV</a>
              <ul>
                <li>
                  <a href="mk6042404s.html#126">Prima epoca</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#124">Tavola XXXII.
                      Opere di Nicola Pisano e de' suoi allievi.
                      XIII e XIV Secolo</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#126">[Text]</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#130">Tavola
                      XXXIII. Continuazione delle opere di Nicola
                      Pisano e de' suoi allievi; bassirilievi della
                      facciata principale della cattedrale di
                      Orvieto. XIII e XIV Secolo</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#129">[Text]</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#132">Tavola XXXIV.
                      Mausoleo di San Pietro martire nella Chiesa
                      di Sant'Eustorgio a Milano. XIV Secolo</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#134">[Text]&#160;</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#136">Tavola XXXV.
                      Statue, bassirilievi ed altre sculture di
                      diverse Scuole d'Italia. XIV Secolo</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#135">[Text]&#160;</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#140">Tavola XXXVI.
                      Tabernacolo dell'Altar maggiore di ›San
                      Giovanni in Laterano‹ a Roma. XIV Secolo
                      ▣</a>
                      <ul>
                        <li>
                          <a href="mk6042404s.html#139">[Text]
                          &#160;</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#142">Tavola
                      XXXVII. Ceselatura [sic] Busti di San Pietro
                      e di San Paolo nella Chiesa di ›San Giovanni
                      in Laterano‹ a Roma. XIV Secolo ▣</a>
                      <ul>
                        <li>
                          <a href="mk6042404s.html#146">[Text]
                          &#160;</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#144">Tavola
                      XXXVIII. Statue, bassirilievi ed altre
                      sculture di diverse Scuole in Italia e fuori
                      d'Italia. XV Secolo</a>
                      <ul>
                        <li>
                          <a href="mk6042404s.html#146">[Text]
                          &#160;</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#150">Tavola XXXIX.
                      Mausoleo del Cardinale d'Alençon nella Chiesa
                      di ›Santa Maria in Trastevere‹ a Roma. XV
                      Secolo ▣</a>
                      <ul>
                        <li>
                          <a href="mk6042404s.html#152">[Text]
                          &#160;</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#154">Tavola XL.
                      Mappamondo inciso sopra una lastra di rame,
                      specie di lavoro alla damaschina. XV
                      Secolo</a>
                      <ul>
                        <li>
                          <a href="mk6042404s.html#153">[Text]
                          &#160;</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#158">Epoca seconda.
                  Progresso del risorgimento della Scultura alla
                  metà del XV secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#156">Tavola XLI.
                      Porta principale del Battistero di Firenze:
                      lavoro in bronzo di Lorenzo Ghiberti. XV
                      Secolo</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#158">[Text]</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#160">Tavola XLII.
                      Dettagli dei bassirilievi della Porta del
                      Battistero di Firenze: Miracolo di San
                      Zenobio altro bassorilievo di Lorenzo
                      Ghiberti. XV Secolo</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#164">[Text]</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#162">Tavola XLIII.
                      Incisioni in incavo eseguite sopra una
                      cassettina di cristallo di rocca da Valerio
                      Belli Vicentino. XVI Secolo</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#164">[Text]</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#166">Tavola XLIV.
                      Medaglioni diversi in legno ed in bronzo. XV
                      e XVI Secolo</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#165">[Text]</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#168">Tavola XLV.
                      Mausoleo della Famiglia Bonsi a San Gregorio
                      sul monte Celio ›San Gregorio Magno‹ a Roma.
                      XVI Secolo ▣</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#170">[Text]</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="mk6042404s.html#171">Epoca terza.
                  Totale risorgimento della Scultura nel XVI
                  secolo</a>
                  <ul>
                    <li>
                      <a href="mk6042404s.html#172">Tavola XLVI.
                      Schizzo del mausoleo progettato da
                      Michelangelo per la sepoltura del pontefice
                      Giulio II, nella chiesa di ›San Pietro in
                      Vincoli‹ a Roma. XVI Secolo ▣</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#171">[Text]</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#174">Tavola XLVII.
                      Altre opere di Scultura di Michelangelo
                      Buonarroti. XVI Secolo</a>
                      <ul>
                        <li>
                          <a href=
                          "mk6042404s.html#176">[Text]</a>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="mk6042404s.html#178">Tavola
                      XLVIII. Specie di riassunto generale della
                      Storia della Scultura per mezzo delle
                      medaglie e delle pietre incise</a>
                      <ul>
                        <li>
                          <a href="mk6042404s.html#177">Medaglie
                          [Text]</a>
                        </li>
                        <li>
                          <a href=
                          "mk6042404s.html#183">Medaglioni
                          [Text]</a>
                        </li>
                        <li>
                          <a href="mk6042404s.html#186">Pietre
                          incise [Text]</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="mk6042404s.html#192">Titoli e soggetti delle
      tavole relative alla scultura</a>
    </li>
  </ul>
  <hr />
</body>
</html>
