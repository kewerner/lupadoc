<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="fa160780s.html#12">Andreae Schotti Itinerarium
      Italiae</a>
      <ul>
        <li>
          <a href="fa160780s.html#14">Illustrissimo ac
          reverendissimo Francesco Card. Barberino</a>
        </li>
        <li>
          <a href="fa160780s.html#17">In itineribus observanda</a>
        </li>
        <li>
          <a href="fa160780s.html#18">Itinerariu,. seu iter
          agentium preces</a>
        </li>
        <li>
          <a href="fa160780s.html#24">Italiae laus et divisio</a>
        </li>
        <li>
          <a href="fa160780s.html#36">Itinerarii Italiae</a>
          <ul>
            <li>
              <a href="fa160780s.html#36">Liber primus</a>
            </li>
            <li>
              <a href="fa160780s.html#294">Liber secundus</a>
            </li>
            <li>
              <a href="fa160780s.html#514">Liber tertius</a>
            </li>
            <li>
              <a href="fa160780s.html#630">Index urbium et
              locorum</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
