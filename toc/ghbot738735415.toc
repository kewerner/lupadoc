<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbot738735415s.html#6">Raccolta di lettere sulla
      pittura, scultura ed architettura, scritte da’ più celebri
      professori che in dette arti fiorirono dal sec. XV al XVII,
      tomo quinto</a>
      <ul>
        <li>
          <a href="ghbot738735415s.html#8">All'illustrissimo, e
          reverendissimo Signore Monsignor D. Sergio Sersale</a>
        </li>
        <li>
          <a href="ghbot738735415s.html#12">All'erudito
          lettore</a>
        </li>
        <li>
          <a href="ghbot738735415s.html#14">Lettere su la
          pittura scultura ed architettura</a>
        </li>
        <li>
          <a href="ghbot738735415s.html#346">Indice degli autori
          delle lettere di questo tomo</a>
        </li>
        <li>
          <a href="ghbot738735415s.html#348">Indice delle cose
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
