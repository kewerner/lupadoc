<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501754s.html#8">Rom. Eine Münchner Pilgerfahrt
      im Jubeljahr 1575</a>
      <ul>
        <li>
          <a href="dg4501754s.html#12">Inhaltsverzeichnis</a>
        </li>
        <li>
          <a href="dg4501754s.html#14">Vorwort</a>
        </li>
        <li>
          <a href="dg4501754s.html#18">Einleitung</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501754s.html#36">Beschreibung aller denkwürdigen
      Sachen so ein Wallfahrer aus Bayern den Weg nach Trient auf
      Rom im Jubeljahr 1575 in Klöstern Kirchen und Stätten
      erfahren und gesehen hat [...]</a>
      <ul>
        <li>
          <a href="dg4501754s.html#38">Rabus beginnt:</a>
        </li>
        <li>
          <a href="dg4501754s.html#57">Von den Hauptkirchen zu
          Rom, auch Ab- und Einteilung derselben</a>
        </li>
        <li>
          <a href="dg4501754s.html#93">Beschreibung der h.
          Fastenzeit des Jubeljahrs und ihr gewöhnlichen Stationen,
          wie die von frommen Pilgram verrichtet worden</a>
        </li>
        <li>
          <a href="dg4501754s.html#155">Eigentliche Beschreibung
          etlicher anderer heiliger und fürnehmer Kirchen, so die
          Pilgram außerhalb der gewöhnlichen Stationen mit Andacht
          besucht haben</a>
        </li>
        <li>
          <a href="dg4501754s.html#187">Hernacher folgt ein kurze
          Verzeichnis, was maßen fromme Pilgram wiederumb nachhause
          gereist sein</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501754s.html#226">Anhang</a>
      <ul>
        <li>
          <a href="dg4501754s.html#226">Rabus an Herzog Albrecht
          V. von Bayern, bittet den Herzog, seine Mauleselin, di
          Begleiterin auf seiner Romreise, in die fürstliche
          Hof-Fütterei einstellen zu dürfen [...]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
