<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="caman9815020s.html#14">Andrea Mantegna</a>
      <ul>
        <li>
          <a href="caman9815020s.html#16">Vorwort</a>
        </li>
        <li>
          <a href="caman9815020s.html#22">Inhalt</a>
        </li>
        <li>
          <a href="caman9815020s.html#24">Verzeichnis der
          Vollbilder</a>
        </li>
        <li>
          <a href="caman9815020s.html#26">Verzeichnis der
          Textillustrationen</a>
        </li>
        <li>
          <a href="caman9815020s.html#34">I. Mantegnas Heimat und
          seine geistige und künstlerische Jugenentwicklung</a>
        </li>
        <li>
          <a href="caman9815020s.html#99">II. Die Fresken der
          Eremitanikapelli in Padua&#160;</a>
        </li>
        <li>
          <a href="caman9815020s.html#167">III. Die frühesten
          Tafelbilder</a>
        </li>
        <li>
          <a href="caman9815020s.html#207">IV. Das Tryptichon für
          S. Zeno in Verona und andere gleichzeitige Werke</a>
        </li>
        <li>
          <a href="caman9815020s.html#263">V. Mantegna in
          Mantua</a>
        </li>
        <li>
          <a href="caman9815020s.html#295">VI. Die ersten Werke
          der Mantuaner Zeit bis zu den Fresken der Camera degli
          sposi</a>
        </li>
        <li>
          <a href="caman9815020s.html#332">VII. Die Camera degli
          sposi im Castello di Corte zu Mantua&#160;</a>
        </li>
        <li>
          <a href="caman9815020s.html#374">VIII. Der Triumph
          Cäsaers und die Fresken im Belvedere des Vatican</a>
        </li>
        <li>
          <a href="caman9815020s.html#410">IX. Die grossen
          Altargemälde und andere religiöse Bilder der Spätzeit</a>
        </li>
        <li>
          <a href="caman9815020s.html#467">X. Die mythologischen
          und allegorischen Bilder der letzten Zeit</a>
        </li>
        <li>
          <a href="caman9815020s.html#516">XI. Mantegna als
          Kupferstecher</a>
        </li>
        <li>
          <a href="caman9815020s.html#563">XII. Mantegna Tod.
          Seine Familie und seine Person. Die Wirkung seiner Kunst
          und ihre Schätzung&#160;</a>
        </li>
        <li>
          <a href="caman9815020s.html#588">Unbenannt</a>
        </li>
        <li>
          <a href="caman9815020s.html#597">Nachrichten über
          verlorene oder verschollene Werke Mantegnas</a>
        </li>
        <li>
          <a href="caman9815020s.html#603">Verzeichnis Mantegna
          zugeschriebene Werke</a>
        </li>
        <li>
          <a href="caman9815020s.html#617">Schriftquellen</a>
        </li>
        <li>
          <a href="caman9815020s.html#643">Dokumente</a>
        </li>
        <li>
          <a href="caman9815020s.html#718">Register</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
