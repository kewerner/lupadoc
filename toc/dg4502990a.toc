<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502990as.html#8">Roma ricercata nel suo
      sito</a>
      <ul>
        <li>
          <a href="dg4502990as.html#10">[Dedica al Cardinale
          Francesco Maria De Medici] Eminentissimo e Reverendissimo
          Prencipe [...]</a>
        </li>
        <li>
          <a href="dg4502990as.html#12">[Dedica] Lettore
          forestiero [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502990as.html#16">[Text]</a>
      <ul>
        <li>
          <a href="dg4502990as.html#16">Giornata Prima</a>
          <ul>
            <li>
              <a href="dg4502990as.html#16">Da ›Ponte
              Sant'Angelo‹ à ›San Pietro in Vaticano‹ per ›Borgo‹
              Vaticano</a>
              <ul>
                <li>
                  <a href="dg4502990as.html#16">[›Castel
                  Sant'Angelo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#23">[San Pietro in
                  Vaticano‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#25">[›San Pietro in
                  Vaticano: Baldacchino di San Pietro‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#26">[›San Pietro in
                  Vaticano: Cattedra di San Pietro‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#27">[›San Pietro in
                  Vaticano: Mosaico della Navicella‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#31">Giornata Seconda</a>
          <ul>
            <li>
              <a href="dg4502990as.html#31">Per il
              ›Trastevere‹</a>
              <ul>
                <li>
                  <a href="dg4502990as.html#31">Palazzo di Santo
                  Spirito ›Palazzo del Banco di Santo Spirito‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#36">›Ospizio
                  Apostolico di San Michele a Ripa Grande (ex)‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#37">›Dogana nuova di
                  Terra‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#38">›Santa Cecilia in
                  Trastevere‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#39">Giornata Terza</a>
          <ul>
            <li>
              <a href="dg4502990as.html#39">Da strada Giulia ›Via
              Giulia‹ al Palazzo di Farnese ›Palazzo Farnese‹ ▣, et
              all'Isola di San Bartolomeo ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4502990as.html#47">›San Bartolomeo
              all'Isola‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#48">Giornata Quarta</a>
          <ul>
            <li>
              <a href="dg4502990as.html#48">Da ›San Lorenzo in
              Damaso‹ ▣, ò Cancellaria al Monte Aventino</a>
            </li>
            <li>
              <a href="dg4502990as.html#53">›Porta San Paolo‹ ▣
              [›Piramide di Caio Cestio‹] ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#54">›Terme di
              Caracalla‹</a>
            </li>
            <li>
              <a href="dg4502990as.html#54">[›Anfiteatro
              Castrense‹] ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#55">Chiesa, e Convento di
              ›Sant'Alessio‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#57">Giornata V</a>
          <ul>
            <li>
              <a href="dg4502990as.html#57">Dalla Piazza di
              ›Monte Giordano‹, à quella di Pasquino ›Piazza di
              Pasquino‹ per li Monti ›Celio‹, e ›Palatino‹ [›Statua
              di Pasquino‹] ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#60">Palazzo de'Savelli
              ›Palazzo Orsini‹ ▣ [›Teatro di Marcello‹] ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#62">Portico detto Giano
              Quadrifronte ›Arco di Giano‹ ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#65">Palazzo Maggiore
              Imperiale ›Palatino‹ ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#68">Chiesa di ›Santo
              Stefano Rotondo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#72">Campo Vaccino ›Foro
              Romano‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#75">Giornata VI.</a>
          <ul>
            <li>
              <a href="dg4502990as.html#75">Da San Salvator del
              lauro à ›Campidoglio‹ ▣, e per le ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4502990as.html#77">›Piazza Navona‹ ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#81">Chiesa d'Aracoeli
              ›Santa Maria in Aracoeli‹ ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#82">›Tempio della
              Concordia‹ ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#85">Tempio di Pace ›Foro
              della Pace‹ ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#86">›Arco di Costantino‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#87">Il ›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#92">›Colonna Traiana‹
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#93">Giornata VII.</a>
          <ul>
            <li>
              <a href="dg4502990as.html#93">Dalla ›Piazza di
              Sant'Agostino‹ per il Monte ›Viminale‹, e
              ›Quirinale‹</a>
              <ul>
                <li>
                  <a href="dg4502990as.html#93">Chiesa di
                  ›Sant'Agostino‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#95">Chiesa di ›San
                  Luigi dei Francesi‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#96">Palazzo de'
                  Medici in Piazza Madama ›Palazzo Madama‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#97">Le Terme antiche
                  di Nerone ›Terme Neroniano-Alessandrine‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#98">Chiesa, e
                  Convento della Minerva ›Santa Maria sopra
                  Minerva‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#100">Parte del Tempio
                  del Sole ›Torre Mesa‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#101">›Porta Maggiore‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#105">Monte Cavallo
                  ›Quirinale‹ ▣ [›Palazzo del Quirinale‹] ▣
                  [›Fontana di Monte Cavallo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#107">La Rotonda
                  ›Pantheon‹</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#109">Giornata VIII</a>
          <ul>
            <li>
              <a href="dg4502990as.html#109">Da ›Piazza Nicosia‹
              à Monte Cavallo ›Quirinale‹, et alle Terme
              Diocletiane ›Terme di Diocleziano‹</a>
              <ul>
                <li>
                  <a href="dg4502990as.html#109">Monte Cavallo
                  ›Piazza del Quirinale‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#112">Dogana Nova à
                  Piazza di Pietra ›Dogana nuova di Terra‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#115">›Santa Maria
                  degli Angeli‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#118">Colonna Coclide
                  d'Antonino Imperatore ›Colonna di Antonino Pio‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#119">Palazzo della
                  Curia Innocenziana nel Monte Citorio ›Palazzo
                  Montecitorio‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#121">Giornata IX.</a>
          <ul>
            <li>
              <a href="dg4502990as.html#121">Da Piazza del
              Prencipe Borghese ›Piazza Borghese‹ alle Porte del
              Popolo ›Porta del Popolo‹, e ›Porta Pinciana‹</a>
            </li>
            <li>
              <a href="dg4502990as.html#121">›Palazzo Borghese‹
              ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#123">Le due Chiese del
              Cardinal Gastaldi ›Santa Maria in Montesanto‹ ▣
              ›Santa Maria dei Miracoli‹ ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#126">Porta Flaminia,
              hoggi detta del Popolo ›Porta del Popolo‹ ▣ [›Santa
              Maria del Popolo‹] ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#134">La Santissima
              ›Trinità dei Monti‹ ▣</a>
            </li>
            <li>
              <a href="dg4502990as.html#135">Giardino de' Medici
              ›Villa Medici‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#136">Giornata X.</a>
          <ul>
            <li>
              <a href="dg4502990as.html#136">Per le nove
              Chiese</a>
              <ul>
                <li>
                  <a href="dg4502990as.html#136">Cattedra antica
                  di San Pietro ›San Pietro in Vaticano: Cattedra
                  di San Pietro‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#137">Chiesa di ›San
                  Pietro in Vaticano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#140">A ›San Paolo
                  fuori le Mura‹</a>
                  <ul>
                    <li>
                      <a href="dg4502990as.html#142">Chiesa di
                      ›San Paolo fuori le Mura‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502990as.html#145">Alle Tre Fontane
                  ›Abbazia delle Tre Fontane‹</a>
                </li>
                <li>
                  <a href="dg4502990as.html#145">All'Annuntiata
                  ›Annunziatella‹</a>
                </li>
                <li>
                  <a href="dg4502990as.html#146">Chiesa di ›San
                  Sebastiano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502990as.html#147">Alla Basilica di
                  ›San Giovanni in Laterano‹</a>
                  <ul>
                    <li>
                      <a href="dg4502990as.html#153">Chiesa di
                      ›San Giovanni in Laterano‹ ▣</a>
                    </li>
                    <li>
                      <a href="dg4502990as.html#157">Scala Sancta
                      Santi Ioannis ›Scala Santa‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502990as.html#158">A ›Santa Croce
                  in Gerusalemme‹</a>
                  <ul>
                    <li>
                      <a href="dg4502990as.html#159">Chiesa di
                      ›Santa Croce in Gerusalemme‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502990as.html#159">A ›San Lorenzo
                  fuori le Mura‹</a>
                  <ul>
                    <li>
                      <a href="dg4502990as.html#160">Chiesa di
                      ›San Lorenzo fuori le Mura‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502990as.html#161">A ›Santa Maria
                  Maggiore‹</a>
                  <ul>
                    <li>
                      <a href="dg4502990as.html#162">Chiesa di
                      ›Santa Maria Maggiore‹ ▣</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#163">Notitia delle Porte,
          Monti, e Rioni della Città di Roma</a>
          <ul>
            <li>
              <a href="dg4502990as.html#163">Porte della
              Città</a>
            </li>
            <li>
              <a href="dg4502990as.html#164">In ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502990as.html#164">In ›Borgo‹</a>
            </li>
            <li>
              <a href="dg4502990as.html#165">Monti dentro la
              Città</a>
            </li>
            <li>
              <a href="dg4502990as.html#165">Rioni</a>
            </li>
            <li>
              <a href="dg4502990as.html#165">Piazze, nelle quali
              si vendono vettovaglie, dette anticamente Macelli</a>
            </li>
            <li>
              <a href="dg4502990as.html#166">Piazze, e Contrade,
              dove risiedono diverse arti, e si fanno Fiere, e
              Mercati</a>
            </li>
            <li>
              <a href="dg4502990as.html#167">Strade principali
              della Città</a>
            </li>
            <li>
              <a href="dg4502990as.html#169">Luoghi, dove al
              presente stanno le Porte dentro la Città di Roma</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4502990as.html#170">Cronologia de' Sommi
          Pontefici Romani</a>
        </li>
        <li>
          <a href="dg4502990as.html#199">Indice delle Chiese</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
