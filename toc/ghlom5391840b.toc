<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghlom5391840bs.html#6">Trattato dell'arte della
      pittura, scoltura, et architettura</a>
      <ul>
        <li>
          <a href="ghlom5391840bs.html#8">Al serenissimo
          principedon Carlo Emanuello, Gran Duca di Savoia</a>
        </li>
        <li>
          <a href="ghlom5391840bs.html#18">Tavola dei capitoli
          del primo libro</a>
        </li>
        <li>
          <a href="ghlom5391840bs.html#26">Tavola de le più
          eccellenti opere, di pittura, &amp; di scoltura</a>
        </li>
        <li>
          <a href="ghlom5391840bs.html#46">Proemio
          dell'opera</a>
        </li>
        <li>
          <a href="ghlom5391840bs.html#62">Libro primo de la
          proportione naturale, et artificiale de le cose</a>
        </li>
        <li>
          <a href="ghlom5391840bs.html#150">Libro secondo del
          sito, positione, decoro, moto, furia, &amp; gratia delle
          figure</a>
        </li>
        <li>
          <a href="ghlom5391840bs.html#232">Libro terzo del
          colore</a>
        </li>
        <li>
          <a href="ghlom5391840bs.html#256">Libro quarto de i
          lumi</a>
        </li>
        <li>
          <a href="ghlom5391840bs.html#290">Libro quinto della
          prospettiva</a>
        </li>
        <li>
          <a href="ghlom5391840bs.html#322">Libro sesto della
          prattica, della pittura</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghlom5391840bs.html#572">Libro settimo
      dell'historia di pittura</a>
    </li>
  </ul>
  <hr />
</body>
</html>
