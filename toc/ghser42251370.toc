<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghser42251370s.html#6">Regole generali di
      architetura sopra le cinque maniere de gli edifici, cioe,
      thoscano, dorico, ionico, corinthio, et composito con gli
      essempi dell’antiquita, che, per la magior parte concordano
      con la dottrina di Vitruvio</a>
      <ul>
        <li>
          <a href="ghser42251370s.html#7">[Dedica di Pietro
          Aretino a Francesco Marcolini] Messer Pietro Aretino a
          Francesco Marcolini</a>
        </li>
        <li>
          <a href="ghser42251370s.html#8">[Dedica dell'autore a
          Ercole II. Duca di Ferrara] Allo illustrissimo, et
          excellentissimo, Signore, ilsignore Herole II Duca IIII
          die Ferrara&#160;</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghser42251370s.html#12">[Tavole]</a>
    </li>
  </ul>
  <hr />
</body>
</html>
