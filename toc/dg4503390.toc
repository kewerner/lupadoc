<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4503390s.html#8">Roma Ampliata, e Rinovata</a>
      <ul>
        <li>
          <a href="dg4503390s.html#7">›San Pietro in Vaticano‹</a>
        </li>
        <li>
          <a href="dg4503390s.html#10">[Dedica al Cardinale
          Giorgio Spinola Sant'Agnese] Eminentissimo, e
          Reverendissimo Principe</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4503390s.html#12">[Text]</a>
      <ul>
        <li>
          <a href="dg4503390s.html#12">Giornata Prima</a>
          <ul>
            <li>
              <a href="dg4503390s.html#12">Da ›Ponte Sant'Angelo‹
              à ›San Pietro in Vaticano‹</a>
              <ul>
                <li>
                  <a href="dg4503390s.html#12">[›Castel
                  Sant'Angelo‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#21">Altar Maggiore
                  [›San Pietro in Vaticano: Baldacchino di San
                  Pietro‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#22">[›San Pietro in
                  Vaticano: Cattedra di San Pietro‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#27">Palazzo di Santo
                  Spirito ›Palazzo del Banco di Santo Spirito‹
                  ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503390s.html#29">Giornata Seconda</a>
          <ul>
            <li>
              <a href="dg4503390s.html#29">Dalla ›Porta Santo
              Spirito‹ al ›Trastevere‹</a>
              <ul>
                <li>
                  <a href="dg4503390s.html#35">›Ospizio Apostolico
                  di San Michele a Ripa Grande (ex)‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503390s.html#38">Giornata Terza</a>
          <ul>
            <li>
              <a href="dg4503390s.html#38">Da strada Giulia ›Via
              Giulia‹ all'Isola di San Bartolomeo ›Isola
              Tiberina‹</a>
              <ul>
                <li>
                  <a href="dg4503390s.html#40">›Palazzo Farnese‹
                  ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#41">In Palatio
                  Farnesiorum ›Statua del Toro Farnese‹</a>
                </li>
                <li>
                  <a href="dg4503390s.html#48">›San Bartolomeo
                  all'Isola‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503390s.html#49">Giornata Quarta</a>
          <ul>
            <li>
              <a href="dg4503390s.html#49">Da ›San Lorenzo in
              Damaso‹ al Monte ›Aventino‹</a>
              <ul>
                <li>
                  <a href="dg4503390s.html#50">Cancellaria
                  ›Palazzo della Cancelleria‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#56">›Porta San Paolo‹ ▣
                  [›Piramide di Caio Cestio‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#58">›San Paolo fuori le
                  Mura‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#59">›Abbazia delle Tre
                  Fontane‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#61">›Terme di
                  Caracalla‹</a>
                </li>
                <li>
                  <a href="dg4503390s.html#61">[›Anfiteatro
                  Castrense‹] ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503390s.html#63">Giornata Quinta</a>
          <ul>
            <li>
              <a href="dg4503390s.html#63">Dalla Piazza di ›Monte
              Giordano‹ per i Monti ›Celio‹, e ›Palatino‹ [›Statua
              di Pasquino‹] ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#67">[Teatro di Marcello]
              ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#69">Portico detto Giano
              Quadrifronte ›Arco di Giano‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#71">›San Sebastiano fuori
              le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#75">›San Giovanni in
              Laterano‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#81">Palazzo Maggiore
              ›Palatino‹</a>
            </li>
            <li>
              <a href="dg4503390s.html#82">›Tempio della
              Concordia‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503390s.html#84">Giornata Sesta</a>
          <ul>
            <li>
              <a href="dg4503390s.html#84">Da ›San Salvatore in
              Lauro‹ a ›Campidoglio‹, e per le ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4503390s.html#86">›Piazza Navona‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#90">›Campidoglio‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#94">Campo Vaccino ›Foro
              Romano‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#97">Tempio di Pace ›Foro
              della Pace‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#99">›Arco di Costantino‹
              ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#100">Il ›Colosseo‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#104">Columna Traiani
              ›Colonna Traiana‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503390s.html#107">Giornata Settima</a>
          <ul>
            <li>
              <a href="dg4503390s.html#107">Dalla ›Piazza di
              Sant'Agostino‹ per i Monti ›Viminale‹, e
              ›Quirinale‹</a>
            </li>
            <li>
              <a href="dg4503390s.html#107">Chiesa di
              ›Sant'Agostino‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#109">Chiesa di ›San Luigi
              dei Francesi‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#110">Palazzo de' Medici in
              Piazza Madama ›Palazzo Madama‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#113">Collegium Societatis
              Iesu ›Collegio Romano‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#118">›Santa Croce in
              Gerusalemme‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#119">›San Lorenzo fuori le
              Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#123">›Santa Maria Maggiore‹
              ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#131">›Sant'Ignazio‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#133">La Rotonda ›Pantheon‹
              ▣ ›Obelisco di Ramsses II (1)‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503390s.html#135">Giornata Ottava</a>
          <ul>
            <li>
              <a href="dg4503390s.html#135">Dalla strada dell'Orso
              ›Via dell'Orso‹ a Monte Cavallo ›Quirinale‹, e alle
              ›Terme di Diocleziano‹</a>
            </li>
            <li>
              <a href="dg4503390s.html#137">Dogana Nova a Piazza
              di Pietra ›Dogana nuova di Terra‹ ▣</a>
            </li>
            <li>
              <a href="dg4503390s.html#138">Monte Cavallo ›Piazza
              del Quirinale‹ ▣ ›Palazzo del Quirinale‹ ▣ ›Fontana
              di Monte Cavallo‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503390s.html#146">Giornata Nona</a>
          <ul>
            <li>
              <a href="dg4503390s.html#146">Dal ›Palazzo Borghese‹
              ▣ a ›Porta del Popolo‹, e a ›Piazza di Spagna‹</a>
            </li>
            <li>
              <a href="dg4503390s.html#148">›Porto di Ripetta‹
              ▣</a>
              <ul>
                <li>
                  <a href="dg4503390s.html#148">[›San Girolamo
                  degli Illirici‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503390s.html#150">›Piazza del Popolo‹
              ▣</a>
              <ul>
                <li>
                  <a href="dg4503390s.html#150">[›Santa Maria dei
                  Miracoli‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#150">[›Santa Maria in
                  Montesanto‹] ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#150">[›Fontana di
                  Piazza del Popolo / Obelisco Flaminio‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4503390s.html#162">La Barcaccia in Piazza
              di Spagna ›Fontana della Barcaccia‹ ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503390s.html#163">Giornata Decima</a>
          <ul>
            <li>
              <a href="dg4503390s.html#163">Dal Monte Citorio
              ›Piazza di Montecitorio‹ alla ›Porta Pia‹. e al Monte
              ›Pincio‹</a>
              <ul>
                <li>
                  <a href="dg4503390s.html#163">›Palazzo
                  Montecitorio ‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#165">›Colonna di
                  Antonino Pio‹ ▣</a>
                </li>
                <li>
                  <a href="dg4503390s.html#167">›Palazzo
                  Barberini‹ ▣</a>
                  <ul>
                    <li>
                      <a href="dg4503390s.html#167">›Fontana del
                      Tritone‹ ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4503390s.html#177">›Trinità dei
                  Monti‹ ▣</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4503390s.html#179">Cronologia di tutti i
          Sommi Pontefici</a>
        </li>
        <li>
          <a href="dg4503390s.html#189">Indice delle cose più
          notabili</a>
        </li>
        <li>
          <a href="dg4503390s.html#201">Prospetto della Basilica
          Lateranense ›San Giovanni in Laterano‹ ▣</a>
        </li>
        <li>
          <a href="dg4503390s.html#202">Facciata di ›San Giovanni
          in Laterano‹</a>
        </li>
        <li>
          <a href="dg4503390s.html#205">›San Giovanni in Laterano:
          Cappella Corsini‹</a>
        </li>
        <li>
          <a href="dg4503390s.html#207">Della nuova Penitenziaria
          ›Oratorio di San Niccolò in Laterano‹, e del Portico
          Laterale dall'altra parte della Basilica</a>
        </li>
        <li>
          <a href="dg4503390s.html#207">Facciata di ›San Giovanni
          dei Fiorentini‹</a>
        </li>
        <li>
          <a href="dg4503390s.html#208">Nuovo Braccio al Palazzo
          di Monte Cavallo ›Palazzo del Quirinale‹ oer commodo
          della Famiglia Pontificia</a>
        </li>
        <li>
          <a href="dg4503390s.html#209">Stalle Pontificie di Monte
          Cavallo ›Scuderie del Quirinale‹</a>
        </li>
        <li>
          <a href="dg4503390s.html#211">Prospetto della Consulta
          ›Palazzo della Consulta‹ ▣</a>
        </li>
        <li>
          <a href="dg4503390s.html#212">›Palazzo della
          Consulta‹</a>
        </li>
        <li>
          <a href="dg4503390s.html#213">›Santi Celso e
          Giuliano‹</a>
        </li>
        <li>
          <a href="dg4503390s.html#214">Braccio nuovo della
          Libraria Varticana ›Biblioteca Apostolica Vaticana‹</a>
        </li>
        <li>
          <a href="dg4503390s.html#215">›Fontana di Trevi‹</a>
        </li>
        <li>
          <a href="dg4503390s.html#216">›Arco di Costantino‹
          Ristorato</a>
        </li>
        <li>
          <a href="dg4503390s.html#217">Carceri per le Donne
          ›Ospizio Apostolico di San Michele a Ripa Grande (ex)‹,
          nuova strada di Monte Citorio, recinto per li Legnami</a>
        </li>
        <li>
          <a href="dg4503390s.html#217">Il ›Campidoglio‹
          illustrato</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
