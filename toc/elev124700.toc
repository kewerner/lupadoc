<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="elev124700s.html#8">Appunti artistici sopra
      Levanto</a>
      <ul>
        <li>
          <a href="elev124700s.html#14">Carissimo amico</a>
        </li>
        <li>
          <a href="elev124700s.html#34">Note</a>
        </li>
        <li>
          <a href="elev124700s.html#68">Documenti</a>
        </li>
        <li>
          <a href="elev124700s.html#162">Di alcuni pittori</a>
        </li>
        <li>
          <a href="elev124700s.html#164">Due documenti
          riguardando Teramo di Daniele</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
