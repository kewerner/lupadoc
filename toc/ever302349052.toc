<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ever302349052s.html#6">Notizie Storiche Delle
      Chiese Di Verona; libro quinto, parte seconda</a>
      <ul>
        <li>
          <a href="ever302349052s.html#8">Indice di alcune cose
          più notabili contenute in questo Quinto Libro</a>
        </li>
        <li>
          <a href="ever302349052s.html#265">Appendice</a>
        </li>
        <li>
          <a href="ever302349052s.html#297">Indice de'Monasteri
          compresi in questa Seconda Parte</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
