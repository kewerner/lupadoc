<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="be40184050s.html#6">Raccolta di tutte le vedute che
      eistevano nel gabinetto del Duca della Torre rappresentanti
      l’eruzione del Monte Vesuvio fin oggi accadute&#160;</a>
      <ul>
        <li>
          <a href="be40184050s.html#8">L'editore</a>
        </li>
        <li>
          <a href="be40184050s.html#10">[Raccolta di tutte le
          vedute che eistevano nel gabinetto del Duca della Torre
          rappresentanti l’eruzione del Monte Vesuvio fin oggi
          accadute]</a>
        </li>
        <li>
          <a href="be40184050s.html#30">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
