<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dr4703700s.html#8">Emo et Rmo Principi Dominico
      S.R.E. Diacono Cardinali Ursini de Gravina Tituli Sanctae
      Mariae ad Martyres...</a>
      <ul>
        <li>
          <a href="dr4703700s.html#10">[Statue]</a>
          <ul>
            <li>
              <a href="dr4703700s.html#10">Sanctus Petrus [›Ponte
              Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#12">Sanctus Paulus [›Ponte
              Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#14">In flagella paratus sum
              [›Ponte Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#16">Tronus meus in columna
              [›Ponte Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#18">In aerumna mea dum
              configitur spina [›Ponte Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#20">Respice in faciem
              Christi tui [›Ponte Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#22">Super vestem meam
              miserunt sortem [›Ponte Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#24">Aspiciant ad me quem
              confixerunt [›Ponte Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#26">Regnavit a ligno deus
              [›Ponte Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#28">Cuius principatus super
              humerum eius [›Ponte Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#30">Potaverunt me aceto
              [›Ponte Sant'Angelo‹] ▣</a>
            </li>
            <li>
              <a href="dr4703700s.html#32">Vulnerasti cor meum
              [›Ponte Sant'Angelo‹] ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
