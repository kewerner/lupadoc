<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cacar66103930s.html#6">Diario degli anni MDCCXX, e
      MDCCXXI</a>
      <ul>
        <li>
          <a href="cacar66103930s.html#8">Introduzione</a>
        </li>
        <li>
          <a href="cacar66103930s.html#14">Aprile</a>
        </li>
        <li>
          <a href="cacar66103930s.html#17">Maggio</a>
        </li>
        <li>
          <a href="cacar66103930s.html#17">Giugno</a>
        </li>
        <li>
          <a href="cacar66103930s.html#30">Luglio</a>
        </li>
        <li>
          <a href="cacar66103930s.html#30">Agosto</a>
        </li>
        <li>
          <a href="cacar66103930s.html#42">Settembre</a>
        </li>
        <li>
          <a href="cacar66103930s.html#49">Ottobre</a>
        </li>
        <li>
          <a href="cacar66103930s.html#55">Novembre</a>
        </li>
        <li>
          <a href="cacar66103930s.html#64">Dicembre</a>
        </li>
        <li>
          <a href="cacar66103930s.html#76">Gennaio</a>
        </li>
        <li>
          <a href="cacar66103930s.html#84">Febbraio</a>
        </li>
        <li>
          <a href="cacar66103930s.html#93">Marzo</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
