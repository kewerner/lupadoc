<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dp430516022s.html#6">Die römischen Mosaiken und
      Malereien der kirchlichen Bauten vom IV. bis XIII.
      Jahrhundert</a>
      <ul>
        <li>
          <a href="dp430516022s.html#8">III. Untersuchungen über
          einzelne Darstellungen</a>
          <ul>
            <li>
              <a href="dp430516022s.html#10">I. Darstellungen
              aus dem Leben Jesu und Mariä</a>
            </li>
            <li>
              <a href="dp430516022s.html#187">II. Darstellungen
              Christi, Mariä, der Engel, Johannes' des Täufers, der
              Apostel und der Evangelisten&#160;</a>
            </li>
            <li>
              <a href="dp430516022s.html#203">III. Darstellungen
              von Märtyrern in den Katakombenkirchen</a>
            </li>
            <li>
              <a href="dp430516022s.html#218">IV. Darstellungen
              von Märtyrern und Bekennern in den oberirdischen
              Kirchen</a>
            </li>
            <li>
              <a href="dp430516022s.html#287">V. Das Gericht</a>
            </li>
            <li>
              <a href="dp430516022s.html#331">VI. Aufenthaltsort
              der Seligen</a>
            </li>
            <li>
              <a href="dp430516022s.html#350">VII. Darstellungen
              der Seligen</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dp430516022s.html#354">IV. Tafelgemälde</a>
          <ul>
            <li>
              <a href="dp430516022s.html#356">I.
              Vorbemerkungen</a>
            </li>
            <li>
              <a href="dp430516022s.html#365">II. Darstellungen
              Christi</a>
            </li>
            <li>
              <a href="dp430516022s.html#397">III. Darstellungen
              Mariä</a>
            </li>
            <li>
              <a href="dp430516022s.html#420">IV. Darstellungen
              des hl. Franz von Assisi</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dp430516022s.html#426">V.
          Schlußbetrachtungen</a>
          <ul>
            <li>
              <a href="dp430516022s.html#428">I. Zweck der
              religiösen Malerein nach römischer Auffassung</a>
            </li>
            <li>
              <a href="dp430516022s.html#434">II. Rückblick</a>
            </li>
            <li>
              <a href="dp430516022s.html#476">Namen- und
              Sachregister</a>
            </li>
            <li>
              <a href="dp430516022s.html#492">Topographisches
              Verzeichnis der Tafel und der Textbilder</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
