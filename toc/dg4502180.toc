<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4502180s.html#8">Mirabilia Urbis Romae</a>
      <ul>
        <li>
          <a href="dg4502180s.html#9">Ad Lectorem</a>
        </li>
        <li>
          <a href="dg4502180s.html#10">[Text]</a>
          <ul>
            <li>
              <a href="dg4502180s.html#10">Septem Praecipuae Urbis
              Eccleasiae</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#10">Prima Ecclesia est
                  Sanctus Ioannis Lateranen ›San Giovanni in
                  Laterano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502180s.html#17">Ecclesia Sancti
                  Petri in Vaticano ›San Pietro in Vaticano‹</a>
                  <ul>
                    <li>
                      <a href="dg4502180s.html#15">[›San Pietro in
                      Vaticano‹] ▣</a>
                    </li>
                  </ul>
                </li>
                <li>
                  <a href="dg4502180s.html#26">Terta Ecclesia
                  Sancti Pauli ›San paolo Fuori le Mura‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502180s.html#28">Quarta Ecclesia
                  Sancta Maria Maior ›Santa Maria Maggiore‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502180s.html#31">Quinta Ecclesia
                  Sancti Laurentij extra muros ›San Lorenzo fuori
                  le Mura‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502180s.html#33">Sexta Ecclesia
                  Sanct Sebastiani ›San Sebastiano‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502180s.html#35">Septima Ecclesia
                  Sancti Crucis in Hierusalem ›Santa Croce in
                  Gerusalemme‹ ▣</a>
                </li>
                <li>
                  <a href="dg4502180s.html#36">Unbenannt</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#36">Ecclesia in Insula
              Tyberina ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#37">In Regione
              Transtyberina ›Trastevere‹</a>
            </li>
            <li>In Suburbio, seu Burgo vulgo</li>
            <li>
              <a href="dg4502180s.html#47">A Porta Flaminia, seu
              Populi ›Porta del Popolo‹ ad radices usque Capitolij
              ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#66">A Capitolio ad laevam
              versus Montes</a>
            </li>
            <li>
              <a href="dg4502180s.html#74">A Capitolio ad dexteram
              Montis versus</a>
            </li>
            <li>
              <a href="dg4502180s.html#80">Indulgentiae, Festa et
              Stationes Ecclesiarum Urbis per totum Annum</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#84">Stationes
                  Adventus</a>
                </li>
                <li>
                  <a href="dg4502180s.html#85">Stationes in
                  Quadrigesima</a>
                </li>
                <li>
                  <a href="dg4502180s.html#85">Unbenannt</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#87">Dux Romanus, nimirum
              qui posset ducere advenas per Urbem ad visenda
              antiqua Urbis monumenta</a>
            </li>
            <li>
              <a href="dg4502180s.html#88">De monte Teastaceo
              ›Tesaccio (monte)‹ alijsque rebus</a>
            </li>
            <li>
              <a href="dg4502180s.html#89">De Thermis Antonianiis
              alijsque ›Terme di Caracalla‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#89">De Sancto Ioanne in
              Laterano ›San Giovanni in Laterano‹, Sancta Cruce
              ›Santa Croce in Gerusalemme‹, aliysque locis</a>
            </li>
            <li>Secunda dies
              <ul>
                <li>De Equis marmoreis in monte Quirinali à quibus
                hic mons dicitur Caballus ›Fontana di Monte
                Cavallo‹, et Thermis Diocletiani ›Terme di
                Diocleziano‹</li>
                <li>
                  <a href="dg4502180s.html#92">De Septem Salis
                  ›Sette Sale‹, Coliseo ›Colosseo‹, et alijs</a>
                </li>
                <li>
                  <a href="dg4502180s.html#93">De Templo Pacis
                  ›Foro della Pace‹, Monte ›Palatino‹</a>
                </li>
                <li>
                  <a href="dg4502180s.html#94">De
                  ›Campidoglio‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#95">Dies tertius</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#95">›Pantheon‹, vulgo
                  Rotunda</a>
                </li>
                <li>
                  <a href="dg4502180s.html#96">De Thermis Agrippa,
                  et Neronis ›Terme di Agrippa‹ ›Terme
                  Neroniano-Alessandrine‹</a>
                </li>
                <li>
                  <a href="dg4502180s.html#96">De Agone, vulgo
                  Navona ›Piazza Navona‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#98">Catalogus Romanorum
              Pontificum</a>
            </li>
            <li>
              <a href="dg4502180s.html#106">Imperatores Romani</a>
            </li>
            <li>
              <a href="dg4502180s.html#111">Reges Francorum</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4502180s.html#112">Antiquitates Almae Urbis
      Romae</a>
      <ul>
        <li>Ad Lectores</li>
        <li>
          <a href="dg4502180s.html#115">[Text]</a>
          <ul>
            <li>
              <a href="dg4502180s.html#115">De aedificatione Almae
              Urbis</a>
            </li>
            <li>
              <a href="dg4502180s.html#117">De ambitus, vel
              circuitu Urbis Romae</a>
            </li>
            <li>
              <a href="dg4502180s.html#117">Portae Urbis</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#118">›Porta Maggiore‹
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#119">Viae Romanae</a>
            </li>
            <li>Pontes</li>
            <li>
              <a href="dg4502180s.html#121">Insula Tiberina ›Isola
              Tiberina‹ ▣</a>
            </li>
            <li>Montes
              <ul>
                <li>[›Palatino‹]</li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#123">Mons Testaceus
              ›Testaccio (monte)‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#123">Aquae variae, et qui
              eas Romam conduxit</a>
            </li>
            <li>
              <a href="dg4502180s.html#124">Aquaeductus</a>
            </li>
            <li>
              <a href="dg4502180s.html#125">De Cloaca omnium
              maxima ›Cloaca Maxima‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#125">Septem aulae, vulgò
              Septem Salae ›Sette Sale‹</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#125">Termae Alessandri
                  Severi ›Terme Neroniano-Alessandrine‹ ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#126">Thermae sive Balnea,
              et quis aedificarunt</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#126">[›Terme di
                  Diocleziano‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#127">Naumachia ubi Navales
              pugna edebantur</a>
            </li>
            <li>
              <a href="dg4502180s.html#127">Circus</a>
            </li>
            <li>
              <a href="dg4502180s.html#128">Theatra qua erant
              eiusque aedificationes</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#128">[›Teatro di
                  Marcello‹]</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#129">Amphitheatra eiusque
              aedificatores</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#129">[›Colosseo‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#130">Fora Romana</a>
            </li>
            <li>
              <a href="dg4502180s.html#131">Arcus Triumphalis</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#131">[›Arco di
                  Costantino‹] ▣</a>
                </li>
                <li>
                  <a href="dg4502180s.html#132">[›Arco di Giano‹]
                  ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#132">Porticus</a>
            </li>
            <li>
              <a href="dg4502180s.html#133">Trophea et
              Columnae</a>
            </li>
            <li>
              <a href="dg4502180s.html#134">Colossi</a>
            </li>
            <li>
              <a href="dg4502180s.html#135">Pyramides</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#135">[›Piramide di Caio
                  Cestio‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#135">Meta</a>
            </li>
            <li>
              <a href="dg4502180s.html#136">Obelisci</a>
            </li>
            <li>
              <a href="dg4502180s.html#136">Statuae</a>
            </li>
            <li>
              <a href="dg4502180s.html#136">Marforius ›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#137">Equi</a>
            </li>
            <li>
              <a href="dg4502180s.html#137">Bibliothecae</a>
            </li>
            <li>
              <a href="dg4502180s.html#137">Horologia</a>
            </li>
            <li>
              <a href="dg4502180s.html#137">Palatia</a>
            </li>
            <li>
              <a href="dg4502180s.html#138">Domus Aurea
              Neronis</a>
            </li>
            <li>
              <a href="dg4502180s.html#139">Domus</a>
            </li>
            <li>
              <a href="dg4502180s.html#139">Curia et quales
              erant</a>
            </li>
            <li>
              <a href="dg4502180s.html#140">Senatula et quae</a>
            </li>
            <li>
              <a href="dg4502180s.html#140">Magistratus</a>
            </li>
            <li>
              <a href="dg4502180s.html#141">Comitia et quae</a>
            </li>
            <li>
              <a href="dg4502180s.html#142">Tribus</a>
            </li>
            <li>
              <a href="dg4502180s.html#142">Regiones Urbis</a>
            </li>
            <li>
              <a href="dg4502180s.html#142">Basilicae</a>
            </li>
            <li>
              <a href="dg4502180s.html#142">›Capitolium‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#143">[›Piazza del
              Campidoglio‹] ▣</a>
            </li>
            <li>
              <a href="dg4502180s.html#144">›Aerarium‹ et quo
              pecunia genere Roma utebantur</a>
            </li>
            <li>
              <a href="dg4502180s.html#145">›Grecostasi‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#145">Secretarium Populi
              Romani ›Secretarium Senatus‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#145">Asylum ›Asilo di
              Romolo‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#145">Rostra ›Rostri‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#145">Columna Miliaria
              ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#146">Templum Carmentae
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#146">›Columna bellica‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#146">›Columna Lactaria‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#146">Aequimelium
              ›Equimelio‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#146">Campus Martius ›Campo
              Marzio‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#147">›Tigillum
              Sororium‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#147">Campi Peregrini
              ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#147">›Taberna
              Meritoria‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#147">›Vivarium‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#147">Horti</a>
            </li>
            <li>
              <a href="dg4502180s.html#148">›Velabro‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#148">›Carinae‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#149">Clivi</a>
            </li>
            <li>
              <a href="dg4502180s.html#149">Prata</a>
            </li>
            <li>
              <a href="dg4502180s.html#149">Horrea publica</a>
            </li>
            <li>
              <a href="dg4502180s.html#149">Carceres publici</a>
            </li>
            <li>
              <a href="dg4502180s.html#150">Festa et ludi
              publici</a>
            </li>
            <li>
              <a href="dg4502180s.html#150">Mausoleum Augusti,
              Adriani, et Septimij</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#151">[›Castel
                  Sant'Angelo‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#151">Templa</a>
              <ul>
                <li>
                  <a href="dg4502180s.html#152">[›Pantheon‹] ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4502180s.html#153">Sacerdotes, Virgines,
              Vestales, Vestimenta, Vasa, aliaque instrumenta in
              usum sacrificorum eorumque institutores</a>
            </li>
            <li>
              <a href="dg4502180s.html#155">Armamentarium
              ›Armamentaria‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#156">Exercitus Romanus
              terra Marique eorumque insignia</a>
            </li>
            <li>
              <a href="dg4502180s.html#156">Triumphi, et quibus
              concedebantur, et quis primus triumphavit, et quot
              erant triumphorum genera</a>
            </li>
            <li>
              <a href="dg4502180s.html#157">Coronae, et quibus
              dabantur</a>
            </li>
            <li>
              <a href="dg4502180s.html#157">Numerus Populi
              Romani</a>
            </li>
            <li>
              <a href="dg4502180s.html#157">Divitiae Populi
              Romani</a>
            </li>
            <li>
              <a href="dg4502180s.html#158">Liberalitas antiquorum
              Romanorum</a>
            </li>
            <li>
              <a href="dg4502180s.html#158">Matrimonia Romanorum
              et quomodo fiebant</a>
            </li>
            <li>
              <a href="dg4502180s.html#158">Bonis moribus filios
              Romani instituebant</a>
            </li>
            <li>
              <a href="dg4502180s.html#159">Matrimoniorum solutio
              ac divortia</a>
            </li>
            <li>
              <a href="dg4502180s.html#159">Exequiae earumque
              ritus</a>
            </li>
            <li>
              <a href="dg4502180s.html#160">Turres</a>
            </li>
            <li>
              <a href="dg4502180s.html#160">Tiberis</a>
            </li>
            <li>
              <a href="dg4502180s.html#161">Palatium Pontificium
              ›Palazzo Apostolico Vaticano‹ et vulgo Pulchrum visu,
              Belvedere ›Cortile del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#164">Transtyberina Regio
              ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4502180s.html#164">Repetitio, seu
              Recapitulatio quaedam Antiquitatis</a>
            </li>
            <li>
              <a href="dg4502180s.html#165">Templa extra Urbem</a>
            </li>
            <li>
              <a href="dg4502180s.html#166">Roma quoties
              direpta</a>
            </li>
            <li>
              <a href="dg4502180s.html#166">De ignibus Antiquorum
              de quibus meminerunt pauci Auctores esceptis
              quibusdam fragmentis et historijs</a>
            </li>
            <li>
              <a href="dg4502180s.html#169">Theatrum Pompei
              ›Teatro di Pompeo‹ ▣</a>
            </li>
            <li>
              <a href="dg4502180s.html#173">[›San Pietro in
              Vaticano‹] ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
