<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="enap163880as.html#6">Succinte notizie intorno
      alla facciata della Chiesa Cattedrale napoletana nelle quali
      si da’ contezza dell’antica speciosa sua porta e del
      ripulimento si’ dell’una, che dell’altra fattosi in
      quest’anno 1788</a>
      <ul>
        <li>
          <a href="enap163880as.html#8">Prospetto</a>
        </li>
        <li>
          <a href="enap163880as.html#10">Eminentissimo,
          reverendissimo Signore Pietro D'Onofri&#160;</a>
        </li>
        <li>
          <a href="enap163880as.html#14">Istoria delle erezione
          della Chiesa Cattedrale di Napoli</a>
        </li>
        <li>
          <a href="enap163880as.html#23">Descrizione della Porta
          grande</a>
        </li>
        <li>
          <a href="enap163880as.html#35">Descrizione del nuovo
          abbellimento</a>
        </li>
        <li>
          <a href="enap163880as.html#44">Conchiusione</a>
        </li>
        <li>
          <a href="enap163880as.html#46">Annotazioni
          ragionate</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
