<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501620s.html#8">Le Antichità Della Città Di
      Roma</a>
      <ul>
        <li>
          <a href="dg4501620s.html#9">Tavola dell'ordine e de'
          Capitoli dell'Antichità di Roma</a>
        </li>
        <li>
          <a href="dg4501620s.html#10">All'illustrissimo et
          honoratissimo Signore, il Signor Giulio Martinengo dalla
          Pallada</a>
        </li>
        <li>
          <a href="dg4501620s.html#18">Tavola per alfabeto de'
          luoghi di queste antichità di Roma</a>
        </li>
        <li>
          <a href="dg4501620s.html#25">Tavola de' luoghi dove le
          Statue sono, secondo l'ordine del Libro</a>
        </li>
        <li>
          <a href="dg4501620s.html#29">Tavola de' nomi delle
          Statue, che si dechiarano chi fussero, per Alphabeto</a>
        </li>
        <li>
          <a href="dg4501620s.html#32">Le Antichità de la città di
          Roma brevissimamente raccolte per Lutio Mauro&#160;</a>
          <ul>
            <li>
              <a href="dg4501620s.html#36">I. Del colle Capitolino
              con le cose, che vi furono, ò che hora vi sono</a>
            </li>
            <li>
              <a href="dg4501620s.html#43">II. Del Colle Palatino
              con tutte le cose sue antiche</a>
            </li>
            <li>
              <a href="dg4501620s.html#49">III. Del Foro Romano,
              del Comitio, de l'Arco di Costantino, Del Coliseo, e
              della casa di Nerone</a>
            </li>
            <li>
              <a href="dg4501620s.html#64">IIII. [sic!] Di quattro
              altri Fori, di Cesare, di Augusto, di Nerva, e di
              Traiano</a>
            </li>
            <li>
              <a href="dg4501620s.html#67">V. De la Valle, che è
              tra il campidoglio e'l Palatino; del FOro Olitorio, e
              del Buario, et del circo Massimo</a>
            </li>
            <li>
              <a href="dg4501620s.html#78">VI. Del Settizonio di
              Severo, de la strada Appia, et porta Capena, con ciò
              che vi era</a>
            </li>
            <li>
              <a href="dg4501620s.html#83">VII. Del piano di
              Testaccio con ciò vi era</a>
            </li>
            <li>
              <a href="dg4501620s.html#86">VIII. Del Colle
              Aventino con tutti i suoi luoghi antichi, e
              moderni</a>
            </li>
            <li>
              <a href="dg4501620s.html#89">IX. Del Celiolo, e del
              Celio co' luoghi loro antichi, e moderni&#160;</a>
            </li>
            <li>
              <a href="dg4501620s.html#98">X. Del colle
              dell'Esquilie, co' luoghi, che vi furono, ò vi
              sono</a>
            </li>
            <li>
              <a href="dg4501620s.html#107">XI. Del Colle
              Viminale, con tutti i luoghi suoi</a>
            </li>
            <li>
              <a href="dg4501620s.html#110">XII. Del Colle
              Quirinale, e del monte de gli Hortoli co' luoghi
              loro</a>
            </li>
            <li>
              <a href="dg4501620s.html#120">XIII. De' luoghi della
              città piana, e fra gli altri del Circo Flaminio, e
              del Theatro di Pompeio</a>
            </li>
            <li>
              <a href="dg4501620s.html#124">XIIII. [sic!] De'
              luoghi del Campo Martio, e del Pantheone, del Circo
              chiamato in Agone, e della palude Caprea</a>
            </li>
            <li>
              <a href="dg4501620s.html#133">XV. Di Trastevere, e
              de' luoghi suoi; e dell'isoola co' ponti, che sono
              su'l Tevere da questa parte</a>
            </li>
            <li>
              <a href="dg4501620s.html#140">XVI. Di tutti i luoghi
              di Vaticano, che chiamano hoggi in Borgo</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501620s.html#146">Delle statue antiche, che
          per tutta Roma, on diversi luoghi, e case si veggono</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
