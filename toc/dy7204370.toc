<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dy7204370s.html#8">La ›Villa Pia‹ des jardins du
      Vatican, architecture de Pirro Ligorio</a>
    </li>
    <li>
      <a href="dy7204370s.html#10">[Text]</a>
      <ul>
        <li>
          <a href="dy7204370s.html#10">Notice historique au Pirro
          Ligorio&#160;</a>
          <ul>
            <li>
              <a href="dy7204370s.html#10">Frise de la façade
              latérale des petits portiques ▣&#160;</a>
            </li>
            <li>
              <a href="dy7204370s.html#21">Ajustement en stuc dans
              la petite galerie au premier étage ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#22">Notes</a>
            </li>
            <li>
              <a href="dy7204370s.html#24">Description de la Villa
              Pia</a>
              <ul>
                <li>
                  <a href="dy7204370s.html#41">Bas-relief en stuc
                  dans les niches du nymphée ▣</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dy7204370s.html#42">Table des planches et
              vignettes contenues dans cet ouvrage&#160;</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dy7204370s.html#44">[Tavole]</a>
      <ul>
        <li>
          <a href="dy7204370s.html#44">Frontespice ▣</a>
        </li>
        <li>
          <a href="dy7204370s.html#46">I. Vue générale ▣
          &#160;</a>
        </li>
        <li>
          <a href="dy7204370s.html#48">II. Plan générale ▣</a>
        </li>
        <li>
          <a href="dy7204370s.html#50">III. élévation générale
          ▣&#160;</a>
        </li>
        <li>
          <a href="dy7204370s.html#52">IV. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#52">Plan du premier étage
              ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#52">Coupe transversale
              ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#52">Plan des combles au
              nivaeu du Belvédére ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#52">Coupe longitudinale
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#54">V. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#54">Façade latérale ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#54">Coupe sur la ligne F.G.
              ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#54">Coupe sur la ligne D.E.
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#56">VI. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#56">Façade principale du
              Casin ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#56">Détails des Bancs
              autour de la cour ovale ▣&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#58">VII. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#58">Entrecolonnement du
              milieu de l'ordre à rez-de-chauseée et détails d'une
              partie des stucs et mosaiques qui décorent le
              vestibule ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#58">Détails de l'ordre è
              rez-de-chausée du casin principal ▣&#160;</a>
            </li>
            <li>
              <a href="dy7204370s.html#58">Profil des marches
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#60">VIII. Détails et profils
          des différent ordres ▣&#160;</a>
        </li>
        <li>
          <a href="dy7204370s.html#62">IX. Vue intérieure du
          vestibule ▣</a>
        </li>
        <li>
          <a href="dy7204370s.html#64">X Détails du Vestibule
          ▣</a>
        </li>
        <li>
          <a href="dy7204370s.html#66">XI. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#66">Voûte de la salle
              marquée a sur le plan ▣&#160;</a>
            </li>
            <li>
              <a href="dy7204370s.html#66">Détail de la corniche
              [...] ▣&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#68">XII. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#68">Voûte de la salle
              marquée e sur le plan ▣&#160;</a>
            </li>
            <li>
              <a href="dy7204370s.html#68">Détail de la corniche
              [...] ▣&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#70">XIII. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#70">Voûte de la salle
              marquée C sur le plan genéral ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#70">Détail du bandeau sous
              la voûte ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#72">XIV. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#72">Voûte de l'escalier
              ▣&#160;</a>
            </li>
            <li>
              <a href="dy7204370s.html#72">Détail de la corniche
              ▣&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#74">XV. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#74">Voûte de la petite
              galerie au premier étage ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#74">Détail de la corniche
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#76">XVI. ▣&#160;</a>
          <ul>
            <li>
              <a href="dy7204370s.html#76">Fontaine en marbre
              blanc sous le vestibule ▣&#160;</a>
            </li>
            <li>
              <a href="dy7204370s.html#76">Fontaine en marbre
              blanc sous la loge ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#78">XVII. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#78">Fontaine en marbre
              violet au milieu de la cour ovale ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#78">Plan ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#80">XVIII. Vue de l'entrée de
          la cour ovale ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#82">XIX. Voûte des petits
              portiques ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#82">Détails des mosaiques
              ▣&#160;</a>
            </li>
            <li>
              <a href="dy7204370s.html#82">Voûte des petits
              portiques ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#82">Details des mosaiques
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#84">XX. Vue de la loge prise
          sous le vestibule du casin ▣</a>
        </li>
        <li>
          <a href="dy7204370s.html#86">XXI. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#86">Èlevation de la loge du
              côte de la cour ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#86">Façade latérale ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#88">XXII. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#88">Coupe sur la longueur
              de la loge ▣&#160;</a>
            </li>
            <li>
              <a href="dy7204370s.html#88">Coupe sur la largeur
              ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dy7204370s.html#90">XXIII. ▣</a>
          <ul>
            <li>
              <a href="dy7204370s.html#90">Détails de la loge
              ▣</a>
            </li>
            <li>
              <a href="dy7204370s.html#90">Voûte de l'hémicycle
              ▣</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
