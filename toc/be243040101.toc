<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="be243040101s.html#6">Viaggio pittorico della
      Toscana</a>
    </li>
    <li>
      <a href="be243040101s.html#6">Tomo I</a>
      <ul>
        <li>
          <a href="be243040101s.html#8">Maestà</a>
        </li>
        <li>
          <a href="be243040101s.html#12">Prefazione</a>
        </li>
        <li>
          <a href="be243040101s.html#14">Carta corografica della
          Toscana</a>
        </li>
        <li>
          <a href="be243040101s.html#18">Veduta di Firenze</a>
        </li>
        <li>
          <a href="be243040101s.html#22">Pianta di Firenze</a>
        </li>
        <li>
          <a href="be243040101s.html#26">Porta a San Frediano</a>
        </li>
        <li>
          <a href="be243040101s.html#30">Porta a San Gallo</a>
          <ul>
            <li>
              <a href="be243040101s.html#29">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#32">Porta A San Niccolò e
          veduta di San Miniato al Monte</a>
        </li>
        <li>
          <a href="be243040101s.html#35">[Immagine]</a>
        </li>
        <li>
          <a href="be243040101s.html#36">Porta a San Pier
          Gattolini</a>
        </li>
        <li>
          <a href="be243040101s.html#38">Palazzo Pitti</a>
        </li>
        <li>
          <a href="be243040101s.html#42">Veduta del cortile del
          Palazzo Pitti</a>
          <ul>
            <li>
              <a href="be243040101s.html#41">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#44">Veduta della Fortezza di
          Belvedere presa da Boboli</a>
        </li>
        <li>
          <a href="be243040101s.html#48">Pianta della Chiesa di
          Santo Spirito</a>
          <ul>
            <li>
              <a href="be243040101s.html#47">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#52">Interno della Chiesa di
          Santo Spirito</a>
          <ul>
            <li>
              <a href="be243040101s.html#51">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#54">Veduta del ponte a Santa
          Trinità</a>
        </li>
        <li>
          <a href="be243040101s.html#58">Veduta della Piazza di
          Santa Trinità</a>
          <ul>
            <li>
              <a href="be243040101s.html#57">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#62">Veduta della Piazza di
          Santa Maria Novella</a>
          <ul>
            <li>
              <a href="be243040101s.html#61">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#66">Veduta del Castello di
          San Giovanni Battista ossia Fortezza da Basso</a>
          <ul>
            <li>
              <a href="be243040101s.html#65">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#68">Veduta dello Spedale di
          Bonifazio</a>
        </li>
        <li>
          <a href="be243040101s.html#72">Pianta della Chiesa di
          San Lorenzo</a>
          <ul>
            <li>
              <a href="be243040101s.html#71">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#76">Cappella de' Depositi
          Medicei in San Lorenzo</a>
          <ul>
            <li>
              <a href="be243040101s.html#75">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#80">Veduta della libreria
          Mediceo-Laurenziana</a>
          <ul>
            <li>
              <a href="be243040101s.html#79">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#82">Veduta di Via Larga</a>
        </li>
        <li>
          <a href="be243040101s.html#86">Veduta della Cappella di
          Sant'Antonino nella Chiesa di San Marco</a>
          <ul>
            <li>
              <a href="be243040101s.html#85">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#90">Veduta dell'orto agrario
          una volta giardino botanico</a>
          <ul>
            <li>
              <a href="be243040101s.html#89">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#92">Veduta della Piazza
          della Nunziata</a>
        </li>
        <li>
          <a href="be243040101s.html#96">Veduta della Cappella
          del Soccorso nella Chiesa della Nunziata</a>
          <ul>
            <li>
              <a href="be243040101s.html#95">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#100">Pianta del Tempio degli
          Angeli</a>
          <ul>
            <li>
              <a href="be243040101s.html#99">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#102">Veduta dell'Arcispedale
          di Santa Maria Nuova</a>
        </li>
        <li>
          <a href="be243040101s.html#106">Veduta della
          cattedrale</a>
          <ul>
            <li>
              <a href="be243040101s.html#105">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#110">Pianta della
          cattedrale</a>
          <ul>
            <li>
              <a href="be243040101s.html#109">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#114">Veduta del Tempio di
          San Giovanni</a>
          <ul>
            <li>
              <a href="be243040101s.html#113">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#118">Veduta d'Or San
          Michele</a>
          <ul>
            <li>
              <a href="be243040101s.html#117">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#120">Veduta della Piazza del
          Granduca</a>
        </li>
        <li>
          <a href="be243040101s.html#122">Veduta della Loggia dei
          Lanzi</a>
        </li>
        <li>
          <a href="be243040101s.html#124">Veduta dell'interno
          degli Uffizi</a>
        </li>
        <li>
          <a href="be243040101s.html#128">Veduta dell'antico
          Palazzo del Potestà oggi luogo delle pubbliche
          carceri</a>
          <ul>
            <li>
              <a href="be243040101s.html#127">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#130">Veduta della Piazza di
          Santa Croca</a>
        </li>
        <li>
          <a href="be243040101s.html#132">Veduta della Cappella
          de' Pazzi nel Chiostro di Santa Croce</a>
        </li>
        <li>
          <a href="be243040101s.html#134">Veduta dell'Arco de'
          Peruzzi</a>
        </li>
        <li>
          <a href="be243040101s.html#138">Pianta dell'Anfiteatro
          Fiorentino</a>
          <ul>
            <li>
              <a href="be243040101s.html#137">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#142">Veduta del Ponte alla
          Badia alle falde di Fiesole</a>
          <ul>
            <li>
              <a href="be243040101s.html#141">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#144">Veduta della Piazza di
          Fiesole</a>
        </li>
        <li>
          <a href="be243040101s.html#148">Veduta del Duomo di
          Fiesole</a>
          <ul>
            <li>
              <a href="be243040101s.html#147">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#152">Veduta di Maiano</a>
          <ul>
            <li>
              <a href="be243040101s.html#151">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#156">Veduta della Badia a
          Ripoli</a>
          <ul>
            <li>
              <a href="be243040101s.html#155">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#160">Veduta del Poggio
          Imperiale</a>
          <ul>
            <li>
              <a href="be243040101s.html#159">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#164">Veduta della Certosa di
          Firenze</a>
          <ul>
            <li>
              <a href="be243040101s.html#163">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#168">Veduta delle Cascine
          presso a Firenze</a>
          <ul>
            <li>
              <a href="be243040101s.html#167">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#172">Veduta del Castello di
          Campi</a>
          <ul>
            <li>
              <a href="be243040101s.html#171">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#174">Veduta della città di
          Prato</a>
        </li>
        <li>
          <a href="be243040101s.html#178">Veduta della cattedrale
          di Prato</a>
          <ul>
            <li>
              <a href="be243040101s.html#177">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#180">Veduta dell'interno
          della Chiesa delle Carceri</a>
        </li>
        <li>
          <a href="be243040101s.html#184">Veduta del Palazzo
          Pretorio</a>
          <ul>
            <li>
              <a href="be243040101s.html#183">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#188">Veduta del Collegio
          Cicognini</a>
          <ul>
            <li>
              <a href="be243040101s.html#187">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#192">Veduta della Chiesa di
          San Domenico</a>
          <ul>
            <li>
              <a href="be243040101s.html#191">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#196">Veduta del Poggio a
          Caiano</a>
          <ul>
            <li>
              <a href="be243040101s.html#195">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#198">Veduta della città di
          Pistoia</a>
        </li>
        <li>
          <a href="be243040101s.html#200">Veduta della cattedrale
          di Pistoia</a>
        </li>
        <li>
          <a href="be243040101s.html#202">Veduta del Battistero
          di San Giovanni</a>
        </li>
        <li>
          <a href="be243040101s.html#204">Veduta del Palazzo
          vescovile</a>
        </li>
        <li>
          <a href="be243040101s.html#208">Veduta del
          Seminario</a>
          <ul>
            <li>
              <a href="be243040101s.html#207">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#212">Veduta del Tempio
          dell'Umiltà</a>
          <ul>
            <li>
              <a href="be243040101s.html#211">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#214">Veduta di Seravalle</a>
        </li>
        <li>
          <a href="be243040101s.html#218">Veduta de' Bagni di
          Montecatini</a>
          <ul>
            <li>
              <a href="be243040101s.html#217">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#222">Veduta della città di
          Pescia</a>
          <ul>
            <li>
              <a href="be243040101s.html#221">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#226">Veduta
          dell'Altopascio</a>
          <ul>
            <li>
              <a href="be243040101s.html#225">[Immagine]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="be243040101s.html#228">Veduta della città di
          Lucca</a>
        </li>
        <li>
          <a href="be243040101s.html#230">Veduta della cattedrale
          di Lucca</a>
        </li>
        <li>
          <a href="be243040101s.html#232">Veduta della Chiesa di
          San Michele</a>
        </li>
        <li>
          <a href="be243040101s.html#234">Veduta dell'interno del
          Palazzo Pubblico di Lucca</a>
        </li>
        <li>
          <a href="be243040101s.html#236">Veduta delle Rovine
          dell'Anfiteatro di Lucca</a>
        </li>
        <li>
          <a href="be243040101s.html#238">Veduta dei bagni di
          Lucca</a>
        </li>
        <li>
          <a href="be243040101s.html#240">Veduta di Viareggio</a>
        </li>
        <li>
          <a href="be243040101s.html#242">Veduta di
          Pietrasanta</a>
        </li>
        <li>
          <a href="be243040101s.html#244">Veduta della terra di
          Seravezza</a>
        </li>
        <li>
          <a href="be243040101s.html#246">Veduta di Fivizzano</a>
        </li>
        <li>
          <a href="be243040101s.html#248">Veduta degli avanzi
          della città di Luni</a>
        </li>
        <li>
          <a href="be243040101s.html#250">Veduta de' due castelli
          Albiano e Caprigola</a>
        </li>
        <li>
          <a href="be243040101s.html#252">Veduta della città di
          Pontremoli</a>
        </li>
        <li>
          <a href="be243040101s.html#254">Veduta della cattedrale
          di Pontremoli</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="be243040101s.html#256">[Indici]</a>
      <ul>
        <li>
          <a href="be243040101s.html#256">Indice delle vedute e
          dei rami</a>
        </li>
        <li>
          <a href="be243040101s.html#258">Indice degli
          artisti</a>
        </li>
        <li>
          <a href="be243040101s.html#262">Indice delle cose più
          notabili</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
