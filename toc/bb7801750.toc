<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="bb7801750s.html#6">Ritratto delle più nobili et
      famose città d'Italia&#160;</a>
      <ul>
        <li>
          <a href="bb7801750s.html#8">All'illustrissimo signore il
          S. Antonio Martinengo condottiero d'huomini d'arme della
          Serenissima Signoria di Venezia Francesco Sansovino</a>
        </li>
        <li>
          <a href="bb7801750s.html#10">Francesco Sansovino a i
          lettori&#160;</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="bb7801750s.html#10">[Elenco delle città]</a>
      <ul>
        <li>
          <a href="bb7801750s.html#10">Aquila</a>
        </li>
        <li>
          <a href="bb7801750s.html#10">Adria</a>
        </li>
        <li>
          <a href="bb7801750s.html#11">Ascolo</a>
        </li>
        <li>
          <a href="bb7801750s.html#11">Arezzo</a>
        </li>
        <li>
          <a href="bb7801750s.html#13">Ancona</a>
        </li>
        <li>
          <a href="bb7801750s.html#13">Asti</a>
        </li>
        <li>
          <a href="bb7801750s.html#13">Alea</a>
        </li>
        <li>
          <a href="bb7801750s.html#14">Angiera</a>
        </li>
        <li>
          <a href="bb7801750s.html#14">Altino</a>
        </li>
        <li>
          <a href="bb7801750s.html#14">Aquilea</a>
        </li>
        <li>
          <a href="bb7801750s.html#15">Borgo San Sepolcro</a>
        </li>
        <li>
          <a href="bb7801750s.html#15">Bevagna</a>
        </li>
        <li>
          <a href="bb7801750s.html#15">Bitonto</a>
        </li>
        <li>
          <a href="bb7801750s.html#16">Brindisi</a>
        </li>
        <li>
          <a href="bb7801750s.html#16">Bari</a>
        </li>
        <li>
          <a href="bb7801750s.html#17">Bisignano</a>
        </li>
        <li>
          <a href="bb7801750s.html#17">Benevento</a>
        </li>
        <li>
          <a href="bb7801750s.html#18">Bologna</a>
        </li>
        <li>
          <a href="bb7801750s.html#20">Bobio</a>
        </li>
        <li>
          <a href="bb7801750s.html#21">Brescia</a>
        </li>
        <li>
          <a href="bb7801750s.html#33">Bergamo</a>
        </li>
        <li>
          <a href="bb7801750s.html#38">Corneto</a>
        </li>
        <li>
          <a href="bb7801750s.html#38">Como</a>
        </li>
        <li>
          <a href="bb7801750s.html#41">Carrara</a>
        </li>
        <li>
          <a href="bb7801750s.html#41">Chiusi</a>
        </li>
        <li>
          <a href="bb7801750s.html#41">Cortona</a>
        </li>
        <li>
          <a href="bb7801750s.html#42">Castro</a>
        </li>
        <li>
          <a href="bb7801750s.html#42">Città di Castello</a>
        </li>
        <li>
          <a href="bb7801750s.html#43">Capua</a>
        </li>
        <li>
          <a href="bb7801750s.html#43">Cocenza</a>
        </li>
        <li>
          <a href="bb7801750s.html#44">Cotrone</a>
        </li>
        <li>
          <a href="bb7801750s.html#44">Camerino</a>
        </li>
        <li>
          <a href="bb7801750s.html#45">Cervia</a>
        </li>
        <li>
          <a href="bb7801750s.html#45">Cesena</a>
        </li>
        <li>
          <a href="bb7801750s.html#46">Comacchio</a>
        </li>
        <li>
          <a href="bb7801750s.html#46">Casale</a>
        </li>
        <li>
          <a href="bb7801750s.html#47">Cremona</a>
        </li>
        <li>
          <a href="bb7801750s.html#48">Cividal di Belluno</a>
        </li>
        <li>
          <a href="bb7801750s.html#50">Chioggia</a>
        </li>
        <li>
          <a href="bb7801750s.html#50">Ceneda</a>
        </li>
        <li>
          <a href="bb7801750s.html#50">Concordia</a>
        </li>
        <li>
          <a href="bb7801750s.html#51">Crema</a>
        </li>
        <li>
          <a href="bb7801750s.html#52">Correggio</a>
        </li>
        <li>
          <a href="bb7801750s.html#54">Eugubio</a>
        </li>
        <li>
          <a href="bb7801750s.html#55">Fiorenza</a>
        </li>
        <li>
          <a href="bb7801750s.html#66">Fuligno</a>
        </li>
        <li>
          <a href="bb7801750s.html#67">Fondi</a>
        </li>
        <li>
          <a href="bb7801750s.html#67">Fermo</a>
        </li>
        <li>
          <a href="bb7801750s.html#68">Fano</a>
        </li>
        <li>
          <a href="bb7801750s.html#68">Fossombrone</a>
        </li>
        <li>
          <a href="bb7801750s.html#69">Forlì</a>
        </li>
        <li>
          <a href="bb7801750s.html#73">Faenza</a>
        </li>
        <li>
          <a href="bb7801750s.html#76">Ferrara</a>
        </li>
        <li>
          <a href="bb7801750s.html#88">Feltro</a>
        </li>
        <li>
          <a href="bb7801750s.html#89">Forlimpopoli</a>
        </li>
        <li>
          <a href="bb7801750s.html#92">Genova</a>
        </li>
        <li>
          <a href="bb7801750s.html#103">Gaeta</a>
        </li>
        <li>
          <a href="bb7801750s.html#104">Gravina</a>
        </li>
        <li>
          <a href="bb7801750s.html#104">Goritia</a>
        </li>
        <li>
          <a href="bb7801750s.html#106">Imola</a>
        </li>
        <li>
          <a href="bb7801750s.html#109">Iustinopoli</a>
        </li>
        <li>
          <a href="bb7801750s.html#110">Luni</a>
        </li>
        <li>
          <a href="bb7801750s.html#110">Leccie</a>
        </li>
        <li>
          <a href="bb7801750s.html#110">Luceria&#160;</a>
        </li>
        <li>
          <a href="bb7801750s.html#112">Lodi</a>
        </li>
        <li>
          <a href="bb7801750s.html#115">Manfredonia</a>
        </li>
        <li>
          <a href="bb7801750s.html#115">Macerata</a>
        </li>
        <li>
          <a href="bb7801750s.html#115">Modona</a>
        </li>
        <li>
          <a href="bb7801750s.html#118">Mantova</a>
        </li>
        <li>
          <a href="bb7801750s.html#126">Milano</a>
        </li>
        <li>
          <a href="bb7801750s.html#160">Nizza</a>
        </li>
        <li>
          <a href="bb7801750s.html#161">Nola</a>
        </li>
        <li>
          <a href="bb7801750s.html#161">Narni</a>
        </li>
        <li>
          <a href="bb7801750s.html#161">Napoli</a>
        </li>
        <li>
          <a href="bb7801750s.html#170">Novara</a>
        </li>
        <li>
          <a href="bb7801750s.html#170">Orvieto</a>
        </li>
        <li>
          <a href="bb7801750s.html#171">Otranto</a>
        </li>
        <li>
          <a href="bb7801750s.html#171">Pisa</a>
        </li>
        <li>
          <a href="bb7801750s.html#174">Pistoia</a>
        </li>
        <li>
          <a href="bb7801750s.html#175">Perugia</a>
        </li>
        <li>
          <a href="bb7801750s.html#179">Pesaro</a>
        </li>
        <li>
          <a href="bb7801750s.html#180">Parma</a>
        </li>
        <li>
          <a href="bb7801750s.html#185">Pavia</a>
        </li>
        <li>
          <a href="bb7801750s.html#190">Parenzo</a>
        </li>
        <li>
          <a href="bb7801750s.html#190">Pola</a>
        </li>
        <li>
          <a href="bb7801750s.html#190">Padova</a>
        </li>
        <li>
          <a href="bb7801750s.html#199">Piacenza</a>
        </li>
        <li>
          <a href="bb7801750s.html#203">Rieti</a>
        </li>
        <li>
          <a href="bb7801750s.html#204">Roma</a>
        </li>
        <li>
          <a href="bb7801750s.html#205">Reggio</a>
        </li>
        <li>
          <a href="bb7801750s.html#205">Rimino</a>
        </li>
        <li>
          <a href="bb7801750s.html#211">Ravenna</a>
        </li>
        <li>
          <a href="bb7801750s.html#220">Siena</a>
        </li>
        <li>
          <a href="bb7801750s.html#223">Sutri</a>
        </li>
        <li>
          <a href="bb7801750s.html#223">Spoleto</a>
        </li>
        <li>
          <a href="bb7801750s.html#224">Sulmona</a>
        </li>
        <li>
          <a href="bb7801750s.html#224">Salerno</a>
        </li>
        <li>
          <a href="bb7801750s.html#226">Terni</a>
        </li>
        <li>
          <a href="bb7801750s.html#229">Trani</a>
        </li>
        <li>
          <a href="bb7801750s.html#229">Viterbo</a>
        </li>
        <li>
          <a href="bb7801750s.html#231">Vicenza</a>
        </li>
        <li>
          <a href="bb7801750s.html#237">Volterra</a>
        </li>
        <li>
          <a href="bb7801750s.html#241">Vercelli</a>
        </li>
        <li>
          <a href="bb7801750s.html#241">Urbino</a>
        </li>
        <li>
          <a href="bb7801750s.html#243">Udene</a>
        </li>
        <li>
          <a href="bb7801750s.html#244">Verona</a>
        </li>
        <li>
          <a href="bb7801750s.html#257">Venetia</a>
        </li>
        <li>
          <a href="bb7801750s.html#275">Treviso</a>
        </li>
        <li>
          <a href="bb7801750s.html#276">Cesena</a>
        </li>
        <li>
          <a href="bb7801750s.html#276">Ravenna</a>
        </li>
        <li>
          <a href="bb7801750s.html#277">Genova</a>
        </li>
        <li>
          <a href="bb7801750s.html#281">Padova</a>
        </li>
        <li>
          <a href="bb7801750s.html#282">Napoli</a>
        </li>
        <li>
          <a href="bb7801750s.html#285">Correggio</a>
        </li>
        <li>
          <a href="bb7801750s.html#290">Lucca</a>
        </li>
        <li>
          <a href="bb7801750s.html#293">Albinga</a>
        </li>
        <li>
          <a href="bb7801750s.html#293">Ascesi</a>
        </li>
        <li>
          <a href="bb7801750s.html#293">Amelia</a>
        </li>
        <li>
          <a href="bb7801750s.html#293">Aversa</a>
        </li>
        <li>
          <a href="bb7801750s.html#294">Amalfi</a>
        </li>
        <li>
          <a href="bb7801750s.html#294">Andri</a>
        </li>
        <li>
          <a href="bb7801750s.html#294">Amiterno</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="bb7801750s.html#295">Tavola delle città che si
      contengono nel presente libro&#160;</a>
    </li>
  </ul>
  <hr />
</body>
</html>
