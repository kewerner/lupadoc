<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="be12504190s.html#8">Storia delle belle arti
      friulane</a>
      <ul>
        <li>
          <a href="be12504190s.html#10">Compartimento</a>
        </li>
        <li>
          <a href="be12504190s.html#11">Indici generali</a>
        </li>
        <li>
          <a href="be12504190s.html#12">Prefazione</a>
        </li>
        <li>
          <a href="be12504190s.html#20">Parte prima</a>
        </li>
        <li>
          <a href="be12504190s.html#196">Note</a>
        </li>
        <li>
          <a href="be12504190s.html#217">Documenti</a>
        </li>
        <li>
          <a href="be12504190s.html#284">Indice dei documenti</a>
        </li>
        <li>
          <a href="be12504190s.html#289">Tavola alfabetica delle
          città</a>
        </li>
        <li>
          <a href="be12504190s.html#292">Tavola alfabetica di
          tutti gli artefici</a>
        </li>
        <li>
          <a href="be12504190s.html#294">Indice delle cose
          principali</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
