<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="calom20187017s.html#6">Rime di Gio. Paolo
      Lomazzi milanese pittore, divise in sette libri</a>
      <ul>
        <li>
          <a href="calom20187017s.html#8">Al serenissimo
          Prencipe Don Carlo Emanuello Gran Duca di Savoia</a>
        </li>
        <li>
          <a href="calom20187017s.html#12">All'istessa
          Altezza</a>
        </li>
        <li>
          <a href="calom20187017s.html#18">Capitolo dove si
          dimostra, che cosa sia Grottesco, et la sua origine</a>
        </li>
        <li>
          <a href="calom20187017s.html#38">I. Nel quale si
          tratta di cose sacre, e religiose, de le virtù, e de le
          arti liberali</a>
        </li>
        <li>
          <a href="calom20187017s.html#84">II. Dove si tratta
          de le lodi di vari Principi, et Signori, di Pittori,
          Scultori, Architetti, et opere loro</a>
        </li>
        <li>
          <a href="calom20187017s.html#150">III. Dove si tratta
          de le lodi di diversi huomini eccellenti in armi et in
          lettere</a>
        </li>
        <li>
          <a href="calom20187017s.html#212">IV. Dove si
          contengono varie dimostrationi, essempi, historie,
          riprensioni, et altre fantasie dichiarate sotto
          metafora</a>
        </li>
        <li>
          <a href="calom20187017s.html#302">V. Dove si
          contengono diverse historie antiche, con diverse
          sentenze, et avvertimenti raccolti insieme</a>
        </li>
        <li>
          <a href="calom20187017s.html#412">VI. Nel quale si
          contengono varij grilli, chimere, caprizzi, bizzarie,
          sotto metafore, si come da studiosi ingegni
          s'intenderà</a>
        </li>
        <li>
          <a href="calom20187017s.html#504">VII. Dove si
          ragiona de i costumi, e de le maniere de alcuni pochi
          Pedanti cantati per scherzo come Pittori</a>
        </li>
        <li>
          <a href="calom20187017s.html#526">Breve trattato
          delle vita dell'autore descritta da lui stesso</a>
        </li>
        <li>
          <a href="calom20187017s.html#568">Tavola de i nomi
          d'alcuni moderni eccellenti nelle arti loro,sparsi per
          tutta l'opera</a>
        </li>
        <li>
          <a href="calom20187017s.html#572">Tavola de i
          grotteschi</a>
        </li>
        <li>
          <a href="calom20187017s.html#595">Tavola dei sonetti,
          e epigrammi in lode del Auttore</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
