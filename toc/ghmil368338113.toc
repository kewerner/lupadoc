<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghmil368338113s.html#6">Principj di architettura
      civile; Tomo III.</a>
      <ul>
        <li>
          <a href="ghmil368338113s.html#8">parte terza</a>
          <ul>
            <li>
              <a href="ghmil368338113s.html#14">I. Della scelta,
              e dell'uso de' materiali per l'Architettura</a>
            </li>
            <li>
              <a href="ghmil368338113s.html#106">II. De' terreni
              idonei per le fabbriche e per i fondamenti</a>
            </li>
            <li>
              <a href="ghmil368338113s.html#165">III. Della
              maniera di fabbricare</a>
            </li>
            <li>
              <a href="ghmil368338113s.html#230">IV. Della
              resistenza de' materiali</a>
            </li>
            <li>
              <a href="ghmil368338113s.html#445">Piano
              dell'Opera</a>
            </li>
            <li>
              <a href="ghmil368338113s.html#452">[Tavole]</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
