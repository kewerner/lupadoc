<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="emil153770s.html#6">Istruzione intorno alle opere
      de’ pittori nazionali ed esteri, esposte in pubblico nella
      città di Milano con qualche notizia degli scultori ed
      architetti</a>
      <ul>
        <li>
          <a href="emil153770s.html#8">Prefazione</a>
        </li>
        <li>
          <a href="emil153770s.html#12">Passeggio primo</a>
        </li>
        <li>
          <a href="emil153770s.html#94">Tavola delle chiese</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
