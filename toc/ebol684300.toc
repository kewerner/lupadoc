<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebol684300s.html#6">Ornato della Porta della nobil
      Casa Salina in Bologna</a>
      <ul>
        <li>
          <a href="ebol684300s.html#8">[Dedica dell'autore a
          Luigi Salina] Chiarissimo Segnore Cavaliere Conte
          Avvocato Luigi Salina&#160;</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ebol684300s.html#10">[Text]</a>
      <ul>
        <li>
          <a href="ebol684300s.html#10">Ornato della Porta di
          Casa Salina già Mamancini in Bologna ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
