<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501840s.html#8">Le Cose Maravigliose Dell’Alma
      Città Di Roma</a>
      <ul>
        <li>
          <a href="dg4501840s.html#10">Le sette chiese
          principali</a>
          <ul>
            <li>
              <a href="dg4501840s.html#10">La Prima Chiesa è ›San
              Giovanni in Laterano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501840s.html#13">Seconda chiesa di ›San
              Pietro in Vaticano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501840s.html#15">La terza chiesa è ›San
              Paolo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4501840s.html#16">La quarta chiesa è
              ›Santa Maria Maggiore‹ ▣</a>
            </li>
            <li>
              <a href="dg4501840s.html#17">La quinta chiesa è ›San
              Lorenzo fuori le Mura‹ ▣</a>
            </li>
            <li>
              <a href="dg4501840s.html#18">La sesta chiesa è ›San
              Sebastiano‹ ▣</a>
            </li>
            <li>
              <a href="dg4501840s.html#18">La settima chiesa è
              ›Santa Croce in Gerusalemme‹ ▣</a>
            </li>
            <li>
              <a href="dg4501840s.html#19">Nell'›Isola
              Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#19">In ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#21">Nel ›Borgo‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#23">Della Porta Flaminia
              fuori del Popolo ›Porta del popolo‹ fino alle radici
              del Campidoglio</a>
            </li>
            <li>
              <a href="dg4501840s.html#32">Del ›Campidoglio‹ a man
              sinistra verso li monti</a>
            </li>
            <li>
              <a href="dg4501840s.html#37">Dal Campidoglio a man
              dritta verso li monti</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501840s.html#42">Tavola delle Chiese</a>
        </li>
        <li>
          <a href="dg4501840s.html#44">Le Stationi, che sono nelle
          chiese di Roma, si per la Quadragesima, come per tutto
          l'anno. Con le solite Indulgenze.</a>
          <ul>
            <li>
              <a href="dg4501840s.html#44">Nel mese di Gennaro</a>
            </li>
            <li>
              <a href="dg4501840s.html#44">Nel mese di Ferbaro
              (sic!)</a>
            </li>
            <li>
              <a href="dg4501840s.html#47">Nel mese di Aprile</a>
            </li>
            <li>
              <a href="dg4501840s.html#47">Nel mese di Maggio</a>
            </li>
            <li>
              <a href="dg4501840s.html#48">Nel mese di Giugno</a>
            </li>
            <li>
              <a href="dg4501840s.html#48">Nel mese di Luglio</a>
            </li>
            <li>
              <a href="dg4501840s.html#48">Nel mese di Agosto</a>
            </li>
            <li>
              <a href="dg4501840s.html#48">Nel mese di
              Settembre</a>
            </li>
            <li>
              <a href="dg4501840s.html#48">Nel mese di Ottobre</a>
            </li>
            <li>
              <a href="dg4501840s.html#48">Nel mese di
              Novembre</a>
            </li>
            <li>
              <a href="dg4501840s.html#48">Le Stationi
              dell'Advento</a>
              <ul>
                <li>
                  <a href="dg4501840s.html#48">Nel mese di
                  Dicembre</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501840s.html#51">Trattato over modo
          d'acquistar l'indulgenze alle Stationi</a>
        </li>
        <li>
          <a href="dg4501840s.html#55">La Guida romana per li
          forasteri, che vengono per vedere le Antichità di Roma, a
          una per una in bellissima forma et brevità</a>
          <ul>
            <li>
              <a href="dg4501840s.html#55">Del Borgo la prima
              giornata</a>
              <ul>
                <li>
                  <a href="dg4501840s.html#56">Del
                  ›Trastevere‹</a>
                </li>
                <li>
                  <a href="dg4501840s.html#57">Dell'›Isola
                  Tiberina‹</a>
                </li>
                <li>
                  <a href="dg4501840s.html#57">Del Ponte Santa
                  Maria ›Ponte Rotto‹, del palazzo di Pilato ›Casa
                  dei Crescenzi‹, et d'altre cose</a>
                </li>
                <li>
                  <a href="dg4501840s.html#58">Delle Therme
                  Antoniane ›Terme di Caracalla‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501840s.html#58">Di ›San Giovanni in
                  Laterano‹, ›Santa Croce in Gerusalemme‹ et
                  altri</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501840s.html#59">Giornata seconda</a>
              <ul>
                <li>
                  <a href="dg4501840s.html#59">Della ›Porta del
                  Popolo‹</a>
                </li>
                <li>
                  <a href="dg4501840s.html#59">Dei cavalli di
                  marmo, che stanno a Monte Cavallo ›Fontana di
                  Monte Cavallo‹, et delle Therme Dioceltiani
                  ›Terme di Diocleziano‹</a>
                </li>
                <li>
                  <a href="dg4501840s.html#60">Della strada Pia
                  ›Via XX settembre‹</a>
                </li>
                <li>
                  <a href="dg4501840s.html#60">Della Vigna del
                  Cardinal di Carpi, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501840s.html#60">Della ›Porta
                  Pia‹</a>
                </li>
                <li>
                  <a href="dg4501840s.html#61">Di ›Santa Agnese
                  fuori le mura‹, et altre anticaglie</a>
                </li>
                <li>
                  <a href="dg4501840s.html#61">Del Tempio d'Iside
                  ›Isis Patritia‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501840s.html#61">Delle ›Sette Sale‹,
                  et del ›Colosseo‹, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501840s.html#62">Del Tempio della
                  Pace ›Tempo della Pace‹ et del monte ›Palatino‹,
                  hora detto Palazzo maggiore, et altre cose</a>
                </li>
                <li>
                  <a href="dg4501840s.html#62">Del ›Campidoglio‹,
                  et altre cose</a>
                </li>
                <li>
                  <a href="dg4501840s.html#63">Dei portichi
                  d'Ottavia, di Settimio ›Portico d'Ottavia‹ et
                  ›Teatro di Pompeo‹</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dg4501840s.html#63">Giornata terza</a>
              <ul>
                <li>
                  <a href="dg4501840s.html#63">Delle due colonne,
                  una di Antonino Pio ›Colonna di Antonino Pio‹, e
                  l'altra di Traiano ›Colonna di Traiano‹, et altre
                  cose</a>
                </li>
                <li>
                  <a href="dg4501840s.html#64">Della Rotonda
                  ovvero ›Pantheon‹</a>
                </li>
                <li>
                  <a href="dg4501840s.html#64">Dei bagni di
                  Agrippa ›Terme di Agrippa‹, e di Nerone ›Terme
                  Neroniano-Alessandrine‹</a>
                </li>
                <li>
                  <a href="dg4501840s.html#64">Della ›Piazza
                  Navona‹, et di mastro Pasquino ›Statua di
                  Pasquino‹</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501840s.html#65">[Tavole]</a>
          <ul>
            <li>
              <a href="dg4501840s.html#65">Summi Pontifices</a>
            </li>
            <li>
              <a href="dg4501840s.html#82">Reges, et imperatores
              romani</a>
            </li>
            <li>
              <a href="dg4501840s.html#86">Li re di Francia</a>
            </li>
            <li>
              <a href="dg4501840s.html#87">Li re del Regno di
              Napoli et di Sicilia [...]</a>
            </li>
            <li>
              <a href="dg4501840s.html#88">Li dogi di Venegia</a>
            </li>
            <li>
              <a href="dg4501840s.html#90">Li duchi di Milano</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    <li>
      <a href="dg4501840s.html#92">L'Antichità di Roma di M.
      Andrea Palladio racolta brevemente da gli Autori antichi, et
      moderni ▣</a>
      <ul>
        <li>
          <a href="dg4501840s.html#93">Alli lettori</a>
        </li>
        <li>
          <a href="dg4501840s.html#94">Libro I. Dell'edification
          di Roma</a>
          <ul>
            <li>
              <a href="dg4501840s.html#95">Del circuito di
              Roma</a>
            </li>
            <li>
              <a href="dg4501840s.html#96">Delle Porte</a>
            </li>
            <li>
              <a href="dg4501840s.html#97">Delle Vie</a>
            </li>
            <li>
              <a href="dg4501840s.html#98">Delli Ponti che sono
              sopra il Tevere, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501840s.html#99">Dell'isola del Tevere
              ›Isola Tiberina‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#99">Delli Monti</a>
            </li>
            <li>
              <a href="dg4501840s.html#100">Del Monte Testaccio
              ›Testaccio (monte)‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#100">Delle acque, et chi le
              condusse in Roma</a>
            </li>
            <li>
              <a href="dg4501840s.html#101">Della Cloaca ›Cloaca
              Maxima‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#101">Delli acquedotti</a>
            </li>
            <li>
              <a href="dg4501840s.html#101">Delle ›Sette Sale‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#101">Delle terme cioè
              bagni, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501840s.html#102">Delle Naumachie, dove
              si facevano le battaglie navali, et che cose
              erano</a>
            </li>
            <li>
              <a href="dg4501840s.html#103">De Cerchi, et che cosa
              erano</a>
            </li>
            <li>
              <a href="dg4501840s.html#103">De Theatri, et che
              cosa erano, et suoi edificatori</a>
            </li>
            <li>
              <a href="dg4501840s.html#103">Delli Anfiteatri, et
              suoi edificatori, et che cosa erano</a>
            </li>
            <li>
              <a href="dg4501840s.html#104">De' Fori cioè
              piazze</a>
            </li>
            <li>
              <a href="dg4501840s.html#105">Delli Archi Trionfali,
              et a chi si davano</a>
            </li>
            <li>
              <a href="dg4501840s.html#105">De portchi</a>
            </li>
            <li>
              <a href="dg4501840s.html#105">De' trofei, et colonne
              memorande</a>
            </li>
            <li>
              <a href="dg4501840s.html#106">De Colossi</a>
            </li>
            <li>
              <a href="dg4501840s.html#106">Delle Piramidi</a>
            </li>
            <li>
              <a href="dg4501840s.html#106">Delle mete</a>
            </li>
            <li>
              <a href="dg4501840s.html#107">Delli obelischi</a>
            </li>
            <li>
              <a href="dg4501840s.html#107">Delle statue</a>
            </li>
            <li>
              <a href="dg4501840s.html#107">Di Marforio ›Statua di
              Marforio‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#107">De' cavalli</a>
            </li>
            <li>
              <a href="dg4501840s.html#107">Delle librarie</a>
            </li>
            <li>
              <a href="dg4501840s.html#108">Delli Horiuoli</a>
            </li>
            <li>
              <a href="dg4501840s.html#108">De' palazzi</a>
            </li>
            <li>
              <a href="dg4501840s.html#108">Della Casa Aurea di
              Nerone ›Domus Aurea‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#109">Dell'altre case de'
              cittadini</a>
            </li>
            <li>
              <a href="dg4501840s.html#109">Delle curie, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501840s.html#109">De Senatuli, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501840s.html#110">De' magistrati</a>
            </li>
            <li>
              <a href="dg4501840s.html#110">Dei comitii, et che
              cosa erano</a>
            </li>
            <li>
              <a href="dg4501840s.html#110">Delle tribù</a>
            </li>
            <li>
              <a href="dg4501840s.html#111">Delle Regioni, cioè
              Rioni et sue insegne ›Regiones Quattuordecim‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#111">Delle Basiliche</a>
            </li>
            <li>
              <a href="dg4501840s.html#111">Del ›Campidoglio‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#112">Dello Erario
              ›Aerarium‹, cioè Camera del commune, et che moneta si
              spendeva in Roma in quei tempi</a>
            </li>
            <li>
              <a href="dg4501840s.html#113">Del ›Grecostasi‹, et
              che cosa era</a>
            </li>
            <li>
              <a href="dg4501840s.html#113">Della Secretaria del
              popolo romano</a>
            </li>
            <li>
              <a href="dg4501840s.html#113">Dell'›Asilo di
              Romolo‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#113">Delle Rostre et che
              cosa erano ›Rostri‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#113">Della Colonna detta
              Miliario ›Miliarium Aureum‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#113">Del Tempio di Carmenta
              ›Carmentis, Carmenta‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#113">Della Colonna Bellica
              ›Columna Bellica‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#114">Della Colonna Lattaria
              ›Columna Lactaria‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#114">Dell'Equimelio
              ›Aequimaelium‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#114">Del ›Campo Marzio‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#114">Del Tigillo Sororio
              ›Tigillum Sororium‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#114">De Campi Forasteri
              ›Castra Peregrina‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#114">Della ›Villa
              Publica‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#114">Della ›Taberna
              Meritoria‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#115">Del ›Vivarium‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#115">De gli Horti</a>
            </li>
            <li>
              <a href="dg4501840s.html#115">Del ›Velabro‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#116">Delle ›Carinae‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#116">Delle Clivi</a>
            </li>
            <li>
              <a href="dg4501840s.html#116">De Prati</a>
            </li>
            <li>
              <a href="dg4501840s.html#116">Dei granari
              publichi</a>
            </li>
            <li>
              <a href="dg4501840s.html#117">Delle carceri
              publiche</a>
            </li>
            <li>
              <a href="dg4501840s.html#117">Di alcune feste, et
              giuochi</a>
            </li>
            <li>
              <a href="dg4501840s.html#117">Del Sepolcro di
              Augusto, d'Adriano et di Settimio ›Mausoleo di
              Augusto‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#118">De Templi</a>
            </li>
            <li>
              <a href="dg4501840s.html#119">De' Sacerdoti delle
              Vergini Vestali, vestimenti, vasi, et altri
              instrumenti fatti per vio delli sacrificij, et suoi
              institutori</a>
            </li>
            <li>
              <a href="dg4501840s.html#120">Dell'armamentario, et
              che cosa era</a>
            </li>
            <li>
              <a href="dg4501840s.html#120">Dell'esercito romano
              di terra, et di mare, et loro insegne</a>
            </li>
            <li>
              <a href="dg4501840s.html#121">De' triomfi et a chi
              si concedevano et chi fu il primo triomfatore, et di
              quante maniere erano</a>
            </li>
            <li>
              <a href="dg4501840s.html#121">Delle corone, et a chi
              si danno</a>
            </li>
            <li>
              <a href="dg4501840s.html#122">Del numero del Popolo
              Romano</a>
            </li>
            <li>
              <a href="dg4501840s.html#122">Delle ricchezze del
              popolo Romano</a>
            </li>
            <li>
              <a href="dg4501840s.html#122">Della liberalità degli
              antichi Romani</a>
            </li>
            <li>
              <a href="dg4501840s.html#122">Delli matrimoni
              antichi, et loro usanza</a>
            </li>
            <li>
              <a href="dg4501840s.html#123">Della buona creanza,
              che davano ai figliuoli</a>
            </li>
            <li>
              <a href="dg4501840s.html#123">Della separatione de
              matrimonij</a>
            </li>
            <li>
              <a href="dg4501840s.html#124">Dell'essequie, antiche
              er sue cerimonie</a>
            </li>
            <li>
              <a href="dg4501840s.html#124">Delle torri</a>
            </li>
            <li>
              <a href="dg4501840s.html#125">Del ›Tevere‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#125">Del Palazzo Papale
              ›Palazzo Apostolico Vaticano‹ et di Belvedere
              ›Cortile del Belvedere‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#126">Del ›Trastevere‹</a>
            </li>
            <li>
              <a href="dg4501840s.html#126">Recapitulatione
              dell'antichità</a>
            </li>
            <li>
              <a href="dg4501840s.html#127">De tempii de gli
              antichi fuori di Roma</a>
            </li>
            <li>
              <a href="dg4501840s.html#128">Quante volte è stata
              presa Roma</a>
            </li>
            <li>
              <a href="dg4501840s.html#128">De i fuochi de gli
              antichi, scritti da pochi autori, causati da alcuni
              fragmenti d'historie</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="dg4501840s.html#131">Tavola delle antichità
          della città di Roma</a>
        </li>
        <li>
          <a href="dg4501840s.html#133">[Tavola] Poste de
          Italia</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
