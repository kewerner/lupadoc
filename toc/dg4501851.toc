<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dg4501851s.html#6">Ludovici Demontiosii Romae
      hospes</a>
      <ul>
        <li>
          <a href="dg4501851s.html#8">S. D. N. Xysto Quinto
          Pontifici Maximo</a>
        </li>
        <li>
          <a href="dg4501851s.html#9">Index eorum</a>
        </li>
        <li>
          <a href="dg4501851s.html#14">Ludocici Demontiosii Gallus
          Romae Hospes</a>
        </li>
        <li>
          <a href="dg4501851s.html#42">Illustrissimo et
          reverendissimo Principi</a>
        </li>
        <li>
          <a href="dg4501851s.html#46">De Pantheo, et Symmetria
          aedium sacrarum</a>
        </li>
        <li>
          <a href="dg4501851s.html#74">Illustrissimo et
          reverendissimo D. Francisco de Gioiosa S. R. E. Cardinali
          Amplissimo</a>
        </li>
        <li>
          <a href="dg4501851s.html#78">Commentarii de sculptura,
          et pictura antiquorum</a>
        </li>
        <li>
          <a href="dg4501851s.html#96">Illustrissimo et
          reverendissimo D. Annae, Duci de Gioiosa, Franciae Pari,
          et Ammiralio</a>
        </li>
        <li>
          <a href="dg4501851s.html#98">Pictura</a>
        </li>
        <li>
          <a href="dg4501851s.html#118">Illustrissimo et
          reverendissimo D. Carolo Rambolieto S.R.E. Cardinali
          Amplissimo</a>
        </li>
        <li>
          <a href="dg4501851s.html#120">De Foro Romano Veteri, et
          Aliis quibusdam locis circunstantibus</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
