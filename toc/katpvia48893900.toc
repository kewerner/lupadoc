<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="katpvia48893900s.html#8">Catalogo di quadri
      esistenti in casa il Signor D.n Giovanni D.r Vianelli,
      Canonico della Cattedrale di Chioggia</a>
      <ul>
        <li>
          <a href="katpvia48893900s.html#10">Lettore amico</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#12">A</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#33">B</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#36">D</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#40">F</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#55">G</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#88">L</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#94">M</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#103">N</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#116">O</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#117">P</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#144">R</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#149">S</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#155">T</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#157">V</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#159">Aggiunta di altri
          Quadri, di cui gli Autori precisi non si conoscono
          [...]</a>
        </li>
        <li>
          <a href="katpvia48893900s.html#176">Tavola</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
