<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="dr4532960s.html#6">Relazione dello stato vecchio, e
      nuovo dell’Acqua Felice con la notizia sel suo accrescimento
      nel presente anno 1696 ; essendo presidente della medesima
      l’Illustriss. e Reverendiss. Monsignor Lorenzo Corsini ...
      [›Fontana del Mosè‹]</a>
      <ul>
        <li>
          <a href="dr4532960s.html#8">[Dedica]</a>
        </li>
        <li>
          <a href="dr4532960s.html#10">I. Breve notizia del modo
          della Conduttura dell'Acqua portata in Roma da Sisto V. e
          dal suo nome po detta Felice</a>
        </li>
        <li>
          <a href="dr4532960s.html#12">II. A chi fosse data la
          cura di tal'Acqua, con le difficoltà incontratevi, e sue
          spese</a>
        </li>
        <li>
          <a href="dr4532960s.html#14">III. Se l'Acqua condotta da
          Sisto V. sia veramente l'Acqua Alessandrina antica; varie
          considerazioni, ed opinioni sopra ciò</a>
        </li>
        <li>
          <a href="dr4532960s.html#17">IV. Mancanza dell'Acqua
          Felice nel proseguimento de' tempi, varie Congregazioni,
          Lavori, e Perizie fatte per rimediarvi</a>
        </li>
        <li>
          <a href="dr4532960s.html#18">V. Ciò che sia seguito
          intorno allo stato dell'Acqua Felice dopo l'antedetta
          Perizia</a>
        </li>
        <li>
          <a href="dr4532960s.html#19">VI. Ciò che si è operato
          intorno all'Acqua Felice essendo io Architetto, e
          Prefettp della medesima, con lo stato del nuovo
          augumento</a>
        </li>
        <li>
          <a href="dr4532960s.html#22">VII. Nuova Visita, e Misura
          del Corpo della dett'Acqua per riconoscere con più
          esattezza l'accrescimento del suo stato</a>
        </li>
        <li>
          <a href="dr4532960s.html#28">Sommario delle Fedi,
          Perizie, e Testimonianze citate nella presente
          Relazione</a>
        </li>
        <li>
          <a href="dr4532960s.html#38">[Mss. allegati]</a>
          <ul>
            <li>
              <a href="dr4532960s.html#40">Pianta generale delle
              forme esistentia Pantano de Griffi dove ha origina
              l'Aqua Felice [...] [›Fontana del Mosè‹] ◉</a>
            </li>
            <li>
              <a href="dr4532960s.html#42">Indice [dei condotti e
              bottini dell'acqua Felice] [›Fontana del Mosè‹] ◉</a>
              <ul>
                <li>
                  <a href="dr4532960s.html#41">Piazza di Monte
                  Cavallo ◉</a>
                </li>
                <li>
                  <a href="dr4532960s.html#41">Sant'Andrea del
                  Noviziato ◉</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dr4532960s.html#44">[Fontana di Acqua
              Felice ›Fontana del Mosè‹ ◉]</a>
              <ul>
                <li>
                  <a href="dr4532960s.html#43">Piazza di Santa
                  Susanna ◉</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dr4532960s.html#46">Pianta del Castello di
              Termine, e suoi Annessi ◉</a>
            </li>
            <li>
              <a href="dr4532960s.html#49">Pianta dello stato
              anticodella fabrica, e sito spettanteall' Ill.mo
              Sig.r Gio. Antonio Sampieri esistente dietro li
              fontanoni di Termine avanti che il medemo vi
              fabricasse ◉ &#160;</a>
            </li>
            <li>
              <a href="dr4532960s.html#50">Pianta della Botte di
              Monte Cavallo ◉</a>
            </li>
            <li>
              <a href="dr4532960s.html#56">[Pianta della fontana
              dell'Acqua Felice ›Fontana del Mosè‹ ◉]</a>
              <ul>
                <li>
                  <a href="dr4532960s.html#56">Indice delle
                  Botticelle de Particolari, che prendono l'aqua
                  dal condotto avanti la mostra</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dr4532960s.html#58">A di 20 Aprile 1695.
              Tabella di tutti i possessori, che godono, e prendono
              l'aqua Felice nelli descritti luoghi, e botti
              publiche &amp;c. [...] Si</a>
              <ul>
                <li>
                  <a href="dr4532960s.html#58">Si principia dal
                  Arco di Frascati fuori di Porta S. Giovanni
                  descrivendo tutte le fistole de particolari
                  imboccate nei lati del acquedotto maestro avanti
                  la mostra di Termine</a>
                </li>
                <li>
                  <a href="dr4532960s.html#58">Nel Bottino vicino
                  a Porta Maggiore</a>
                </li>
                <li>
                  <a href="dr4532960s.html#59">Seguono le fistole
                  nelli Bottini tra Porta Maggiore e Porta San
                  Lorenzo</a>
                </li>
                <li>
                  <a href="dr4532960s.html#59">Seguono le fistole
                  nelli Bottini a Porta S. Lorenzo</a>
                </li>
                <li>
                  <a href="dr4532960s.html#59">Seguono le altre
                  fistole poste nel acquedotto tra Porta S.
                  Lorenzo, e la Vigna de Padri della Certosa</a>
                </li>
                <li>
                  <a href="dr4532960s.html#60">Botte di Monte
                  Cavallo</a>
                </li>
                <li>
                  <a href="dr4532960s.html#64">Seguono le fistole
                  dentro la Vigna de P.P. Certosini</a>
                </li>
                <li>
                  <a href="dr4532960s.html#64">Seguono l'altre
                  fistole poste nel med.mo Acquedotto sino alla
                  Mostra delli Fontanoni esistenti a Termine</a>
                </li>
                <li>
                  <a href="dr4532960s.html#65">Seguono le fistole
                  dentro la nuova Botticella accanto li Fontanoni
                  di Termine, quai ricevono l'aqua doppo la mostra
                  dei Medesimi</a>
                </li>
                <li>
                  <a href="dr4532960s.html#66">Seguono le fistole
                  tra li fontanoni di Termine e la Botte di Monte
                  Cavallo</a>
                </li>
                <li>
                  <a href="dr4532960s.html#67">Seguono le fistole
                  nella nuova Botticella di S. Carlino</a>
                </li>
                <li>
                  <a href="dr4532960s.html#67">Seguono le altre
                  fistole che bevono dai condotti forzati della
                  Botte di Monte Cavallo, l'acque delle quali sono
                  regolate dalli livelli</a>
                </li>
                <li>
                  <a href="dr4532960s.html#67">Seguono l'altre
                  fistole esistenti nella Botte di Monte
                  Cavallo</a>
                </li>
                <li>
                  <a href="dr4532960s.html#70">Seguono le fistole
                  esistenti nella Botte di Campidoglio</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="dr4532960s.html#76">A di 6 Settembre
              1687</a>
            </li>
            <li>
              <a href="dr4532960s.html#78">A di 9 Settembre
              1687</a>
            </li>
            <li>
              <a href="dr4532960s.html#79">A di 11 Settembre
              1687</a>
            </li>
            <li>
              <a href="dr4532960s.html#80">A di 11 Settembre
              1687</a>
            </li>
            <li>
              <a href="dr4532960s.html#81">A di 6 Settembre
              1687</a>
            </li>
            <li>
              <a href="dr4532960s.html#84">Die 18. Septembris
              1687</a>
            </li>
            <li>
              <a href="dr4532960s.html#85">Die 20. Septembris
              1687</a>
            </li>
            <li>
              <a href="dr4532960s.html#86">Die 25. Septembris
              1687</a>
            </li>
            <li>
              <a href="dr4532960s.html#86">Die 7.a Octobris
              1687</a>
            </li>
            <li>
              <a href="dr4532960s.html#90">A di 12. Novembre
              1687</a>
            </li>
            <li>
              <a href="dr4532960s.html#100">Scritture, che
              difendono il Rossi fatte dal mede.mo</a>
            </li>
            <li>
              <a href="dr4532960s.html#109">Copia della Fede, e
              giustificazione del Rossi Architetto</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
