<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghcat3411540s.html#6">I Qvattro Primi Libri Di
      Architettvra Di Pietro Cataneo Senese</a>
      <ul>
        <li>
          <a href="ghcat3411540s.html#8">All'illustrissimo
          Signore, il Signor Enea Piccolhomini</a>
        </li>
        <li>
          <a href="ghcat3411540s.html#10">Libro primo</a>
        </li>
        <li>
          <a href="ghcat3411540s.html#60">Libro secondo</a>
        </li>
        <li>
          <a href="ghcat3411540s.html#79">Libro terzo</a>
        </li>
        <li>
          <a href="ghcat3411540s.html#101">Libro quarto</a>
        </li>
        <li>
          <a href="ghcat3411540s.html#118">Tavola di quanto ne i
          quattro libri dell'architettura si contiene</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
