<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghbot738735414s.html#6">Raccolta Di Lettere Sulla
      Pittura Scultura Ed Architettura; Tomo IV.</a>
      <ul>
        <li>
          <a href="ghbot738735414s.html#8">A Monsignor D.
          Innocenzio Conti</a>
        </li>
        <li>
          <a href="ghbot738735414s.html#12">Prefazione</a>
        </li>
        <li>
          <a href="ghbot738735414s.html#14">Lettere su la
          pittura scultura ed architettura</a>
        </li>
        <li>
          <a href="ghbot738735414s.html#407">Indice degli Autori
          delle Lettere contenute in questi quattro Tomi</a>
        </li>
        <li>
          <a href="ghbot738735414s.html#414">Indice delle
          persone, a cui sono indirizzate le Lettere, contenute in
          questi quattro Tomi</a>
        </li>
        <li>
          <a href="ghbot738735414s.html#421">Indice delle cose
          notabili in questo Tomo IV.</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
