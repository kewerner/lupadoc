<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghpoz986729301s.html#8">Perspectiva Pictorum et
      architectorum</a>
      <ul>
        <li>
          <a href="ghpoz986729301s.html#12">[Dedica dell'autore
          a Leopoldo Austriaco] Alla sacra cesare Maesta' di
          Leopoldo Austriaco Imperadore</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#18">[Dedica dell'autore
          ai lettori] Al lettore</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghpoz986729301s.html#19">[Text]</a>
      <ul>
        <li>
          <a href="ghpoz986729301s.html#19">I. Spiegatione delle
          linee del piano e dell'orizzonte, e de' punti dell'occhio
          e della distanza ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#21">II. Modo di
          disegnare un quadro in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#23">III. Quadro in
          prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#25">IV. Quadro doppio in
          prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#27">V. Piante de'
          quadrati con l'elevationi ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#29">VI. Modo di
          disegnare in prospettiva senza linee occulte ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#31">VII. Un altro
          esempio del far la pianta geometrica con l'elevatione
          della lunghezza ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#33">VIII. Piedestallo in
          prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#35">IV. Architettura del
          Vignola messa in prospettiva, e prima del piedestallo
          d'Ordine Toscano ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#37">X. Piedestallo
          Dorico in prospettiva; col modo di schivar la confusione
          nel disegnar le piante ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#39">XI. Piedestallo
          Jonico in prospettiva; col modo di fuggire la confusione
          nelle elevationi ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#41">XII. Piedestallo
          Corinthio con le sue pilastrate in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#43">XIII. Piedestallo
          d'ordine Composito in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#45">XIV. Circoli in
          prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#47">XV. Colonna in
          prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#49">XVI. Ase Toscana in
          prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#51">XVII. Base Dorica in
          prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#53">XVIII. Base Jonica
          in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#55">XIX. Base Corinthia
          in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#57">XX. Base Atticurga
          in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#59">XXI. Capitello
          Toscano in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#61">XXII. Capitello
          Dorico in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#63">XXIII. Capitello
          Jonico in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#65">XXIV. Capitello
          Corinthio in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#67">XV. Capitello
          Composito in prospettiva ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#69">XXVI. Cornicion
          Toscano in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#71">XXVII. Cornicion
          Dorico in prospettiva ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#73">XXVIII. Preparatione
          della figura seguente ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#75">XXIX. Fabrica Dorica
          in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#77">XXX. Edificio Jonico
          in prospettiva; col modo di congiugnere il finto col vero
          ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#79">XXXI. Cornicion
          Corinthio col capitello e la sommitä della colonna ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#81">XXXII. Disegno
          geometrico d'un cornicione d'Ordine Composito ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#83">XXXIII. Cornicione
          Composito in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#85">XXXIV. Preparatione
          della figura 35 ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#87">XXV. Cornicione
          composito in prospettiva, veduto di fianco ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#90">XXXVI. Preparatione
          della figura 37 ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#92">XXXVII. Colonna
          Toscana in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#94">XXXVIII.
          Preparatione della figura 39 ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#96">XXXIX. Edificio
          Dorico in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#98">XL. Pianta
          geometrica d'una fabbrica d'ordine Dorico ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#100">XLI. Elevatione
          geometrica d'una fabbrica Dorica ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#102">XLII. Modo di
          schivar la confusione nel far gli scorci delle piante e
          delle elevationi ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#104">XLIII. Pianta della
          figura quarantesima in prospettiva ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#106">XLIV. Elevatione
          della figura 41 in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#108">XLV. La metà d'una
          fabbrica Dorica in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#110">XLVI. L'altra metà
          della medesima fabrica ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#112">XLVII. Piante d'una
          fabbrica Jonica ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#114">XLVIII. Elevation
          geometrica d'una fabbrica Jonica ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#116">XLIX. ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#118">L. Architettura
          Jonica ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#120">LI. Ordine
          Corinthio &#160;▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#122">LII. Colonna
          spirale d'Ordine Composito ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#125">LIII. Tre maniere
          di fare le colonne spirali ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#127">LIV. Pianta d'una
          fabbrica d'ordine Corinthio ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#129">LV. Elevatione
          della fabbrica d'Ordine Corinthio ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#131">LVI. Piante ed
          elevatione in prospettiva della fabbrica Corinthia ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#133">LVII. Abbozzo della
          figura seguente ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#135">LVIII. Edificio
          ottangloare d'Ordine Corinthio ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#137">LIX. Piante d'un
          tabernaolo ottangolare ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#139">LX. Tabernacolo
          ottangolare ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#141">LXI. Modo d'alzar
          le machine che sono composte di più ordine di telari
          ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#143">LXII. Del
          graticolare i telari che rappresentano fabbriche di
          rilievo ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#145">LXIII. Piante d'una
          fabbrica quadrata ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#147">LXIV. Fabbrica
          quadrata ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#149">LXV. Piante d'una
          fabbrica rotonda in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#151">LXVI. Fabbrica
          rotonda in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#153">LXVII. Pianta
          geometrica e prima preparatione della figura 71 ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#155">LXVIII. Elevation
          geometrica della pianta passata, e seconda preparatione
          della figura 71 ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#157">LXIX. Pianta in
          iscorcio della figura 67, e preparation terza alla figura
          71 ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#159">LXX. Profilo in
          prospettiva della figura 68, e quarta preparatione alla
          figura 71 ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#161">LXXI. Teatro delle
          Nozze di Cana Galilea fatto nella Chiesa del GIESU' di
          Roma l'anno 1685 per le 40 hore ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#163">LXXII. De i Teatri
          Scenici ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#165">LXXIII. Altra
          pianta del teatro, con la maniera di trovare il suo punto
          ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#167">LXXIV. Profilo
          delle scene d'un Teatro ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#169">LXXV. Elevatione
          delle scene in faccia: e come le scene storte si facciano
          parer diritte ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#171">LXXVI. Modo di fare
          il disegno delle scene ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#173">LXXVII. Modo di
          graticolare e di dipingere le scene del teatro ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#175">LXXVIII.
          Prospettive orizzontali ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#177">LXXIX. Pianta
          alzata della mensola in prospettiva ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#179">LXXX. Mensola
          ombreggiata di sotto in su ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#181">LXXXI. Piedestalli
          Corinthi di sotto in su ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#183">LXXXII. Colonna
          Corinthia di sotto in su ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#185">LXXXIII. Capitelli
          Corinthi di sotto in su ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#187">LXXXIV. Cornicion
          Corinthio ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#189">LXXXV. Cornicion
          Corinthio di sotto in su ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#191">LXXXVI. Colonna in
          prospettiva di sotto in su ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#193">LXXXVII.
          Preparatione neccessaria per la figura a seguente, e per
          tutte le altre prospettive di sotto in su ne'soffitti o
          nelle volte ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#195">LXXXVIII.
          Balaustrata della figura 87, meßa in prospettiva di sotto
          in su con distanza corta ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#197">LXXXIX.
          Architettura in prospettiva in un soffitto quadrato ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#199">XC. Cupola in
          prospettiva di sotto in su ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#201">XCI. Cupola della
          figura 90, co' suoi chiari e scuri ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#203">XCII. Cupola
          ottangolare ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#205">XCIII. Pianta
          geometrica della Chiesa di Sant'Ignatio di Roma
          ›Sant'Ignazio‹ ▣&#160;</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#207">XCIV. Elevation
          geometrica della Chiesa di Sant'Ignatio Sant'Ignazio‹
          ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#209">XCV. Altre
          preparationi alle figure 98 e 99 ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#211">XCVI. Altre
          preparationi alle figure 98 e 99 ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#213">XCVII. Altra
          preparatione alle figure 98 e 99 ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#215">XCVIII. Un quarto
          ombreggiato di tutta l'Opera ▣</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#217">XCIX. Un altro
          quarto dell'Opera σ</a>
        </li>
        <li>
          <a href="ghpoz986729301s.html#219">C. Modo di far la
          graticola nelle volte ▣</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghpoz986729301s.html#222">Indice</a>
    </li>
  </ul>
  <hr />
</body>
</html>
