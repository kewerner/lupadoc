<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghcio9413250s.html#6">La pittura in Parnaso</a>
      <ul>
        <li>
          <a href="ghcio9413250s.html#8">Illustrissimo
          Signore</a>
        </li>
        <li>
          <a href="ghcio9413250s.html#14">L'autore a chi
          legge</a>
        </li>
        <li>
          <a href="ghcio9413250s.html#25">Indice de' capitoli</a>
        </li>
        <li>
          <a href="ghcio9413250s.html#30">Parte prima</a>
          <ul>
            <li>
              <a href="ghcio9413250s.html#30">I. Ragioni per la
              quali la Pittura pensa di portarsi in Parnaso</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#34">II. La Pittura
              risolve portarsi in Parnaso, e suoi preparamenti</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#38">III. Arrivo della
              Pittura in Parnaso, e sua entratura</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#43">IV. Descrizione del
              Monte Parnaso</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#49">V. La Pittura è
              ammessa all'udienza d'Apollo, e suo trattamento</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#54">VI. Gli onori
              ricevuti dalla Pittura causano bisbiglio in
              Parnaso</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#58">VII. Descrizione
              delle cinque parti, in cui la Pittura si divide, e
              prima del Disegno</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#62">VIII. Della seconda
              parte, che è l'Invenzione</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#66">IX. Della terza
              parte, cioè delle Attitudini in varie positure
              poste</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#68">X. Della quarta
              parte, cioè della Simetria delle membra, e degli
              scorsi loro</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#72">XI. Della quinta
              parte, che è l' Colorito</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#76">XII. Se veri sieno
              alcuni errori, che a' Pittori vengono
              attribuiti&#160;</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#80">XIII. Difesa de'
              Pittori nelle Tavole, che sono in Santa Maria
              Novella</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#86">XIV. Del
              risorgimento della Pittura seguito in Santa Maria
              Novella</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#96">XV. Altro
              ragionamento sopra le Tavole di Santa Maria
              Novella</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#101">XVI. Che
              nell'opera per la Chiesa non solo i Pittori, ma anche
              gli Scultori debbono osservare il decoro</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#105">XVII. Che operando
              per la Chiesa anche gli Archtietti debbono rifare il
              decoro</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#113">XVIII. Delle
              Pitture di San Lorenzo, e della Cupola del Duomo</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#118">XIX. Ragionamento
              sopra alcuni Quadri di Tiziano, e di un altro Pittore
              Fiammingo citati dal Borghini</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#122">XX. Se l' dipigner
              Figure ideali per aria ovvero Amorini senza ali sia
              errore</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#127">XXI. Riflessioni
              della Pittura, sopra il Riposo di Raffaello
              Borghini</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#135">XXII. Che cerca
              qualità di errori i quali in alcune Pitture si
              veggono non sempre dipendendo da chi ha operato</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghcio9413250s.html#141">Parte seconda</a>
          <ul>
            <li>
              <a href="ghcio9413250s.html#141">I. Che molti
              Scrittori non essendo dell'Arte, troppo
              autorevolmente si mettono a trattar di Pittura</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#149">II. Chi più vaglia
              nel rappresentare, o la Poesia, o la Pittura</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#154">III. Che'l non
              arrivar la Poesia a rappresentare al vivo è
              mancamento dell'Arte non dei Poeti</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#157">IV. Che gl'
              Istorici contraddicendosi fra di loro, oppur da se
              medesimi levar qualche pregia all' Istoria</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#160">V. Che gl'
              Istorici nel descrivere la Vita altrui, non
              dovrebbero palesar quei vizzi che pubblici non
              sono</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#165">VI. Che gl'
              Istorici scrivendo cose impossibili levan la fede
              all' Istoria</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#168">VII. Se sia
              possibile che gli Antichi stessero a mensa su i letti
              mangiando a diacere</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#183">VIII. Che molto
              Arti son sottoposte ad alcune eccezioni, alle quali
              la Pittura non è soggetta&#160;</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#187">IX. Che la Pittura
              non è sogetta a mutazioni, o alterazioni, come sono
              diverse Arti&#160;</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#191">X. Che molti
              Scrittori inalzando con smoderatezza l' Arte loro,
              fanno poca stima dell' altre Virtù, e
              Prosessioni&#160;</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#196">XI. Risposta a
              Valerio Massimo circa la bassa stima, che ci fa della
              Pittura</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#199">XII. In quanta
              stima sia stata la Pittura aprpesso al Mondo ne'
              tempi antichi</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#203">XIII. In quale
              stima sia stata la Pittura appresso al Mondo ne i
              tempi più moderni</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#207">XIV. In quale
              stima sia l'Arte della Pittura appresso alle Barbare
              Nazioni&#160;</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#211">XV. In quale stima
              sia la Pittura appresso a Iddio</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#217">XVI. Di alcuni
              Teologi, che mettono in dubbio, se la Pittura possa
              esercitarsi in giorno festivo</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#221">XVII. Avvertimento
              a i Pittori circa il lavorar le stesse</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#224">XVIII. Delle
              Pitture lascrive</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#228">XIX. Qual sorta di
              Pitture posson' esser tenute per le case</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#232">XX. Se per la case
              posson tenersi Pitture, che reppresentan cose
              Idolatre&#160;</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#237">XXI. Se il farsi
              fare il ritratto sia effetto di vanità, e di
              superbia</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghcio9413250s.html#242">Parte terza</a>
          <ul>
            <li>
              <a href="ghcio9413250s.html#242">I. Quale sia nel
              Mondo il vero sapere</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#253">II. Abboccamento
              della Pittura, e della Scultura seguito in
              Parnaso</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#256">III. Apollo
              ordina, che la causa della Pittura, e della Scultura,
              si vegga per via di ragioni nel suo Tirbunale</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#261">IV. Ragioni
              prodotte dalla Pittura in favor de' Pittori</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#268">V. Ragioni
              prodotte dalla Scultura in favore degli Scultori</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#271">VI. Risposta fatta
              dalla Scultura alle ragioni state prodotte dalla
              Pittura</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#275">VII. Risposta
              della Pittura alla prima delle dieci ragioni prodotte
              dalla Scultura</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#278">VIII. Risposta
              alla seconda ragione prodotta dalla Scultura</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#281">IX. Risposta alla
              terza ragione degli Scultori</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#283">X. Risposta alla
              quarta ragione degli Scultori</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#285">XI. Risposta alla
              quinta ragione degli Scultori</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#288">XII. Risposta alla
              sesta ragione degli Scultori</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#292">XIII. Risposta
              alla settima ragione degli Scultori</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#294">XIV. Risposta all'
              ottava ragione degli Scultori</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#298">XV. Risposta alla
              nuova ragione degli Scultori</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#301">XVI. Risposta alla
              decima ragione degli Scultori</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#304">XVII. La Pittura,
              e la Scultura sono esominate strettamente sopra l'
              fine, che hanno nella lor Arte</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#309">XVIII. L'
              Architettura procura accordo fra le due sorelle</a>
            </li>
            <li>
              <a href="ghcio9413250s.html#314">XIX. Comlimento di
              congratulazione fatto da Apollo alle due sorelle per
              essersi mezzo dell' Architettura amichevolmente
              accordate</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghcio9413250s.html#321">Lettera responsiva
          all'illustrissimo Signor Cavaliere Francesco Maria
          Niccolò Gabburri</a>
        </li>
        <li>
          <a href="ghcio9413250s.html#328">Tavola delle cose più
          notabili&#160;</a>
        </li>
        <li>
          <a href="ghcio9413250s.html#337">Approvazioni</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
