<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="calan480350s.html#4">Disegno della Loggia di San
      Pietro in Vaticano dove si da la Benedizione ›San Pietro in
      Vaticano‹</a>
    </li>
    <li>
      <a href="calan480350s.html#6">[Tavole]</a>
      <ul>
        <li>
          <a href="calan480350s.html#6">III. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#8">IV. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#10">V. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#12">VI. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#14">VII. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#16">VIII. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#18">IX. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#20">X. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#22">XI. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#24">XII. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#26">XIII. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#28">XIV. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#30">XV. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#32">XVI. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#34">XVII. ▣</a>
        </li>
        <li>
          <a href="calan480350s.html#36">XVIII. ▣</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
