<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="efir8403330s.html#6">Descrizione e studj
      dell’insigne fabbrica di S. Maria del Fiore, metropolitana
      fiorentina in varie carte intagliati</a>
      <ul>
        <li>
          <a href="efir8403330s.html#8">Altezza Reale</a>
        </li>
        <li>
          <a href="efir8403330s.html#14">Lo stampatore a chi
          legge</a>
        </li>
        <li>
          <a href="efir8403330s.html#18">Descrizione dell'insigne
          fabbrica di S. Maria del Fiore di Firenze&#160;</a>
        </li>
        <li>
          <a href="efir8403330s.html#54">Sonetto</a>
        </li>
        <li>
          <a href="efir8403330s.html#55">Sonetto</a>
        </li>
        <li>
          <a href="efir8403330s.html#57">[Tavole]</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
