<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ghvas409168012s.html#8">Le Vite de' più
      eccellenti Pittori, Scultori, e Architettori [...] Prima, e
      Seconda Parte</a>
      <ul>
        <li>
          <a href="ghvas409168012s.html#10">Allo Illustrissimo
          et Eccellentissimo Signor Cosimo Medici Duca di Fiorenza
          e Siena [dedica dell'autore]</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#14">Allo Illustrissimo
          et Eccellentissimo Signore il Signor Cosimo de Medici
          Duca di Fiorenza [dedica dell'autore (già premessa
          all'edizione del 1550)]</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#18">[Privilegio di
          stampa (Motu proprio di Sisto V)]</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#19">[Errata
          corrige]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvas409168012s.html#24">[Indici]</a>
      <ul>
        <li>
          <a href="ghvas409168012s.html#24">Indice copioso
          delle cose più notabili [...]</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#42">Tavola de ritratti
          [...]</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#45">Tavola delle vite
          de gli artefici [...]</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#47">Tavola de luoghi,
          dove sono l'opere descritte [...]</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvas409168012s.html#64">Proemio di tutta
      l'opera</a>
    </li>
    <li>
      <a href="ghvas409168012s.html#73">Introduzzione [sic] di
      Messer Giorgio Vasari pittore aretino, alle tre Arti del
      Disegno, cioè Architettura, Pittura, et Scoltura, et prima
      dell'Architettura</a>
      <ul>
        <li>
          <a href="ghvas409168012s.html#73">I. Delle diverse
          pietre, che servono agl'Architetti per gl'ornamenti, et
          per le statue alla Scoltura</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#83">II. Che cosa sia il
          lavoro di quadro semplice, et il lavoro di quadro
          intagliato</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#84">III. De' cinque
          ordini d'architettura Rustico, Dorico, Ionico, Corinto
          [sic], composito, et del lavoro Tedesco</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#89">IIII. Del fare le
          volte di getto, che vengano intagliate quando si
          disarmino; et d'impastar lo stucco</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#90">V. Come di Tartari,
          et di colature d'acque si conducono le Fontane Rustiche,
          et come nello stucco si murano le Telline, et le colature
          delle pietre cotte</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#92">VI. Del modo di
          fare i Pavimenti di commesso</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#93">VII. Come si ha a
          conoscere uno edificio proporzionato bene, et che parti
          generalmente se li convengono</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvas409168012s.html#95">Della Scultura</a>
      <ul>
        <li>
          <a href="ghvas409168012s.html#95">VIII. Che cosa sia
          la Scultura, et come siano fatte le sculture buone; et
          che parti elle debbino havere, per essere tenute
          perfette</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#96">IX. Del fare i
          modelli di cera, et di terra, et come si vestino; et come
          à proporzione si ringrandischino poi nel marmo; come si
          subbino, et si gradinino, et pulischino, et impomicino;
          et si lustrino, et si rendino finiti</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#99">X. De' bassi, et
          de' mezzi Rilievi; la difficultà del fargli; et in che
          consista il condurgli a perfezzione [sic]</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#100">XI. Come si fanno
          i modelli per fare di bronzo le figure grandi et
          picciole; et come le forme, per buttarle; come si armino
          di ferri, et come si gettino di metallo; et di tre sorti
          bronzo; et come gittate si ceselino, et si rinettino; et
          come mancando pezzi, che non fussero venuti, s'innestino,
          et commettino nel medesimo bronzo</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#103">XII. De' conij
          d'acciaio per fare le medaglie di bronzo, o d'altri
          metalli, et come elle si fanno di essi metalli; di pietre
          orientali, et di Cammei</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#104">XIII. Come di
          stucco si conducono i lavori bianchi, et del modo del
          fare la forma di sotto murata, et come si lavorano</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#105">XIIII. Come si
          conducono le figure di legno, et che legno sia buono a
          farle</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvas409168012s.html#106">Della pittura</a>
      <ul>
        <li>
          <a href="ghvas409168012s.html#106">XV. Che cosa sia
          disegno, et come si fanno, et si conoscono le buone
          Pitture, et a che; et dell'invenzione delle storie</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#109">XVI. Degli schizzi
          disegni, cartoni, et ordine di prospettive; et per quel,
          che si fanno, et a quello che i Pittori se ne servono</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#110">XVII. De li scorti
          delle figure al disotto, in su, et di quelli in piano</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#111">XVIII. Come si
          debbino unire i colori a olio, a fresco, ò a tempera; et
          come le carni, i panni, et tutto quello che si dipinge,
          venga nell'opera a unire in modo che le figure non
          venghino divise; et habbino rilievo, et forza, e mostrino
          l'opera chiara, et aperta</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#113">XIX. Del dipingere
          in muro, come si fa; et perché si chiama lavorare in
          fresco</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#113">XX. Del dipignere
          a tempera ò vero a uovo su le tavole; ò tele, et come si
          può usare sul muro che sia secco</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#114">XXI. Del dipingere
          a olio, in tavola, et su le tele</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#115">XXII. Del pingere
          a olio nel muro, che sia secco</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#116">XXIII. Del
          dipignere a olio su le tele</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#117">XXIIII. Del
          dipingere in pietra a olio, et, che pietre siano
          buone</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#117">XXV. Del dipignere
          nelle mura di chiaro, et scuro di varie terrette, et come
          si contrafanno le cose di Bronzo, et delle storie di
          terretta per archi, o per feste, a colla, che è chiamato
          a guazzo, et a tempera</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#118">XXVI. Degli
          sgraffiti delle case, che reggono a l'acqua; quello che
          si adoperi a fargli; et come si lavorino le Grottesche
          nelle mura</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#119">XXVII. Come si
          lavorino le grottesche su lo stucco</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#119">XXVIII. Del modo
          di mettere d'oro a bolo, et a mordente, et altri modi</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#120">XXIX. Del Musaico
          de' vetri, et a quello, che si conosce il buono, et
          lodato</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#122">XXX. Dell'istorie,
          et delle figure, che si fanno di commesso ne' Pavimenti,
          ad imitazione delle cose di chiaro, et scuro</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#123">XXXI. Del musaico
          di legname, cioè delle Tarsie: et dell'istorie, che si
          fanno di legni tinti, et commessi a guisa di Pitture</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#124">XXXII. Del
          dipignere le finestre di vetro; et come elle si
          conduchino co' piombi, e co' ferri da sostenerle senza
          impedimento delle figure</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#126">XXXIII. Del
          Niello, e come per quello habbiamo le stampe di rame; et
          come s'intaglino gl'argenti, per fare gli smalti di basso
          rilievo, et similmente si ceselino le grosserie</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#128">XXXIIII. Della
          Tausia, cioè lavoro a la Damaschina</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#128">XXXV. De le stampe
          di legno; et del modo di farle, et del primo Inventor
          loro; et come con tre stampe si fanno le carte, che
          paiono disegnate; et mostrano il lume, il mezzo, e
          l'ombre</a>
        </li>
      </ul>
    </li>
    <li>
      <a href="ghvas409168012s.html#130">[Vite]</a>
      <ul>
        <li>
          <a href="ghvas409168012s.html#130">Proemio delle
          Vite</a>
        </li>
        <li>
          <a href="ghvas409168012s.html#145">Parte Prima</a>
          <ul>
            <li>
              <a href="ghvas409168012s.html#146">Cimabue [Cenni
              di Pepo]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#151">Arnolfo di
              Cambio</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#160">Nicola e
              Giovanni Pisano</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#170">Andrea
              Tafi</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#174">Gaddo Gaddi di
              Zanobi</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#178">Margaritone
              d'Arezzo [Margarito di Magnano]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#182">Giotto di
              Bondone</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#197">Agostino di
              Giovanni [Agostino da Siena] e Agnolo di Ventura
              [Agnolo da Siena]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#203">Stefano
              Fiorentino e Ugolino da Siena [Ugolino di Neri]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#207">Pietro
              Lorenzetti</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#211">Andrea Pisano
              [Andrea di Ugolino di Nino da Pontedera]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#217">Buffalmacco
              [Buonamico di Maestro Martino]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#227">Ambrogio
              Lorenzetti</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#230">Pietro
              Cavallini [Pietro Cerroni]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#233">Simone Martini
              [Simone di Memmi]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#238">Taddeo
              Gaddi</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#245">Orcagna
              [Andrea di Cione]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#252">Giottino
              [Giotto di Maestro Stefano]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#256">Giovanni del
              Ponte [Giovanni di Marco]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#258">Agnolo Gaddi
              [Agnolo di Taddeo]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#263">Barna da
              Siena</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#266">Duccio di
              Buoninsegna</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#269">Antonio di
              Francesco [Antonio Veneziano]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#272">Jacopo del
              Casentino [Iacopo Landini]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#275">Spinello
              Aretino [Spinello di Luca Spinelli]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#283">Gherardo
              Starnina [Gherardo di Jacopo]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#285">Lippo di
              Benivieni</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#288">Lorenzo Monaco
              [Piero di Giovanni]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#291">Taddeo di
              Bartolo</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#294">Lorenzo di
              Bicci</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ghvas409168012s.html#300">Seconda parte</a>
          <ul>
            <li>
              <a href="ghvas409168012s.html#300">Proemio</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#308">Iacopo della
              Quercia</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#312">Niccolò
              Lamberti [Niccolò Aretino; il Pela]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#315">Dello Delli
              [Dello di Niccolò di Dello; Niccolò Fiorentino]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#318">Nanni di Banco
              [Nanni d'Antonio di Banco]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#321">Luca della
              Robbia</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#327">Paolo Uccello
              [Paolo di Dono]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#334">Lorenzo
              Ghiberti</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#346">Masolino da
              Panicale [Tommaso di Cristofano Fini]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#349">Parri
              Spinelli</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#354">Masaccio
              [Tommaso Guidi]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#360">Filippo
              Brunelleschi</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#386">Donatello
              [Donato di Niccolò di Betto Bardi]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#397">Michelozzo di
              Bartolomeo</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#406">Filarete
              [Antonio Averlino; Verulanus, Antonius]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#409">Giuliano da
              Maiano [Giuliano di Leonardo d'Antonio]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#412">Piero della
              Francesca</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#417">Fra Angelico
              [Beato Angelico da Fiesole]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#425">Leon Battista
              Alberti</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#430">Lazzaro
              Vasari</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#434">Antonello da
              Messina</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#438">Alessio
              Baldovinetti [Alesso Baldovinetti]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#442">Bartolomeo
              Bellano [Bartolommeo Bellano]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#444">Filippo Lippi
              [Fra' Filippo del Carmine]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#451">Paolo Romano
              [Paulus Salvati], Mino del Reame [Mino del Regno],
              Chimenti di Leonardo Camicia</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#453">Andrea del
              Castagno [Andrea di Bartolo] e Domenico Veneziano
              [Domenico di Bartolomeo]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#459">Gentile da
              Fabriano e Pisanello [Antonio di Puccio Pisano]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#463">Pesello
              [Giuliano d'Arrighi] e Pesellino [Francesco di
              Stefano]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#465">Benozzo
              Gozzoli [Benozzo di Lese di Sandro]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#469">Francesco di
              Giorgio Martini e il Vecchietta [Lorenzo di
              Pietro]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#471">Antonio e
              Bernardo Rossellino [Antonio e Bernardo Bora]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#475">Desiderio da
              Settignano</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#478">Mino da
              Fiesole [Mino di Giovanni]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#482">Lorenzo
              Costa</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#485">Ercole de'
              Roberti [Ercole Grandi]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#488">Jacopo,
              Giovanni e Gentile Bellini</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#496">Cosimo
              Rosselli</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#499">Francesco
              d'Angelo Cecca [Francesco La Cecca]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#505">Bartolomeo
              Della Gatta [Pietro di Antonio Dei]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#510">Gherardo di
              Giovanni del Fora</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#513">Domenico
              Ghirlandaio [Domenico Bigordi]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#522">Antonio e
              Piero del Pollaiuolo [Antonio Pollaiolo e Piero
              Pollaiolo]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#527">Sandro
              Botticelli [Alessandro Filipepi]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#533">Benedetto da
              Maiano</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#538">Andrea del
              Verrocchio [Andrea Cioni]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#544">Andrea
              Mantegna</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#550">Filippino
              Lippi</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#555">Pinturicchio
              [Pintoricchio]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#559">Francesco
              Francia [Francesco Francia Raibolini]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#564">Perugino
              [Pietro Vannucci]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#575">Vittore
              Carpaccio</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#581">Iacopo Torni
              [Indaco]</a>
            </li>
            <li>
              <a href="ghvas409168012s.html#583">Luca
              Signorelli [Luca da Cortona]</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
