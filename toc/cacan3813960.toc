<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.4.0" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="cacan3813960s.html#9">Le sculture e le pitture di
      Antonio Canova, pubblicate fino a quest’anno 1795</a>
      <ul>
        <li>
          <a href="cacan3813960s.html#11">A sua eccellenza
          Francesco Pesaro</a>
        </li>
        <li>
          <a href="cacan3813960s.html#13">Cortese lettore</a>
        </li>
        <li>
          <a href="cacan3813960s.html#15">Le sculture e le
          pitture di Antonio Canova</a>
        </li>
        <li>
          <a href="cacan3813960s.html#101">Indice delle opere qui
          descritte</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
