<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.7.16" />
  <title></title>
  <style type="text/css">
  /*<![CDATA[*/
  <!--
  .xflip {
    -moz-transform: scaleX(-1);
    -webkit-transform: scaleX(-1);
    -o-transform: scaleX(-1);
    transform: scaleX(-1);
    filter: fliph;
  }
  .yflip {
    -moz-transform: scaleY(-1);
    -webkit-transform: scaleY(-1);
    -o-transform: scaleY(-1);
    transform: scaleY(-1);
    filter: flipv;
  }
  .xyflip {
    -moz-transform: scaleX(-1) scaleY(-1);
    -webkit-transform: scaleX(-1) scaleY(-1);
    -o-transform: scaleX(-1) scaleY(-1);
    transform: scaleX(-1) scaleY(-1);
    filter: fliph + flipv;
  }
  -->
  /*]]>*/
  </style>
</head>
<body>
  <a name="outline" id="outline"></a>
  <h1>Document Outline</h1>
  <ul>
    <li>
      <a href="ebol673760s.html#6">Il Claustro di San Michele in
      Bosco di Bologna&#160;</a>
    </li>
    <li>
      <a href="ebol673760s.html#8">[Text]&#160;</a>
      <ul>
        <li>
          <a href="ebol673760s.html#8">[Dedica dal pittore
          Giampietro Zanotti ai lettori]</a>
          <ul>
            <li>
              <a href="ebol673760s.html#8">Monastero di San
              Michele in Bosco de' Monaci ▣</a>
            </li>
            <li>
              <a href="ebol673760s.html#18">[Ritratto del
              pittore] Giampietro Cavazzoni Zanotti ▣&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#20">[Dipinti di Francesco
          Brizio]</a>
          <ul>
            <li>
              <a href="ebol673760s.html#20">[1.] Il nato San
              Benedetto in grembo alla Allevatrice ▣&#160;</a>
              <ul>
                <li>
                  <a href="ebol673760s.html#22">[Descrizione
                  dell'opera]&#160;</a>
                </li>
                <li>
                  <a href="ebol673760s.html#24">[Ritratto del
                  pittore] Francesco Brizio ▣&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ebol673760s.html#26">[2.] Santa Cecilia
              genuflessa, intenta alla melodia, che fanno alcuni
              Angioletti ▣&#160;</a>
              <ul>
                <li>
                  <a href="ebol673760s.html#28">[Descrizione
                  dell'opera]&#160;</a>
                </li>
              </ul>
            </li>
            <li>
              <a href="ebol673760s.html#30">3. Valeriano, che
              porgendo la destra alla sua Sposa Cecilia, secolei
              s'incammina verso le proprie Case ▣&#160;</a>
              <ul>
                <li>
                  <a href="ebol673760s.html#32">[Descrizione
                  dell'opera]</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#34">4. San Benedetto
          Fanciullo, che s'incammina al Deserto, seguitato dagli
          sconsolati Parenti ▣ [di Lorenzo Garbieri]</a>
          <ul>
            <li>
              <a href="ebol673760s.html#36">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#40">[5.] San Benedetto, che
          stando sul limitare del suo Romitorio, riceve i presenti
          di quei vicini rustici Abitatori ▣ [di Guido
          Reni]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#42">[Descrizione
              dell'opera]&#160;</a>
            </li>
            <li>
              <a href="ebol673760s.html#45">[Ritratto del
              pittore] Guido Reni ▣&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#46">6. San Benedetto, che si
          butta nelle spine per liberarsi dalla tentazione di una
          rea Femmina ▣ [di Sebastiano Razali]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#48">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#50">7. Santa Cecilia in
          Camera, che discorre con Valeriano ▣ [di Aurelio
          Bonelli]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#52">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#54">8. Valeriano, che da
          &#160;alcuni Poverelli si fa insegnare la via per ire a
          piedi dell'ascoso Pontefice ▣ [di Baldassare
          Galanino]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#56">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#57">[Ritratto del
              pittore] Baldassare Galanino ▣&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#58">[9.] Placido liberato
          miracolosamente dal sommergersi nel Fiume ▣ [di Lucio
          Massari]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#60">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#62">Lucio Massari ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#64">10. La Manaja ritrovata
          prodigiosamente nel Lago ▣ [di Lucio Massari]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#66">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#67">Veduta di dentro
              dell'ingresso nel Monumento di San Michele in Bosco
              &#160;di Bologna [...] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#68">11. Valeriano, che dal
          Santo Pontefice riceve l'acque Battesimali ▣ [di Lorenzo
          Garbieri] &#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#70">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#70">[Ritratto del
              pittore] Lorenzo Garbieri ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#72">12. L'Angelo, che offre a
          Cecilia ed a Valeriano due Ghirlande di fiori colti in
          Paradiso ▣ [di Lorenzo Garbieri]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#74">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#76">13. Il Prete, che invaso
          dal Demonio, che viene liberato dal Santo ▣ [di Lodovico
          Caracci]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#78">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#81">[Ritratto del
              pittore] Lodovico Caracci ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#82">[14.] Il gran Sasso reso
          immobile dal Demonio, che su vi guace, liberato dal Santo
          ▣ [di Lodovico Caracci] &#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#84">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#86">Veduta dell'Atrio che
              introduce nel secondo Claustro del Monastero di San
              Michele in Bosco di Bologna ▣&#160;</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#88">15. La Cucina, che per
          arte del Demonio pare, che tutta arda, e avvampi,
          liberata dal Santo ▣ [di Lodovico Caracci]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#90">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#91">Facciata della
              Chiesa, e Monastero di San Michele in Bosco di
              Bologna ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#92">16. Vari Martiri, che per
          opera di Santa Cecila sono partiti alla Sepoltura ▣ [di
          Giacomo Cavedone]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#94">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#96">[Ritratto del
              pittore] Giacomo Cavedone ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#98">17. Li santi Fratelli
          Valeriano, e Tiburzio martirizzati ▣&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#100">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#102">18. Le scellerate
          Femmine mandate a tentare d'impudicizia San Benedetto,
          che se ne liberò fuggendo ▣ [di Lodovico
          Caracci]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#104">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#106">[19.] Totila
          accompagnato dal numeroso suo Esercito, che adora il
          Santo Abbate Benedetto ▣ [di Lodovico Caracci]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#108">[Descrizoine
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#112">20. La Pazza, che corre,
          l'uso suo folle seguendo, ne sa dove, e che alla perfine
          ottenne sanità ▣ [di Lodovico Caracci]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#114">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#115">Veduta del Claustro
              nel Monastero di San Michele in Bosco di Boligna
              [...] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#116">21. Li Santi Valeriano,
          e Tiburzio, che decapitati, sono portati a seppellire ▣
          [di Alessandro Albini]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#118">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#119">Veduta del Secondo
              Claustro del Monastero di San Michele in Bosco di
              Bologna ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#120">22. Santa Cecilia, che
          gitta in terra l'inutile suo Organetto ▣ [di Alessandro
          Albini]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#122">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#123">Veduta interiore
              dell'Ingresso nel Monastero di San Michele in Bosco
              di Bologna [...] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#124">23. Il morto Giovinetto
          resuscitato dal Santo Abbate ▣ [di Alessandro
          Albini]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#126">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#128">24. Il Frumento trovato,
          e da' Facchini portato a riposri ▣ [di Lucio
          Massari]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#130">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#132">25. Santa Cecilia, che
          dispensa a' Poverelli le sue richezze ▣ [di Tomaso
          Campana]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#134">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#135">Prospettiva di
              Angelo Michele Colonna in capo allo Stradone a San
              Michele in Bosco di Bologna ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#136">[26.] Santa Cecilia, che
          avanti ad Almachio Prefetto di Roma ricusa di sacrificare
          agl'Idoli ▣ [di Tommaso Campana]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#138">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#140">[27.] Le Monache morte
          che escono della Sepoltura ▣ [di Lucio Massari]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#142">[Descrizione
              dell'opera]&#160;</a>
            </li>
            <li>
              <a href="ebol673760s.html#143">Chiesa, e Monastero
              di San Michele in Bosco [...] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#144">[28.] La grande Storia
          del Monaco dissotterrato [a fresco] ▣ [di Allessandro
          Tiarini]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#146">[Descrizione
              dell'opera]&#160;</a>
            </li>
            <li>
              <a href="ebol673760s.html#150">[Ritratto del
              pittore] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#152">29. Il Monaco
          precipitato dai Demonj giù dalla Fabbrica, e dal Santo
          scampato prodigiosamente ▣ [di Leonello Spada]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#154">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#157">[Ritratto del
              pittore] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#158">30. Santa Cecila esposta
          alla atrocità delle fiamme ▣ [di Leonella
          Spada]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#160">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#162">31. Santa Cecilia
          decapitata ▣ [di Lorenzo Garbieri]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#164">[Descrizione
              dell'opera]</a>
            </li>
            <li>
              <a href="ebol673760s.html#165">Veduta del Terzo
              Claustro del Monastero di San Michele in Bosco di
              Bologna ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#166">32. Ruggero, che
          discorre con San Benedetto ▣ [di Giacomo
          Cavedone]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#168">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#170">[33.] L'incendio, et il
          saccheggiamento di Monte Cassino in tempo di notte ▣ [di
          Lodovico Carracci]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#172">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#174">34. Il Contadino
          condotto dai Ladri, e liberato da San Benedetto ▣ [di
          Lorenzo Garbieri]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#176">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#178">35. Santa Cecilia
          moribonda in braccio a pietosi Cristiani, che piangono, e
          le asciugano il sangue delle ferite ▣ [di Lorenzo
          Garbieri]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#180">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#182">36. Santa Cecilia
          portata alla Sepoltura ▣ [di Lorenzo Garbieri]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#184">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#186">37. Morte di San
          Benedetto, e l'Anima sua portata dagli Angeli in Paradiso
          ▣ [di Giacomo Cavedone]&#160;</a>
          <ul>
            <li>
              <a href="ebol673760s.html#188">[Descrizione
              dell'opera]</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#190">Termini</a>
          <ul>
            <li>
              <a href="ebol673760s.html#196">42.[sic!] ▣
              &#160;</a>
            </li>
            <li>
              <a href="ebol673760s.html#198">41. ▣</a>
            </li>
            <li>
              <a href="ebol673760s.html#200">43. ▣</a>
            </li>
            <li>
              <a href="ebol673760s.html#202">39.[sic!]
              ▣&#160;</a>
            </li>
            <li>
              <a href="ebol673760s.html#204">38. ▣</a>
            </li>
            <li>
              <a href="ebol673760s.html#206">[40.?] ▣</a>
            </li>
            <li>
              <a href="ebol673760s.html#208">45. ▣</a>
            </li>
            <li>
              <a href="ebol673760s.html#210">[44.?] ▣</a>
            </li>
          </ul>
        </li>
        <li>
          <a href="ebol673760s.html#212">Pianta del Claustro de'
          Monaci di San Michele in Bosco di Bologna, dipinto da
          Lodovico Carracci, e sua Scuola ▣&#160;</a>
        </li>
        <li>
          <a href="ebol673760s.html#214">Indice delle Stampe, e
          sue Descrizioni contenute in questo Volume [...]</a>
        </li>
        <li>
          <a href="ebol673760s.html#216">Esposizione delle
          Vignette, e Finali, che ornano il presente Libro</a>
        </li>
      </ul>
    </li>
  </ul>
  <hr />
</body>
</html>
