# lupaDoc


## Overview

The LVPA project was a first take on the idea of adding a table of contents to digitized rare books.

The workflow was as follows:

0. Converting the digitized volumes into PDF
0. In the PDF, adding an outline using the usual tools
0. Exporting the outline as <ul> tree in plain HTML

During the creation of the outline in the PDF files, the transcription rules were as follows:

0. Transkribe diplomatically (i.e. as-is)
0. Enclose keywords or toponyms in french quotations.
0. Mark images or maps with an additional ▣ sign 

Examples are: 

*De oratorio sancti Laurentii in palacio quod dicitur sancta lanctorum ›San Lorenzo in Palatio ad Sancta Sanctorum‹*

*Del Ponte Trionfale ›Pons Neronianus‹*

*De' campi forastieri ›Castra Peregrina‹*

*Rione di ›Trastevere‹*

*Pianta del Tempio della Pace ›Foro della Pace‹* ▣

*Insula in effigiem navis Aesculapio sacra ›Isola Tiberina‹* ▣


## The Files

The [toc](https://gitlab.mpcdf.mpg.de/kewerner/lupadoc/-/tree/main/toc?ref_type=heads) folder contains the exported and converted TOC files in HTML format. These can be used to re-attach the TAC to any transkribed document as outline. An example would be [Dg 450-1580](https://dlib.biblhertz.it/m3s/?show=https://dlib.biblhertz.it/iiif/dg4501580/manifest.json).

The [data](https://gitlab.mpcdf.mpg.de/kewerner/lupadoc/-/tree/main/data?ref_type=heads) folder (15.933 entries) instead carries 2 datasets:

- "lupa-data" is the complete list of all citations with relevant toponym, reference (volume and scan number), complete link (ie. viwer+manifest+canvas) and - in order to be futureproof - again a separate manifest and canvas (for use with any future viewer).

This table is relationally linked to the lupa-lut table on "lupa":

- "lupa-lut" instead is the Look Up Table with all statements regarding a specific toponym: the lupa name (which acts as ID), the zuccaro value, the census and gnd values, notes, and e simple geo point.

## The Cloud Database MementoDB

The cloud database contains 2 tables for the above mentioned database files.

The **lupa-data** table (1183 entries) contains (Memento Id http://libs.mobi/s/E7eirWEoT):

| entry | type | description |
| ----- | ---- | ----------- |
| lupa  | link-to-entry | lupa ID     |
| citation  | text | the original string complete with quotation marks     |
| type | text | image or text |
| reference | text | volume title, year and scan number |
| year | integer | date of publication |
| century | integer | corresponding century (for heatmaps etc) |
| link | hyperlink | full link with viewer + manifest + scan |
| manifest | hyperlink | manifest only |
| canvas | integer | scan number only |


The **lupa-lut** table contains (Memento Id http://libs.mobi/s/3226LUjJT):

| entry | type | description |
| ----- | ---- | ----------- |
| lupa | text | lupa ID (UNIQUE) |
| wikidata | text | Wikidata ID |
| zuccaro | text | Zuccaro ID |
| zuccaroLabel | text | Zuccaro label |
| fototecaLabel | text | Fototeca label |
| census | text | Census ID |
| censusLabel | text | Census label |
| gnd | text | GND ID|
| gndLabel | text | GND Label |
| notes | text | notes |
| alternates | text | alternative names |
| sample | text | sample volume (shelf number) |
| geopoint | geo | POINT |

### Census, Fototeca and GND ID

The Census, Fototeca (anyway only a text string) and GND values are legacy values and NOT intended to be used. Please refer to the Wikidata statements online - only these are updated regularly.

### Link vs Viewer+Manifest+Canvas

The LUT table provides in fact 2 ways for getting a link to a Viewer with the referenced data:

- The LINK entry which provides the volume and the relevant scan for display in the Universal Viewer, eg.

```bash
https://universalviewer.io/uv.html?manifest=https://dlib.biblhertz.it/iiif/dn105320/manifest2.json#?cv=7
```

- the MANIFEST and CANVAS entries, which can be combined with any other viewer (not all viewers take the canvas parameter): 

```bash
https://dlib.biblhertz.it/m3s/?show=https://dlib.biblhertz.it/iiif/dn105320/manifest2.json
```

*EOF*